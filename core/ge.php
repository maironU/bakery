<?php
 class GeCore{
 
  /*public $host="localhost";
  public $userdb="root";
  public $passdb="";
  public $dbdb="pb";*/
  
  public $host=DBSERV;
  public $userdb=DBUSR;
  public $passdb=DBPASS;
  public $dbdb=DB;
  
  public $linky;
  public $res;
  
  //Datos de cliente
  public $customername=CUSTOMER;
  public $customerweb=CUSTOMERURL;
  
  //FileUpload vars
  public $fname;
  public $fsize;
  public $ftype;
  public $fTmp;
  
  //Paging Vars
  public $tam_page=30;
  public $page;
  public $start;
  
  
  public function connect(){
   $this->linky = mysqli_connect($this->host,$this->userdb,$this->passdb, $this->dbdb);
   //mysqli_select_db($this->dbdb,$this->linky);
   
  }
  
  public function close(){
   mysqli_close($this->linky);
  }
  
  public function Execute($query){
   return mysqli_query($this->linky, $query); 
  }
  
  public function ExecuteS($query){

   $this->res = $this->Execute($query);
   
   $i=0;
   while($i < $this->getNumRows($this->res))
   {
     $r[$i] = mysqli_fetch_assoc($this->res);
	 $i++;
   }
   
   return $r;
    
  }
  
  public function getLastID()
  {
   return mysqli_insert_id($this->linky);
  }
  
  public function getNumRows()
  {
   return mysqli_num_rows($this->res);
  }
  
  public function chkTables($table){
  	
	$sql="SHOW TABLES FROM ".$this->dbdb;
	$res=mysqli_query($this->linky, $sql);
	while($row=mysqli_fetch_row($res)){
		if($row[0]==$table){
		 return true;
		}
	}
	
	return false;
  }
  
  public function importSQLFile($file){


 			// To avoid problems we're reading line by line ...
 			$lines = file($file, FILE_IGNORE_NEW_LINES | FILE_SKIP_EMPTY_LINES);
 			
 			$buffer = '';
 			foreach ($lines as $line) {
 			
 				// Skip lines containing EOL only
 				if (($line = trim($line)) == '')
 					continue;

 				// skipping SQL comments
 				if (substr(ltrim($line), 0, 2) == '--')
 					continue;

 				// An SQL statement could span over multiple lines ...
 				if (substr($line, -1) != ';') {
 					// Add to buffer
 					$buffer .= $line;
 					// Next line
 					continue;
 				} else
 					if ($buffer) {
 						$line = $buffer . $line;
 						// Ok, reset the buffer
 						$buffer = '';
 					}

 				// strip the trailing ;
 				$line = substr($line, 0, -1);

 				// Write the data				
 				$this->Execute($line);
 	
 			}
 		
 	
  }
  
  public function getVars($string){
   $this->connect();
   if(is_array($_GET[$string]))
   {
   	return $_GET[$string];
   }
   else
   {
   	return mysqli_real_escape_string($this->linky, $_GET[$string]);
   }
  }
  
  public function postVars($string){
   $this->connect();
   if(is_array($_POST[$string]))
   {
   	return $_POST[$string];
   }
   else
   {
   	return mysqli_real_escape_string($this->linky, $_POST[$string]);
   }
  }
  
  public function upLoadFileProccess($field='', $folder = '/', $filename = ''){
   
   
   if(!empty($field))
   {
$ext = explode('.', $this->fname = $_FILES[$field]['name']);
   		   if($filename == '')
                   {
                     $this->fname = $_FILES[$field]['name'];
                   }
                   else
                   {
                     $this->fname = $filename.".".$ext[1];
                   }
		   $this->fsize = $_FILES[$field]['size'];
		   $this->ftype = $_FILES[$field]['type'];
		   $this->fTmp = $_FILES[$field]['tmp_name'];
		   
		   $path = realpath('./');
		   $path.="/".$folder."/";  
		   $path.=$this->fname;
		   
		   if(is_uploaded_file($this->fTmp))
		   {
			   if(move_uploaded_file($this->fTmp,$path))
			   {
					return true;
			   }
			   else
			   {
				return false;
			   }
		   }
		   else
		   {
			return false;
		   }
   }
   else
   {
   	return false;
   }
   
  }
  
  public function destroyFile($file){
   unlink($file);
  }
  
  public function getMenu(){
	
	 $sql="SELECT * FROM `menu`";
	 return $this->ExecuteS($sql);
	 
  }
  
  public function getSubMenu($id_menu){
	
	 $sql="SELECT * FROM `submenu` where m_id='$id_menu'";
	 return $this->ExecuteS($sql);
	 
  }
  
  public function hasSubmenu($id_menu){
  	$sql="SELECT * FROM `submenu` where m_id='$id_menu'";
	$row=$this->ExecuteS($sql);
	
	if(count($row)>0){
	 return true;
	}else{
	 return false;
	}
  }
	
	public function getMenuWithPermissions()
	{
	 $sql="select * from menu,pro_menu where menu.m_id=pro_menu.m_id and pro_menu.up_id='".$_SESSION['user_profile']."' order by menu.m_id asc";
	 $menu=$this->ExecuteS($sql);
	 
	 foreach($menu as $menu)
	 {
	  $html.='<li><a href="home.php?p='.$menu["m_link"].'">'.$menu["m_nom"].'</a></li>';
	 }
	 
	 return $html;
	}
	
	public function getSubMenuWithPermissions()
	{
	 $sql="select * from submenu as sm inner join pro_menu as pm on sm.sm_id=pm.sm_id where pm.up_id='".$_SESSION['user_profile']."' order by sm.sm_id asc";
	 $menu=$this->ExecuteS($sql);
	 
	 foreach($menu as $menu)
	 {
	  $html.='<li><a href="home.php?p='.$menu["sm_link"].'">'.$menu["sm_nom"].'</a></li>';
	 }
	 
	 return $html;
	}
	
	public function getSession(){
	 session_start();
	 setcookie(session_name(), $_COOKIE[session_name()], time()+86400);
	 if(!isset($_SESSION['user_id'])){
	  	header('Location: index.php');
	 }
	}
	
	public function closeSession(){
	 
	 session_destroy();
	 header('Location: index.php');
	}
	
	public function Validator($data=NULL,$required=0,$isEmail=0){
	
	 if($required==1){
	  if(empty($data)){
	   return false;
	  }
	 }
	 
	 if($isEmail==1){
	  if(!filter_var($data,FILTER_VALIDATE_EMAIL)){
	   	return false;
	  }
	 }
	 
	 return true;
	}
	
	//Date Functions 
	public function getMounthName($m){
	 
	 switch($m){
	 	case '01': $name="Enero";
		break;
		case '02': $name="Febrero";
		break;
		case '03': $name="Marzo";
		break;
		case '04': $name="Abril";
		break;
		case '05': $name="Mayo";
		break;
		case '06': $name="Junio";
		break;
		case '07': $name="Julio";
		break;
		case '08': $name="Agosto";
		break;
		case '09': $name="Septiembre";
		break;
		case '10': $name="Octubre";
		break;
		case '11': $name="Noviembre";
		break;
		case '12': $name="Diciembre";
		break;
		
	 }
	 
	 return $name;
	 
	}
	
	//paging
	public function startPage($page=''){
	 if(empty($page)){
	  $this->page=1;
	  $this->start=0;
	 }else{
	  $this->page=$page;
	  $this->start=($this->page-1)*$this->tam_page;
	 }
	}
	
	
	public function getPages($num=0){
	 if($num==0){
	  $total_page=1;
	 }else{
	  $total_page=ceil($num/$this->tam_page);
	 }
	 
	 $htm='
	 <div style="width:100%; position:relative; right:0px">
	 <form action="#" method="post" name="frmPage">
	  <label style="float:left"><strong>Paginas: </strong></label>
	  <select name="page" id="page" style="float:left">
	 ';
	 
	 for($i=1;$i<=$total_page;$i++)
	 {
		if($i==$this->postVars('page'))
		{
		 $htm.='<option value="'.$i.'" selected="selected">'.$i.'</option>';
		}
		else
		{
		 $htm.='<option value="'.$i.'">'.$i.'</option>';
		}
	 }
	 
	 $htm.='</select>
	 <input type="submit" value="ir" style="float:left" />
	 </form></div>';
	 
      return $htm;                   
	}
	
	//Configuration methods
	public function getValueByName($name){
	 $sql="select * from configuration where conf_name='$name'";
	 $row=$this->ExecuteS($sql);
	 if(count($row)>0){
	  foreach($row as $row){
	   return $row["conf_value"];
	  }
	 }
	}
	
	public function editConfig(){
	 $id=$this->postVars('id');
	 $value=$this->postVars('value');
	 
	 $sql="update configuration set conf_value='$value' where conf_id='$id'";
	 $this->Execute($sql);
	}
	
	public function getConfigZone($zone){
	 $sql="select * from configuration where conf_zone='$zone' order by conf_id asc";
	 return $this->ExecuteS($sql);
	}
	
	public function getGlobalConfig(){
	 $sql="select * from configuration group by conf_zone";
	 return $this->ExecuteS($sql);
	}
	
	//ZIP / UNZIP Methods
	public function createZip($files = array(),$destination = '',$overwrite = false, $base_dir = '') 
	{
		//print_r($files);
		//if the zip file already exists and overwrite is false, return false
		if(file_exists($destination) && !$overwrite) { return false; }
		//vars
		$valid_files = array();
		//if files were passed in...
		if(is_array($files)) {
		
			//cycle through each file
			foreach($files as $file) {
				//make sure the file exists
				if(file_exists($base_dir.$file)) {
					$valid_files[] = $file;
				}
			}
		}
		
		//if we have good files...
		if(count($valid_files)) {
			//create the archive
			$zip = new ZipArchive();
			
			if($zip->open($destination,$overwrite ? ZIPARCHIVE::OVERWRITE : ZIPARCHIVE::CREATE) !== true) {
				return false;
			}
			//add the files
			foreach($valid_files as $file) {
				$zip->addFile($base_dir.$file,$base_dir.$file);
			}
			//debug
			//echo $destination;
			//echo 'The zip archive contains ',$zip->numFiles,' files with a status of ',$zip->status;
			//echo $zip->getStatusString();
			
			//close the zip -- done!
			$zip->close();
			
			//check to make sure the file exists
			return file_exists($destination);
		}
		else
		{
			return false;
		}
	}
	
	public function extractZip($zipfile = "", $destination = "")
	{
	 $zipfile = realpath('./').$destination.$zipfile;
	 $zip = new ZipArchive();
	 if ($zip->open($zipfile)) {
	    
            $zip->extractTo(realpath('./').$destination);
            $zip->close();
            return true;
         }else{
          return false;
         }
	}
	
	//license checker
	public function license()
	{
		if(file_exists('js/goelyliccontrol.js'))
		{
			return $license = md5(GOEKEY.$_SERVER['SERVER_NAME']);
		} else {
		mail("monitor@goely.co","VIOLACION DE CODIGO FUENTE",$_SERVER['SERVER_NAME']);
			die('Verificador de licencia no existe');
		}
		
	}
  
  
  
}
 
?>