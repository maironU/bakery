<?php
 class GeCore{
 
 
  public $userdb="root";
  public $passdb="";
  public $dbdb="pb";
  public $linky;
  public $res;
  
  //Datos de cliente
  public $customername="Paint Ball App";
  public $customerweb="";
  
  //FileUpload vars
  public $fname;
  public $fsize;
  public $ftype;
  public $fTmp;
  
  
  
  public function connect(){
   $this->linky = mysql_connect($this->host,$this->userdb,$this->passdb);
   mysql_select_db($this->dbdb,$this->linky);
   
  }
  
  public function close(){
   mysql_close($this->linky);
  }
  
  public function Execute($query){
   return mysql_query($query,$this->linky); 
  }
  
  public function ExecuteS($query){

   $this->res = $this->Execute($query);
   
   $i=0;
   while($i < $this->getNumRows($this->res))
   {
     $r[$i] = mysql_fetch_assoc($this->res);
	 $i++;
   }
   
   return $r;
    
  }
  
  public function getLastID()
  {
   return mysql_insert_id($this->linky);
  }
  
  public function getNumRows()
  {
   return mysql_num_rows($this->res);
  }
  
  public function chkTables($table){
  	
	$sql="SHOW TABLES FROM ".$this->dbdb;
	$res=mysql_query($sql,$this->linky);
	while($row=mysql_fetch_row($res)){
		if($row[0]==$table){
		 return true;
		}
	}
	
	return false;
  }
  
  public function getVars($string){
   return $_GET[$string];
  }
  
  public function postVars($string){
   return $_POST[$string];
  }
  
  public function upLoadFileProccess($field='', $folder = '/'){
   
   
   if(!empty($field))
   {
   		   $this->fname = $_FILES[$field]['name'];
		   $this->fsize = $_FILES[$field]['size'];
		   $this->ftype = $_FILES[$field]['type'];
		   $this->fTmp = $_FILES[$field]['tmp_name'];
		   
		   $path = realpath('./');
		   $path.="/".$folder."/";  
		   $path.=$this->fname;
		   
		   if(is_uploaded_file($this->fTmp))
		   {
			   if(move_uploaded_file($this->fTmp,$path))
			   {
					return true;
			   }
			   else
			   {
				return false;
			   }
		   }
		   else
		   {
			return false;
		   }
   }
   else
   {
   	return false;
   }
   
  }
  
  public function getMenu(){
	
	 $sql="SELECT * FROM `menu`";
	 return $this->ExecuteS($sql);
	 
	}
	
	public function getMenuWithPermissions()
	{
	 $sql="select * from menu,pro_menu where menu.m_id=pro_menu.m_id and pro_menu.up_id='".$_SESSION['user_profile']."' order by menu.m_id asc";
	 $menu=$this->ExecuteS($sql);
	 
	 foreach($menu as $menu)
	 {
	  $html.='<li><a href="home.php?p='.$menu["m_link"].'">'.$menu["m_nom"].'</a></li>';
	 }
	 
	 return $html;
	}
	
	public function getSession(){
	 session_start();
	 if(!isset($_SESSION['user_id'])){
	  	header('Location: index.php');
	 }
	}
	
	public function closeSession(){
	 
	 session_destroy();
	 header('Location: index.php');
	}
	
	//Moviles
	public function check_user_agent ( $type = NULL ) {
        $user_agent = strtolower ( $_SERVER['HTTP_USER_AGENT'] );
        if ( $type == 'bot' ) {
                // matches popular bots
                if ( preg_match ( "/googlebot|adsbot|yahooseeker|yahoobot|msnbot|watchmouse|pingdom\.com|feedfetcher-google/", $user_agent ) ) {
                        return true;
                        // watchmouse|pingdom\.com are "uptime services"
                }
        } else if ( $type == 'browser' ) {
                // matches core browser types
                if ( preg_match ( "/mozilla\/|opera\//", $user_agent ) ) {
                        return true;
                }
        } else if ( $type == 'mobile' ) {
                // matches popular mobile devices that have small screens and/or touch inputs
                // mobile devices have regional trends; some of these will have varying popularity in Europe, Asia, and America
                // detailed demographics are unknown, and South America, the Pacific Islands, and Africa trends might not be represented, here
                if ( preg_match ( "/phone|iphone|itouch|ipod|symbian|android|htc_|htc-|palmos|blackberry|opera mini|iemobile|windows ce|nokia|fennec|hiptop|kindle|mot |mot-|webos\/|samsung|sonyericsson|^sie-|nintendo/", $user_agent ) ) {
                        // these are the most common
                        return true;
                } else if ( preg_match ( "/mobile|pda;|avantgo|eudoraweb|minimo|netfront|brew|teleca|lg;|lge |wap;| wap /", $user_agent ) ) {
                        // these are less common, and might not be worth checking
                        return true;
                }
        }
        return false;
}

//City functions
public function getCities(){
	$sql="select * from city order by city_name";
	return $this->ExecuteS($sql);
}


	
  
  
  
}
 
?>