<?php
 class menuCore extends GeCore{
 
 	public function getMenu(){
	
	 $sql="select * from menu";
	 return $this->ExecuteS($sql);
	 
	}
	
	public function getMenuWithPermissions()
	{
	 $sql="select * from menu,pro_menu where menu.m_id=pro_menu.m_id and pro_menu.up_id='".$_SESSION['user_profile']."' order by menu.m_id asc";
	 $menu=$this->ExecuteS($sql);
	 
	 foreach($menu as $menu)
	 {
	  $html.='<li><a href="home.php?p='.$menu["m_link"].'">'.$menu["m_nom"].'</a></li>';
	 }
	 
	 return $html;
	}
	
	public function newMenu(){
		
		$modName=$this->postVars('name');
		$modFolder1=$this->postVars('folder');
		$icon=$this->postVars('icon');
		$type=$this->postVars('type');
		$class=ucfirst($modFolder1);
		$modFolder="home.php?p=modules/".$modFolder1."/view/index.php";
		
		
		
		$sql="insert into `menu` (`m_nom`, `m_link`, `m_isModule`, `m_modelMod`, `m_folder`, `m_icon`) values ('$modName','$modFolder','$type','$class','$modFolder1', '$icon')";
		$this->Execute($sql);
	}
	
	public function newMenuWithFile($field_name, $path)
	{
	 	$ok=$this->upLoadFileProccess($field_name, $path);
		
		if($ok){
		 
		 $ok2 = $this->extractZip($this->fname, $path);
		 if($ok2)
		 {
		  
		  $folder = explode('.', $this->fname);
		  $xml = simplexml_load_file(realpath('./').$path.$folder[0]."/config.xml");
	
		  $modName = $xml->module->name;
		  $modFolder1 = $xml->module->folder;
		  $class = $xml->module->class;
		  $type = $xml->module->type;
		  $version = $xml->module->version;
		  $icon = $xml->module->icon;
		  $modFolder="home.php?p=modules/".$modFolder1."/view/index.php";
		  
		  $sql="insert into `menu` (`m_nom`, `m_link`, `m_isModule`, `m_modelMod`, `m_folder`, `m_icon`) values ('$modName','$modFolder','$type','$class','$modFolder1','$icon')";
		$this->Execute($sql);
		   
		   unlink(realpath('./').$path.$this->fname);
		  
		 }
		}
	}
	
	public function delMenu(){
	 	$id=$this->getVars('id');
		
		 $sql="select * from menu where m_id='$id'";
		 $row=$this->ExecuteS($sql);
		 
		 foreach($row as $row){
		  $class=$row["m_modelMod"];
		 }
		
		$sql="delete from menu where m_id='$id'";
		$this->Execute($sql);
		
		return $class;
		
	}
	
	public function getModules($type=1){
	
	 $sql="select * from menu where m_isModule='$type'";
	 return $this->ExecuteS($sql);
	 
	}
  
 };
?>
