<?php
class masiveup extends GeCore{
	
	var $folder = "masiveup";
	//Customer Methods
	public function loadCustomer($nom,$nit,$dir,$tel,$web,$user,$ti,$tc,$prospect)
	{
	    $sql="insert into `customer` (`c_nom`, `c_nit`, `c_dir`, `c_tel`, `c_web`, `u_id`, `ti_id`, `ccl_id`, `c_prospective`) values ('$nom','$nit','$dir','$tel','$web','$user','$ti','$tc','$prospect')";
	    $this->Execute($sql);
	    return $this->getCustomerLastID();
	}
	
	public function loadCustomerContact($nom,$cargo,$tel,$ini,$cel,$email,$id,$tp,$tc,$dir)
	{
	    $sql="insert into `customer_contact` (`cc_nom`, `cc_cargo`, `cc_telefono`, `cc_birth`, `cc_cel`, `cc_mail`, `c_id`, `cprof_id`, `tc_id`, `cc_dir`) values ('$nom','$cargo','$tel','$ini','$cel','$email','$id','$tp','$tc','$dir')";
		$this->Execute($sql);
	}
	
	public function loadCustomerProcess()
	{
	    $ok=$this->upLoadFileProccess('arch', 'modules/masiveup/files');
		
		if($ok){
		    $counter = 0;
		    $f = fopen('modules/masiveup/files/'.$this->fname,'r');
		    while(!feof($f))
		    {
		        if($counter != 0)
		        {
		            $line = fgets($f, 10000);
		            $datas = explode(';', $line);
		            $id_customer = $this->loadCustomer($datas[0],$datas[1],$datas[2],$datas[3],$datas[4],$datas[5],$datas[6],$datas[7],$datas[8]);
    		        $c = json_decode(utf8_encode($datas[9]),true);
    		        if($c != NULL)
    		        {
    		            for($i=0; $i<count($c['contactos']); $i++)
    		            {
    		                $this->loadCustomerContact($c['contactos'][$i]['nombre'],$c['contactos'][$i]['cargo'],$c['contactos'][$i]['telefono'],$c['contactos'][$i]['nacimiento'],$c['contactos'][$i]['celular'],$c['contactos'][$i]['email'],$id_customer,$c['contactos'][$i]['profesion'],$c['contactos'][$i]['tipocargo'],$c['contactos'][$i]['direccion']);
    		            }
    		        }
    		        $counter ++;
		        }
		        else
		        {
		            $counter ++;
		        }
		        
		    }
		 
		}
		return $counter;
	}
	
	//Products Methods
	public function loadProducts($name,$sdesc,$desc,$reference,$active,$price,$height,$weight,$depth,$width, $cat_id,$warranty,$minstock, $stock, $desc_mov)
	{
	        $inventory = new inventory();
	    	$inventory->connect();
	    	
	    	$sql="insert into `products` (`pro_name`, `pro_sdesc`, `pro_desc`, `pro_reference`, `pro_ean`, `pro_upc`, `pro_active`, `pro_price`, `pro_pricefob`, `pro_wholesaleprice`, `pro_height`, `pro_weight`, `pro_depth`, `pro_width`, `com_id`, `cli_id`, `cat_id`, `pro_warranty`) values ('$name','$sdesc','$desc','$reference','$ean','$upc','$active','$price','$pricefob','$pricewholesale','$height','$weight','$depth','$width','$com_id','$cli_id','$cat_id','$warranty')";
	    	
	    	$this->Execute($sql);
	    	$id_product = $this->getLastID();
	    	
	    	$id_pinv = $inventory->newCapsInvProd2($name, $sdesc, $price, $minstock);
	    	$inventory->editProduct($id_product,$id_pinv,1);
	    	
	    	$inventory->registerMov($id_productinv,$stock,1,'','in',$id_product,$desc_mov,'1','1','','no');
	    	
	    	return $id_product;
	}
	
	public function loadProductPrices($price, $id_currency, $id_product, $name, $id_tax)
	{   
	    $products = new products();
	    $products->connect();
	    
	    $row = $products->getTaxById($id_tax);
    	 foreach($row as $row)
    	 {
    	     $por = $row['prot_value'];
    	 }
	 
    	 $por /= 100;
    	 $tax = $price * $por;
    	 $final_price = $price + $tax;
	    
	    $sql="INSERT INTO `product_prices`(`pp_price`, `pc_id`, `pro_id`, `pp_name`, `prot_id`, `pp_base`, `pp_tax`) VALUES ('$final_price','$id_currency','$id_product','$name','$id_tax','$price','$tax')";
	    $this->Execute($sql);
	}
	
	public function loadProductProcess()
	{
	    $ok=$this->upLoadFileProccess('arch', 'modules/masiveup/files');
		
		if($ok){
		    $counter = 0;
		    $f = fopen('modules/masiveup/files/'.$this->fname,'r');
		    while(!feof($f))
		    {
		        if($counter != 0)
		        {
		            $line = fgets($f, 10000);
		            $datas = explode(';', $line);
		            $id_customer = $this->loadProducts($datas[0],$datas[1],$datas[2],$datas[3],$datas[4],$datas[5],$datas[6],$datas[7],$datas[8],$datas[9], $datas[10],$datas[11],$datas[12], $datas[13], $datas[14]);
    		        $c = json_decode(utf8_encode($datas[15]),true);
    		        if($c != NULL)
    		        {
    		            for($i=0; $i<count($c['precios']); $i++)
    		            {
    		                $this->loadProductPrices($c['precios'][$i]['precio'], $c['precios'][$i]['currency'], $c['precios'][$i]['producto'], $c['precios'][$i]['name'], $c['precios'][$i]['tax']);
    		            }
    		        }
    		        $counter ++;
		        }
		        else
		        {
		            $counter ++;
		        }
		        
		    }
		 
		}
		return $counter;
	}

	
	//Installers
   public function Checker(){
    
   }
   
   public function install(){
    	
   }
   
   public function uninstall(){
    	
   }
	
};
?>
