<?php
 include('modules/masiveup/view/include.php');
 
 
 //Acciones
 if($obj->getVars('actionDel')==true){
	 $obj->delMenu();
  
 }
 ?>
<div class="widget3">
 <div class="widgetlegend">Carga Masiva </div>
 <?php
  if($msg)
  {
  ?>
   <div class="ui-widget">
	<div class="ui-state-highlight ui-corner-all" style="margin-top: 20px; padding: 0 .7em;">
		<p><span class="ui-icon ui-icon-info" style="float: left; margin-right: .3em;"></span>
		<strong>Felicitaciones!</strong> eliminado satisfactoriamente.</p>
	</div>
</div>
  <?php
  }
 ?>
<p style="width:100">
	<!--<a href="<?php $_SERVER['PHP_SELF'];?>?p=modules/<?php echo $obj->folder?>/view/newMenu.php" class="btn_normal" style="float:left; margin:5px;">Nuevo </a>-->
</p>
 <br /><br /><br />
<table>
    <form action="" method="post" enctype="multipart/form-data">
	<tr>
		<td>Carga masiva de clientes:</td>
		<td><input type="file" name="arch" /></td>
		<td>
		    <input type="hidden" name="action" value="customer" />
		    <input type="submit" value="Cargar" class="btn_submit" /></td>
	</tr>
	<tr>
	    <td colspan="3">La carga masiva de cliente procesa la carga de clientes con sus contactos. Por favor usar el archivo con la estructura correcta y formato para que la carga se haga de forma satisfactoria.La carga masiva no tiene encuenta la primera linea del archivo. Usted puede descargar un ejemplo <a href="<?php echo $obj->customerweb?>/modules/masiveup/samples/sample_customer.csv" target="_blank">Aqui</a></td>
	</tr>
	</form>
	
	 <form action="" method="post" enctype="multipart/form-data">
	<tr>
		<td>Carga masiva de productos:</td>
		<td><input type="file" name="arch" /></td>
		<td>
		    <input type="hidden" name="action" value="products" />
		    <input type="submit" value="Cargar" class="btn_submit" /></td>
	</tr>
	<tr>
	    <td colspan="3">La carga masiva de productos carga los productos, con sus precios y carga de stock inicial. La carga masiva no tiene encuenta la primera linea del archivo. Usted puede descargar un ejemplo <a href="<?php echo $obj->customerweb?>/modules/masiveup/samples/sample_products.csv" target="_blank">Aqui</a></td>
	</tr>
	</form>
	
</table>
</div>


