CREATE TABLE `centercost` (
  `cc_id` int(11) NOT NULL AUTO_INCREMENT,
  `cc_nom` varchar(255) NOT NULL,
  `cc_track` varchar(255) NOT NULL,
  PRIMARY KEY (`cc_id`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1 AUTO_INCREMENT=1 ;

CREATE TABLE `convenio` (
  `co_id` int(11) NOT NULL AUTO_INCREMENT,
  `co_nom` varchar(255) NOT NULL,
  `co_por` varchar(255) NOT NULL,
  `co_descripcion` longtext NOT NULL,
  PRIMARY KEY (`co_id`)
) ENGINE=MyISAM  DEFAULT CHARSET=utf8 AUTO_INCREMENT=2 ;

CREATE TABLE `crmparam` (
  `cp_id` int(11) NOT NULL AUTO_INCREMENT,
  `cp_name` varchar(255) NOT NULL,
  `cp_value` varchar(255) NOT NULL,
  PRIMARY KEY (`cp_id`)
) ENGINE=MyISAM  DEFAULT CHARSET=utf8 AUTO_INCREMENT=10 ;

CREATE TABLE `customer` (
  `c_id` int(11) NOT NULL AUTO_INCREMENT,
  `c_nom` varchar(255) NOT NULL,
  `c_nit` varchar(255) NOT NULL,
  `c_dir` varchar(255) NOT NULL,
  `c_tel` varchar(255) NOT NULL,
  `c_web` varchar(255) NOT NULL,
  `u_id` int(11) NOT NULL,
  `ti_id` int(11) NOT NULL,
  `ccl_id` int(11) NOT NULL,
  PRIMARY KEY (`c_id`)
) ENGINE=MyISAM  DEFAULT CHARSET=utf8 AUTO_INCREMENT=145 ;

CREATE TABLE `customer_clafisification` (
  `ccl_id` int(11) NOT NULL AUTO_INCREMENT,
  `ccl_nom` varchar(255) NOT NULL,
  `ccl_puntos` varchar(255) NOT NULL,
  `ccl_color` varchar(255) NOT NULL,
  PRIMARY KEY (`ccl_id`)
) ENGINE=MyISAM  DEFAULT CHARSET=utf8 AUTO_INCREMENT=4 ;

CREATE TABLE `customer_contact` (
  `cc_id` int(11) NOT NULL AUTO_INCREMENT,
  `cc_nom` varchar(255) NOT NULL,
  `cc_cargo` varchar(255) NOT NULL,
  `cc_telefono` varchar(255) NOT NULL,
  `cc_birth` date NOT NULL,
  `cc_cel` varchar(255) NOT NULL,
  `cc_mail` varchar(255) NOT NULL,
  `c_id` int(11) NOT NULL,
  `cprof_id` int(11) NOT NULL,
  `tc_id` int(11) NOT NULL,
  `cc_user` varchar(255) NOT NULL,
  `cc_pass` varchar(255) NOT NULL,
  PRIMARY KEY (`cc_id`)
) ENGINE=MyISAM  DEFAULT CHARSET=utf8 AUTO_INCREMENT=230 ;

CREATE TABLE `customer_history` (
  `ch_id` int(11) NOT NULL AUTO_INCREMENT,
  `ch_date` date NOT NULL,
  `ch_desc` longtext NOT NULL,
  `ch_kpi` int(11) NOT NULL,
  `u_name` varchar(255) NOT NULL,
  `u_id` int(11) NOT NULL,
  `c_id` int(11) NOT NULL,
  `co_id` int(11) NOT NULL,
  `ch_apointment` date NOT NULL,
  `ch_duration` varchar(100) NOT NULL,
  PRIMARY KEY (`ch_id`)
) ENGINE=MyISAM  DEFAULT CHARSET=utf8 AUTO_INCREMENT=4 ;

CREATE TABLE `customer_industry` (
  `ci_id` int(11) NOT NULL AUTO_INCREMENT,
  `ci_nom` varchar(255) NOT NULL,
  `ci_desc` text NOT NULL,
  PRIMARY KEY (`ci_id`)
) ENGINE=MyISAM  DEFAULT CHARSET=utf8 AUTO_INCREMENT=2 ;

CREATE TABLE `customer_oportunities` (
  `co_id` int(11) NOT NULL AUTO_INCREMENT,
  `co_trackid` varchar(255) NOT NULL,
  `co_category` varchar(255) NOT NULL,
  `co_priority` varchar(255) NOT NULL,
  `co_subject` varchar(255) NOT NULL,
  `co_message` longtext NOT NULL,
  `co_date_add` datetime NOT NULL,
  `co_date_upd` datetime NOT NULL,
  `co_ip` varchar(255) NOT NULL,
  `co_status` varchar(255) NOT NULL,
  `co_lastreplier` varchar(255) NOT NULL,
  `co_archive` int(11) NOT NULL DEFAULT '0',
  `co_attachments` varchar(255) NOT NULL,
  `c_id` int(11) NOT NULL,
  `u_id` int(11) NOT NULL,
  `co_price` varchar(255) NOT NULL,
  PRIMARY KEY (`co_id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8 AUTO_INCREMENT=1 ;

CREATE TABLE `customer_oportunities_categories` (
  `coc_id` int(11) NOT NULL AUTO_INCREMENT,
  `coc_nom` varchar(255) NOT NULL,
  PRIMARY KEY (`coc_id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8 AUTO_INCREMENT=1 ;

CREATE TABLE `customer_oportunities_priority` (
  `cop_id` int(11) NOT NULL AUTO_INCREMENT,
  `cop_nom` varchar(255) NOT NULL,
  `cop_color` varchar(255) NOT NULL,
  PRIMARY KEY (`cop_id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8 AUTO_INCREMENT=1 ;

CREATE TABLE `customer_oportunities_status` (
  `cos_id` int(11) NOT NULL AUTO_INCREMENT,
  `cos_nom` varchar(255) NOT NULL,
  `cos_color` varchar(255) NOT NULL,
  PRIMARY KEY (`cos_id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8 AUTO_INCREMENT=1 ;

CREATE TABLE `customer_position` (
  `cp_id` int(11) NOT NULL AUTO_INCREMENT,
  `cp_nom` varchar(255) NOT NULL,
  PRIMARY KEY (`cp_id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8 AUTO_INCREMENT=1 ;

CREATE TABLE `customer_profesion` (
  `cprof_id` int(11) NOT NULL AUTO_INCREMENT,
  `cprof_nom` varchar(255) NOT NULL,
  PRIMARY KEY (`cprof_id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8 AUTO_INCREMENT=1 ;

CREATE TABLE `enterprise` (
  `e_id` int(11) NOT NULL AUTO_INCREMENT,
  `e_img` varchar(255) NOT NULL,
  `e_nom` varchar(255) NOT NULL,
  `e_info` longtext NOT NULL,
  PRIMARY KEY (`e_id`)
) ENGINE=MyISAM  DEFAULT CHARSET=utf8 AUTO_INCREMENT=2 ;

CREATE TABLE `kpi_goal` (
  `kg_id` int(11) NOT NULL AUTO_INCREMENT,
  `kg_y` varchar(255) NOT NULL,
  `kg_m` varchar(255) NOT NULL,
  `ki_id` int(11) NOT NULL,
  `kg_value` varchar(255) NOT NULL,
  PRIMARY KEY (`kg_id`)
) ENGINE=MyISAM  DEFAULT CHARSET=utf8 AUTO_INCREMENT=2 ;

CREATE TABLE `kpi_item` (
  `ki_id` int(11) NOT NULL AUTO_INCREMENT,
  `ki_nom` varchar(255) NOT NULL,
  `ki_por` varchar(255) NOT NULL,
  `ki_medicion` varchar(255) NOT NULL,
  `ki_del` varchar(255) NOT NULL,
  `ki_showin` varchar(100) NOT NULL,
  PRIMARY KEY (`ki_id`)
) ENGINE=MyISAM  DEFAULT CHARSET=utf8 AUTO_INCREMENT=6 ;

CREATE TABLE `orders` (
  `o_id` int(11) NOT NULL AUTO_INCREMENT,
  `o_fecha` datetime NOT NULL,
  `o_subto` varchar(255) NOT NULL,
  `o_iva` varchar(255) NOT NULL,
  `o_total` varchar(255) NOT NULL,
  `o_mod_pago` varchar(255) NOT NULL,
  `o_fecha_fin` date NOT NULL,
  `u_id` int(11) NOT NULL,
  `e_id` int(11) NOT NULL,
  `o_trm` varchar(255) NOT NULL,
  `cc_id` int(11) NOT NULL,
  `o_subto_base` varchar(255) NOT NULL,
  `o_track` varchar(255) NOT NULL,
  `o_obs` longtext NOT NULL,
  `o_porganancia` varchar(255) NOT NULL,
  `o_ganancia` varchar(255) NOT NULL,
  `o_tipo` varchar(255) NOT NULL,
  `o_desc` text NOT NULL,
  `o_active` varchar(100) NOT NULL,
  `c_id` int(11) NOT NULL,
  `o_quotes` varchar(100) NOT NULL,
  `o_aid` varchar(255) NOT NULL,
  `o_anom` varchar(255) NOT NULL,
  `o_cc` varchar(255) NOT NULL,
  PRIMARY KEY (`o_id`)
) ENGINE=MyISAM  DEFAULT CHARSET=utf8 AUTO_INCREMENT=407 ;

CREATE TABLE `orders_detail` (
  `od_id` int(11) NOT NULL AUTO_INCREMENT,
  `o_id` int(11) NOT NULL,
  `od_kpi` int(11) NOT NULL,
  `id_product` int(11) NOT NULL,
  `od_desc` longtext NOT NULL,
  `od_price` varchar(255) NOT NULL,
  `od_qty` varchar(255) NOT NULL,
  `od_total` varchar(255) NOT NULL,
  `id_currency` int(11) NOT NULL,
  `od_trm` varchar(255) NOT NULL,
  PRIMARY KEY (`od_id`)
) ENGINE=MyISAM  DEFAULT CHARSET=utf8 AUTO_INCREMENT=3214 ;

CREATE TABLE `orders_detail_temp` (
  `odt_id` int(11) NOT NULL AUTO_INCREMENT,
  `u_id` int(11) NOT NULL,
  `odt_kpi` int(11) NOT NULL,
  `id_product` int(11) NOT NULL,
  `odt_desc` longtext NOT NULL,
  `odt_price` varchar(255) NOT NULL,
  `odt_qty` varchar(255) NOT NULL,
  `odt_total` varchar(255) NOT NULL,
  `id_currency` int(11) NOT NULL,
  `odt_trm` varchar(255) NOT NULL,
  PRIMARY KEY (`odt_id`)
) ENGINE=MyISAM  DEFAULT CHARSET=utf8 AUTO_INCREMENT=3264 ;

CREATE TABLE `orders_history` (
  `oh_id` int(11) NOT NULL AUTO_INCREMENT,
  `oh_date` datetime NOT NULL,
  `oh_desc` longtext NOT NULL,
  `o_id` int(11) NOT NULL,
  `oh_name` varchar(255) NOT NULL,
  `oh_color` varchar(255) NOT NULL,
  PRIMARY KEY (`oh_id`)
) ENGINE=MyISAM  DEFAULT CHARSET=utf8 AUTO_INCREMENT=1662 ;

CREATE TABLE `orders_state` (
  `os_id` int(11) NOT NULL AUTO_INCREMENT,
  `os_name` varchar(255) NOT NULL,
  `os_color` varchar(255) NOT NULL,
  PRIMARY KEY (`os_id`)
) ENGINE=MyISAM  DEFAULT CHARSET=latin1 AUTO_INCREMENT=9 ;

CREATE TABLE `pricelist` (
  `pl_id` int(11) NOT NULL AUTO_INCREMENT,
  `pl_fabricante` varchar(255) NOT NULL,
  `pl_referencia` varchar(255) NOT NULL,
  `pl_descripcion` longtext NOT NULL,
  `pl_precio` varchar(255) NOT NULL,
  `pl_subcat` varchar(255) NOT NULL,
  `pl_cat` varchar(255) NOT NULL,
  `pl_disponibilidad` varchar(255) NOT NULL,
  `pl_desc_rapida` varchar(255) NOT NULL,
  `pl_dolar` varchar(100) NOT NULL,
  `pl_last_fecha` date NOT NULL,
  `u_nom` varchar(255) NOT NULL,
  `pl_noiva` varchar(100) NOT NULL,
  `pl_finder` longtext NOT NULL,
  PRIMARY KEY (`pl_id`)
) ENGINE=MyISAM  DEFAULT CHARSET=utf8 AUTO_INCREMENT=2 ;

CREATE TABLE `pricelistcategories` (
  `plc_id` int(11) NOT NULL AUTO_INCREMENT,
  `plc_nom` varchar(255) NOT NULL,
  `plc_porventa` varchar(255) NOT NULL,
  `plc_father` varchar(255) NOT NULL,
  PRIMARY KEY (`plc_id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8 AUTO_INCREMENT=1 ;

CREATE TABLE `quote` (
  `q_id` int(11) NOT NULL AUTO_INCREMENT,
  `q_fecha` datetime NOT NULL,
  `q_subto` varchar(255) NOT NULL,
  `q_iva` varchar(255) NOT NULL,
  `q_total` varchar(255) NOT NULL,
  `q_mod_pago` varchar(255) NOT NULL,
  `q_fecha_fin` date NOT NULL,
  `u_id` int(11) NOT NULL,
  `e_id` int(11) NOT NULL,
  `q_trm` varchar(255) NOT NULL,
  `cc_id` int(11) NOT NULL,
  `c_id` int(11) NOT NULL,
  `q_subto_base` varchar(255) NOT NULL,
  `q_track` varchar(255) NOT NULL,
  `q_obs` longtext NOT NULL,
  `q_porganancia` varchar(255) NOT NULL,
  `q_ganancia` varchar(255) NOT NULL,
  `q_tipo` varchar(255) NOT NULL,
  `q_desc` varchar(255) NOT NULL,
  `q_active` varchar(100) NOT NULL,
  `q_kpi` int(11) NOT NULL,
  PRIMARY KEY (`q_id`)
) ENGINE=MyISAM  DEFAULT CHARSET=utf8 AUTO_INCREMENT=472 ;

CREATE TABLE `quote_detail` (
  `qd_id` int(11) NOT NULL AUTO_INCREMENT,
  `q_id` int(11) NOT NULL,
  `id_product` int(11) NOT NULL,
  `qd_desc` longtext NOT NULL,
  `qd_price` varchar(255) NOT NULL,
  `qd_qty` varchar(255) NOT NULL,
  `qd_total` varchar(255) NOT NULL,
  `id_currency` int(11) NOT NULL DEFAULT '1',
  `qd_trm` varchar(255) NOT NULL,
  PRIMARY KEY (`qd_id`)
) ENGINE=MyISAM  DEFAULT CHARSET=utf8 AUTO_INCREMENT=4049 ;

CREATE TABLE `quote_detail_temp` (
  `qdt_id` int(11) NOT NULL AUTO_INCREMENT,
  `u_id` int(11) NOT NULL,
  `id_product` int(11) NOT NULL,
  `qdt_desc` longtext NOT NULL,
  `qdt_price` varchar(255) NOT NULL,
  `qdt_qty` varchar(255) NOT NULL,
  `qdt_total` varchar(255) NOT NULL,
  `id_currency` int(11) NOT NULL DEFAULT '1',
  `qdt_trm` varchar(255) NOT NULL,
  PRIMARY KEY (`qdt_id`)
) ENGINE=MyISAM  DEFAULT CHARSET=utf8 AUTO_INCREMENT=4638 ;

CREATE TABLE `terms_payment` (
  `tp_id` int(11) NOT NULL AUTO_INCREMENT,
  `tp_nom` varchar(255) NOT NULL,
  `tp_descripcion` varchar(255) NOT NULL,
  PRIMARY KEY (`tp_id`)
) ENGINE=MyISAM  DEFAULT CHARSET=utf8 AUTO_INCREMENT=4 ;

ALTER TABLE `user` ADD `crm_letAuth` INT( 100 ) NOT NULL ;
ALTER TABLE `user` ADD `isApprover` INT( 100 ) NOT NULL;
ALTER TABLE `user` ADD `isCommercial` INT( 100 ) NOT NULL;
ALTER TABLE `orders` ADD `o_currentstate` INT NOT NULL ;

INSERT INTO `crmparam` (`cp_id`, `cp_name`, `cp_value`) VALUES
(1, 'Tracking Cotizaciones', '1'),
(2, 'Tracking Ordenes de Pedido', '1'),
(3, 'Tracking Oportunidades', '1'),
(4, 'Tope de aprobación', '1'),
(5, 'Correo remitente', ' '),
(6, 'Correo seguimiento', ''),
(7, 'Mensaje en correo', ''),
(8, 'Track Anticipos', '1'),
(9, 'Track Anticipos', '1'); 

ALTER TABLE `orders` ADD `o_horaentrega` VARCHAR(255) NOT NULL AFTER `o_cc`;