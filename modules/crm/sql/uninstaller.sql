DROP TABLE `convenio`, `crmparam`, `customer`, `customer_clafisification`, `customer_contact`, `customer_history`, `customer_industry`, `customer_oportunities`, `customer_oportunities_categories`, `customer_oportunities_priority`, `customer_oportunities_status`, `customer_position`, `customer_profesion`, `enterprise`, `kpi_goal`, `kpi_item`, `orders`, `orders_detail`, `orders_detail_temp`, `orders_history`, `orders_state`, `pricelist`, `pricelistcategories`, `quote`, `quote_detail`, `quote_detail_temp`, `terms_payment`;

ALTER TABLE `user` DROP `crm_letAuth` INT( 100 ) NOT NULL ;
ALTER TABLE `user` DROP `isApprover` INT( 100 ) NOT NULL;
ALTER TABLE `orders` DROP `o_currentstate` INT NOT NULL ;
ALTER TABLE `user` DROP `isCommercial` INT( 100 ) NOT NULL;