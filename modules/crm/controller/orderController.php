<?php
 class orderControllerGe extends GeCore{
 	
	public function newOrderProcess(){
	 
	 $order = new orderModelGe();
	 $order->connect();
	 $track=$order->getParam(2);
	
	 $id=$order->newOrder($track);
	 if(!empty($id)){
	 	
		$row=$order->getOrderDetailTemp();
		if(count($row)>0){
			foreach($row as $row){
		
				
				$order->newOrderDetailItem($id,$row["id_product"],$row["odt_desc"],$row["odt_price"],$row["odt_qty"],$row["odt_kpi"],$row["odt_trm"],$row["id_currency"],$row["odt_total"]);
			}
		}
		
	 }
	 
	 $order->newOrderHistory2(14,$id);
	 $order->flushOrderDetailItemTemp();
	 $order->editParam(2);
	 
	 $sendemail=$order->postVars('sendemail');
	 
	 if($sendemail==1){
	  $this->sendEmailProcess($id);
	 }
	 
	}
	
	public function newOrderDetailTempProcess(){
		
	 //$finder=$this->postVars('finder');
	 $id_product=$this->postVars('id_product');
	 $cant=$this->postVars('cant');
	 
	 $finder=explode(':',$finder);
	 
	 $quote = new quoteModelGe();
	 $quote->connect();
	 
	 $products = new products();
	 $products->connect();
	 
	 //$row=$quote->getPriceById($finder[2]);
	 $row=$products->getProductsById($id_product);
	 if(count($row)>0){
	  foreach($row as $row){
	   	$desc=$row["pro_sdesc"];
		$price=$row["pro_price"];
	  }
	 }
	 
	 $por_convenio=$_SESSION["por_convenio"]/100;
	 $por_convenio2=$price*$por_convenio;
	 $price+=$por_convenio2;
	 
	 $order = new orderModelGe();
	 $order->connect();
	 $order->newOrderDetailItemTemp($id_product,$desc,$price,$cant,$kpi);
	 
	
	}
	
	public function newOrderDetailTempProcess2(){
		
	 //$finder=$this->postVars('finder');
	 $q_id=$this->postVars('q_id');
	 	 
	 //$finder=explode(':',$finder);
	 			
	 $quote = new quoteModelGe();
	 $quote->connect();
	 
	 $order = new orderModelGe();
	 $order->connect();
	 
	 $row=$quote->getQuoteDetailByTrack($q_id);

	 if(count($row)>0){
	  foreach($row as $row){
	   	 $order = new orderModelGe();
	 	 $order->connect();
	 	 //$res=$order->newOrderDetailItemTemp($row["id_product"],$row["qd_desc"],$row["qd_price"],$row["qd_qty"]);
		 $res=$order->newOrderDetailItemTemp($row["id_product"],$row["qd_desc"],$row["qd_price"],$row["qd_qty"],$kpi,$row["qd_trm"],$row["id_currency"],$row["qd_total"]);
	  }
	 }
	
	}
	
	public function newOrderDetailTempProcess3(){
		
	 //$finder=$this->postVars('finder');
	 $q_id=$this->postVars('q_id');
	 $id_order=$this->postVars('id_order');
	 	 
	 //$finder=explode(':',$finder);
	 			
	 $quote = new quoteModelGe();
	 $quote->connect();
	 
	 $order = new orderModelGe();
	 $order->connect();
	 
	 $row=$quote->getQuoteDetailByTrack($q_id);
	 if(count($row)>0){
	  foreach($row as $row){
	   	 $order = new orderModelGe();
	 	 $order->connect();
	 	 //$res=$order->newOrderDetailItemTemp($row["id_product"],$row["qd_desc"],$row["qd_price"],$row["qd_qty"]);
		 //$res=$order->newOrderDetailItemTemp($row["id_product"],$row["qd_desc"],$row["qd_price"],$row["qd_qty"],$kpi,$row["qd_trm"],$row["id_currency"],$row["qd_total"]);
		 $res=$order->newOrderDetailItem($id_order,$row["id_product"],$row["qd_desc"],$row["qd_price"],$row["qd_qty"],$kpi,$row["qd_trm"],$row["id_currency"],$row["qd_total"]);
	  }
	 }
	
	}
	
	public function printOrderProcess(){
		
		
	 $id=$this->getVars('id');
	 
	 $order = new orderModelGe();
	 $order->connect();
	 
	 $crm = new crmScs();
	 $crm->connect();
	 
	 $products = new products();
	 $products->connect();
	 
	 $row1=$order->getOrderById($id);
	 $row2=$order->getOrdersDetail($id);
	 
	 $f=fopen('../formats/orderformat2.html','r');
	 $html=fread($f,filesize('../formats/orderformat2.html'));
	 
	 if(count($row1)>0){
	  foreach($row1 as $row1){
	  	$html=str_replace('[track]',$row1["o_track"],$html);
		$html=str_replace('[fechacreacion]',$row1["o_fecha"],$html);
		$html=str_replace('[observaciones]',$row1["o_obs"],$html);
		$html=str_replace('[subtotal]',number_format($row1["o_subto"]),$html);
		$html=str_replace('[subtotalcop]',number_format($row1["o_subto"]),$html);
		$html=str_replace('[trm]',number_format($row1["o_trm"]),$html);
		$html=str_replace('[ivacop]',number_format($row1["o_iva"]),$html);
		$html=str_replace('[totalcop]',number_format($row1["o_total"]),$html);
		$html=str_replace('[valorletras]',$crm->num2letras($row1["o_subto"], false, false),$html);
		$html=str_replace('[fechaentrega]',$row['o_fecha_fin'],$html);
		$html=str_replace('[horaentrega]',$row['o_horaentrega'],$html);
		
		//Otros Datos
		$cc_id=$row1["cc_id"];
		$c_id=$row1["c_id"];
		$e_id=$row1["e_id"];
		$u_id=$row1["u_id"];
	  }
	 }
	 
	 $i=0;
	 
	 if(count($row2)>0){
	  foreach($row2 as $row2){
	   if($i%2==0){
	    $bg="#f5f5f5";
	   }else{
	    $bg="#fff";
	   }
	   $i++;
	   
	   $row3=$products->getCurrencyById($row2["id_currency"]);
	   if(count($row3)>0){
		 foreach($row3 as $row3){
		  $currency=$row3["pc_code"];
		 }
	   }
	   
	   $qdet.='<tr valign="top" class="txt_destacado" bgcolor="'.$bg.'">
          <td><div align="center">'.$row2['od_desc'].'</div></td>
		  <td><div align="center">$'.number_format($row2['od_price'],2).'</div></td>';
		$qdet.='
          <td><div align="center">'.$row2['od_qty'].'</div></td>
          <td><div align="center">$'.number_format($row2['od_total'],2).'</div></td>
        </tr>';
	  } 
	 }
	
	 $html=str_replace('[detalleorden]',$qdet,$html);
	 
	 
	 //Datos del cliente
	 $customer = new customerModelGe();
	 $customer->connect();
	 
	 $row1=$customer->getCustomerById2($c_id);
	 $row2=$customer->getCustomerContactById($cc_id);
	 
	 if(count($row1)>0){
	  foreach($row1 as $row1){
	  	$html=str_replace('[nombrecliente]',$row1["c_nom"],$html);
		$html=str_replace('[identificacion]',$row1["c_nit"],$html);
		$html=str_replace('[direccion]',$row1["c_dir"],$html);
		$html=str_replace('[telefono]',$row1["c_tel"],$html);
	  }
	 }
	 
	 if(count($row2)>0){
	  foreach($row2 as $row2){
	  	$html=str_replace('[nombrecontacto]',$row2["cc_nom"],$html);
	  }
	 }
	 
	 //Datos de la Compa�ia
	 $crmModelObj=new crmScs();
	 $crmModelObj->connect();
	 
	 $row=$crmModelObj->getCompanyById($e_id);
	 if(count($row)>0){
	  foreach($row as $row){
	  	$comInf.='<table width="163" border="0" align="center">
          <tr>
            <td><div align="center"><img src="../Logos/'.$row["e_img"].'" /><br />
            </div></td>
          </tr>
          <tr>
            <td><div align="center">'.$row["e_info"].'</div></td>
          </tr>
        </table>';
	  }
	 }
	 
	 $html=str_replace('[company]',$comInf,$html);
	 //Datos del usuario
	 if(!empty($u_id)){
		 $user = new User();
		 $user->connect();
		 $user->getUserById($u_id);
		 if(!empty($user->sign)){
		 $img="<img src='../../../usr/".$user->sign."' />";
		 $html=str_replace('[firmaejecutivo]',$img,$html);
		 }else{
		 $html=str_replace('[firmaejecutivo]',$user->name,$html);
		 }
		 
	 }
	 
	 
	 return $html; 
	 
	
		
	}
	
	public function showOrderProcess(){
		
	 $id=$this->getVars('id');
	 
	 $order = new orderModelGe();
	 $order->connect();
	 
	 $crm = new crmScs();
	 $crm->connect();
	 
	  $products = new products();
	 $products->connect();
	 
	 $row1=$order->getOrderById($id);
	 $row2=$order->getOrdersDetail($id);
	 
	 $f=fopen('../formats/orderformat.html','r');
	 $html=fread($f,filesize('../formats/orderformat.html'));
	 
	 if(count($row1)>0){
	  foreach($row1 as $row1){
	  	$html=str_replace('[track]',$row1["o_track"],$html);
		$html=str_replace('[fechacreacion]',$row1["o_fecha"],$html);
		$html=str_replace('[observaciones]',$row1["o_obs"],$html);
		$html=str_replace('[subtotal]',number_format($row1["o_subto"]),$html);
		$html=str_replace('[subtotalcop]',number_format($row1["o_subto"]),$html);
		$html=str_replace('[trm]',number_format($row1["o_trm"]),$html);
		$html=str_replace('[ivacop]',number_format($row1["o_iva"]),$html);
		$html=str_replace('[totalcop]',number_format($row1["o_total"]),$html);
		$html=str_replace('[valorletras]',$crm->num2letras($row1["o_subto"], false, false),$html);
		$html=str_replace('[fechaentrega]',$row['o_fecha_fin'],$html);
		$html=str_replace('[horaentrega]',$row['o_horaentrega'],$html);
		
		//Otros Datos
		$cc_id=$row1["cc_id"];
		$c_id=$row1["c_id"];
		$e_id=$row1["e_id"];
		$u_id=$row1["u_id"];
	  }
	 }
	 
	 $i=0;
	 
	 if(count($row2)>0){
	  foreach($row2 as $row2){
	   if($i%2==0){
	    $bg="#f5f5f5";
	   }else{
	    $bg="#fff";
	   }
	   $i++;
	   
	   $row3=$products->getCurrencyById($row2["id_currency"]);
	   if(count($row3)>0){
		 foreach($row3 as $row3){
		  $currency=$row3["pc_code"];
		 }
	   }
	   
	   $qdet.='<tr valign="top" class="txt_destacado" bgcolor="'.$bg.'">
          <td><div align="center">'.$row2['od_desc'].'</div></td>
		  <td><div align="center">$'.number_format($row2['od_price'],2).'</div></td>';
		$qdet.='
          <td><div align="center">'.$row2['od_qty'].'</div></td>
          <td><div align="center">$'.number_format($row2['od_total'],2).'</div></td>
        </tr>';
	  } 
	 }
	 
	 $html=str_replace('[detalleorden]',$qdet,$html);
	 
	 
	 //Datos del cliente
	 $customer = new customerModelGe();
	 $customer->connect();
	 
	 $row1=$customer->getCustomerById2($c_id);
	 $row2=$customer->getCustomerContactById($cc_id);
	 
	 if(count($row1)>0){
	  foreach($row1 as $row1){
	  	$html=str_replace('[nombrecliente]',$row1["c_nom"],$html);
		$html=str_replace('[identificacion]',$row1["c_nit"],$html);
		$html=str_replace('[direccion]',$row1["c_dir"],$html);
		$html=str_replace('[telefono]',$row1["c_tel"],$html);
	  }
	 }
	 
	 if(count($row2)>0){
	  foreach($row2 as $row2){
	  	$html=str_replace('[nombrecontacto]',$row2["cc_nom"],$html);
	  }
	 }
	 
	 //Datos de la Compa�ia
	 $crmModelObj=new crmScs();
	 $crmModelObj->connect();
	 
	 $row=$crmModelObj->getCompanyById($e_id);
	 if(count($row)>0){
	  foreach($row as $row){
	  	$comInf.='<table width="163" border="0" align="center">
          <tr>
            <td><div align="center"><img src="../Logos/'.$row["e_img"].'" /><br />
            </div></td>
          </tr>
          <tr>
            <td><div align="center">'.$row["e_info"].'</div></td>
          </tr>
        </table>';
	  }
	 }
	 
	 $html=str_replace('[company]',$comInf,$html);
	 
	 //Datos del usuario
	 if(!empty($u_id)){
		 $user = new User();
		 $user->connect();
		 $user->getUserById($u_id);
		 if(!empty($user->sign)){
		 $img="<img src='../../../usr/".$user->sign."' />";
		 $html=str_replace('[firmaejecutivo]',$img,$html);
		 }else{
		 $html=str_replace('[firmaejecutivo]',$user->name,$html);
		 }
		 
	 }
	 
	 
	 return $html; 	
	
	}
	
	public function sendEmailProcess($id){
	 
	 $order = new orderModelGe();
	 $order->connect();
	 
	 $crm = new crmScs();
	 $crm->connect();
	 
	 $products = new products();
	 $products->connect();
	 
	 $row1=$order->getOrderById($id);
	 $row2=$order->getOrdersDetail($id);
	 
	 $f=fopen('modules/crm/formats/orderformat3.html','r');
	 $html=fread($f,filesize('modules/crm/formats/orderformat3.html'));
	 
	 if(count($row1)>0){
	  foreach($row1 as $row1){
	  	$html=str_replace('[track]',$row1["o_track"],$html);
		$html=str_replace('[fechacreacion]',$row1["o_fecha"],$html);
		$html=str_replace('[observaciones]',$row1["o_obs"],$html);
		$html=str_replace('[subtotal]',number_format($row1["o_subto"]),$html);
		$html=str_replace('[subtotalcop]',number_format($row1["o_subto"]),$html);
		$html=str_replace('[trm]',number_format($row1["o_trm"]),$html);
		$html=str_replace('[ivacop]',number_format($row1["o_iva"]),$html);
		$html=str_replace('[totalcop]',number_format($row1["o_total"]),$html);
		$html=str_replace('[valorletras]',$crm->num2letras($row1["o_subto"], false, false),$html);
		$html=str_replace('[fechaentrega]',$row['o_fecha_fin'],$html);
		$html=str_replace('[horaentrega]',$row['o_horaentrega'],$html);
		
		//Otros Datos
		$cc_id=$row1["cc_id"];
		$c_id=$row1["c_id"];
		$e_id=$row1["e_id"];
		$u_id=$row1["u_id"];
	  }
	 }
	 
	 $i=0;
	 
	 if(count($row2)>0){
	  foreach($row2 as $row2){
	   if($i%2==0){
	    $bg="#f5f5f5";
	   }else{
	    $bg="#fff";
	   }
	   $i++;
	   
	   $row3=$products->getCurrencyById($row2["id_currency"]);
	   if(count($row3)>0){
		 foreach($row3 as $row3){
		  $currency=$row3["pc_code"];
		 }
	   }
	   
	   $qdet.='<tr valign="top" class="txt_destacado" bgcolor="'.$bg.'">
          <td><div align="center">'.$row2['od_desc'].'</div></td>
		  <td><div align="center">$'.number_format($row2['od_price'],2).'</div></td>';
		$qdet.='
          <td><div align="center">'.$row2['od_qty'].'</div></td>
          <td><div align="center">$'.number_format($row2['od_total'],2).'</div></td>
        </tr>';
	  } 
	 }
	
	 $html=str_replace('[detalleorden]',$qdet,$html);
	 
	 
	 //Datos del cliente
	 $customer = new customerModelGe();
	 $customer->connect();
	 
	 $row1=$customer->getCustomerById2($c_id);
	 $row2=$customer->getCustomerContactById($cc_id);
	 
	 if(count($row1)>0){
	  foreach($row1 as $row1){
	  	$html=str_replace('[nombrecliente]',$row1["c_nom"],$html);
		$html=str_replace('[identificacion]',$row1["c_nit"],$html);
		$html=str_replace('[direccion]',$row1["c_dir"],$html);
		$html=str_replace('[telefono]',$row1["c_tel"],$html);
	  }
	 }
	 
	 if(count($row2)>0){
	  foreach($row2 as $row2){
	  	$html=str_replace('[nombrecontacto]',$row2["cc_nom"],$html);
	  }
	 }
	 
	 //Datos de la Compa�ia
	 $crmModelObj=new crmScs();
	 $crmModelObj->connect();
	 
	 $row=$crmModelObj->getCompanyById($e_id);
	 if(count($row)>0){
	  foreach($row as $row){
	  	$comInf.='<table width="163" border="0" align="center">
          <tr>
            <td><div align="center"><img src="'.$this->customerweb.__FOLDER__.'modules/crm/Logos/'.$row["e_img"].'" /><br />
            </div></td>
          </tr>
          <tr>
            <td><div align="center">'.$row["e_info"].'</div></td>
          </tr>
        </table>';
	  }
	 }
	 
	 $html=str_replace('[company]',$comInf,$html);
	 //Datos del usuario
	 if(!empty($u_id)){
		 $user = new User();
		 $user->connect();
		 $user->getUserById($u_id);
		 if(!empty($user->sign)){
		 $img="<img src='../../../usr/".$user->sign."' />";
		 $html=str_replace('[firmaejecutivo]',$img,$html);
		 }else{
		 $html=str_replace('[firmaejecutivo]',$user->name,$html);
		 }
		 
	 }
	 
	 
	//Remitente
	 $from=$crm->getCrmParamById(5);
	 
	 
	 //Correo de seguimiento
	 $bcc=$crm->getCrmParamById(6);
	 
	 
	 //mensaje
	 $msg=$crm->getCrmParamById(7);
	 
	 
	 ini_set('SMTP','mail.goely.co');
  	 $headers="from:".$from."\r\n";
	 
	 //Enviar con copia
	 $contactmail=$crm->postVars('contactmail');
	 for($i=0;$i<=count($contactmail);$i++){
	  $headers.="Cc:".$contactmail[$i]."\r\n";
	 }
	 
	 $headers.="Bcc:".$bcc."\r\n";
  	 $headers.= "MIME-Version: 1.0\r\n"; 
  	 $headers.= "Content-type: text/html; charset=UTF-8\r\n";

	 
	 mail($c_email,"PEDIDO ".$this->customername,$html,$headers);
	 
	
		
	
	}
	
	public function _init($action){
		
		//$action=$this->getVars('actions');
		
		switch($action){
			
			case 'NewOrder': $res=$this->newOrderProcess();
			break;
			
			case 'EditOrder': 
				$order = new orderModelGe();
				$res=$order->editOrder();
			break;
			
			case 'DelOrder': 
				$order = new orderModelGe();
				$order->delOrder();
			break;
			
			default:
			    $act=$this->getVars('act'); 
				$order = new orderModelGe();
				$res=$order->getOrders($act);
			break;
			
			case 'GetOrderById':
				$id=$this->getVars('id'); 
				$order = new orderModelGe();
				$res=$order->getOrderById($id);
			break;
			
			case 'GetOrderByCustomer':
				$id=$this->getVars('id'); 
				$order = new orderModelGe();
				$order->connect();
				$res=$order->getOrderByCustomer($id);
			break;
			
			case 'PrintOrder': 
				$res=$this->printOrderProcess();
			break;
			
			case 'ShowOrder':
				$res=$this->showOrderProcess();
			break;
			
			case 'GetAllOrders':
				$order = new orderModelGe();
				$order->connect();
				$order->startPage($order->postVars('page'));
				$res=$order->getOrders(1,$order->start);
			break;
			
			case 'GetUserOrders':
				$order = new orderModelGe();
				$order->connect();
				$order->getSession();
				$order->startPage($order->postVars('page'));
				$res=$order->getOrdersByUser(1,$order->start,$_SESSION["user_id"]);
			break;
			
			case 'GetUserOrders2':
				$order = new orderModelGe();
				$order->connect();
				$order->getSession();
				$order->startPage($order->postVars('page'));
				$res=$order->getOrdersByUser(1,$order->start,$order->postVars('u_id'));
			break;
			
			case 'GetOrderByFind':
				$term=$this->getVars('term'); 
				$order = new orderModelGe();
				$order->connect();
				$res=$order->getOrderByFind($term);
			break;
			
			case 'GetUserOrderByFind':
				$term=$this->getVars('term'); 
				$order = new orderModelGe();
				$order->connect();
				$order->getSession();
				$res=$order->getUserOrderByFind($term,$id_user);
			break;
			
			case 'GetOrderBySearch':
				$finder=$this->postVars('finder');
				$f=explode(':',$finder); 
				$order = new orderModelGe();
				$order->connect();
				$res=$order->getOrderByTrack($f[0]);
			break;
			
			case 'GetOrderByCenterCost':
				$centercost=$this->postVars('centercost'); 
				$order = new orderModelGe();
				$order->connect();
				$res=$order->getOrderByCenterCost($cenetercost);
			break;
			
			//Order Details
			case 'NewOrderDetailTemp':
					$this->newOrderDetailTempProcess();
			break;
			
			case 'NewOrderDetailTemp2':
					$this->newOrderDetailTempProcess2();
			break;
			
			case 'NewOrderDetailTemp3':
					$this->newOrderDetailTempProcess3();
			break;
		
			
			case 'EditOrderDetailTemp':
				
				$id=$this->postVars('id');
				$fab=$this->postVars('fab');
				$ref=$this->postVars('ref');
				$desc=$this->postVars('desc');
				$pr=$this->postVars('pr');
				$type=$this->postVars('type');
				$cant=$this->postVars('cant');
				$disp=$this->postVars('disp');
				$total=$this->postVars('total');
				$noiva=$this->postVars('noiva');
				$por_margen=$this->postVars('por_margen');
				$pbase=$this->postVars('pbase');
				$te=$this->postVars('te');
				$gan1=$this->postVars('gan1');
				
				$order = new orderModelGe();
				$res=$order->editOrderDetailItemTemp($id,$fab,$ref,$desc,$pr,$type,$cant,$disp,$total,$noiva,$por_margen,$pbase,$te,$gan1);
			break;
			
			case 'DelOrderDetailTemp':
				$id_item=$this->getVars('id_item');
				$order = new orderModelGe();
				$order->connect();
				$res=$order->delOrderDetailItemTemp($id_item);
			break;
			
			case 'GetOrderDetailTemp':
				$id=$this->getVars('id');
				$order = new orderModelGe();
				$order->connect();
				$res=$order->getOrderDetailTemp();
			break;
			
			case 'NewOrderDetail':
				
				$id=$this->postVars('id');
				$fab=$this->postVars('fab');
				$ref=$this->postVars('ref');
				$desc=$this->postVars('desc');
				$pr=$this->postVars('pr');
				$type=$this->postVars('type');
				$cant=$this->postVars('cant');
				$disp=$this->postVars('disp');
				$total=$this->postVars('total');
				$noiva=$this->postVars('noiva');
				$por_margen=$this->postVars('por_margen');
				$pbase=$this->postVars('pbase');
				$te=$this->postVars('te');
				$gan1=$this->postVars('gan1');
				
				$order = new orderModelGe();
				$res=$order->newOrderDetailItem($id,$fab,$ref,$desc,$pr,$type,$cant,$disp,$total,$noiva,$por_margen,$pbase,$te,$gan1);
			break;
			
			case 'EditOrderDetail':
				
				$id=$this->postVars('id');
				$fab=$this->postVars('fab');
				$ref=$this->postVars('ref');
				$desc=$this->postVars('desc');
				$pr=$this->postVars('pr');
				$type=$this->postVars('type');
				$cant=$this->postVars('cant');
				$disp=$this->postVars('disp');
				$total=$this->postVars('total');
				$noiva=$this->postVars('noiva');
				$por_margen=$this->postVars('por_margen');
				$pbase=$this->postVars('pbase');
				$te=$this->postVars('te');
				$gan1=$this->postVars('gan1');
				
				$order = new orderModelGe();
				$res=$order->editOrderDetailItem($id,$fab,$ref,$desc,$pr,$type,$cant,$disp,$total,$noiva,$por_margen,$pbase,$te,$gan1);
			break;
			
			case 'DelOrderDetail':
				$id=$this->getVars('id');
				$order = new orderModelGe();
				$res=$order->delOrderDetailItem($id);
			break;
			
			//Pricelist
			case 'NewPrice':
				
				$fab=$this->postVars('fab');
				$ref=$this->postVars('ref');
				$desc=$this->postVars('desc');
				$precio=$this->postVars('precio');
				$subcat=$this->postVars('subcat');
				$cat=$this->postVars('cat');
				$disp=$this->postVars('disp');
				$fab=$this->postVars('fab');
				$descr=$this->postVars('descr');
				$moneda=$this->postVars('moneda');
				$exiva=$this->postVars('exiva');
				
				$quote = new quoteModelScs();
				$quote->newPrice($fab,$ref,$desc,$precio,$subcat,$cat,$disp,$descr,$moneda,$exiva);
			break;
			
			case 'EditPrice':
				
				$id=$this->postVars('id');
				$fab=$this->postVars('fab');
				$ref=$this->postVars('ref');
				$desc=$this->postVars('desc');
				$precio=$this->postVars('precio');
				$subcat=$this->postVars('subcat');
				$cat=$this->postVars('cat');
				$disp=$this->postVars('disp');
				$fab=$this->postVars('fab');
				$descr=$this->postVars('descr');
				$moneda=$this->postVars('moneda');
				$exiva=$this->postVars('exiva');
				
				$quote = new quoteModelScs();
				$quote->editPrice($id,$fab,$ref,$desc,$precio,$subcat,$cat,$disp,$descr,$moneda,$exiva);
			break;
			
			case 'DelPrice':
				
				$id=$this->getVars('id');
				$quote = new quoteModelScs();
				$quote->delPrice($id);
			break;
			
			case 'GetPrice':
				$quote = new quoteModelScs();
				$res=$quote->getPriceList();
			break;
			
			case 'GetPriceById':
				$id=$this->getVars('id');
				$quote = new quoteModelScs();
				$res=$quote-> getPriceById($id);
			break;
			
			case 'LoadPrice':
				$this->uploadPriceProcess();
			break;
			
			//categories
			case 'NewCategory':
				$this->newCategoryProcess();
			break;
			case 'editCategory':
				$this->editCategoryProcess();
			break;
			
			case 'delCategory':
				$id=$this->getVars('id');
				$quote = new quoteModelScs();
				$res=$quote->delCat($id);
			break;
			
			case 'getCategory':
				$quote = new quoteModelScs();
				$res=$quote->getCat();
			break;
			
			case 'getSubCategory':
				$id=$this->getVars('id');	
				$quote = new quoteModelScs();
				$res=$quote->getSubCat($id);
			break;
			
			case 'getCategoryById':
				$id=$this->getVars('id');	
				$quote = new quoteModelScs();
				$res=$quote->getCatById($id);
			break;
			
		
		}
		
		return $res;
	}
	
	
 }
?>