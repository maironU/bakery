<?php
 class customerControllerGe extends GeCore{
 	
	public function newCustomerProcess(){
	 
	 $customer = new customerModelGe();
	 $customer->connect();
	 $id=$customer->newCustomer();
	 
	
	 
	 if(!empty($id) || $id==true){
	 	$idc=$customer->newCustomerContact($id);
		if(!empty($idc)){
			return "Yes"; //Se crea el cliente y el contacto
		}else{
		 return "No Contact"; // No se crea el contacto
		}
	 }else{
	 	return $customer->_errors;
	 }
	}
	
	public function editCustomerProcess(){
	 
	 $customer = new customerModelGe();
	 $customer->connect();
	 $res=$customer->editCustomer();
	 
	 if($res!=false){
	  return "Yes";
	  }else{
	   return $customer->_errors;
	  }
	  
	}
	
	public function delCustomerProcess(){
	 
	 $id=$this->getVars('id');
	 $customer = new customerModelGe();
	 $customer->connect();
	 $customer->delCustomer($id);
	 $customer->delCustomerContactByCustomer($id);
	 
	 return "Yes";
	}
	
	public function _init($action=NULL){
		
		//$action=$this->getVars('actions');
		
		switch($action){
			
			case 'NewCustomer': $res=$this->newCustomerProcess();
			break;
			
			case 'EditCustomer': $res=$this->editCustomerProcess();
			break;
			
			case 'delCustomer': $res=$this->delCustomerProcess();
			break;
			
			case 'getCustomerById': 
				$customer = new customerModelGe();
				$customer->connect();
				$res=$customer->getCustomerById();
			break;
			
			default: 
				$customer = new customerModelGe();
				$customer->connect();
				$customer->startPage($customer->postVars('page'));
				$res=$customer->getCustomers($customer->start);
			break;
			
			case 'GetCustomerByFind':
				$term=$this->postVars('finder');
				$customer = new customerModelGe();
				$customer->connect();
				$res=$customer->getCustomerByFind2($term);
			break;
			
			case 'GetUserCustomerByFind':
				$term=$this->getVars('term');
				$customer = new customerModelGe();
				$customer->connect();
				$customer->getSession();
				$res=$customer->getUserCustomerByFind($term,$_SESSION['user_id']);
			break;
			
			case 'FindCustomer': 
				$customer = new customerModelGe();
				$customer->connect();
				$res=$customer->findCustomerById();
			break;
			
			case 'GetUserCustomer': 
				$customer = new customerModelGe();
				$customer->connect();
				$customer->getSession();
				$customer->startPage($customer->postVars('page'));
				$res=$customer->getUserCustomer($_SESSION['user_id'],$customer->start);
			break;
			
			//Contacts
			case 'NewContact':
			    $id=$this->postVars('id');
				$customer = new customerModelGe();
				$customer->connect();
				$res=$customer->newCustomerContact($id);
			break;
			
			case 'EditContact':
				$customer = new customerModelGe();
				$customer->connect();
				$res=$customer->editCustomerContact();
			break;
			
			case 'DelContact':
				$customer = new customerModelGe();
				$customer->connect();
				$res=$customer->delCustomerContact();
			break;
			
			case 'GetContact':
			    $id=$this->getVars('id');
				$customer = new customerModelGe();
				$customer->connect();
				$res=$customer->getCustomerContact($id);
			break;
			
			case 'GetContactById':
			    $id=$this->getVars('id');
				$customer = new customerModelGe();
				$customer->connect();
				$res=$customer->getCustomerContactById($id);
			break;
			
			//Setup
			case 'NewIndustry':	
				$customer = new customerModelGe();
				$customer->connect();
				$res=$customer->newCustomerIndustry();
			break;
			
			case 'EditIndustry':	
				$customer = new customerModelGe();
				$customer->connect();
				$res=$customer->editCustomerIndustry();
			break;
			
			case 'DelIndustry':	
				$customer = new customerModelGe();
				$customer->connect();
				$res=$customer->delCustomerIndustry();
			break;
			
			case 'getIndustry':	
				$customer = new customerModelGe();
				$customer->connect();
				$res=$customer->getCustomerIndustry();
			break;
			
			case 'getIndustryById':
				$id=$this->getVars('id');	
				$customer = new customerModelGe();
				$customer->connect();
				$res=$customer->getCustomerIndustryById($id);
			break;
			
			case 'NewPosition':	
				$customer = new customerModelGe();
				$customer->connect();
				$res=$customer->newCustomerPositions();
			break;
			
			case 'EditPosition':	
				$customer = new customerModelGe();
				$customer->connect();
				$res=$customer->editCustomerPosition();
			break;
			
			case 'DelPosition':	
				$customer = new customerModelGe();
				$customer->connect();
				$res=$customer->delCustomerPosition();
			break;
			
			case 'getPosition':	
				$customer = new customerModelGe();
				$customer->connect();
				$res=$customer->getCustomerPosition();
			break;
			
			case 'getPositionById':
				$id=$this->getVars('id');	
				$customer = new customerModelGe();
				$customer->connect();
				$res=$customer->getCustomerPositionById($id);
			break;
			
			case 'NewClasification':
				$customer = new customerModelGe();
				$customer->connect();
				$res=$customer->newCustomerClasification();
			break;
			
			case 'EditClasification':
				$customer = new customerModelGe();
				$customer->connect();
				$res=$customer->editCustomerClasification();
			break;
			
			case 'DelClasification':
				$customer = new customerModelGe();
				$customer->connect();
				$res=$customer->delCustomerClasification();
			break;
			
			case 'GetClasification':
				$customer = new customerModelGe();
				$customer->connect();
				$res=$customer->getCustomerClasification();
			break;
			
			case 'NewProfession':
				$customer = new customerModelGe();
				$customer->connect();
				$res=$customer->newCustomerProfession();
			break;
			
			case 'EditProfession':
				$customer = new customerModelGe();
				$customer->connect();
				$res=$customer->editCustomerProfession();
			break;
			
			case 'DelProfession':
				$customer = new customerModelGe();
				$customer->connect();
				$res=$customer->delCustomerProfession();
			break;
			
			case 'GetProfession':
				$customer = new customerModelGe();
				$customer->connect();
				$res=$customer->getCustomerProfession();
			break;
			
			case 'GetClasificationById':
				$id=$this->getVars('id');
				$customer = new customerModelGe();
				$customer->connect();
				$res=$customer->getCustomerProfessionById($id);
			break;
			
		
		}
		
		return $res;
	}
	
	
 }
?>