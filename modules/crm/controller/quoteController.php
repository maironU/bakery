<?php
 class quoteControllerGe extends GeCore{
 	
	public function newQuoteProcess(){
	 
	 $quote = new quoteModelGe();
	 $quote->connect();
	 $track=$quote->getParam(1);
	 
	 
	 
	 $id=$quote->newQuote($track);
	 if(!empty($id)){
	 	
		$row=$quote->getQuoteDetailTemp();
		if(count($row)>0){
			foreach($row as $row){
				$quote->newQuoteDetailItem($id,$row["id_product"],$row["qdt_desc"],$row["qdt_price"],$row["qdt_qty"],$row["qdt_trm"],$row["id_currency"],$row["qdt_total"]);
			}
		}
		
	 }
	 
	 $quote->flushQuoteDetailItemTemp();
	 $quote->editParam(1);
	 
	 $sendemail=$quote->postVars('sendemail');
	 
	 if($sendemail==1){
	  $this->sendEmailProcess($id);
	 }
	 
	}
	
	public function loadPriceProcess($flush=NULL){
	    $quote = new quoteModelGe();
		$quote->connect();
		
		if($flush==1){
			$quote->flushPrice();
		}else{	
			$quote->flushPrice();
			$quote->uploadPriceList();
		}
	}
	
	public function newCategoryProcess(){
		$nom=$this->postVars('nom');
		$por=$this->postVars('por');
		$father=$this->postVars('father');
		
		$quote = new quoteModelGe();
		$quote->connect();
		if($father==0){
			$res=$quote->newCat($nom,$por);
		}else{
			$res=$quote->newCat($nom,$por,$father);
		}
		
	}
	
	public function editCategoryProcess(){
		
		$id=$this->postVars('id');
		$nom=$this->postVars('nom');
		$por=$this->postVars('por');
		$father=$this->postVars('father');
		
		$quote = new quoteModelGe();
		$quote->connect();
		if($father==0){
			$res=$quote->editCat($id,$nom,$por);
		}else{
			$res=$quote->editCat($id,$nom,$por,$father);
		}
		
	}
	
	public function printQuoteProcess(){
	 $id=$this->getVars('id');
	 
	 $quote = new quoteModelGe();
	 $quote->connect();
	 
	 $crm = new crmScs();
	 $crm->connect();
	 
	 $products = new products();
	 $products->connect();
	 
	 $row1=$quote->getQuoteById($id);
	 $row2=$quote->getQuoteDetail($id);
	 
	 $f=fopen('../formats/quoteformat.html','r');
	 $html=fread($f,filesize('../formats/quoteformat.html'));
	 
	 if(count($row1)>0){
	  foreach($row1 as $row1){
	  	$html=str_replace('[track]',$row1["q_track"],$html);
		$html=str_replace('[fechacotizacion]',$row1["q_fecha"],$html);
		$html=str_replace('[fechavencimiento]',$row1["q_fecha_fin"],$html);
		$html=str_replace('[observaciones]',$row1["q_obs"],$html);
		$html=str_replace('[subtotal]',number_format($row1["q_subto"]),$html);
		$html=str_replace('[subtotalcop]',number_format($row1["q_subto"]),$html);
		if(empty($row1["q_trm"])){
		 $row1["q_trm"]=1;
		}
		$html=str_replace('[trm]',number_format($row1["q_trm"]),$html);
		$html=str_replace('[ivacop]',number_format($row1["q_iva"]),$html);
		$html=str_replace('[totalcop]',number_format($row1["q_total"]),$html);
		$html=str_replace('[valorletras]',$crm->num2letras($row1["q_subto"], false, false),$html);
		
		//Otros Datos
		$cc_id=$row1["cc_id"];
		$c_id=$row1["c_id"];
		$e_id=$row1["e_id"];
		$u_id=$row1["u_id"];
	  }
	 }
	 
	 $i=0;
	 
	 if(count($row2)>0){
	  foreach($row2 as $row2){
	   if($i%2==0){
	    $bg="#f5f5f5";
	   }else{
	    $bg="#fff";
	   }
	   $i++;
	   
	   $row3=$products->getCurrencyById($row2["id_currency"]);
	   if(count($row3)>0){
		 foreach($row3 as $row3){
		  $currency=$row3["pc_code"];
		 }
	   }
	   
	   $qdet.='<tr valign="top" class="txt_destacado" bgcolor="'.$bg.'">
          <td width="100"><div align="justify">'.$row2['qd_desc'].'</div></td>
		  <td width="100"><div align="justify">'.$currency.'</div></td>
		  <td><div align="center">$'.number_format($row2['qd_trm']).'</div></td>';
		  
		  if($row2["qd_trm"]==1 || empty($row2["qd_trm"])){
		  	$qdet.='<td><div align="center">&nbsp;</div></td>
			<td><div align="center">$'.number_format($row2['qd_price'],2).'</div></td>';
		  }else{
		  	$qdet.='<td><div align="center">$'.number_format($row2['qd_price'],2).'</div></td>
			<td><div align="center">&nbsp;</div></td>';
		  }
		 
          $qdet.='<td><div align="center">'.$row2['qd_qty'].'</div></td>
          <td><div align="center">$'.number_format($row2['qd_total']).'</div></td>
        </tr>';
	  } 
	 }
	 
	 $html=str_replace('[detallecotizacion]',$qdet,$html);
	 
	 
	 //Datos del cliente
	 $customer = new customerModelGe();
	 $customer->connect();
	 
	 $row1=$customer->getCustomerById2($c_id);
	 $row2=$customer->getCustomerContactById($cc_id);
	 
	 if(count($row1)>0){
	  foreach($row1 as $row1){
	  	$html=str_replace('[nombrecliente]',$row1["c_nom"],$html);
		$html=str_replace('[identificacion]',$row1["c_nit"],$html);
		$html=str_replace('[direccion]',$row1["c_dir"],$html);
		$html=str_replace('[telefono]',$row1["c_tel"],$html);
	  }
	 }
	 
	 if(count($row2)>0){
	  foreach($row2 as $row2){
	  	$html=str_replace('[nombrecontacto]',$row2["cc_nom"],$html);
	  }
	 }
	 
	 //Datos de la Compa�ia
	 $crmModelObj=new crmScs();
	 $crmModelObj->connect();
	 
	 $row=$crmModelObj->getCompanyById($e_id);
	 if(count($row)>0){
	  foreach($row as $row){
	  	$comInf.='<table width="163" border="0" align="center">
          <tr>
            <td><div align="center"><img src="../Logos/'.$row["e_img"].'" /><br />
            </div></td>
          </tr>
          <tr>
            <td><div align="center">'.$row["e_info"].'</div></td>
          </tr>
        </table>';
	  }
	 }
	 
	 $html=str_replace('[company]',$comInf,$html);
	 
	 //Datos del usuario
	 if(!empty($u_id)){
		 $user = new User();
		 $user->connect();
		 $user->getUserById($u_id);
		 if(!empty($user->sign)){
		 $img="<img src='../../../usr/".$user->sign."' />";
		 $html=str_replace('[firmaejecutivo]',$img,$html);
		 }else{
		 $html=str_replace('[firmaejecutivo]',$user->name,$html);
		 }
		 
	 }
	 
	 return $html; 
	 
	}
	
	public function showQuoteProcess(){
	 $id=$this->getVars('id');
	 
	 $quote = new quoteModelGe();
	 $quote->connect();
	 
	 $products = new products();
	 $products->connect();
	 
	 $row1=$quote->getQuoteById($id);
	 $row2=$quote->getQuoteDetail($id);
	 
	 $crm = new crmScs();
	 $crm->connect();
	 
	 $f=fopen('../formats/quoteformat2.html','r');
	 $html=fread($f,filesize('../formats/quoteformat2.html'));
	 
	 if(count($row1)>0){
	  foreach($row1 as $row1){
	  	$html=str_replace('[track]',$row1["q_track"],$html);
		$html=str_replace('[fechacotizacion]',$row1["q_fecha"],$html);
		$html=str_replace('[fechavencimiento]',$row1["q_fecha_fin"],$html);
		$html=str_replace('[observaciones]',$row1["q_obs"],$html);
		$html=str_replace('[subtotal]',number_format($row1["q_subto"]),$html);
		$html=str_replace('[subtotalcop]',number_format($row1["q_subto"]),$html);
		if(empty($row1["q_trm"])){
		 $row1["q_trm"]=1;
		}
		$html=str_replace('[trm]',number_format($row1["q_trm"]),$html);
		$html=str_replace('[ivacop]',number_format($row1["q_iva"]),$html);
		$html=str_replace('[totalcop]',number_format($row1["q_total"]),$html);
		$html=str_replace('[valorletras]',$crm->num2letras($row1["q_subto"], false, false),$html);
		
		//Otros Datos
		$cc_id=$row1["cc_id"];
		$c_id=$row1["c_id"];
		$e_id=$row1["e_id"];
		$u_id=$row1["u_id"];
	  }
	 }
	 
	 $i=0;
	 
	 if(count($row2)>0){
	  foreach($row2 as $row2){
	   if($i%2==0){
	    $bg="#f5f5f5";
	   }else{
	    $bg="#fff";
	   }
	   $i++;
	   
	   $row3=$products->getCurrencyById($row2["id_currency"]);
	   if(count($row3)>0){
		 foreach($row3 as $row3){
		  $currency=$row3["pc_code"];
		 }
	   }
	   
	   $qdet.='<tr valign="top" class="txt_destacado" bgcolor="'.$bg.'">
          <td width="100"><div align="justify">'.$row2['qd_desc'].'</div></td>
		  <td width="100"><div align="justify">'.$currency.'</div></td>
		  <td><div align="center">$'.number_format($row2['qd_trm']).'</div></td>';
		  
		  if($row2["qd_trm"]==1 || empty($row2["qd_trm"])){
		  	$qdet.='<td><div align="center">&nbsp;</div></td>
			<td><div align="center">$'.number_format($row2['qd_price'],2).'</div></td>';
		  }else{
		  	$qdet.='<td><div align="center">$'.number_format($row2['qd_price'],2).'</div></td>
			<td><div align="center">&nbsp;</div></td>';
		  }
		 
          $qdet.='<td><div align="center">'.$row2['qd_qty'].'</div></td>
          <td><div align="center">$'.number_format($row2['qd_total']).'</div></td>
        </tr>';
	  } 
	 }
	 
	 $html=str_replace('[detallecotizacion]',$qdet,$html);
	 
	 
	 //Datos del cliente
	 $customer = new customerModelGe();
	 $customer->connect();
	 
	 $row1=$customer->getCustomerById2($c_id);
	 $row2=$customer->getCustomerContactById($cc_id);
	 
	 if(count($row1)>0){
	  foreach($row1 as $row1){
	  	$html=str_replace('[nombrecliente]',$row1["c_nom"],$html);
		$html=str_replace('[identificacion]',$row1["c_nit"],$html);
		$html=str_replace('[direccion]',$row1["c_dir"],$html);
		$html=str_replace('[telefono]',$row1["c_tel"],$html);
	  }
	 }
	 
	 if(count($row2)>0){
	  foreach($row2 as $row2){
	  	$html=str_replace('[nombrecontacto]',$row2["cc_nom"],$html);
	  }
	 }
	 
	 //Datos de la Compa�ia
	 $crmModelObj=new crmScs();
	 $crmModelObj->connect();
	 
	 $row=$crmModelObj->getCompanyById($e_id);
	 if(count($row)>0){
	  foreach($row as $row){
	  	$comInf.='<table width="163" border="0" align="center">
          <tr>
            <td><div align="center"><img src="../Logos/'.$row["e_img"].'" /><br />
            </div></td>
          </tr>
          <tr>
            <td><div align="center">'.$row["e_info"].'</div></td>
          </tr>
        </table>';
	  }
	 }
	 $html=str_replace('[company]',$comInf,$html);
	 
	 //Datos del usuario
	 if(!empty($u_id)){
		 $user = new User();
		 $user->connect();
		 $user->getUserById($u_id);
		 if(!empty($user->sign)){
		 $img="<img src='../../../usr/".$user->sign."' />";
		 $html=str_replace('[firmaejecutivo]',$img,$html);
		 }else{
		 $html=str_replace('[firmaejecutivo]',$user->name,$html);
		 }
		 
	 }
	 
	 return $html; 
	 
	}
	
	public function sendEmailProcess($id){
	 
	 $quote = new quoteModelGe();
	 $quote->connect();
	 
	 $crm = new crmScs();
	 $crm->connect();
	 
	 $products = new products();
	 $products->connect();
	 
	 $row1=$quote->getQuoteById($id);
	 $row2=$quote->getQuoteDetail($id);
	 
	 $f=fopen('modules/crm/formats/quoteformat3.html','r');
	 $html=fread($f,filesize('modules/crm/formats/quoteformat3.html'));
	 
	 if(count($row1)>0){
	  foreach($row1 as $row1){
	  	$html=str_replace('[track]',$row1["q_track"],$html);
		$html=str_replace('[fechacotizacion]',$row1["q_fecha"],$html);
		$html=str_replace('[fechavencimiento]',$row1["q_fecha_fin"],$html);
		$html=str_replace('[observaciones]',$row1["q_obs"],$html);
		$html=str_replace('[subtotal]',number_format($row1["q_subto"]),$html);
		$html=str_replace('[subtotalcop]',number_format($row1["q_subto"]),$html);
		if(empty($row1["q_trm"])){
		 $row1["q_trm"]=1;
		}
		$html=str_replace('[trm]',number_format($row1["q_trm"]),$html);
		$html=str_replace('[ivacop]',number_format($row1["q_iva"]),$html);
		$html=str_replace('[totalcop]',number_format($row1["q_total"]),$html);
		$html=str_replace('[valorletras]',$crm->num2letras($row1["q_subto"], false, false),$html);
		
		//Otros Datos
		$cc_id=$row1["cc_id"];
		$c_id=$row1["c_id"];
		$e_id=$row1["e_id"];
		$u_id=$row1["u_id"];
	  }
	 }
	 
	 $i=0;
	 
	 if(count($row2)>0){
	  foreach($row2 as $row2){
	   if($i%2==0){
	    $bg="#f5f5f5";
	   }else{
	    $bg="#fff";
	   }
	   $i++;
	   
	  $row3=$products->getCurrencyById($row2["id_currency"]);
	   if(count($row3)>0){
		 foreach($row3 as $row3){
		  $currency=$row3["pc_code"];
		 }
	   }
	   
	   $qdet.='<tr valign="top" class="txt_destacado" bgcolor="'.$bg.'">
          <td width="100"><div align="justify">'.$row2['qd_desc'].'</div></td>
		  <td width="100"><div align="justify">'.$currency.'</div></td>
		  <td><div align="center">$'.number_format($row2['qd_trm']).'</div></td>';
		  
		  if($row2["qd_trm"]==1){
		  	$qdet.='<td><div align="center">&nbsp;</div></td>
			<td><div align="center">$'.number_format($row2['qd_price']).'</div></td>';
		  }else{
		  	$qdet.='<td><div align="center">$'.number_format($row2['qd_price'],2).'</div></td>
			<td><div align="center">&nbsp;</div></td>';
		  }
		 
          $qdet.='<td><div align="center">'.$row2['qd_qty'].'</div></td>
          <td><div align="center">$'.number_format($row2['qd_total']).'</div></td>
        </tr>';
	  } 
	 }
	 
	 $html=str_replace('[detallecotizacion]',$qdet,$html);
	 
	 
	 //Datos del cliente
	 $customer = new customerModelGe();
	 $customer->connect();
	 
	 $row1=$customer->getCustomerById2($c_id);
	 $row2=$customer->getCustomerContactById($cc_id);
	 
	 if(count($row1)>0){
	  foreach($row1 as $row1){
	  	$html=str_replace('[nombrecliente]',$row1["c_nom"],$html);
		$html=str_replace('[identificacion]',$row1["c_nit"],$html);
		$html=str_replace('[direccion]',$row1["c_dir"],$html);
		$html=str_replace('[telefono]',$row1["c_tel"],$html);
	  }
	 }
	 
	 if(count($row2)>0){
	  foreach($row2 as $row2){
	  	$html=str_replace('[nombrecontacto]',$row2["cc_nom"],$html);
		$c_email=$row2["cc_mail"];
	  }
	 }
	 
	 //Datos de la Compa�ia
	 $crmModelObj=new crmScs();
	 $crmModelObj->connect();
	 
	 $row=$crmModelObj->getCompanyById($e_id);
	 if(count($row)>0){
	  foreach($row as $row){
	  	$comInf.='<table width="163" border="0" align="center">
          <tr>
            <td><div align="center"><img src="'.$this->customerweb.__FOLDER__.'modules/crm/Logos/'.$row["e_img"].'" /><br />
            </div></td>
          </tr>
          <tr>
            <td><div align="center">'.$row["e_info"].'</div></td>
          </tr>
        </table>';
	  }
	 }
	 
	 $html=str_replace('[company]',$comInf,$html);
	 
	 //Datos del usuario
	 if(!empty($u_id)){
		 $user = new User();
		 $user->connect();
		 $user->getUserById($u_id);
		 if(!empty($user->sign)){
		 $img="<img src='../../../usr/".$user->sign."' />";
		 $html=str_replace('[firmaejecutivo]',$img,$html);
		 }else{
		 $html=str_replace('[firmaejecutivo]',$user->name,$html);
		 }
		 
	 }
	 
	 //Remitente
	 $from=$crm->getCrmParamById(5);
	 
	 
	 //Correo de seguimiento
	 $bcc=$crm->getCrmParamById(6);
	 
	 
	 //mensaje
	 $msg=$crm->getCrmParamById(7);
	 
	 
	 ini_set('SMTP','mail.goely.co');
  	 $headers="from:".$from."\r\n";
	 
	 //Enviar con copia
	 $contactmail=$crm->postVars('contactmail');
	 for($i=0;$i<=count($contactmail);$i++){
	  $headers.="Cc:".$contactmail[$i]."\r\n";
	 }
	 
	 $headers.="Bcc:".$bcc."\r\n";
  	 $headers.= "MIME-Version: 1.0\r\n"; 
  	 $headers.= "Content-type: text/html; charset=UTF-8\r\n";

	 
	 mail($c_email,"COTIZACION ".$this->customername,$html,$headers);
	}
	
	public function newQuoteDetailProcess(){
	 //$finder=$this->postVars('finder');
	 $id_product=$this->postVars('id_product');
	 $cant=$this->postVars('cant');
	 $trm=$this->postVars('trm');
	 $id_price=$this->postVars('id_price');
	 
	 //$finder=explode(':',$finder);
	 
				
	 $quote = new quoteModelGe();
	 $quote->connect();
	 
	 $products = new products();
	 $products->connect();
	 
	 //$row=$quote->getPriceById($finder[2]);
	 $row=$products->getProductsById($id_product);
	 if(count($row)>0){
	  foreach($row as $row){
	   	$desc=$row["pro_sdesc"];
		//$price=$row["pro_price"];
	  }
	 }
	 
	 //Busco el precio del producto
	 $row=$products->getPriceById($id_price);
	 foreach($row as $row){
	  $price=$row["pp_price"];
	  $id_currency=$row["pc_id"];
	 }
	 
	 
	 $res=$quote->newQuoteDetailItemTemp($id_product,$desc,$price,$cant,$trm,$id_currency);
	 
	}
	
	public function newQuoteDetailProcess2(){
	 $finder=$this->postVars('finder');
	 $cant=$this->postVars('cant');
	 $id=$this->postVars('id');
	 
	 $finder=explode(':',$finder);
	 
				
	 $quote = new quoteModelGe();
	 $quote->connect();
	 
	 $products = new products();
	 $products->connect();
	 
	 $row=$products->getProductsById($finder[2]);
	 if(count($row)>0){
	  foreach($row as $row){
	   	$desc=$row["pro_sdesc"];
		$price=$row["pro_price"];
	  }
	 }
	 
	 $por_convenio=$_SESSION["por_convenio"]/100;
	 $por_convenio2=$price*$por_convenio;
	 $price+=$por_convenio2;
	 
	 $res=$quote->newQuoteDetailItem($id,$finder[2],$desc,$price,$cant);
	 
	}
	
	public function _init($action){
		
		//$action=$this->getVars('actions');
		
		switch($action){
			
			case 'NewQuote': $res=$this->newQuoteProcess();
			break;
			
			case 'EditQuote': 
				$quote = new quoteModelGe();
				$quote->connect();
				$res=$quote->editQuote();
				
				$id=$quote->postVars('id');
				$sendemail=$quote->postVars('sendemail');
				if ($sendemail == 1) $this->sendEmailProcess($id);
				
			break;
			
			case 'DelQuote': 
				$quote = new quoteModelGe();
				$quote->connect();
				$res=$quote->delQuote();
			break;
			
			case 'PrintQuote': 
				$res=$this->printQuoteProcess();
			break;
			
			case 'showQuote': 
				$res=$this->showQuoteProcess();
			break;
			
			default:
			    $act=$this->getVars('act'); 
				$quote = new quoteModelGe();
				$res=$quote->getQuotes($act);
			break;
			
			case 'GetQuoteById':
				$id=$this->getVars('id'); 
				$quote = new quoteModelGe();
				$quote->connect();
				$res=$quote->getQuoteById($id);
			break;
			
			case 'GetQuoteByCustomerId':
				$id=$this->getVars('id'); 
				$quote = new quoteModelGe();
				$quote->connect();
				$res=$quote->getQuoteByCustomerId($id);
			break;
			
			case 'GetQuoteByCustomerId2': 
				$quote = new quoteModelGe();
				$quote->connect();
				$quote->getSession();
				$res=$quote->getQuoteByCustomerId($_SESSION['customer']);
			break;
			
			case 'GetQuoteAll':
				//$id=$this->getVars('id'); 
				$quote = new quoteModelGe();
				$quote->connect();
				$quote->startPage($quote->postVars('page'));
				$res=$quote->getQuotes(1,$quote->start);
			break;
			
			case 'GetQuoteByUser':
				//$id=$this->getVars('id'); 
				$quote = new quoteModelGe();
				$quote->connect();
				$quote->getSession();
				$quote->startPage($quote->postVars('page'));
				$res=$quote->getQuotesByUser(1,$quote->start,$_SESSION['user_id']);
			break;
			
			case 'GetQuoteByFind':
				$term=$this->getVars('term');
				$quote = new quoteModelGe();
				$quote->connect();
				$res=$quote->getQuoteByFind($term);
			break;
			
			case 'GetUserQuoteByFind':
				$term=$this->getVars('term');
				$quote = new quoteModelGe();
				$quote->connect();
				$quote->getSession();
				$res=$quote->getUserQuoteByFind($term,$_SESSION['user_id']);
			break;
			
			case 'GetQuoteBySearch':
				$finder=$this->postVars('finder');
				$f=explode(':',$finder); 
				$quote = new quoteModelGe();
				$quote->connect();
				$res=$quote->getQuoteByTrack($f[0]);
			break;
			
			//Quote Details
			case 'NewQuoteDetailTemp':
				$this->newQuoteDetailProcess();
			break;
			
			case 'EditQuoteDetailTemp':
				
				$id=$this->postVars('id');
				$fab=$this->postVars('fab');
				$ref=$this->postVars('ref');
				$desc=$this->postVars('desc');
				$pr=$this->postVars('pr');
				$type=$this->postVars('type');
				$cant=$this->postVars('cant');
				$disp=$this->postVars('disp');
				$total=$this->postVars('total');
				$noiva=$this->postVars('noiva');
				$por_margen=$this->postVars('por_margen');
				$pbase=$this->postVars('pbase');
				$te=$this->postVars('te');
				$gan1=$this->postVars('gan1');
				
				$quote = new quoteModelGe();
				$res=$quote->editQuoteDetailItemTemp($id,$fab,$ref,$desc,$pr,$type,$cant,$disp,$total,$noiva,$por_margen,$pbase,$te,$gan1);
			break;
			
			case 'DelQuoteDetailTemp':
				$id_item=$this->getVars('id_item');
				$quote = new quoteModelGe();
				$quote->connect();
				$res=$quote->delQuoteDetailItemTemp($id_item);
			break;
			
			case 'GetQuoteDetailTemp':
				$quote = new quoteModelGe();
				$quote->connect();
				$res=$quote->getQuoteDetailTemp();
			break;
			
			case 'NewQuoteDetail':
					$this->newQuoteDetailProcess2();
			break;
			
			case 'EditQuoteDetail':
				
				$id=$this->postVars('id');
				$fab=$this->postVars('fab');
				$ref=$this->postVars('ref');
				$desc=$this->postVars('desc');
				$pr=$this->postVars('pr');
				$type=$this->postVars('type');
				$cant=$this->postVars('cant');
				$disp=$this->postVars('disp');
				$total=$this->postVars('total');
				$noiva=$this->postVars('noiva');
				$por_margen=$this->postVars('por_margen');
				$pbase=$this->postVars('pbase');
				$te=$this->postVars('te');
				$gan1=$this->postVars('gan1');
				
				$quote = new quoteModelGe();
				$res=$quote->editQuoteDetailItem($id,$fab,$ref,$desc,$pr,$type,$cant,$disp,$total,$noiva,$por_margen,$pbase,$te,$gan1);
			break;
			
			case 'DelQuoteDetail':
				$id_item_quote=$this->getVars('id_item_quote');
				$quote = new quoteModelGe();
				$quote->connect();
				$res=$quote->delQuoteDetailItem($id_item_quote);
			break;
			
			case 'GetQuoteDetail':
				$id=$this->getVars('id');
				$quote = new quoteModelGe();
				$quote->connect();
				$res=$quote->getQuoteDetail($id);
			break;
			
			
			
			//Pricelist
			case 'NewPrice':
				
				$fab=$this->postVars('fab');
				$ref=$this->postVars('ref');
				$desc=$this->postVars('desc');
				$precio=$this->postVars('precio');
				$subcat=$this->postVars('subcat');
				$cat=$this->postVars('cat');
				$disp=$this->postVars('disp');
				$fab=$this->postVars('fab');
				$descr=$this->postVars('descr');
				$moneda=$this->postVars('moneda');
				$exiva=$this->postVars('exiva');
				
				$quote = new quoteModelGe();
				$quote->connect();
				$quote->newPrice($fab,$ref,$desc,$precio,$subcat,$cat,$disp,$descr,$moneda,$exiva);
			break;
			
			case 'EditPrice':
				
				$id=$this->postVars('id');
				$fab=$this->postVars('fab');
				$ref=$this->postVars('ref');
				$desc=$this->postVars('desc');
				$precio=$this->postVars('precio');
				$subcat=$this->postVars('subcat');
				$cat=$this->postVars('cat');
				$disp=$this->postVars('disp');
				$fab=$this->postVars('fab');
				$descr=$this->postVars('descr');
				$moneda=$this->postVars('moneda');
				$exiva=$this->postVars('exiva');
				
				$quote = new quoteModelGe();
				$quote->connect();
				$quote->editPrice($id,$fab,$ref,$desc,$precio,$subcat,$cat,$disp,$descr,$moneda,$exiva);
			break;
			
			case 'DelPrice':
				
				$id=$this->getVars('id');
				$quote = new quoteModelGe();
				$quote->connect();
				$quote->delPrice($id);
			break;
			
			case 'GetPrice':
				$quote = new quoteModelGe();
				$quote->connect();
				$res=$quote->getPriceList();
			break;
			
			case 'GetPriceById':
				$id=$this->getVars('id');
				$quote = new quoteModelGe();
				$quote->connect();
				$res=$quote-> getPriceById($id);
			break;
			
			case 'GetPriceByFinder':
				$finder=$this->postVars('finder');
				$id=explode(':',$finder);
				$quote = new quoteModelGe();
				$quote->connect();
				$res=$quote->getPriceById($id[2]);
			break;
			
			case 'LoadPrice':
			    $flush_manual=$this->getVars('flush');
				$this->loadPriceProcess($flush_manual);
			break;
			
			
			case 'GetPriceByFind':
				$term=$this->postVars('term');
				$quote = new quoteModelGe();
				$quote->connect();
				$res=$quote->getPriceByFind($term);
			break;
			
			//categories
			case 'NewCategory':
				$this->newCategoryProcess();
			break;
			case 'editCategory':
				$this->editCategoryProcess();
			break;
			
			case 'delCategory':
				$id=$this->getVars('id');
				$quote = new quoteModelGe();
				$quote->connect();
				$res=$quote->delCat($id);
			break;
			
			case 'getCategory':
				$quote = new quoteModelGe();
				$quote->connect();
				$res=$quote->getCat();
			break;
			
			case 'getSubCategory':
				$id=$this->getVars('id');	
				$quote = new quoteModelGe();
				$res=$quote->getSubCat($id);
			break;
			
			case 'getCategoryById':
				$id=$this->getVars('id');	
				$quote = new quoteModelGe();
				$quote->connect();
				$res=$quote->getCatById($id);
			break;
			
		
		}
		
		return $res;
	}
	
	
	
	
 }
?>