// JavaScript Document
function myAutoComplete(cssid,page){
	
 $("#"+cssid).autocomplete({
							source:page,
							select: function(event,ui){
								
							}
						});
}

function getCtzResumePrices(){

	$.ajax({
	 type:'POST',
	 url:'modules/crm/view/getCtzResumePrices.php',
	 async:false,
	 success:function(data){
		 document.getElementById('resumeCtz').innerHTML=data;
	 }
	});
}

function saveOrderKpi(id,kpi){
 
	$.ajax({
	 type:'POST',
	 url:'modules/crm/view/editOrderKpi.php',
	 async:false,
	 data: 'id='+id+"&kpi="+kpi,
	});
}

function saveOrderKpi2(id,kpi){
 
	$.ajax({
	 type:'POST',
	 url:'modules/crm/view/editOrderKpi2.php',
	 async:false,
	 data: 'id='+id+"&kpi="+kpi,
	});
}

function saveConvenioValue(convenio){
	$.ajax({
	 type:'POST',
	 url:'modules/crm/view/saveConvenioValue.php',
	 async:false,
	 data: 'convenio='+convenio,
	});
}

function findPrice(){
	document.getElementById('gauch').style.display='block';
	var term=document.getElementById('term').value;
	$.ajax({
	 type:'POST',
	 url:'findPrice.php',
	 async:false,
	 data: 'term='+term,
	 success:function(data){
		 document.getElementById('gauch').style.display='none';
		 document.getElementById('showPl').innerHTML=data;
	 }
	});
}

function findPrice2(id){
	document.getElementById('gauch').style.display='block';
	var term=document.getElementById('term').value;
	$.ajax({
	 type:'POST',
	 url:'findPrice.php',
	 async:false,
	 data: 'term='+term+'&id='+id,
	 success:function(data){
		 document.getElementById('gauch').style.display='none';
		 document.getElementById('showPl').innerHTML=data;
	 }
	});
}
