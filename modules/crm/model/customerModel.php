<?php
	class customerModelGe extends GeCore{
	
	public $_errors=array();
	
	 //Customer Methods ///////////////////////////////////////////////////////////////
	public function newCustomer(){
		
		if(!$this->valDatas()){
		 return false;
		}
		
		$nom=$this->postVars('nom');
 		$nit=$this->postVars('nit');
 		$dir=$this->postVars('dire');
 		$tel=$this->postVars('tel');
 		$web=$this->postVars('web');
 		$tc=$this->postVars('tc');
 		$ti=$this->postVars('ti');
 		$nit2=$this->postVars('nit2');
		
		if(empty($nit2)){
			$nit=$nit."-".$nit2;
		}
		$this->getSession();
		
		 $sql="insert into `customer` (`c_nom`, `c_nit`, `c_dir`, `c_tel`, `c_web`, `u_id`, `ti_id`, `ccl_id`) values ('$nom','$nit','$dir','$tel','$web','".$_SESSION["user_id"]."','$ti','$tc')";
		$this->Execute($sql);
		
		$idc=$this->getLastID();
		
		return $idc;
	}

	
	public function editCustomer(){
	
	    if(!$this->valDatas(0)){
		 return false;
		}
		
		$id=$this->postVars('id');
		$nom=$this->postVars('nom');
 		$nit=$this->postVars('nit');
 		$dir=$this->postVars('dire');
 		$tel=$this->postVars('tel');
 		$web=$this->postVars('web');
 		$tc=$this->postVars('tc');
 		$ti=$this->postVars('ti');
 		$nit2=$this->postVars('nit2');
		
		if(empty($nit2)){
			$nit=$nit."-".$nit2;
		}
		
		$sql="update `customer` set `c_nom`='$nom', `c_nit`='$nit', `c_dir`='$dir', `c_tel`='$tel', `c_web`='$web', `ti_id`='$ti', `ccl_id`='$tc' where c_id='$id'";
		$this->Execute($sql);
		
	}
	
	public function delCustomer($id){
		$sql="delete from `customer` where c_id='$id'";
		$this->Execute($sql);
	}
	
	
	public function getCustomers($start=0){
	 $sql="select * from customer order by c_nom asc LIMIT $start,".$this->tam_page;
	 return $this->ExecuteS($sql);
	}
	
	public function getTotalCustomers(){
	 $sql="select * from customer";
	 $this->res=$this->Execute($sql);
	 return $this->getNumRows();
	}
	
	public function getCustomerById(){
	  $id=$this->getVars('id');
	  $sql="select * from customer where c_id='$id'";
	  return $this->ExecuteS($sql);
	}
	
	public function getCustomerById2($id){
	  $sql="select * from customer where c_id='$id'";
	  return $this->ExecuteS($sql);
	}
	
	public function checkCustomerExists($nit){
	  $sql="select * from customer where c_nit='$nit'";
	  $row=$this->ExecuteS($sql);
	  
	  if(count($row)>0){
	   return true;
	  }else{
	   return false;
	  }
	}
	
	public function getCustomerByFind($term){
	 $sql="select * from customer as c inner join customer_contact as cc on cc.c_id=c.c_id where c_nom like '%$term%'";
	 $row=$this->ExecuteS($sql);
	 
	 if(count($row)>0){
	 foreach($row as $row){
	 	$arr[]=$row["c_nom"]." ".$row["c_nit"]." ".$row["cc_mail"].":".$row["c_id"];
	 }
	}
	return json_encode($arr);
	}
	
	public function getCustomerByFind2($term){
	 $sql="select * from customer where c_nom like '%$term%'";
	 return $row=$this->ExecuteS($sql);
	}
	
	public function getUserCustomerByFind($term,$id_user){
	 $sql="select * from customer where c_nom like '%$term%' and u_id='$id_user'";
	 $row=$this->ExecuteS($sql);
	 
	 if(count($row)>0){
	 foreach($row as $row){
	 	$arr[]=$row["c_nom"].":".$row["c_id"];
	 }
	}
	return json_encode($arr);
	}
	
	public function findCustomerById(){
	  $finder=$this->postVars('finder');
	  $f=explode(':',$finder);
	  $sql="select * from customer where c_id='".$f[1]."'";
	  return $this->ExecuteS($sql);
	}
	
	public function getCustomerAsigned(){
	  $user=$this->postVars('user');
	  
	  $sql="select * from customer where u_id='$user' order by c_nom asc";
	  return $this->ExecuteS($sql);
	}
	
	public function AsignUserCustomer(){
	 $customer=$this->postVars('customers');
	 $user=$this->postVars('user');
	 
	 	 for ($i=0;$i<count($customer);$i++)    
		 {     
		  $sql="update customer set u_id='$user' where c_id='".$customer[$i]."'";
		  $this->Execute($sql);    
		 }
	}
	
	public function getUserCustomer($user,$start=0){
	  $sql="select * from customer where u_id='$user' order by c_nom asc LIMIT $start,".$this->tam_page;
	  return $this->ExecuteS($sql);
	}
	
	public function getTotalUserCustomer($user){
	  $sql="select * from customer where u_id='$user'";
	  $this->res=$this->Execute($sql);
	  return $this->getNumRows();
	}
	
	//Customers Contacts//////////////////////////////////////////////////////////////
	public function newCustomerContact($id){
		
		$nom=$this->postVars('nomc');
 		$cargo=$this->postVars('cargo');
 		$email=$this->postVars('email');
 		$tel=$this->postVars('telc');
 		$cel=$this->postVars('cel');
		$ini=$this->postVars('ini');
		$emp=$this->postVars('emp');
		$tc=$this->postVars('tcargo');
		$tp=$this->postVars('tp');
		
		$sql="insert into `customer_contact` (`cc_nom`, `cc_cargo`, `cc_telefono`, `cc_birth`, `cc_cel`, `cc_mail`, `c_id`, `cprof_id`, `tc_id`) values ('$nom','$cargo','$tel','$ini','$cel','$email','$id','$tp','$tc')";
		
		$this->Execute($sql);
		
		return $idcc=$this->getLastID();
	}
	
	public function editCustomerContact(){
		
		$id=$this->postVars('id');
		$nom=$this->postVars('nomc');
 		$cargo=$this->postVars('cargo');
 		$email=$this->postVars('email');
 		$tel=$this->postVars('telc');
 		$cel=$this->postVars('cel');
		$ini=$this->postVars('ini');
		$emp=$this->postVars('emp');
		$tc=$this->postVars('tcargo');
		$tp=$this->postVars('tp');
		
	 $sql="update `customer_contact` set `cc_nom`='$nom', `cc_cargo`='$cargo', `cc_telefono`='$tel', `cc_birth`='$ini', `cc_cel`='$cel', `cc_mail`='$email', `cprof_id`='$tp', `tc_id`='$tc' where cc_id='$id'";
	 
	 	$this->Execute($sql);
	}
	
	public function delCustomerContact(){
	 $id=$this->getVars('idc');
	 $sql="delete from `customer_contact` where cc_id='$id'";
	 $this->ExecuteS($sql);
	}
	
	public function delCustomerContactByCustomer($id){

	 $sql="delete from `customer_contact` where c_id='$id'";
	 $this->ExecuteS($sql);
	}
	
	public function getCustomerContact($id){
		$sql="select * from customer_contact where c_id='$id'";
		return $this->ExecuteS($sql);
	}
	
	public function getCustomerContactById($id){
		$sql="select * from customer_contact where cc_id='$id'";
		return $this->ExecuteS($sql);
	}
	
	//Customer Setup////////////////////////////////////////////////////////////////////////
	//Industry
	public function newCustomerIndustry(){
	 $nom=$this->postVars('nom');
     $desc=$this->postVars('desc');
	 
	  $sql="insert into `customer_industry` (`ci_nom`, `ci_desc`) values ('$nom','$desc')";
	  $this->Execute($sql);
	}
	
	public function editCustomerIndustry(){
	 
	 $id=$this->postVars('id');
	 $nom=$this->postVars('nom');
     $desc=$this->postVars('desc');
	 
	 $sql="update `customer_industry` set `ci_nom`='$nom',`ci_desc`='$desc' where ci_id='$id'";
	 $this->Execute($sql);
	}
	
	public function delCustomerIndustry(){
	 
	 $id=$this->getVars('id');
	 
	 $sql="delete from `customer_industry` where ci_id='$id'";
	 $this->Execute($sql);
	}
	
	public function getCustomerIndustry(){
		 $sql="select * from `customer_industry`";
	 	 return $this->ExecuteS($sql);
	}
	
	public function getCustomerIndustryById($id){
	  $sql="select * from `customer_industry` where ci_id='$id'";
	  return $this->ExecuteS($sql);
	}
	
	//Positions
	public function newCustomerPositions(){
	 $nom=$this->postVars('nom');
	 
	  $sql="insert into `customer_position` (`cp_nom`) values ('$nom')";
	  $this->Execute($sql);
	}
	
	public function editCustomerPosition(){
	 
	 $id=$this->postVars('id');
	 $nom=$this->postVars('nom');
     $desc=$this->postVars('desc');
	 
	 $sql="update `customer_position` set `cp_nom`='$nom' where cp_id='$id'";
	 $this->Execute($sql);
	}
	
	public function delCustomerPosition(){
	 
	 $id=$this->getVars('id');
	 
	 $sql="delete from `customer_position` where cp_id='$id'";
	 $this->Execute($sql);
	}
	
	public function getCustomerPosition(){
		 $sql="select * from `customer_position`";
	 	 return $this->ExecuteS($sql);
	}
	
	public function getCustomerPositionById($id){
	  $sql="select * from `customer_position` where cp_id='$id'";
	  return $this->ExecuteS($sql);
	}
	
	//Clasification
	public function newCustomerClasification(){
	 $nom=$this->postVars('nom');
     $score=$this->postVars('score');
	 $color=$this->postVars('color');
	 
	  $sql="insert into `customer_clafisification` (`ccl_nom`, `ccl_puntos`, `ccl_color`) values ('$nom','$score','$color')";
	  $this->Execute($sql);
	}
	
	public function editCustomerClasification(){
	 
	 $id=$this->postVars('id');
	  $nom=$this->postVars('nom');
     $score=$this->postVars('score');
	 $color=$this->postVars('color');
	 
	 $sql="update `customer_clafisification` set `ccl_nom`='$nom',`ccl_puntos`='$score',`ccl_color`='$color' where ccl_id='$id'";
	 $this->Execute($sql);
	}
	
	public function delCustomerClasification(){
	 
	 $id=$this->getVars('id');
	 
	 $sql="delete from `customer_clafisification` where ccl_id='$id'";
	 $this->Execute($sql);
	}
	
	public function getCustomerClasification(){
		 $sql="SELECT * FROM `customer_clafisification`";
	 	 return $this->ExecuteS($sql);
	}
	
	public function getCustomerClasificationById($id){
	  $sql="SELECT * FROM `customer_clafisification` where ccl_id='$id'";
	  return $this->ExecuteS($sql);
	}
	
	//Profession
	public function newCustomerProfession(){
	 $nom=$this->postVars('nom');
	 
	  $sql="insert into `customer_profesion` (`cprof_nom`) values ('$nom')";
	  $this->Execute($sql);
	}
	
	public function editCustomerProfession(){
	 
	 $id=$this->postVars('id');
	 $nom=$this->postVars('nom');
	 
	 $sql="update `customer_profesion` set `cprof_nom`='$nom' where cprof_id='$id'";
	 $this->Execute($sql);
	}
	
	public function delCustomerProfession(){
	 
	 $id=$this->getVars('id');
	 
	 $sql="delete from `customer_profesion` where cprof_id='$id'";
	 $this->Execute($sql);
	}
	
	public function getCustomerProfession(){
		 $sql="select * from `customer_profesion`";
	 	 return $this->ExecuteS($sql);
	}
	
	public function getCustomerProfessionById($id){
	  $sql="select * from `customer_profesion` where cprof_id='$id'";
	  return $this->ExecuteS($sql);
	}
	
	public function valDatas($checkId=1){
	    
		$pass=true;
		  
	 	$nom=$this->postVars('nom');
 		$nit=$this->postVars('nit');
 		$dir=$this->postVars('dire');
 		$tel=$this->postVars('tel');
 		$web=$this->postVars('web');
 		$tc=$this->postVars('tc');
 		$ti=$this->postVars('ti');
 		$nit2=$this->postVars('nit2');
		
		if(!$this->Validator($nom,1)){
			$this->_errors[]="Debe asignar un nombre";
			$pass=false;
		}
		
		if(!$this->Validator($nit,0)){
			$this->_errors[]="Debe asignar una identificacion";
			$pass=false;
		}
		
		if(!$this->Validator($nit2,0)){
			$this->_errors[]="Debe asignar un digito de verificacion. Si es persona natural el digito de verificación es 0";
			$pass=false;
		}
		
		if($checkId==1){
			if($this->checkCustomerExists($nit)){
				$this->_errors[]="Ya hay una identificación registrada con este numero ".$nit;
				$pass=false;
			}
		}
		
		return $pass;
	 
	}
	
};
?>
