<?php
 class orderModelGe extends GeCore{
  //Order Methods ///////////////////////////////////////////////////
	public function newOrder($track){
	
	 $company=$this->postVars('company');
 	 $customer=$this->postVars('customer');
 	 $contact=$this->postVars('contact');
 	 $term=$this->postVars('term');
 	 $ini=$this->postVars('ini');
 	 $subto=$this->postVars('subto');
 	 $subtotalcop=$this->postVars('subtotalcop');
 	 $ivacop=$this->postVars('ivacop');
 	 $totalcop=$this->postVars('totalcop');
 	 $obs=$this->postVars('obs');
 	 $creacion=$this->postVars('creacion1');
 	 $trm=$this->postVars('trm');
 	 $gestion=$this->postVars('gestion');
 	 $desc=$this->postVars('desc'); 
 	 $porgan=$this->postVars('porgan');
 	 $gan1=$this->postVars('gan1');
 	 $tipo=$this->postVars('tipo');
	 $ctz=$this->postVars('ctz');
	 $desc=$this->postVars('desc');
	 $cc=$this->postVars('cc');
	 $horaentrega=$this->postVars('horaentrega');
	 
	 
	 //$track=$this->getLastTrack();
	 $hoy=date('Y-m-d H:i:s');
	 
		 $sql="insert into `orders` (`o_fecha`, `o_subto`, `o_iva`, `o_total`, `o_mod_pago`, `o_fecha_fin`, `u_id`, `e_id`, `o_trm`, `cc_id`, `o_subto_base`,`o_track`,`o_obs`,`o_porganancia`,`o_ganancia`,`o_tipo`,`o_active`,`o_quotes`,`c_id`,`o_desc`,`o_cc`,`o_horaentrega`) values ('$hoy','$subtotalcop','$ivacop','$totalcop','$term','$ini','".$_SESSION["user_id"]."','$company','$trm','$contact','$subto','$track','$obs','$porgan','$gan1','$tipo','1','$ctz','$customer','$desc','$cc','$horaentrega')"; 
	  
	  $this->Execute($sql);
	  $idq=$this->getLastID();
	  
	  return $idq;
	}
	
	public function editOrder(){
	 
	 $id=$this->postVars('id');
	 $company=$this->postVars('company');
 	 $customer=$this->postVars('customer');
 	 $contact=$this->postVars('contact');
 	 $term=$this->postVars('term');
 	 $ini=$this->postVars('ini');
 	 $subto=$this->postVars('subto');
 	 $subtotalcop=$this->postVars('subtotalcop');
 	 $ivacop=$this->postVars('ivacop');
 	 $totalcop=$this->postVars('totalcop');
 	 $obs=$this->postVars('obs');
 	 $creacion=$this->postVars('creacion1');
 	 $trm=$this->postVars('trm');
 	 $gestion=$this->postVars('gestion');
 	 $desc=$this->postVars('desc'); 
 	 $porgan=$this->postVars('porgan');
 	 $gan1=$this->postVars('gan1');
 	 $tipo=$this->postVars('tipo');
	 $ctz=$this->postVars('ctz');
	 $horaentrega=$this->postVars('horaentrega');
	 
	 $sql="update `orders` set `o_subto`='$subtotalcop',`o_iva`='$ivacop',`o_total`='$totalcop',`o_mod_pago`='$term',`o_fecha_fin`='$ini',`e_id`='$company',`o_trm`='$trm',`cc_id`='$contact',`o_subto_base`='$subto',`o_obs`='$obs',`c_id`='$customer',`o_desc`='$desc',`o_horaentrega`='$horaentrega' where o_id='$id'";
	 
	  $this->Execute($sql);
	  
	}
	
	public function delOrder(){
	 $id=$this->postVars('id');
	 $aid=$this->postVars('aid');
	 
	 switch($aid){
	  case 1: $msg="Error al ingresar datos";
	  break;
	  case 2: $msg="Cliente no pago";
	  break;
	  case 3: $msg="Cliente Cancela la compra";
	  break;
	 }
	 
	 $sql="update `orders` set `o_active`='0', `o_aid`='$aid', `o_anom`='$msg' where o_id='$id'";
	 $this->Execute($sql);
	 
	 return 'ok';
	}
	
	public function flushOrders(){
	 $id=$this->getVars('id');
	 $sql="delete from `orders` where `q_active`='0'";
	 $this->Execute($sql);
	}
	
	public function getOrders($act=1, $start=0){
	 $sql="select * from orders where o_active='$act' order by o_fecha desc LIMIT $start,".$this->tam_page;
	 return $this->ExecuteS($sql);
	}
	
	public function getTotalOrders($act=1){
	 $sql="select * from orders where o_active='$act'";
	 $this->res=$this->Execute($sql);
	 return $this->getNumRows();
	}
	
	public function getOrdersByUser($act=1, $start=0, $id_user){
	 $sql="select * from orders where o_active='$act' and u_id='$id_user' order by o_fecha desc LIMIT $start,".$this->tam_page;
	 return $this->ExecuteS($sql);
	}
	
	public function getTotalOrdersByUser($act=1, $id_user){
	 $sql="select * from orders where o_active='$act' and u_id='$id_user'";
	 $this->res=$this->Execute($sql);
	 return $this->getNumRows();
	}
	
	public function getOrderById($id){
	 $sql="select * from orders where o_id='$id'";
	 return $this->ExecuteS($sql);
	}
	
	public function getOrderByCustomer($id){
	 $sql="select * from orders where c_id='$id' and o_active='1'";
	 //$sql="select * from orders where c_id='$id'";
	 return $this->ExecuteS($sql);
	}
	
	public function editOrderStatus($id,$status){
	 $sql="update `orders` set `os_id`='$status' where o_id='$id'";
	 $this->Execute($sql);
	}
	
	public function getOrderByFind($id){
	 $sql="select * from orders where o_track like '%$id%' and o_active='1'";
	 $row=$this->ExecuteS($sql);
	  if(count($row)>0){
	 foreach($row as $row){
	 	$arr[]=$row["o_track"].":".$row["o_desc"];
	 }
	}
	
	return json_encode($arr);
	}
	
	public function getUserOrderByFind($id,$id_user){
	 $sql="select * from orders where o_track like '%$id%' and u_id='$id_user' and o_active='1'";
	 $row=$this->ExecuteS($sql);
	  if(count($row)>0){
	 foreach($row as $row){
	 	$arr[]=$row["o_track"].":".$row["o_desc"];
	 }
	}
	
	return json_encode($arr);
	}
	
	public function getOrderByTrack($id){
	 $sql="select * from orders where o_track='$id'";
	 return $this->ExecuteS($sql);
	}
	
	public function getOrderByCenterCost($cc){
	 $sql="select * from orders where o_cc='$cc'";
	 return $this->ExecuteS($sql);
	}
	
	//Order details Methods//////////////////////////////////////
	//Temportal Details
	public function newOrderDetailItemTemp($id_product=NULL,$desc=NULL,$price=NULL,$qty=NULL,$kpi=NULL,$trm=NULL,$id_currency=NULL,$total=NULL){
	
	  //$total=$price*$qty;
	  $sql="insert into `orders_detail_temp` (`u_id`, `odt_kpi`, `id_product`, `odt_desc`, `odt_price`, `odt_qty`, `odt_total`, `odt_trm`, `id_currency`) values ('".$_SESSION['user_id']."','$kpi','$id_product','$desc','$price','$qty','$total','$trm','$id_currency')";
	  $this->Execute($sql);
	  
	}
	
	public function editOrderDetailItemTemp($id=NULL,$id_product=NULL,$desc=NULL,$price=NULL,$qty=NULL,$kpi=NULL){
		
		$total=$price*$qty;
		 $sql="update `orders_detail_temp` set `odt_kpi`='$kpi', `id_product`='$id_product', `odt_desc`='$desc', `odt_price`='$price', `odt_qty`='$qty', `odt_total`='$total' where odt='$id'";
	  
	  $this->Execute($sql);
	
	}
	
	public function delOrderDetailItemTemp($id=NULL){
	  $sql="delete from `orders_detail_temp` where odt_id='$id'";
	  $this->Execute($sql);
	}
	
	public function flushOrderDetailItemTemp($id=NULL){
	  $sql="delete from `orders_detail_temp` where u_id='".$_SESSION["user_id"]."'";
	  $this->Execute($sql);
	}
	
	public function getOrderDetailTemp(){
	 $sql="select * from `orders_detail_temp` where u_id='".$_SESSION["user_id"]."'";
	 return $this->ExecuteS($sql);
	}
	
	public function editOrderKpiItem(){
	 $id=$this->postVars('id');
	 $kpi=$this->postVars('kpi');
	 $sql="update orders_detail_temp set odt_kpi='$kpi' where odt_id='$id'";
	 $this->Execute($sql);
	}
	
	public function editOrderKpiItem2(){
	 $id=$this->postVars('id');
	 $kpi=$this->postVars('kpi');
	 $sql="update orders_detail set od_kpi='$kpi' where od_id='$id'";
	 $this->Execute($sql);
	}
	
	//Orders Details
	public function newOrderDetailItem($id=NULL,$id_product=NULL,$desc=NULL,$price=NULL,$qty=NULL,$kpi=NULL,$trm=NULL,$id_currency=NULL,$total=NULL){
	
		//$total=$price*$qty;
	  $sql="insert into `orders_detail` (`o_id`, `od_kpi`, `id_product`, `od_desc`, `od_price`, `od_qty`, `od_total`, `od_trm`, `id_currency`) values ('$id','$kpi','$id_product','$desc','$price','$qty','$total','$trm','$id_currency')";
	  
	  $this->Execute($sql);
	  
	}
	
	public function editOrderDetailItem($id=NULL,$id_product=NULL,$desc=NULL,$price=NULL,$qty=NULL,$kpi=NULL){
	
		 $sql="update `orders_detail` set `od_kpi`='$kpi', `id_product`='$id_product', `od_desc`='$desc', `od_price`='$price', `od_qty`='$qty', `od_total`='$total' where od_id='$id'";
	  
	  $this->Execute($sql);
	
	}
	
	public function delOrderDetailItem($id=NULL){
	  $sql="delete from `orders_detail` where od_id='$id'";
	  $this->Execute($sql);
	}
	
	public function getOrdersDetail($id){
	 $sql="select * from `orders_detail` where o_id='$id' order by od_desc asc";
	 return $this->ExecuteS($sql);
	}
	
	public function getParam($id){
	 $sql="select * from crmparam where cp_id='$id'";
	 $row=$this->ExecuteS($sql);
	 foreach($row as $row){
	 	$track=$row["cp_value"];
	 }
	 
	 return $track;
	}
	
	public function editParam($id){
	 $sql="select * from crmparam where cp_id='$id'";
	 $row=$this->ExecuteS($sql);
	 foreach($row as $row){
	 	$track=$row["cp_value"];
	 }
	 
	 $track++;
	 
	 $sql="update crmparam set cp_value='$track' where cp_id='$id'";
	 $this->Execute($sql);
	}
	
	/*Order States*/
	public function newOrderState(){
	 $name=$this->postVars('name');
	 $color=$this->postVars('color');
	 
	 $sql="insert into `orders_state` (`os_name`, `os_color`) values ('$name','$color')";
	 $this->Execute($sql);
	}
	
	public function editOrderState(){
	 
	 $id=$this->postVars('id');
	 $name=$this->postVars('name');
	 $color=$this->postVars('color');
	 
	 $sql="update `orders_state` set `os_name`='$name', `os_color`='$color' where os_id='$id'";
	 $this->Execute($sql);
	}
	
	public function delOrderState(){
	 
	 $id=$this->getVars('id');
	 
	 $sql="delete from `orders_state` where os_id='$id'";
	 $this->Execute($sql);
	}
	
	public function getOrderState(){
	 
	 $sql="select * from `orders_state`";
	 return $this->ExecuteS($sql);
	}
	
	public function getOrderStateById($id){
	 
	 $sql="select * from `orders_state` where os_id='$id'";
	 return $this->ExecuteS($sql);
	}
	
	public function newOrderHistory(){
	 
	 $id=$this->postVars('id');
	 $desc=$this->postVars('desc');
	 $os=$this->postVars('os');
	 
	 if($this->Validator($os,1)){
	 
		 $row=$this->getOrderStateById($os);
		 foreach($row as $row){
		  $os_name=$row["os_name"];
		  $os_color=$row["os_color"];
		 }
		 
		 $hoy=date('Y-m-d H:i:s');
		 
		 $sql="insert into `orders_history` (`oh_date`, `oh_desc`, `o_id`, `oh_name`, `oh_color`) values ('$hoy','$desc','$id','$os_name','$os_color')";
		 $this->Execute($sql);
		 
		 $sql="update orders set o_current_state='$os' where o_id='$id'";
		 $this->Execute($sql);
	 }else{
	  return "nok";
	 }
	 
	}
	
	public function newOrderHistory2($id_state,$id_order){
	 
	 
	 $row=$this->getOrderStateById($id_state);
	 foreach($row as $row){
	  $os_name=$row["os_name"];
	  $os_color=$row["os_color"];
	 }
	 
	 $hoy=date('Y-m-d H:i:s');
	 
	 $sql="insert into `orders_history` (`oh_date`, `oh_desc`, `o_id`, `oh_name`, `oh_color`) values ('$hoy','$desc','$id_order','$os_name','$os_color')";
	 $this->Execute($sql);
	 
	}
	
	public function getOrderHistory($id){		
		$sql="select * from orders_history where o_id='$id' order by oh_date desc";
		return $this->ExecuteS($sql);
	}
	
	public function getLastOrderHistory($id){		
		$sql="select * from orders_history where o_id='$id' order by oh_date desc LIMIT 0,1";
		return $this->ExecuteS($sql);
	}
 };
?>
