<?php
class crmScs extends GeCore{

	public function editCrmParam(){
	 $id=$this->postVars('id');
	 $value=$this->postVars('value');
	 
	 $sql="update crmparam set cp_value='$value' where cp_id='$id'";
	 $this->Execute($sql);
	}
	
	public function getCrmParamById($id){
	 $sql="select * from crmparam where cp_id='$id'";
	 $row=$this->ExecuteS($sql);
	 
	 foreach($row as $row){
	  $value=$row["cp_value"];
	 }
	 
	 return $value;
	}
	
	public function getCrmParam(){
	 $sql="select * from crmparam";
	 return $this->ExecuteS($sql);
	}
	
	//Company
	public function newCompany(){
	    $nom=$this->postVars('nom');
		$desc=$this->postVars('desc');
		
		$sql="insert into `enterprise` (`e_nom`, `e_img`, `e_info`) values ('$nom','$img','$desc')";
		$this->Execute($sql);
		
		$id=$this->getLastID();
		
		$this->uploadLogoEnterprise($id);
	}
	
	public function uploadLogoEnterprise($id){
		
		$ok=$this->upLoadFileProccess('img', 'modules/crm/Logos');
		
		if($ok){
		 $sql="update enterprise set e_img='".$this->fname."' where e_id='$id'";
		 $this->Execute($sql);
		}
		
	}
	
	public function editCompany(){
	    $id=$this->postVars('id');
		$nom=$this->postVars('nom');
		$desc=$this->postVars('desc');
		
		$sql="update `enterprise` set `e_nom`='$nom', `e_info`='$desc' where e_id='$id'";
		$this->Execute($sql);
		
		$this->uploadLogoEnterprise($id);
	}
	
	public function delCompany(){
	 $id=$this->getVars('id');
	 $sql="delete from `enterprise` where e_id='$id'";
	 $this->Execute($sql);
	}
	
	public function getCompany(){
		$sql="select * from enterprise";
		return $this->ExecuteS($sql);
	}
	
	public function getCompanyById($id){
		$sql="select * from enterprise where e_id='$id'";
		return $this->ExecuteS($sql);
	}
	
	//Payment Terms
	public function newTerm(){
	 $nom=$this->postVars('nom');
     $desc=$this->postVars('desc');
	 
	  $sql="insert into `terms_payment` (`tp_nom`, `tp_descripcion`) values ('$nom','$desc')";
	  $this->Execute($sql);
	}
	
	public function editTerm(){
	 
	 $id=$this->postVars('id');
	 $nom=$this->postVars('nom');
     $desc=$this->postVars('desc');
	 
	  $sql="update `terms_payment` set `tp_nom`='$nom', `tp_descripcion`='$desc' where tp_id='$id'";
	 $this->Execute($sql);
	}
	
	public function delTerm(){
	 
	 $id=$this->getVars('id');
	 
	 $sql="delete from `terms_payment` where tp_id='$id'";
	 $this->Execute($sql);
	}
	
	public function getTerm(){
		 $sql="select * from `terms_payment`";
	 	 return $this->ExecuteS($sql);
	}
	
	public function getTermById($id){
	  $sql="select * from `terms_payment` where tp_id='$id'";
	  return $this->ExecuteS($sql);
	}
	
	//Users Commercials
	public function addCommercial(){
		$id=$this->postVars('u_id');
		$approver=$this->postVars('approver');
		$letdel=$this->postVars('letdel');
		$sql="update user set isCommercial='1',isApprover='$approver',crm_letAuth='$letdel'  where u_id='$id'";
		$this->Execute($sql);	
	}
	
	public function delCommercial(){
		$id=$this->getVars('id');
		$sql="update user set isCommercial='0' where u_id='$id'";
		$this->Execute($sql);
	}
	
	public function getCommercials(){
	 $sql="select * from user where isCommercial='1'";
	 return $this->ExecuteS($sql);
	}
	
	public function letCommercialDel($id_user)
	{
		$sql="SELECT * FROM user WHERE u_id='$id_user'";
		$row=$this->ExecuteS($sql);
		foreach($row as $row)
		{
			$letDel=$row['crm_letAuth'];
		}
		
		if($letDel==1)
		{
			return true;
		}else{
			return false;
		}
	}
	
	//Centro de costos
	public function newCenterCost(){
	 $nom=$this->postVars('nom');
     $track=$this->postVars('track');
	 
	  $sql="insert into `centercost` (`cc_nom`, `cc_track`) values ('$nom','$track')";
	  $this->Execute($sql);
	}
	
	public function editCenterCost(){
	 
	 $id=$this->postVars('id');
	 $nom=$this->postVars('nom');
     $track=$this->postVars('track');
	 
	  $sql="update `centercost` set `cc_nom`='$nom', `cc_track`='$track' where cc_id='$id'";
	 $this->Execute($sql);
	}
	
	public function delCenterCost(){
	 
	 $id=$this->getVars('id');
	 
	 $sql="delete from `centercost` where cc_id='$id'";
	 $this->Execute($sql);
	}
	
	public function getCenterCost(){
		 $sql="select * from `centercost`";
	 	 return $this->ExecuteS($sql);
	}
	
	public function getCenterCostById($id){
	  $sql="select * from `centercost` where cc_id='$id'";
	  return $this->ExecuteS($sql);
	}
	//Installers
   public function Checker(){
    return $this->chkTables('customer');
   }
   
   public function install(){
	$this->importSQLFile('modules/crm/sql/installer.sql');
	
	//Se instala los submenus
	$sql="select * from menu where m_folder='crm'";
	$row=$this->ExecuteS($sql);
	foreach($row as $row){
	 $m_id=$row["m_id"];
	}
	
	$sql="insert into `submenu` (`sm_nom`, `sm_link`, `m_id`) values ('Clientes','home.php?p=modules/crm/view/showCustomers.php','$m_id')";
	$this->Execute($sql);
	$sql="insert into `submenu` (`sm_nom`, `sm_link`, `m_id`) values ('Cotizaciones','home.php?p=modules/crm/view/showAllQuotes.php','$m_id')";
	$this->Execute($sql);
	$sql="insert into `submenu` (`sm_nom`, `sm_link`, `m_id`) values ('Pedidos','home.php?p=modules/crm/view/showAllOrders.php','$m_id')";
	$this->Execute($sql);
	$sql="insert into `submenu` (`sm_nom`, `sm_link`, `m_id`) values ('Mis Cotizaciones','home.php?p=modules/crm/view/showUserQuotes.php','$m_id')";
	$this->Execute($sql);
	$sql="insert into `submenu` (`sm_nom`, `sm_link`, `m_id`) values ('Mis Ordenes','home.php?p=modules/crm/view/showUserOrders.php','$m_id')";
	$this->Execute($sql);
	$sql="insert into `submenu` (`sm_nom`, `sm_link`, `m_id`) values ('Mis Clientes','home.php?p=modules/crm/view/showUserCustomers.php','$m_id')";
	$this->Execute($sql);
	$sql="insert into `submenu` (`sm_nom`, `sm_link`, `m_id`) values ('Setup','home.php?p=modules/crm/view/showSetup.php','$m_id')";
	$this->Execute($sql);
	
	$sql="ALTER TABLE `user` ADD `isCommercial` VARCHAR( 100 ) NOT NULL ;";
	$this->Execute($sql);
	
	$sql="ALTER TABLE `orders` ADD `o_aid` VARCHAR( 255 ) NOT NULL ,
ADD `o_anom` VARCHAR( 255 ) NOT NULL ;";
	$this->Execute($sql);
	
	$sql="ALTER TABLE `orders` ADD `o_cc` VARCHAR( 255 ) NOT NULL ;";
	$this->Execute($sql);
	
	$sql="INSERT INTO `crmparam` (`cp_id` ,`cp_name` ,`cp_value`) VALUES (NULL , 'Correo remitente', ''), (NULL , 'Correo seguimiento', ''), (NULL , 'Mensaje en correo', '');";
	$this->Execute($sql);
	
	
   }
   
   public function uninstall(){
    $this->importSQLFile('modules/crm/sql/uninstaller.sql');
	
	$sql="DROP TABLE `convenio`, `crmparam`, `customer`, `customer_clafisification`, `customer_contact`, `customer_history`, `customer_industry`, `customer_oportunities`, `customer_oportunities_categories`, `customer_oportunities_priority`, `customer_oportunities_status`, `customer_position`, `customer_profesion`, `enterprise`, `kpi_goal`, `kpi_item`, `orders`, `orders_detail`, `orders_detail_temp`, `pricelist`, `pricelistcategories`, `quote`, `quote_detail`, `quote_detail_temp`, `terms_payment`, `orders_history`, ` 	orders_state`";
	$this->Execute($sql);
	
	$sql="select * from menu where m_folder='crm'";
	$row=$this->ExecuteS($sql);
	foreach($row as $row){
	 $m_id=$row["m_id"]; 
	}
	
	$sql="select * from submenu as sm inner join pro_submenu as psm on sm.sm_id=psm.sm_id where sm.m_id='$m_id'";
	$row=$this->ExecuteS($sql);
	foreach($row as $row){
	 $sql1="delete from pro_submenu where sm_id='".$row["sm_id"]."'";
	 $this->Execute($sql);
	}
	
	$sql1="delete from submenu where m_id='$m_id'";
	$this->Execute($sql);
	
	$sql="ALTER TABLE `user` DROP `isCommercial";
	$this->Execute($sql);
	$sql="ALTER TABLE `user` DROP `isCommercial";
	$this->Execute($sql);
   }
   
   public function num2letras($num, $fem = true, $dec = true) { 
//if (strlen($num) > 14) die("El n?mero introducido es demasiado grande"); 
   $matuni[2]  = "dos"; 
   $matuni[3]  = "tres"; 
   $matuni[4]  = "cuatro"; 
   $matuni[5]  = "cinco"; 
   $matuni[6]  = "seis"; 
   $matuni[7]  = "siete"; 
   $matuni[8]  = "ocho"; 
   $matuni[9]  = "nueve"; 
   $matuni[10] = "diez"; 
   $matuni[11] = "once"; 
   $matuni[12] = "doce"; 
   $matuni[13] = "trece"; 
   $matuni[14] = "catorce"; 
   $matuni[15] = "quince"; 
   $matuni[16] = "dieciseis"; 
   $matuni[17] = "diecisiete"; 
   $matuni[18] = "dieciocho"; 
   $matuni[19] = "diecinueve"; 
   $matuni[20] = "veinte"; 
   $matunisub[2] = "dos"; 
   $matunisub[3] = "tres"; 
   $matunisub[4] = "cuatro"; 
   $matunisub[5] = "quin"; 
   $matunisub[6] = "seis"; 
   $matunisub[7] = "sete"; 
   $matunisub[8] = "ocho"; 
   $matunisub[9] = "nove"; 

   $matdec[2] = "veint"; 
   $matdec[3] = "treinta"; 
   $matdec[4] = "cuarenta"; 
   $matdec[5] = "cincuenta"; 
   $matdec[6] = "sesenta"; 
   $matdec[7] = "setenta"; 
   $matdec[8] = "ochenta"; 
   $matdec[9] = "noventa"; 
   $matsub[3]  = 'mill'; 
   $matsub[5]  = 'bill'; 
   $matsub[7]  = 'mill'; 
   $matsub[9]  = 'trill'; 
   $matsub[11] = 'mill'; 
   $matsub[13] = 'bill'; 
   $matsub[15] = 'mill'; 
   $matmil[4]  = 'millones'; 
   $matmil[6]  = 'billones'; 
   $matmil[7]  = 'de billones'; 
   $matmil[8]  = 'millones de billones'; 
   $matmil[10] = 'trillones'; 
   $matmil[11] = 'de trillones'; 
   $matmil[12] = 'millones de trillones'; 
   $matmil[13] = 'de trillones'; 
   $matmil[14] = 'billones de trillones'; 
   $matmil[15] = 'de billones de trillones'; 
   $matmil[16] = 'millones de billones de trillones'; 

   $num = trim((string)@$num); 
   if ($num[0] == '-') { 
      $neg = 'menos '; 
      $num = substr($num, 1); 
   }else 
      $neg = ''; 
   while ($num[0] == '0') $num = substr($num, 1); 
   if ($num[0] < '1' or $num[0] > 9) $num = '0' . $num; 
   $zeros = true; 
   $punt = false; 
   $ent = ''; 
   $fra = ''; 
   for ($c = 0; $c < strlen($num); $c++) { 
      $n = $num[$c]; 
      if (! (strpos(".,'''", $n) === false)) { 
         if ($punt) break; 
         else{ 
            $punt = true; 
            continue; 
         } 

      }elseif (! (strpos('0123456789', $n) === false)) { 
         if ($punt) { 
            if ($n != '0') $zeros = false; 
            $fra .= $n; 
         }else 

            $ent .= $n; 
      }else 

         break; 

   } 
   $ent = '     ' . $ent; 
   if ($dec and $fra and ! $zeros) { 
      $fin = ' coma'; 
      for ($n = 0; $n < strlen($fra); $n++) { 
         if (($s = $fra[$n]) == '0') 
            $fin .= ' cero'; 
         elseif ($s == '1') 
            $fin .= $fem ? ' una' : ' un'; 
         else 
            $fin .= ' ' . $matuni[$s]; 
      } 
   }else 
      $fin = ''; 
   if ((int)$ent === 0) return 'Cero ' . $fin; 
   $tex = ''; 
   $sub = 0; 
   $mils = 0; 
   $neutro = false; 
   while ( ($num = substr($ent, -3)) != '   ') { 
      $ent = substr($ent, 0, -3); 
      if (++$sub < 3 and $fem) { 
         $matuni[1] = 'una'; 
         $subcent = 'as'; 
      }else{ 
         $matuni[1] = $neutro ? 'un' : 'uno'; 
         $subcent = 'os'; 
      } 
      $t = ''; 
      $n2 = substr($num, 1); 
      if ($n2 == '00') { 
      }elseif ($n2 < 21) 
         $t = ' ' . $matuni[(int)$n2]; 
      elseif ($n2 < 30) { 
         $n3 = $num[2]; 
         if ($n3 != 0) $t = 'i' . $matuni[$n3]; 
         $n2 = $num[1]; 
         $t = ' ' . $matdec[$n2] . $t; 
      }else{ 
         $n3 = $num[2]; 
         if ($n3 != 0) $t = ' y ' . $matuni[$n3]; 
         $n2 = $num[1]; 
         $t = ' ' . $matdec[$n2] . $t; 
      } 
      $n = $num[0]; 
      if ($n == 1) { 
         $t = ' ciento' . $t; 
      }elseif ($n == 5){ 
         $t = ' ' . $matunisub[$n] . 'ient' . $subcent . $t; 
      }elseif ($n != 0){ 
         $t = ' ' . $matunisub[$n] . 'cient' . $subcent . $t; 

      } 
      if ($sub == 1) { 
      }elseif (! isset($matsub[$sub])) { 
         if ($num == 1) { 
            $t = ' mil'; 
         }elseif ($num > 1){ 
            $t .= ' mil'; 
         } 
      }elseif ($num == 1) { 
         $t .= ' ' . $matsub[$sub] . 'on'; 
      }elseif ($num > 1){ 
         $t .= ' ' . $matsub[$sub] . 'ones'; 
      }   
      if ($num == '000') $mils ++; 
      elseif ($mils != 0) { 
         if (isset($matmil[$sub])) $t .= ' ' . $matmil[$sub]; 
         $mils = 0; 
      } 
      $neutro = true; 
      $tex = $t . $tex; 
   } 
   $tex = $neg . substr($tex, 1) . $fin; 
   return ucfirst($tex); 
}
	
};
?>
