<?php
class customerHistoryModelGe extends GeCore{
	//Customer History
	public function newCustomerHistory(){
	 $kpi=$this->postVars('kpi');
	 $desc=$this->postVars('desc');
	 $id=$this->postVars('id');
	 $oportunitie=$this->postVars('oportunitie');
	 $apointment=$this->postVars('apointment');
	 $duration=$this->postVars('duration');
	 
	 $hoy=date('Y-m-d H:i:s');
	 
	 $sql="insert into customer_history(ch_date,ch_desc,ch_kpi,u_name,u_id,c_id,co_id,ch_apointment,ch_duration) values ('$hoy','$desc','$kpi','".$_SESSION["user_name"]."','".$_SESSION["user_id"]."','$id','$oportunitie','$apointment','$duration')";
	 
	 $this->Execute($sql);
	}
	
	public function getCustomerHistory(){
		$id=$this->getVars('id');
		$sql="select * from customer_history where c_id='$id'";
		return $this->ExecuteS($sql);
	} 
	
	public function getLastCustomerHistory(){
		$id=$this->getValue('id');
		$sql="select * from customer_history where c_id='$id' order by ch_date desc LIMIT 0,20";
		return $this->ExecuteS($sql);
	}
	
	//KPI
	public function newKpiItem(){
	 
	 $nom=$this->postVars('nom');
	 $por=$this->postVars('por');
	 $medicion=$this->postVars('medicion');
	 $showin=$this->postVars('showin');
	 
	 $sql="insert into `kpi_item` (`ki_nom`, `ki_por`, `ki_medicion`, `ki_del`, `ki_showin`) values ('$nom','$por','$medicion','1','$showin')";
	 $this->Execute($sql);
	}
	
	public function editKpiItem(){
		
		$id=$this->postVars('id');
		$nom=$this->postVars('nom');
	 	$por=$this->postVars('por');
	 	$medicion=$this->postVars('medicion');
		$showin=$this->postVars('showin');
		
		$sql="update `kpi_item` set `ki_nom`='$nom', `ki_por`='$por', `ki_medicion`='$medicion', `ki_showin`='$showin' where ki_id='$id'";
		$this->Execute($sql);
	}
	
	public function delKpiItem(){
		
		$id=$this->getVars('id');
		
		$sql="delete from `kpi_item` where ki_id='$id'";
		$this->Execute($sql);
	}
	
	public function getKpiItem(){
	 $sql="select * from kpi_item";
	 return $this->ExecuteS($sql);
	}
	
	public function getKpiItemByType($medicion){
	 $sql="select * from kpi_item where ki_medicion='$medicion'";
	 return $this->ExecuteS($sql);
	}
	
	public function getKpiItemById($id){
	 $sql="select * from kpi_item where ki_id='$id'";
	 return $this->ExecuteS($sql);
	}
	
	public function newKpiGoal($id_kpi,$y,$m,$value){
		$sql="insert into kpi_goal (kg_y,kg_m,ki_id,kg_value) values ('$y','$m','$id_kpi','$value')";
		$this->Execute($sql);
	}
	
	public function editKpiGoal($id,$id_kpi,$y,$m,$value){
		$sql="update kpi_goal set kg_y='$y',kg_m='$m',ki_id='$id_kpi',kg_value='$value' where kg_id='$id'";
		$this->Execute($sql);
	}
	
	public function delKpiGoal($id){
		$sql="delete from kpi_goal where kg_id='$id'";
		$this->Execute($sql);
	}
	
	public function getKpiGoal($y=NULL,$m=NULL){
	 $sql="select * from kpi_goal where kg_y='$y' and kg_m='$m'";
	 return $this->ExecuteS($sql);
	}
	
	public function getKpiGoalById($id){
	 $sql="select * from kpi_goal where kg_id='$id'";
	 return $this->ExecuteS($sql);
	}
	
	public function getKpiGoalValue($id,$m,$y){
	 $sql="select * from kpi_goal where ki_id='$id' and kg_m='$m' and kg_y='$y'";
	 $row=$this->ExecuteS($sql);
	 if(count($row)>0){
	 foreach($row as $row){
	  return $row["kg_value"];
	 }
	 }
	}
	
	public function getKpiByZone($zone){
	 $sql="select * from kpi_item where ki_showin='$zone'";
	 return $this->ExecuteS($sql);
	}
	
	public function getKpiName($id){
	 $sql="select * from kpi_item where ki_id='$id'";
	 $row=$this->ExecuteS($sql);
	 foreach($row as $row){
	  return $row["ki_nom"];
	 }
	}
	
	public function getCalcKpi($id_kpi,$user,$m,$y,$medicion){
	 
	 //Obtengo los datos del KPI
	 $row=$this->getKpiItemById($id_kpi);
	 foreach($row as $row){
	  $showin=$row["ki_showin"];
	  $med=$row["ki_medicion"];
	 }
	 
	 
	 if($showin==1){
	  if($med==2){
	  	$sql="select * from customer_history where u_id='$user' and ch_kpi='$id_kpi' and ch_date like '".$y."-".$m."%'";
	  	$row=$this->ExecuteS($sql);
	  	$num=$this->getNumRows();
	  }
	 }
	 
	 if($showin==2){
	  $sql="select * from quote where u_id='$user' and q_kpi='$id_kpi' and q_fecha like '".$y."-".$m."%'";
	  $row=$this->ExecuteS($sql);
	  if($med==2){
	  	$num=$this->getNumRows();
	   }else{
	    $num=0;
		if(count($row)>0){
		foreach($row as $row){
		 $num+=$row["q_subto"];
		}
		}
	   }
	   
	 }
	 
	 if($showin==3){
	  $sql="select * from orders_detail as od inner join orders as o on o.o_id=od.o_id where o.u_id='$user' and od.od_kpi='$id_kpi' and o.o_fecha like '".$y."-".$m."%'";
	  $row=$this->ExecuteS($sql);
	  if($med==2){
	  	$num=$this->getNumRows();
	  }else{
	  	$num=0;
		if(count($row)>0){
		foreach($row as $row){
		 $num+=$row["od_total"];
		}
		}
	  }
	  
	 }
	 
	 return $num;
	 
	}
	
	//Oportunities
	public function newOportunities(){
		
		$trackid=$this->postVars('trackid');
		$categoria=$this->postVars('category');
		$prioridad=$this->postVars('priority');
		$asunto=$this->postVars('subject');
		$desc=$this->postVars('desc');
		$ip=$this->postVars('ip');
		$status=$this->postVars('status');
		$c_id=$this->postVars('c_id');
		$price=$this->postVars('price');
		
		$hoy=date('Y-m-d H:i:s');
		
	  $sql="insert into `customer_oportunities` (`co_trackid`, `co_category`, `co_priority`, `co_subject`, `co_message`, `co_date_add`, `co_date_upd`, `co_ip`, `co_status`, `co_lastreplier`, `co_archive`, `co_attachments`, `c_id`, `u_id`, `co_price`) values ('$trackingID','$categoria','$prioridad','$asunto','$desc','$hoy','$hoy','$ip','$status','','','','$c_id','".$_SESSION["user_id"]."','$price')";
	  
	  	$this->Execute($sql);
	}
	
	public function editOportunities(){
		
		$id=$this->postVars('id');
		$trackid=$this->postVars('trackid');
		$categoria=$this->postVars('category');
		$prioridad=$this->postVars('priority');
		$asunto=$this->postVars('subject');
		$desc=$this->postVars('desc');
		$ip=$this->postVars('ip');
		$status=$this->postVars('status');
		$c_id=$this->postVars('c_id');
		$price=$this->postVars('price');
		
		$hoy=date('Y-m-d H:i:s');
		
	  $sql="update `customer_oportunities` set  `co_category`='$categoria', `co_priority`='$prioridad', `co_subject`='$asusnto', `co_message`='$desc',  `oc_date_upd`='$hoy', `co_ip`='$ip', `co_status`='$status', `co_lastreplier`='".$_SESSION["user_name"]."',u_id='".$_SESSION["user_id"]."', `co_price`='$price' where id='$id'";
	  
	  	$this->Execute($sql);
	}
	
	public function delOportunities(){
		
		$id=$this->getVars('id');
		
		$sql="delete from `customer_oportunities` where id='$id'";
	  
	  	$this->Execute($sql);
	}
	
	public function getOportunities(){
	 $sql="select * from customer_oportunities";
	 return $this->ExecuteS($sql);
	}
	
	public function getOportinitiesById($id){
	 $sql="select * from customer_oportunities where co_id='$id'";
	 return $this->ExecuteS($sql);
	}
	
	public function getOportinitiesByCustomer($id){
	 $sql="select * from customer_oportunities where c_id='$id'";
	 return $this->ExecuteS($sql);
	}
	
	//Priority
	public function newPriority(){
		$nom=$this->postVars('nom');
		$puntos=$this->postVars('puntos');
		$color=$this->postVars('color');
		
		$sql="insert into `customer_oportunities_priority` (`cop_nom`, `cop_color`) values ('$nom','$color')";
		$this->Execute($sql);
	}
	
	public function editPriority(){
	 	
		$id=$this->postVars('id');
		$nom=$this->postVars('nom');
		$puntos=$this->postVars('puntos');
		$color=$this->postVars('color');
		
		$sql="update `customer_oportunities_priority` set `cop_nom`='$nom', `cop_color`='$color' where cop_id='$id'";
		$this->Execute($sql);
	}
	
	public function delPriority(){
	 	
		$id=$this->getVars('id');
		
		$sql="delete from `customer_oportunities_priority` where cop_id='$id'";
		$this->Execute($sql);
	}
	
	public function getPriorities(){
	 $sql="select * from customer_oportunities_priority";
	 return $this->ExecuteS($sql);
	}
	
	public function getPrioritiesById(){
	 $sql="select * from customer_oportunities_priority where cop_id='$id'";
	 return $this->ExecuteS($sql);
	}
	
	//Categories
	public function newCategories(){
		$nom=$this->postVars('nom');
		
		$sql="insert into `customer_oportunities_categories` (`coc_nom`) values ('$nom')";
		$this->Execute($sql);
	}
	
	public function editCategories(){
	 	
		$id=$this->postVars('id');
		$nom=$this->postVars('nom');
		
		$sql="update `customer_oportunities_categories` set `coc_nom`='$nom' where coc_id='$id'";
		$this->Execute($sql);
	}
	
	public function delCategory(){
	 	
		$id=$this->getVars('id');
		
		$sql="delete from `customer_oportunities_category` where coc_id='$id'";
		$this->Execute($sql);
	}
	
	public function getCategories(){
	 $sql="select * from customer_oportunities_categories";
	 return $this->ExecuteS($sql);
	}
	
	public function getCategoriesById(){
	 $sql="select * from customer_oportunities_categories where coc_id='$id'";
	 return $this->ExecuteS($sql);
	}
	
	//Status
	public function newStatus(){
		$nom=$this->postVars('nom');
		$color=$this->postVars('color');
		
		$sql="insert into `customer_oportunities_status` (`cos_nom`,`cos_color`) values ('$nom','$color')";
		$this->Execute($sql);
	}
	
	public function editStatus(){
	 	
		$id=$this->postVars('id');
		$nom=$this->postVars('nom');
		$color=$this->postVars('color');
		
		$sql="update `customer_oportunities_status` set `cos_nom`='$nom',`cos_color`='$color' where cos_id='$id'";
		$this->Execute($sql);
	}
	
	public function delStatus(){
	 	
		$id=$this->getVars('id');
		
		$sql="delete from `customer_oportunities_status` where cos_id='$id'";
		$this->Execute($sql);
	}
	
	public function getStatus(){
	 $sql="select * from customer_oportunities_status";
	 return $this->ExecuteS($sql);
	}
	
	public function getStatusById(){
	 $sql="select * from customer_oportunities_status where cos_id='$id'";
	 return $this->ExecuteS($sql);
	}
};
?>