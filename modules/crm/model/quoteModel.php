<?php
 class quoteModelGe extends GeCore{
  //Quotes Methods ///////////////////////////////////////////////////
	public function newQuote($track){
	
	 $company=$this->postVars('company');
 	 $customer=$this->postVars('customer');
 	 $contact=$this->postVars('contact');
 	 $term=$this->postVars('term');
 	 $ini=$this->postVars('ini');
 	 $subto=$this->postVars('subto');
 	 $subtotalcop=$this->postVars('subtotalcop');
 	 $ivacop=$this->postVars('ivacop');
 	 $totalcop=$this->postVars('totalcop');
 	 $obs=$this->postVars('obs');
 	 $trm=$this->postVars('trm');
 	 $gestion=$this->postVars('gestion');
 	 $desc=$this->postVars('desc'); 
 	 $porgan=$this->postVars('porgan');
 	 $gan1=$this->postVars('gan1');
 	 $tipo=$this->postVars('tipo');
	 $kpi=$this->postVars('kpi');
	 
	 //$track=$this->getLastTrack();
	 $creacion=date('Y-m-d H:i:s');
	 
	 $top=$this->getParam(4);
	  
	  /*if($totalcop<$top){
	   $active=1;
	  }else{
	   $active=1;
	  }*/
	  
	  //Se modifica para que quede en estado de apronacion = 2
	  $active=2;
	 
	 $sql="insert into `quote` (`q_fecha`, `q_subto`, `q_iva`, `q_total`, `q_mod_pago`, `q_fecha_fin`, `u_id`, `e_id`, `q_trm`, `cc_id`, `q_subto_base`,`q_track`,`q_obs`,`q_porganancia`,`q_ganancia`,`q_tipo`,`q_active`,`c_id`,`q_desc`,`q_kpi`) values ('$creacion','$subtotalcop','$ivacop','$totalcop','$term','$ini','".$_SESSION["user_id"]."','$company','$trm','$contact','$subto','$track','$obs','$porgan','$gan1','$tipo','$active','$customer','$desc','$kpi')";
	 
	 
	  
	  $this->Execute($sql);
	  $idq=$this->getLastID();
	  
	  return $idq;
	}
	
	public function editQuote(){
	 
	 $id=$this->postVars('id');
	 $company=$this->postVars('company');
 	 $customer=$this->postVars('customer');
 	 $contact=$this->postVars('contact');
 	 $term=$this->postVars('term');
 	 $ini=$this->postVars('ini');
 	 $subto=$this->postVars('subto');
 	 $subtotalcop=$this->postVars('subtotalcop');
 	 $ivacop=$this->postVars('ivacop');
 	 $totalcop=$this->postVars('totalcop');
 	 $obs=$this->postVars('obs');
 	 $creacion=$this->postVars('creacion1');
 	 $trm=$this->postVars('trm');
 	 $gestion=$this->postVars('gestion');
 	 $desc=$this->postVars('desc'); 
 	 $porgan=$this->postVars('porgan');
 	 $gan1=$this->postVars('gan1');
 	 $tipo=$this->postVars('tipo');
	 $desc=$this->postVars('desc');
	 $kpi=$this->postVars('kpi');
	 $active=$this->postVars('active');
	 
	 $sql="update `quote` set `q_subto`='$subtotalcop',`q_iva`='$ivacop',`q_total`='$totalcop',`q_mod_pago`='$term',`q_fecha_fin`='$ini',`e_id`='$company',`q_trm`='$trm',`cc_id`='$contact',`q_subto_base`='$subto',`q_obs`='$obs',`q_porganancia`='$porgan',`q_ganancia`='$gan1',`q_tipo`='$tipo',`c_id`='$customer',`q_desc`='$desc',`q_kpi`='$kpi',`q_active`='$active' where q_id='$id'";
	 
	  $this->Execute($sql);
	  
	}
	
	public function delQuote(){
	 $id=$this->getVars('id');
	 $sql="update `quote` set `q_active`='0' where q_id='$id'";
	 $this->Execute($sql);
	 return "OK";
	}
	
	public function flushQuote(){
	 $sql="delete from `quote` where `q_active`='0'";
	 $this->Execute($sql);
	}
	
	public function getQuotes($act=1,$start=0){
	 $sql="select * from quote where q_active='$act' order by q_fecha desc LIMIT $start,".$this->tam_page;
	 return $this->ExecuteS($sql);
	}
	
	public function getTotalQuotes($act=1){
	 $sql="select * from quote where q_active='$act'";
	 $this->res=$this->Execute($sql);
	 return $this->getNumRows();
	}
		
	
	public function getQuotesByUser($act=1,$start=0,$id_user){
	 $sql="select * from quote where q_active='$act' and u_id='$id_user' order by q_fecha desc LIMIT $start,".$this->tam_page;
	 return $this->ExecuteS($sql);
	}
	
	public function getTotalQuotesByUser($act=1,$id_user){
	 $sql="select * from quote where q_active='$act' and u_id='$id_user'";
	 $this->res=$this->Execute($sql);
	 return $this->getNumRows();
	}
	
	public function getQuoteById($id){
	 $sql="select * from quote where q_id='$id'";
	 return $this->ExecuteS($sql);
	}
	
	public function getQuoteByTrack($id){
	 $sql="select * from quote where q_track='$id'";
	 return $this->ExecuteS($sql);
	}
	
	public function getQuoteByCustomerId($id){
	 $sql="select * from quote where c_id='$id' and q_active!='0'";
	 return $this->ExecuteS($sql);
	}
	
	public function editQuoteStatus($id,$status){
	 $sql="update `quote` set `q_active`='$status' where q_id='$id'";
	 $this->Execute($sql);
	}
	
	//Quote details Methods//////////////////////////////////////
	//Temportal Details
	public function newQuoteDetailItemTemp($id_product=NULL,$desc=NULL,$price=NULL,$qty=NULL,$trm=NULL,$id_currency=NULL){
	
	  //$this->getSession();
	  
	  if($trm!=1){
		$price1 = $price * $trm;  
	  	$total=$qty*$price1;
	  }else{
	  	$total=$qty*$price;
	  }
	  
	  
	  	
	  $sql="insert into `quote_detail_temp` (`u_id`, `id_product`, `qdt_desc`, `qdt_price`, `qdt_qty`, `qdt_total`, `qdt_trm`, `id_currency`) values ('".$_SESSION['user_id']."','$id_product','$desc','$price','$qty','$total','$trm','$id_currency')";
	  
	  $this->Execute($sql);
	  
	}
	
	public function editQuoteDetailItemTemp($id=NULL,$id_product=NULL,$desc=NULL,$price=NULL,$qty=NULL){
	
	  $total=$qty*$price;
	  $sql="update `quote_detail_temp` set `id_product`='$id_product', `qdt_desc`='$desc', `qdt_price`='$price', `qdt_qty`='$qty', `qdt_total`='$total' where qdt='$id'";
	  $this->Execute($sql);
	
	}
	
	public function delQuoteDetailItemTemp($id=NULL){
	  $sql="delete from `quote_detail_temp` where qdt_id='$id'";
	  $this->Execute($sql);
	}
	
	public function flushQuoteDetailItemTemp($id=NULL){
	  $sql="delete from `quote_detail_temp` where u_id='".$_SESSION["user_id"]."'";
	  $this->Execute($sql);
	}
	
	public function getQuoteDetailTemp(){
	 
	 $sql="select * from `quote_detail_temp` where u_id='".$_SESSION["user_id"]."'";
	 return $this->ExecuteS($sql);
	}
	
	public function getQuoteByFind($term){
	 $sql="select * from quote where q_track like '%$term%' and q_active='1'";
	 $row=$this->ExecuteS($sql);
	 
	 if(count($row)>0){
	 foreach($row as $row){
	 	$arr[]=$row["q_track"].":".$row["q_desc"];
	 }
	}
	
	return json_encode($arr);
	}
	
	public function getUserQuoteByFind($term,$id_user){
	 $sql="select * from quote where q_track like '%$term%' and u_id='$id_user' and q_active='1'";
	 $row=$this->ExecuteS($sql);
	 
	 if(count($row)>0){
	 foreach($row as $row){
	 	$arr[]=$row["q_track"].":".$row["q_desc"];
	 }
	}
	
	return json_encode($arr);
	}
	
	//Quote Details
	public function newQuoteDetailItem($id=NULL,$id_product=NULL,$desc=NULL,$price=NULL,$qty=NULL,$trm=NULL,$id_currency=NULL,$total=NULL){
		
	  	
	  $sql="insert into `quote_detail` (`q_id`, `id_product`, `qd_desc`, `qd_price`, `qd_qty`, `qd_total`, `qd_trm`, `id_currency`) values ('$id','$id_product','$desc','$price','$qty','$total','$trm','$id_currency')"; 
	  $this->Execute($sql);
	  
	}
	
	public function editQuoteDetailItem($id=NULL,$id_product=NULL,$desc=NULL,$price=NULL,$qty=NULL){
		
		$total=$price*$qty;
		 $sql="update `quote_detail` set `id_product`='$id_product', `qd_desc`='$desc', `qd_price`='$price', `qd_qty`='$qty', `qd_total`='$total' where qd_id='$id'";
	  
	  $this->Execute($sql);
	
	}
	
	public function delQuoteDetailItem($id=NULL){
	  $sql="delete from `quote_detail` where qd_id='$id'";
	  $this->Execute($sql);
	}
	
	public function getQuoteDetail($id){
	 $sql="select * from `quote_detail` where q_id='$id' order by qd_desc asc";
	 return $this->ExecuteS($sql);
	}
	
	public function getQuoteDetailByTrack($track){
	 $sql="select * from `quote` as q inner join `quote_detail` as dq on q.q_id=dq.q_id where q.q_track='$track'";
	 return $this->ExecuteS($sql);
	}
	
	
	//Quote Methods Setup //////////////////////////////////////////////////
	//Price List
	public function newPrice($fab=NULL,$ref=NULL,$desc=NULL,$precio=NULL,$subcat=NULL,$cat=NULL,$disp=NULL,$descr=NULL,$moneda=NULL,$exiva=NULL){
	 
	 $hoy=date('Y-m-d H:i:s');
	 
	 $sql="insert into `pricelist` (`pl_fabricante`, `pl_referencia`, `pl_descripcion`, `pl_precio`, `pl_subcat`, `pl_cat`,`pl_disponibilidad`, `pl_desc_rapida`, `pl_dolar`, `pl_last_fecha`, `u_nom`, `pl_noiva`, `pl_finder`) values ('$fab','$ref','$desc','$precio','$subcat','$cat','$disp','$descr','$moneda','$hoy','".$_SESSION["user_name"]."','$exiva','$descr')";
	  $this->Execute($sql);
	  
	}
	
	public function editPrice($id=NULL,$fab=NULL,$ref=NULL,$desc=NULL,$precio=NULL,$subcat=NULL,$cat=NULL,$disp=NULL,$descr=NULL,$moneda=NULL,$exiva=NULL){
	 
	 $hoy=date('Y-m-d H:i:s');
	 
	 $sql="update `pricelist` set `pl_fabricante`='$fab', `pl_referencia`='$ref', `pl_descripcion`='$desc', `pl_precio`='$precio', `pl_subcat`='$subcat', `pl_cat`='$cat',`pl_disponibilidad`='$disp', `pl_desc_rapida`='$descr', `pl_dolar`='$moneda', `pl_last_fecha`='$hoy', `u_nom`='".$_SESSION["user_name"]."', `pl_noiva`='$exiva', `pl_finder`='$descr' where pl_id='$id'";
	 
	 $this->Execute($sql);
	  
	}
	
	public function delPrice($id=NULL){
	 $sql="delete from `pricelist` where pl_id='$id'";
	 $this->Execute($sql);
	}
	
	public function flushPrice(){
	 $sql="delete from `pricelist`";
	 $this->Execute($sql);
	}
	
	public function getPriceList(){
	 $sql="select * from pricelist LIMIT 0,200";
	 return $this->ExecuteS($sql);
	}
	
	public function getPriceById($id){
	 $sql="select * from pricelist where pl_id='$id'";
	 return $this->ExecuteS($sql);
	}
	
	public function getPriceByFind($term){
	 $sql="select * from pricelist where pl_desc_rapida like '%$term%'";
	 $row=$this->ExecuteS($sql);
	 
	 /*if(count($row)>0){
	 foreach($row as $row){
	 	$arr[]=substr($row["pl_descripcion"],0,100).":".$row["pl_referencia"].":".$row["pl_id"];
	 }
	}
	
	return json_encode($arr);*/
	return $row;
	}
	
	public function uploadPriceList(){
		
		$ok=$this->upLoadFileProccess('pricelist', 'modules/crm/priceList');
		
		if($ok){
		 $f=fopen('modules/crm/priceList/'.$this->fname,'r');
		 while(!feof($f)){
		 	$line=fgets($f,2048);
			$data=explode(';',$line);
			
			$data[3]=str_replace(',','.',$data[3]);
			$row=$this->getCatByName(trim($data[5]));
			foreach($row as $row){
			 $id_cat=$row['plc_id'];
			}
			
			$this->newPrice($data[0],$data[1],trim($data[2]),$data[3],$data[4],$id_cat,$data[6],$data[7],$data[8],$data[9]);
		  
		 }
		}
		
	}
	
	public function newCat($nom,$por,$father=0){
	 $sql="insert into `pricelistcategories` (`plc_nom`,`plc_porventa`,`plc_father`) values ('$nom','$por','$father')";
	 $this->Execute($sql);
	}
	
	public function editCat($id,$nom,$por,$father=0){
	 $sql="update `pricelistcategories` set `plc_nom`='$nom',`plc_porventa`='$por',`plc_father`='$father' where plc_id='$id'";
	 $this->Execute($sql);
	}
	
	public function delCat($id){
	 $sql="delete from `pricelistcategories` where plc_id='$id'";
	 $this->Execute($sql);
	 
	 $sql="delete from `pricelistcategories` where plc_father='$id'";
	 $this->Execute($sql);
	}
	
	public function getCat(){
		$sql="select * from `pricelistcategories`";
		return $this->ExecuteS($sql);
	}
	
	public function getSubCat($id){
		$sql="select * from `pricelistcategories` where plc_father='$id'";
		return $this->ExecuteS($sql);
	}
	
	public function getCatById($id){
		$sql="select * from `pricelistcategories` where plc_id='$id'";
		return $this->ExecuteS($sql);
	}
	
	public function getCatByName($name){
		$sql="select * from `pricelistcategories` where plc_nom='$name'";
		return $this->ExecuteS($sql);
	}
	
	public function editCrmParam($id,$value){
	 $sql="update crmparam set cp_value='$value' where cp_id='$id'";
	 $this->Execute($sql);
	}
	
	//Quote Agreements
	public function newAgreements(){
		$nom=$this->postVars('nom');
		$por=$this->postVars('por');
		$desc=$this->postVars('desc');
		
		$sql="insert into `convenio` (`co_nom`, `co_por`, `co_descripcion`) values ('$nom','$por','$desc')";
		$this->Execute($sql);
	}
	
	public function editAgreements(){
		
		$id=$this->postVars('id');
		$nom=$this->postVars('nom');
		$por=$this->postVars('por');
		$desc=$this->postVars('desc');
		
		 $sql="update `convenio` set `co_nom`='$nom',`co_descripcion`='$desc',`co_por`='$por' where co_id='$id'";
		$this->Execute($sql);
	}
	
	public function delAgreements(){
		 $id=$this->getVars('id');
		 $sql="delete from `convenio` where co_id='$id'";
		 $this->Execute($sql);
	}
	
	public function getAgreements(){
	 $sql="select * from convenio order by co_nom";
	 return $this->ExecuteS($sql);
	}
	
	public function getAgreementsById($id){
	 $sql="select * from convenio where co_id='$id'";
	 return $this->ExecuteS($sql);
	}
	
	public function getParam($id){
	 $sql="select * from crmparam where cp_id='$id'";
	 $row=$this->ExecuteS($sql);
	 foreach($row as $row){
	 	$track=$row["cp_value"];
	 }
	 
	 return $track;
	}
	
	public function editParam($id){
	 $sql="select * from crmparam where cp_id='$id'";
	 $row=$this->ExecuteS($sql);
	 foreach($row as $row){
	 	$track=$row["cp_value"];
	 }
	 
	 $track++;
	 
	 $sql="update crmparam set cp_value='$track' where cp_id='$id'";
	 $this->Execute($sql);
	}
	
	public function calcPrice($precio,$trm=1,$por_convenio=0,$por_margen){
	 
	 if($por_margen=='')
  	 {
   		$por_margen=30;
  	 }
	 
	 
	  //$por=$por_margen+$por_convenio+1;
  	  $por=$por_margen+$por_convenio;
	  $price1=$por/100;
	 
	  $price1=1-$price1;
	  
	  $price=$precio/$price1;
	  
	  $price*=$trm;
  	  //$price*=2000.00;
      
      return $price;
	}
	
	public function trm(){
		
		//$url="http://www.banrep.gov.co/";
		$url="http://www.superfinanciera.gov.co/Cifras/informacion/diarios/tcrm/tcrm.htm";
		$ch = curl_init();
		
		curl_setopt ($ch, CURLOPT_URL,$url);
		curl_setopt ($ch, CURLOPT_HEADER, 0);
		curl_setopt ($ch, CURLOPT_RETURNTRANSFER, 1);
		$page = trim(curl_exec($ch));
		
		//echo $page;
		$page=strip_tags($page);
		$pos1=strpos($page,'Tasa de Cambio Representativa del Mercado');
		
		
		$pos2=strpos($page,'Pulse aqu� para copiar');
		
		$puntuacion=substr($page,1157,20);
		
		$puntuacion=trim($puntuacion);
		$puntuacion=str_replace(',','',$puntuacion);
		return $puntuacion;
	}
	
	
 };
?>
