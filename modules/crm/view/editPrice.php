<?php
 include('includes.php');
 
 if($_POST){
   $quoteCtrlObj->_init('EditPrice');
  $msg=true;
 }
 
 $row=$quoteCtrlObj->_init('GetPriceById');
 if(count($row)>0){
  foreach($row as $row){
  	$id=$row["pl_id"];
	$fab=$row["pl_fabricante"];
	$ref=$row["pl_referencia"];
	$desc=$row["pl_descripcion"];
	$precio=$row["pl_precio"];
	$cat=$row["pl_cat"];
	$disp=$row["pl_disponibilidad"];
	$descr=$row["pl_desc_rapida"];
	$dolar=$row["pl_dolar"];
	$noiva=$row["pl_noiva"];
  }
 }

?>
<div class="widget3">
 <div class="widgetlegend">Editar Precio en la lista </div>
 <?php
  if($msg)
  {
  ?>
   <div class="ui-widget">
	<div class="ui-state-highlight ui-corner-all" style="margin-top: 20px; padding: 0 .7em;">
		<p><span class="ui-icon ui-icon-info" style="float: left; margin-right: .3em;"></span>
		<strong>Felicitaciones!</strong> se ha sido creado satisfactoriamente.</p>
	</div>
</div>
  <?php
  }
 ?>
<p style="width:100">
	<a href="<?php $_SERVER['PHP_SELF'];?>?p=modules/crm/view/showPrices.php" class="btn_normal" style="float:left; margin:5px;">Volver </a></p>
 
<form action="#" method="post" enctype="multipart/form-data" name="form1" id="form1">
<table width="803" height="48" border="0" style="float:left">
  <tr>
    <td><label>Fabricante: </label><br />
      <input name="fab" type="text" id="fab" value="<?php echo $fab; ?>" />    </td>
  </tr>
  <tr>
    <td><label>Referencia: </label>
      <br />
      <input name="ref" type="text" id="ref" value="<?php echo $ref; ?>" /></td>
  </tr>
  <tr>
    <td><label>Precio: </label>
      <br />
      <input name="precio" type="text" id="precio" value="<?php echo $precio; ?>" /></td>
  </tr>
  <tr>
    <td><label>Descripcion: </label>
      <br />
      <textarea name="desc" cols="50" rows="5" id="desc"><?php echo $desc; ?></textarea></td>
  </tr>
  <tr>
    <td><label>Grupo de Margen<br />
    </label>
      <select name="cat" id="cat">
	   <option value="">--Seleccionar--</option>
	   <?php 
	    $row=$quoteCtrlObj->_init('getCategory');
		if(count($row)){
		 foreach($row as $row){
		  if($cat==$row["plc_id"]){
		  ?>
		 	<option value="<?php echo $row["plc_id"];?>" selected="selected"><?php echo $row["plc_nom"];?></option>
		 <?php
		  }else{
		 ?>
		 	<option value="<?php echo $row["plc_id"];?>"><?php echo $row["plc_nom"];?></option>
		 <?php
		  }	
		 }
		}
	   ?>
      </select></td>
  </tr>
  <tr>
    <td><label>Disponibilidad: </label>
      <br />
      <input name="disp" type="text" id="disp" value="<?php echo $disp; ?>" /></td>
  </tr>
  <tr>
    <td><label>Descripcion Corta (para buscador): </label>
      <br />
      <textarea name="descr" cols="50" rows="5" id="descr"><?php echo $descr; ?></textarea></td>
  </tr>
  <tr>
    <td><label>Moneda<br />
    </label>
      <select name="moneda" id="moneda">
	    <?php 
		 if($dolar==0){
		 ?>
		  	<option value="0" selected="selected">COP Pesos Colombianos</option>
            <option value="1">DOLAR Estados Unidos</option>
		 <?php
		 }else{
		 ?>
		  <option value="0">COP Pesos Colombianos</option>
          <option value="1" selected="selected">DOLAR Estados Unidos</option>
		 <?php
		 }
		?>
        
      </select></td>
  </tr>
  <tr>
    <td><label>Excento<br />
    </label>
      <label>
	  <?php
	   if($noiva==1){
	   ?>
	  	<input name="exiva" type="checkbox" id="exiva" value="1" checked="checked" /> 
	   <?php
	   }else{
	    ?>
		 <input name="exiva" type="checkbox" id="exiva" value="1" />
		<?php
	   }
	  ?>
      
      </label></td>
  </tr>
  <tr>
    <td><a href="javascript:;" class="btn_normal" onclick="document.form1.submit();">Guardar
      <input name="id" type="hidden" id="id" value="<?php echo $id; ?>" />
    </a></td>
  </tr>
</table>
</form>


</div>
