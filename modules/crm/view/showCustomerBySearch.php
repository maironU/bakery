<?php
 include('includes_self.php');
 
 $crmModelObj->connect();
 $customerModelObj->connect();
 
  //Se verifica si el modulo esta instalado
 if(!$crmModelObj->Checker()){
  $ins=true;
 }
 
 $msg=false;
 
 //Instala el modulo
 if($crmModelObj->getVars('install')){
  $crmModelObj->install();
  $msgIns=true;
 }
 
 //Actions
 if($_POST){
  $finder=$crmModelObj->postVars('finder');
  $c=explode(':',$finder);
  $_SESSION["id_customer"]=$c[1];
  $_SESSION["customer_name"]=$c[0];
 }
 
?>
<script type="text/javascript" src="../js/jquery-1.10.1.js"></script>
<script type="text/javascript" src="../js/jquery-ui-1.9.2.custom.js"></script>
<script type="text/javascript" src="../js/jsCrmFunctions.js"></script>
<link href="../css/jquery-ui.css" rel="stylesheet" type="text/css" />
<link type="text/css" href="../css/crmStyles.css" rel="stylesheet"  />
<div class="widget3">
 <div class="widgetlegend">GE &reg; CRM - Clientes </div>
 <?php
  if($msg)
  {
  ?>
   <div class="ui-widget">
	<div class="ui-state-highlight ui-corner-all" style="margin-top: 20px; padding: 0 .7em;">
		<p><span class="ui-icon ui-icon-info" style="float: left; margin-right: .3em;"></span>
		<strong>Felicitaciones!</strong> Se ha sido eliminado satisfactoriamente.</p>
	</div>
</div>
  <?php
  }
 ?>
<p style="width:100">
	<a href="<?php $_SERVER['PHP_SELF'];?>?p=modules/crm/view/newCustomer.php" class="btn_normal" style="float:left; margin:5px;">Nuevo Cliente </a>
</p><br />
<br />
<br />

<form action="" method="post" name="frmSearch">
<table width="100%" border="0">
  <tr>
    <td width="6%">Buscar</td>
    <td width="20%"><label>
      <input type="text" name="finder" id="finder" nkeyup="myAutoComplete('finder','modules/crm/view/findCustomer.php')" onkeydown="myAutoComplete('finder','modules/crm/view/findCustomer.php')"/>
      <input name="action" type="hidden" id="action" value="FindCustomer" />
    </label></td>
    <td width="74%"><a href="javascript:;" class="btn_1" onclick="document.frmSearch.submit();">Seleccionar</a></td>
  </tr>
</table>
</form>

<br />
</div>