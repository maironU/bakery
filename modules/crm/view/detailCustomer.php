<?php
 include('includes.php');
 
 $crmModelObj->connect();
 
  //Se verifica si el modulo esta instalado
 if(!$crmModelObj->Checker()){
  $ins=true;
 }
 
 $msg=false;
 
 //Instala el modulo
 if($crmModelObj->getVars('install')){
  $crmModelObj->install();
  $msgIns=true;
 }
 
//Actions
 if($_POST){
 $action=$crmModelObj->postVars('action');
 if(!empty($action)){
	 $res=$customerCtrlObj->_init($action);
 }
}

$action=$crmModelObj->getVars('action');
 if(!empty($action)){
	 $res=$customerCtrlObj->_init($action);
 }
 


//Get the customer inf
$row=$res=$customerCtrlObj->_init('getCustomerById');
if(count($row)>0){
 foreach($row as $row){
 	$nom=$row["c_nom"];
	$nit=$row["c_nit"];
	$dir=$row["c_dir"];
	$tel=$row["c_tel"];
	$web=$row["c_web"];
	$ti_id=$row["ti_id"];
	$ccl_id=$row["ccl_id"];
	$id=$row["c_id"];
 }
}
$nit=explode('-',$nit);

?>
<link type="text/css" href="modules/crm/css/crmStyles.css" rel="stylesheet"  />
<!-- Add mousewheel plugin (this is optional) -->
<script type="text/javascript" src="modules/crm/js/jquery.mousewheel-3.0.6.pack.js"></script>
<!-- Add fancyBox main JS and CSS files -->
<script type="text/javascript" src="modules/crm/js/jquery.fancybox.js?v=2.1.4"></script>
<link rel="stylesheet" type="text/css" href="modules/crm/css/jquery.fancybox.css?v=2.1.4" media="screen" />
<style type="text/css">
<!--
.Estilo1 {font-weight: bold}
-->
</style>
<div class="widget3">
 <div class="widgetlegend">GE &reg; CRM - Clientes </div>
 <?php
  if($res=='Yes')
  {
  ?>
   <div class="ui-widget">
	<div class="ui-state-highlight ui-corner-all" style="margin-top: 20px; padding: 0 .7em;">
		<p><span class="ui-icon ui-icon-info" style="float: left; margin-right: .3em;"></span>
		<strong>Felicitaciones!</strong> El cliente y contacto se han creado satisfactoriamente.</p>
	</div>
</div>
  <?
  }elseif($res=='No Contact'){
  ?>
  
  <?php
  }elseif($res=='No Customer'){
  ?>
  
  <?php
  }
 ?>
<p style="width:100">
	<a href="<?php $_SERVER['PHP_SELF'];?>?p=modules/crm/view/showCustomers.php" class="btn_normal" style="float:left; margin:5px;">Volver </a>
	<a href="<?php $_SERVER['PHP_SELF'];?>?p=modules/crm/view/newQuote.php&id=<?php echo $id;?>" class="btn_normal" style="float:left; margin:5px;">Nueva Cotización </a>
	<a href="<?php $_SERVER['PHP_SELF'];?>?p=modules/crm/view/newOrder.php&id=<?php echo $id;?>" class="btn_normal" style="float:left; margin:5px;">Nuevo Pedido </a>
	<a href="javascript:;" onclick="showInfo(<?php echo $id;?>,'modules/crm/view/CustomerHistory.php')" class="btn_normal" style="float:left; margin:5px;">Historial </a>
    <!-- Adicionales -->
    <?php
	 if($crmModelObj->chkTables('advances')){
	?>
	 <a href="<?php $_SERVER['PHP_SELF'];?>?p=modules/advances/view/newAdvances.php&id=<?php echo $id;?>" class="btn_normal" style="float:left; margin:5px;">Nuevo Anticipo </a>
	<?php
	 }
	?>
</p><br />
<br />
<br />

<form name="form1" method="post" action="#"><table width="1070" border="0">
  <tr>
    <th width="446"><div align="left">Informacion de cliente </div></th>
    <th width="459"><div align="left">Contacto</div></th>
  </tr>
  <tr>
    <td><table width="384" border="0" align="center">
      <tr>
        <td width="136">Nombre:</td>
        <td width="238"><label><?php echo $nom; ?>        </label></td>
      </tr>
      <tr>
        <td>Identificaci&oacute;n:</td>
        <td><?php echo $nit[0]; ?> 
          - 
            <label><?php echo $nit[1]; ?> </label></td>
      </tr>
      <tr>
        <td>Direcci&oacute;n:</td>
        <td><?php echo $dir; ?></td>
      </tr>
      <tr>
        <td>Tel&eacute;fono:</td>
        <td><?php echo $tel; ?></td>
      </tr>
      <tr>
        <td>Sitio Web: </td>
        <td><?php echo $web; ?></td>
      </tr>
      <tr>
        <td>Tipo de Cliente: </td>
        <td> <?php
		    $customerCtrlObj->connect();
		    $row=$customerCtrlObj->_init('GetClasification');
 			if(count($row)>0){
  				foreach($row as $row){
				if($ccl_id==$row["ccl_id"]){
				 echo $row["ccl_nom"];
				}
			 }
			}
		   ?></td>
      </tr>
      <tr>
        <td>Industria:</td>
        <td> <?php
		    $row=$customerCtrlObj->_init('getIndustry');
 			if(count($row)>0){
  				foreach($row as $row){
				if($ti_id==$row["ci_id"]){
					 echo $row["ci_nom"];
				}
				
			 }
			}
		   ?></td>
      </tr>
      <tr>
        <td>&nbsp;</td>
        <td>&nbsp;</td>
      </tr>
    </table></td>
    <td valign="top"><table width="506" border="0">

	  <tr>
        <th width="114">Nombre</th>
        <th width="81">Email</th>
        <th width="124">Telefono</th>
		<th width="124">Cel</th>
        </tr>
	  <?php
	   $rowe=$customerCtrlObj->_init('GetContact');
	   if(count($rowe)>0){
	    foreach($rowe as $row){
		?>
		<tr>
			<td><?php echo $row["cc_nom"];?></td>
			<td><?php echo $row["cc_mail"];?></td>
			<td><?php echo $row["cc_telefono"];?></td>
			<td><?php echo $row["cc_cel"];?></td>
			</tr>
		<?php
		}
	   }
	  ?>
     
    </table></td>
  </tr>
  <tr>
    <td><div align="right">
      <input name="action" type="hidden" id="action" value="EditCustomer" />
      <input name="id" type="hidden" id="id" value="<?php echo $id; ?>" />
    <td>&nbsp;</td>
  </tr>
</table>
  <br />
  <br />
  <table width="1073" border="0">
    <tr>
      <th colspan="2"><div align="left">Cotizaciones</div>        <div align="left"></div></th>
      </tr>
    <tr>
      <td colspan="2"><table width="1065" height="48" border="0">
        <tr>
          <th width="83" scope="col">Track</th>
          <th width="83" scope="col">Aprobacion</th>
		  <th width="196" scope="col">Fecha creacion </th>
		  <th width="196" scope="col">Descripcion </th>
          <th width="216" scope="col">Total</th>
          <th width="301" scope="col">Fecha Vencimiento </th>
          <th width="100" scope="col">Creado por </th>
          <th colspan="4" scope="col">Acciones</th>
        </tr>
		<?php
		 $row=$quoteCtrlObj->_init('GetQuoteByCustomerId');
		 
		 if(count($row)>0){
		 	foreach($row as $row){
		?>
        <tr>
          <td><?php echo $row["q_track"];?></td>
          <td><?php 
		  if($row["q_active"]==1)
		  	echo "Aprobado";
		  else
		  	echo "Sin aprobar";
		  ?></td>
          <td><?php echo $row["q_fecha"];?></td>
		  <td><?php echo $row["q_desc"];?></td>
          <td>$<?php echo number_format($row["q_total"]);?></td>
          <td><?php echo $row["q_fecha_fin"];?></td>
          <td><?php
			 $user=new User();
			 $user->connect();
			 $user->getUserById($row["u_id"]);
			 echo $user->name;
			?></td>
			

			 	<td width="36"><a href="<?php echo $_SERVER['PHP_SELF'];?>?p=modules/crm/view/editQuote.php&id=<?php echo $row["q_id"];?>" class="btn_1">Editar</a></td>
				<?php
	 	if($crmModelObj->letCommercialDel($_SESSION['user_id'])){
	 ?>
          		<td width="48"><a href="javascript:;" onclick="showInfo(<?php echo $row["q_id"];?>,'modules/crm/view/delQuote.php')" class="btn_2">Eliminar</a></td>
				<?php } ?>
          		<td width="24"><a href="javascript:;" class="btn_3" onclick="showInfo(<?php echo $row["q_id"];?>,'modules/crm/view/printQuote.php')">Imprimir</a></td>
          		<td width="25"><a href="javascript:;" class="btn_4" onclick="showInfo(<?php echo $row["q_id"];?>,'modules/crm/view/showQuote.php')">Ver</a></td>
          
        </tr>
		<?php
			}
		 }
		?>
      </table></td>
      </tr>
    <tr>
      <td width="446"><div align="right"></td>
      <td width="459">&nbsp;</td>
    </tr>
  </table>
  <br />
  <br />
  <table width="1073" border="0">
    <tr>
      <th colspan="2"><div align="left">Pedidos</div>
          <div align="left"></div></th>
    </tr>
    <tr>
      <td colspan="2">
      <table width="1065" height="48" border="0">
          <tr>
            <th width="65" scope="col">Track</th>
            <th width="141" scope="col">Fecha creacion </th>
            <th width="151" scope="col">Descripcion </th>
            <th width="114" scope="col">Total</th>
            <th width="135" scope="col">Fecha Vencimiento </th>
            <th width="100" scope="col">Creado por </th>
            <th width="91" scope="col">Estado</th>
            <th colspan="6" scope="col">Acciones</th>
          </tr>
          <?php
		 $row=$orderCtrlObj->_init('GetOrderByCustomer');
		 if(count($row)>0){
		 	foreach($row as $row){
		?>
          <tr>
            <td><?php echo $row["o_track"];?></td>
            <td><?php echo $row["o_fecha"];?></td>
            <td><?php echo $row["o_desc"];?></td>
            <td>$<?php echo number_format($row["o_total"],2);?></td>
            <td><?php echo $row["o_fecha_fin"];?></td>
            <td><?php
			 $user=new User();
			 $user->connect();
			 $user->getUserById($row["u_id"]);
			 echo $user->name;
			?></td>
            <td><?php
			 $orderModelObj->connect();
             $row1=$orderModelObj->getLastOrderHistory($row["o_id"]);
			 if(count($row1)>0){
			 	foreach($row1 as $row1){
				 ?>
                 <span style="width:100px; height:25px; display:block; color:#000; background-color:#<?php echo $row1["oh_color"]?>; padding:4px;"><?php echo $row1["oh_name"]?></span>
                 <?php
				}
			 }
			?></td>
				<td width="52"><a href="<?php echo $_SERVER['PHP_SELF'];?>?p=modules/crm/view/editOrder.php&id=<?php echo $row["o_id"];?>" class="btn_3" >Editar</a></td>
				<td width="48"><a href="javascript:;" onclick="showInfo(<?php echo $row["o_id"];?>,'modules/crm/view/OrderStates.php')" class="btn_1">Estados</a></td>
				<?php
	 	if($crmModelObj->letCommercialDel($_SESSION['user_id'])){
	 ?>
            	<td width="44"><a href="javascript:;" onclick="showInfo(<?php echo $row["o_id"];?>,'modules/crm/view/delOrder.php')" class="btn_2">Anular</a></td>
				<?php } ?>
            	<td width="51"><a href="javascript:;" class="btn_3" onclick="showInfo(<?php echo $row["o_id"];?>,'modules/crm/view/printOrder.php')">Imprimir</a></td>
            	<td width="23"><a href="javascript:;" class="btn_4" onclick="showInfo(<?php echo $row["o_id"];?>,'modules/crm/view/showOrder.php')">Ver</a></td>
				
				<?php
				 if($crmModelObj->chkTables('track')){
				  ?>
				  <td width="52"><a href="<?php echo $_SERVER['PHP_SELF'];?>?p=modules/tracker/view/showHistory.php&id_order=<?php echo $row["o_id"];?>" class="btn_3" >Rastreo</a></td>
				  <?php
				 }
				?>
            
          </tr>
          <?php
			}
		 }
		?>
      </table></td>
    </tr>
    <tr>
      <td width="446"><div align="right"></td>
      <td width="459">&nbsp;</td>
    </tr>
  </table>
  <br />
  <br />
  <?php
     //Adicionales
	 if($crmModelObj->chkTables('advances')){
		include('modules/advances/view/showCustomerAdvances.php');
	 }
	?>
</form>
</div>
<script>
 function showInfo(id,site)
 {
	$.fancybox.open({
					href : site+'?id='+id,
					type : 'iframe',
					padding:5
				}); 
 }
</script>

