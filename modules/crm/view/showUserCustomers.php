<?php
 include('includes.php');
 
 $crmModelObj->connect();
 $customerModelObj->connect();
 
  //Se verifica si el modulo esta instalado
 if(!$crmModelObj->Checker()){
  $ins=true;
 }
 
 $msg=false;
 
 //Instala el modulo
 if($crmModelObj->getVars('install')){
  $crmModelObj->install();
  $msgIns=true;
 }
 
 //Actions
 $action=$crmModelObj->getVars('action');
 if(!empty($action)){
	 $customerCtrlObj->_init($action);
 }
 
?>
<script type="text/javascript" src="modules/crm//js/jquery-1.10.1.js"></script>
<script type="text/javascript" src="modules/crm//js/jquery-ui-1.9.2.custom.js"></script>
<script type="text/javascript" src="modules/crm//js/jsCrmFunctions.js"></script>
<link href="modules/crm//css/jquery-ui.css" rel="stylesheet" type="text/css" />
<link type="text/css" href="modules/crm/css/crmStyles.css" rel="stylesheet"  />
<div class="widget3">
 <div class="widgetlegend">GE &reg; CRM - Clientes </div>
 <?php
  if($msg)
  {
  ?>
   <div class="ui-widget">
	<div class="ui-state-highlight ui-corner-all" style="margin-top: 20px; padding: 0 .7em;">
		<p><span class="ui-icon ui-icon-info" style="float: left; margin-right: .3em;"></span>
		<strong>Felicitaciones!</strong> Se ha sido eliminado satisfactoriamente.</p>
	</div>
</div>
  <?php
  }
 ?>
<p style="width:100">
	<a href="<?php $_SERVER['PHP_SELF'];?>?p=modules/crm/view/newCustomer.php" class="btn_normal" style="float:left; margin:5px;">Nuevo Cliente </a>
</p><br />
<br />
<br />

<form action="" method="post" name="frmSearch">
<table width="100%" border="0">
  <tr>
    <td width="6%">Buscar</td>
    <td width="20%"><label>
      <input type="text" name="finder" id="finder" nkeyup="myAutoComplete('finder','modules/crm/view/findUserCustomer.php')" onkeydown="myAutoComplete('finder','modules/crm/view/findUserCustomer.php')"/>
      <input name="action" type="hidden" id="action" value="FindCustomer" />
    </label></td>
    <td width="74%"><a href="javascript:;" class="btn_1" onclick="document.frmSearch.submit();">Buscar</a></td>
  </tr>
</table>
</form>

<br />
<table width="100%" border="0">
<tr>
 <th>ID</th>
 <th>Nombre</th>
 <th>Identificacion</th>
 <th>Direccion</th>
 <th>Telefono</th>
 <th>Ejecutivo</th>
 <th colspan="3">Actions</th>
</tr>
<?php
 $action=$crmModelObj->postVars('action');
 if($action=='FindCustomer'){
 	$row=$customerCtrlObj->_init($action);
 }else{
 	$row=$customerCtrlObj->_init('GetUserCustomer');
 }
 if(count($row)>0){
  foreach($row as $row){
   ?>
   <tr>
	 <td><?php echo $row["c_id"];?></td>
	 <td><?php echo $row["c_nom"];?></td>
	 <td><?php echo $row["c_nit"];?></td>
	 <td><?php echo $row["c_dir"];?></td>
	 <td><?php echo $row["c_tel"];?></td>
	 <td><?php echo $row["c_id"];?></td>
	 
	 <?php
	  $customerCtrlObj->getSession();
	  if($_SESSION["user_permisions"]==1){
	  ?>
	   	<td><div align="center"><a href="<?php $_SERVER['PHP_SELF'];?>?id=<? echo $row["c_id"]; ?>&p=modules/crm/view/editCustomer.php" class="btn_1">Editar</a></div></td>
	 	<td><div align="center"><a href="<?php $_SERVER['PHP_SELF'];?>?id=<? echo $row["c_id"]; ?>&p=<?php echo $crmModelObj->getVars('p');?>&action=delCustomer" class="btn_2">Eliminar</a></div></td>
	 	<td><div align="center"><a href="<?php $_SERVER['PHP_SELF'];?>?id=<? echo $row["c_id"]; ?>&p=modules/crm/view/detailCustomer.php" class="btn_3">Mas</a></div></td>
	  <?php
	  }else{
	  ?>
	   	<td><div align="center"><a href="<?php $_SERVER['PHP_SELF'];?>?id=<? echo $row["c_id"]; ?>&p=modules/crm/view/editCustomer.php" class="btn_1">Editar</a></div></td>
	 	<td><div align="center">&nbsp;</div></td>
	 	<td><div align="center"><a href="<?php $_SERVER['PHP_SELF'];?>?id=<? echo $row["c_id"]; ?>&p=modules/crm/view/detailCustomer.php" class="btn_3">Mas</a></div></td>
	  <?php
	  }
	 ?>
   </tr>
   <?php
  }
 }
?>
 <tr>
     <td colspan="12"><?php
	    $customerModelObj->startPage($customerModelObj->postVars('page'));
		$num=$customerModelObj->getTotalUserCustomer($_SESSION["user_id"]);
		echo $customerModelObj->getPages($num);
	   ?></td>
    </tr>
</table>

</div>