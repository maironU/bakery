<?php
 include('includes.php');
 
 $crmModelObj->connect();
 
 $msg=false;
 
 $id=$crmModelObj->getVars('id'); //ID dof Quote
 
 //Actions
 if($_POST){
  $quoteCtrlObj->_init('EditQuote');
 }
 
 $row=$quoteCtrlObj->_init('GetQuoteById');
 foreach($row as $row){
 	
	$term=$row["q_mod_pago"];
	$vencimiento=$row["q_fecha_fin"];
	$company=$row["c_id"];
	$obs=$row["q_obs"];
	$trm=$row["q_trm"];
	$contact=$row["cc_id"];
	$enterprise=$row["e_id"];
	$desc=$row["q_desc"];
	$kpi=$row["q_kpi"];
	$active=$row["q_active"];
 }
 
 if($crmModelObj->getVars('action')=='DelQuoteDetailTemp'){
 	$id_item=$quoteModelObj->getVars('id_item');
	$quoteModelObj->connect();
	$quoteModelObj->delQuoteDetailItem($id_item);
 }
 
?>
<link type="text/css" href="modules/crm/css/crmStyles.css" rel="stylesheet"  />
<link href="modules/crm/css/smart_wizard_vertical.css" rel="stylesheet" type="text/css">
<!--<script type="text/javascript" src="modules/crm/js/jquery-2.0.0.min.js"></script>-->
<script type="text/javascript" src="modules/crm/js/jquery.smartWizard.js"></script>
<script type="text/javascript" src="modules/crm/js/jsCrmFunctions.js"></script>
<script type="text/javascript">
   
    $(document).ready(function(){
    	// Smart Wizard	
  		$('#wizard').smartWizard({transitionEffect:'slide'});
     
		});
</script>
 <div class="widget3">
 <div class="widgetlegend">GE &reg; Cotizaciones </div>
 <?php
  if($msg)
  {
  ?>
   <div class="ui-widget">
	<div class="ui-state-highlight ui-corner-all" style="margin-top: 20px; padding: 0 .7em;">
		<p><span class="ui-icon ui-icon-info" style="float: left; margin-right: .3em;"></span>
		<strong>Felicitaciones!</strong> El slide ha sido eliminado satisfactoriamente.</p>
	</div>
</div>
  <?php
  }
 ?>
<p style="width:100">
	<!--<a href="<?php $_SERVER['PHP_SELF'];?>?p=modules/sliders/view/newSlide.php" class="btn_normal" style="float:left; margin:5px;">Nuevo Slide </a>-->
</p>
 
<form name="formCtz" method="post" action=""> 
<table border="0">
	<tr>
		<td>
			<!-- Tabs -->
  			<div id="wizard" class="swMain">
  			<ul>
				<li><a href="#step-1">
                <label class="stepNumber">1</label>
                <span class="stepDesc">
                   Paso 1<br />
                   <small>Seleccione los Item de su cotizaci�n</small>
                </span>
            </a></li>
  				<li><a href="#step-2">
                <label class="stepNumber">2</label>
                <span class="stepDesc">
                   Paso 2<br />
                   <small>Ingrese los detalles de la cotizaci�n</small>
                </span>
            </a></li>
  				<li><a href="#step-3">
                <label class="stepNumber">3</label>
                <span class="stepDesc">
                   Paso 3<br />
                   <small>Valores y Observaciones</small>
                </span>                   
             </a></li>
  				<li><a href="#step-4">
                <label class="stepNumber">4</label>
                <span class="stepDesc">
                   Paso 4<br />
                   <small>otro datos informativos de la cotizacion</small>
                </span>                   
            </a></li>
  			</ul>
		<div id="step-1">	
            <h2 class="StepTitle">Paso 1 Items de la cotizaci�n</h2>
			<p><?php include('editQuoteStep1.php');?></p> 
                     			
        </div>
  			<div id="step-2">
            <h2 class="StepTitle">Step 2 Ingrese los detalles de la cotizacion</h2>	
            <p><?php include('editQuoteStep2.php');?></p>      
        </div>                      
  			<div id="step-3">
            <h2 class="StepTitle">Step 3 Resumen de precios</h2>	
            <p><?php include('editQuoteStep3.php');?></p>               				          
        </div>
  			<div id="step-4">
            <h2 class="StepTitle">Step 4 Datos complementarios</h2>	
             <p><?php include('editQuoteStep4.php');?></p>            			
        </div>
  		</div>
			<!-- End SmartWizard Content -->  
		</td>
	</tr>
</table>
<input type="hidden" name="id" id="id" value="<?php echo $id?>" />
</form>
</div>

