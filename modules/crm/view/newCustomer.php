<?php
 include('includes.php');
 
 $crmModelObj->connect();
 
  //Se verifica si el modulo esta instalado
 if(!$crmModelObj->Checker()){
  $ins=true;
 }
 
 $msg=false;
 
 //Instala el modulo
 if($crmModelObj->getVars('install')){
  $crmModelObj->install();
  $msgIns=true;
 }
 
 //Actions
 if($_POST){
 $action=$crmModelObj->postVars('action');
 if(!empty($action)){
	 $res=$customerCtrlObj->_init($action);
 }
 }
?>
<link type="text/css" href="modules/crm/css/crmStyles.css" rel="stylesheet"  />
<div class="widget3">
 <div class="widgetlegend">GE &reg; CRM - Clientes </div>
 <?php
  if($res=='Yes')
  {
  ?>
   <div class="ui-widget">
	<div class="ui-state-highlight ui-corner-all" style="margin-top: 20px; padding: 0 .7em;">
		<p><span class="ui-icon ui-icon-info" style="float: left; margin-right: .3em;"></span>
		<strong>Felicitaciones!</strong> El cliente y contacto se han creado satisfactoriamente.</p>
	</div>
   </div>
  <?php
  }else{
   if(count($res)>0){
    for($i=0;$i<count($res);$i++){
	 ?>
	  <div class="ui-widget">
		<div class="ui-state-error ui-corner-all" style="margin-top: 20px; padding: 0 .7em;">
			<p><span class="ui-icon ui-icon-info" style="float: left; margin-right: .3em;"></span>
			<strong>Error</strong> <?php echo $res[$i]; ?></p>
		</div>
   		</div>
	 <?php
	}
   }
  }
 ?>
<p style="width:100">
	<a href="<?php $_SERVER['PHP_SELF'];?>?p=modules/crm/view/showCustomers.php" class="btn_normal" style="float:left; margin:5px;">Volver </a>
</p><br />
<br />
<br />

<form name="form1" method="post" action="#"><table width="915" border="0">
  <tr>
    <th width="446"><div align="left">Informacion de cliente </div></th>
    <th width="459"><div align="left">Contacto</div></th>
  </tr>
  <tr>
    <td><table width="384" border="0" align="center">
      <tr>
        <td width="136">Nombre:</td>
        <td width="238"><label>
          <input name="nom" type="text" id="nom" />
        </label></td>
      </tr>
      <tr>
        <td>Identificaci&oacute;n:</td>
        <td><input name="nit" type="text" id="nit" size="10" /> 
        - 
          <label>
          <input name="nit2" type="text" id="nit2" size="5" />
          </label></td>
      </tr>
      <tr>
        <td>Direcci&oacute;n:</td>
        <td><input name="dire" type="text" id="dire" /></td>
      </tr>
      <tr>
        <td>Tel&eacute;fono:</td>
        <td><input name="tel" type="text" id="tel" /></td>
      </tr>
      <tr>
        <td>Customer Services: </td>
        <td><input name="web" type="text" id="web" /></td>
      </tr>
      <tr>
        <td>Tipo de Cliente: </td>
        <td><label>
          <select name="tc" id="tc">
		   <option value="">-- Seleccionar --</option>
		   <?php
		    $row=$customerCtrlObj->_init('GetClasification');
 			if(count($row)>0){
  				foreach($row as $row){
			?>
			 <option value="<?php echo $row["ccl_id"];?>"><?php echo $row["ccl_nom"];?></option>
			<?php	
			 }
			}
		   ?>
          </select>
        </label></td>
      </tr>
      <tr>
        <td>Industria:</td>
        <td><select name="ti" id="ti">
         <option value="">-- Seleccionar --</option>
		   <?php
		    $row=$customerCtrlObj->_init('getIndustry');
 			if(count($row)>0){
  				foreach($row as $row){
			?>
			 <option value="<?php echo $row["ci_id"];?>"><?php echo $row["ci_nom"];?></option>
			<?php	
			 }
			}
		   ?>
		</select></td>
      </tr>
      <tr>
        <td>&nbsp;</td>
        <td>&nbsp;</td>
      </tr>
    </table></td>
    <td><table width="384" border="0" align="center">
      <tr>
        <td width="136">Nombre:</td>
        <td width="238"><input name="nomc" type="text" id="nomc" /></td>
      </tr>
      <tr>
        <td>Cargo:</td>
        <td><input name="cargo" type="text" id="cargo" /></td>
      </tr>
      <tr>
        <td>Email:</td>
        <td><input name="email" type="text" id="email" /></td>
      </tr>
      <tr>
        <td>Tel&eacute;fono:</td>
        <td><input name="telc" type="text" id="telc" /></td>
      </tr>
      <tr>
        <td>Celular: </td>
        <td><input name="cel" type="text" id="cel" /></td>
      </tr>
      <tr>
        <td>Cumplea&ntilde;os: </td>
        <td><input name="ini" type="text" id="ini" class="datepicker" /></td>
      </tr>
      <tr>
        <td>Tipo de cargo :</td>
        <td><select name="tcargo" id="tcargo">
          <option value="">-- Seleccionar --</option>
		   <?php
		    $row=$customerCtrlObj->_init('getPosition');
 			if(count($row)>0){
  				foreach($row as $row){
			?>
			 <option value="<?php echo $row["cp_id"];?>"><?php echo $row["cp_nom"];?></option>
			<?php	
			 }
			}
		   ?>
		 </select></td>
      </tr>
      <tr>
        <td>Profesi&oacute;n:</td>
        <td><select name="tp" id="tp">
        	 <option value="">-- Seleccionar --</option>
		   <?php
		    $row=$customerCtrlObj->_init('GetProfession');
 			if(count($row)>0){
  				foreach($row as $row){
			?>
			 <option value="<?php echo $row["cprof_id"];?>"><?php echo $row["cprof_nom"];?></option>
			<?php	
			 }
			}
		   ?>
		</select></td>
      </tr>
    </table></td>
  </tr>
  <tr>
    <td><input name="action" type="hidden" id="action" value="NewCustomer" /></td>
    <td><div align="right"><a href="#" class="btn_normal" onclick="document.form1.submit();">Guardar</a></div></td>
  </tr>
</table></form>
</div>

