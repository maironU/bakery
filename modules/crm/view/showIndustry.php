<?php
 include('includes.php');
 
 $customerModelObj->connect();
 
 if($customerModelObj->getVars('ActionDel')=='true'){
  $customerModelObj->delCustomerIndustry();
 }
 
?>
<div class="widgetlegend">Industrias</div>
<p style="width:100">
	<a href="<?php $_SERVER['PHP_SELF'];?>?p=modules/crm/view/newIndustry.php" class="btn_normal" style="float:left; margin:5px;">Nuevo </a>
	<a href="<?php $_SERVER['PHP_SELF'];?>?p=modules/crm/view/showSetup.php" class="btn_normal" style="float:left; margin:5px;">Volver </a>
</p>
 <br /><br /><br />


<table width="98%" height="48" border="0" align="center">
  <tr>
    <th>ID</th>
    <th>Nombre</th>
	<th>Color</th>
    <th colspan="2">Acciones</th>
  </tr>
  <?php
   $row=$customerModelObj->getCustomerIndustry();
   
   if(count($row)>0){
 	foreach($row as $row){
  ?>
  <tr>
    <td><?php echo $row["ci_id"];?></td>
	<td><?php echo $row["ci_nom"];?></td>
    <td width="86"><a href="<?php $_SERVER['PHP_SELF'];?>?p=modules/crm/view/editIndustry.php&id=<?php echo $row["ci_id"]?>" class="btn_normal">Editar</a></td>
    <td width="81"><a href="<?php $_SERVER['PHP_SELF'];?>?p=modules/crm/view/showIndustry.php&id=<?php echo $row["ci_id"]?>&ActionDel=true" class="btn_borrar">Eliminar</a></td>
  </tr>  <?php
   }
  } 
  ?>
</table>

</div>
