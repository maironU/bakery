<?php
 include('includes.php');
 
 $customerHistoryModelObj->connect();
 
 if($customerHistoryModelObj->getVars('ActionDel')=='true'){
  $customerHistoryModelObj->delKpiItem();
 }
 
?>
<div class="widgetlegend">KPI - Niveles de rendimiento</div>
<p style="width:100">
	<a href="<?php $_SERVER['PHP_SELF'];?>?p=modules/crm/view/newKpi.php" class="btn_normal" style="float:left; margin:5px;">Nuevo </a>
	<a href="<?php $_SERVER['PHP_SELF'];?>?p=modules/crm/view/showKpiGoals.php" class="btn_normal" style="float:left; margin:5px;">Metas </a>
	<a href="<?php $_SERVER['PHP_SELF'];?>?p=modules/crm/view/showKpiInf.php" class="btn_normal" style="float:left; margin:5px;">Informe </a>
	<a href="<?php $_SERVER['PHP_SELF'];?>?p=modules/crm/view/showSetup.php" class="btn_normal" style="float:left; margin:5px;">Volver </a>
</p>
 <br /><br /><br />


<table width="98%" height="48" border="0" align="center">
  <tr>
    <th>ID</th>
    <th>Nombre</th>
	<th>Porcentaje</th>
	<th>Medicion</th>
	<th>Mostrado en</th>
    <th colspan="2">Acciones</th>
  </tr>
  <?php
   $row=$customerHistoryModelObj->getKpiItem();
   
   if(count($row)>0){
 	foreach($row as $row){
  ?>
  <tr>
    <td><?php echo $row["ki_id"];?></td>
	<td><?php echo $row["ki_nom"];?></td>
	<td><?php echo $row["ki_por"];?>%</td>
	<td><?php 
		if($row["ki_medicion"]=='1'){
		 echo "Valor";
		}else{
		 echo "Contador";
		}
	
	?></td>
	<td>
	<?php
	 switch($row["ki_showin"]){
	 	case '1': echo "Historial";
		break;
		
		case '2': echo "Cotizaciones";
		break;
		
		case '3': echo "Ordenes";
		break;
	 }
	?>
	</td>
    <td width="86"><a href="<?php $_SERVER['PHP_SELF'];?>?p=modules/crm/view/editKpi.php&id=<?php echo $row["ki_id"]?>" class="btn_normal">Editar</a></td>
    <td width="81"><a href="<?php $_SERVER['PHP_SELF'];?>?p=modules/crm/view/showKpi.php&id=<?php echo $row["ki_id"]?>&ActionDel=true" class="btn_borrar">Eliminar</a></td>
  </tr>  <?php
   }
  } 
  ?>
</table>

</div>
