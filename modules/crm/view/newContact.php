<?php
  include('includes_self.php');
  
  $id=$crmModelObj->getVars('id');
  
  if($_POST){
   $res='';
   echo $res=$customerCtrlObj->_init('NewContact');
  }
?>
<link href="../../../css/scsStyle.css" rel="stylesheet" type="text/css" />
<link href="../../../css/smoothness/jquery-ui-1.9.2.custom.css" rel="stylesheet">
<link href="../../../css/smoothness/jquery-ui-1.9.2.custom.min.css" rel="stylesheet" type="text/css" />
<script src="../../../js/jquery-1.8.3.js"></script>
<script src="../../../js/jquery-ui-1.9.2.custom.js"></script>
<script type="text/javascript">
 $(function() {
		$( ".datepicker" ).datepicker();
		$('.datepicker').datepicker('option', {dateFormat: 'yy-mm-dd'});
       
	});
</script>
<?php
 if(!empty($res)){
 ?>
  <div class="ui-widget">
	<div class="ui-state-highlight ui-corner-all" style="margin-top: 20px; padding: 0 .7em;">
		<p><span class="ui-icon ui-icon-info" style="float: left; margin-right: .3em;"></span>
		<strong>Felicitaciones!</strong> El contacto se ha creado satisfactoriamente.</p>
	</div>
</div>
 <?php
 }
?>

<div class="widget3">
 <div class="widgetlegend">Nuevo Contacto </div>
  <form action="" method="post" name="form1">
    <table width="384" border="0" align="center">
      <tr>
        <td width="136">Nombre:</td>
        <td width="238"><input name="nomc" type="text" id="nomc" /></td>
      </tr>
      <tr>
        <td>Cargo:</td>
        <td><input name="cargo" type="text" id="cargo" /></td>
      </tr>
      <tr>
        <td>Email:</td>
        <td><input name="email" type="text" id="email" /></td>
      </tr>
      <tr>
        <td>Tel&eacute;fono:</td>
        <td><input name="telc" type="text" id="telc" /></td>
      </tr>
      <tr>
        <td>Celular: </td>
        <td><input name="cel" type="text" id="cel" /></td>
      </tr>
      <tr>
        <td>Cumplea&ntilde;os: </td>
        <td><input name="ini" type="text" id="ini" class="datepicker" /></td>
      </tr>
      <tr>
        <td>Tipo de cargo :</td>
        <td><select name="tcargo" id="tcargo">
            <option value="">-- Seleccionar --</option>
            <?php
		    $row=$customerCtrlObj->_init('getPosition');
 			if(count($row)>0){
  				foreach($row as $row){
			?>
            <option value="<?php echo $row["cp_id"];?>"><?php echo $row["cp_nom"];?></option>
            <?php	
			 }
			}
		   ?>
        </select></td>
      </tr>
      <tr>
        <td>Profesi&oacute;n:</td>
        <td><select name="tp" id="tp">
            <option value="">-- Seleccionar --</option>
            <?php
		    $row=$customerCtrlObj->_init('GetProfession');
 			if(count($row)>0){
  				foreach($row as $row){
			?>
            <option value="<?php echo $row["cprof_id"];?>"><?php echo $row["cprof_nom"];?></option>
            <?php	
			 }
			}
		   ?>
        </select></td>
      </tr>
      <tr>
        <td><input name="id" type="hidden" id="id" value="<?php echo $id; ?>" /></td>
        <td><a href="#" class="btn_normal" onclick="document.form1.submit();">Guardar</a></td>
      </tr>
    </table>
  </form>

</div>