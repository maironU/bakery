<?php
  include('includes_self.php');
  
  $orderModelObj->connect();
  
  if($_POST){
   $res='';
   $res=$orderModelObj->newOrderHistory();
  }
  
  $id=$orderModelObj->getVars('id');
  
?>
<link href="../../../css/scsStyle.css" rel="stylesheet" type="text/css" />
<link href="../../../css/smoothness/jquery-ui-1.9.2.custom.css" rel="stylesheet">
<link href="../../../css/smoothness/jquery-ui-1.9.2.custom.min.css" rel="stylesheet" type="text/css" />
<script src="../../../js/jquery-1.8.3.js"></script>
<script src="../../../js/jquery-ui-1.9.2.custom.js"></script>
<script type="text/javascript">
 $(function() {
		$( ".datepicker" ).datepicker();
		$('.datepicker').datepicker('option', {dateFormat: 'yy-mm-dd'});
       
	});
</script>
<?php
 if($res=='nok'){
 ?>
  <div class="ui-widget">
		<div class="ui-state-error ui-corner-all" style="margin-top: 20px; padding: 0 .7em;">
			<p><span class="ui-icon ui-icon-info" style="float: left; margin-right: .3em;"></span>
			<strong>Error</strong> Debe seleccionar un estado</p>
		</div>
   		</div>
 <?php
 }
?>

<div class="widget3">
 <div class="widgetlegend">Estados </div>
  <form action="" method="post" name="form1">
    <table width="703" height="88" border="0" align="center">
      <tr>
        <td>Estado:</td>
        <td><label>
          <select name="os" id="os">
		   <option value="">-- Seleccionar --</option>
		   <?php
		    $row=$orderModelObj->getOrderState();
			foreach($row as $row){
			?>
				<option value="<?php echo $row["os_id"];?>"><?php echo $row["os_name"];?></option>
			<?php
			}
		   ?>
          </select>
        </label></td>
      </tr>
      <!--<tr>
        <td>Descripci&oacute;n del estado: </td>
        <td><label>
          <textarea name="desc" cols="40" rows="6" id="desc"></textarea>
        </label></td>
      </tr> -->
      <tr>
        <td width="136"><input name="id" type="hidden" id="id" value="<?php echo $id; ?>" /></td>
        <td width="238"><a href="#" class="btn_normal" onclick="document.form1.submit();">Confirmar</a></td>
      </tr>
    </table>
  </form>
 <br />
  <table width="703" border="0">
    <tr>
      <th width="229">FECHA</th>
      <th width="118">ESTADO</th>
      <!--<th width="340">DESCRIPCION</th> -->
    </tr>
	<?php
	 $row=$orderModelObj->getOrderHistory($id);
	 foreach($row as $row){
	?>
    <tr>
      <td><?php echo $row["oh_date"]; ?></td>
      <td><span style="background-color:#<?php echo $row["oh_color"]?>; display:block; border: solid #000000 1px; padding:3px;  height:50px;"><?php echo $row["oh_name"]?></span></td>
      <!--<td><?php echo $row["oh_desc"]; ?></td> -->
    </tr>
	<?php
	 }
	?>
  </table>
</div>