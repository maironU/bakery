<?php
 include('includes.php');
 
 $user=new User();
 $user->connect();
 $crmModelObj->connect();
 $customerHistoryModelObj->connect();
 
 if($_GET){
  $actiond=$customerHistoryModelObj->getVars('ActionDel');
  $id=$customerHistoryModelObj->getVars('id');
  if($actiond==true){
   $customerHistoryModelObj->delKpiGoal($id);
  }
 }
 
?>
<div class="widgetlegend">Informe de Metas</div>
<p style="width:100">
	<a href="<?php $_SERVER['PHP_SELF'];?>?p=modules/crm/view/showKpi.php" class="btn_normal" style="float:left; margin:5px;">Volver </a>
	<a href="<?php $_SERVER['PHP_SELF'];?>?p=modules/crm/view/showKpiInf2.php" class="btn_normal" style="float:left; margin:5px;">Detallado </a>
 <br /><br /><br />
 <table width="100%" border="0">
   <tr>
     <td><form id="frmGoals" name="frmGoals" method="post" action="">
       <table width="100%" border="0">
         <tr>
           <td>Mes:</td>
           <td><label>
             <select name="m" id="m">
               <option value="">-- Seleccionar --</option>
               <?php
				for($i=1;$i<=12;$i++){
				 if($i<9){
				  $m='0'.$i;
				 }else{
				  $m=$i;
				 }
				?>
               <option value="<?php echo $m; ?>"><?php echo $customerHistoryModelObj->getMounthName($m);?></option>
               <?php
			}
		   ?>
             </select>
             <input name="action" type="hidden" id="action" value="search" />
           </label></td>
           <td>A&ntilde;o:</td>
           <td><select name="a" id="a">
             <option value="">-- Seleccionar --</option>
             <?php
		    for($i=date('Y');$i>=2000;$i--){
			?>
             <option value="<?php echo $i; ?>"><?php echo $i; ?></option>
             <?php
			}
		   ?>
           </select></td>
           <td><a href="javascript:;" onclick="document.frmGoals.submit()" class="btn_1">Buscar</a></td>
         </tr>
       </table>
               </form>     </td>
   </tr>
   <tr>
     <td>&nbsp;</td>
   </tr>
   <tr>
     <td>
	 <?php
	  
	  if($_POST){
	   $m=$customerHistoryModelObj->postVars('m');
	   $a=$customerHistoryModelObj->postVars('a');
	  }else{
	   $m=date('m');
	   $a=date('Y');
	  }
	  
	 
	   
	  $row=$customerHistoryModelObj->getKpiItemByType(2);
	  if(count($row)>0){
	   foreach($row as $row){
	    
		$data[0]=$row["ki_nom"];
		$legend[0]="Meta";
		$data[1]=$customerHistoryModelObj->getKpiGoalValue($row["ki_id"],$m,$a);
	
	    //$row1=$user->getUsersList();
		$row1=$crmModelObj->getCommercials();
		$i=2;
		$j=1;
		if(count($row1)>0){
		 foreach($row1 as $row1){
		   $num=$customerHistoryModelObj->getCalcKpi($row["ki_id"],$row1["u_id"],$m,$a,2);
		   $legend[$j]=$row1["u_nom"];
		   $data[$i]=$num;
		   $i++;
		   $j++;
		 }
		}
		
		$datas[]=$data;
		
	   }
	  }
	  
	 
	  
	  /*$datas=array(
	   array('a',0,1,3,4),
	   array('b',10,2,40,1)
	  );*/
	  
	  //Funciones para graficar
	  $graph =new PHPlot(1024,765);
	  
	  $graph->setprintimage(false);
      $graph->setfileformat("jpg");

   	  $graph->setoutputfile("modules/crm/graph/perform.jpg");

      $graph->setisinline(true);
	  $graph->SetDataValues($datas);
	  $graph->SetDataType("text-data");
	  
	  $graph->SetLegend($legend);
	  $graph->SetYTickIncrement(100);
	  $graph->settickcolor('white');
	  $graph->SetXDataLabelAngle(45);
	  $graph->SetXTitle("");
	  $graph->SetYTitle("");
	  $graph->SetPlotType("bars");
	  $graph->DrawGraph();
	  
	  $graph->PrintImage();
	 ?>
	 <div style="overflow:auto; width:1024px">
	  <img src="modules/crm/graph/perform.jpg" />	 </div>	 </td>
   </tr>
   <tr>
     <td>&nbsp;</td>
   </tr>
   <tr>
     <td><span style="overflow:auto; width:1024px">
       <?php
	  
	  if($_POST){
	   $m=$customerHistoryModelObj->postVars('m');
	   $a=$customerHistoryModelObj->postVars('a');
	  }else{
	   $m=date('m');
	   $a=date('Y');
	  }
	  
	 unset($datas);
	 unset($data);
	   
	  $row=$customerHistoryModelObj->getKpiItemByType(1);
	  if(count($row)>0){
	   foreach($row as $row){
	    
		$data[0]=$row["ki_nom"];
		$legend[0]="Meta";
		$data[1]=$customerHistoryModelObj->getKpiGoalValue($row["ki_id"],$m,$a);
	
	    //$row1=$user->getUsersList();
		$row1=$crmModelObj->getCommercials();
		$i=2;
		$j=1;
		if(count($row1)>0){
		 foreach($row1 as $row1){
		   $num=$customerHistoryModelObj->getCalcKpi($row["ki_id"],$row1["u_id"],$m,$a,1);
		   $legend[$j]=$row1["u_nom"];
		   $data[$i]=$num;
		   $i++;
		   $j++;
		 }
		}
		
		$datas[]=$data;
		
	   }
	  }
	  
	  /*$datas=array(
	   array('a',0,1,3,4),
	   array('b',10,2,40,1)
	  );*/
	  
	  //Funciones para graficar
	  $graph =new PHPlot(1024,765);
	  
	  $graph->setprintimage(false);
      $graph->setfileformat("jpg");

   	  $graph->setoutputfile("modules/crm/graph/perform2.jpg");

      $graph->setisinline(true);
	  $graph->SetDataValues($datas);
	  $graph->SetDataType("text-data");
	  
	  $graph->SetLegend($legend);
	  $graph->SetYTickIncrement(1000000);
	  $graph->settickcolor('white');
	  $graph->SetXDataLabelAngle(45);
	  $graph->SetXTitle("");
	  $graph->SetYTitle("");
	  $graph->SetPlotType("bars");
	  $graph->DrawGraph();
	  
	  $graph->PrintImage();
	 ?>
     <div style="overflow:auto; width:1024px">
	  <img src="modules/crm/graph/perform2.jpg" />	 </div></span></td>
   </tr>
   <tr>
     <td>&nbsp;</td>
   </tr>
 </table>
 </div>
