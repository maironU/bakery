<?php
 include('includes.php');
 
 $user=new User();
 $user->connect();
 $customerHistoryModelObj->connect();
 
 if($_GET){
  $actiond=$customerHistoryModelObj->getVars('ActionDel');
  $id=$customerHistoryModelObj->getVars('id');
  if($actiond==true){
   $customerHistoryModelObj->delKpiGoal($id);
  }
 }
 
?>
<div class="widgetlegend">Metas</div>
<p style="width:100">
	<a href="<?php $_SERVER['PHP_SELF'];?>?p=modules/crm/view/showKpi.php" class="btn_normal" style="float:left; margin:5px;">Volver </a>
	<a href="<?php $_SERVER['PHP_SELF'];?>?p=modules/crm/view/newKpiGoal.php" class="btn_normal" style="float:left; margin:5px;">Nuevo </a>
</p>
 <br /><br /><br />
 <table width="100%" border="0">
   <tr>
     <td><form id="frmGoals" name="frmGoals" method="post" action="">
       <table width="100%" border="0">
         <tr>
           <td>Mes:</td>
           <td><label>
             <select name="m" id="m">
               <option value="">-- Seleccionar --</option>
               <?php
				for($i=1;$i<=12;$i++){
				 if($i<9){
				  $m='0'.$i;
				 }else{
				  $m=$i;
				 }
				?>
               <option value="<?php echo $m; ?>"><?php echo $customerHistoryModelObj->getMounthName($m);?></option>
               <?php
			}
		   ?>
             </select>
             <input name="action" type="hidden" id="action" value="search" />
           </label></td>
           <td>A&ntilde;o:</td>
           <td><select name="a" id="a">
             <option value="">-- Seleccionar --</option>
             <?php
		    for($i=date('Y');$i>=2000;$i--){
			?>
             <option value="<?php echo $i; ?>"><?php echo $i; ?></option>
             <?php
			}
		   ?>
           </select></td>
           <td><a href="javascript:;" onclick="document.frmGoals.submit()" class="btn_1">Buscar</a></td>
         </tr>
       </table>
               </form>
     </td>
   </tr>
   <tr>
     <td><table width="100%" border="0">
       <tr>
         <th>ID</th>
         <th>MES</th>
         <th>A&Ntilde;O</th>
         <th>ITEM</th>
         <th>VALOR</th>
         <th colspan="2">ACCIONES</th>
        </tr>
		<?php
		 if($_POST){
		 	$row=$customerHistoryModelObj->getKpiGoal($customerHistoryModelObj->postVars('a'),$customerHistoryModelObj->postVars('m'));
		 }else{
		 	$row=$customerHistoryModelObj->getKpiGoal(date('Y'),date('m'));
		 }
		 
		 
		 
		 
		 if(count($row)>0){
		 foreach($row as $row){
		?>
       <tr>
         <td><?php echo $row["kg_id"]; ?></td>
         <td><?php echo $customerHistoryModelObj->getMounthName($row["kg_m"]); ?></td>
         <td><?php echo $row["kg_y"]; ?></td>
         <td><?php $r=$customerHistoryModelObj->getKpiItemById($row["ki_id"]); 
		  foreach($r as $r){
		   echo $r["ki_nom"];
		  }
		 ?></td>
         <td><?php echo $row["kg_value"]; ?></td>
          <td width="86"><a href="<?php $_SERVER['PHP_SELF'];?>?p=modules/crm/view/editKpiGoal.php&id=<?php echo $row["kg_id"]?>" class="btn_normal">Editar</a></td>
    	  <td width="81"><a href="<?php $_SERVER['PHP_SELF'];?>?p=modules/crm/view/showKpiGoals.php&id=<?php echo $row["kg_id"]?>&ActionDel=true" class="btn_borrar">Eliminar</a></td>
       </tr>
	   <?php
	    }
	   }
	   ?>
     </table></td>
   </tr>
   <tr>
     <td>&nbsp;</td>
   </tr>
 </table>
 </div>
