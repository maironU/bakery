<?php
 include('includes.php');
 
 $customerModelObj->connect();
 
 if($customerModelObj->getVars('ActionDel')=='true'){
  $customerModelObj->delCustomerClasification();
 }
 
?>
<div class="widgetlegend">Clasificaciones de cliente</div>
<p style="width:100">
	<a href="<?php $_SERVER['PHP_SELF'];?>?p=modules/crm/view/newClasification.php" class="btn_normal" style="float:left; margin:5px;">Nuevo </a>
	<a href="<?php $_SERVER['PHP_SELF'];?>?p=modules/crm/view/showSetup.php" class="btn_normal" style="float:left; margin:5px;">Volver </a>
</p>
 <br /><br /><br />


<table width="98%" height="48" border="0" align="center">
  <tr>
    <th>ID</th>
    <th>Nombre</th>
	<th>Color</th>
    <th colspan="2">Acciones</th>
  </tr>
  <?php
   $row=$customerModelObj->getCustomerClasification();
   
   if(count($row)>0){
 	foreach($row as $row){
  ?>
  <tr>
    <td><?php echo $row["ccl_id"];?></td>
	<td><?php echo $row["ccl_nom"];?></td>
	<td><span style="background-color:#<?php echo $row["ccl_color"]?>; display:block; border: solid #000000 1px; width:50px; height:50px;">&nbsp;</span> </td>
    <td width="86"><a href="<?php $_SERVER['PHP_SELF'];?>?p=modules/crm/view/editClasification.php&id=<?php echo $row["ccl_id"]?>" class="btn_normal">Editar</a></td>
    <td width="81"><a href="<?php $_SERVER['PHP_SELF'];?>?p=modules/crm/view/showClasification.php&id=<?php echo $row["ccl_id"]?>&ActionDel=true" class="btn_borrar">Eliminar</a></td>
  </tr>  <?php
   }
  } 
  ?>
</table>

</div>
