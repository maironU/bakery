<?php
	//Models
	include('modules/crm/model/crm.php');
	include('modules/crm/model/customerhistoryModel.php');
	include('modules/crm/model/customerModel.php');
	include('modules/crm/model/ordersModel.php');
	include('modules/crm/model/quoteModel.php');
	include('usr/model/User.php');
	include('modules/products/model/products.php');
	
	//Controllers
	include('modules/crm/controller/customerController.php');
	include('modules/crm/controller/orderController.php');
	include('modules/crm/controller/quoteController.php');
	
	require_once("modules/crm/graphclass/phplot.php");
 	require_once("modules/crm/graphclass/phplot_data.php");
	
	//Model Objects
	$crmModelObj=new crmScs();
	$customerHistoryModelObj=new customerHistoryModelGe();
	$customerModelObj=new customerModelGe();
	$orderModelObj=new orderModelGe();
	$quoteModelObj=new quoteModelGe();
	$products=new products();
	
	//Controllers Objects
	$customerCtrlObj = new customerControllerGe();
	$orderCtrlObj = new orderControllerGe();
	$quoteCtrlObj = new quoteControllerGe();

?>
