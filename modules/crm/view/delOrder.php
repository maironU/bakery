<?php
  include('includes_self.php');
  
  $orderModelObj->connect();
  
  if($_POST){
   $res='';
   $res=$orderModelObj->delOrder();
  }
  
  $id=$orderModelObj->getVars('id');
  
?>
<link href="../../../css/scsStyle.css" rel="stylesheet" type="text/css" />
<link href="../../../css/smoothness/jquery-ui-1.9.2.custom.css" rel="stylesheet">
<link href="../../../css/smoothness/jquery-ui-1.9.2.custom.min.css" rel="stylesheet" type="text/css" />
<script src="../../../js/jquery-1.8.3.js"></script>
<script src="../../../js/jquery-ui-1.9.2.custom.js"></script>
<script type="text/javascript">
 $(function() {
		$( ".datepicker" ).datepicker();
		$('.datepicker').datepicker('option', {dateFormat: 'yy-mm-dd'});
       
	});
</script>
<?php
 if(!empty($res)){
 ?>
  <div class="ui-widget">
	<div class="ui-state-highlight ui-corner-all" style="margin-top: 20px; padding: 0 .7em;">
		<p><span class="ui-icon ui-icon-info" style="float: left; margin-right: .3em;"></span>
		<strong>Felicitaciones!</strong> La orden ha sido anulada correctamente.</p>
	</div>
</div>
 <?php
 }
?>

<div class="widget3">
 <div class="widgetlegend">Confirmacion de Anulaci&oacute;n </div>
  <form action="" method="post" name="form1">
    <table width="692" height="70" border="0" align="center">
      <tr>
        <td colspan="2">Esta apunto de anular un pedido. Si realiza este proceso, usted deber&aacute; generar otro medido completamente nuevo con datos diferentes. Est&aacute; seguro de anular este pedido? </td>
      </tr>
      <tr>
        <td>Motivo de Anulaci&oacute;n: </td>
        <td><label>
          <select name="aid" id="aid">
            <option>--Seleccionar--</option>
            <option value="1">Error al ingresar datos</option>
            <option value="2">Cliente no pago</option>
            <option value="3">Cliente cancela</option>
          </select>
        </label></td>
      </tr>
      <tr>
        <td width="136"><input name="id" type="hidden" id="id" value="<?php echo $id; ?>" /></td>
        <td width="238"><a href="#" class="btn_borrar" onclick="document.form1.submit();">Confirmar</a></td>
      </tr>
    </table>
  </form>

</div>