<?php
 include('includes.php');
 
  $orderCtrlObj->connect();
  $orderModelObj->connect();
 
  
//Actions
 if($_POST){
 $action=$orderCtrlObj->postVars('action');
}


?>
<link type="text/css" href="modules/crm/css/crmStyles.css" rel="stylesheet"  />
<!-- Add mousewheel plugin (this is optional) -->
<script type="text/javascript" src="modules/crm/js/jquery.mousewheel-3.0.6.pack.js"></script>
<!-- Add fancyBox main JS and CSS files -->
<script type="text/javascript" src="modules/crm/js/jquery.fancybox.js?v=2.1.4"></script>
<link rel="stylesheet" type="text/css" href="modules/crm/css/jquery.fancybox.css?v=2.1.4" media="screen" />
<script type="text/javascript" src="modules/crm/js/jsCrmFunctions.js"></script>
<style type="text/css">
<!--
.Estilo1 {font-weight: bold}
-->
</style>
<div class="widget3">
 <div class="widgetlegend">GE &reg; CRM - Pedidos </div>
 <?php
  if($res=='Yes')
  {
  ?>
   <div class="ui-widget">
	<div class="ui-state-highlight ui-corner-all" style="margin-top: 20px; padding: 0 .7em;">
		<p><span class="ui-icon ui-icon-info" style="float: left; margin-right: .3em;"></span>
		<strong>Felicitaciones!</strong> El cliente y contacto se han creado satisfactoriamente.</p>
	</div>
</div>
  <?
  }elseif($res=='No Contact'){
  ?>
  
  <?php
  }elseif($res=='No Customer'){
  ?>
  
  <?php
  }
 ?>
<p style="width:100">
	<a href="<?php $_SERVER['PHP_SELF'];?>?p=modules/crm/view/index.php" class="btn_normal" style="float:left; margin:5px;">Volver </a>
<br />
<br />
<br />
<form action="" method="post" name="frmSearch">
<table width="100%" border="0">
  <tr>
    <td width="6%">Buscar</td>
    <td width="20%"><label>
      <input type="text" name="finder" id="finder" nkeyup="myAutoComplete('finder','modules/crm/view/findUserOrder.php')" onkeydown="myAutoComplete('finder','modules/crm/view/findUserOrder.php')"/>
      <input name="action" type="hidden" id="action" value="FindOrder" />
    </label></td>
    <td width="74%"><a href="javascript:;" class="btn_1" onclick="document.frmSearch.submit();">Buscar</a></td>
  </tr>
</table>
</form>

<form name="form1" method="post" action="#">
<br />
  <table width="1073" border="0">
    <tr>
      <th colspan="2"><div align="left">Pedidos</div>
          <div align="left"></div></th>
    </tr>
    <tr>
      <td colspan="2"><table width="1065" height="48" border="0">
          <tr>
            <th width="83" scope="col">Track</th>
            <th width="196" scope="col">Fecha creacion </th>
			<th width="196" scope="col">Cliente </th>
            <th width="216" scope="col">Total</th>
            <th width="301" scope="col">Fecha Vencimiento </th>
            <th width="100" scope="col">Creado por </th>
			<th width="100" scope="col">Estado </th>
            <th colspan="4" scope="col">Acciones</th>
          </tr>
          <?php
		  if($action=='FindOrder'){
		  	$row=$orderCtrlObj->_init('GetOrderBySearch');
		  }else{
		  	$row=$orderCtrlObj->_init('GetUserOrders');
		  }
		 
		 if(count($row)>0){
		 	foreach($row as $row){
		?>
          <tr>
            <td><?php echo $row["o_track"];?></td>
            <td><?php echo $row["o_fecha"];?></td>
			<td><?php
			$customerModelObj->connect();
			$rowx=$customerModelObj->getCustomerById2($row["c_id"]);
			foreach($rowx as $rowx){
			 echo $rowx["c_nom"];
			}
			?></td>
            <td>$<?php echo number_format($row["o_total"]);?></td>
            <td><?php echo $row["o_fecha_fin"];?></td>
            <td><?php
			 $user=new User();
			 $user->connect();
			 $user->getUserById($row["u_id"]);
			 echo $user->name;
			?></td>
			<td><?php
			 $rowe=$orderModelObj->getLastOrderHistory($row["o_id"]);
			 foreach($rowe as $rowe){
			  ?>
			   <span style="background-color:#<?php echo $rowe["oh_color"]?>; color:#000000; display:block; width:100%; height:100%; border:solid 1px #000"><?php echo $rowe["oh_name"]?></span>
			  <?php
			 }
			?></td>
            
			<?php
			 $orderCtrlObj->getSession();
			 if($_SESSION["user_permisions"]==1){
			 ?>
			 <td width="36"><a href="javascript:;" onclick="showInfo(<?php echo $row["o_id"];?>,'modules/crm/view/OrderStates.php')" class="btn_1">Estados</a></td>
             <td width="48"><a href="javascript:;" onclick="showInfo(<?php echo $row["o_id"];?>,'modules/crm/view/delOrder.php')" class="btn_2">Anular</a></td>
             <td width="24"><a href="javascript:;" class="btn_3" onclick="showInfo(<?php echo $row["o_id"];?>,'modules/crm/view/printOrder.php')">Imprimir</a></td>
             <td width="25"><a href="javascript:;" class="btn_4" onclick="showInfo(<?php echo $row["o_id"];?>,'modules/crm/view/showOrder.php')">Ver</a></td>
			 <?php
			 }else{
			 ?>
			 <td width="36"><a href="javascript:;" onclick="showInfo(<?php echo $row["o_id"];?>,'modules/crm/view/OrderStates.php')" class="btn_1">Estados</a></td>
			 <td width="48">&nbsp;</td>
             <td width="24"><a href="javascript:;" class="btn_3" onclick="showInfo(<?php echo $row["o_id"];?>,'modules/crm/view/printOrder.php')">Imprimir</a></td>
             <td width="25"><a href="javascript:;" class="btn_4" onclick="showInfo(<?php echo $row["o_id"];?>,'modules/crm/view/showOrder.php')">Ver</a></td>
			 <?php
			 }
			?>
			
          </tr>
          <?php
			}
		 }
		?>
      </table></td>
    </tr>
    <tr>
      <td width="446">
	  <?php
	    $orderModelObj->startPage($orderModelObj->postVars('page'));
		$num=$orderModelObj->getTotalOrdersByUser(1,$_SESSION['user_id']);
		echo $orderModelObj->getPages($num);
	   ?>
	  </td>
      <td width="459">&nbsp;</td>
    </tr>
  </table>
</form>
</div>
<script>
 function showInfo(id,site)
 {
	$.fancybox.open({
					href : site+'?id='+id,
					type : 'iframe',
					padding:5
				}); 
 }
</script>

