<?php
 include('includes.php');
 
 $orderModelObj->connect();
 
 if($orderModelObj->getVars('ActionDel')=='true'){
  $orderModelObj->delOrderState();
 }
 
?>
<div class="widgetlegend">Administracion de estados</div>
<p style="width:100">
	<a href="<?php $_SERVER['PHP_SELF'];?>?p=modules/crm/view/newOrderState.php" class="btn_normal" style="float:left; margin:5px;">Nuevo </a>
	<a href="<?php $_SERVER['PHP_SELF'];?>?p=modules/crm/view/showSetup.php" class="btn_normal" style="float:left; margin:5px;">Volver </a>
</p>
 <br /><br /><br />


<table width="98%" height="48" border="0" align="center">
  <tr>
    <th>ID</th>
    <th>Nombre</th>
	<th>Color</th>
    <th colspan="2">Acciones</th>
  </tr>
  <?php
   $row=$orderModelObj->getOrderState();
   
   if(count($row)>0){
 	foreach($row as $row){
  ?>
  <tr>
    <td><?php echo $row["os_id"];?></td>
	<td><?php echo $row["os_name"];?></td>
	<td><span style="background-color:#<?php echo $row["os_color"]?>; display:block; border: solid #000000 1px; width:50px; height:50px;">&nbsp;</span> </td>
    <td width="86"><a href="<?php $_SERVER['PHP_SELF'];?>?p=modules/crm/view/editOrderState.php&id=<?php echo $row["os_id"]?>" class="btn_normal">Editar</a></td>
    <td width="81"><a href="<?php $_SERVER['PHP_SELF'];?>?p=modules/crm/view/showOrderStates.php&id=<?php echo $row["os_id"]?>&ActionDel=true" class="btn_borrar">Eliminar</a></td>
  </tr>  <?php
   }
  } 
  ?>
</table>

</div>
