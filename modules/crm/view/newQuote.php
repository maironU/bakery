<?php
 include('includes.php');
 
 $crmModelObj->connect();
 $products->connect();
 
 $msg=false;
 
 $id=$crmModelObj->getVars('id');
 
 //Actions
 if($_POST){
  $quoteCtrlObj->_init('NewQuote');
 }
 
?>
<link type="text/css" href="modules/crm/css/crmStyles.css" rel="stylesheet"  />
<link href="modules/crm/css/smart_wizard_vertical.css" rel="stylesheet" type="text/css">
<!--<script type="text/javascript" src="modules/crm/js/jquery-2.0.0.min.js"></script>-->
<script type="text/javascript" src="modules/crm/js/jquery.smartWizard.js"></script>
<script type="text/javascript" src="modules/crm/js/jsCrmFunctions.js"></script>
<script type="text/javascript">
   
    $(document).ready(function(){
    	// Smart Wizard	
  		$('#wizard').smartWizard({transitionEffect:'slide'});
     
		});
</script>
 <div class="widget3">
 <div class="widgetlegend">GE &reg; Cotizaciones </div>
 <?php
  if($msg)
  {
  ?>
   <div class="ui-widget">
	<div class="ui-state-highlight ui-corner-all" style="margin-top: 20px; padding: 0 .7em;">
		<p><span class="ui-icon ui-icon-info" style="float: left; margin-right: .3em;"></span>
		<strong>Felicitaciones!</strong> El slide ha sido eliminado satisfactoriamente.</p>
	</div>
</div>
  <?php
  }
 ?>
<p style="width:100">
	<!--<a href="<?php $_SERVER['PHP_SELF'];?>?p=modules/sliders/view/newSlide.php" class="btn_normal" style="float:left; margin:5px;">Nuevo Slide </a>-->
</p>
 
<form name="formCtz" method="post" action=""> 
<table border="0">
	<tr>
		<td>
			<!-- Tabs -->
  			<div id="wizard" class="swMain">
  			<ul>
  				<!--<li><a href="#step-0">
                <label class="stepNumber">0</label>
                <span class="stepDesc">
                   Paso 0<br />
                   <small>Seleccione el convenio de su cotizaci�n</small>
                </span>
            </a></li>-->
				<li><a href="#step-1">
                <label class="stepNumber">1</label>
                <span class="stepDesc">
                   Paso 1<br />
                   <small>Seleccione los Item de su cotizaci�n</small>
                </span>
            </a></li>
  				<li><a href="#step-2">
                <label class="stepNumber">2</label>
                <span class="stepDesc">
                   Paso 2<br />
                   <small>Ingrese los detalles de la cotizaci�n</small>
                </span>
            </a></li>
  				<li><a href="#step-3">
                <label class="stepNumber">3</label>
                <span class="stepDesc">
                   Paso 3<br />
                   <small>Valores y Observaciones</small>
                </span>                   
             </a></li>
  				<li><a href="#step-4">
                <label class="stepNumber">4</label>
                <span class="stepDesc">
                   Paso 4<br />
                   <small>otro datos informativos de la cotizacion</small>
                </span>                   
            </a></li>
  			</ul>
  		<!--<div id="step-0">	
            <h2 class="StepTitle">Paso 0 Convenio de la cotizaci�n</h2>
			<p><?php include('newQuoteStep0.php');?></p>
                     			
        </div>-->
		<div id="step-1">	
            <h2 class="StepTitle">Paso 1 Items de la cotizaci�n</h2>
			<!--<iframe frameborder="0" src="modules/crm/view/newQuoteStep1.php" allowtransparency="yes" scrolling="auto" width="1000" height="1000"></iframe> -->
             <p><?php include('newQuoteStep1.php');?></p>           			
        </div>
  			<div id="step-2">
            <h2 class="StepTitle">Step 2 Ingrese los detalles de la cotizacion</h2>	
            <p><?php include('newQuoteStep2.php');?></p>      
        </div>                      
  			<div id="step-3">
            <h2 class="StepTitle">Step 3 Resumen de precios</h2>	
            <p><?php include('newQuoteStep3.php');?></p>               				          
        </div>
  			<div id="step-4">
            <h2 class="StepTitle">Step 4 Datos complementarios</h2>	
             <p><?php include('newQuoteStep4.php');?></p>            			
        </div>
  		</div>
			<!-- End SmartWizard Content -->  
		</td>
	</tr>
</table>
</form>
</div>

