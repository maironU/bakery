<?php
 include('includes.php');
 
 $crmModelObj->connect();
 
  //Se verifica si el modulo esta instalado
 if(!$crmModelObj->Checker()){
  $ins=true;
 }
 
 $msg=false;
 
 //Instala el modulo
 if($crmModelObj->getVars('install')){
  $crmModelObj->install();
  $msgIns=true;
 }
 
 //Actions
 
?>
<link type="text/css" href="modules/crm/css/crmStyles.css" rel="stylesheet"  />

 <div class="widget3">
 <div class="widgetlegend">GE &reg; CRM </div>
 <?php
  if($msg)
  {
  ?>
   <div class="ui-widget">
	<div class="ui-state-highlight ui-corner-all" style="margin-top: 20px; padding: 0 .7em;">
		<p><span class="ui-icon ui-icon-info" style="float: left; margin-right: .3em;"></span>
		<strong>Felicitaciones!</strong> El slide ha sido eliminado satisfactoriamente.</p>
	</div>
</div>
  <?php
  }
 ?>
<p style="width:100">
	<!--<a href="<?php $_SERVER['PHP_SELF'];?>?p=modules/sliders/view/newSlide.php" class="btn_normal" style="float:left; margin:5px;">Nuevo Slide </a>-->
</p>
 <div id="crmOpt">
  <ul>
   <li><a href="<?php $_SERVER['PHP_SELF'];?>?p=modules/crm/view/showEnterprises.php"><img src="modules/crm/images/scs_notebook.png" border="0" alt="Empresas" /><span class="txt_notebook">empresa</span></a></li>
   <li><a href="<?php $_SERVER['PHP_SELF'];?>?p=modules/crm/view/showParams.php"><img src="modules/crm/images/scs_notebook.png" border="0" alt="Parametros generales" /><span class="txt_notebook">Param</span></a></li>
   <li><a href="<?php $_SERVER['PHP_SELF'];?>?p=modules/crm/view/showTp.php"><img src="modules/crm/images/scs_notebook.png" border="0" alt="Tipos de pago" /><span class="txt_notebook">Pagos</span></a></li>
   <li><a href="<?php $_SERVER['PHP_SELF'];?>?p=modules/crm/view/showPrices.php"><img src="modules/crm/images/scs_notebook.png" border="0" alt="Listas de Precios" /><span class="txt_notebook">Listas</span></a></li>
   <li><a href="<?php $_SERVER['PHP_SELF'];?>?p=modules/crm/view/showConvenio.php"><img src="modules/crm/images/scs_notebook.png" border="0" alt="convenios" /><span class="txt_notebook">convenios</span></a></li>
   <li><a href="<?php $_SERVER['PHP_SELF'];?>?p=modules/crm/view/showOrderStates.php"><img src="modules/crm/images/scs_notebook.png" border="0" /><span class="txt_notebook">Estados</span></a> </li>
   <li><a href="<?php $_SERVER['PHP_SELF'];?>?p=modules/crm/view/showClasification.php"><img src="modules/crm/images/scs_notebook.png" border="0" /><span class="txt_notebook">clasificacion</span></a></li>
   <li><a href="<?php $_SERVER['PHP_SELF'];?>?p=modules/crm/view/showIndustry.php"><img src="modules/crm/images/scs_notebook.png" border="0" /><span class="txt_notebook">Industrias</span></a></li>
   <li><a href="<?php $_SERVER['PHP_SELF'];?>?p=modules/crm/view/showKpi.php"><img src="modules/crm/images/scs_notebook.png" border="0" /><span class="txt_notebook">KPI</span></a></li>
   <li><a href="<?php $_SERVER['PHP_SELF'];?>?p=modules/crm/view/showCustomerAsign.php"><img src="modules/crm/images/scs_notebook.png" border="0" /><span class="txt_notebook">Asignaciones</span> </a> </li>
   <li><a href="<?php $_SERVER['PHP_SELF'];?>?p=modules/crm/view/showProfesions.php"><img src="modules/crm/images/scs_notebook.png" border="0" /><span class="txt_notebook">Profesiones</span> </a> </li>
   <li><a href="<?php $_SERVER['PHP_SELF'];?>?p=modules/crm/view/showPositions.php"><img src="modules/crm/images/scs_notebook.png" border="0" /><span class="txt_notebook">Cargos</span> </a> </li>
   <li><a href="<?php $_SERVER['PHP_SELF'];?>?p=modules/crm/view/showCommercials.php"><img src="modules/crm/images/scs_notebook.png" border="0" /><span class="txt_notebook">Ejecutivos</span></a></li>
   <li><a href="<?php $_SERVER['PHP_SELF'];?>?p=modules/crm/view/showCenterCost.php"><img src="modules/crm/images/scs_notebook.png" border="0" /><span class="txt_notebook">C. Costo</span></a></li>
  </ul>
 </div>


</div>
