<?php
  include('includes_self.php');
  
  $products->connect();
  
  $quoteCtrlObj->getSession();
  if($_POST){
   $quoteModelObj->connect();
   
   $trm=$quoteModelObj->postVars('trm');
   $price=$quoteModelObj->postVars('price');
   $qty=$quoteModelObj->postVars('cant');
   
   if($trm!=1){
    $price1 = $price * $trm;
	$total= $price1 * $qty;
   }else{
    $total= $price * $qty;
   }
   
   $quoteModelObj->newQuoteDetailItemTemp('0',$quoteModelObj->postVars('desc'),$quoteModelObj->postVars('price'),$quoteModelObj->postVars('cant'),$quoteModelObj->postVars('trm'),$quoteModelObj->postVars('id_currency'),$total);
  }
  
  //Actions
  $action=$quoteCtrlObj->getVars('action');
  if(!empty($action)){
  	$quoteCtrlObj->_init($action);
  } 
  
?>
<link href="../../../css/scsStyle.css" rel="stylesheet" type="text/css" />
<link href="../../../css/smoothness/jquery-ui-1.9.2.custom.css" rel="stylesheet">
<link href="../../../css/smoothness/jquery-ui-1.9.2.custom.min.css" rel="stylesheet" type="text/css" />
<script src="../../../js/jquery-1.8.3.js"></script>
<script src="../../../js/jquery-ui-1.9.2.custom.js"></script>
<script src="../../../js/jquery-ui-1.9.2.custom.js"></script>
<script src="../js/jsCrmFunctions.js"></script>

<div class="widget3">
 <div class="widgetlegend">Nuevo Item </div>
  <form action="" method="post" name="form1">
    <table width="703" height="88" border="0" align="center">
      <tr>
        <td>Descripci&oacute;n:</td>
        <td><label>
          <input name="desc" type="text" id="desc" size="40" />
        </label></td>
      </tr>
      <tr>
        <td>Precio:</td>
        <td><input name="price" type="text" id="price" value="0" size="40" /></td>
      </tr>
      <tr>
        <td>TRM:</td>
        <td><input name="trm" type="text" id="trm" value="1" size="40" /></td>
      </tr>
      <tr>
        <td>Moneda:</td>
        <td><select name="id_currency">
        <?php
         $row=$products->getCurrency();
		 foreach($row as $row){
		 ?>
         <option value="<?php echo $row["pc_id"]?>"><?php echo $row["pc_code"]?></option>
         <?php
		 }
		?>
        </select></td>
      </tr>
      <tr>
        <td>Cantidad:</td>
        <td><input name="cant" type="text" id="cant" value="1" size="40" /></td>
      </tr>
      <tr>
        <td width="136">&nbsp;</td>
        <td width="238"><a href="#" class="btn_normal" onclick="document.form1.submit();">Agregar</a></td>
      </tr>
    </table>
  </form>
 <br />
 
</div>
