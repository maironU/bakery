<?php
 include('includes.php');
 
 $crmModelObj->connect();
 
  //Se verifica si el modulo esta instalado
 if(!$crmModelObj->Checker()){
  $ins=true;
 }
 
 $msg=false;
 
 //Instala el modulo
 if($crmModelObj->getVars('install')){
  $crmModelObj->install();
  $msgIns=true;
 }
 
 //Actions
 
?>
<link type="text/css" href="modules/crm/css/crmStyles.css" rel="stylesheet"  />
<?php
 if(!$ins){
 ?>
 <div class="widget3">
 <div class="widgetlegend">CRM </div>
 <?php
  if($msg)
  {
  ?>
   <div class="ui-widget">
	<div class="ui-state-highlight ui-corner-all" style="margin-top: 20px; padding: 0 .7em;">
		<p><span class="ui-icon ui-icon-info" style="float: left; margin-right: .3em;"></span>
		<strong>Felicitaciones!</strong> El slide ha sido eliminado satisfactoriamente.</p>
	</div>
</div>
  <?php
  }
 ?>
<p style="width:100">
	<!--<a href="<?php $_SERVER['PHP_SELF'];?>?p=modules/sliders/view/newSlide.php" class="btn_normal" style="float:left; margin:5px;">Nuevo Slide </a>-->
</p>
 <div id="crmOpt">
  <ul>
   <li>
   	<a href="<?php $_SERVER['PHP_SELF'];?>?p=modules/crm/view/showCustomers.php"><img src="modules/crm/images/scs_notebook.png" border="0" />
	<span class="txt_notebook">clientes</span>	</a>   </li>
   <!--<li>
   	<a href="<?php $_SERVER['PHP_SELF'];?>?p=modules/crm/view/showUserQuotes.php"><img src="modules/crm/images/scs_notebook.png" />
	<span class="txt_notebook">cotizaciones</span>
	<span class="text-content">&nbsp;</span></a>   </li>-->
   <li>
   <!--<a href="<?php $_SERVER['PHP_SELF'];?>?p=modules/crm/view/showUserOrders.php"><img src="modules/crm/images/scs_notebook.png" /><span class="txt_notebook">mis pedidos</span></a></li>-->
   <!--<li>
   <a href="<?php $_SERVER['PHP_SELF'];?>?p=modules/crm/view/showSetup.php"><img src="modules/crm/images/scs_notebook.png" /><span class="txt_notebook">Configuracion</span></a></li>-->
   </ul>
 </div>


</div>

<?php } else {?>
<br />
 <div class="widget3">
 <div class="widgetlegend">Instalador GE &reg; CRM </div>
 <?php
  if($msgIns)
  {
  ?>
   <div class="ui-widget">
	<div class="ui-state-highlight ui-corner-all" style="margin-top: 20px; padding: 0 .7em;">
		<p><span class="ui-icon ui-icon-info" style="float: left; margin-right: .3em;"></span>
		<strong>Felicitaciones!</strong> El modulo ha sido instalado satisfactoriamente.</p>
	</div>
</div>
  <?php
  }
 ?>
<form id="form1" name="form1" method="post" action="#">
  <table width="804" border="0" style="float:left;">
    <tr>
      <td width="525" valign="top">
        <p style="float:left; text-align:justify; width:804px">Bienvenido al instalador de GEADMIN &reg; CRM . Gracias por adquirir el modulo de CRM (Customer Relationship Management) de GEADMIN&reg;. Con este nuevo modulo usted podr&aacute; controlar todo el proceso comercial de su compa&ntilde;&iacute;a. Estas son las ventajas que usted encontrar&aacute; dentro de este modulo:</p>
        <p style="float:left; text-align:justify; width:804px">	- Control de sus clientes actuales y potenciales</p>
        <p style="float:left; text-align:justify; width:804px">- Control de gesti&oacute;n de su equipo comercial</p>
        <p style="float:left; text-align:justify; width:804px">- Administraci&oacute;n completa de sus productos y servicios</p>
        <p style="float:left; text-align:justify; width:804px">-Control de actividades de tus clientes</p>
        <p style="float:left; text-align:justify; width:804px">-Cotizaciones y ordenes de trabajo centralizadas   </p></td>
    </tr>
    <tr>
      <td valign="top"><div align="right"><a href="<?php $_SERVER['PHP_SELF'];?>?p=modules/crm/view/index.php&amp;install=true" class="btn_normal">Instalar</a></div></td>
    </tr>
  </table>
</form>
</div>
<?php } ?>

