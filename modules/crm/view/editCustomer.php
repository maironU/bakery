<?php
 include('includes.php');
 
 $crmModelObj->connect();
 
  //Se verifica si el modulo esta instalado
 if(!$crmModelObj->Checker()){
  $ins=true;
 }
 
 $msg=false;
 
 //Instala el modulo
 if($crmModelObj->getVars('install')){
  $crmModelObj->install();
  $msgIns=true;
 }
 
 //Actions
 if($_POST){
 $action=$crmModelObj->postVars('action');
 if(!empty($action)){
	 $res=$customerCtrlObj->_init($action);
 }
}

$action=$crmModelObj->getVars('action');
 if(!empty($action)){
	 $res=$customerCtrlObj->_init($action);
 }

//Get the customer inf
$row=$customerCtrlObj->_init('getCustomerById');
if(count($row)>0){
 foreach($row as $row){
 	$nom=$row["c_nom"];
	$nit=$row["c_nit"];
	$dir=$row["c_dir"];
	$tel=$row["c_tel"];
	$web=$row["c_web"];
	$ti_id=$row["ti_id"];
	$ccl_id=$row["ccl_id"];
	$id=$row["c_id"];
 }
}
$nit=explode('-',$nit);

?>
<link type="text/css" href="modules/crm/css/crmStyles.css" rel="stylesheet"  />
<!-- Add mousewheel plugin (this is optional) -->
<script type="text/javascript" src="modules/crm/js/jquery.mousewheel-3.0.6.pack.js"></script>
<!-- Add fancyBox main JS and CSS files -->
<script type="text/javascript" src="modules/crm/js/jquery.fancybox.js?v=2.1.4"></script>
<link rel="stylesheet" type="text/css" href="modules/crm/css/jquery.fancybox.css?v=2.1.4" media="screen" />
<style type="text/css">
<!--
.Estilo1 {font-weight: bold}
-->
</style>
<div class="widget3">
 <div class="widgetlegend">GE &reg; CRM - Clientes </div>
 <?php

  if(count($res)==0 && $_POST)
  {
  ?>
   <div class="ui-widget">
	<div class="ui-state-highlight ui-corner-all" style="margin-top: 20px; padding: 0 .7em;">
		<p><span class="ui-icon ui-icon-info" style="float: left; margin-right: .3em;"></span>
		<strong>Felicitaciones!</strong> El cliente se han actualizado satisfactoriamente.</p>
	</div>
</div>
  <?php
  }else{
  if(count($res)>0){
    for($i=0;$i<count($res);$i++){
	 ?>
	  <div class="ui-widget">
		<div class="ui-state-error ui-corner-all" style="margin-top: 20px; padding: 0 .7em;">
			<p><span class="ui-icon ui-icon-info" style="float: left; margin-right: .3em;"></span>
			<strong>Error</strong> <?php echo $res[$i]; ?></p>
		</div>
   		</div>
	 <?php
	}
   }
  }
  
 ?>
<p style="width:100">
	<a href="<?php $_SERVER['PHP_SELF'];?>?p=modules/crm/view/showCustomers.php" class="btn_normal" style="float:left; margin:5px;">Volver </a>
</p><br />
<br />
<br />

<form name="form1" method="post" action="#"><table width="915" border="0">
  <tr>
    <th width="446"><div align="left">Informacion de cliente </div></th>
    <th width="459"><div align="left">Contacto</div></th>
  </tr>
  <tr>
    <td><table width="384" border="0" align="center">
      <tr>
        <td width="136">Nombre:</td>
        <td width="238"><label>
          <input name="nom" type="text" id="nom" value="<?php echo $nom; ?>" />
        </label></td>
      </tr>
      <tr>
        <td>Identificaci&oacute;n:</td>
        <td><input name="nit" type="text" id="nit" value="<?php echo $nit[0]; ?>" size="10" /> 
          - 
            <label>
            <input name="nit2" type="text" id="nit2" value="<?php echo $nit[1]; ?>" size="5" />
            </label></td>
      </tr>
      <tr>
        <td>Direcci&oacute;n:</td>
        <td><input name="dire" type="text" id="dire" value="<?php echo $dir; ?>" /></td>
      </tr>
      <tr>
        <td>Tel&eacute;fono:</td>
        <td><input name="tel" type="text" id="tel" value="<?php echo $tel; ?>" /></td>
      </tr>
      <tr>
        <td>Customer Services: </td>
        <td><input name="web" type="text" id="web" value="<?php echo $web; ?>" /></td>
      </tr>
      <tr>
        <td>Tipo de Cliente: </td>
        <td><label>
          <select name="tc" id="tc">
		   <option value="">-- Seleccionar --</option>
		   <?php
		    $row=$customerCtrlObj->_init('GetClasification');
 			if(count($row)>0){
  				foreach($row as $row){
				if($ccl_id==$row["ccl_id"]){
				?>
			 		<option value="<?php echo $row["ccl_id"];?>" selected="selected"><?php echo $row["ccl_nom"];?></option>
				<?php
				}else{
				?>
			 		<option value="<?php echo $row["ccl_id"];?>"><?php echo $row["ccl_nom"];?></option>
				<?php	
				}
				
			 }
			}
		   ?>
          </select>
        </label></td>
      </tr>
      <tr>
        <td>Industria:</td>
        <td><select name="ti" id="ti">
         <option value="">-- Seleccionar --</option>
		   <?php
		    $row=$customerCtrlObj->_init('getIndustry');
 			if(count($row)>0){
  				foreach($row as $row){
				if($ti_id==$row["ci_id"]){
					?>
			 			<option value="<?php echo $row["ci_id"];?>" selected="selected"><?php echo $row["ci_nom"];?></option>
					<?php
				}else{
					?>
			 			<option value="<?php echo $row["ci_id"];?>"><?php echo $row["ci_nom"];?></option>
					<?php	
				}
				
			 }
			}
		   ?>
		</select></td>
      </tr>
      <tr>
        <td>&nbsp;</td>
        <td>&nbsp;</td>
      </tr>
    </table></td>
    <td valign="top"><table width="465" border="0">
      <tr>
        <td colspan="5"><a href="#" class="btn_1" onclick="showInfo(<?php echo $id; ?>,'modules/crm/view/newContact.php')">Nuevo</a></td>
        </tr>
	  <tr>
        <th width="114">Nombre</th>
        <th width="81">Email</th>
        <th width="124">Telefono</th>
		<th width="124">Cel</th>
        <th colspan="2">Acciones</th>
      </tr>
	  <?php
	   $rowe=$customerCtrlObj->_init('GetContact');
	   if(count($rowe)>0){
	    foreach($rowe as $row){
		?>
		<tr>
			<td><?php echo $row["cc_nom"];?></td>
			<td><?php echo $row["cc_mail"];?></td>
			<td><?php echo $row["cc_telefono"];?></td>
			<td><?php echo $row["cc_cel"];?></td>
			<td width="64"><a href="#" class="btn_1" onclick="showInfo(<?php echo $row["cc_id"]; ?>,'modules/crm/view/editContact.php')">Editar</a></td>
			<td width="60"><a href="<?php $_SERVER['PHP_SELF'];?>?id=<?php echo $crmModelObj->getVars('id')?>&p=<?php echo $crmModelObj->getVars('p')?>&idc=<?php echo $row["cc_id"]?>&action=DelContact" class="btn_2">Eliminar</a></td>
      	</tr>
		<?php
		}
	   }
	  ?>
     
    </table></td>
  </tr>
  <tr>
    <td><div align="right">
      <input name="action" type="hidden" id="action" value="EditCustomer" />
      <input name="id" type="hidden" id="id" value="<?php echo $id; ?>" />
      <a href="#" class="btn_normal" onclick="document.form1.submit();">Guardar</a></div></td>
    <td>&nbsp;</td>
  </tr>
</table>
</form>
</div>
<script>
 function showInfo(id,site)
 {
	$.fancybox.open({
					href : site+'?id='+id,
					type : 'iframe',
					padding:5
				}); 
 }
</script>

