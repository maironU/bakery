<?php
 include('includes.php');
 
 $crmModelObj->connect();
 $orderModelObj->connect();
 $customerModelObj->connect();
 $products->connect();
 
 $msg=false;
 
 $id=$orderModelObj->getVars('id');
 
 //Actions
 if($_POST){
  $orderModelObj->editOrder();
 }
 
 if($orderModelObj->getVars('action')=='DelOrderDetail'){
	$id_item=$orderModelObj->getVars('id_item'); 
  	$orderModelObj->delOrderDetailItem($id_item);
 }
 
 //Se consultan los datos de la orden
 $row=$orderModelObj->getOrderById($id);
 foreach($row as $row)
 {
 	$company=$row["e_id"];
 	 $customer=$row["c_id"];
 	 $contact=$row["cc_id"];
 	 $term=$row["o_mod_pago"];
 	 $ini=$row["o_fecha_fin"];
 	 $subto=$row["o_subto_base"];
 	 $subtotalcop=$row["o_subto"];
 	 $ivacop=$row["o_iva"];
 	 $totalcop=$row["o_total"];
 	 $obs=$row["o_obs"];
 	 $trm=$row["o_trm"];
 	 $desc=$row["o_desc"];
 	 $horaentrega=$row["o_horaentrega"];
 }
 
 if($crmModelObj->getVars('action')=='DelOrderDetail'){
 	$id_item=$orderModelObj->getVars('id_item');
	$orderModelObj->connect();
	$orderModelObj->delOrderDetailItem($id_item);
 }
 
?>
<link type="text/css" href="modules/crm/css/crmStyles.css" rel="stylesheet"  />
<link href="modules/crm/css/smart_wizard_vertical.css" rel="stylesheet" type="text/css">
<!--<script type="text/javascript" src="modules/crm/js/jquery-2.0.0.min.js"></script>-->
<script type="text/javascript" src="modules/crm/js/jquery.smartWizard.js"></script>
<script type="text/javascript" src="modules/crm/js/jsCrmFunctions.js"></script>
<script type="text/javascript">
   
    $(document).ready(function(){
    	// Smart Wizard	
  		$('#wizard').smartWizard({transitionEffect:'slide'});
     
		});
</script>
 <div class="widget3">
 <div class="widgetlegend">GE &reg; Ordenes </div>
 <?php
  if($msg)
  {
  ?>
   <div class="ui-widget">
	<div class="ui-state-highlight ui-corner-all" style="margin-top: 20px; padding: 0 .7em;">
		<p><span class="ui-icon ui-icon-info" style="float: left; margin-right: .3em;"></span>
		<strong>Felicitaciones!</strong> El slide ha sido eliminado satisfactoriamente.</p>
	</div>
</div>
  <?php
  }
 ?>
<p style="width:100">
	<!--<a href="<?php $_SERVER['PHP_SELF'];?>?p=modules/sliders/view/newSlide.php" class="btn_normal" style="float:left; margin:5px;">Nuevo Slide </a>-->
</p>
 
<form name="formOrder" method="post" action=""> 
<table border="0">
	<tr>
		<td>
			<!-- Tabs -->
  			<div id="wizard" class="swMain">
  			<ul>
  				<!--<li><a href="#step-0">
                <label class="stepNumber">0</label>
                <span class="stepDesc">
                   Paso 0<br />
                   <small>Seleccione el convenio</small>
                </span></li>-->
				<li><a href="#step-1">
                <label class="stepNumber">1</label>
                <span class="stepDesc">
                   Paso 1<br />
                   <small>Seleccione los Items de su orden</small>
                </span>
            </a></li>
  				<li><a href="#step-2">
                <label class="stepNumber">2</label>
                <span class="stepDesc">
                   Paso 2<br />
                   <small>Ingrese los detalles de la orden</small>
                </span>
            </a></li>
  				<li><a href="#step-3">
                <label class="stepNumber">3</label>
                <span class="stepDesc">
                   Paso 3<br />
                   <small>Valores y Observaciones</small>
                </span>                   
             </a></li>
  				<li><a href="#step-4">
                <label class="stepNumber">4</label>
                <span class="stepDesc">
                   Paso 4<br />
                   <small>otro datos informativos de la orden</small>
                </span>                   
            </a></li>
  			</ul>
  			<!--<div id="step-0">	
            <h2 class="StepTitle">Paso 1 Seleccione el convenio</h2>
			<p><?php include('editOrderStep0.php');?></p>
                     			
        </div>-->
			<div id="step-1">	
            <h2 class="StepTitle">Paso 1 Items de la orden</h2>
			<!--<iframe frameborder="0" src="modules/crm/view/newOrderStep1.php" allowtransparency="yes" scrolling="auto" width="1000" height="1000"></iframe> -->
             <p><?php include('editOrderStep1.php');?></p>        			
        </div>
  			<div id="step-2">
            <h2 class="StepTitle">Step 2 Ingrese los detalles de la orden</h2>	
            <p><?php include('editOrderStep2.php');?></p>      
        </div>                      
  			<div id="step-3">
            <h2 class="StepTitle">Step 3 Resumen de precios</h2>	
            <p><?php include('editOrderStep3.php');?></p>               				          
        </div>
  			<div id="step-4">
            <h2 class="StepTitle">Step 4 Datos complementarios</h2>	
             <p><?php include('editOrderStep4.php');?></p>            			
        </div>
  		</div>
			<!-- End SmartWizard Content -->  
		</td>
	</tr>
</table>
</form>
</div>

