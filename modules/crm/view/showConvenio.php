<?php
 include('includes.php');
 
 $quoteModelObj->connect();
 
 if($quoteModelObj->getVars('ActionDel')=='true'){
  $quoteModelObj->delAgreements();
 }
 
?>
<div class="widgetlegend">Convenios</div>
<p style="width:100">
	<a href="<?php $_SERVER['PHP_SELF'];?>?p=modules/crm/view/newConvenio.php" class="btn_normal" style="float:left; margin:5px;">Nuevo </a>
	<a href="<?php $_SERVER['PHP_SELF'];?>?p=modules/crm/view/showSetup.php" class="btn_normal" style="float:left; margin:5px;">Volver </a>
</p>
 <br /><br /><br />


<table width="100%" height="48" border="0" align="center">
  <tr>
    <th>ID</th>
    <th>Nombre</th>
	<th>Descripcion</th>
	<th>Porcentaje</th>
    <th colspan="2">Acciones</th>
  </tr>
  <?php
   $row=$quoteModelObj->getAgreements();
   
   if(count($row)>0){
 	foreach($row as $row){
  ?>
  <tr>
    <td><?php echo $row["co_id"];?></td>
	<td><?php echo $row["co_nom"];?></td>
	<td><?php echo $row["co_descripcion"];?></td>
	<td><?php echo $row["co_por"];?>%</td>
    <td width="86"><a href="<?php $_SERVER['PHP_SELF'];?>?p=modules/crm/view/editConvenio.php&id=<?php echo $row["co_id"]?>" class="btn_normal">Editar</a></td>
    <td width="81"><a href="<?php $_SERVER['PHP_SELF'];?>?p=modules/crm/view/showConvenio.php&id=<?php echo $row["co_id"]?>&ActionDel=true" class="btn_borrar">Eliminar</a></td>
  </tr>
  <?php
   }
  } 
  ?>
</table>

</div>
