<?php
 include('includes.php');
 
 $quoteCtrlObj->connect();
 
 if($quoteCtrlObj->getVars('ActionDel')=='true'){
  $quoteCtrlObj->_init('delCategory');
 }
 
?>
<div class="widgetlegend">Lista de precios</div>
<p style="width:100">
	<a href="<?php $_SERVER['PHP_SELF'];?>?p=modules/crm/view/newCategoryPrice.php" class="btn_normal" style="float:left; margin:5px;">Nuevo </a>
	<a href="<?php $_SERVER['PHP_SELF'];?>?p=modules/crm/view/showPrices.php" class="btn_normal" style="float:left; margin:5px;">Volver </a>
</p>
 <br /><br /><br />


<table width="100%" height="48" border="0" align="center">
  <tr>
    <th>ID</th>
    <th>Nombre</th>
	<th>Margen</th>
    <th colspan="2">Acciones</th>
  </tr>
  <?php
   $row=$quoteCtrlObj->_init('getCategory');
   
   if(count($row)>0){
 	foreach($row as $row){
  ?>
  <tr>
    <td><?php echo $row["plc_id"];?></td>
	<td><?php echo $row["plc_nom"];?></td>
	<td><?php echo $row["plc_porventa"];?>%</td>
    <td width="86"><a href="<?php $_SERVER['PHP_SELF'];?>?p=modules/crm/view/editCategoryPrice.php&id=<?php echo $row["plc_id"]?>" class="btn_normal">Editar</a></td>
    <td width="81"><a href="<?php $_SERVER['PHP_SELF'];?>?p=modules/crm/view/showCategoryPrices.php&id=<?php echo $row["plc_id"]?>&ActionDel=true" class="btn_borrar">Eliminar</a></td>
  </tr>
  <?php
   }
  } 
  ?>
</table>

</div>
