<?php
 include('includes.php');
 
 $user=new User();
 $user->connect();
 $customerHistoryModelObj->connect();
 $msg=false;
 
 if($_POST){
   $id=$customerHistoryModelObj->postVars('id');
   $id_kpi=$customerHistoryModelObj->postVars('id_kpi');
   $y=$customerHistoryModelObj->postVars('a');
   $m=$customerHistoryModelObj->postVars('m');
   $value=$customerHistoryModelObj->postVars('value');
   
   $customerHistoryModelObj->editKpiGoal($id,$id_kpi,$y,$m,$value);
   
   $msg=true;
 }
 
 $id=$customerHistoryModelObj->getVars('id');
 $row=$customerHistoryModelObj->getKpiGoalById($id);
 foreach($row as $row){
  $id_kpi=$row["ki_id"];
  $mo=$row["kg_m"];
  $aa=$row["kg_y"];
  $value=$row["kg_value"];
 }
 
?>
<div class="widgetlegend">Metas</div>
<?php
  if($msg)
  {
  ?>
   <div class="ui-widget">
	<div class="ui-state-highlight ui-corner-all" style="margin-top: 20px; padding: 0 .7em;">
		<p><span class="ui-icon ui-icon-info" style="float: left; margin-right: .3em;"></span>
		<strong>Felicitaciones!</strong> se ha sido guardado satisfactoriamente.</p>
	</div>
</div>
  <?php
  }
 ?>
<p style="width:100">
	<a href="<?php $_SERVER['PHP_SELF'];?>?p=modules/crm/view/showKpiGoals.php" class="btn_normal" style="float:left; margin:5px;">Volver </a>
 <br /><br /><br />
<table width="100%" border="0">
  <tr>
    <td>
      <form id="frmGoals" name="frmGoals" method="post" action=""><table width="100%" border="0">
      <tr>
        <td width="18%">Item Kpi: </td>
        <td width="82%"><label>
          <select name="id_kpi" id="id_kpi">
		  <?php
		   $row=$customerHistoryModelObj->getKpiItem();
		   foreach($row as $row){
		   if($id_kpi==$row["ki_id"]){
		    ?>
		   <option value="<?php echo $row["ki_id"];?>" selected="selected"><?php echo $row["ki_nom"];?></option>
		   <?php
		   }else{
		   ?>
		   <option value="<?php echo $row["ki_id"];?>"><?php echo $row["ki_nom"];?></option>
		   <?php
		   }
		   }
		  ?>
          </select>
        </label></td>
      </tr>
      <tr>
        <td>Mes:</td>
        <td><select name="m" id="m">
          <option value="">-- Seleccionar --</option>
          <?php
				for($i=1;$i<=12;$i++){
				 if($i<9){
				  $m='0'.$i;
				 }else{
				  $m=$i;
				 }
				 
				 if($m==$mo){
				 ?>
          		<option value="<?php echo $m; ?>" selected="selected"><?php echo $customerHistoryModelObj->getMounthName($m);?></option>
          		<?php
				 }else{
				 ?>
          		<option value="<?php echo $m; ?>"><?php echo $customerHistoryModelObj->getMounthName($m);?></option>
          		<?php
				 }
				
			}
		   ?>
        </select></td>
      </tr>
      <tr>
        <td>A&ntilde;o:</td>
        <td><select name="a" id="a">
          <option value="">-- Seleccionar --</option>
          <?php
		    for($i=date('Y');$i>=2000;$i--){
			
			if($aa==$i){
			?>
          <option value="<?php echo $i; ?>" selected="selected"><?php echo $i; ?></option>
          <?php
			}else{
			?>
          <option value="<?php echo $i; ?>"><?php echo $i; ?></option>
          <?php
			}
			
			}
		   ?>
        </select></td>
      </tr>
      <tr>
        <td>Valor</td>
        <td><label>
          <input name="value" type="text" id="value" value="<?php echo $value; ?>" />
        </label></td>
      </tr>
      <tr>
        <td><input name="id" type="hidden" id="id" value="<?php echo $id; ?>" /></td>
        <td><a href="javascript:;" class="btn_1" onclick="document.frmGoals.submit();">Guardar</a></td>
      </tr>
    </table>
     </form>    </td>
  </tr>
 </table>
 </div>
