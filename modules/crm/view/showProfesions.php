<?php
 include('includes.php');
 
 $customerModelObj->connect();
 
 if($customerModelObj->getVars('ActionDel')=='true'){
  $customerModelObj->delCustomerProfession();
 }
 
?>
<div class="widgetlegend">Profesiones</div>
<p style="width:100">
	<a href="<?php $_SERVER['PHP_SELF'];?>?p=modules/crm/view/newProfesion.php" class="btn_normal" style="float:left; margin:5px;">Nuevo </a>
	<a href="<?php $_SERVER['PHP_SELF'];?>?p=modules/crm/view/showSetup.php" class="btn_normal" style="float:left; margin:5px;">Volver </a>
</p>
 <br /><br /><br />


<table width="98%" height="48" border="0" align="center">
  <tr>
    <th>ID</th>
    <th>Nombre</th>
    <th colspan="2">Acciones</th>
  </tr>
  <?php
   $row=$customerModelObj->getCustomerProfession();
   
   if(count($row)>0){
 	foreach($row as $row){
  ?>
  <tr>
    <td><?php echo $row["cprof_id"];?></td>
	<td><?php echo $row["cprof_nom"];?></td>
    <td width="86"><a href="<?php $_SERVER['PHP_SELF'];?>?p=modules/crm/view/editProfesion.php&id=<?php echo $row["cprof_id"]?>" class="btn_normal">Editar</a></td>
    <td width="81"><a href="<?php $_SERVER['PHP_SELF'];?>?p=modules/crm/view/showProfesions.php&id=<?php echo $row["cprof_id"]?>&ActionDel=true" class="btn_borrar">Eliminar</a></td>
  </tr>  <?php
   }
  } 
  ?>
</table>

</div>
