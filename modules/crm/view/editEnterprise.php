<?php
 include('includes.php');
 
 $crmModelObj->connect();
 $msg=false;
 
 if($_POST){
  $crmModelObj->editCompany();
  $msg=true;
 }
 
 $id=$crmModelObj->getVars('id');
 $row=$crmModelObj->getCompanyById($id);
 foreach($row as $row){
  $nom=$row["e_nom"];
  $desc=$row["e_info"];
  $img=$row["e_img"];
 }

?>
<div class="widget3">
 <div class="widgetlegend">Editar empresa </div>
 <?php
  if($msg)
  {
  ?>
   <div class="ui-widget">
	<div class="ui-state-highlight ui-corner-all" style="margin-top: 20px; padding: 0 .7em;">
		<p><span class="ui-icon ui-icon-info" style="float: left; margin-right: .3em;"></span>
		<strong>Felicitaciones!</strong> se ha sido editado satisfactoriamente.</p>
	</div>
</div>
  <?php
  }
 ?>
<p style="width:100">
	<a href="<?php $_SERVER['PHP_SELF'];?>?p=modules/crm/view/showEnterprises.php" class="btn_normal" style="float:left; margin:5px;">Volver </a>
</p>
 
<form action="#" method="post" enctype="multipart/form-data" name="form1" id="form1">
<table width="803" height="48" border="0" style="float:left">
  <tr>
    <td><label>Nombre: </label><br />
      <input name="nom" type="text" id="nom" value="<?php echo $nom;?>" />    </td>
  </tr>
  <tr>
    <td><label>Imagen: </label><br />
      <label>
      <input name="img" type="file" id="img" />
      </label><br />
	  <img src="modules/crm/Logos/<?php echo $img; ?>" />
   </td>
  </tr>
  <tr>
    <td><label>Texto: </label><br />
      <textarea name="desc" cols="50" rows="5" id="desc"><?php echo $desc; ?></textarea>    </td>
  </tr>
  <tr>
    <td><input type="hidden" name="id" id="id" value="<?php echo $id; ?>" /><a href="javascript:;" class="btn_normal" onclick="document.form1.submit();">Guardar</a></td>
  </tr>
</table>
</form>


</div>
