<?php
    include('../../../core/config.php');
	include('../../../core/ge.php');
	include('../../../usr/model/User.php');
	//Models
	include('../model/crm.php');
	include('../model/customerhistoryModel.php');
	include('../model/customerModel.php');
	include('../model/ordersModel.php');
	include('../model/quoteModel.php');
	include('../../products/model/products.php');
	
	//Controllers
	include('../controller/customerController.php');
	include('../controller/orderController.php');
	include('../controller/quoteController.php');
	
	//Model Objects
	$crmModelObj=new crmScs();
	$customerHistoryModelObj=new customerHistoryModelGe();
	$customerModelObj=new customerModelGe();
	$orderModelObj=new orderModelGe();
	$quoteModelObj=new quoteModelGe();
	$products=new products();
	
	//Controllers Objects
	$customerCtrlObj = new customerControllerGe();
	$orderCtrlObj = new orderControllerGe();
	$quoteCtrlObj = new quoteControllerGe();

?>
