<?php
 include('includes.php');
 
$quoteCtrlObj->connect();
$quoteModelObj->connect();
 
//Actions
 if($_POST){
 $action=$quoteCtrlObj->postVars('action');
}

?>
<link type="text/css" href="modules/crm/css/crmStyles.css" rel="stylesheet"  />
<!-- Add mousewheel plugin (this is optional) -->
<script type="text/javascript" src="modules/crm/js/jquery.mousewheel-3.0.6.pack.js"></script>
<!-- Add fancyBox main JS and CSS files -->
<script type="text/javascript" src="modules/crm/js/jquery.fancybox.js?v=2.1.4"></script>
<link rel="stylesheet" type="text/css" href="modules/crm/css/jquery.fancybox.css?v=2.1.4" media="screen" />
<script type="text/javascript" src="modules/crm/js/jsCrmFunctions.js"></script>
<style type="text/css">
<!--
.Estilo1 {font-weight: bold}
-->
</style>
<div class="widget3">
 <div class="widgetlegend">GE &reg; CRM - Cotizaciones </div>
 <?php
  if($res=='Yes')
  {
  ?>
   <div class="ui-widget">
	<div class="ui-state-highlight ui-corner-all" style="margin-top: 20px; padding: 0 .7em;">
		<p><span class="ui-icon ui-icon-info" style="float: left; margin-right: .3em;"></span>
		<strong>Felicitaciones!</strong> El cliente y contacto se han creado satisfactoriamente.</p>
	</div>
</div>
  <?
  }elseif($res=='No Contact'){
  ?>
  
  <?php
  }elseif($res=='No Customer'){
  ?>
  
  <?php
  }
 ?>
<p style="width:100">
	<a href="<?php $_SERVER['PHP_SELF'];?>?p=modules/crm/view/index.php" class="btn_normal" style="float:left; margin:5px;">Volver </a></p>
<br />
<br />
<br />

<form action="" method="post" name="frmSearch">
<table width="100%" border="0">
  <tr>
    <td width="6%">Buscar</td>
    <td width="20%"><label>
      <input type="text" name="finder" id="finder" nkeyup="myAutoComplete('finder','modules/crm/view/findUserQuote.php')" onkeydown="myAutoComplete('finder','modules/crm/view/findUserQuote.php')"/>
      <input name="action" type="hidden" id="action" value="FindQuote" />
    </label></td>
    <td width="74%"><a href="javascript:;" class="btn_1" onclick="document.frmSearch.submit();">Buscar</a></td>
  </tr>
</table>
</form>

<form name="form1" method="post" action="#">
  <br />
  <br />
  <table width="1073" border="0">
    <tr>
      <th colspan="2"><div align="left">Cotizaciones</div>        <div align="left"></div></th>
      </tr>
    <tr>
      <td colspan="2"><table width="1065" height="48" border="0">
        <tr>
          <th width="83" scope="col">Track</th>
          <th width="83" scope="col">Aprobacion</th>
		  <th width="196" scope="col">Fecha creacion </th>
		  <th width="196" scope="col">Descripcion </th>
          <th width="216" scope="col">Total</th>
          <th width="301" scope="col">Fecha Vencimiento </th>
          <th width="100" scope="col">Creado por </th>
          <th colspan="4" scope="col">Acciones</th>
        </tr>
		<?php
		 if($action=='FindQuote'){
		  $row=$quoteCtrlObj->_init('GetQuoteBySearch');
		 }else{
		  $row=$quoteCtrlObj->_init('GetQuoteByUser');
		 }
		 if(count($row)>0){
		 	foreach($row as $row){
		?>
        <tr>
          <td><?php echo $row["q_track"];?></td>
          <td><?php 
		  		if($row["q_active"]==1){
			  		echo "Aprobado";	
			  }
			  else{
					echo "Pendiente";  
			  }?></td>
          <td><?php echo $row["q_fecha"];?></td>
		  <td><?php echo $row["q_desc"];?></td>
          <td>$<?php echo number_format($row["q_total"]);?></td>
          <td><?php echo $row["q_fecha_fin"];?></td>
          <td><?php
			 $user=new User();
			 $user->connect();
			 $user->getUserById($row["u_id"]);
			 echo $user->name;
			?></td>
			
		  <?php
		   $quoteCtrlObj->getSession();
		   if($_SESSION["user_permisions"]==1){
		   ?>
		   <td width="36"><a href="<?php echo $_SERVER['PHP_SELF'];?>?p=modules/crm/view/editQuote.php&id=<?php echo $row["q_id"];?>" class="btn_1">Editar</a></td>
          <td width="48"><a href="javascript:;" onclick="showInfo(<?php echo $row["q_id"];?>,'modules/crm/view/delQuote.php')" class="btn_2">Eliminar</a></td>
          <td width="24"><a href="javascript:;" class="btn_3" onclick="showInfo(<?php echo $row["q_id"];?>,'modules/crm/view/printQuote.php')">Imprimir</a></td>
          <td width="25"><a href="javascript:;" class="btn_4" onclick="showInfo(<?php echo $row["q_id"];?>,'modules/crm/view/showQuote.php')">Ver</a></td>
		   <?php
		   }else{
		   ?>
		   <td width="36"><a href="<?php echo $_SERVER['PHP_SELF'];?>?p=modules/crm/view/editQuote.php&id=<?php echo $row["q_id"];?>" class="btn_1">Editar</a></td>
          <td width="48">&nbsp;</td>
          <td width="24"><a href="javascript:;" class="btn_3" onclick="showInfo(<?php echo $row["q_id"];?>,'modules/crm/view/printQuote.php')">Imprimir</a></td>
          <td width="25"><a href="javascript:;" class="btn_4" onclick="showInfo(<?php echo $row["q_id"];?>,'modules/crm/view/showQuote.php')">Ver</a></td>
		   <?php
		   }
		  ?>	
          
        </tr>
		<?php
			}
		 }
		?>
      </table></td>
      </tr>
    <tr>
      <td width="446"> <?php
	    $quoteModelObj->startPage($quoteModelObj->postVars('page'));
		$num=$quoteModelObj->getTotalQuotesByUser(1,$_SESSION["user_id"]);
		echo $quoteModelObj->getPages($num);
	   ?></td>
      <td width="459">&nbsp;</td>
    </tr>
  </table>
  <br />
  <br />
</form>
</div>
<script>
 function showInfo(id,site)
 {
	$.fancybox.open({
					href : site+'?id='+id,
					type : 'iframe',
					padding:5
				}); 
 }
</script>

