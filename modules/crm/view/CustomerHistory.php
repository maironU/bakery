<?php
  include('includes_self.php');
  
  $customerHistoryModelObj->getSession();
  $customerHistoryModelObj->connect();
  
  if($_POST){
   $res='';
   $res=$customerHistoryModelObj->newCustomerHistory();
  }
  
  $id=$customerHistoryModelObj->getVars('id');
?>
<link href="../../../css/scsStyle.css" rel="stylesheet" type="text/css" />
<link href="../../../css/smoothness/jquery-ui-1.9.2.custom.css" rel="stylesheet">
<link href="../../../css/smoothness/jquery-ui-1.9.2.custom.min.css" rel="stylesheet" type="text/css" />
<script src="../../../js/jquery-1.8.3.js"></script>
<script src="../../../js/jquery-ui-1.9.2.custom.js"></script>
<script type="text/javascript">
 $(function() {
		$( ".datepicker" ).datepicker();
		$('.datepicker').datepicker('option', {dateFormat: 'yy-mm-dd'});
       
	});
</script>
<?php
 if(!empty($res)){
 ?>
  <div class="ui-widget">
	<div class="ui-state-highlight ui-corner-all" style="margin-top: 20px; padding: 0 .7em;">
		<p><span class="ui-icon ui-icon-info" style="float: left; margin-right: .3em;"></span>
		<strong>Felicitaciones!</strong> El contacto se ha creado satisfactoriamente.</p>
	</div>
</div>
 <?php
 }
?>

<div class="widget3">
 <div class="widgetlegend">Historial del Cliente </div>
  <form action="" method="post" name="form1">
    <table width="703" height="88" border="0" align="center">
      <tr>
        <td>Tipo de acci�n:</td>
        <td><label>
          <select name="kpi" id="kpi">
		   <option value="">-- Seleccionar --</option>
		   <?php
		    $row=$customerHistoryModelObj->getKpiByZone(1);
			foreach($row as $row){
			?>
				<option value="<?php echo $row["ki_id"];?>"><?php echo $row["ki_nom"];?></option>
			<?php
			}
		   ?>
          </select>
        </label></td>
      </tr>
      <tr>
        <td>Descripci&oacute;n: </td>
        <td><label>
          <textarea name="desc" cols="40" rows="6" id="desc"></textarea>
        </label></td>
      </tr>
      <tr>
        <td>Fecha de agendamiento: </td>
        <td><label>
          <input name="apointment" type="text" class="datepicker" id="apointment" />
        </label></td>
      </tr>
      <tr>
        <td>Duracion de contacto (visita o llamada): </td>
        <td><label>
          <input name="duration" type="text" id="duration" value="0" size="10" />
        minutos</label></td>
      </tr>
      <tr>
        <td width="136"><input name="id" type="hidden" id="id" value="<?php echo $id; ?>" /></td>
        <td width="238"><a href="#" class="btn_normal" onclick="document.form1.submit();">Confirmar</a></td>
      </tr>
    </table>
  </form>
 <br />
  <table width="703" border="0">
    <tr>
      <th width="229">FECHA</th>
      <th width="118">KPI</th>
      <th width="169">DESCRIPCION</th>
      <th width="169">FECHA AGENDAMIENTO </th>
      <th width="340">DURACION  </th>
    </tr>
	<?php
	 $row=$customerHistoryModelObj->getCustomerHistory();
	 if(count($row)>0){
	 foreach($row as $row){
	?>
    <tr valign="top">
      <td><?php echo $row["ch_date"]; ?></td>
      <td><?php echo $customerHistoryModelObj->getKpiName($row["ch_kpi"]); ?></td>
      <td><?php echo $row["ch_desc"]; ?></td>
      <td><?php echo $row["ch_apointment"]; ?></td>
      <td><?php echo $row["ch_duration"]; ?></td>
    </tr>
	<?php
	 }
	 }
	?>
  </table>
</div>