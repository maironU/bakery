<?php
header('Content-Type: text/html ; charset=UTF-8');
 include('modules/blocks/model/blocks.php');
 
 $obj = new blocks();
 $obj->connect();
 
  //Se verifica si el modulo esta instalado
 if(!$obj->Checker()){
  $ins=true;
 }
 
 $msg=false;
 
 if($obj->getVars('ActionDel')){
  $obj->delBlock();
  $msg=true;
 }
 
 //Instala el modulo
 if($obj->getVars('install')){
  $obj->install();
  $msgIns=true;
 }

 if(!$ins){
 ?>
 <div class="widget3">
 <div class="widgetlegend">Bloques </div>
 <?php
  if($msg)
  {
  ?>
   <div class="ui-widget">
	<div class="ui-state-highlight ui-corner-all" style="margin-top: 20px; padding: 0 .7em;">
		<p><span class="ui-icon ui-icon-info" style="float: left; margin-right: .3em;"></span>
		<strong>Felicitaciones!</strong> El slide ha sido eliminado satisfactoriamente.</p>
	</div>
</div>
  <?php
  }
 ?>
<p style="width:100">
	<a href="<?php $_SERVER['PHP_SELF'];?>?p=modules/blocks/view/newBlock.php" class="btn_normal" style="float:left; margin:5px;">Nuevo </a>
</p>
 
<table width="803" height="48" border="0">
  <tr>
    <th width="30">ID</th>
    <th width="142">Nombre</th>
    <th width="142">Titulos</th>
    <th width="213">Url</th>
    <th width="125">Imagen</th>
    <th width="96">Maximo Caracteres</th>
    <th colspan="2">Acciones</th>
  </tr>
  <?php
   $row=$obj->getBlocks();
   
   if(count($row)>0){
 	foreach($row as $row){
  ?>
  <tr>
    <td><?php echo $row["bl_id"];?></td>
    <td><?php echo $row["bl_name"];?></td>
    <td><?php echo $row["bl_title1"]." ".$row["bl_title2"];?></td>
    <td><?php echo $row["bl_link"];?></td>
    <td><a href="modules/blocks/imgBlocks/<?php echo $row["bl_image"];?>"><img src="modules/blocks/imgBlocks/<?php echo $row["bl_image"];?>" width="100" height="100"  /></a></td>
    <td><?php echo $row["bl_maxtext"];?></td>
    <td width="86"><a href="<?php $_SERVER['PHP_SELF'];?>?p=modules/blocks/view/editBlock.php&id=<?php echo $row["bl_id"]?>" class="btn_normal">Editar</a></td>
    <td width="81"><a href="<?php $_SERVER['PHP_SELF'];?>?p=modules/blocks/view/index.php&id=<?php echo $row["bl_id"]?>&ActionDel=true" class="btn_borrar">Eliminar</a></td>
  </tr>
  <?php
   }
  } 
  ?>
</table>

</div>

<?php } else {?>
<br />
 <div class="widget3">
 <div class="widgetlegend">Instalador Modulo de Bloques </div>
 <?php
  if($msgIns)
  {
  ?>
   <div class="ui-widget">
	<div class="ui-state-highlight ui-corner-all" style="margin-top: 20px; padding: 0 .7em;">
		<p><span class="ui-icon ui-icon-info" style="float: left; margin-right: .3em;"></span>
		<strong>Felicitaciones!</strong> El modulo ha sido instalado satisfactoriamente.</p>
	</div>
</div>
  <?php
  }
 ?>
<form id="form1" name="form1" method="post" action="#">
  <table width="804" border="0" style="float:left;">
    <tr>
      <td width="525" valign="top">&nbsp; </td>
    </tr>
    <tr>
      <td valign="top"><div align="right"><a href="<?php $_SERVER['PHP_SELF'];?>?p=modules/blocks/view/index.php&amp;install=true" class="btn_normal">Instalar</a></div></td>
    </tr>
  </table>
</form>
</div>
<?php } ?>

