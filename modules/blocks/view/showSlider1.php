<?php
header('Content-Type: text/html ; charset=UTF-8');
 include("admin/modules/slider/model/slider.php");
 $slider=new Slider();
 $slider->connect();
?>


<div id="wrapper">
          <div class="slider-wrapper theme-default">
            <div id="slider" class="nivoSlider">
              <?php
			  $row=$slider->getActiveSlides();
			  if(count($row)>0){
				  foreach($row as $row){
			 ?>
			 <img src="admin/modules/slider/imgSlider/<?php echo $row["s_image"];?>" />
			 <?php
			   }
			  }
			 ?>
            </div>
        </div>

  </div>
    <script type="text/javascript" src="scripts/jquery-1.7.1.min.js"></script>
    <script type="text/javascript" src="js/jquery.nivo.slider.pack.js"></script>
    <script type="text/javascript">
    $(window).load(function() {
        $('#slider').nivoSlider({
        	effect: 'fade',
        	animSpeed: 500,
        	pauseTime: 6000,
        	controlNav: false,
        });
        	
    });
    </script>
