<?php
header('Content-Type: text/html ; charset=UTF-8');
 include('modules/blocks/model/blocks.php');
 
 $obj = new blocks();
 $obj->connect();
 
 $msg=false;
 
 if($_POST){
  $obj->newBlock();
  $msg=true;
 }
?>
<div class="widget3">
 <div class="widgetlegend">Bloques </div>
<?php

  if($msg)
  {
  ?>
   <div class="ui-widget">
	<div class="ui-state-highlight ui-corner-all" style="margin-top: 20px; padding: 0 .7em;">
		<p><span class="ui-icon ui-icon-info" style="float: left; margin-right: .3em;"></span>
		<strong>Felicitaciones!</strong> El bloque ha sido creado satisfactoriamente.</p>
	</div>
</div>
  <?php
  }
 ?>
<p style="width:100">
	<a href="<?php $_SERVER['PHP_SELF'];?>?p=modules/blocks/view/index.php" class="btn_normal" style="float:left; margin:5px;">Volver </a>
</p>
 <br /><br /><br /><br />
<form action="#" method="post" enctype="multipart/form-data" name="form1" id="form1">
<table width="803" height="48" border="0" align="center" style="float:left">
  <tr>
    <td><label>Nombre del Bloque (para referencia interna): </label><br />
      <input name="name" type="text" id="name" />    </td>
  </tr>
  <tr>
    <td><label>Titulo 1: </label><br />
      <input name="title1" type="text" id="title1" />    </td>
  </tr>
  <tr>
    <td><label>Titulo 2: </label><br />
      <input name="title2" type="text" id="title2" />    </td>
  </tr>
  <tr>
    <td><label>Nombre de la URL: </label><br />
      <input name="linkname" type="text" id="linkname" />    </td>
  </tr>
  <tr>
    <td><label>Url: </label><br />
      <input name="link" type="text" id="link" value="http://" />    </td>
  </tr>
  <tr>
    <td><label>Imagen: </label><br />
      <label>
      <input name="img" type="file" id="img" />
      </label></td>
  </tr>
  <tr>
    <td><label>Font Awesome: </label><br />
      <label>
      <input name="awe" type="text" id="awe" />
      </label></td>
  </tr>
  <tr>
    <td><label>Texto: </label><br />
      <textarea name="text" cols="50" rows="5" id="text"></textarea>    </td>
  </tr>
  <tr>
    <td><label>Maximo de caracteres: </label><br />
      <input name="maxtext" type="text" id="maxtext" value="0" />    </td>
  </tr>
  <tr>
    <td><label>Pertenece a Bloque dinamico: </label><br />
      <input name="dyn" type="checkbox" id="dyn" value="1" class="toggle" />    </td>
  </tr>
  <tr>
    <td><a href="javascript:;" class="btn_normal" onclick="document.form1.submit();">Guardar</a></td>
  </tr>
</table>
</form>


</div>
