<?php
class blocks extends GeCore{
	
	
	public function newBlock(){
		$name=$this->postVars('name');
		$title1=$this->postVars('title1');
		$title2=$this->postVars('title2');
		$link=$this->postVars('link');
		$text=$this->postVars('text');
		$maxtext=$this->postVars('maxtext');
		$linkname=$this->postVars('linkname');
		$awe=$this->postVars('awe');
		$dyn=$this->postVars('dyn');
		
		$title1=str_replace("'","",$title1);
		$title2=str_replace("'","",$title2);
		$text=str_replace("'","",$text);
		
		$sql="insert into `blocks` (`bl_name`, `bl_title1`, `bl_title2`, `bl_link`, `bl_text`, `bl_image`, `bl_maxtext`, `bl_linkname`, `bl_awe`, `bl_isDynamic`) values ('$name','$title1','$title2','$link','$text','','$maxtext','$linkname', '$awe', '$dyn')";
		$this->Execute($sql);
		
		$id=$this->getLastID();
		
		$this->uploadImg($id);
	}
	
	public function uploadImg($id){
		
		$ok=$this->upLoadFileProccess('img', 'modules/blocks/imgBlocks');
		
		if($ok){
		 $sql="update blocks set bl_image='".$this->fname."' where bl_id='$id'";
		 $this->Execute($sql);
		}
		
	}
	
	public function editBlock(){
		$id=$this->postVars('id');
		$name=$this->postVars('name');
		$title1=$this->postVars('title1');
		$title2=$this->postVars('title2');
		$link=$this->postVars('link');
		$text=$this->postVars('text');
		$maxtext=$this->postVars('maxtext');
		$linkname=$this->postVars('linkname');
		$awe=$this->postVars('awe');
		$dyn=$this->postVars('dyn');
		
		$title1=str_replace("'","",$title1);
		$title2=str_replace("'","",$title2);
		$text=str_replace("'","",$text);
		
		$sql="update `blocks` set `bl_name`='$name', `bl_title1`='$title1', `bl_title2`='$title2', `bl_link`='$link', `bl_text`='$text', `bl_maxtext`='$maxtext', `bl_linkname`='$linkname', `bl_awe`='$awe', `bl_isDynamic`='$dyn' where `bl_id`='$id'";
		$this->Execute($sql);
		
		$this->uploadImg($id);
	}
	
	public function delBlock(){
		$id=$this->getVars('id');
		
		$sql="delete from `blocks` where bl_id='$id'";
		$this->Execute($sql);
	}
	
	
	public function getBlocks(){
	 $sql="select * from blocks";
	 return $this->ExecuteS($sql);
	}
	
	public function getBlockById($id){
	 
	  $sql="select * from blocks where bl_id='$id'";
	  return $row=$this->ExecuteS($sql);
	}
	
	public function showText($text,$maxtext){
	 
	 $num=strlen($text);
	 
	 if($num<$maxtext){
	  return utf8_encode($text);	 
	 }else{
	  return substr(utf8_encode($text),0,$maxtext)."...";	 
	 }
	  
	}
	
	public function getDynamicBlocks(){
	 $sql="select * from blocks where bl_isDynamic='1'";
	 return $this->ExecuteS($sql);
	}
	
	//Installers
   public function Checker(){
    return $this->chkTables('blocks');
   }
   
   public function install(){
    $sql='CREATE TABLE `blocks` (
    `bl_id` INT NOT NULL AUTO_INCREMENT PRIMARY KEY ,
    `bl_name` VARCHAR( 255 ) NOT NULL ,
    `bl_title1` VARCHAR( 255 ) NOT NULL ,
    `bl_title2` VARCHAR( 255 ) NOT NULL ,
    `bl_link` VARCHAR( 255 ) NOT NULL ,
    `bl_linkname` VARCHAR( 255 ) NOT NULL ,
    `bl_text` LONGTEXT NOT NULL ,
    `bl_image` VARCHAR( 255 ) NOT NULL ,
    `bl_awe` VARCHAR( 255 ) NOT NULL ,
    `bl_maxtext` VARCHAR( 100 ) NOT NULL
    ) ENGINE = MYISAM ;';
	
	$this->Execute($sql);
	
	$sql = "ALTER TABLE `blocks` ADD `bl_isDynamic` INT NOT NULL AFTER `bl_maxtext`;";
	$this->Execute($sql);
	
   }
   
   public function uninstall(){
    $sql='DROP TABLE `blocks`';
	$this->Execute($sql);
   }
	
};
?>
