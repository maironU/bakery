<?php
 include('modules/prodorders/view/include.php');
 
  //Se verifica si el modulo esta instalado
 if(!$obj->Checker()){
  $ins=true;
 }
 
 $msg=false;
 $msg2=false;
 
 //Instala el modulo
 if($obj->getVars('install')){
  $obj->install();
  $msgIns=true;
 }
 
 //Actions
 if($obj->getVars('ActionDel')==true)
 {
     $obj->delProdOrder();
     $msg=true;
 }
 
 if($obj->getVars('ActionState')==true)
 {
     $id = $obj->getVars('id');
     $state = $obj->getVars('state');
     $obj->editProdOrdeActive($id, $state);
     
     $row = $obj->getProdOrderById($id);
     if(count($row)>0)
     {
         foreach($row as $row)
         {
             $track = $row['prdo_track'];
             $qtyprd = $row['prdo_qtyprod'];
             $id_product = $row['pro_id'];
         }
     }
     
     if($state == 2)
     {
         $row = $obj->getProdOrderDetail($id);
         if(count($row)>0)
         {
             foreach($row as $row)
             {
                 $inv->registerMov($row['inv_id'],$row['prdod_qty'],$_SESSION['user_id'],'',2,$row['pro_id'],"Salida por Orden de produccion No.".$track,1,1,'','no');
             }
         }
     }
     
     
     if($state == 3)
     {
         $id_inv = $inv->getCapsInvProdByProductWh($id_product, 1);
         $inv->registerMov($id_inv,$qtyprd,$_SESSION['user_id'],'',1,$id_product,"Entrada por Orden de produccion No.".$track,1,1,'','no');
     }
     
     $msg2=true;
 }

?>
<!-- Add mousewheel plugin (this is optional) -->
<script type="text/javascript" src="modules/prodorders/js/jquery.mousewheel-3.0.6.pack.js"></script>
<!-- Add fancyBox main JS and CSS files -->
<script type="text/javascript" src="modules/prodorders/js/jquery.fancybox.js?v=2.1.4"></script>
<link rel="stylesheet" type="text/css" href="modules/prodorders/css/jquery.fancybox.css?v=2.1.4" media="screen" />
<script src="modules/prodorders/js/functions.js"></script>
<?php
 if(!$ins){
 ?>
 <div class="widget3">
 <div class="widgetlegend">Ordenes de Produccion </div>
 <?php
  if($msg)
  {
  ?>
   <div class="ui-widget">
	<div class="ui-state-highlight ui-corner-all" style="margin-top: 20px; padding: 0 .7em;">
		<p><span class="ui-icon ui-icon-info" style="float: left; margin-right: .3em;"></span>
		<strong>Felicitaciones!</strong> se ha sido eliminado satisfactoriamente.</p>
	</div>
</div>
  <?php
  }
 ?>
 <?php
  if($msg2)
  {
  ?>
   <div class="ui-widget">
	<div class="ui-state-highlight ui-corner-all" style="margin-top: 20px; padding: 0 .7em;">
		<p><span class="ui-icon ui-icon-info" style="float: left; margin-right: .3em;"></span>
		<strong>Felicitaciones!</strong> se ha cambiado satisfactoriamente e inventario modificado.</p>
	</div>
</div>
  <?php
  }
 ?>
<p style="width:100">
	<a href="<?php $_SERVER['PHP_SELF'];?>?p=modules/prodorders/view/newProdOrder.php" class="btn_normal" style="float:left; margin:5px;">Nuevo </a>
</p>
 
<table width="803" height="48" border="0">
  <tr>
    <th width="30">ID</th>
    <th width="142">Fecha</th>
    <th width="213">Track</th>
    <th width="125">Desc</th>
    <th width="96">Estado</th>
    <th colspan="4">Acciones</th>
  </tr>
  <?php
   $row=$obj->getProdOrder();
   
   if(count($row)>0){
 	foreach($row as $row){
  ?>
  <tr>
    <td><?php echo $row["prdo_id"];?></td>
    <td><?php echo $row["prdo_date"];?></td>
    <td><?php echo $row["prdo_track"];?></td>
    <td><?php echo $row["prdo_desc"];?></td>
    <td><?php 
	if($row["prdo_active"]==1)
	{
		echo "Pendiente Para produccion";
	}
	elseif($row["prdo_active"]==2)
	{
	 echo "En produccion";
	}
	else
	{
	   echo "Terminada"; 
	}
	
	?></td>
	<td width="86">
        <?php 
        	if($row["prdo_active"]==1)
        	{
        		?>
        		<a href="<?php $_SERVER['PHP_SELF'];?>?p=modules/prodorders/view/index.php&id=<?php echo $row["prdo_id"]?>&state=2&ActionState=true" class="btn_1">Pasar Produccion</a>
        		<?php
        	}
        	elseif($row["prdo_active"]==2)
        	{
        	    ?>
        		<a href="<?php $_SERVER['PHP_SELF'];?>?p=modules/prodorders/view/index.php&id=<?php echo $row["prdo_id"]?>&state=3&ActionState=true" class="btn_4">Fin Produccion</a>
        		<?php
        	}
    	?>
    </td>
    <td width="86">
        <?php 
        	if($row["prdo_active"]==1)
        	{
        		?>
        		<a href="<?php $_SERVER['PHP_SELF'];?>?p=modules/prodorders/view/editProdOrder.php&id=<?php echo $row["prdo_id"]?>" class="btn_normal">Editar</a>
        		<?php
        	}
    	?>
    </td>
    <td width="86">
        <a href="javascript:;" onClick="showInfo(<?php echo $row["prdo_id"]?>,'modules/prodorders/view/printProdOrder.php')" class="btn_normal">Imprimir</a>
    </td>
    <td width="81">
        <?php 
        	if($row["prdo_active"]==1)
        	{
        		?>
        		<a href="<?php $_SERVER['PHP_SELF'];?>?p=modules/prodorders/view/index.php&id=<?php echo $row["prdo_id"]?>&ActionDel=true" class="btn_borrar">Eliminar</a>
        		<?php
        	}
    	?>
    </td>
  </tr>
  <?php
   }
  } 
  ?>
</table>

</div>

<?php } else {?>
<br />
 <div class="widget3">
 <div class="widgetlegend">Instalador Modulo Ordenes de Produccion </div>
 <?php
  if($msgIns)
  {
  ?>
   <div class="ui-widget">
	<div class="ui-state-highlight ui-corner-all" style="margin-top: 20px; padding: 0 .7em;">
		<p><span class="ui-icon ui-icon-info" style="float: left; margin-right: .3em;"></span>
		<strong>Felicitaciones!</strong> El modulo ha sido instalado satisfactoriamente.</p>
	</div>
</div>
  <?php
  }
 ?>
<form id="form1" name="form1" method="post" action="#">
  <table width="804" border="0" style="float:left;">
    <tr>
      <td width="525" valign="top">&nbsp;</td>
    </tr>
    <tr>
      <td valign="top"><div align="right"><a href="<?php $_SERVER['PHP_SELF'];?>?p=modules/prodorders/view/index.php&amp;install=true" class="btn_normal">Instalar</a></div></td>
    </tr>
  </table>
</form>
</div>
<?php } ?>