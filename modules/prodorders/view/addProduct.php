<?php
    include('include_self.php');
    
    $id_product = $obj->postVars('id_product');
    $qty = $obj->postVars('qty');
    
    $row = $products->getProductsById($id_product);
    if(count($row)>0)
    {
        foreach($row as $row)
        {
            $price = $row['pro_costprice'];
            $product_name = $row['pro_name'];
        }
    }
    
    $id_inv = $inv->getCapsInvProdByProductWh($id_product, 1);
    
    $obj->newProdOrderDetailTemp($_SESSION['user_id'], $id_product, $id_inv, $product_name, $qty, $price);
    
    $row = $obj->getProdOrderDetailTemp($_SESSION['user_id']);
    
    $htm = "<table>
            <tr>
                <th>ID</th>
                <th>Nombre</th>
                <th>Cantidad</th>
                <th>&nbsp;</th>
            </tr>
    ";
    
    if(count($row)>0)
    {
        foreach($row as $row)
        {
            $htm .= "
            <tr>
                <td>".$row['prdodt_id']."</td>
                <td>".$row['prdodt_productname']."</td>
                <td><input type='text' name='qty' value='".$row['prdodt_qty']."' onChange='chgQty(this.value,".$row['prdodt_id'].")' /></td>
                <td><a href='javascript:;' class='btn_2' onClick='delProduct(".$row['prdodt_id'].")'>Eliminar</a></td>
            </tr>";
        }
    }
    else
    {
        $htm .= "
            <tr>
                <td clspan='4'>No se encuentra productos</td>
            </tr>";
    }
    
    $htm .= "</table>";
    
    echo $htm;
    
?>