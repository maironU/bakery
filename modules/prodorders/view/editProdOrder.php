<?php
 include('modules/prodorders/view/include.php');
 
 $msg = false;
 
 if($_POST)
 {
    $obj->editProdOrder(); 
    $msg = true;
 }
 
 $id = $obj->getVars('id');
 $row = $obj->getProdOrderById($id);
 foreach($row as $row)
 {
     $desc = $row['prdo_desc'];
     $obs = $row['prdo_obs'];
     $qty = $row['prdo_qtyprod'];
     $id_product = $row['pro_id'];
 }

?>
<script src="modules/prodorders/js/functions.js"></script>
 <div class="widget3">
 <div class="widgetlegend">Editar Orden de Produccion </div>
 <?php
  if($msg)
  {
  ?>
   <div class="ui-widget">
	<div class="ui-state-highlight ui-corner-all" style="margin-top: 20px; padding: 0 .7em;">
		<p><span class="ui-icon ui-icon-info" style="float: left; margin-right: .3em;"></span>
		<strong>Felicitaciones!</strong> se ha creado satisfactoriamente.</p>
	</div>
</div>
  <?php
  }
 ?>
<p style="width:100">
	<a href="<?php $_SERVER['PHP_SELF'];?>?p=modules/prodorders/view/index.php" class="btn_normal" style="float:left; margin:5px;">Volver </a>
</p>

<form action="#" method="post">
<table width="100%" height="48" border="0">
  <tr>
    <td width="33%" valign="top">
        <table>
            <tr>
                <td><input type="text" name="prod_name" id="prod_name" placeholder="Buscar Producto"/></td>
                <td><input type="button" value="Buscar" class="btn_3" onClick="findProduct2(<?php echo $id?>)" /></td>
            </tr>
            <tr>
                <td colspan="2">
                    <div id="listprod"></div>
                </td>
            </tr>
        </table>
    </td>
    <td width="33%" valign="top">
        <div id="det">
            <table>
                <tr>
                    <th>ID</th>
                    <th>Nombre</th>
                    <th>Cantidad</th>
                    <th>&nbsp;</th>
                </tr>
                <?php
                    $row = $obj->getProdOrderDetail($id);
                    if(count($row)>0)
                    {
                        foreach($row as $row)
                        {
                            ?>
                            <tr>
                                <td><?php echo $row['prdod_id']?></td>
                                <td><?php echo $row['prdod_productname']?></td>
                                <td><input type='text' name='qty' value='<?php echo $row['prdod_qty']?>' onChange='chgQty2(this.value,<?php echo $row['prdod_id']?>,<?php echo $id?>)' /></td>
                                <td><a href="javascript:;" class="btn_2" onClick='delProduct2(<?php echo $row['prdod_id']?>,<?php echo $id?>)'>Eliminar</a></td>
                            </tr>
                            <?php
                        }
                    }
                ?>
            </table>
        </div>
    </td>
    <td width="33%" valign="top">
        <table>
            <tr>
                <td>Producto a producir:<br>
                    <select name="id_product" required>
                        <option value="">--Producto--</option>
                        <?php
                            $row = $obj->getProducts();
                            if(count($row)>0)
                            {
                                foreach($row as $row)
                                {
                                    if($id_product == $row['pro_id'])
                                    {
                                      ?>
                                        <option value="<?php echo $row['pro_id']?>" selected><?php echo $row['pro_name']?></option>
                                        <?php  
                                    }
                                    else
                                    {
                                        ?>
                                        <option value="<?php echo $row['pro_id']?>"><?php echo $row['pro_name']?></option>
                                        <?php
                                    }
                                    
                                }
                            }
                        ?>
                    </select>
                </td>
            </tr>
            <tr>
                <td>Cantidad a producir:<br>
                    <input type="text" name="qty" value="<?php echo $qty?>" required /> 
                </td>
            </tr>
            <tr>
                <td>Descripcion:<br>
                    <textarea cols="40" rows="6" name="desc" required><?php echo $desc?></textarea>
                </td>
            </tr>
            <tr>
                <td>Observaciones:<br>
                    <textarea cols="40" rows="6" name="obs"><?php echo $obs?></textarea>
                </td>
            </tr>
            <tr>
                <td>
                    <input type="hidden" name="id" value="<?php echo $id?>" />
                    <input type="submit" value="Guardar" class="btn_submit" />
                </td>
            </tr>
        </table>
    </td>
  </tr>
  
</table>
</form>

</div>