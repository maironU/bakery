<?php
    include('include_self.php');
    
    $id = $obj->postVars('id');
    $id_po = $obj->postVars('id_po');
    
    $obj->delProdOrderDetail($id);
    
    $htm = "<table>
            <tr>
                <th>ID</th>
                <th>Nombre</th>
                <th>Cantidad</th>
                <th>&nbsp;</th>
            </tr>
    ";
    $row = $obj->getProdOrderDetail($id_po);
    if(count($row)>0)
    {
        foreach($row as $row)
        {
            $htm .= "
            <tr>
                <td>".$row['prdod_id']."</td>
                <td>".$row['prdod_productname']."</td>
                <td><input type='text' name='qty' value='".$row['prdod_qty']."' onChange='chgQty2(this.value,".$row['prdod_id'].",".$id_po.")' /></td>
                <td><a href='javascript:;' class='btn_2' onClick='delProduct2(".$row['prdod_id'].",".$id_po.")'>Eliminar</a></td>
            </tr>";
        }
    }
    else
    {
        $htm .= "
            <tr>
                <td clspan='4'>No se encuentra productos</td>
            </tr>";
    }
    
    $htm .= "</table>";
    
    echo $htm;
    
?>