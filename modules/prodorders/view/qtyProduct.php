<?php
    include('include_self.php');
    
    $id = $obj->postVars('id');
    $qty = $obj->postVars('qty');
    
    $obj->editQtyOrderDetailTemp($id, $qty);
    
    $htm = "<table>
            <tr>
                <th>ID</th>
                <th>Nombre</th>
                <th>Cantidad</th>
                <th>&nbsp;</th>
            </tr>
    ";
    $row = $obj->getProdOrderDetailTemp($_SESSION['user_id']);
    if(count($row)>0)
    {
        foreach($row as $row)
        {
            $htm .= "
            <tr>
                <td>".$row['prdodt_id']."</td>
                <td>".$row['prdodt_productname']."</td>
                <td><input type='text' name='qty' value='".$row['prdodt_qty']."' onChange='chgQty(this.value,".$row['prdodt_id'].")' /></td>
                <td><a href='javascript:;' class='btn_2' onClick='delProduct(".$row['prdodt_id'].")'>Eliminar</a></td>
            </tr>";
        }
    }
    else
    {
        $htm .= "
            <tr>
                <td clspan='4'>No se encuentra productos</td>
            </tr>";
    }
    
    $htm .= "</table>";
    
    echo $htm;
    
?>