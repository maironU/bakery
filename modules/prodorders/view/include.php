<?php
    include('modules/prodorders/model/prodorders.php');
    include('modules/inventory/model/inventory.php');
    include('modules/products/model/products.php');
    
    $obj = new prodorders();
    $obj->connect();
    
    $inv = new inventory();
    $inv->connect();
    
    $products = new products();
    $products->connect();
?>