<?php
    session_start();
    include('../../../core/config.php');
    include('../../../core/ge.php');
    include('../model/prodorders.php');
    include('../../inventory/model/inventory.php');
    include('../../products/model/products.php');
    include('../../crm/model/crm.php');
    
    $obj = new prodorders();
    $obj->connect();
    
    $inv = new inventory();
    $inv->connect();
    
    $products = new products();
    $products->connect();
?>