function findProduct()
{
	var term=document.getElementById('prod_name').value;
	$.ajax({
	 type:'POST',
	 url:'modules/prodorders/view/findProduct.php',
	 async:false,
	 data: 'term='+term,
	 success:function(data){
		 document.getElementById('listprod').innerHTML=data;
	 }
	});
 }
 
function findProduct2(id)
{
	var term=document.getElementById('prod_name').value;
	$.ajax({
	 type:'POST',
	 url:'modules/prodorders/view/findProduct2.php',
	 async:false,
	 data: 'term='+term+"&id="+id,
	 success:function(data){
		 document.getElementById('listprod').innerHTML=data;
	 }
	});
 }
 
 function addProduct(id)
 {
     var qty = document.getElementById('qty'+id).value;
     $.ajax({
	 type:'POST',
	 url:'modules/prodorders/view/addProduct.php',
	 async:false,
	 data: 'id_product='+id+"&qty="+qty,
	 success:function(data){
		 document.getElementById('det').innerHTML=data;
	 }
	});
     
 }
 
 function addProduct2(id, id_po)
 {
     var qty = document.getElementById('qty'+id).value;
     $.ajax({
	 type:'POST',
	 url:'modules/prodorders/view/addProduct2.php',
	 async:false,
	 data: 'id_product='+id+"&qty="+qty+"&id_po="+id_po,
	 success:function(data){
		 document.getElementById('det').innerHTML=data;
	 }
	});
     
 }
 
 function chgQty(qty, id)
 {
     $.ajax({
	 type:'POST',
	 url:'modules/prodorders/view/qtyProduct.php',
	 async:false,
	 data: 'id='+id+"&qty="+qty,
	 success:function(data){
		 document.getElementById('det').innerHTML=data;
	 }
	});
     
 }
 
 function chgQty2(qty, id, id_po)
 {
     $.ajax({
	 type:'POST',
	 url:'modules/prodorders/view/qtyProduct2.php',
	 async:false,
	 data: 'id='+id+"&qty="+qty+"&id_po="+id_po,
	 success:function(data){
		 document.getElementById('det').innerHTML=data;
	 }
	});
     
 }
 
 function delProduct(id)
 {
     $.ajax({
	 type:'POST',
	 url:'modules/prodorders/view/delProduct.php',
	 async:false,
	 data: 'id='+id,
	 success:function(data){
		 document.getElementById('det').innerHTML=data;
	 }
	});
     
 }
 
 function delProduct2(id, id_po)
 {
     $.ajax({
	 type:'POST',
	 url:'modules/prodorders/view/delProduct2.php',
	 async:false,
	 data: 'id='+id+"&id_po="+id_po,
	 success:function(data){
		 document.getElementById('det').innerHTML=data;
	 }
	});
     
 }
 
 function showInfo(id,site)
 {
	$.fancybox.open({
					href : site+'?id='+id,
					type : 'iframe',
					padding:5,
					afterClose: function () { // USE THIS IT IS YOUR ANSWER THE KEY WORD IS "afterClose"
                		parent.location.reload(true);
					}
	}); 
 }