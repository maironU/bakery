<?php
    class prodorders extends GeCore
    {
        //Prod Orders Methods
        public function newProdOrder()
        {
            
            $desc = $this->postVars('desc');
            $obs = $this->postVars('obs');
            $qty = $this->postVars('qty');
            $id_product = $this->postVars('id_product');
            
            $date = date('Y-m-d H:i:s');
            $track = $this->genTrack();
            
            $sql = "INSERT INTO `prodorders`(`prdo_date`, `u_id`, `prdo_track`, `prdo_desc`, `prdo_obs`, `prdo_active`, `prdo_qtyprod`, `pro_id`) VALUES ('$date','".$_SESSION['user_id']."','$track','$desc','$obs','1','$qty','$id_product')";
            $this->Execute($sql);
            $id = $this->getLastID();
            return $id;
        }
        
        public function editProdOrder()
        {
            
            $id = $this->postVars('id');
            $desc = $this->postVars('desc');
            $obs = $this->postVars('obs');
            $qty = $this->postVars('qty');
            $id_product = $this->postVars('id_product');
            
            
            $sql = "UPDATE `prodorders` SET `prdo_desc`='$desc', `prdo_obs`='$obs', `prdo_qtyprod`='$qty', `pro_id`='$id_product' WHERE prdo_id='$id'";
            $this->Execute($sql);
        }
        
        public function editProdOrdeActive($id, $active)
        {
            $sql = "UPDATE `prodorders` SET `prdo_active`='$active' WHERE prdo_id='$id'";
            $this->Execute($sql);
        }
        
        public function delProdOrder()
        {
            
            $id = $this->getVars('id');
           
            $sql = "DELETE FROM `prodorders` WHERE prdo_id='$id'";
            $this->Execute($sql);
        }
        
        public function getProdOrder()
        {
            $sql = "SELECT * FROM `prodorders` ORDER BY prdo_date DESC";
            return $this->ExecuteS($sql);
        }
        
        public function getProdOrderById($id)
        {
            $sql = "SELECT * FROM `prodorders` WHERE prdo_id='$id'";
            return $this->ExecuteS($sql);
        }
        
        public function printProdOrder($id, $path='../formats/')
    	{
    	    $row = $this->getProdOrderById($id);
    	    if(count($row)>0)
    	    {
    	        foreach($row as $row)
    	        {
    	            $track = $row['prdo_track'];
    	            $date = $row['prdo_date'];
    	            $desc = $row['prdo_desc'];
    	            $qtyprod = $row['prdo_qtyprod'];
    	            $obs = $row['prdo_obs'];
    	            $id_product = $row['pro_id'];
    	            $id_user = $row['u_id'];
    	        }
    	    }
    	    
    	    $sql = "select * from user where u_id='$id_user'";
    	    $row = $this->ExecuteS($sql);
    	    if(count($row)>0)
    	    {
    	        foreach($row as $row)
    	        {
    	            $name_user = $row['u_nom'];
    	        }
    	    }
    	    
    	    $sql = "select * from products where pro_id='$id_product'";
    	    $row = $this->ExecuteS($sql);
    	    if(count($row)>0)
    	    {
    	        foreach($row as $row)
    	        {
    	            $name_product = $row['pro_name'];
    	        }
    	    }
    	    
    	    $row = $this->getProdOrderDetail($id);
    	    if(count($row)>0)
    	    {
    	        foreach($row as $row)
    	        {
    	            $det .= '
    	                <tr>
    	                    <td>'.$row['prdod_productname'].'</td>
    	                    <td>'.$row['prdod_qty'].'</td>
    	                </tr>
    	            ';
    	        }
    	    }
    	    
    	    $f=fopen($path.'orderformat.html','r');
    	    $html=fread($f,filesize($path.'/orderformat.html'));
    	    
    	    $html=str_replace('[track]',$track,$html);
    	    $html=str_replace('[date]',$date,$html);
    	    $html=str_replace('[obs]',$obs,$html);
    	    $html=str_replace('[desc]',$desc,$html);
    	    $html=str_replace('[product_name]',$name_product,$html);
    	    $html=str_replace('[qtyprod]',$qtyprod,$html);
    	    $html=str_replace('[user_name]',$name_user,$html);
    	    $html=str_replace('[detalle]',$det,$html);
    	    
    	    $crmModelObj=new crmScs();
    	    $crmModelObj->connect();
    	 
        	 $row=$crmModelObj->getCompanyById(2);
        	 if(count($row)>0)
        	 {
        	     foreach($row as $row)
        	     {
        	        $comInf.='<table width="163" border="0" align="center">
                      <tr>
                        <td><div align="center"><img src="../../crm/Logos/'.$row["e_img"].'" /><br />
                        </div></td>
                      </tr>
                      <tr>
                        <td><div align="center">'.$row["e_info"].'</div></td>
                      </tr>
                    </table>';
        	    }
    	    }
    	    
    	    $html=str_replace('[company]',$comInf,$html);
    	    
    	    return $html;
    	    
    	    
    	}
        
        //Prod Orders Detail Methods
        public function newProdOrderDetail($id, $id_product, $id_inv, $product_name, $qty, $price)
        {
            $sql = "INSERT INTO `prodorders_detail`(`prdo_id`, `pro_id`, `inv_id`, `prdod_productname`, `prdod_qty`, `prdod_price`) VALUES ('$id','$id_product','$id_inv','$product_name','$qty','$price')";
            $this->Execute($sql);
        }
        
        public function delProdOrderDetail($id)
        {
            $sql = "DELETE FROM `prodorders_detail` WHERE prdod_id='$id'";
            $this->Execute($sql);
        }
        
        public function getProdOrderDetail($id)
        {
            $sql = "SELECT * FROM `prodorders_detail` WHERE prdo_id='$id'";
            return $this->ExecuteS($sql);
        }
        
        public function editQtyOrderDetail($id, $qty)
        {
            $sql = "UPDATE `prodorders_detail` SET prdod_qty='$qty' WHERE prdod_id='$id'";
            $this->Execute($sql);
        }
        
        //Prod Orders Detail Temp Methods
        public function newProdOrderDetailTemp($id_user, $id_product, $id_inv, $product_name, $qty, $price)
        {
            $sql = "INSERT INTO `prodorders_detail_temp`(`u_id`, `pro_id`, `inv_id`, `prdodt_productname`, `prdodt_qty`, `prdodt_price`) VALUES ('$id_user','$id_product','$id_inv','$product_name','$qty','$price')";
            $this->Execute($sql);
        }
        
        public function delProdOrderDetailTemp($id)
        {
            $sql = "DELETE FROM `prodorders_detail_temp` WHERE prdodt_id='$id'";
            $this->Execute($sql);
        }
        
        public function getProdOrderDetailTemp($id)
        {
            $sql = "SELECT * FROM `prodorders_detail_temp` WHERE u_id='$id'";
            return $this->ExecuteS($sql);
        }
        
        public function editQtyOrderDetailTemp($id, $qty)
        {
            $sql = "UPDATE `prodorders_detail_temp` SET prdodt_qty='$qty' WHERE prdodt_id='$id'";
            $this->Execute($sql);
        }
        
        public function flushOrderDetailTemp($id)
        {
            $sql = "DELETE FROM `prodorders_detail_temp` WHERE u_id='$id'";
            $this->Execute($sql);
        }
        
        public function genTrack()
        {
            $str = "ABCDEFGHIJKMNOPQRSTXYZ0987654321";
            for($i=0; $i<6; $i++)
            {
                $pos = rand(0, strlen($str));
                $t .= $str[$pos];
            }
            
            return $t;
        }
        
        //Miselaneo
        public function getProducts()
        {
            $sql = "SELECT * FROM products WHERE cat_id <> '14' ORDER BY pro_name ASC";
            return $this->ExecuteS($sql);
        }
        
        public function findProducts($name)
        {
            $sql = "SELECT * FROM products WHERE pro_name LIKE '%$name%' AND cat_id <> '14' ORDER BY pro_name ASC";
            return $this->ExecuteS($sql);
        }
        
        //Installers
       public function Checker()
       {
           $res=$this->chkTables('prodorders');
           if($res)
           {
        	 return true;
           }
           else
           {
               return false;
           }
       }
       
       public function install()
       {
           $sql = "CREATE TABLE `prodorders` (
              `prdo_id` int(11) NOT NULL PRIMARY KEY AUTO_INCREMENT,
              `prdo_date` datetime NOT NULL,
              `u_id` int(11) NOT NULL,
              `prdo_track` varchar(255) NOT NULL,
              `prdo_desc` longtext NOT NULL,
              `prdo_obs` longtext NOT NULL,
              `prdo_active` int(11) NOT NULL,
              `prdo_qtyprod` int(11) NOT NULL,
              `pro_id` int(11) NOT NULL
            ) ENGINE=MyISAM DEFAULT CHARSET=utf8;";
            $this->Execute($sql);
            
            $sql = "CREATE TABLE `prodorders_detail` (
              `prdod_id` int(11) NOT NULL PRIMARY KEY AUTO_INCREMENT,
              `prdo_id` int(11) NOT NULL,
              `pro_id` int(11) NOT NULL,
              `inv_id` int(11) NOT NULL,
              `prdod_productname` varchar(255) NOT NULL,
              `prdod_qty` int(11) NOT NULL,
              `prdod_price` varchar(255) NOT NULL
            ) ENGINE=MyISAM DEFAULT CHARSET=utf8;";
            $this->Execute($sql);
            
            $sql = "CREATE TABLE `prodorders_detail_temp` (
              `prdodt_id` int(11) NOT NULL PRIMARY KEY AUTO_INCREMENT,
              `u_id` int(11) NOT NULL,
              `pro_id` int(11) NOT NULL,
              `inv_id` int(11) NOT NULL,
              `prdodt_productname` varchar(255) NOT NULL,
              `prdodt_qty` int(11) NOT NULL,
              `prdodt_price` varchar(255) NOT NULL
            ) ENGINE=MyISAM DEFAULT CHARSET=utf8;
            ";
            $this->Execute($sql);
       }
       
       public function uninstall()
       {
          $sql='DROP TABLE `prodorders`,`prodorders_detail` ,`prodorders_detail_temp`';
          $this->Execute($sql);
       }
        
    };
?>