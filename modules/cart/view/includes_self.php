<?php
    session_start();
    include('../../../core/config.php');
    include('../../../core/ge.php');
    include('../model/cart.php');
    include('../../../usr/model/User.php');
    include('../../products/model/products.php');
    include('../../crm/model/ordersModel.php');
    include('../../crm/model/customerModel.php');
    
    $obj = new cart();
    $obj->connect();
    
    $usr = new User();
    $usr->connect();
    
    $products = new products();
    $products->connect();
    
    $orders = new orderModelGe();
    $orders->connect();
    
    $customer = new customerModelGe;
    $customer->connect();
?>