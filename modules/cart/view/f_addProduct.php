<?php
    include('includes_self.php');
    
    $id_product = $obj->postVars('id_product');
    $id_cart = $obj->postVars('id_cart');
    $id_customer = $obj->postVars('id_customer');
    $session = $obj->postVars('session');
    $qty = $obj->postVars('qty');
    
    
    if($id_cart == '0')
    {
        $id_cart = $obj->getCustomerActiveCart($id_customer, $session);
    }
    
    
    $row = $products->getProductsById($id_product);
    if(count($row)>0)
    {
        foreach($row as $row)
        {
            $name = $row['pro_name'];
            $desc = $row['pro_sdesc'];
            $price = $row['pro_price'];
            $id_tax = $row['prot_id'];
        }
    }
    
    $tax = $products->calcTaxValue($price, $id_tax);
    
    $obj->newCartDetail($id_cart, $id_product, $name, $desc, $price, $qty, $trm, $id_currency, $session, $tax);
    
    $_SESSION['id_cart'] = $id_cart;
    
    
    $htm = "<ul>";
    
    $row = $obj->getCartDetailByCustomer($id_customer, $session);
    if(count($row)>0)
    {
        foreach($row as $row)
        {
            $htm .= "
                <li>".$row['crd_name']."....$".number_format($row['crd_qty']*$row['crd_price'])."
                <a href='javascript:;' onClick='removeProd(".$row['crd_id'].",".$id_cart.",".$id_customer.")'><i class='fa fa-remove' style='color: #f00'></i></a>
                </li>
            ";
        }
        $htm .= "<li><h3>Total: $".$obj->getCartTotalByCustomer($id_customer, 1)."</h3></li>";
        $htm .= "<li><a href='javascript:;' onClick='newOrder(".$id_cart.", ".$id_customer.")' class='btn btn-tobais btn-special'>Hacer Pedido y Pagar</a></li>";
    }
    else
    {
        $htm .= "<li>No hay productos en carrito</li>"; 
    }
    
    $htm .= "</ul>";
    
    echo $htm;
?>