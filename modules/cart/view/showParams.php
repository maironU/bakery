<?php
    include('modules/notifications/model/notifications.php');
 
    $obj = new notifications();
    $obj->connect();
    
    if($_POST)
    {
        $id = $obj->postVars('id');
        $value = $obj->postVars('value');
        $type = $obj->postVars('type');
        
        $obj->editParam($id, $value, $type);
    }
 
?>
<div class="widget3">
<div class="widgetlegend">Parametros generales</div>
<p style="width:100">
	<a href="<?php $_SERVER['PHP_SELF'];?>?p=modules/notifications/view/index.php" class="btn_normal" style="float:left; margin:5px;">Volver </a>
</p>
 <br /><br /><br />

<table width="100%" height="48" border="0" align="center">
  <tr>
    <th width="30">ID</th>
    <th width="213">Nombre</th>
	<th width="213">Valor</th>
    <th colspan="2">Acciones</th>
  </tr>
  <?php
   $row=$obj->getParams();
   $i=1;
   if(count($row)>0)
   {
       foreach($row as $row)
       {
          ?>
          <form action="#" method="post" name="form<? echo $i; ?>" enctype="multipart/form-data">
          <tr>
            <td><?php echo $row["np_id"];?><input type="hidden" name="id" id="id" value="<?php echo $row["np_id"];?>" /></td>
        	<td><?php echo $row["np_name"];?></td>
        	<td>
        	    <?php
        	        if($row['np_type'] == 'file')
        	        {
        	            ?>
        	            <input type="file" name="img" />
        	            <img src="modules/notifications/imagesLogo/<?php echo $row['np_value']?>" />
        	            <?php
        	        }
        	        else
        	        {
        	            ?>
        	            <input type="text" name="value" id="value" value="<?php echo $row["np_value"];?>" />
        	            <?php
        	        }
        	    ?>
        	</td>
            <td width="86">
                <input type="hidden" name="type" value="<?php echo $row['np_type']?>" />
                <a href="javascript:;" onclick="document.form<? echo $i; ?>.submit();" class="btn_normal">Cambiar</a></td>
          </tr>
          </form>
          <?php
          $i++;
       }
  } 
  ?>
</table>

</div>
