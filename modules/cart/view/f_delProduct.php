<?php
    include('includes_self.php');
    
    $id = $obj->postVars('id');
    $id_cart = $obj->postVars('id_cart');
    $id_customer = $obj->postVars('id_customer');
    $session = $obj->postVars('session');
    $folder = $obj->postVars('folder');
    
    $obj->delCartDetail($id);
    
    $id_cart = $obj->getCustomerActiveCart($id_customer, $session);
    
    $htm = '
    <div id="cart_list">
    <table class="table">
                            <thead>
                                <tr>
                                    <th>Images</th>
                                    <th>Product Name</th>
                                    <th>Price</th>
                                    <th>Quantity</th>
                                    <th>Total</th>
                                    <th>Remove</th>
                                </tr>
                            </thead>
                            <tbody>';
    
    $row = $obj->getCartDetail($id_cart);
    if(count($row)>0)
    {
        foreach($row as $row)
        {
            $htm .= '
            <tr>
                <td class="thumbnail-img">
                    <a href="productDetail.php?id_product='.$row['id_product'].'"><img class="img-fluid" src="'.$folder.'/modules/products/imagesProd/'.$row['pro_image'].'" alt="" /></a>
                </td>
                <td class="name-pr">
                    <a href="productDetail.php?id_product='.$row['id_product'].'">'.utf8_encode($row['pro_name']).'</a>
                </td>
                <td class="price-pr">
                    <p>$ '.number_format($row['crd_price'],2,'.',',').'</p>
                </td>
                <td class="quantity-box">
                    <input type="number" size="4" value="'.$row['crd_qty'].'" min="0" step="1" class="c-input-text qty text" onChange="updQty('.$row['crd_id'].', this.value, \''.$folder.'\', '.$id_customer.', \''.$session.'\')">
                </td>
                <td class="total-pr">
                    <p>$ '.number_format($row['crd_price']*$row['crd_qty'],2,'.',',').'</p>
                </td>
                <td class="remove-pr">
                    <a href="javascript:;" onClick="removeProd('.$row['crd_id'].', '.$id_cart.', '.$id_customer.', \''.$folder.'\', \''.$session.'\')"><i class="fas fa-times"></i></a>
                </td>
            
            </tr>';
        }
    }
    else
    {
        $htm .= "<tr><td>No hay productos en carrito</td></tr>"; 
    }
    
    $htm .= "</tbody>
                        </table>
                        </div>";
    
    echo $htm;
?>