<?php
    include('includes_self.php');
    
    $id_cart = $obj->postVars('id_cart');
    
    $total = $obj->getCartTotalByUser($_SESSION['user_id'], 0);
    $obj->editCart($id_cart, $total, $tax, $total, $trm);
    
    $row = $obj->getCartById($id_cart);
    if(count($row)>0)
    {
        foreach($row as $row)
        {
            $id_customer = $row['c_id'];
        }
    }
    
    $row = $geo->getCompaniesById($id_customer);
    if(count($row)>0)
    {
        foreach($row as $row)
        {
            $name = $row['com_name'];
            $email = $row['com_email'];
            $phone = $row['com_phone'];
        }
    }
    
    $id_order = $ol->newOrderLite2($name, $email, $phone, $desc, $obs, $deliverdate, $deliverhour, $id_store, $id_customer, $total, $tax, $total, $id_cart);
    
    $row = $obj->getCartDetailByUser($_SESSION['user_id']);
    if(count($row)>0)
    {
        foreach($row as $row)
        {
            $ol->newOrderLiteDetail($id_order, $row['id_product'], $row['crd_name'], $row['crd_qty'], $row['crd_price'], $row['crd_tax'], $_SESSION['user_id']);
        }
    }
    
    $obj->editCartActive($id_cart, 0);
    
    $row = $ol->getOrderLiteById($id_order);
    if(count($row)>0)
    {
        foreach($row as $row)
        {
            $track = $row['ol_track'];
        }
    }
    
    $htm = '
    <table>
        <tr>
            <td><i class="fa fa fa-check-circle" style="color: #f00; font-size: 24px"></i></td>
            <td><h2>PEDIDO NO. '.$track.'</h2><td>
        </tr>
        <tr>
            <td colspan="2"><a href="#" class="btn_1">Pagar Order</a></td>
        </tr>
    </table>
    ';
    
    echo $htm;
    
?>