<?php
    include('includes_self.php');
    
    $id_cart = $obj->postVars('id_cart');
    $id_customer = $obj->postVars('id_customer');
    $id_contact = $obj->postVars('id_contact');
    
    $total = $obj->getCartTotalByCustomer($id_customer, 0);
    $tax = $obj->getCartTaxByCustomer($id_customer, 0);
    $subto = $obj->getCartSubtoByCustomer($id_customer, 0);
    
    $obj->editCart($id_cart, $subto, $tax, $total, $trm);
    
    $row = $customer->getCustomerById2($id_customer);
    if(count($row)>0)
    {
        foreach($row as $row)
        {
            $name = $row['c_nom'];
            $phone = $row['c_tel'];
        }
    }
    
    $row = $customer->getCustomerContact($id_contact);
    if(count($row)>0)
    {
        foreach($row as $row)
        {
            $email = $row['cc_mail'];
        }
    }
    
    $track=$orders->getParam(2);
    $id_order = $orders->newOrder2($track, 2, $id_customer, $id_contact, 1, $ini, $subto, $subto, $tax, $total, $obs, $creacion, 1, 0, "Venta por tienda en Linea");
    $orders->editParam(2);
    
    $row = $obj->getCartDetail($id_cart);
    if(count($row)>0)
    {
        foreach($row as $row)
        {
            $total = $row['crd_qty'] * $row['crd_price'];
            
            $orders->newOrderDetailItem($id_order,$row['id_product'],$row['crd_name'],$row['crd_price'],$row['crd_qty'],0,1,1,$total);
        }
    }
    
    $obj->editCartActive($id_cart, 0);
    
    echo $id_order;
    
    
    
?>