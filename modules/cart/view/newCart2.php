<?php
  include('modules/cart/view/include.php');
 
 $msg=false;
 
 if($_POST)
 {
 }
 
 $id = $obj->getVars('id');

?>
<script src="modules/cart/js/back_cart.js"></script>
<div class="widget3">
 <div class="widgetlegend">Nuevo Carrito </div>
 <?php
  if($msg)
  {
  ?>
   <div class="ui-widget">
	<div class="ui-state-highlight ui-corner-all" style="margin-top: 20px; padding: 0 .7em;">
		<p><span class="ui-icon ui-icon-info" style="float: left; margin-right: .3em;"></span>
		<strong>Felicitaciones!</strong> Se ha sido creado satisfactoriamente.</p>
	</div>
</div>
  <?php
  }
 ?>
<p style="width:100">
	<a href="<?php $_SERVER['PHP_SELF'];?>?p=modules/cart/view/index.php" class="btn_normal" style="float:left; margin:5px;">Volver </a>
</p>
 
<table>
    <tr>
        <td valign="top" width="33%">
            <h3>Buscar productos</h3>
            <input type="text" name="prod_name" id="prod_name"/><br>
            <input type="button" value="Buscar" onClick="findProd(<?php echo $id?>)" class="btn_normal" />
            <div id="list_prod"></div>
        </td>
        <td valign="top" width="33%">
            <h2>Lista de productos en carrito</h2>
            <div id="cart_list">
                <table>
                    <?php
                        $row = $obj->getCartDetail($id);
                        if(count($row)>0)
                        {
                            foreach($row as $row)
                            {
                                ?>
                                <tr>
                                    <td><?php echo $row['crd_name']?></td>
                                    <td><?php echo $row['crd_desc']?></td>
                                    <td><input type="text" name="qty" onChange="updQty(<?php echo $row['crd_id']?>, this.value, <?php echo $id?>)" value="<?php echo $row['crd_qty']?>" /></td>
                                    <td>$<?php echo number_format($row['crd_price'])?></td>
                                    <td>$<?php echo number_format($row['crd_qty']*$row['crd_price'])?></td>
                                    <td><a href="javascript:;" onClick="removeProd(<?php echo $row['crd_id']?>, <?php echo $id?>)"><i class="fa fa-remove" style="color: #f00"></i></a></td>
                                </tr>
                                <?php
                            }
                        }
                    ?>
                </table>
            </div>
        </td>
        <td valign="top" width="33%">
            <h1>Total ($):<div id="cart_total"><?php echo $obj->getCartTotal($id, 1)?></div></h1>
            <input type="button" value="Crear Pedido" class="btn_submit" onClick="newOrder(<?php echo $id?>)" id="btnOrder" />
            <div id="orderInfo"></div>
        </td>
    </tr>
</table>


</div>
