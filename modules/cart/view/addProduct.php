<?php
    include('includes_self.php');
    
    $id_product = $obj->postVars('id_product');
    $id_cart = $obj->postVars('id_cart');
    
    $row = $products->getProductsById($id_product);
    if(count($row)>0)
    {
        foreach($row as $row)
        {
            $name = $row['pro_name'];
            $desc = $row['pro_sdesc'];
            $price = $row['pro_price'];
        }
    }
    
    $obj->newCartDetail($id_cart, $id_product, $name, $desc, $price, 1, $trm, $id_currency);
    
    
    $htm = "<table>";
    
    $row = $obj->getCartDetail($id_cart);
    if(count($row)>0)
    {
        foreach($row as $row)
        {
            $htm .= "
                <tr>
                    <td>".$row['crd_name']."</td>
                    <td>".$row['crd_desc']."</td>
                    <td><input type='text' name='qty' onChange='updQty(".$row['crd_id'].", this.value, ".$id_cart.")' value='".$row['crd_qty']."' /></td>
                    <td>$".number_format($row['crd_price'])."</td>
                    <td>$".number_format($row['crd_qty']*$row['crd_price'])."</td>
                    <td><a href='javascript:;' onClick='removeProd(".$row['crd_id'].", ".$id_cart.")'><i class='fa fa-remove' style='color: #f00'></i></a></td>
                </tr>
            ";
        }
    }
    
    $htm .= "</table>";
    
    echo $htm;
?>