<?php
    include('includes_self.php');
    
    $id = $obj->postVars('id');
    $qty = $obj->postVars('qty');
    $id_cart = $obj->postVars('id_cart');
    
    $obj->editCartDetailQty($id, $qty);
    
    $htm = "<table>";
    
    $row = $obj->getCartDetail($id_cart);
    if(count($row)>0)
    {
        foreach($row as $row)
        {
            $htm .= "
                <tr>
                    <td>".$row['crd_name']."</td>
                    <td>".$row['crd_desc']."</td>
                    <td><input type='text' name='qty' onChange='updQty(".$row['crd_id'].", this.value, ".$id_cart.")' value='".$row['crd_qty']."' /></td>
                    <td>$".number_format($row['crd_price'])."</td>
                    <td>$".number_format($row['crd_qty']*$row['crd_price'])."</td>
                    <td><a href='javascript:;' onClick='removeProd(".$row['crd_id'].", ".$id_cart.")'><i class='fa fa-remove' style='color: #f00'></i></a></td>
                </tr>
            ";
        }
    }
    
    $htm .= "</table>";
    
    echo $htm;
?>