<?php
    include('includes_self.php');
    
    $id_customer = $obj->postVars('id_customer');
    $session = $obj->postVars('session');
    $folder = $obj->postVars('folder');
    
    $id_cart = $obj->getCustomerActiveCart($id_customer, $session);
    
    $row = $obj->getCartDetail($id_cart);
    $subto = 0;
    $tax = 0;
    if(count($row)>0)
    {
        foreach($row as $row)
        {
            $s = $row['crd_price'] * $row['crd_qty'];
            
            if($row['prot_id']!=0)
            {
                $row1 = $products->getTaxById($row['prot_id']);
                if(count($row1)>0)
                {
                    foreach($row1 as $row1)
                    {
                        $t = $row1['prot_value'];
                    }
                }
                
                $t /= 100;
                $t1 = $s * $t;
                $tax += $t1;
            }
            
            $subto += $s;
        }
    }
    
    $gtotal = $subto + $tax;

    $htm = '
    <div class="col-lg-12 col-sm-12">
                    <div class="order-box">
                        <h3>Order summary</h3>
                        <div class="d-flex">
                            <h4>Sub Total</h4>
                            <div class="ml-auto font-weight-bold"> $'.number_format($subto,2,'.',',').' </div>
                        </div>
                        <hr class="my-1">
                        <div class="d-flex">
                            <h4>Tax</h4>
                            <div class="ml-auto font-weight-bold"> $'.number_format($tax,2,'.',',').' </div>
                        </div>
                        <div class="d-flex">
                            <h4>Shipping Cost</h4>
                            <div class="ml-auto font-weight-bold"> Free </div>
                        </div>
                        <hr>
                        <div class="d-flex gr-total">
                            <h5>Grand Total</h5>
                            <div class="ml-auto h5"> $'.number_format($gtotal,2,'.',',').' </div>
                        </div>
                        <hr> </div>
                </div>
 ';
    
    echo $htm;
?>