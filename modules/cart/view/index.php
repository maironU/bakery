<?php
 include('modules/cart/view/include.php');
 
  //Se verifica si el modulo esta instalado
 if(!$obj->Checker()){
  $ins=true;
 }
 
 $msg=false;
 
 if($obj->getVars('ActionDel')){
  $obj->delCart($obj->getVars('id'));
  $msg=true;
 }
 
 if($obj->getVars('action') == 'active'){
  $obj->editCartActive($obj->getVars('id'), $obj->getVars('act'));
  $msg=true;
 }
 
 //Instala el modulo
 if($obj->getVars('install')){
  $obj->install();
  $msgIns=true;
 }

?>
<?php
 if(!$ins){
 ?>
 <div class="widget3">
 <div class="widgetlegend">Carritos de compras </div>
 <?php
  if($msg)
  {
  ?>
   <div class="ui-widget">
	<div class="ui-state-highlight ui-corner-all" style="margin-top: 20px; padding: 0 .7em;">
		<p><span class="ui-icon ui-icon-info" style="float: left; margin-right: .3em;"></span>
		<strong>Felicitaciones!</strong> El slide ha sido eliminado satisfactoriamente.</p>
	</div>
</div>
  <?php
  }
 ?>
<p style="width:100">
	<a href="<?php $_SERVER['PHP_SELF'];?>?p=modules/cart/view/newCart.php" class="btn_normal" style="float:left; margin:5px;">Nuevo </a>
	<!--<a href="<?php $_SERVER['PHP_SELF'];?>?p=modules/notifications/view/showParams.php" class="btn_normal" style="float:left; margin:5px;">Parametros </a>-->
</p>
 
<table width="803" height="48" border="0">
  <tr>
    <th width="30">ID</th>
    <th width="142">Cliente</th>
    <th width="213">Total</th>
    <th width="125">Tax</th>
    <th width="96">Subtotal</th>
    <th width="96">Usuario</th>
    <th width="96">Activo</th>
    <th colspan="3">Acciones</th>
  </tr>
  <?php
   $row=$obj->getCarts();
   
   if(count($row)>0){
 	foreach($row as $row){
  ?>
  <tr>
    <td><?php echo $row["cr_id"];?></td>
    <td>
        <?php
            $row1 = $customer->getCustomerById2($row["c_id"]);
            if(count($row1)>0)
            {
                foreach($row1 as $row1)
                {
                    echo $row1['c_nom'];
                }
            }
        ?>
    </td>
    <td>$<?php echo number_format($row["cr_total"]);?></td>
    <td>$<?php echo number_format($row["cr_tax"]);?></td>
    <td>$<?php echo number_format($row["cr_subto"]);?></td>
    <td>
        <?php
            $usr->getUserById($row["u_id"]);
            echo $usr->name;
        ?>
    </td>
    <td>
        <?php
            if($row["cr_active"]=='1')
            {
                $act = 0;
                ?>
                <i class="fa fa-circle" style="color:#0f0"></i>
                <?php
            }
            else
            {
                $act = 1;
                ?>
                <i class="fa fa-circle" style="color:#f00"></i>
                <?php
            }
        ?>
    </td>
    <td>
        <?php
            if($row["cr_active"]=='1')
            {
                ?>
                <a href="<?php $_SERVER['PHP_SELF'];?>?p=modules/cart/view/newCart2.php&id=<?php echo $row["cr_id"]?>" class="btn_normal">Continuar</a>
                <?php
            }
        ?>
    </td>
    <td width="86"><a href="<?php $_SERVER['PHP_SELF'];?>?p=modules/cart/view/index.php&id=<?php echo $row["cr_id"]?>&act=<?php echo $act?>&action=active" class="btn_normal">Activar/Desactivar</a></td>
    <td width="81"><a href="<?php $_SERVER['PHP_SELF'];?>?p=modules/cart/view/index.php&id=<?php echo $row["cr_id"]?>&ActionDel=true" class="btn_borrar">Eliminar</a></td>
  </tr>
  <?php
   }
  } 
  ?>
</table>

</div>

<?php } else {?>
<br />
 <div class="widget3">
 <div class="widgetlegend">Instalador Modulo Carrito de Compras </div>
 <?php
  if($msgIns)
  {
  ?>
   <div class="ui-widget">
	<div class="ui-state-highlight ui-corner-all" style="margin-top: 20px; padding: 0 .7em;">
		<p><span class="ui-icon ui-icon-info" style="float: left; margin-right: .3em;"></span>
		<strong>Felicitaciones!</strong> El modulo ha sido instalado satisfactoriamente.</p>
	</div>
</div>
  <?php
  }
 ?>
<form id="form1" name="form1" method="post" action="#">
  <table width="804" border="0" style="float:left;">
    <tr>
      <td width="525" valign="top">&nbsp; </td>
    </tr>
    <tr>
      <td valign="top"><div align="right"><a href="<?php $_SERVER['PHP_SELF'];?>?p=modules/cart/view/index.php&amp;install=true" class="btn_normal">Instalar</a></div></td>
    </tr>
  </table>
</form>
</div>
<?php } ?>

