<?php
    include('includes_self.php');
    
    $prod_name = $obj->postVars('prod_name');
    $id_cart = $obj->postVars('id_cart');
    
    $htm = "<table>";
    
    $row = $products->findProduct($prod_name);
    if(count($row)>0)
    {
        foreach($row as $row)
        {
            $htm .= "
                <tr>
                    <td>".$row['pro_name']."</td>
                    <td>".$row['pro_sdesc']."</td>
                    <td><a href='javascript:;' onClick='addProd(".$row['pro_id'].",".$id_cart.")' class='btn_1'>Agregar</a></td>
                </tr>
            ";
        }
    }
    else
    {
        $htm .= "<tr><td>No se encontraron productos</td></tr>";
    }
    
    $htm .= "</table>";
    
    echo $htm;
?>