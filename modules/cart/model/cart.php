<?php
class cart extends GeCore{

	public function newCartCustomer($id_customer, $id_user, $session)
	{
	    $today = date('Y-m-d H:i:s');
	    
	    $sql = "INSERT INTO cart (cr_date, c_id, u_id, cr_active, cr_session) VALUES ('$today', '$id_customer', '$id_user','1', '$session')";
	    $this->ExecuteS($sql);
	    
	    return $this->getLastID();
	}
	
	public function editCart($id, $total, $tax, $subto, $trm)
	{
	    $sql = "UPDATE cart SET cr_total='$total', cr_tax='$tax', cr_subto='$subto', cr_trm='$trm' WHERE cr_id='$id'";
	    $this->ExecuteS($sql);
	}
	
	public function editCartSessionToCustomer($session, $id_customer)
	{
	    $sql = "UPDATE cart SET c_id='$id_customer' WHERE cr_session='$session'";
	    $this->ExecuteS($sql);
	}
	
	public function editCartActive($id, $act)
	{
	    $sql = "UPDATE cart SET cr_active='$act' WHERE cr_id='$id'";
	    $this->ExecuteS($sql);
	}
	
	public function delCart($id_cart)
	{
	    $sql = "DELETE FROM cart WHERE cr_id='$id_cart'";
	    $this->Execute($sql);
	    
	    $sql = "DELETE FROM cart_detail WHERE cr_id='$id_cart'";
	    $this->Execute($sql);
	}
	
	public function getCartByCustomer($id_customer)
	{
	    $sql = "SELECT * FROM cart WHERE c_id='$id_customer' ORDER BY cr_date DESC";
	    return $this->ExecuteS($sql);
	}
	
	public function getCarts()
	{
	    $sql = "SELECT * FROM cart ORDER BY cr_date DESC";
	    return $this->ExecuteS($sql);
	}
	
	public function getCartsByActive($act)
	{
	    $sql = "SELECT * FROM cart WHERE cr_active='$act' ORDER BY cr_date DESC";
	    return $this->ExecuteS($sql);
	}
	
	public function getCartCustomerCart($id_customer, $act)
	{
	    $sql = "SELECT * FROM cart WHERE c_id='$id_customer' AND cr_active='$act' ORDER BY cr_date DESC";
	    return $this->ExecuteS($sql);
	}
	
	public function getCartSessionCart($session, $act)
	{
	    $sql = "SELECT * FROM cart WHERE cr_session='$session' AND cr_active='$act' ORDER BY cr_date DESC";
	    return $this->ExecuteS($sql);
	}
	
	public function getCartUserCart($id_user, $act)
	{
	    $sql = "SELECT * FROM cart WHERE u_id='$id_user' AND cr_active='$act' ORDER BY cr_date DESC";
	    return $this->ExecuteS($sql);
	}
	
	public function getCartById($id)
	{
	    $sql = "SELECT * FROM cart WHERE cr_id='$id'";
	    return $this->ExecuteS($sql);
	}
	
	public function getCustomerActiveCart($id_customer, $session)
	{
	    if($id_customer != 0)
	    {
	        $row = $this->getCartCustomerCart($id_customer, 1);
    	    if(count($row)>0)
    	    {
    	        foreach($row as $row)
    	        {
    	            return $row['cr_id'];
    	        }
    	    }
	    }
	    else
	    {
	        $row = $this->getCartSessionCart($session, 1);
	        if(count($row)>0)
	        {
	            foreach($row as $row)
    	        {
    	            return $row['cr_id'];
    	        }
	        }
	        else
	        {
	            return $this->newCartCustomer($id_customer, $id_user, $session);
	        }
	    }
	}
	
	//Cart Detail Methods
	public function newCartDetail($id_cart, $id_product, $name, $desc, $price, $qty, $trm, $id_currency, $session, $tax)
	{
	    $sql = "INSERT INTO `cart_detail`(cr_id, id_product, crd_name, crd_desc,crd_price, crd_qty, crd_trm, id_currency, crd_tax) VALUES ('$id_cart', '$id_product', '$name','$desc','$price','$qty','$trm','$id_currency','$tax')";
	    $this->Execute($sql);
	}
	
	public function editCartDetailQty($id, $qty)
	{
	    $sql = "UPDATE cart_detail SET crd_qty='$qty' WHERE crd_id='$id'";
	    $this->Execute($sql);
	}
	
	public function delCartDetail($id)
	{
	    $sql = "DELETE FROM cart_detail WHERE crd_id='$id'";
	    $this->Execute($sql);
	}
	
	public function getCartDetailByCustomer($id_customer, $session)
	{
	    if($id_customer != '')
	    {
	        $sql = "SELECT * FROM cart AS c 
    	    INNER JOIN cart_detail AS cd ON c.cr_id=cd.cr_id
    	    WHERE c.c_id='$id_customer' AND c.cr_active='1'";
    	    return $this->ExecuteS($sql);
	    }
	    else
	    {
	        $sql = "SELECT * FROM cart AS c 
    	    INNER JOIN cart_detail AS cd ON c.cr_id=cd.cr_id
    	    WHERE c.cr_session='$session' AND c.cr_active='1'";
    	    return $this->ExecuteS($sql);
	    }
	    
	}
	
	public function getCartTaxByCustomer($id_customer, $format = 0)
	{
	    $sql = "SELECT * FROM cart AS c 
	    INNER JOIN cart_detail AS cd ON c.cr_id=cd.cr_id
	    WHERE c.c_id='$id_customer' AND c.cr_active='1'";
	    $row = $this->ExecuteS($sql);
	    
	    $t = 0;
	    
	    if(count($row)>0)
	    {
	        foreach($row as $row)
	        {
	            $t += ($row['crd_tax']) * $row['crd_qty'];
	        }
	    }
	    
	    if($format == 0)
	    {
	        return $t;
	    }
	    else
	    {
	        return number_format($t);
	    }
	    
	}
	
	public function getCartSubtoByCustomer($id_customer, $format = 0)
	{
	    $sql = "SELECT * FROM cart AS c 
	    INNER JOIN cart_detail AS cd ON c.cr_id=cd.cr_id
	    WHERE c.c_id='$id_customer' AND c.cr_active='1'";
	    $row = $this->ExecuteS($sql);
	    
	    $t = 0;
	    
	    if(count($row)>0)
	    {
	        foreach($row as $row)
	        {
	            $t += ($row['crd_price']) * $row['crd_qty'];
	        }
	    }
	    
	    if($format == 0)
	    {
	        return $t;
	    }
	    else
	    {
	        return number_format($t);
	    }
	    
	}
	
	public function getCartTotalByCustomer($id_customer, $format = 0)
	{
	    $sql = "SELECT * FROM cart AS c 
	    INNER JOIN cart_detail AS cd ON c.cr_id=cd.cr_id
	    WHERE c.c_id='$id_customer' AND c.cr_active='1'";
	    $row = $this->ExecuteS($sql);
	    
	    $t = 0;
	    
	    if(count($row)>0)
	    {
	        foreach($row as $row)
	        {
	            $t += ($row['crd_price']+$row['crd_tax']) * $row['crd_qty'];
	        }
	    }
	    
	    if($format == 0)
	    {
	        return $t;
	    }
	    else
	    {
	        return number_format($t);
	    }
	    
	}
	
	public function getCartDetailByUser($id_user)
	{
	    $sql = "SELECT * FROM cart AS c 
	    INNER JOIN cart_detail AS cd ON c.cr_id=cd.cr_id
	    WHERE c.u_id='$id_user' AND c.cr_active='1'";
	    return $this->ExecuteS($sql);
	}
	
	public function getCartTotalByUser($id_user, $format = 0)
	{
	    $sql = "SELECT * FROM cart AS c 
	    INNER JOIN cart_detail AS cd ON c.cr_id=cd.cr_id
	    WHERE c.u_id='$id_user' AND c.cr_active='1'";
	    $row = $this->ExecuteS($sql);
	    
	    $t = 0;
	    
	    if(count($row)>0)
	    {
	        foreach($row as $row)
	        {
	            $t += $row['crd_price'] * $row['crd_qty'];
	        }
	    }
	    
	    if($format == 0)
	    {
	        return $t;
	    }
	    else
	    {
	        return number_format($t);
	    }
	}
	
	public function getCartNumItems($id_cart)
	{
	    $items = 0;
	    $sql = "SELECT * FROM cart_detail WHERE cr_id='$id_cart'";
	    $row = $this->ExecuteS($sql);
	    if(count($row)>0)
	    {
	        foreach($row as $row)
	        {
	            $items += $row['crd_qty'];
	        }
	    }
	    return $items;
	}
	
	public function productInCart($id_cart, $id_product)
	{
	    $sql = "SELECT * FROM cart_detail WHERE cr_id='$id_cart' AND id_product='$id_product'";
	    $row = $this->ExecuteS($sql);
	    if(count($row)>0)
	    {
	        return true;
	    }
	    else
	    {
	        return false;
	    }
	}
	
	public function getCartDetail($id_cart)
	{
	    $sql = "SELECT * FROM cart AS c INNER JOIN cart_detail AS cd ON c.cr_id=cd.cr_id INNER JOIN products AS p ON cd.id_product = p.pro_id WHERE c.cr_id='$id_cart' AND c.cr_active='1'";
	    return $this->ExecuteS($sql);
	}
	
	public function getCartTotal($id_cart, $format = 0)
	{
	    $sql = "SELECT * FROM cart AS c 
	    INNER JOIN cart_detail AS cd ON c.cr_id=cd.cr_id
	    WHERE c.cr_id='$id_cart' AND c.cr_active='1'";
	    $row = $this->ExecuteS($sql);
	    
	    $t = 0;
	    
	    if(count($row)>0)
	    {
	        foreach($row as $row)
	        {
	            $t += ($row['crd_price']+$row['crd_tax']) * $row['crd_qty'];
	        }
	    }
	    
	    if($format == 0)
	    {
	        return $t;
	    }
	    else
	    {
	        return number_format($t);
	    }
	}
	
    
	
	//Installers
   public function Checker(){
    return $this->chkTables('cart');
   }
   
   public function install(){
    $sql='CREATE TABLE `cart` (
  `cr_id` int(11) NOT NULL PRIMARY KEY AUTO_INCREMENT,
  `cr_date` datetime NOT NULL,
  `cr_total` varchar(255) NOT NULL,
  `cr_tax` varchar(255) NOT NULL,
  `cr_subto` varchar(255) NOT NULL,
  `cr_trm` varchar(255) NOT NULL,
  `c_id` int(11) NOT NULL,
  `u_id` int(11) NOT NULL,
  `cr_active` int(11) NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8;';
	
	$this->Execute($sql);
	
	$sql='CREATE TABLE `cart_detail` (
  `crd_id` int(11) NOT NULL PRIMARY KEY AUTO_INCREMENT,
  `cr_id` int(11) NOT NULL,
  `id_product` int(11) NOT NULL,
  `crd_name` varchar(255) NOT NULL,
  `crd_desc` varchar(255) NOT NULL,
  `crd_price` varchar(255) NOT NULL,
  `crd_qty` varchar(255) NOT NULL,
  `crd_trm` varchar(255) NOT NULL,
  `id_currency` varchar(255) NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8;';
	
	$this->Execute($sql);
	
	$sql='CREATE TABLE `cart_params` (
  `crp_id` int(11) NOT NULL PRIMARY KEY AUTO_INCREMENT,
  `crp_name` varchar(255) NOT NULL,
  `crp_value` varchar(255) NOT NULL,
  `crp_type` varchar(255) NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8;';
	
	$this->Execute($sql);
	
	$sql = "ALTER TABLE `cart` ADD `cr_session` VARCHAR(255) NOT NULL AFTER `cr_active`;";
	$this->Execute($sql);
	
   }
   
   public function uninstall(){
    $sql='DROP TABLE `cart`, `cart_params`, `cart_params`';
	$this->Execute($sql);
   }
	
};
?>
