function findProd(id_cart, folder)
{

	var prod_name=document.getElementById('prod_name').value;
	$.ajax({
	 type:'POST',
	 url:folder+'/modules/cart/view/f_findProduct.php',
	 async:false,
	 data: 'prod_name='+prod_name+"&id_cart="+id_cart,
	 success:function(data){
		 document.getElementById('list_prod').innerHTML=data;
	 }
	});
}

function addProd(id_product, id_cart, id_customer, session, folder)
{

	$.ajax({
	 type:'POST',
	 url:folder+'/modules/cart/view/f_addProduct.php',
	 async:false,
	 data: 'id_product='+id_product+"&id_cart="+id_cart+"&id_customer="+id_customer+"&session="+session+"&qty=1",
	 success:function(data){

		 //document.getElementById('cart_list').innerHTML=data;
		 //document.getElementById('displayAddCart').style.display='none';
		 //document.getElementById('displayNoAddCart').style.display='block';
		 //getTotal();
		 updateCartQty(id_cart, folder, session);
	 }
	});
}

function addProd2(id_product, id_cart, id_customer, session, folder)
{

    var qty = document.getElementById('prod_qty').value;
    
	$.ajax({
	 type:'POST',
	 url:folder+'/modules/cart/view/f_addProduct.php',
	 async:false,
	 data: 'id_product='+id_product+"&id_cart="+id_cart+"&id_customer="+id_customer+"&session="+session+"&qty="+qty,
	 success:function(data){

		 //document.getElementById('cart_list').innerHTML=data;
		 //document.getElementById('displayAddCart').style.display='none';
		 //document.getElementById('displayNoAddCart').style.display='block';
		 //getTotal();
		 updateCartQty(id_cart, folder, session);
	 }
	});
}

function updQty(id, qty, folder, id_customer, session)
{
    if(qty <= 0)
    {
        alert("La cantidad no puede ser 0 o negativo");
        return;
    }
    
	$.ajax({
	 type:'POST',
	 url:folder+'/modules/cart/view/f_cartQty.php',
	 async:false,
	 data: 'id='+id+"&qty="+qty+"&id_customer="+id_customer+"&session="+session+"&folder="+folder,
	 success:function(data){
		 document.getElementById('cart_list').innerHTML=data;
		 getCartSummary(folder, id_customer, session);
		 //getTotal(folder);
	 }
	});
}

function removeProd(id, id_cart, id_customer, folder, session)
{
	$.ajax({
	 type:'POST',
	 url:folder+'/modules/cart/view/f_delProduct.php',
	 async:false,
	 data: 'id='+id+"&id_cart="+id_cart+"&id_customer="+id_customer+"&session="+session+"&folder="+folder,
	 success:function(data){
		 document.getElementById('cart_list').innerHTML=data;
		 //document.getElementById('displayAddCart').style.display='block';
		 //document.getElementById('displayNoAddCart').style.display='none';
		 //getTotal();
		 updateCartQty(id_cart, folder, session);
		 getCartSummary(folder, id_customer, session);
	 }
	});
}

function getTotal(folder)
{
    $.ajax({
	 type:'POST',
	 url:folder+'/modules/cart/view/f_getCartTotal.php',
	 async:false,
	 success:function(data){
		 document.getElementById('cart_total').innerHTML=data;
	 }
	});
}

function getCartSummary(folder, id_customer, session)
{
    $.ajax({
	 type:'POST',
	 url:folder+'/modules/cart/view/f_getCartSummary.php',
	 async:false,
	 data: "id_customer="+id_customer+"&session="+session+"&folder="+folder,
	 success:function(data){
		 document.getElementById('cart_summary').innerHTML=data;
	 }
	});
}

function newOrder(id_cart, id_customer, id_contact, folder)
{
    //alert(folder+'/modules/cart/view/f_cart2order.php');
    
	$.ajax({
	 type:'POST',
	 url:folder+'/modules/cart/view/f_cart2order.php',
	 async:false,
	 data: 'id_cart='+id_cart+"&id_customer="+id_customer+"&id_contact="+id_contact,
	 success:function(data){
		 //document.getElementById('orderInfo').innerHTML=data;
		 window.location.href="payOrder.php?id_order="+data;
	 }
	});
}

function updateCartQty(id_cart, folder, session)
{
    $.ajax({
	 type:'POST',
	 url:folder+'/modules/cart/view/f_getCartQty.php',
	 async:false,
	 data: 'id_cart='+id_cart+"&session="+session,
	 success:function(data){

		 document.getElementById('cartQty').innerHTML=data;
		 
	 }
	});
}