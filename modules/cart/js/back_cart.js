function findProd(id_cart)
{

	var prod_name=document.getElementById('prod_name').value;
	$.ajax({
	 type:'POST',
	 url:'modules/cart/view/findProduct.php',
	 async:false,
	 data: 'prod_name='+prod_name+"&id_cart="+id_cart,
	 success:function(data){
		 document.getElementById('list_prod').innerHTML=data;
	 }
	});
}

function addProd(id_product, id_cart)
{
	$.ajax({
	 type:'POST',
	 url:'modules/cart/view/addProduct.php',
	 async:false,
	 data: 'id_product='+id_product+"&id_cart="+id_cart,
	 success:function(data){
		 document.getElementById('cart_list').innerHTML=data;
		 getTotal(id_Cart);
	 }
	});
}

function updQty(id, qty, id_cart)
{
    if(qty <= 0)
    {
        alert("La cantidad no puede ser 0 o negativo");
        return;
    }
    
	$.ajax({
	 type:'POST',
	 url:'modules/cart/view/cartQty.php',
	 async:false,
	 data: 'id='+id+"&qty="+qty+"&id_cart="+id_cart,
	 success:function(data){
		 document.getElementById('cart_list').innerHTML=data;
		 getTotal(id_cart);
	 }
	});
}

function removeProd(id, id_cart)
{
	$.ajax({
	 type:'POST',
	 url:'modules/cart/view/delProduct.php',
	 async:false,
	 data: 'id='+id+'&id_cart='+id_cart,
	 success:function(data){
		 document.getElementById('cart_list').innerHTML=data;
		 getTotal(id_cart);
	 }
	});
}

function getTotal(id_cart)
{
    $.ajax({
	 type:'POST',
	 url:'modules/cart/view/getCartTotal.php',
	 async:false,
	 data: 'id_cart='+id_cart,
	 success:function(data){
		 document.getElementById('cart_total').innerHTML=data;
	 }
	});
}

function newOrder(id_cart)
{
	$.ajax({
	 type:'POST',
	 url:'modules/cart/view/cart2order.php',
	 async:false,
	 data: 'id_cart='+id_cart,
	 success:function(data){
		 document.getElementById('orderInfo').innerHTML=data;
	 }
	});
}