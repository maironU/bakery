<?php
include('modules/inventory/model/inventory.php');
include('modules/crm/model/customerModel.php');
include('usr/model/User.php');
 
 $obj = new inventory();
 $obj->connect();
 
 $customer = new customerModelGe();
 $customer->connect();
 
 $user = new User();
 $user->connect();
 
 if($obj->getVars('actionDel')==true){
	 
 }
 
?>
<!-- Add mousewheel plugin (this is optional) -->
<script type="text/javascript" src="modules/inventory/js/jquery.mousewheel-3.0.6.pack.js"></script>
<!-- Add fancyBox main JS and CSS files -->
<script type="text/javascript" src="modules/inventory/js/jquery.fancybox.js?v=2.1.4"></script>
<link rel="stylesheet" type="text/css" href="modules/inventory/css/jquery.fancybox.css?v=2.1.4" media="screen" />
<link rel="stylesheet" type="text/css" href="modules/inventory/css/stylePos.css" />
<div class="widget3">
 <div class="widgetlegend">Remisiones</div>
<p style="width:100">
    <a href="<?php $_SERVER['PHP_SELF'];?>?p=modules/inventory/view/newQReferral.php" class="btn_normal" style="float:left; margin:5px;">Nuevo </a>
</p>
 
<table width="100%" height="48" border="0">
  <tr>
        <th width="30">ID</th>
	<th width="30">Fecha</th>
	<th width="30">Track</th>
        <th width="142">Descripcion</th>
	<th width="213">Usuario</th>
        <th colspan="3">Acciones</th>
  </tr>
  <?php
   $row=$obj->getQuickReferral();
   
   if(count($row)>0){
 	foreach($row as $row){
  ?>
  <tr>
        <td><?php echo $row["iqr_id"];?></td>
	<td><?php echo $row["iqr_date"];?></td>
        <td><?php echo $row["iqr_track"];?></td>
        <td><?php echo substr($row["iqr_desc"], 0, 50)."...";?></td>
	<td><?php 
		$row1 = $user->getUserById2($row["u_id"]);
		if(count($row1)>0)
		{
			foreach($row1 as $row1)
			{
				echo $row1["u_nom"];
			}
		}
	?></td>
       <td width="86"><a href="javascript:;" class="btn_1" onClick="showInfo(<?php echo $row['iqr_id']?>,'modules/inventory/view/showPrintQReferral.php')">Ver</a></td>
       <td width="86"><a class="btn_3" href="<?php $_SERVER['PHP_SELF'];?>?p=modules/inventory/view/editQReferral.php&id=<?php echo $row["iqr_id"]?>">Editar</a></td>
       <td width="86"><a href="javascript:;" class="btn_2" onClick="showInfo2(<?php echo $row['iqr_id']?>,'modules/inventory/view/delQReferral.php')">Anular</a></td>
  </tr>
  <?php
   }
  } 
  ?>
</table>

</div>
<script>
 function showInfo(id,site)
 {
	$.fancybox.open({
					href : site+'?id='+id,
					type : 'iframe',
					padding:5
				}); 
 }
 function showInfo2(id,site)
 {
	$.fancybox.open({
					href : site+'?id='+id,
					type : 'iframe',
					padding:2,
					'afterClose' : function(){
					 	parent.location.reload(true);
					}
				}); 
 }
 </script>
