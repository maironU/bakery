<?php
include('modules/inventory/model/inventory.php');
 
 $obj = new inventory();
 $obj->connect();
 
 $msg=false;
 
 if($_POST){
  	 
  $obj->newCapsInvProd();
  $msg=true;
 }

?>
<div class="widget3">
 <div class="widgetlegend">Nuevo producto de inventario </div>
 <?php
  if($msg)
  {
  ?>
   <div class="ui-widget">
	<div class="ui-state-highlight ui-corner-all" style="margin-top: 20px; padding: 0 .7em;">
		<p><span class="ui-icon ui-icon-info" style="float: left; margin-right: .3em;"></span>
		<strong>Felicitaciones!</strong> Se ha sido creado satisfactoriamente.</p>
	</div>
</div>
  <?php
  }
 ?>
<p style="width:100">
	<a href="<?php $_SERVER['PHP_SELF'];?>?p=modules/inventory/view/showSetup.php" class="btn_normal" style="float:left; margin:5px;">Volver </a>
</p>
 
<form action="#" method="post" enctype="multipart/form-data" name="form1" id="form1">
<table width="803" height="48" border="0" style="float:left">
  <tr>
    <td><label>Nombre: </label><br />
      <input name="name" type="text" id="name" value="" required />   </td>
  </tr>
  <tr>
    <td><label>Descripcion: </label><br />
      <textarea name="desc" cols="40" rows="5" id="desc"></textarea></td>
  </tr>
  <tr>
    <td><label>Precio: </label><br />
      $<input name="price" type="text" id="price" value="" />   </td>
  </tr>
  <tr>
    <td><label>Stock Min (Para alertas): </label><br />
      <input name="minstock" type="text" id="minstok" value="1" required />   </td>
  </tr>
  <tr>
    <td>
    <input type="submit" class="btn_submit" value="Guardar" /></td>
  </tr>
</table>
</form>


</div>
