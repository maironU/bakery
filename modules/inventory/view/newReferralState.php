<?php
include('modules/inventory/model/inventory.php');
 
 $obj = new inventory();
 $obj->connect();
 
 $msg=false;
 
 if($_POST){
  $name = $obj->postVars('name');
  $run = $obj->postVars('run');
  $color = $obj->postVars('color');
  $obj->newReferralState($name, $run, $color);
  $msg=true;
 }

?>
<script src="modules/inventory/js/jscolor.js" type="text/javascript"></script>
<div class="widget3">
 <div class="widgetlegend">Nuevo Estado para Remision </div>
 <?php
  if($msg)
  {
  ?>
   <div class="ui-widget">
	<div class="ui-state-highlight ui-corner-all" style="margin-top: 20px; padding: 0 .7em;">
		<p><span class="ui-icon ui-icon-info" style="float: left; margin-right: .3em;"></span>
		<strong>Felicitaciones!</strong> Se ha sido creado satisfactoriamente.</p>
	</div>
</div>
  <?php
  }
 ?>
<p style="width:100">
	<a href="<?php $_SERVER['PHP_SELF'];?>?p=modules/inventory/view/showReferralStates.php" class="btn_normal" style="float:left; margin:5px;">Volver </a>
</p>
 
<form action="#" method="post" enctype="multipart/form-data" name="form1" id="form1">
<table width="803" height="48" border="0" style="float:left">
  <tr>
    <td><label>Nombre: </label><br />
      <input name="name" type="text" id="name" required />   </td>
  </tr>
  <tr>
    <td><label>Permite hacer movimiento: </label><br />
      <input name="run" type="checkbox" id="run" value="1" />   </td>
  </tr>
  <tr>
    <td><label>Color: </label><br />
      <div id="#The_colorPicker"><input name="color" type="text" class="color" required/></div>   </td>
  </tr>
  <tr>
    <td><input type="submit" class="btn_submit" value="Guardar" /></td>
  </tr>
</table>
</form>


</div>
