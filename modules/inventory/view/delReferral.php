<?php
session_start();
  include('../../../core/config.php');
	include('../../../core/ge.php');
	
	//Models
	include('../model/inventory.php');
 	include('../../products/model/products.php');
	
	//Model Objects
	$inv=new inventory();
 	$inv->connect();
 
 	$products=new products();
 	$products->connect();
 	
 	$id=$inv->getVars('id');
 	$msg = false;
 	
 	if($_POST)
 	{
 		$row = $inv->getReferralsById($id);
		 foreach($row as $row)
		 {
		 	$state = $row['ir_state'];
		 	$track = $row['ir_track'];
		 	$who = $row['iwo_id'];
 			$whd = $row['iwd_id'];
		 }
		 $row = $inv->getReferralStateById($state);
	 	if(count($row)>0)
	 	{
	 		foreach($row as $row)
	 		{
	 			$run = $row["irs_runmov"];
	 		}
	 	}
	 	
	 	if($run == 1)
		{
			$row = $inv->getReferralDetail($id);
			if(count($row)>0)
			{
				foreach($row as $row)
				{
					$inv->registerMov($row['id_product'],$row["SUM(ird_qty)"],$_SESSION['user_id'],'',$id_move,'',"Anulacion Remision No.".$track,$whd,$who,$id,'yes');
				}
			}
		}
		$inv->delReferral($id, 0);
		$msg=true;
 	}

  
?>
<link href="../../../css/scsStyle.css" rel="stylesheet" type="text/css" />
<link href="../../../css/smoothness/jquery-ui-1.9.2.custom.css" rel="stylesheet">
<link href="../../../css/smoothness/jquery-ui-1.9.2.custom.min.css" rel="stylesheet" type="text/css" />
<script src="../../../js/jquery-1.8.3.js"></script>
<script src="../../../js/jquery-ui-1.9.2.custom.js"></script>
<script type="text/javascript">
 $(function() {
		$( ".datepicker" ).datepicker();
		$('.datepicker').datepicker('option', {dateFormat: 'yy-mm-dd'});
       
	});
</script>
<?php
 if($msg){
 ?>
  <div class="ui-widget">
	<div class="ui-state-highlight ui-corner-all" style="margin-top: 20px; padding: 0 .7em;">
		<p><span class="ui-icon ui-icon-info" style="float: left; margin-right: .3em;"></span>
		<strong>Felicitaciones!</strong> La remisi&oacute;n ha sido anulada correctamente y los movimientos han sido reversados.</p>
	</div>
</div>
 <?php
 }
?>

<div class="widget3">
 <div class="widgetlegend">Confirmacion de Anulaci&oacute;n </div>
  <form action="" method="post" name="form1">
    <table width="692" height="70" border="0" align="center">
      <tr>
        <td colspan="2">Esta apunto de anular una remisi&oacute;n. Si realiza este proceso, usted deber&aacute; generar una completamente nueva, y dependiendo del estado de la remisi&oacute;n, los movimientos de los productos ser&aacute;n reversados . <h3>Est&aacute; seguro de anular la remisi&oacute;n?</h3> </td>
      </tr>
      <tr>
        <td width="136"><input name="id" type="hidden" id="id" value="<?php echo $id; ?>" /></td>
        <td width="238"><a href="#" class="btn_borrar" onclick="document.form1.submit();">Confirmar</a></td>
      </tr>
    </table>
  </form>

</div>