<?php
    session_start();
    include('../../../core/config.php');
	include('../../../core/ge.php');
	
	//Models
	include('../model/inventory.php');
 	include('../../products/model/products.php');
	
	//Model Objects
	$inv=new inventory();
 	$inv->connect();
 
 	$products=new products();
 	$products->connect();
	
		
 	$ref=$inv->postVars('ref');
 	$name=$inv->postVars('name');
 	
 	if($ref != '')
 	{
 	    $row=$products->findProduct2($ref);
 	} elseif ($name != '') {
 	    $row=$products->findProductByName($name);
 	}
	
	$html.='
	 <table border="0" width="100%">
	  <tr>
	  	<th>Producto</th>
		<th>Referencia</th>
		<th>Seleccionar</th>
	  </tr>
	';
	
	if(count($row)>0)
	{
	 foreach($row as $row)
	 {
	  $html.="
	   <tr>
			<td>".$row["pro_name"]."</td>
			<td>".$row["pro_reference"]."</td>
			<td><input type='radio' name='id_product' id='id_product' value='".$row['pro_id']."' required></td>";
			
	 }
	}
	
	$html.="
	</table>";
	echo $html;
?>
