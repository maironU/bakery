<?php
	include('modules/inventory/model/inventory.php');
	include('modules/products/model/products.php');
	include('modules/crm/model/customerModel.php');


	$obj=new inventory();
	$obj->connect();

	$products=new products();
	$products->connect();

	$customer=new customerModelGe();
	$customer->connect();

	$profiles = $obj->getConfigValue(3);
	$profiles = explode('-',$profiles);

	$up = $obj->getUserProfile($_SESSION['user_id']);

?>
<!-- Add mousewheel plugin (this is optional) -->
<script type="text/javascript" src="modules/inventory/js/jquery.mousewheel-3.0.6.pack.js"></script>
<!-- Add fancyBox main JS and CSS files -->
<script type="text/javascript" src="modules/inventory/js/jquery.fancybox.js?v=2.1.4"></script>
<link rel="stylesheet" type="text/css" href="modules/inventory/css/jquery.fancybox.css?v=2.1.4" media="screen" />
<link rel="stylesheet" type="text/css" href="modules/inventory/css/stylePos.css" />
<style>
	.swal2-popup {
		background: #fff !important;
	}

	table td, th {
		font-size: 11px;
	}
</style>
<link href="//cdn.jsdelivr.net/npm/@sweetalert2/theme-dark@4/dark.css" rel="stylesheet">
<script src="//cdn.jsdelivr.net/npm/sweetalert2@11/dist/sweetalert2.min.js"></script>
 
<div class="card shadow p-3 mb-4">
	<div class="widgetlegend">Nuevo Pedido </div>

	<div class="wizard mx-auto" style="width:60%">
		<div class="wizard-navigation">
			<ul class="pl-0">
				<li class="tab-active" data-wizard="bodega" id="tab-bodega"><a href="#">Bodega</a></li>
				<li data-wizard="producto" id="tab-producto"><a href="#">Producto</a></li>
			</ul>
			<div class="moving-tab" style="transform: translate3d(0px, 0px, 0px); transition: all 0.3s ease-out 0s;">Bodega</div>
		</div>
	</div>

	<div class="tab-content-wizard mx-auto" style="width: 65%">
		<div class="tab-pane-wizard" id="producto">
			<div class="row">
				<div class="form-group mt-3">
					<label for="">Cantidad</label>
					<input name="qty" type="number" id="qty" value="" required class="form-control">
				</div>
				<div class="accordion" id="accordionExample">
					<div class="accordion-item">
						<h2 class="accordion-header" id="headingOne">
						<button class="accordion-button" type="button" data-bs-toggle="collapse" data-bs-target="#collapseOne" aria-expanded="true" aria-controls="collapseOne">
							Buscar producto por referencia
						</button>
						</h2>
						<div id="collapseOne" class="accordion-collapse collapse show" aria-labelledby="headingOne" data-bs-parent="#accordionExample">
							<div class="accordion-body d-flex">
								<input style="width: 40%"  type="text" name="ref" id="ref" placeholder="Referencia" class="form-control mr-4">
								<button style="width: 40%" class="btn btn-success btn-block" onClick="findProduct()">Buscar</button>
							</div>
						</div>
					</div>
					<div class="accordion-item">
						<h2 class="accordion-header" id="headingTwo">
						<button class="accordion-button collapsed" type="button" data-bs-toggle="collapse" data-bs-target="#collapseTwo" aria-expanded="false" aria-controls="collapseTwo">
							Buscar producto por nombre
						</button>
						</h2>
						<div id="collapseTwo" class="accordion-collapse collapse" aria-labelledby="headingTwo" data-bs-parent="#accordionExample">
							<div class="accordion-body d-flex">
								<input style="width: 40%" type="text" name="name" id="name" placeholder="Nombre de producto" class="form-control mr-4">
								<button style="width: 40%" class="btn btn-success btn-block" onClick="findProduct()">Buscar</button>
							</div>
						</div>
					</div>
				</div>
				<div id="findProd" style="max-height: 400px; overflow: scroll"></div>
				<div id="showL" class="mt-4" style="max-height: 400px; overflow: scroll">
				<?php
	            if(in_array($up, $profiles))
	            {
					?>
					<h3 class="text-center">Productos Agregados</h3>
					<table border="0" width="100%">
						<tr>
							<th>&nbsp;</th>
							<th>Producto</th>
							<th style="min-width: 70px">Cantidad</th>
							<th style="min-width: 80px">Contenedor</th>
							<th style="min-width: 60px">Total</th>
							<th>Precio</th>
							<th>Total</th>
							<th>&nbsp;</th>
						</tr>
						<?php
							$row = $obj->getReferralDetailTemp($_SESSION['user_id']);
							if(count($row)>0)
							{
								foreach($row as $row)
								{
									?>
									<tr>
										<td><img src="modules/products/imagesProd/<?php echo $obj->getProductImage($row["id_product"]);?>" width="50" height="50" /></td>
										<td><?php echo $row["irdt_productname"];?></td>
										<td>
											<input type='text' name='qty' id="qty_prod_<?php echo $row["irdt_id"] ?>" value='<?php echo $row["qty3"] ?>' onChange='editQtyItem(<?php echo $row["irdt_id"] ?>, 2)' class='border-0 w-25' style='font-size: 15px; height: 20px'>
											<img src='images/icons/less.png' alt='' width='22px' class='m-0' onClick='editQtyItem(<?php echo $row["irdt_id"] ?>, 0)'> 
											<img src='images/icons/more.png' alt='' width='22px' class='m-0' onClick='editQtyItem(<?php echo $row["irdt_id"] ?>, 1)'>
										</td>
										<td><?php echo $row["qty2"];?></td>
										<td><?php echo $row["qty"];?></td>
										<td>$<?php echo number_format($row["irdt_price"],2);?></td>
										<td>$<?php echo number_format($row["irdt_price"]*$row["qty"],2);?></td>
										<td><a href="javascript:;" class="btn btn-danger" onClick="delProduct(<?php echo $row['irdt_id']?>)">Borrar</a></td>
									</tr>
									<?php
								}
							}
						?>
					</table>
					<?php
						}
						else
						{
							?>
							<h3 class="text-center">Productos Agregados</h3>
							<table border="0" width="100%">
						<tr>
							<th>&nbsp;</th>
							<th>Producto</th>
							<th style="min-width: 70px">Cantidad</th>
							<th style="min-width: 80px">Contenedor</th>
							<th style="min-width: 60px">Total</th>
							<th>&nbsp;</th>
						</tr>
						<?php
							$row = $obj->getReferralDetailTemp($_SESSION['user_id']);
							if(count($row)>0)
							{
								foreach($row as $row)
								{
									?>
									<tr>
										<td><img src="modules/products/imagesProd/<?php echo $obj->getProductImage($row["id_product"]);?>" width="50" height="50" /></td>
										<td><?php echo $row["irdt_productname"];?></td>
										<td>
											<input type='text' name='qty' id="qty_prod_<?php echo $row["irdt_id"] ?>" value='<?php echo $row["qty3"] ?>' onChange='editQtyItem(<?php echo $row["irdt_id"] ?>, 2)' class='border-0 w-25' style='font-size: 15px; height: 20px'>
											<img src='images/icons/less.png' alt='' width='22px' class='m-0' onClick='editQtyItem(<?php echo $row["irdt_id"] ?>, 0)'> 
											<img src='images/icons/more.png' alt='' width='22px' class='m-0' onClick='editQtyItem(<?php echo $row["irdt_id"] ?>, 1)'>
										</td>
										<td><?php echo $row["qty2"];?></td>
										<td><?php echo $row["qty"];?></td>
										<td><a href="javascript:;" style="font-size: 11px" class="btn btn-danger" onClick="delProduct(<?php echo $row['irdt_id']?>)">Borrar</a></td>
									</tr>
									<?php
								}
							}
						?>
					</table>
					<?php
						}
					?>
				</div>
				<div id="gauch2" style="display:none"><img src="modules/inventory/image/loading.gif" /></div>

				<div class="form-group d-flex flex-column mt-3">
					<label for="">Motivo de Remision</label>
					<textarea name="desc" cols="40" rows="5" id="desc" required class="form-control"></textarea>
				</div>
			</div>
		</div>

		
		<div class="tab-pane-wizard tab-pane-active" id="bodega">
			<div class="row">
				<div class="form-group">
					<label for="">Tipo de envio</label>
					<select name="id_type" id="id_type" required class="form-control">
						<option value="">-- Tipo de envio--</option>
						<?php
							$row = $obj->getReferralsType();
							if(count($row)>0)
							{
								foreach($row as $row)
								{
									?>
									<option value="<?php echo $row["irt_id"]?>"><?php echo utf8_encode($row["irt_name"])?></option>
									<?php
								}
							}
						?>
					</select>
				</div>
			</div>

			<div class="form-group">
				<label for="type">Tipo de movimiento</label>
				<select name="type" id="type" class="form-control" required>
					<option value="">-- Tipo de Movimiento--</option>
					<?php
						$row = $obj->getMovTypebyAction('in');
						
						if(count($row)>0)
						{
							foreach($row as $row)
							{
								?>
									<option value="<?php echo $row["imt_id"]?>"><?php echo utf8_encode($row["imt_name"])?></option>
								<?php
							}
						}
					?>
				</select>
			</div>

			<div class="form-group">
				<label for="who">Bodega de Origen:</label>
				<select name="who" id="who" class="form-control" required>
					<option value="">--Bodega--</option>
					<?php
						$row = $obj->getWarehouse();
						if(count($row)>0)
						{
							foreach($row as $row)
							{
								?>
								<option value="<?php echo $row['iw_id']?>"><?php echo utf8_encode($row['iw_name'])?></option>
								<?php
							}
						}
					?>
				</select>
			</div>

			<div class="form-group">
				<label for="whd">Bodega de Destino:</label>
				<select name="whd" id="whd" class="form-control" required>
					<option value="">--Bodega--</option>
					<?php
						$row = $obj->getWarehouse();
						if(count($row)>0)
						{
							foreach($row as $row)
							{
								?>
								<option value="<?php echo $row['iw_id']?>"><?php echo utf8_encode($row['iw_name'])?></option>
								<?php
							}
						}
					?>
				</select>
			</div>

			<div class="form-group">
				<label for="whd">Estado de remision:</label>
				<select name="state" id="state" class="form-control" required>
					<option value="">--Estado--</option>
					<?php
						$row = $obj->getReferralState();
						if(count($row)>0)
						{
							foreach($row as $row)
							{
								?>
								<option value="<?php echo $row['irs_id']?>"><?php echo utf8_encode($row['irs_name'])?></option>
								<?php
							}
						}
					?>
				</select>
			</div>
		</div>
	</div>
	<div class="wizard-footer d-flex justify-content-between" style="width: 90%; margin: 0 auto">
		<div class="pull-left">
			<input type='button' class='btn btn-previous btn-fill btn-wd btn-sm pull' name='next' value='Anterior' style="background: #538a9a; color:white"/>
		</div>

		<div class="pull-right">
			<input type='button' class='btn btn-next btn-fill btn-wd btn-sm pull-btn-active pull' name='next' value='Siguiente' style="background: #538a9a; color:white"/>
			<input type='button' class='btn btn-finish btn-fill btn-success btn-wd btn-sm pull' name='finish' value='Guardar' onclick="savePedido()"/>
		</div>
	</div>
</div>

<script type="text/javascript" src="js/wizardpedidos.js"></script>

<script>
 function showInfo(id,site)
 {
	$.fancybox.open({
					href : site+'?id='+id,
					type : 'iframe',
					padding:2,
					'afterShow': function() {
						
        				//$('#fancybox-frame').contents().find("#barcode").focus();
						//document.frm1.barcode.focus();
					},
					'afterClose' : function(){
					 	//parent.location.reload(true);
					}
					
	}); 
 }
 
 function savePedido(){
	Swal.fire({
		title: 'Quiere realizar otro pedido o perdida con la misma configuracion?',
		showDenyButton: true,
		//showCancelButton: true,
		confirmButtonText: 'Si',
		denyButtonText: `No`,
		confirmButtonColor: '#49bf9d',
		})
		.then((result) => {
			if (result.isConfirmed) {
				fetchPedido(true)
			}else if (result.isDenied) {
				fetchPedido(false)
			}
		})
		$("#swal2-title").css("color", "#a2a2a2")
}

function fetchPedido(resend){
	let desc = $('#desc').val();
	let type = $('#type').val();
	let who = $('#who').val();
	let whd = $('#whd').val();
	let state = $("#state").val()
	let id_type = $('#id_type').val();
	let user_id = '<?php echo $_SESSION['user_id'] ?>'

	let data = {
		desc,
		type,
		who,
		whd,
		state,
		id_type,
		user_id,
		resend
	}

	$.post('modules/inventory/model/api.php?method=savePedido', data).then(res => {
		let response = JSON.parse(res)
		if(response.success){
			messageSuccess(response.message)
			$('#showL').html("");
			if(!resend) emptyAll()
		}else{
			messageError(response.message)
		}
	})
}

function emptyAll(){
	$('#qty').val("");
	$('#type').val("");
	$('#id_type').val("");
	$('#desc').val("");
	$('#who').val("");
	$('#whd').val("");
	$("#state").val("");
	$('#findProd').html("");
	$('#ref').val("");
	$('#name').val("");

	active = "bodega"
	activeTab(active)
	showButtonsFooter()
    movingTab("Bodega", 0)
}

 function findProduct()
 {
	var ref = document.getElementById("ref").value;
	var name = document.getElementById("name").value;
	
	//document.getElementById("gauch1").style.display="block";
	$.ajax({
		type:'POST',
		url:'modules/inventory/view/findProduct.php',
		async:false,
		data:"ref="+ref+"&name="+name,
		success:function(data){
		    //alert(data);
			document.getElementById("findProd").innerHTML=data;
			//document.getElementById("gauch1").style.display="none";
			//document.getElementById("token_ws").value=response.token;
		},

	});

	}

function addProduct(ref, stock)
{
	var qty = document.getElementById("qty").value;

	if(!isQty(qty)) return

	var id_type = document.getElementById("id_type").value;

	document.getElementById("gauch2").style.display="block";
	$.ajax({
		type:'POST',
		url:'modules/inventory/view/addProduct.php',
		async:false,
		data:"ref="+ref+"&qty="+qty+"&id_type="+id_type,
		success:function(data){				
			$("#showL").html(`
				<h3 class="text-center">Productos Agregados</h3>
				${data}
			`)
			document.getElementById("gauch2").style.display="none";
			messageSuccess('Producto agregado correctamente')
		},
					
	});
 }

 function isQty(qty){
	if(qty == ""){
		messageError(`La cantidad no puede estar vacia`)
		return false
	}else if(qty == 0){
		messageError(`La cantidad no puede ser cero`);
		return false
	}

	return true
 }

function messageError(message){
	Toastify({
		text: message,
		duration: 2000,
		newWindow: true,
		close: true,
		position: "center", // `left`, `center` or `right`
		stopOnFocus: true, // Prevents dismissing of toast on hover
		style: {
			background: "#dc3545",
		},
		onClick: function() {} // Callback after click
	}).showToast();
}

function messageSuccess(message){
	Toastify({
		text: `Felicitaciones ${message}`,
		duration: 2000,
		newWindow: true,
		close: true,
		position: "center", // `left`, `center` or `right`
		stopOnFocus: true, // Prevents dismissing of toast on hover
		style: {
			background: "#1cc88a",
		},
		onClick: function() {} // Callback after click
	}).showToast();
}

 function delProduct(id)
 {
 	document.getElementById("gauch2").style.display="block";
	$.ajax({
		type:'POST',
		url:'modules/inventory/view/delProduct.php',
		async:false,
		data:"id="+id,
		success:function(data){	
			$("#showL").html(`
				<h3 class="text-center">Productos Agregados</h3>
				${data}
			`)
			document.getElementById("gauch2").style.display="none";
			messageSuccess('Producto eliminado correctamente')
		},
					
	});
	 
 }

 function editQtyItem(id, action) {
	let qty = $("#qty_prod_" + id).val()

	if((action == 0 && qty > 1) || (action == 1) || (action == 2 && qty > 0)){
		$.ajax({
			type: 'POST',
			url: 'modules/inventory/view/updateProduct.php',
			async: false,
			data: 'id=' + id + '&qty=' + qty + '&action=' + action,
			success: function(data) {
				$("#showL").html(`
					<h3 class="text-center">Productos Agregados</h3>
					${data}
				`)
				document.getElementById("gauch2").style.display="none";
				messageSuccess('Producto actualizado correctamente')
			}
		});
	}else{
		messageError("La cantidad no puede ser cero o vacia")
	}
}
</script>
