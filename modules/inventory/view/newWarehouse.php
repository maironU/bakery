<?php
include('modules/inventory/model/inventory.php');
include('modules/products/model/products.php');
 
 $obj = new inventory();
 $obj->connect();
 
 $products = new products();
 $products->connect();
 
 $msg=false;
 
 if($_POST){
	$id_wh = $obj->newWarehouse();
	$msg=true;
	
	$row = $products->getProducts();
	if(count($row)>0)
	{
	    foreach($row as $row)
	    {
	        $obj->newCapsInvProd3($row['pro_name'], $row['pro_sdesc'], $row['pro_price'], $row['capsinv_qty'], $row['pro_id'], $id_wh);
	    }
	}
 }

?>
<div class="widget3">
 <div class="widgetlegend">Nueva Bodega </div>
 <?php
  if($msg)
  {
  ?>
   <div class="ui-widget">
	<div class="ui-state-highlight ui-corner-all" style="margin-top: 20px; padding: 0 .7em;">
		<p><span class="ui-icon ui-icon-info" style="float: left; margin-right: .3em;"></span>
		<strong>Felicitaciones!</strong> Se ha sido creado satisfactoriamente.</p>
	</div>
</div>
  <?php
  }
 ?>
<p style="width:100">
	<a href="<?php $_SERVER['PHP_SELF'];?>?p=modules/inventory/view/showWarehouse.php" class="btn_normal" style="float:left; margin:5px;">Volver </a>
</p>
 
<form action="#" method="post" enctype="multipart/form-data" name="form1" id="form1">
<table width="803" height="48" border="0" style="float:left">
  <tr>
    <td><label>Nombre: </label><br />
      <input name="name" type="text" id="name" required />   </td>
  </tr>
  <tr>
    <td><label>Descripcion: </label><br />
      <textarea name="desc" cols="40" rows="6"></textarea>  </td>
  </tr>
  <tr>
    <td><label>Responsable: </label><br />
      <input name="resp" type="text" id="resp" required />   </td>
  </tr>
  <tr>
    <td><label>Imagen: </label><br />
      <input name="arch" type="file" id="arch" />   </td>
  </tr>
  <tr>
    <td><label>Direccion: </label><br />
      <input name="addr" type="text" id="addr" required />   </td>
  </tr>
  <tr>
    <td><label>Telefono: </label><br />
      <input name="phone" type="text" id="phone" required />   </td>
  </tr>
  <tr>
    <td><label>Bodega por defecto: </label><br />
    <input type="checkbox" name="def" value="1" />
	
	</td>
  </tr>
  <tr>
    <td><input type="submit" class="btn_submit" value="Guardar" /></td>
  </tr>
</table>
</form>


</div>
