<?php
  session_start();
  include('../../../core/config.php');
  include('../../../core/ge.php');
  include('../../products/model/products.php');
  include('../model/inventory.php');
  
  $obj=new inventory();
  $obj->connect();
 
  $product = new products();
  $product->connect();
  
  $id = $obj->getVars('id');
  
  $action = $obj->postVars('action');
  $reference = $obj->postVars('reference');
  
  $msg = false;
  
  if($action == 'newitem')
  {
  	$id_product = $obj->postVars('id_product');
  	$qty = $obj->postVars('qty');
  	$id_move = $obj->postVars('id_move');
  	$who = $obj->postVars('who');
  	$whd = $obj->postVars('whd');
  	$id = $obj->postVars('id');
  	
  	//Info Product
  	$row = $obj->getCapsInvProdById($id_product);
  	foreach($row as $row)
  	{
  		$name_product = $row['ip_name'];
  	}
  	
  	//Info Move
   	$row = $obj->getMovTypeById($id_move);
  	foreach($row as $row)
  	{
  		$typemov = $row['imt_action'];
  	}
  	
  	//Info Warehouse Origin
   	$row = $obj->getWarehouseById($who);
  	foreach($row as $row)
  	{
  		$whoname = $row['iw_name'];
  	} 
  	
  	//Info Warehouse Destination
   	$row = $obj->getWarehouseById($whd);
  	foreach($row as $row)
  	{
  		$whdname = $row['iw_name'];
  	}
  	
  	$obj->newQuickReferralDetail($id, $qty, $id_move, $typemov, $id_product, $who, $whd, $whoname, $whdname, $name_product);
  	$id_item = $obj->getQuickReferralItemLastID();
  	//Se registran los movimientos
 	$row = $obj->getQuickReferralDetailById($id_item);
 	if(count($row)>0)
 	{
 		foreach($row as $row)
 		{
 			$obj->registerMov($row['id_product'],$row['iqrd_qty'],$_SESSION['user_id'],'',$row['id_move'],$row['id_product'],"Movimiento entre bodegas Remision Rapida No.".$row['iqr_id'],$row['iqrd_who'],$row['iqrd_whd'],$row['iqr_id'], 'no');
 		}
 	}
  	$msg = true;	
  	
  }
  
?>
<link href="../../../css/scsStyle.css" rel="stylesheet" type="text/css" />
<div class="widget3">
 <div class="widgetlegend">Nuevo Item</div>
 <?php
  if($msg)
  {
  ?>
   <div class="ui-widget">
	<div class="ui-state-highlight ui-corner-all" style="margin-top: 20px; padding: 0 .7em;">
		<p><span class="ui-icon ui-icon-info" style="float: left; margin-right: .3em;"></span>
		<strong>Felicitaciones!</strong> ha agregado un item satisfactoriamente.</p>
	</div>
</div>
  <?php
  }
 ?>
 <form action="" method="post" enctype="multipart/form-data" name="formadd"><table width="200" border="0">
  <tr>
    <td>Buscar referencia: <input name="reference" type="text" required /> </td>
    <td>
    <input value="search" type="hidden" name="action" />
    <input value="Buscar" type="submit" class="btn_submit" /></td>
  </tr>
</table>
 </form> 
<form action="" method="post">
<table width="100%">
  <tr>
  	<td>
  		<table>
  		 	<?php
  		 		if($action == 'search')
  		 		{
  		 			$row = $product->findProduct2($reference);
  		 			if(count($row)>0)
  		 			{
  		 				?>
  		 				<tr>
  		 					<th>Nombre </th>
  		 					<th>Referencia </th>
  		 					<th>Stock </th>
  		 					<th>&nbsp;</th>	
  		 				</tr>
  		 				<?php
  		 				foreach($row as $row)
  		 				{
  		 					$row1 = $obj->getCapsInvProdById($row['inv_id']);
  		 					if(count($row1)>0)
  		 					{
  		 						foreach($row1 as $row1)
  		 						{
  		 							$stock = $row1['ip_stock'];
  		 						}
  		 					}
  		 					?>
  		 					<tr>
  		 						<td><?php echo $row['pro_name']?></td>
  		 						<td><?php echo $row['pro_reference']?></td>
  		 						<td><?php echo $stock?></td>
  		 						<td>
  		 						<?php
  		 						if($stock == 0)
  		 						{
  		 						?>
  		 						No hay stock
  		 						<?php
  		 						}
  		 						else
  		 						{
  		 						?>
  		 						<input type="radio" name="id_product" value="<?php echo $row['inv_id']?>" onClick="displayForm()" required/>
  		 						<?php
  		 						}
  		 						?>
  		 						</td>
  		 					</tr>
  		 					<?php
  		 				}
  		 			}
  		 			else
  		 			{
  		 			?>
  		 			<tr>
  		 				<td>No se encontraron productos</td>
  		 			</tr>
  		 			<?php
  		 			}
  		 			
  		 		}
  		 	?>
  		</table>
  	</td>
  </tr>
  <tr>
  	<td>
  		<div id="frmItem" style="display:none">
  		<table>
  			<tr>
  				<td>Cantidad: <input type="text" name="qty" value="1" required /></td>
  			</tr>
  			<tr>
  				<td>Movimiento: <select name="id_move" required>
  				<option value="">--Seleccionar</option>
  				<?php
  					$row = $obj->getMovType();
  					if(count($row)>0)
  					{
  						foreach($row as $row)
  						{
  							?>
  							<option value="<?php echo $row['imt_id']?>"><?php echo $row['imt_name']?></option>
  							<?php
  						}
  					}
  				?>
  				</select>
  				</td>
  			</tr>
  			<tr>
  				<td>Bodega Origen: <select name="who" required>
  				<option value="">--Seleccionar</option>
  				<?php
  					$row = $obj->getWarehouse();
  					if(count($row)>0)
  					{
  						foreach($row as $row)
  						{
  							?>
  							<option value="<?php echo $row['iw_id']?>"><?php echo $row['iw_name']?></option>
  							<?php
  						}
  					}
  				?>
  				</select>
  				</td>
  			</tr>
  			<tr>
  				<td>Bodega Destino: <select name="whd">
  				<option value="">--Seleccionar</option>
  				<?php
  					$row = $obj->getWarehouse();
  					if(count($row)>0)
  					{
  						foreach($row as $row)
  						{
  							?>
  							<option value="<?php echo $row['iw_id']?>"><?php echo $row['iw_name']?></option>
  							<?php
  						}
  					}
  				?>
  				</select>
  				</td>
  			</tr>
  			<tr>
  				<td>
  				<input value="<?php echo $id?>" type="hidden" name="id" />
  				<input value="newitem" type="hidden" name="action" />
    				<input value="Guardar" type="submit" class="btn_submit" /></td>
  			</tr>
  		</table>
  		</div>
  	</td>
  <tr>
</table>
</form>
</div>
<script>
function displayForm()
{
	document.getElementById('frmItem').style.display = 'block';
}
</script>