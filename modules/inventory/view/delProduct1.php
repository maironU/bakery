<?php
    session_start();
    include('../../../core/config.php');
	include('../../../core/ge.php');
	
	//Models
	include('../model/inventory.php');
 	include('../../products/model/products.php');
	
	//Model Objects
	$inv=new inventory();
 	$inv->connect();
 
 	$products=new products();
 	$products->connect();
 	
 	$profiles = $inv->getConfigValue(3);
    $profiles = explode('-',$profiles);
 
    $up = $inv->getUserProfile($_SESSION['user_id']);
	
		
 	$id=$inv->postVars('id');
 	$id_referral=$inv->postVars('id_referral');
	$inv->delReferralDetail($id);
	
	
	if(in_array($up, $profiles))
	{
	   $html.='
	 <table border="0" width="100%">
	  <tr>
	    <th>&nbsp;</th>
	  	<th>Producto</th>
	  	<th>Cantidad</th>
		<th>Contenedor</th>
		<th>Cantidad Total</th>
		<th>Precio</th>
		<th>Total</th>
		<th>&nbsp;</th>
	  </tr>
	';
	$row = $inv->getReferralDetail($id_referral);
	if(count($row)>0){
	 foreach($row as $row){
	  $html.="
	   <tr>
	        <td><img src='modules/products/imagesProd/".$inv->getProductImage($row["id_product"])."' width='50' height='50' /></td>
			<td>".$row["ird_productname"]."</td>
			<td>".$row["qty3"]."</td>
			<td>".$row["qty2"]."</td>
			<td>".$row["qty"]."</td>
			<td>$".number_format($row["ird_price"],2)."</td>
			<td>$".number_format($row["ird_price"]*$row["qty"],2)."</td>
			<td><a href='javascript:;' class='btn_2' onClick='delProduct(".$row['ird_id'].", ".$id_referral.")'>Borrar</a></td>
		</tr>
	  ";
	 }
	}
	
	$html.="</table>"; 
	}
	else
	{
	    $html.='
	 <table border="0" width="100%">
	  <tr>
	  	<th>&nbsp;</th>
	  	<th>Producto</th>
	  	<th>Cantidad</th>
		<th>Contenedor</th>
		<th>Cantidad Total</th>
		<th>&nbsp;</th>
	  </tr>
	';
	$row = $inv->getReferralDetail($id_referral);
	if(count($row)>0){
	 foreach($row as $row){
	  $html.="
	   <tr>
	        <td><img src='modules/products/imagesProd/".$inv->getProductImage($row["id_product"])."' width='50' height='50' /></td>
			<td>".$row["ird_productname"]."</td>
			<td>".$row["qty3"]."</td>
			<td>".$row["qty2"]."</td>
			<td>".$row["qty"]."</td>
			<td><a href='javascript:;' class='btn_2' onClick='delProduct(".$row['ird_id'].", ".$id_referral.")'>Borrar</a></td>
		</tr>
	  ";
	 }
	}
	
	$html.="</table>";
	}
	
	
	echo $html;
?>
