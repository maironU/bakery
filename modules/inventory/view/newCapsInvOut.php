<?php
include('modules/inventory/model/inventory.php');
include('modules/products/model/products.php');
 
 $obj = new inventory();
 $obj->connect();
 
 $products = new products();
 $products->connect();
 
 $msg=false;
 
 if($_POST){
  $id_product=$obj->postVars('id_product');
  $qty=$obj->postVars('qty');
  $type=$obj->postVars('type');
  $desc=$obj->postVars('desc');
  $who=$obj->postVars('who');
  $whd=$obj->postVars('whd');
  
  $id_ipo = $obj->getCapsInvProdByProductWh($id_product, $who);
  $id_ipd = $obj->getCapsInvProdByProductWh($id_product, $whd);
  
  $row=$products->getProductsById($id_product);
	 foreach($row as $row){
	  $product_qtycontainer=$row["pro_qtycontainer"];
	}
	
	$qty = $product_qtycontainer * $qty;
  
  if($who == $whd)
  {
      $obj->registerMov($id_ipo,$qty,$_SESSION['user_id'],$id_account,$type,$id_product,$desc,$who,$whd,$referral);    
  }
  else
  {
      $obj->registerMov($id_ipo,$qty,$_SESSION['user_id'],$id_account,$type,$id_product,$desc,$who,$whd,$referral);
      $obj->registerMov($id_ipd,$qty,$_SESSION['user_id'],$id_account,1,$id_product,$desc,$who,$whd,$referral);
  }
  $msg=true;
 }

?>
<div class="widget3">
 <div class="widgetlegend">Nuevo Egreso </div>
 <?php
  if($msg)
  {
  ?>
   <div class="ui-widget">
	<div class="ui-state-highlight ui-corner-all" style="margin-top: 20px; padding: 0 .7em;">
		<p><span class="ui-icon ui-icon-info" style="float: left; margin-right: .3em;"></span>
		<strong>Felicitaciones!</strong> Se ha sido creado satisfactoriamente.</p>
	</div>
</div>
  <?php
  }
 ?>
<p style="width:100">
	<!--<a href="<?php $_SERVER['PHP_SELF'];?>?p=modules/inventory/view/index.php" class="btn_normal" style="float:left; margin:5px;">Volver </a>-->
</p>
 
<form action="#" method="post" enctype="multipart/form-data" name="form1" id="form1">
<table width="803" height="48" border="0" style="float:left">
  <tr>
    <td><label>Producto: </label><br />
        <div id="accordion">
  	       <h3>Buscar producto por referencia</h3>
  	        <div>
  	            <p>
  	                <table>
  	                    <tr>
  	                        <td><input type="text" name="ref" id="ref" placeholder="Referencia" /></td>
  	                        <td><input type="button" value="Buscar" class="btn_submit" onClick="findProduct()" /></td>
  	                    </tr>
  	                </table>
  	            </p>
  	        </div>
  	        <h3>Buscar producto por nombre</h3>
  	        <div>
  	            <p>
  	                <table>
  	                    <tr>
  	                        <td><input type="text" name="name" id="name" placeholder="Nombre de producto" /></td>
  	                        <td><input type="button" value="Buscar" class="btn_submit"  onClick="findProduct2()"/></td>
  	                    </tr>
  	                </table>
  	            </p>
  	        </div>
  	    </div>
        <div id="showPl"></div></td>
  </tr>
  <tr>
    <td><label>Cantidad a ingresar: </label><br />
      <input name="qty" type="number" id="qty" value="0" required/>   </td>
  </tr>
  <tr>
    <td><label>Descripcion: </label><br />
      <textarea name="desc" cols="40" rows="5" id="desc" required></textarea></td>
  </tr>
  <tr>
    <td><label>Tipo de movimiento: </label><br />
      <select name="type" required>
		<option value="">-- Tipo de Movimiento--</option>
		<?php
			$row = $obj->getMovTypebyAction('out');
			if(count($row)>0)
			{
				foreach($row as $row)
				{
					?>
					<option value="<?php echo $row["imt_id"]?>"><?php echo utf8_encode($row["imt_name"])?></option>
					<?php
				}
			}
		?>
	  </select>
	 </td>
  </tr>
  <tr>
    <td><label>Bodega de Origen: </label><br />
      <select name="who" required>
		<option value="">--Bodega--</option>
		<?php
			$row = $obj->getWarehouse();
			if(count($row)>0)
			{
				foreach($row as $row)
				{
					?>
					<option value="<?php echo $row['iw_id']?>"><?php echo utf8_encode($row['iw_name'])?></option>
					<?php
				}
			}
		?>
	</select>
	 </td>
  </tr>
  <tr>
    <td><label>Bodega de Destino: </label><br />
      <select name="whd">
		<option value="">--Bodega--</option>
		<?php
			$row = $obj->getWarehouse();
			if(count($row)>0)
			{
				foreach($row as $row)
				{
					?>
					<option value="<?php echo $row['iw_id']?>"><?php echo utf8_encode($row['iw_name'])?></option>
					<?php
				}
			}
		?>
	</select>
	 </td>
  </tr>
  <tr>
    <td>
		<input type="submit" class="btn_submit" value="Guardar" />
	</td>
  </tr>
</table>
</form>
<script>
 function findProduct()
 {
	var ref = document.getElementById("ref").value;
	$.ajax({
		type:'POST',
		url:'modules/inventory/view/sProduct.php',
		async:false,
		data:"ref="+ref,
		success:function(data){				
			document.getElementById("showPl").innerHTML=data;
			//document.getElementById("token_ws").value=response.token;
		},
					 
	});
	 
 }
 function findProduct2()
 {
	var name = document.getElementById("name").value;
	
	$.ajax({
		type:'POST',
		url:'modules/inventory/view/sProduct.php',
		async:false,
		data:"name="+name,
		success:function(data){	

			document.getElementById("showPl").innerHTML=data;
			//document.getElementById("token_ws").value=response.token;
		},
					 
	});
	 
 }
</script>


</div>
