<?php
 include('modules/inventory/model/inventory.php');
 
 $obj = new inventory();
 $obj->connect();
 
 if(!$obj->Checker()){
  $ins=true;
 }
 
 //Instala el modulo
 if($obj->getVars('install')){
  $obj->install();
  $msgIns=true;
 }
 
 
 $msg=false;
 
 //Acciones
 if($obj->getVars('ActionDel')==true){
  
  $msg=true;
 }
 
 ?>
  <?php
 if(!$ins){
 ?>
 <div class="widget3">
 <div class="widgetlegend">Inventario </div>
 <?php
  if($msg)
  {
  ?>
   <div class="ui-widget">
	<div class="ui-state-highlight ui-corner-all" style="margin-top: 20px; padding: 0 .7em;">
		<p><span class="ui-icon ui-icon-info" style="float: left; margin-right: .3em;"></span>
		<strong>Felicitaciones!</strong> eliminado satisfactoriamente.</p>
	</div>
</div>
  <?php
  }
 ?>
<p style="width:100">
	<a href="<?php $_SERVER['PHP_SELF'];?>?p=modules/inventory/view/newCapsInvIn.php" class="btn_normal" style="float:left; margin:5px;">Nuevo </a>
</p>
 <br /><br /><br />

<form action="" method="post">
<table width="100%" height="48" border="0">
 <tr>
  <td width="270">
	<select name="id_wh" required>
		<option value="">--Bodega--</option>
		<?php
			$row = $obj->getWarehouse();
			if(count($row)>0)
			{
				foreach($row as $row)
				{
					?>
					<option value="<?php echo $row['iw_id']?>"><?php echo utf8_encode($row['iw_name'])?></option>
					<?php
				}
			}
		?>
	</select>
  </td>
  <td width="130"><input type="submit" name="button" id="button" value="Buscar" class="btn_submit" /></td>
 </tr>
</table>
</form>
 
<table width="100%" height="48" border="0">
  <tr>
    <th width="30">ID</th>
    <th width="142">Nombre</th>
    <th width="142">Inventario Minimo</th>
    <th width="142">Inventario Actual</th>
    <th width="142">Bodega</th>
  </tr>
  <?php
   if($_POST)
   {
       $id_wh = $obj->postVars('id_wh');
       $row1 = $obj->getCapsInvProdByWarehouse($id_wh);
   }
   
   
   if(count($row1)>0){
 	foreach($row1 as $row1){
  ?>
  <tr>
    <td><?php echo $row1["ip_id"];?></td>
    <td><?php echo $row1["ip_name"];?></td>
    <td><?php echo $row1["ip_minstock"];?></td>
    <td><?php echo $row1["ip_stock"];?></td>
    <td><?php 
            $row2=$obj->getWarehouseById($row1['id_wh']);
		foreach($row2 as $row2){
		 echo $who_name=$row2["iw_name"];
		}
    
    ?></td>
  </tr>
  <?php
   }
  } 
  ?>
</table>

</div>
<?php } else {?>
<br />
 
 <div class="widget3">
 <div class="widgetlegend">Instalador Modulo de Invetario </div>
 <?php
  if($msgIns)
  {
  ?>
   <div class="ui-widget">
	<div class="ui-state-highlight ui-corner-all" style="margin-top: 20px; padding: 0 .7em;">
		<p><span class="ui-icon ui-icon-info" style="float: left; margin-right: .3em;"></span>
		<strong>Felicitaciones!</strong> El modulo ha sido instalado satisfactoriamente.</p>
	</div>
</div>
  <?php
  }
 ?>
<form id="form1" name="form1" method="post" action="#">
  <table width="804" border="0" style="float:left;">
    <tr>
      <td width="525" valign="top">&nbsp; </td>
    </tr>
    <tr>
      <td valign="top"><div align="right"><a href="<?php $_SERVER['PHP_SELF'];?>?p=modules/inventory/view/index.php&amp;install=true" class="btn_normal">Instalar</a></div></td>
    </tr>
  </table>
</form>
</div>
<?php } ?>

