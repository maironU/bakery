<?php
 include('modules/inventory/model/inventory.php');
 include('modules/products/model/products.php');
 include('modules/crm/model/customerModel.php');

 
 $obj=new inventory();
 $obj->connect();
 
 $products=new products();
 $products->connect();
 
 $customer=new customerModelGe();
 $customer->connect();
 
 $profiles = $obj->getConfigValue(3);
 $profiles = explode('-',$profiles);
 
 $up = $obj->getUserProfile($_SESSION['user_id']);
 
 $msg=false;
 
 if($_POST)
 {
 	$desc=$obj->postVars('desc');
 	$id_move=$obj->postVars('type');
 	$who=$obj->postVars('who');
 	$whd=$obj->postVars('whd');
 	$state=$obj->postVars('state');
 	$id_type=$obj->postVars('id_type');
 	
 	$row = $obj->getReferralStateById($state);
 	if(count($row)>0)
 	{
 		foreach($row as $row)
 		{
 			$run = $row["irs_runmov"];
 		}
 	}
 	
 	$row = $obj->getWarehouseById($who);
 	if(count($row)>0)
 	{
 		foreach($row as $row)
 		{
 			$whnameo = $row["iw_name"];
 		}
 	}
 	
 	$row = $obj->getWarehouseById($whd);
 	if(count($row)>0)
 	{
 		foreach($row as $row)
 		{
 			$whnamed = $row["iw_name"];
 		}
 	}
 	
 	$id_referral = $obj->newReferral($desc, $state, $_SESSION['user_id'], $id_move, $id_invoice, $id_po, $id_customer, $who, $whd, $whnameo, $whnamed, $id_type);
 	
 	$row = $obj->getReferralDetailTemp($_SESSION['user_id']);
	if(count($row)>0)
	{
		foreach($row as $row)
		{
			$obj-> newReferralDetail($id_referral, $row['irdt_productname'], $row['id_product'], $row["qty"], $row['irdt_price'], $row["qty2"], $row["qty3"]);//Detalle remision
			$t += $row['irdt_price'] * $row['qty'];
			if($run == 1)
			{
			    $id_ipo = $obj->getCapsInvProdByProductWh($row['id_product'], $who); 
                $id_ipd = $obj->getCapsInvProdByProductWh($row['id_product'], $whd);
			    
				if($who == $whd)
				{
				    $obj->registerMov($id_ipo,$row["qty"],$_SESSION['user_id'],$id_account,$id_move,$id_product,$desc,$who,$whd,$id_referral);    
				}
				else
				{
				    $obj->registerMov($id_ipo,$row["qty"],$_SESSION['user_id'],$id_account,$id_move,$row['id_product'],$desc,$who,$whd,$id_referral);
				    $obj->registerMov($id_ipd,$row["qty"],$_SESSION['user_id'],$id_account,1,$row['id_product'],$desc,$who,$whd,$id_referral);
				}
			}
		}
	}
	
	$obj->editReferralTotal($id_referral, $t);
	$obj->flushReferralDetailTemp($_SESSION['user_id']);
	$msg=true;
 	
 }
 
?>
<!-- Add mousewheel plugin (this is optional) -->
<script type="text/javascript" src="modules/inventory/js/jquery.mousewheel-3.0.6.pack.js"></script>
<!-- Add fancyBox main JS and CSS files -->
<script type="text/javascript" src="modules/inventory/js/jquery.fancybox.js?v=2.1.4"></script>
<link rel="stylesheet" type="text/css" href="modules/inventory/css/jquery.fancybox.css?v=2.1.4" media="screen" />
<link rel="stylesheet" type="text/css" href="modules/inventory/css/stylePos.css" />

 
<div class="widget3">
 <div class="widgetlegend">Nueva Remision  Por Perdida</div>
 <?php
  if($msg)
  {
  ?>
   <div class="ui-widget">
	<div class="ui-state-highlight ui-corner-all" style="margin-top: 20px; padding: 0 .7em;">
		<p><span class="ui-icon ui-icon-info" style="float: left; margin-right: .3em;"></span>
		<strong>Felicitaciones!</strong> Se ha sido guardado satisfactoriamente.</p>
	</div>
</div>
  <?php
  }
 ?>
<p style="width:100">
    <a href="<?php $_SERVER['PHP_SELF'];?>?p=modules/inventory/view/showReferrals.php" class="btn_normal" style="float:left; margin:5px;">Volver </a>
</p>
 <form action="#" name="formref1" method="post">
<table width="981" border="0">
 <td valign="top">
 	<table width="100%">
	 <tr>
 	        <td>
 	            <div id="MsgProductAdd" style="display: none">
	          <div class="ui-widget">
                	<div class="ui-state-highlight ui-corner-all" style="margin-top: 20px; padding: 0 .7em;">
                		<p><span class="ui-icon ui-icon-info" style="float: left; margin-right: .3em;"></span>
                		<strong>Informacion!</strong> Producto agregado satisfactoriamente.</p>
                	</div>
                </div>
	      </div>
	      <div id="MsgProductDel" style="display: none">
	          <div class="ui-widget">
                	<div class="ui-state-highlight ui-corner-all" style="margin-top: 20px; padding: 0 .7em;">
                		<p><span class="ui-icon ui-icon-info" style="float: left; margin-right: .3em;"></span>
                		<strong>Informacion!</strong> Producto eliminado satisfactoriamente.</p>
                	</div>
                </div>
	      </div>
 	        </td>
 	    </tr>
	 <tr>
	     <td><label>Cantidad: </label><br />
	         <input type="text" name="qty" id="qty" value="0" /></td>
	 </tr>
	 <tr>
	     <td>Tipo de remision: <select name="id_type" id="id_type" required>
				<option value="">-- Tipo de Remision--</option>
				<?php
					$row = $obj->getReferralsType();
					if(count($row)>0)
					{
						foreach($row as $row)
						{
							?>
							<option value="<?php echo $row["irt_id"]?>"><?php echo utf8_encode($row["irt_name"])?></option>
							<?php
						}
					}
				?>
			  </select></td>
	 </tr>
	 <tr>
	  <td>
	      <label>Producto: </label><br />
        <div id="accordion">
  	       <h3>Buscar producto por referencia</h3>
  	        <div>
  	            <p>
  	                <table>
  	                    <tr>
  	                        <td><input type="text" name="ref" id="ref" placeholder="Referencia" /></td>
  	                        <td><input type="button" value="Buscar" class="btn_submit" onClick="findProduct()" /></td>
  	                    </tr>
  	                </table>
  	            </p>
  	        </div>
  	        <h3>Buscar producto por nombre</h3>
  	        <div>
  	            <p>
  	                <table>
  	                    <tr>
  	                        <td><input type="text" name="name" id="name" placeholder="Nombre de producto" /></td>
  	                        <td><input type="button" value="Buscar" class="btn_submit"  onClick="findProduct()"/></td>
  	                    </tr>
  	                </table>
  	            </p>
  	        </div>
  	    </div>
        <div id="findProd"></div>
	  </td>
	 </tr>
	 
	 <tr>
	  <td>
	   <div id="showL">
	       
	       <?php
	            if(in_array($up, $profiles))
	            {
	                ?>
	                <table border="0" width="100%">
			  <tr>
			      <th>&nbsp;</th>
				<th>Producto</th>
				<th>Cantidad</th>
				<th>Contenedor</th>
				<th>Cantidad Total</th>
				<th>Precio</th>
				<th>Total</th>
				<th>&nbsp;</th>
			  </tr>
			  <?php
				$row = $obj->getReferralDetailTemp($_SESSION['user_id']);
				if(count($row)>0)
				{
					foreach($row as $row)
					{
						?>
						<tr>
						    <td><img src="modules/products/imagesProd/<?php echo $obj->getProductImage($row["id_product"]);?>" width="50" height="50" /></td>
							<td><?php echo $row["irdt_productname"];?></td>
							<td><?php echo $row["qty3"];?></td>
							<td><?php echo $row["qty2"];?></td>
							<td><?php echo $row["qty"];?></td>
							<td>$<?php echo number_format($row["irdt_price"],2);?></td>
							<td>$<?php echo number_format($row["irdt_price"]*$row["qty"],2);?></td>
							<td><a href="javascript:;" class="btn_2" onClick="delProduct(<?php echo $row['irdt_id']?>)">Borrar</a></td>
						</tr>
						<?php
					}
				}
			  ?>
		</table>
	                <?php
	            }
	            else
	            {
	                ?>
	                <table border="0" width="100%">
			  <tr>
			      <th>&nbsp;</th>
				<th>Producto</th>
				<th>Cantidad</th>
				<th>Contenedor</th>
				<th>Cantidad Total</th>
				<th>&nbsp;</th>
			  </tr>
			  <?php
				$row = $obj->getReferralDetailTemp($_SESSION['user_id']);
				if(count($row)>0)
				{
					foreach($row as $row)
					{
						?>
						<tr>
						    <td><img src="modules/products/imagesProd/<?php echo $obj->getProductImage($row["id_product"]);?>" width="50" height="50" /></td>
							<td><?php echo $row["irdt_productname"];?></td>
							<td><?php echo $row["qty3"];?></td>
							<td><?php echo $row["qty2"];?></td>
							<td><?php echo $row["qty"];?></td>
							<td><a href="javascript:;" class="btn_2" onClick="delProduct(<?php echo $row['irdt_id']?>)">Borrar</a></td>
						</tr>
						<?php
					}
				}
			  ?>
		</table>
	                <?php
	            }
	       ?>
	       
		
	    
	   </div>
	   <div id="gauch2" style="display:none"><img src="modules/inventory/image/loading.gif" /></div>
	  </td>
	 </tr>
	</table>
 </td>
 <td valign="top">
	
		<table width="100%">
			<tr>
				<td>Motivo de remision: <textarea name="desc" cols="30" rows="4" required></textarea></td>
			</tr>
			<tr>
				<td>Tipo de movimiento: <select name="type" required>
				<option value="">-- Tipo de Movimiento--</option>
				<?php
					$row = $obj->getMovTypebyAction('out');
					if(count($row)>0)
					{
						foreach($row as $row)
						{
							?>
							<option value="<?php echo $row["imt_id"]?>"><?php echo utf8_encode($row["imt_name"])?></option>
							<?php
						}
					}
				?>
			  </select></td>
			</tr>
			<tr>
				<td><label>Bodega de Origen: </label><br />
				  <select name="who" required>
					<option value="">--Bodega--</option>
					<?php
						$row = $obj->getWarehouse();
						if(count($row)>0)
						{
							foreach($row as $row)
							{
								?>
								<option value="<?php echo $row['iw_id']?>"><?php echo utf8_encode($row['iw_name'])?></option>
								<?php
							}
						}
					?>
				</select>
				 </td>
			  </tr>
			  <tr>
				<td><label>Bodega de Destino: </label><br />
				  <select name="whd">
					<option value="">--Bodega--</option>
					<?php
						$row = $obj->getWarehouse();
						if(count($row)>0)
						{
							foreach($row as $row)
							{
								?>
								<option value="<?php echo $row['iw_id']?>"><?php echo utf8_encode($row['iw_name'])?></option>
								<?php
							}
						}
					?>
				</select>
				 </td>
			  </tr>
			  <tr>
				<td><label>Estado de Remision: </label><br />
				  <select name="state" required>
					<option value="">--Estado--</option>
					<?php
						$row = $obj->getReferralState();
						if(count($row)>0)
						{
							foreach($row as $row)
							{
								?>
								<option value="<?php echo $row['irs_id']?>"><?php echo utf8_encode($row['irs_name'])?></option>
								<?php
							}
						}
					?>
				</select>
				 </td>
			  </tr>
			  <tr>
				<td>
					<input type="submit" class="btn_submit" value="Guardar" />
				</td>
			  </tr>
		</table>
	
  </td>
</table>
</form>
</div>
<script>
 function showInfo(id,site)
 {
	$.fancybox.open({
					href : site+'?id='+id,
					type : 'iframe',
					padding:2,
					'afterShow': function() {
						
        				//$('#fancybox-frame').contents().find("#barcode").focus();
						//document.frm1.barcode.focus();
					},
					'afterClose' : function(){
					 	//parent.location.reload(true);
					}
					
	}); 
 }
 
 function findProduct()
 {
	var ref = document.getElementById("ref").value;
	var name = document.getElementById("name").value;
	
	//document.getElementById("gauch1").style.display="block";
	$.ajax({
		type:'POST',
		url:'modules/inventory/view/findProduct.php',
		async:false,
		data:"ref="+ref+"&name="+name,
		success:function(data){
		    //alert(data);
			document.getElementById("findProd").innerHTML=data;
			//document.getElementById("gauch1").style.display="none";
			//document.getElementById("token_ws").value=response.token;
		},
					 
	});
	 
 }
 
 function addProduct(ref, stock)
 {
	var qty = document.getElementById("qty").value;
	var id_type = document.getElementById("id_type").value;
	
	document.getElementById("gauch2").style.display="block";
	$.ajax({
		type:'POST',
		url:'modules/inventory/view/addProduct.php',
		async:false,
		data:"ref="+ref+"&qty="+qty+"&id_type="+id_type,
		success:function(data){				
			document.getElementById("showL").innerHTML=data;
			document.getElementById("gauch2").style.display="none";
			document.getElementById("MsgProductAdd").style.display="block";
			document.getElementById("MsgProductDel").style.display="none";
			//document.getElementById("token_ws").value=response.token;
		},
					 
	});
	 
 }
 function delProduct(id)
 {
 	document.getElementById("gauch2").style.display="block";
	$.ajax({
		type:'POST',
		url:'modules/inventory/view/delProduct.php',
		async:false,
		data:"id="+id,
		success:function(data){				
			document.getElementById("showL").innerHTML=data;
			document.getElementById("gauch2").style.display="none";
			document.getElementById("MsgProductAdd").style.display="none";
			document.getElementById("MsgProductDel").style.display="block";
			//document.getElementById("token_ws").value=response.token;
		},
					 
	});
	 
 }
</script>
