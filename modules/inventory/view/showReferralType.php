<?php
include('modules/inventory/model/inventory.php');
 
 $obj = new inventory();
 $obj->connect();
 
 if($obj->getVars('actionDel')==true){
  $obj->delReferralsType(); 
 }
 
?>

<div class="widget3">
 <div class="widgetlegend">Tipos de Remisiones</div>
<p style="width:100">
	<a href="<?php $_SERVER['PHP_SELF'];?>?p=modules/inventory/view/showSetup.php" class="btn_normal" style="float:left; margin:5px;" >Volver </a>
    <a href="<?php $_SERVER['PHP_SELF'];?>?p=modules/inventory/view/newReferralType.php" class="btn_normal" style="float:left; margin:5px;">Nuevo </a>
</p>
 
<table width="803" height="48" border="0">
  <tr>
    <th width="30">ID</th>
    <th width="142">Nombre</th>
    <th colspan="2">Acciones</th>
  </tr>
  <?php
   $row=$obj->getReferralsType();
   
   if(count($row)>0){
 	foreach($row as $row){
  ?>
  <tr>
    <td><?php echo $row["irt_id"];?></td>
    <td><?php echo $row["irt_name"];?></td>
    <td width="86"><a href="<?php $_SERVER['PHP_SELF'];?>?p=modules/inventory/view/editReferralType.php&id=<?php echo $row["irt_id"]?>" class="btn_normal">Editar</a></td>
    <td width="86"><a href="<?php $_SERVER['PHP_SELF'];?>?p=modules/inventory/view/showReferralType.php&id=<?php echo $row["irt_id"]?>&actionDel=true" class="btn_borrar">Eliminar</a></td>
  </tr>
  <?php
   }
  } 
  ?>
</table>

</div>
