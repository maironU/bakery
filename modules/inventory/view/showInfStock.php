<?php
 include('modules/inventory/model/inventory.php');
 include('usr/model/User.php');
 
 $obj = new inventory();
 $obj->connect();
 
 $usr = new User();
 $usr->connect();
 
 
 $msg=false;
 
 //Acciones
 
 ?>
<div class="widget3">
 <div class="widgetlegend">Informe de Stock </div>
 <?php
  if($msg)
  {
  ?>
   <div class="ui-widget">
	<div class="ui-state-highlight ui-corner-all" style="margin-top: 20px; padding: 0 .7em;">
		<p><span class="ui-icon ui-icon-info" style="float: left; margin-right: .3em;"></span>
		<strong>Felicitaciones!</strong> eliminado satisfactoriamente.</p>
	</div>
</div>
  <?php
  }
 ?>
<p style="width:100">
    <a href="javascript" onClick="PrintElem('invInf')" class="btn_normal" style="float:left; margin:5px;" target="_blank">Imprimir </a>
    <a href="<?php $_SERVER['PHP_SELF'];?>?p=modules/inventory/view/showInf.php" class="btn_normal" style="float:left; margin:5px;">Volver </a>
</p>
 <br /><br /><br />

<form action="" method="post">
<table width="100%" height="48" border="0">
 <tr>
 	<td><select name="id_wh">
		<option value="">--Bodega--</option>
		<?php
			$row = $obj->getWarehouse();
			if(count($row)>0)
			{
				foreach($row as $row)
				{
					?>
					<option value="<?php echo $row['iw_id']?>"><?php echo utf8_encode($row['iw_name'])?></option>
					<?php
				}
			}
		?>
	</select></td>
 </tr>
 <tr>
 <td colspan="2"><input type="submit" name="button" id="button" value="Buscar" class="btn_submit" /></td>
 </tr>
</table>
</form>
<br /><br /><br />
<?php
 if($_POST){
?>
<div id="invInf">
<table width="807" height="48" border="0">
  <tr>
    <th width="30">ID</th>
    <th width="30">Producto</th>
    <th width="30">Stock Actual</th>
    <th width="142">Stock Minimo</th>
    <th width="142">Bodega</th>
  </tr>
<?php
 if($_POST){
  $row=$obj->getCapsInvProdByWarehouse($obj->postVars('id_wh'));
  if(count($row)>0){
  foreach($row as $row){
      if($row['ip_stock'] <= $row['ip_minstock']) {
         ?>
           <tr>
            <td><?php echo $row["ip_id"]?></td>
            <td><?php echo $row["ip_name"]?></td>
            <td><?php echo $row["ip_stock"]?></td>
            <td><?php echo $row["ip_minstock"]?></td>
            <td>
                <?php
                    $row1=$obj->getWarehouseById($row['id_wh']);
            		foreach($row1 as $row1){
            		 echo $row1["iw_name"];
            		}
                ?>
            </td>
           </tr>
        <?php 
      }
  }
  ?>
  <?php
  }
 }
?> 
</table>
</div>
<?php
 }
?>
</div>
<script>
function PrintElem(elem)
{
    var mywindow = window.open('', 'PRINT', 'height=600,width=800');

    mywindow.document.write('<html><head><title>INFORME DE INVENTARIO</title>');
    mywindow.document.write('</head><body >');
    mywindow.document.write('<h1>' + document.title  + '</h1>');
    mywindow.document.write(document.getElementById(elem).innerHTML);
    mywindow.document.write('</body></html>');

    mywindow.document.close(); // necessary for IE >= 10
    mywindow.focus(); // necessary for IE >= 10*/

    mywindow.print();
    mywindow.close();

    return true;
}
</script>


