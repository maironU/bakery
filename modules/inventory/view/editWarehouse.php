<?php
include('modules/inventory/model/inventory.php');
 
 $obj = new inventory();
 $obj->connect();
 
 $msg=false;
 
 if($_POST){
	$obj->editWarehouse();
	$msg=true;
 }
 
 $id=$obj->getVars('id');
 $row=$obj->getWarehouseById($id);
 foreach($row as $row)
 {
	 $name = $row['iw_name'];
	 $desc = $row['iw_desc'];
	 $resp = $row['iw_responsble'];
	 $img = $row['iw_img'];
	 $addr = $row['iw_addr'];
	 $phone = $row['iw_phone'];
	 $def = $row['iw_isdefault'];
 }

?>
<div class="widget3">
 <div class="widgetlegend">Editar Bodega </div>
 <?php
  if($msg)
  {
  ?>
   <div class="ui-widget">
	<div class="ui-state-highlight ui-corner-all" style="margin-top: 20px; padding: 0 .7em;">
		<p><span class="ui-icon ui-icon-info" style="float: left; margin-right: .3em;"></span>
		<strong>Felicitaciones!</strong> Se ha guardado satisfactoriamente.</p>
	</div>
</div>
  <?php
  }
 ?>
<p style="width:100">
	<a href="<?php $_SERVER['PHP_SELF'];?>?p=modules/inventory/view/showWarehouse.php" class="btn_normal" style="float:left; margin:5px;">Volver </a>
</p>
 
<form action="#" method="post" enctype="multipart/form-data" name="form1" id="form1">
<table width="803" height="48" border="0" style="float:left">
  <tr>
    <td><label>Nombre: </label><br />
      <input name="name" type="text" id="name" value="<?php echo $name?>" required />   </td>
  </tr>
  <tr>
    <td><label>Descripcion: </label><br />
      <textarea name="desc" cols="40" rows="6"><?php echo $desc?></textarea>  </td>
  </tr>
  <tr>
    <td><label>Responsable: </label><br />
      <input name="resp" type="text" id="resp" value="<?php echo $resp?>" required />   </td>
  </tr>
  <tr>
    <td><label>Imagen: </label><br />
      <input name="arch" type="file" id="arch" />   </td>
  </tr>
  <tr>
    <td><label>Direccion: </label><br />
      <input name="addr" type="text" id="addr" value="<?php echo $addr?>" required />   </td>
  </tr>
  <tr>
    <td><label>Telefono: </label><br />
      <input name="phone" type="text" id="phone" value="<?php echo $phone?>" required />   </td>
  </tr>
  <tr>
    <td><label>Bodega por defecto: </label><br />
    <?php
		if($def == 1)
		{
			?>
			<input type="checkbox" name="def" value="1" checked/>
			<?php
		}
		else
		{
			?>
			<input type="checkbox" name="def" value="1" />
			<?php
		}
	?>
	
	</td>
  </tr>
  <tr>
    <td>
	<input name="id" type="hidden" id="id" value="<?php echo $id?>" /> 
	<input type="submit" class="btn_submit" value="Guardar" /></td>
  </tr>
</table>
</form>


</div>
