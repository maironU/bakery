<?php
    session_start();
    include('../../../core/config.php');
	include('../../../core/ge.php');
	
	//Models
	include('../model/inventory.php');
 	include('../../products/model/products.php');
	
	//Model Objects
	$inv=new inventory();
 	$inv->connect();
 
 	$products=new products();
 	$products->connect();
	
		
 	$ref=$inv->postVars('ref');
 	$id_referral=$inv->postVars('id_referral');
 	$name=$inv->postVars('name');
 	
 	if($ref != '')
 	{
 	    $row=$products->findProduct2($ref);
 	} elseif ($name != '') {
 	    $row=$products->findProductByName($name);
 	}
	
	$html.='
	 <table border="0" width="100%">
	  <tr>
	    <th>&nbsp;</th>
	  	<th>Producto</th>
		<th>Referencia</th>
		<th>&nbsp;</th>
	  </tr>
	';
	if(count($row)>0)
	{
	    foreach($row as $row)
	    {
	    $html.="
	        <tr>
	            <td><img src='modules/products/imagesProd/".$row["pro_image"]."' width='50' height='50' /></td>
    			<td>".$row["pro_name"]."</td>
    			<td>".$row["pro_reference"]."</td>
    			<td><a href='javascript:;' class='btn_1' onClick='addProduct(".$row['pro_id'].",".$id_referral.",0)'>Agregar</a></td>
    		</tr>";
	    }
	}
	
	$html.="
	</table>";
	echo $html;
?>
