<?php
    session_start();
    include('../../../core/config.php');
	include('../../../core/ge.php');
	
	//Models
	include('../model/inventory.php');
 	include('../../products/model/products.php');
	
	//Model Objects
	$inv=new inventory();
 	$inv->connect();
 
 	$products=new products();
 	$products->connect();
	
		
 	$id=$inv->postVars('id');
 	$qty=$inv->postVars('qty');
 	$action=$inv->postVars('action');

    if($action == "1"){
        $qty += 1;
    }else if($action == "0"){
        $qty -= 1;
    }

    $row = $inv->getReferralDetailTempById($id);

    $container = 0;
    if(count($row) > 0){
        foreach($row as $row){
            $container = $row["irdt_qty2"];
        }
    }

    $qtytotal = $container*$qty;
	$inv->updateReferralDetailTemp($id, $qty, $qtytotal);
	
	$profiles = $inv->getConfigValue(3);
    $profiles = explode('-',$profiles);
 
    $up = $inv->getUserProfile($_SESSION['user_id']);
    
    if(in_array($up, $profiles))
    {
        $html.='
	 <table border="0" width="100%">
	  <tr>
	    <th>&nbsp;</th>
	  	<th>Producto</th>
	  	<th style="min-width: 70px">Cantidad</th>
							<th style="min-width: 80px">Contenedor</th>
							<th style="min-width: 60px">Total</th>
		<th>Precio</th>
		<th>Total</th>
		<th>&nbsp;</th>
	  </tr>
	';
	$row = $inv->getReferralDetailTemp($_SESSION['user_id']);
	if(count($row)>0){
	 foreach($row as $row){
	  $html.="
	   <tr>
	        <td><img src='modules/products/imagesProd/".$inv->getProductImage($row["id_product"])."' width='50' height='50' /></td>
			<td>".$row["irdt_productname"]."</td>
			<td>
                <input type='text' name='qty' id='qty_prod_".$row['irdt_id']."' value='".$row['qty3']."' onChange='editQtyItem(".$row["irdt_id"].", 2)' class='border-0 w-25' style='font-size: 15px; height: 20px'>
				<img src='images/icons/less.png' alt='' width='22px' class='m-0' onClick='editQtyItem(".$row["irdt_id"].", 0)'> 
				<img src='images/icons/more.png' alt='' width='22px' class='m-0' onClick='editQtyItem(".$row["irdt_id"].", 1)'>
			</td>
			<td>".$row["qty2"]."</td>
			<td>".$row["qty"]."</td>
			<td>$".number_format($row["irdt_price"],2)."</td>
			<td>$".number_format($row["irdt_price"],2)*$row["qty"]."</td>
			<td><a href='javascript:;' class='btn btn-danger' style='font-size: 11px' onClick='delProduct(".$row['irdt_id'].")'>Borrar</a></td>
		</tr>
	  ";
	 }
	}
	
	$html.="</table>";
    }
    else
    {
        $html.='
	 <table border="0" width="100%">
	  <tr>
	    <th>&nbsp;</th>
	  	<th>Producto</th>
	  	<th style="min-width: 70px">Cantidad</th>
							<th style="min-width: 80px">Contenedor</th>
							<th style="min-width: 60px">Total</th>
		<th>&nbsp;</th>
	  </tr>
	';
	$row = $inv->getReferralDetailTemp($_SESSION['user_id']);
	if(count($row)>0){
	 foreach($row as $row){
	  $html.="
	   <tr>
	        <td><img src='modules/products/imagesProd/".$inv->getProductImage($row["id_product"])."' width='50' height='50' /></td>
			<td>".$row["irdt_productname"]."</td>
			<td>
                <input type='text' name='qty' id='qty_prod_".$row['irdt_id']."' value='".$row['qty3']."' onChange='editQtyItem(".$row["irdt_id"].", 2)' class='border-0 w-25' style='font-size: 15px; height: 20px'>
				<img src='images/icons/less.png' alt='' width='22px' class='m-0' onClick='editQtyItem(".$row["irdt_id"].", 0)'> 
				<img src='images/icons/more.png' alt='' width='22px' class='m-0' onClick='editQtyItem(".$row["irdt_id"].", 1)'>
			</td>
			<td>".$row["qty2"]."</td>
			<td>".$row["qty"]."</td>
			<td><a href='javascript:;' class='btn btn-danger' style='font-size: 11px' onClick='delProduct(".$row['irdt_id'].")'>Borrar</a></td>
		</tr>
	  ";
	 }
	}
	
	$html.="</table>";
    }
	
	
	echo $html;
?>
