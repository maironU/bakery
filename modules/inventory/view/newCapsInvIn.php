<?php
	include('modules/inventory/model/inventory.php');
	include('modules/products/model/products.php');
	
	$obj = new inventory();
	$obj->connect();
	
	$products = new products();
	$products->connect();

?>
<style>
	.swal2-popup {
		background: #fff !important;
	}
</style>
<link href="//cdn.jsdelivr.net/npm/@sweetalert2/theme-dark@4/dark.css" rel="stylesheet">
<script src="//cdn.jsdelivr.net/npm/sweetalert2@11/dist/sweetalert2.min.js"></script>

<div class="card shadow p-3 mb-4">
	<div class="widgetlegend">Nuevo Ingreso </div>
	<div id="success-egress">
		
	</div>

	<div class="wizard mx-auto" style="width:60%">
		<div class="wizard-navigation">
			<ul class="pl-0">
				<li class="tab-active" data-wizard="bodega" id="tab-bodega"><a href="#">Bodega</a></li>
				<li data-wizard="producto" id="tab-producto"><a href="#">Producto</a></li>
			</ul>
		</div>
	</div>

	<div class="tab-content-wizard mx-auto" style="width: 65%">
		<div class="tab-pane-wizard" id="producto">
			<div class="row">
				<div class="accordion" id="accordionExample">
					<div class="accordion-item">
						<h2 class="accordion-header" id="headingOne">
						<button class="accordion-button" type="button" data-bs-toggle="collapse" data-bs-target="#collapseOne" aria-expanded="true" aria-controls="collapseOne">
							Buscar producto por referencia
						</button>
						</h2>
						<div id="collapseOne" class="accordion-collapse collapse show" aria-labelledby="headingOne" data-bs-parent="#accordionExample">
							<div class="accordion-body d-flex">
								<input style="width: 40%"  type="text" name="ref" id="ref" placeholder="Referencia" class="form-control mr-4">
								<button style="width: 40%" class="btn btn-success btn-block" onClick="findProduct()">Buscar</button>
							</div>
						</div>
					</div>
					<div class="accordion-item">
						<h2 class="accordion-header" id="headingTwo">
						<button class="accordion-button collapsed" type="button" data-bs-toggle="collapse" data-bs-target="#collapseTwo" aria-expanded="false" aria-controls="collapseTwo">
							Buscar producto por nombre
						</button>
						</h2>
						<div id="collapseTwo" class="accordion-collapse collapse" aria-labelledby="headingTwo" data-bs-parent="#accordionExample">
							<div class="accordion-body d-flex">
								<input style="width: 40%" type="text" name="name" id="name" placeholder="Nombre de producto" class="form-control mr-4">
								<button style="width: 40%" class="btn btn-success btn-block" onClick="findProduct2()">Buscar</button>
							</div>
						</div>
					</div>
				</div>

				<div id="showPl"></div>

				<div class="form-group mt-3">
					<label for="">Cantidad a ingresar</label>
					<input name="qty" type="number" id="qty" value="" required class="form-control w-100">
				</div>

				<div class="form-group d-flex flex-column">
					<label for="">Multiplicar por cantidad de contenedor: </label>
					<input name="container" id="container" type="checkbox" id="container" value="1" /> 
				</div>
                                                       
				<div class="form-group d-flex flex-column">
					<label for="">Descripcion</label>
					<textarea name="desc" cols="40" rows="5" id="desc" required class="form-control"></textarea>
				</div>
			</div>
		</div>

		<div class="tab-pane-wizard tab-pane-active" id="bodega">
			<div class="row">
				<div class="form-group">
					<label for="type">Tipo de movimiento</label>
					<select name="type" id="type" class="form-control" required>
						<option value="">-- Tipo de Movimiento--</option>
						<?php
							$row = $obj->getMovTypebyAction('in');
							if(count($row)>0)
							{
								foreach($row as $row)
								{
									?>
										<option value="<?php echo $row["imt_id"]?>"><?php echo utf8_encode($row["imt_name"])?></option>
									<?php
								}
							}
						?>
					</select>
				</div>

				<div class="form-group">
					<label for="who">Bodega de Origen:</label>
					<select name="who" id="who" class="form-control" required>
						<option value="">--Bodega--</option>
						<?php
							$row = $obj->getWarehouse();
							if(count($row)>0)
							{
								foreach($row as $row)
								{
									?>
									<option value="<?php echo $row['iw_id']?>"><?php echo utf8_encode($row['iw_name'])?></option>
									<?php
								}
							}
						?>
					</select>
				</div>

				<div class="form-group">
					<label for="whd">Bodega de Destino:</label>
					<select name="whd" id="whd" class="form-control" required>
						<option value="">--Bodega--</option>
						<?php
							$row = $obj->getWarehouse();
							if(count($row)>0)
							{
								foreach($row as $row)
								{
									?>
									<option value="<?php echo $row['iw_id']?>"><?php echo utf8_encode($row['iw_name'])?></option>
									<?php
								}
							}
						?>
					</select>
				</div>
			</div>
		</div>

		<div class="wizard-footer d-flex justify-content-between">
			<div class="pull-left">
				<input type='button' class='btn btn-previous btn-fill btn-wd btn-sm pull' name='next' value='Anterior' style="background: #538a9a; color: white"/>
			</div>

			<div class="pull-right">
				<input type='button' class='btn btn-next btn-fill btn-wd btn-sm pull-btn-active pull' name='next' value='Siguiente' style="background: #538a9a; color: white"/>
				<input type='button' class='btn btn-finish btn-fill btn-success btn-wd btn-sm pull' name='finish' value='Guardar' onclick="saveIngress()"/>
			</div>
		</div>
	</div>
</div>

<script type="text/javascript" src="js/wizard.js"></script>
<script>
	function saveIngress(){
		let qty= $('#qty').val();
		if(!isQty(qty)) return

		Swal.fire({
			title: 'Quiere realizar otro envio de producto con la misma configuracion?',
			showDenyButton: true,
			//showCancelButton: true,
			confirmButtonText: 'Si',
			denyButtonText: `No`,
			confirmButtonColor: '#49bf9d',
			})
			.then((result) => {
				if (result.isConfirmed) {
					fetchIngress(true)
				}else if (result.isDenied) {
					fetchIngress(false)
				}
			})
			$("#swal2-title").css("color", "#a2a2a2")
	}

	function fetchIngress(resend){

		let id_product = $("input[name=id_product]:checked", "table").val()
		let qty= $('#qty').val();
		let type= $('#type').val();
		let desc= $('#desc').val();
		let who= $('#who').val();
		let whd= $('#whd').val();
		let container= $('#container').val();

		let data = {
			id_product,
			qty,
			type,
			desc,
			who,
			whd,
			container 
		}

		$.post('modules/inventory/model/api.php?method=saveIngress', data).then(res => {
			let response = JSON.parse(res)
			if(response.success){
				Toastify({
                        text: `Felicitaciones ${response.message}`,
                        duration: 2000,
                        newWindow: true,
                        close: true,
                        position: "center", // `left`, `center` or `right`
                        stopOnFocus: true, // Prevents dismissing of toast on hover
                        style: {
                            background: "#1cc88a",
                        },
                        onClick: function() {} // Callback after click
                    }).showToast();
				if(!resend) emptyAll()
			}
		})
	}

	function emptyAll(){
		$('#id_product').val("");
		$('#qty').val("");
		$('#type').val("");
		$('#desc').val("");
		$('#who').val("");
		$('#whd').val("");
		$('#ref').val("");
		$('#showPl').html("");
		$('#container').prop("checked", false);

		active = "bodega"
		activeTab(active)
		showButtonsFooter()
	}

	function findProduct()
	{
		var ref = document.getElementById("ref").value;
		
		if(ref == '')
		{
			alert("Indique una referencia del producto");
			return;
		}
		
		$.ajax({
			type:'POST',
			url:'modules/inventory/view/sProduct.php',
			async:false,
			data:"ref="+ref,
			success:function(data){				
				document.getElementById("showPl").innerHTML=data;
				//document.getElementById("token_ws").value=response.token;
			},
						
		});
		
	}
	function findProduct2()
	{
		var name = document.getElementById("name").value;
		
		if(name == '')
		{
			alert("Indique el nombre del producto a buscar");
			return;
		}
		
		$.ajax({
			type:'POST',
			url:'modules/inventory/view/sProduct.php',
			async:false,
			data:"name="+name,
			success:function(data){	

				document.getElementById("showPl").innerHTML=data;
				//document.getElementById("token_ws").value=response.token;
			},
						
		});
		
	}

	function messageError(message){
		Toastify({
			text: message,
			duration: 2000,
			newWindow: true,
			close: true,
			position: "center", // `left`, `center` or `right`
			stopOnFocus: true, // Prevents dismissing of toast on hover
			style: {
				background: "#dc3545",
			},
			onClick: function() {} // Callback after click
		}).showToast();
	}

	function isQty(qty){
		if(qty == ""){
			messageError(`La cantidad no puede estar vacia`)
			return false
		}else if(qty == 0){
			messageError(`La cantidad no puede ser cero`);
			return false
		}

		return true
	}
</script>

