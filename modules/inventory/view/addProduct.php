<?php
    session_start();
    include('../../../core/config.php');
	include('../../../core/ge.php');
	
	//Models
	include('../model/inventory.php');
 	include('../../products/model/products.php');
	
	//Model Objects
	$inv=new inventory();
 	$inv->connect();
 
 	$products=new products();
 	$products->connect();
	
		
 	$ref=$inv->postVars('ref');
	$qty=$inv->postVars('qty');
	$id_type=$inv->postVars('id_type');
	
	$row=$products->getProductsById($ref);
	 foreach($row as $row){
	  $product_id=$row["pro_id"];
	  $product_name=$row["pro_sdesc"];
	  $product_costprice=$row["pro_costprice"];
	  $product_salesprice=$row["pro_price"];
	  $product_qtycontainer=$row["pro_qtycontainer"];
	  $inv_id=$row["inv_id"];
	}
	
	if($id_type == 2)
	{
	    $product_qtycontainer = 1;
	    $product_price = $product_salesprice;
	}
	else
	{
	    $product_price = $product_costprice;
	}
	
	$qtyt = $product_qtycontainer * $qty;
	
	$profiles = $inv->getConfigValue(3);
    $profiles = explode('-',$profiles);
 
    $up = $inv->getUserProfile($_SESSION['user_id']);
	
	
	$inv->newReferralDetailTemp($_SESSION['user_id'], $product_name, $product_id, $qtyt, $product_price, $product_qtycontainer, $qty);
	if(in_array($up, $profiles))
	{
	   $html.='
	 <table border="0" width="100%">
	  <tr>
	  	<th>&nbsp;</th>
	  	<th>Producto</th>
	  	<th style="min-width: 70px">Cantidad</th>
							<th style="min-width: 80px">Contenedor</th>
							<th style="min-width: 60px">Total</th>
		<th>Precio</th>
		<th>Total</th>
		<th>&nbsp;</th>
	  </tr>
	';
	$row = $inv->getReferralDetailTemp($_SESSION['user_id']);
	if(count($row)>0){
	 foreach($row as $row){
	  $html.="
	   <tr>
			<td><img src='modules/products/imagesProd/".$inv->getProductImage($row["id_product"])."' width='50' height='50' /></td>
			<td>".$row["irdt_productname"]."</td>
			<td>
				<input type='text' name='qty' id='qty_prod_".$row['irdt_id']."' value='".$row['qty3']."' onChange='editQtyItem(".$row["irdt_id"].", 2)' class='border-0 w-25' style='font-size: 15px; height: 20px'>
				<img src='images/icons/less.png' alt='' width='22px' class='m-0' onClick='editQtyItem(".$row["irdt_id"].", 0)'> 
				<img src='images/icons/more.png' alt='' width='22px' class='m-0' onClick='editQtyItem(".$row["irdt_id"].", 1)'>
			</td>
			<td>".$row["qty2"]."</td>
			<td>".$row["qty"]."</td>
			<td>$".number_format($row["irdt_price"],2)."</td>
			<td>$".number_format($row["irdt_price"],2)*$row["qty"]."</td>
			<td><a href='javascript:;' class='btn btn-danger' onClick='delProduct(".$row['irdt_id'].")'>Borrar</a></td>
		</tr>
	  ";
	 }
	}
	
	$html.="</table>"; 
	}
	else
	{
	    $html.='
	 <table border="0" width="100%">
	  <tr>
	    <th>&nbsp;</th>
	  	<th>Producto</th>
	  	<th style="min-width: 70px">Cantidad</th>
							<th style="min-width: 80px">Contenedor</th>
							<th style="min-width: 60px">Total</th>
		<th>&nbsp;</th>
	  </tr>
	';
	$row = $inv->getReferralDetailTemp($_SESSION['user_id']);
	if(count($row)>0){
	 foreach($row as $row){
	  $html.="
	   <tr>
	        <td><img src='modules/products/imagesProd/".$inv->getProductImage($row["id_product"])."' width='50' height='50' /></td>
			<td>".$row["irdt_productname"]."</td>
			<td>
				<input type='text' name='qty' id='qty_prod_".$row['irdt_id']."' value='".$row['qty3']."' onChange='editQtyItem(".$row["irdt_id"].", 2)' class='border-0 w-25' style='font-size: 15px; height: 20px'>
				<img src='images/icons/less.png' alt='' width='22px' class='m-0' onClick='editQtyItem(".$row["irdt_id"].", 0)'> 
				<img src='images/icons/more.png' alt='' width='22px' class='m-0' onClick='editQtyItem(".$row["irdt_id"].", 1)'>
			</td>
			<td>".$row["qty2"]."</td>
			<td>".$row["qty"]."</td>
			<td><a href='javascript:;' class='btn btn-danger' onClick='delProduct(".$row['irdt_id'].")'>Borrar</a></td>
		</tr>
	  ";
	 }
	}
	
	$html.="</table>";
	}
	
	echo $html;
?>
