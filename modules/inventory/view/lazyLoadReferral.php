<?php 
    session_start();
    include('../../../core/config.php');
    include('../../../core/ge.php');

	include('../model/inventory.php');
    include('../../crm/model/customerModel.php');
    include('../../../usr/model/User.php');

    $obj = new inventory();
    $obj->connect();
    
    $customer = new customerModelGe();
    $customer->connect();
    
    $user = new User();
    $user->connect();

    if(isset($_GET)){

        $minLimit = $_GET['start'] == 0 ? 0 : intval($_GET['start']);
        $maxLimit =  $_GET['length'];
        $id_type = isset($_GET['id_type']) ? $_GET['id_type'] : null;
        $type = isset($_GET['type']) ? $_GET['type'] : null;
        $track = isset($_GET['track']) ? $_GET['track'] : null;

        $referrals = $obj->getReferralsLazyLoad($minLimit, $maxLimit, $type, $track, $id_type);
        $total = $obj->getReferralsLazyLoadTotal($minLimit, $maxLimit, $type, $track, $id_type);

        $data = [];
        $total_data = count($total);

        if(count($referrals)>0){
            foreach($referrals as $row){
                $arrayRow = [];

                array_push($arrayRow, $row["ir_id"]);
                array_push($arrayRow, $row["ir_date"]);
                array_push($arrayRow, $row["ir_track"]);
                array_push($arrayRow, referralState($obj, $row));
                array_push($arrayRow, movType($obj, $row));
                array_push($arrayRow, $row["id_invoice"]);
                array_push($arrayRow, $row["id_po"]);
                array_push($arrayRow, customer($customer, $row));
                array_push($arrayRow, $row["ir_who"]);
                array_push($arrayRow, $row["ir_whd"]);
                array_push($arrayRow, user($user, $row));
                array_push($arrayRow, referrals($obj, $row));
                array_push($arrayRow, '<div style="    min-width: 240px!important;">'.edit($obj, $row).'</div>');
                    
                array_push($data, $arrayRow);
            }   
        }

        echo json_encode(["data" => $data, "recordsFiltered" => $total_data, "recordsTotal" => $total_data]);
    }
    

    function referralState($obj, $row){
        $row1 = $obj->getReferralStateById($row["ir_state"]);
        if(count($row1)>0){
            foreach($row1 as $row1)
            {
                $td = '<div style="background-color: #'.$row1['irs_color'].'">'.utf8_encode($row1['irs_name']).'</div>';
            }
        }
        return $td;
    }

    function movType($obj, $row){
        $row1 = $obj->getMovTypeById($row["imt_id"]);
        if(count($row1)>0){
            foreach($row1 as $row1)
            {
                $td = $row1["imt_name"];
            }
        }
        return $td;
    }

    function customer($customer, $row){
        $row1 = $customer->getCustomerById2($row["id_customer"]);
        if(count($row1)>0){
            foreach($row1 as $row1)
            {
                $td = $row1["c_nom"];
            }
        }
        return $td;
    }

    function user($user, $row){
        $row1 = $user->getUserById2($row["u_id"]);
        if(count($row1)>0){
            foreach($row1 as $row1)
            {
                $td = $row1["u_nom"];
            }
        }
        return $td;
    }

    function referrals($obj, $row){
        $row1 = $obj->getReferralsTypeById($row["irt_id"]);
        if(count($row1)>0){
            foreach($row1 as $row1)
            {
                $td = $row1["irt_name"];
            }
        }
        return $td;
    }
    
    function edit($obj, $row){
        $td = '<a style="margin-right: 5px" href="javascript:;" class="btn btn-success" onclick="showInfo('.$row["ir_id"].',`modules/inventory/view/showPrintReferral.php`)">Ver</a>';

        $row1 = $obj->getReferralStateById($row["ir_state"]);
        if(count($row1)>0){
            foreach($row1 as $row1){
                $run = $row1["irs_runmov"];
                if($run != 1){
                    $td.= "
                        <a style='margin-right: 10px' class='btn btn-primary' href='". $_SERVER['PHP_SELF'].`?p=modules/inventory/view/editReferral.php&id=`.$row['ir_id']."' class='btn_1'>Editar</a>
                    ";
                }
            }
        }

        $td.= " <a style='margin-right: 5px' href='javascript:;' class='btn btn-danger' onClick='showInfo2(".$row['ir_id'].",`modules/inventory/view/delReferral.php`)'>Anular</a>";

        return $td;
    }
?>