<?php
include('modules/inventory/model/inventory.php');
 
 $obj = new inventory();
 $obj->connect();
 
 if($obj->getVars('actionDel')==true){
  $id = $obj->getVars('id'); 
  $obj->delReferralState($id); 
 }
 
?>

<div class="widget3">
 <div class="widgetlegend">Estados de Remisiones</div>
<p style="width:100">
	<a href="<?php $_SERVER['PHP_SELF'];?>?p=modules/inventory/view/showSetup.php" class="btn_normal" style="float:left; margin:5px;" >Volver </a>
    <a href="<?php $_SERVER['PHP_SELF'];?>?p=modules/inventory/view/newReferralState.php" class="btn_normal" style="float:left; margin:5px;">Nuevo </a>
</p>
 
<table width="803" height="48" border="0">
  <tr>
    <th width="30">ID</th>
    <th width="142">Nombre</th>
    <th width="213">Corre Movimiento</th>
    <th width="213">Color</th>
    <th colspan="2">Acciones</th>
  </tr>
  <?php
   $row=$obj->getReferralState();
   
   if(count($row)>0){
 	foreach($row as $row){
  ?>
  <tr>
    <td><?php echo $row["irs_id"];?></td>
    <td><?php echo $row["irs_name"];?></td>
    <td><?php 
    if($row["irs_runmov"] == 1)
    {
    	echo "Si";
    }
    else
    {
    	echo "No";
    }
    
    ?></td>
    <td><div style="width: 30px; height: 30px; border-radius; 5px; background-color: #<?php echo $row['irs_color']?>; display: block">&nbsp;</div></td>
    <td width="86"><a href="<?php $_SERVER['PHP_SELF'];?>?p=modules/inventory/view/editReferralState.php&id=<?php echo $row["irs_id"]?>" class="btn_normal">Editar</a></td>
    <td width="86"><a href="<?php $_SERVER['PHP_SELF'];?>?p=modules/inventory/view/showReferralStates.php&id=<?php echo $row["irs_id"]?>&actionDel=true" class="btn_borrar">Eliminar</a></td>
  </tr>
  <?php
   }
  } 
  ?>
</table>

</div>
