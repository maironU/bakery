<?php
 include('modules/inventory/model/inventory.php');
 include('usr/model/User.php');
 
 $obj = new inventory();
 $obj->connect();
 
 $usr = new User();
 $usr->connect();
 
 
 $msg=false;
 
 //Acciones
 
 ?>
<div class="widget3">
 <div class="widgetlegend">Informes de Inventario </div>
 <?php
  if($msg)
  {
  ?>
   <div class="ui-widget">
	<div class="ui-state-highlight ui-corner-all" style="margin-top: 20px; padding: 0 .7em;">
		<p><span class="ui-icon ui-icon-info" style="float: left; margin-right: .3em;"></span>
		<strong>Felicitaciones!</strong> eliminado satisfactoriamente.</p>
	</div>
</div>
  <?php
  }
 ?>
<p style="width:100">
    <!--<a href="javascript" onClick="PrintElem('invInf')" class="btn_normal" style="float:left; margin:5px;" target="_blank">Imprimir </a>
    <a href="<?php $_SERVER['PHP_SELF'];?>?p=modules/inventory/view/showInfReferral.php" class="btn_normal" style="float:left; margin:5px;">Inf. Remisiones </a>
    <a href="<?php $_SERVER['PHP_SELF'];?>?p=modules/inventory/view/showInfProducts.php" class="btn_normal" style="float:left; margin:5px;">Inf. Mov. Productos </a>-->
</p>
 <br /><br /><br />

<br /><br /><br />
<table>
    <tr>
        <td><i class="fa fa-info-circle" style="font-size: 16px; color: #f00"></i></td>
        <td>Informe general de inventario</td>
        <td><a href="<?php $_SERVER['PHP_SELF'];?>?p=modules/inventory/view/showInfCapsInventory.php" class="btn_normal">Entrar</a></td>
    </tr>
    <tr>
        <td><i class="fa fa-info-circle" style="font-size: 16px; color: #0f0"></i></td>
        <td>Informe Informe de remisiones</td>
        <td><a href="<?php $_SERVER['PHP_SELF'];?>?p=modules/inventory/view/showInfReferral.php" class="btn_normal">Entrar</a></td>
    </tr>
    <tr>
        <td><i class="fa fa-info-circle" style="font-size: 16px; color: #ff0"></i></td>
        <td>Informe Movimientos de productos</td>
        <td><a href="<?php $_SERVER['PHP_SELF'];?>?p=modules/inventory/view/showInfProducts.php" class="btn_normal">Entrar</a></td>
    </tr>
    <tr>
        <td><i class="fa fa-info-circle" style="font-size: 16px; color: #f0f"></i></td>
        <td>Informe de Stock</td>
        <td><a href="<?php $_SERVER['PHP_SELF'];?>?p=modules/inventory/view/showInfStock.php" class="btn_normal">Entrar</a></td>
    </tr>
</table>
</div>
<script>
function PrintElem(elem)
{
    var mywindow = window.open('', 'PRINT', 'height=600,width=800');

    mywindow.document.write('<html><head><title>INFORME DE INVENTARIO</title>');
    mywindow.document.write('</head><body >');
    mywindow.document.write('<h1>' + document.title  + '</h1>');
    mywindow.document.write(document.getElementById(elem).innerHTML);
    mywindow.document.write('</body></html>');

    mywindow.document.close(); // necessary for IE >= 10
    mywindow.focus(); // necessary for IE >= 10*/

    mywindow.print();
    mywindow.close();

    return true;
}
</script>


