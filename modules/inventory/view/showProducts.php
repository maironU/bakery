<?php
 include('modules/inventory/model/inventory.php');
 include('modules/products/model/products.php');
 
 $obj = new inventory();
 $obj->connect();
 
 $products = new products();
 $products->connect();
 
 
 $msg=false;
 
 //Acciones
 if($_POST){
   $num=$obj->postVars('num');
   for($i=1;$i<=$num;$i++){
    
	$caps="caps".$i;
	$id_pro="id_pro".$i;
	$qty="qty".$i;
	
	$fcaps=$obj->postVars($caps);
	$fid_pro=$obj->postVars($id_pro);
	$fqty=$obj->postVars($qty);
	
	$obj->editProduct($fid_pro,$fcaps,$fqty);
	
   }
   
   $msg=true;
  }
 
 ?>
 <div class="widget3">
 <div class="widgetlegend">Productos - Inventario </div>
 <?php
  if($msg)
  {
  ?>
   <div class="ui-widget">
	<div class="ui-state-highlight ui-corner-all" style="margin-top: 20px; padding: 0 .7em;">
		<p><span class="ui-icon ui-icon-info" style="float: left; margin-right: .3em;"></span>
		<strong>Felicitaciones!</strong> guardado satisfactoriamente.</p>
	</div>
</div>
  <?php
  }
 ?>
<p style="width:100">
	<a href="<?php $_SERVER['PHP_SELF'];?>?p=modules/inventory/view/showSetup.php" class="btn_normal" style="float:left; margin:5px;">Volver </a>
</p><br /><br /><br />
 <form action="" method="post" name="form1">
<table width="803" height="48" border="0">
  <tr>
    <th width="30">ID</th>
    <th width="30">Referencia</th>
    <th width="142">Producto</th>
    <th width="142">Descripcion</th>
    <th width="142">Consumo</th>
  </tr>
  <?php
   $i=1;
   $row=$products->getProducts();
   if(count($row)>0){
 	foreach($row as $row){
		
		$caps="caps".$i;
		$id_pro="id_pro".$i;
		$qty="qty".$i;
  ?>
  <tr>
    <td><?php echo $row["pro_id"];?></td>
    <td><?php echo $row["pro_reference"];?></td>
    <td><?php echo $row["pro_name"];?></td>
    <td><?php echo $row["pro_sdesc"];?></td>
    <td>
    <input name="<?php echo $qty?>" type="text" value="<?php echo $row["inv_qty"]?>" />
    <input name="<?php echo $id_pro?>" type="hidden" value="<?php echo $row["pro_id"]?>" />
    <select name="<?php echo $caps?>">
     <option value="0">No usa inventario</option>
     <?php
        $row1=$obj->getCapsInvProd();
		foreach($row1 as $row1){
		if($row1["ip_id"]==$row["inv_id"]){
		?>
        <option value="<?php echo $row1["ip_id"];?>" selected="selected"><?php echo utf8_encode($row1["ip_name"]);?></option>
        <?php
		}else{
		?>
        <option value="<?php echo $row1["ip_id"];?>"><?php echo utf8_encode($row1["ip_name"]);?></option>
        <?php
		}	
		
		}
	   ?>
    </select>
    </td>
  </tr>
  <?php
    $i++;
   }
  } 
  ?>
  <tr>
   <td colspan="5"><div align="left"><a href="javascript:;" class="btn_normal" onclick="document.form1.submit();">Guardar</a></div></td>
  </tr>
  <input type="hidden" name="num" value="<?php echo $i-1?>" />
</table>
</form>

</div>
