<?php
include('modules/inventory/model/inventory.php');
 
 $obj = new inventory();
 $obj->connect();
 
 if($obj->getVars('actionDel')==true){
  $obj->delEmail(); 
 }
 
?>

<div class="widget3">
 <div class="widgetlegend">Emails</div>
<p style="width:100">
	<a href="<?php $_SERVER['PHP_SELF'];?>?p=modules/inventory/view/showSetup.php" class="btn_normal" style="float:left; margin:5px;" >Volver </a>
    <a href="<?php $_SERVER['PHP_SELF'];?>?p=modules/inventory/view/newEmail.php" class="btn_normal" style="float:left; margin:5px;">Nuevo </a>
</p>
 
<table width="803" height="48" border="0">
  <tr>
    <th width="30">ID</th>
    <th width="142">Nombre</th>
    <th width="213">Email</th>
    <th colspan="2">Acciones</th>
  </tr>
  <?php
   $row=$obj->getEmails();
   
   if(count($row)>0){
 	foreach($row as $row){
  ?>
  <tr>
    <td><?php echo $row["ie_id"];?></td>
    <td><?php echo $row["ie_name"];?></td>
    <td><?php echo $row["ie_mails"];?></td>
    <td width="86"><a href="<?php $_SERVER['PHP_SELF'];?>?p=modules/inventory/view/editEmail.php&id=<?php echo $row["ie_id"]?>" class="btn_normal">Editar</a></td>
    <td width="86"><a href="<?php $_SERVER['PHP_SELF'];?>?p=modules/inventory/view/showEmails.php&id=<?php echo $row["ie_id"]?>&actionDel=true" class="btn_borrar">Eliminar</a></td>
  </tr>
  <?php
   }
  } 
  ?>
</table>

</div>
