<?php
include('modules/inventory/model/inventory.php');
include('modules/crm/model/customerModel.php');
include('usr/model/User.php');
 
 $obj = new inventory();
 $obj->connect();
 
 $customer = new customerModelGe();
 $customer->connect();
 
 $user = new User();
 $user->connect();
 
 $types_movi = $obj->getMovTypebyAction('in');

 if($obj->getVars('actionDel')==true){
	 
 }
 
?>
<!-- Add mousewheel plugin (this is optional) -->
<script type="text/javascript" src="modules/inventory/js/jquery.mousewheel-3.0.6.pack.js"></script>
<!-- Add fancyBox main JS and CSS files -->
<script type="text/javascript" src="modules/inventory/js/jquery.fancybox.js?v=2.1.4"></script>
<link rel="stylesheet" type="text/css" href="modules/inventory/css/jquery.fancybox.css?v=2.1.4" media="screen" />
<link rel="stylesheet" type="text/css" href="modules/inventory/css/stylePos.css" />

<style>
	table td, th {
		font-size: 11px;
	}

	@media (max-width: 768px){
		.content_filter {
			flex-direction: column;
			align-items: center;
		}
	}

	.dataTables_wrapper .dataTables_processing {
		position: absolute;
		top: 50%;
		left: 50%;
		width: 100%;
		height: 40px;
		margin-left: -50%;
		margin-top: -25px;
		padding-top: 20px;
		text-align: center;
		font-size: 1.2em;
	}
</style>
<div class="card shadow mb-4">
	<div class="widgetlegend" style="margin: 1rem">Pedidos </div>
	<div class="card-header py-3">
    	<a class="btn bg-color-bottom text-white" href="<?php $_SERVER['PHP_SELF'];?>?p=modules/inventory/view/newReferral.php" class="btn_normal" style="float:left; margin:5px;">Nuevo </a>
	</div>
	<div class="m-3 row content_filter">
		<div class="form-group d-flex flex-column col-12 col-sm-8 col-md-3">
			<label for="">Numero de pedido</label>
			<input type="text" name="track" id="track" autofocus class="form-control"/></td>
		</div>
		<div class="form-group col-12 col-sm-8 d-flex flex-column col-md-3">
			<label for="type">Tipo de movimiento</label>
			<select name="type" id="type" class="form-control" required>
				<option value="">-- Tipo de Movimiento--</option>
				<?php
					$row = $obj->getMovTypebyAction('in');
					
					if(count($row)>0)
					{
						foreach($row as $row)
						{
							?>
								<option value="<?php echo $row["imt_id"]?>"><?php echo utf8_encode($row["imt_name"])?></option>
							<?php
						}
					}
				?>
			</select>
		</div>
		<div class="form-group col-12 col-sm-8 d-flex flex-column col-md-3">
			<label for="">Tipo de envio</label>
			<select name="id_type" id="id_type" required class="form-control">
				<option value="">-- Tipo de envio--</option>
				<?php
					$row = $obj->getReferralsType();
					if(count($row)>0)
					{
						foreach($row as $row)
						{
							?>
							<option value="<?php echo $row["irt_id"]?>"><?php echo utf8_encode($row["irt_name"])?></option>
							<?php
						}
					}
				?>
			</select>
		</div>
		<div class="d-flex align-items-end col-12 col-sm-8 col-md-2">
			<input type="button" value="Buscar" class="btn btn-success mb-3 btn-block" onclick="search()"/></td>
		</div>
    </div>
  	<div class="card-body">
      <div class="table-responsive">
	  	<table class="table table-bordered" id="pedidos" width="100%" cellspacing="0">
		  	<thead>
			  	<tr class="bg-white">
				  	<th>ID</th>
					<th style="min-width: 100px">Fecha</th>
					<th>Track</th>
					<th style="min-width: 100px">Estado</th>
					<th>Tipo de movimiento</th>
					<th>Factura</th>
					<th style="min-width: 60px">Orden de Compra</th>
					<th>Cliente</th>
					<th>Bodega Origen</th>
					<th>Bodega Destino</th>
					<th>Usuario</th>
					<th>Tipo</th>
					<th>Acciones</th>
                </tr>
			  </thead>
			  <tfoot>
                <tr class="bg-white">
                  	<th>ID</th>
					<th style="min-width: 100px">Fecha</th>
					<th>Track</th>
					<th style="min-width: 100px">Estado</th>
					<th>Tipo de movimiento</th>
					<th>Factura</th>
					<th style="min-width: 60px">Orden de Compra</th>
					<th>Cliente</th>
					<th>Bodega Origen</th>
					<th>Bodega Destino</th>
					<th>Usuario</th>
					<th>Tipo</th>
					<th>Acciones</th>
                </tr>
			</tfoot>
            <tbody id="show_referrals">
				
            </tbody>
	  	</table>
      </div>
	</div>
	<div width="100%" id="loading" style="display:none">
		<p align="center"><img src="modules/inventory/view/loading.gif" style="position: absolute; height: auto; width: 120px; top:300px" /></p>
	</div>
</div>


<script>
$(document).ready(function() {
	initDataTable()
} );

function initDataTable(){
	$('#pedidos').DataTable({
		"processing": true,
		"serverSide": true,
		"searching": false,
		"ajax": {
			"url": "modules/inventory/view/lazyLoadReferral.php",
			"data": function ( d ) {
				return $.extend( {}, d, {
					"track": $('#track').val(),
					"id_type": $('#id_type').val(),
					"type": $('#type').val(),
				} );
			},
		},
		"aaSorting": [],
		language: {
			"sProcessing":     "Procesando...",
			"sLengthMenu":     "Mostrar _MENU_ registros",
			"sZeroRecords":    "No se encontraron resultados",
			"sEmptyTable":     "Ningun dato disponible en esta tabla",
			"sInfo":           "Mostrando registros del _START_ al _END_ de un total de _TOTAL_ registros",
			"sInfoEmpty":      "Mostrando registros del 0 al 0 de un total de 0 registros",
			"sInfoFiltered":   "(filtrado de un total de _MAX_ registros)",
			"sInfoPostFix":    "",
			"sSearch":         "Buscar:",
			"sUrl":            "",
			"sInfoThousands":  ",",
			"sLoadingRecords": "Cargando...",
			"oPaginate": {
				"sFirst":    "Primero",
				"sLast":     "Ultimo",
				"sNext":     "Siguiente",
				"sPrevious": "Anterior"
			},
			"oAria": {
				"sSortAscending":  ": Activar para ordenar la columna de manera ascendente",
				"sSortDescending": ": Activar para ordenar la columna de manera descendente"
			},
			"buttons": {
				"copy": "Copiar",
				"colvis": "Visibilidad"
			}
		}
	});
}

 function showInfo(id,site)
 {
	$.fancybox.open({
					href : site+'?id='+id,
					type : 'iframe',
					padding:5
				}); 
 }
 function showInfo2(id,site)
 {
	$.fancybox.open({
		href : site+'?id='+id,
		type : 'iframe',
		padding:2,
		'afterClose' : function(){
			parent.location.reload(true);
		}
	}); 
 }

 function search(){
	var table = $('#pedidos').DataTable();
	table.ajax.reload(); 
}
</script>
