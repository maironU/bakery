<?php
 include('modules/inventory/model/inventory.php');
 include('modules/products/model/products.php');
 include('usr/model/User.php');
 
 $obj = new inventory();
 $obj->connect();
 
 $usr = new User();
 $usr->connect();
 
 $products = new products();
 $products->connect();
 
 
 $msg=false;
 
 //Acciones
 
 ?>
<div class="widget3">
 <div class="widgetlegend">Informe Movimiento de productos </div>
 <?php
  if($msg)
  {
  ?>
   <div class="ui-widget">
	<div class="ui-state-highlight ui-corner-all" style="margin-top: 20px; padding: 0 .7em;">
		<p><span class="ui-icon ui-icon-info" style="float: left; margin-right: .3em;"></span>
		<strong>Felicitaciones!</strong> eliminado satisfactoriamente.</p>
	</div>
</div>
  <?php
  }
 ?>
<p style="width:100">
    <a href="javascript" onClick="PrintElem('invInf')" class="btn_normal" style="float:left; margin:5px;" target="_blank">Imprimir </a>
    <a href="<?php $_SERVER['PHP_SELF'];?>?p=modules/inventory/view/showInf.php" class="btn_normal" style="float:left; margin:5px;">Volver </a>
</p>
 <br /><br /><br />

<form action="" method="post">
<table width="100%" height="48" border="0">
 <tr>
  	<td><input type="text" name="start" id="start" class="datepicker" placeholder="Desde" required /></td>
  	<td><input type="text" name="end" id="end" class="datepicker2" placeholder="Hasta" required /></td>
 </tr>
 <tr>
   <td colspan="2">
     <select name="id_product" id="id_product" required>
       <option value="">--Producto--</option>
       <?php
       	$row = $products->getProducts();
       	if(count($row)>0)
       	{
       		foreach($row as $row)
       		{
       		?>
       		<option value="<?php echo $row['pro_id']?>"><?php echo utf8_encode($row['pro_name'])?></option>
       		<?php
       		}
       	}
       ?>
     </select></td>
   
 </tr>
 <tr>
 	<td><select name="who">
		<option value="">--Bodega Origen--</option>
		<?php
			$row = $obj->getWarehouse();
			if(count($row)>0)
			{
				foreach($row as $row)
				{
					?>
					<option value="<?php echo $row['iw_id']?>"><?php echo utf8_encode($row['iw_name'])?></option>
					<?php
				}
			}
		?>
	</select></td>
 	<td><select name="whd">
		<option value="">--Bodega Destino--</option>
		<?php
			$row = $obj->getWarehouse();
			if(count($row)>0)
			{
				foreach($row as $row)
				{
					?>
					<option value="<?php echo $row['iw_id']?>"><?php echo utf8_encode($row['iw_name'])?></option>
					<?php
				}
			}
		?>
	</select></td>
 </tr>
 <tr>
 <td colspan="2"><input type="submit" name="button" id="button" value="Buscar" class="btn_submit" /></td>
 </tr>
</table>
</form>
<br /><br /><br />
<?php
 if($_POST){
?>
<div id="invInf">
<table width="807" height="48" border="0">
  <tr>
    <th width="30">ID</th>
    <th width="30">Fecha</th>
    <th width="30">Tipo</th>
    <th width="142">Cantidad</th>
    <th width="142">Producto</th>
    <th width="142">Usuario</th>
    <th width="142">Bodega Origen</th>
    <th width="142">Bodega Destino</th>
    <th width="142">Descripcion</th>
  </tr>
<?php
 if($_POST){
  $row = $obj->getProductMov($obj->postVars('start'),$obj->postVars('end'), $obj->postVars('id_product'), $obj->postVars('who'), $obj->postVars('whd'));
  if(count($row)>0){
  foreach($row as $row){
   ?>
   <tr>
    <td><?php echo $row["im_id"]?></td>
    <td><?php echo $row["im_date"]?></td>
    <td><?php echo $row["im_type"]?></td>
    <td><?php echo $row["im_qty"]?></td>
    <td><?php echo $row["ip_name"]?></td>
    <td><?php 
	$usr->getUserById($row["u_id"]);
	echo $usr->name;
	?></td>
    <td><?php echo $row["iwo_name"]?></td>
    <td><?php echo $row["iwd_name"]?></td>
    <td><?php echo $row["im_desc"]?></td>
   </tr>
   <?php
   $total_qty+=$row["im_qty"];
  }
  ?>
  <tr>
   <td>&nbsp;</td>
   <td>&nbsp;</td>
   <td><strong>Total Cantidad:</strong></td>
   <td><?php echo $total_qty;?></td>
   <td>&nbsp;</td>
   <td>&nbsp;</td>
   <td>&nbsp;</td>
   <td>&nbsp;</td>
   <td>&nbsp;</td>
  </tr>
  <?php
  }
  else{
   ?>
    <tr>
     <td colspan="9">No hay movimientos</td>
    </tr>
   <?php
  }
 }
?> 
</table>
</div>
<?php
 }
?>
</div>
<script>
function PrintElem(elem)
{
    var mywindow = window.open('', 'PRINT', 'height=600,width=800');

    mywindow.document.write('<html><head><title>INFORME DE INVENTARIO</title>');
    mywindow.document.write('</head><body >');
    mywindow.document.write('<h1>' + document.title  + '</h1>');
    mywindow.document.write(document.getElementById(elem).innerHTML);
    mywindow.document.write('</body></html>');

    mywindow.document.close(); // necessary for IE >= 10
    mywindow.focus(); // necessary for IE >= 10*/

    mywindow.print();
    mywindow.close();

    return true;
}
</script>


