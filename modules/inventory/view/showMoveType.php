<?php
include('modules/inventory/model/inventory.php');
 
 $obj = new inventory();
 $obj->connect();
 
 if($obj->getVars('actionDel')==true){
	 $id=$obj->getVars('id');
  $obj->delMovType($id); 
 }
 
?>

<div class="widget3">
 <div class="widgetlegend">Tipo de movimientos</div>
<p style="width:100">
	<a href="<?php $_SERVER['PHP_SELF'];?>?p=modules/inventory/view/showSetup.php" class="btn_normal" style="float:left; margin:5px;" >Volver </a>
    <a href="<?php $_SERVER['PHP_SELF'];?>?p=modules/inventory/view/newMoveType.php" class="btn_normal" style="float:left; margin:5px;">Nuevo </a>
</p>
 
<table width="803" height="48" border="0">
  <tr>
    <th width="30">ID</th>
    <th width="142">Nombre</th>
    <th width="213">Movimiento</th>
    <th colspan="2">Acciones</th>
  </tr>
  <?php
   $row=$obj->getMovType();
   
   if(count($row)>0){
 	foreach($row as $row){
  ?>
  <tr>
    <td><?php echo $row["imt_id"];?></td>
    <td><?php echo $row["imt_name"];?></td>
    <td><?php echo $row["imt_action"];?></td>
    <td width="86"><a href="<?php $_SERVER['PHP_SELF'];?>?p=modules/inventory/view/editMoveType.php&id=<?php echo $row["imt_id"]?>" class="btn_normal">Editar</a></td>
    <td width="86"><a href="<?php $_SERVER['PHP_SELF'];?>?p=modules/inventory/view/showMoveType.php&id=<?php echo $row["imt_id"]?>&actionDel=true" class="btn_borrar">Eliminar</a></td>
  </tr>
  <?php
   }
  } 
  ?>
</table>

</div>
