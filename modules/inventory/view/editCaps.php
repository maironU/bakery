<?php
include('modules/inventory/model/inventory.php');
 
 $obj = new inventory();
 $obj->connect();
 
 $msg=false;
 
 if($_POST){
  	 
  $obj->editCapsInvProd();
  $msg=true;
 }
 
 $id=$obj->getVars('id');
 $row=$obj->getCapsInvProdById($id);
 foreach($row as $row){
  $name=$row["ip_name"];
  $desc=$row["ip_desc"];
  $price=$row["ip_price"];
  $stockmin=$row["ip_minstock"];
 }

?>
<div class="widget3">
 <div class="widgetlegend">Editar producto de inventario </div>
 <?php
  if($msg)
  {
  ?>
   <div class="ui-widget">
	<div class="ui-state-highlight ui-corner-all" style="margin-top: 20px; padding: 0 .7em;">
		<p><span class="ui-icon ui-icon-info" style="float: left; margin-right: .3em;"></span>
		<strong>Felicitaciones!</strong> Se ha sido creado satisfactoriamente.</p>
	</div>
</div>
  <?php
  }
 ?>
<p style="width:100">
	<a href="<?php $_SERVER['PHP_SELF'];?>?p=modules/inventory/view/showSetup.php" class="btn_normal" style="float:left; margin:5px;">Volver </a>
</p>
 
<form action="#" method="post" enctype="multipart/form-data" name="form1" id="form1">
<table width="803" height="48" border="0" style="float:left">
  <tr>
    <td><label>Nombre: </label><br />
      <input name="name" type="text" id="name" value="<?php echo $name?>" required />   </td>
  </tr>
  <tr>
    <td><label>Descripcion: </label><br />
      <textarea name="desc" cols="40" rows="5" id="desc"><?php echo $desc?></textarea></td>
  </tr>
  <tr>
    <td><label>Precio: </label><br />
      $<input name="price" type="text" id="price" value="<?php echo $price?>" />   </td>
  </tr>
  <tr>
    <td><label>Stock Min (Para alertas): </label><br />
      <input name="minstock" type="text" id="minstok" value="<?php echo $stockmin?>" required />   </td>
  </tr>
  <tr>
    <td>
    <input type="hidden" name="id" value="<?php echo $id?>" />
    <input type="submit" class="btn_submit" value="Guardar" /></td>
  </tr>
</table>
</form>


</div>
