<?php
include('modules/inventory/model/inventory.php');
 
 $obj = new inventory();
 $obj->connect();
 
 $msg=false;
 
 if($_POST){
	$id = $obj->postVars('id');
	$name = $obj->postVars('name');
	$action = $obj->postVars('action');
	$obj->editMovType($id, $name, $action);
	$msg=true;
 }
 
 $id = $obj->getVars('id');
 $row = $obj->getMovTypeById($id);
 foreach($row as $row)
 {
	 $name = $row["imt_name"];
	 $action = $row["imt_action"];
 }

?>
<div class="widget3">
 <div class="widgetlegend">Editar Tipo de Movimiento </div>
 <?php
  if($msg)
  {
  ?>
   <div class="ui-widget">
	<div class="ui-state-highlight ui-corner-all" style="margin-top: 20px; padding: 0 .7em;">
		<p><span class="ui-icon ui-icon-info" style="float: left; margin-right: .3em;"></span>
		<strong>Felicitaciones!</strong> Se ha guardado satisfactoriamente.</p>
	</div>
</div>
  <?php
  }
 ?>
<p style="width:100">
	<a href="<?php $_SERVER['PHP_SELF'];?>?p=modules/inventory/view/showMoveType.php" class="btn_normal" style="float:left; margin:5px;">Volver </a>
</p>
 
<form action="#" method="post" enctype="multipart/form-data" name="form1" id="form1">
<table width="803" height="48" border="0" style="float:left">
  <tr>
    <td><label>Nombre: </label><br />
      <input name="name" type="text" id="name" value="<?php echo $name?>" required />   </td>
  </tr>
  <tr>
    <td><label>Accion: </label><br />
	<?php
	 if($action == 'in')
	 {
		 ?>
			Ingreso - <input type="radio" name="action" value="in" required checked />
			Egreso - <input type="radio" name="action" value="out" required />
		 <?php
	 } 
	 elseif ($action == 'out')
	 {
		?>
		Ingreso - <input type="radio" name="action" value="in" required />
		Egreso - <input type="radio" name="action" value="out" required checked />
		<?php
	 }
	 else
	 {
		 ?>
			Ingreso - <input type="radio" name="action" value="in" required />
			Egreso - <input type="radio" name="action" value="out" required />
		 <?php
	 }
	?>
    
	</td>
  </tr>
  <tr>
    <td>
	<input type="hidden" name="id" value="<?php echo $id?>" />
	<input type="submit" class="btn_submit" value="Guardar" /></td>
  </tr>
</table>
</form>


</div>
