<?php
include('modules/inventory/model/inventory.php');
 
 $obj = new inventory();
 $obj->connect();
 
 if($obj->getVars('actionDel')==true){
	 $obj->delWarehouse(); 
 }
 
?>

<div class="widget3">
 <div class="widgetlegend">Bodegas</div>
<p style="width:100">
	<a href="<?php $_SERVER['PHP_SELF'];?>?p=modules/inventory/view/showSetup.php" class="btn_normal" style="float:left; margin:5px;" >Volver </a>
    <a href="<?php $_SERVER['PHP_SELF'];?>?p=modules/inventory/view/newWarehouse.php" class="btn_normal" style="float:left; margin:5px;">Nuevo </a>
</p>
 
<table width="803" height="48" border="0">
  <tr>
    <th width="30">ID</th>
	<th width="30">Imagen</th>
    <th width="142">Nombre</th>
    <th width="213">Responsable</th>
	<th width="213">Direccion</th>
	<th width="213">Telefono</th>
	<th width="213">Defecto</th>
    <th colspan="2">Acciones</th>
  </tr>
  <?php
   $row=$obj->getWarehouse();
   
   if(count($row)>0){
 	foreach($row as $row){
  ?>
  <tr>
    <td><?php echo $row["iw_id"];?></td>
	<td><img src="modules/inventory/whImages/<?php echo $row["iw_img"];?>" width="80" height="80" /></td>
    <td><?php echo $row["iw_name"];?></td>
    <td><?php echo $row["iw_responsble"];?></td>
	<td><?php echo $row["iw_addr"];?></td>
	<td><?php echo $row["iw_phone"];?></td>
	<td><?php 
		if($row["iw_isdefault"]=='1')
		{
			echo "Si";
		}
		else
		{
			echo "No";
		}
		
	?></td>
    <td width="86"><a href="<?php $_SERVER['PHP_SELF'];?>?p=modules/inventory/view/editWarehouse.php&id=<?php echo $row["iw_id"]?>" class="btn_normal">Editar</a></td>
    <td width="86"><a href="<?php $_SERVER['PHP_SELF'];?>?p=modules/inventory/view/showWarehouse.php&id=<?php echo $row["iw_id"]?>&actionDel=true" class="btn_borrar">Eliminar</a></td>
  </tr>
  <?php
   }
  } 
  ?>
</table>

</div>
