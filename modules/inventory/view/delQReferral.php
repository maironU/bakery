<?php
  session_start();
  include('../../../core/config.php');
  include('../../../core/ge.php');
  include('../../products/model/products.php');
  include('../model/inventory.php');
  
  $obj=new inventory();
  $obj->connect();
 
  $product = new products();
  $product->connect();
  
  if($_POST)
  {
  	$id = $obj->postVars('id');
  	$row = $obj->getQuickReferralDetail($id);
	foreach($row as $row)
	{
		$obj->registerMov($row['id_product'],$row['iqrd_qty'],$_SESSION['user_id'],'',$row['id_move'],$row['id_product'],"Movimiento entre bodegas Remision Rapida No.".$row['iqr_id'],$row['iqrd_who'],$row['iqrd_whd'],$row['iqr_id'], 'yes');
		
	}
	$obj->editQuickReferralActive($id);
	$msg=true;
  }
  
  $id = $obj->getVars('id');
  
  
?>
<link href="../../../css/scsStyle.css" rel="stylesheet" type="text/css" />
<div class="widget3">
 <div class="widgetlegend">Anular Remisi&oacute;n</div>
 <?php
  if($msg)
  {
  ?>
   <div class="ui-widget">
	<div class="ui-state-highlight ui-corner-all" style="margin-top: 20px; padding: 0 .7em;">
		<p><span class="ui-icon ui-icon-info" style="float: left; margin-right: .3em;"></span>
		<strong>Felicitaciones!</strong> ha anulado satisfactoriamente.</p>
	</div>
</div>
  <?php
  }
 ?>
 <form action="" method="post" enctype="multipart/form-data" name="formadd"><table width="200" border="0">
  <tr>
    <td>Esta apunto de anular una remisi&oacute;n combinada. Con esta anulacioacute;n los movimientos serán invertidos, segun como los halla detallado en el momento de hacer esta remisi&oacute;n. <h2>Esta seguro de anular la remisi&oacute;n?</h2> </td>
  </tr>
  <tr>
  <td>
    <input value="<?php echo $id?>" type="hidden" name="id" />
    <input value="Anular" type="submit" class="btn_submit" /></td>
  </tr>
</table>
 </form> 
</div>
<script>
function displayForm()
{
	document.getElementById('frmItem').style.display = 'block';
}
</script>