<?php
include('modules/inventory/model/inventory.php');
include('modules/crm/model/customerModel.php');
include('usr/model/User.php');
 
 $obj = new inventory();
 $obj->connect();
 
 $customer = new customerModelGe();
 $customer->connect();
 
 $user = new User();
 $user->connect();
 
 if($_POST)
 {
 	$desc = $obj->postVars('desc');
 	$id_qreferral = $obj->newQuickReferral($desc, $_SESSION['user_id']);
 	$row = $obj->getQuickReferralDetailTemp($_SESSION['user_id']);
 	if(count($row)>0)
 	{
 		foreach($row as $row)
 		{
 			$obj->newQuickReferralDetail($id_qreferral, $row['iqrdt_qty'], $row['id_move'], $row['iqrdt_typemov'], $row['id_product'], $row['iqrdt_who'], $row['iqrdt_whd'], $row['iqrdt_whoname'], $row['iqrdt_whdname'], $row['iqrdt_name']);
 		}
 		$obj->flushQuickReferralDetailTemp();
 	}
 	
 	//Se registran los movimientos
 	$row = $obj->getQuickReferralDetail($id_qreferral);
 	if(count($row)>0)
 	{
 		foreach($row as $row)
 		{
 			$obj->registerMov($row['id_product'],$row['iqrd_qty'],$_SESSION['user_id'],'',$row['id_move'],$row['id_product'],"Movimiento entre bodegas Remision Rapida No.".$id_qreferral,$row['iqrd_who'],$row['iqrd_whd'],$id_qreferral, 'no');
 		}
 	}
 }
 
 
 if($obj->getVars('actionDel')==true){
	$id_item = $obj->getVars('id_item');
	$obj->delQuickReferralDetailTemp($id_item); 
 }
 
?>
<!-- Add mousewheel plugin (this is optional) -->
<script type="text/javascript" src="modules/inventory/js/jquery.mousewheel-3.0.6.pack.js"></script>
<!-- Add fancyBox main JS and CSS files -->
<script type="text/javascript" src="modules/inventory/js/jquery.fancybox.js?v=2.1.4"></script>
<link rel="stylesheet" type="text/css" href="modules/inventory/css/jquery.fancybox.css?v=2.1.4" media="screen" />
<link rel="stylesheet" type="text/css" href="modules/inventory/css/stylePos.css" />
<div class="widget3">
 <div class="widgetlegend">Remisiones</div>
<p style="width:100">
    <a href="<?php $_SERVER['PHP_SELF'];?>?p=modules/inventory/view/showQReferral.php" class="btn_normal" style="float:left; margin:5px;">Volver </a>
</p>
<form action="" method="post">
<table width="100%" height="48" border="0">
  <tr>
        <td><a href="javascript:;" class="btn_1" onClick="showInfo2(0,'modules/inventory/view/newQReferralItem.php')">Nuevo Item</a></td>
  </tr>
  <tr>
        <td>
        	<table width="100%">
        		<tr>
        			<th>ID</th>
        			<th>Producto</th>
        			<th>Cantidad</th>
        			<th>Movimiento</th>
        			<th>Bodega Origen</th>
        			<th>Bodega Destino</th>
        			<th>&nbsp;</th>
        		</tr>
        		<?php
        			$row = $obj->getQuickReferralDetailTemp($_SESSION['user_id']);
        			if(count($row)>0)
        			{
        				foreach($row as $row)
        				{
        				?>
        				<tr>
		        			<td><?php echo $row['iqrdt_id']?></td>
		        			<td><?php echo $row['iqrdt_name']?></td>
		        			<td><?php echo $row['iqrdt_qty']?></td>
		        			<td><?php echo $row['iqrdt_typemov']?></td>
		        			<td><?php echo $row['iqrdt_whoname']?></td>
		        			<td><?php echo $row['iqrdt_whdname']?></td>
		        			<td><a href="<?php $_SERVER['PHP_SELF'];?>?p=modules/inventory/view/newQReferral.php&id_item=<?php echo $row['iqrdt_id']?>&actionDel=true" class="btn_2">Eliminar</a></td>
		        		</tr>
        				<?php
        				}
        			}
        		?>
        	</table>
        </td>
  </tr>
  <tr>
  	<td><textarea name="desc" cols="40" rows="6" placeholder="Describa el motivo del movimiento" required></textarea></td>
  </tr>
  <tr>
  	<td><input type="submit" value="Guardar" class="btn_submit" /></td>
  </tr>
</table>
</form>
</div>
<script>
 function showInfo(id,site)
 {
	$.fancybox.open({
					href : site+'?id='+id,
					type : 'iframe',
					padding:5
				}); 
 }
 function showInfo2(id,site)
 {
	$.fancybox.open({
					href : site+'?id='+id,
					type : 'iframe',
					padding:2,
					'afterClose' : function(){
					 	parent.location.reload(true);
					}
				}); 
 }
 </script>
