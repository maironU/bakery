<?php
include('modules/inventory/model/inventory.php');
include('modules/crm/model/customerModel.php');
include('usr/model/User.php');
 
 $obj = new inventory();
 $obj->connect();
 
 $customer = new customerModelGe();
 $customer->connect();
 
 $user = new User();
 $user->connect();
 
 if($obj->getVars('actionDel')==true){
	 
 }
 
?>
<!-- Add mousewheel plugin (this is optional) -->
<script type="text/javascript" src="modules/inventory/js/jquery.mousewheel-3.0.6.pack.js"></script>
<!-- Add fancyBox main JS and CSS files -->
<script type="text/javascript" src="modules/inventory/js/jquery.fancybox.js?v=2.1.4"></script>
<link rel="stylesheet" type="text/css" href="modules/inventory/css/jquery.fancybox.css?v=2.1.4" media="screen" />
<link rel="stylesheet" type="text/css" href="modules/inventory/css/stylePos.css" />
<div class="widget3">
 <div class="widgetlegend">Remisiones</div>
<p style="width:100">
    <a href="<?php $_SERVER['PHP_SELF'];?>?p=modules/inventory/view/newReferral.php" class="btn_normal" style="float:left; margin:5px;">Nuevo </a>
</p>
<br><br><br>
    <form action="" method="post">
        <table>
            <tr>
                <td><input type="text" name="track" placeholder="Numero de Remision" autofocus /></td>
                <td><input type="submit" value="Buscar" class="btn_submit" /></td>
            </tr>
        </table>
    </form>
<br><br><br>
 
<table width="100%" height="48" border="0">
  <tr>
    <th width="30">ID</th>
	<th width="30">Fecha</th>
	<th width="30">Track</th>
    <th width="142">Estado</th>
    <th width="213">Tipo de movimiento</th>
	<th width="213">Factura</th>
	<th width="213">Orden de Compra</th>
	<th width="213">Cliente</th>
	<th width="213">Bodega Origen</th>
	<th width="213">Bodega Destino</th>
	<th width="213">Usuario</th>
	<th width="213">Tipo</th>
    <th colspan="2">Acciones</th>
  </tr>
  <?php
  if($_POST)
  {
      $track = $obj->postVars('track');
      $row=$obj->etUserReferralsByTrack($_SESSION['user_id'], $track);
  }
  else
  {
      $row=$obj->getUserReferrals($_SESSION['user_id']);
  }
   
   if(count($row)>0){
 	foreach($row as $row){
  ?>
  <tr>
    <td><?php echo $row["ir_id"];?></td>
	<td><?php echo $row["ir_date"];?></td>
    <td><?php echo $row["ir_track"];?></td>
    <td><?php 
    $row1 = $obj->getReferralStateById($row["ir_state"]);
    if(count($row1)>0)
		{
			foreach($row1 as $row1)
			{
				?>
				<div style="padding: 5px; min-width: 100px; height: 30px; border-radius; 5px; background-color: #<?php echo $row1['irs_color']?>; display: block"><?php echo utf8_encode($row1['irs_name'])?></div>
				<?php
			}
		}
    ?></td>
	<td><?php 
		$row1 = $obj->getMovTypeById($row["imt_id"]);
		if(count($row1)>0)
		{
			foreach($row1 as $row1)
			{
				echo $row1["imt_name"];
			}
		}
	?></td>
	<td><?php echo $row["id_invoice"];?></td>
	<td><?php echo $row["id_po"];?></td>
	<td><?php 
		$row1 = $customer->getCustomerById2($row["id_customer"]);
		if(count($row1)>0)
		{
			foreach($row1 as $row1)
			{
				echo $row1["c_nom"];
			}
		}
	?></td>
	<td><?php echo $row["ir_who"];?></td>
	<td><?php echo $row["ir_whd"];?></td>
	<td><?php 
		$row1 = $user->getUserById2($row["u_id"]);
		if(count($row1)>0)
		{
			foreach($row1 as $row1)
			{
				echo $row1["u_nom"];
			}
		}
	?></td>
	<td><?php 
		$row1 = $obj->getReferralsTypeById($row["irt_id"]);
		if(count($row1)>0)
		{
			foreach($row1 as $row1)
			{
				echo $row1["irt_name"];
			}
		}
	?></td>
    <td width="86"><a href="javascript:;" class="btn_1" onClick="showInfo(<?php echo $row['ir_id']?>,'modules/inventory/view/showUserPrintReferral.php')">Ver</a></td>
    <td width="86">
    <?php
    $row1 = $obj->getReferralStateById($row["ir_state"]);
    if(count($row1)>0)
		{
			foreach($row1 as $row1)
			{
				$run = $row1["irs_runmov"];
			}
		}
		if($run != 1)
		{
		?>
		<a class="btn_3" href="<?php $_SERVER['PHP_SELF'];?>?p=modules/inventory/view/editReferral.php&id=<?php echo $row["ir_id"]?>">Editar</a>
		<?php
		}
    ?>
    </td>
  </tr>
  <?php
   }
  } 
  ?>
</table>

</div>
<script>
 function showInfo(id,site)
 {
	$.fancybox.open({
					href : site+'?id='+id,
					type : 'iframe',
					padding:5
				}); 
 }
 function showInfo2(id,site)
 {
	$.fancybox.open({
					href : site+'?id='+id,
					type : 'iframe',
					padding:2,
					'afterClose' : function(){
					 	parent.location.reload(true);
					}
				}); 
 }
 </script>
