<?php
include('modules/inventory/model/inventory.php');
 
 $obj = new inventory();
 $obj->connect();
 
 
 $msg=false;
 
 //Acciones
 if($obj->getVars('ActionDel')==true){
  $obj->delCapsInvProd();
  $msg=true;
 }
 
 ?>
 <div class="widget3">
 <div class="widgetlegend">Configuracion Inventario </div>
 <?php
  if($msg)
  {
  ?>
   <div class="ui-widget">
	<div class="ui-state-highlight ui-corner-all" style="margin-top: 20px; padding: 0 .7em;">
		<p><span class="ui-icon ui-icon-info" style="float: left; margin-right: .3em;"></span>
		<strong>Felicitaciones!</strong> eliminado satisfactoriamente.</p>
	</div>
</div>
  <?php
  }
 ?>
<p style="width:100">
	<a href="<?php $_SERVER['PHP_SELF'];?>?p=modules/products/view/index.php" class="btn_normal" style="float:left; margin:5px;">Adm Productos </a>
    	<!--<a href="<?php $_SERVER['PHP_SELF'];?>?p=modules/inventory/view/showProducts.php" class="btn_normal" style="float:left; margin:5px;">Productos </a>-->
    	<a href="<?php $_SERVER['PHP_SELF'];?>?p=modules/inventory/view/showEmails.php" class="btn_normal" style="float:left; margin:5px;">Emails </a>
    	<a href="<?php $_SERVER['PHP_SELF'];?>?p=modules/inventory/view/showCapsInvConfig.php" class="btn_normal" style="float:left; margin:5px;">Setup </a>
    	<a href="<?php $_SERVER['PHP_SELF'];?>?p=modules/inventory/view/showMoveType.php" class="btn_normal" style="float:left; margin:5px;">Movimientos </a>
    	<a href="<?php $_SERVER['PHP_SELF'];?>?p=modules/inventory/view/showWarehouse.php" class="btn_normal" style="float:left; margin:5px;">Bodegas </a>
    	<a href="<?php $_SERVER['PHP_SELF'];?>?p=modules/inventory/view/showReferralStates.php" class="btn_normal" style="float:left; margin:5px;">Estados </a>
    	<a href="<?php $_SERVER['PHP_SELF'];?>?p=modules/inventory/view/showReferralType.php" class="btn_normal" style="float:left; margin:5px;">Tipos </a>
</p>
 
<table width="803" height="48" border="0">
  <tr>
    <th width="30">ID</th>
    <th width="142">Nombre</th>
    <th width="142">Descripcion</th>
    <th width="142">Precio</th>
    <th width="142">Stock Minimo</th>
    <th width="142">Stock</th>
    <th width="142">Tienda</th>
    <!--<th colspan="2">Acciones</th>-->
  </tr>
  <?php
   $row=$obj->getCapsInvProd();
   
   if(count($row)>0){
 	foreach($row as $row){
  ?>
  <tr>
    <td><?php echo $row["ip_id"];?></td>
    <td><?php echo $row["ip_name"];?></td>
    <td><?php echo $row["ip_desc"];?></td>
    <td>$<?php echo number_format($row["ip_price"]);?></td>
    <td><?php echo $row["ip_minstock"];?></td>
    <td><?php echo $row["ip_stock"];?></td>
   <td><?php echo $row["id_wh"];?></td>
   
   
   <!--<td width="86"><a href="<?php $_SERVER['PHP_SELF'];?>?p=modules/inventory/view/editCaps.php&id=<?php echo $row["ip_id"]?>" class="btn_normal">Editar</a></td>
    <td width="81"><a href="<?php $_SERVER['PHP_SELF'];?>?p=modules/inventory/view/showSetup.php&id=<?php echo $row["ip_id"]?>&ActionDel=true" class="btn_borrar">Eliminar</a></td>-->
  </tr>
  <?php
   }
  } 
  ?>
</table>

</div>
