<?php
    include('../view/include_self.php');
    include('../../products/model/products.php');
    
    $products = new products();
    $products->connect();

    $method = $_GET['method'];

    if(isset($method)){
        switch($method){
            case "saveIngress":
                $id_product= $obj->postVars('id_product');
                $qty= $obj->postVars('qty');
                $type= $obj->postVars('type');
                $desc= $obj->postVars('desc');
                $who= $obj->postVars('who');
                $whd= $obj->postVars('whd');
                $container= $obj->postVars('container');
                $user_id= $obj->postVars('user_id');

                $row=$products->getProductsById($id_product);

                foreach($row as $row){
                    $product_qtycontainer=$row["pro_qtycontainer"];
                }

                if($container == 1){
                      $qty = $product_qtycontainer * $qty;
                }

                $id_ipo = $obj->getCapsInvProdByProductWh($id_product, $who);
                $id_ipd = $obj->getCapsInvProdByProductWh($id_product, $whd);

                if($who == $whd){
                    $obj->registerMov($id_ipo,$qty,$user_id,$id_account,$type,$id_product,$desc,$who,$whd,$referral);    
                }
                else{
                    $obj->registerMov($id_ipo,$qty,$user_id,$id_account,2,$id_product,$desc,$who,$whd,$referral);
                    $obj->registerMov($id_ipd,$qty,$user_id,$id_account,$type,$id_product,$desc,$who,$whd,$referral);
                }

                echo json_encode(["success" => true, "message" => "Ha sido creado satisfactoriamente"]);
                break;
            
            case "savePedido":
                $desc=$obj->postVars('desc');
                $id_move=$obj->postVars('type');
                $who=$obj->postVars('who');
                $whd=$obj->postVars('whd');
                $state=$obj->postVars('state');
                $id_type=$obj->postVars('id_type');
                $user_id= $obj->postVars('user_id');
                $resend= $obj->postVars('resend');
                
                if(count($obj->getReferralDetailTemp($user_id)) > 0){
                    $row = $obj->getReferralStateById($state);
                    if(count($row)>0)
                    {
                        foreach($row as $row)
                        {
                            $run = $row["irs_runmov"];
                        }
                    }
                    
                    $row = $obj->getWarehouseById($who);
                    if(count($row)>0)
                    {
                        foreach($row as $row)
                        {
                            $whnameo = $row["iw_name"];
                        }
                    }
                    
                    $row = $obj->getWarehouseById($whd);
                    if(count($row)>0)
                    {
                        foreach($row as $row)
                        {
                            $whnamed = $row["iw_name"];
                        }
                    }
                    
                    $id_referral = $obj->newReferral($desc, $state, $user_id, $id_move, $id_invoice, $id_po, $id_customer, $who, $whd, $whnameo, $whnamed,$id_type);
                    
                    $row = $obj->getReferralDetailTemp($user_id);
                    if(count($row)>0)
                    {
                        foreach($row as $row)
                        {
                            $obj-> newReferralDetail($id_referral, $row['irdt_productname'], $row['id_product'], $row["qty"], $row['irdt_price'], $row["qty2"], $row["qty3"]);//Detalle remision
                            $t += $row['irdt_price'] * $row['qty'];
                            if($run == 1)
                            {
                                $id_ipo = $obj->getCapsInvProdByProductWh($row['id_product'], $who); 
                                $id_ipd = $obj->getCapsInvProdByProductWh($row['id_product'], $whd);
                                
                                if($who == $whd)
                                {
                                    $obj->registerMov($id_ipo,$row["qty"],$user_id,$id_account,$id_move,$row['id_product'],$desc,$who,$whd,$id_referral);    
                                }
                                else
                                {
                                    $obj->registerMov($id_ipo,$row["qty"],$user_id,$id_account,2,$row['id_product'],$desc,$who,$whd,$id_referral);
                                    $obj->registerMov($id_ipd,$row["qty"],$user_id,$id_account,$id_move,$row['id_product'],$desc,$who,$whd,$id_referral);
                                }
                            }
                        }
                    }
    
                    $obj->editReferralTotal($id_referral, $t);
                    //if($resend == "false" || $resend == false){
                    $obj->flushReferralDetailTemp($user_id);
                    //}
    
                    echo json_encode(["success" => true, "message" => "Ha sido creado satisfactoriamente"]);
                }else{
                    echo json_encode(["success" => false, "message" => "No hay productos agregados"]);
                }
                break;
        }
    }
?>