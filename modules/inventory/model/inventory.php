<?php
class inventory extends GeCore{

	public function newCapsInvProd(){
		$name=$this->postVars('name');
		$desc=$this->postVars('desc');
		$price=$this->postVars('price');
		$minstock=$this->postVars('minstock');
		
		$sql="insert into `inv_prod` (`ip_name`, `ip_desc`, `ip_price`, `ip_stock`, `ip_minstock`) values ('$name','$desc','$price','0','$minstock')";
		$this->Execute($sql);
		
		$id=$this->getLastID();
		
	}
	
	public function newCapsInvProd2($name, $desc, $price, $minstock){
			
		$sql="insert into `inv_prod` (`ip_name`, `ip_desc`, `ip_price`, `ip_stock`, `ip_minstock`) values ('$name','$desc','$price','0','$minstock')";
		$this->Execute($sql);
		
		$id=$this->getLastID();
		return $id;
		
	}
	
	public function newCapsInvProd3($name, $desc, $price, $minstock, $id_product, $id_wh){
			
		$sql="insert into `inv_prod` (`ip_name`, `ip_desc`, `ip_price`, `ip_stock`, `ip_minstock`, `id_product`, `id_wh`) values ('$name','$desc','$price','0','$minstock','$id_product', '$id_wh')";
		$this->Execute($sql);
		
		$id=$this->getLastID();
		return $id;
		
	}

	
	public function editCapsInvProd(){
		$id=$this->postVars('id');
		$name=$this->postVars('name');
		$desc=$this->postVars('desc');
		$price=$this->postVars('price');
		$minstock=$this->postVars('minstock');
		
		$sql="update `inv_prod` set `ip_name`='$name', `ip_desc`='$desc', `ip_price`='$price', `ip_minstock`='$minstock' where `ip_id`='$id'";
		$this->Execute($sql);
		
	}
	
	public function editCapsInvProd2($id, $name, $desc, $price, $minstock){
			
		$sql="update `inv_prod` set `ip_name`='$name', `ip_desc`='$desc', `ip_price`='$price', `ip_minstock`='$minstock' where `ip_id`='$id'";
		$this->Execute($sql);
		
	}
	
	public function editCapsInvProd3($id, $name, $desc, $price, $minstock){
			
		$sql="update `inv_prod` set `ip_name`='$name', `ip_desc`='$desc', `ip_price`='$price', `ip_minstock`='$minstock' where `id_product`='$id'";
		$this->Execute($sql);
		
	}
	
	public function editCapsInvProdStock($id,$stock,$id_product=NULL,$type){
	 $row=$this->getCapsInvProdById($id);
	 foreach($row as $row){
	  $actual_stock=$row["ip_stock"];
	  $min_stock=$row["ip_minstock"];
	 }
	 if($type=='in'){
		  $new_stock=$actual_stock+$stock;
	 }
	  if($type=='out'){
		  $new_stock=$actual_stock-$stock;
		  if($new_stock<=$min_stock){
			  //Enviar alarma
			  //$this->sendAlert($id);
		  }
	 }
	 
	 $sql="update `inv_prod` set  `ip_stock`='$new_stock' where `ip_id`='$id'";
	 $this->Execute($sql);
	 
	}
	
	public function delCapsInvProd(){
		$id=$this->getVars('id');
		
		$sql="delete from `inv_prod` where ip_id='$id'";
		$this->Execute($sql);
	}
	
	public function delCapsInvProd2($id_product){
		
		$sql="delete from `inv_prod` where id_product='$id_product'";
		$this->Execute($sql);
	}
	
	public function getCapsInvProd(){
		$sql="select * from `inv_prod` order by ip_name asc";
	 	return $this->ExecuteS($sql);
	}
	
	public function getCapsInvProdById($id){
		$sql="select * from `inv_prod` where ip_id='$id'";
	 	return $this->ExecuteS($sql);
	}
	
	public function getCapsInvProdByProduct($id){
		$sql="select * from `inv_prod` where id_product='$id'";
	 	return $this->ExecuteS($sql);
	}
	
	public function getCapsInvProdByProductWh($id_product, $id_wh){
		$sql="select * from `inv_prod` where id_product='$id_product' and id_wh='$id_wh'";
	 	$row = $this->ExecuteS($sql);
	 	if(count($row)>0)
	 	{
	 	    foreach($row as $row)
	 	    {
	 	        return $row['ip_id'];
	 	    }
	 	}
	}
	
	public function getCapsInvProdByWarehouse($id){
		$sql="select * from `inv_prod` where id_wh='$id'";
	 	return $this->ExecuteS($sql);
	}
	
	public function getNumProducts(){
		$sql="select * from `inv_prod`";
	 	$this->res=$this->Execute($sql);
		return $this->getNumRows();
	}
	
	public function getCapsInvProdMov($id,$type,$start='',$end='',$warehouse){
	 if($warehouse == '')
	 {
	 	$warehouse = $this->getDefaultWarehouse();
	 }
	 if($start=='' && $end==''){
	 	$sql="select * from `inv_mov` where ip_id='$id' and im_type='$type' and (iwo_id='$warehouse' or iwd_id='$warehouse')";
	 }else{
		$start.=" 00:00:00";
		$end.=" 23:59:59"; 
	    $sql="select * from `inv_mov` where ip_id='$id' and im_type='$type' and im_date between '$start' and '$end' and (iwo_id='$warehouse' or iwd_id='$warehouse')";
	 }	
	 
	 $row=$this->res=$this->ExecuteS($sql);
	 $total=0;
	 if(count($row)>0){
	  foreach($row as $row){
	   $total+=$row["im_qty"];
	  }
	  
	 }
	 
	 return $total;
	 
	}
	
	public function checkCapsInvProdStock($id){
	 $row=$this->getCapsInvProdById($id);
	 foreach($row as $row){
	  $stock=$row["ip_stock"];
	 }
	 
	 if($stock>0){
	  return true;
	 }else{
	  return false;
	 }
	 
	}
	
	public function useCapsInventory($id_product){
	 $sql="select * from products where pro_id='$id_product'";
	 $row=$this->ExecuteS($sql);
	 foreach($row as $row){
	  $capsinv_id=$row["inv_id"];
	 }
	 
	 if($capsinv_id!=0){
	  return true;
	 }else{
	  return false;
	 }
	}
	
	//Mov Methods
	public function registerMov($id_capsinvprod=NULL,$qty=NULL,$id_user=NULL,$id_account=NULL,$type=NULL,$id_product=NULL,$desc=NULL,$who=NULL,$whd=NULL,$referral=NULL, $anulation = 'no')
	{
		
		$today=date('Y-m-d H:i:s');
		
		$row=$this->getCapsInvProdById($id_capsinvprod);
		foreach($row as $row){
		 $cip_name=$row["ip_name"];
		}
		
		$row=$this->getWarehouseById($who);
		foreach($row as $row){
		 $who_name=$row["iw_name"];
		}
		
		$row=$this->getWarehouseById($whd);
		foreach($row as $row){
		 $whd_name=$row["iw_name"];
		}
		
		$row = $this->getMovTypeById($type);
		foreach($row as $row){
		 $act=$row["imt_action"];
		}
		
		$sql="insert into `inv_mov` (`im_date`, `imt_id`, `im_qty`, `id_account`, `u_id`, `ip_id`, `id_product`, `im_desc`, `ip_name`, `product_name`, `iwo_id`, `iwd_id`, `ir_id`,`iwo_name`,`iwd_name`,`im_type`) 
		values ('$today','$type','$qty','$id_account','$id_user','$id_capsinvprod','$id_product','$desc','$cip_name','$product_name','$who','$whd','$referral','$who_name', '$whd_name','$act')";
		$this->Execute($sql);
		
		if($anulation == 'yes')
		{
			switch($act)
			{
				case 'in': $act = 'out';
				break;
				
				case 'out': $act = 'in';
				break;
			
			}
		}
		
		$this->editCapsInvProdStock($id_capsinvprod,$qty,$id_product,$act);
	
	}
	
	public function getMovs($id_user='',$start,$end,$type='',$who='',$whd=''){
	 
	 $start.=" 00:00:00";
	 $end.=" 23:59:59";
	 
	 
	 $sql="select * from `inv_mov` where im_date between '$start' and '$end'";
	 if($type != '')
	 {
	 	$sql .= " and imt_id='$type'";
	 }
	 if($id_user != '')
	 {
	 	$sql .= " and u_id='$id_user'";
	 }
	 if($who != '')
	 {
	 	$sql .= " and iwo_id='$who'";
	 }
	 if($whd != '')
	 {
	 	$sql .= " and iwd_id='$whd'";
	 }
	 
	 //$sql .= " LIMIT $start,".$this->tam_page;
	 
	 //echo $sql;
	 return $this->ExecuteS($sql);
	 
	}
	
	public function getProductMov($start, $end, $id_product, $who, $whd)
	{
	    $start .= " 00:00:00";
	    $start .= " 23:59:59";
	    
	    $sql = "SELECT * FROM inv_mov WHERE im_date BETWEEN '$start' AND '$end' AND id_product='$id_product'";
	    
	    if($who != '')
	    {
	        $sql .= " AND iwo_id='$who'";
	    }
	    
	    if($whd != '')
	    {
	        $sql .= " AND iwd_id='$whd'";
	    }
	    return $this->ExecuteS($sql);
	}
	
	//Other Methods
	public function editProduct($id_product,$id_capsinv,$qty){
	 $sql="update products set inv_qty='$qty', inv_id='$id_capsinv' where pro_id='$id_product'";
	 $this->Execute($sql);
	}
	
	//Method email config
	public function newEmail(){
		$name=$this->postVars('name');
		$email=$this->postVars('email');
		
		$sql="insert into `inv_emails` (`ie_name`, `ie_mails`) values ('$name','$email')";
		$this->Execute($sql);
	}
	
	public function editEmail(){
		$id=$this->postVars('id');
		$name=$this->postVars('name');
		$email=$this->postVars('email');
		
		$sql="update `inv_emails` set `ie_name`='$name', `ie_mails`='$email' where ie_id='$id'";
		$this->Execute($sql);
	}
	
	public function delEmail(){
		$id=$this->getVars('id');
		
		$sql="delete from `inv_emails` where ie_id='$id'";
		$this->Execute($sql);
	}
	
	public function getEmails(){
	 $sql="select * from `inv_emails`";
	 return $this->ExecuteS($sql);	
	}
	
	public function getEmailsToSend(){
	 $sql="select * from `inv_emails`";
	 $row=$this->ExecuteS($sql);
	 foreach($row as $row){
	 	
		$emails.=$row["ie_mails"].",";
	 
	 }
	 
	 return $emails;	
	}
	
	public function getEmailById($id){
	 $sql="select * from `inv_emails` where ie_id='$id'";
	 return $this->ExecuteS($sql);		
	}
	
	public function sendAlert($id_capsinv){
		
		$row=$this->getCapsInvProdById($id_capsinv);
		foreach($row as $row){
		 $caps_name=$row["ip_name"];
		 $caps_stockmin=$row["ip_minstock"];
		 $caps_stock=$row["ip_stock"]-1;
		 $id_wh=$row["id_wh"];
		}
		
		$row=$this->getWarehouseById($id_wh);
		foreach($row as $row){
		 $wh_name=$row["iw_name"];
		}
	
	$remitente=$this->getConfigValue(1);
	
	ini_set('SMTP',$this->getConfig('EMAIL_HOST'));
  	$headers="from:".$remitente."\r\n";
  	$headers .= "MIME-Version: 1.0\r\n"; 
  	$headers .= "Content-type: text/html; charset=UTF-8\r\n"; 
	
	try{
	$f=fopen('modules/inventory/tpls/email.html','r');
	
	if(!empty($f)){
		$m = fread($f,filesize('modules/inventory/tpls/email.html'));
	}else{
		$f=fopen('../tpls/email.html','r');
		$m = fread($f,filesize('../tpls/email.html'));
	}
	}catch(Exception $e){}
	
	
	
	$m=str_replace('[web]',$this->customerweb,$m);
	$m=str_replace('[name]',$caps_name,$m);
	$m=str_replace('[stockmin]',$caps_stockmin,$m);
	$m=str_replace('[stock]',$caps_stock,$m);
	$m=str_replace('[wh]',$wh_name,$m);
	
	
	mail($this->getEmailsToSend(),"ALERTA DE INVENTARIO ".$this->customerweb,$m,$headers);
   
		
	}
	
	//Config
	public function editConfig(){
	 $id=$this->postVars('id');
	 $value=$this->postVars('value');
	 
	 $sql="update inv_config set ic_value='$value' where ic_id='$id'";
	 $this->Execute($sql);
	 	
	}
	public function editConfig2($id, $value){
	 	 
	 $sql="update inv_config set ic_value='$value' where ic_id='$id'";
	 $this->Execute($sql);
	 	
	}
	
	public function getConfigById($id){
	 $sql="select * from inv_config where ic_id='$id'";
	 return $this->ExecuteS($sql);
	}
	
	public function getConfig(){
	 $sql="select * from inv_config";
	 return $this->ExecuteS($sql);
	}
	
	public function getConfigValue($id){
	 $sql="select * from inv_config where ic_id='$id'";
	 $row=$this->ExecuteS($sql);
	 if(count($row)>0){
	  foreach($row as $row){
		$value=$row["ic_value"];  
	   }
	 }
	 
	 return $value;
	}
	
	public function updateConfigValue($id)
	{
		$value = $this->getConfigValue($id);
		$value++;
		$this->editConfig2($id, $value);		
	}
	
	//Mov Type Methods
	public function newMovType($name, $action)
	{
		$sql="INSERT INTO `inv_movtype` (`imt_name`, `imt_action`) VALUES ('$name', '$action')";
		$this->Execute($sql);
	}
	
	public function editMovType($id, $name, $action)
	{
		$sql="UPDATE `inv_movtype` SET `imt_name`='$name', `imt_action`='$action' WHERE imt_id='$id'";
		$this->Execute($sql);
	}
	
	public function delMovType($id)
	{
		$sql="DELETE FROM `inv_movtype` WHERE imt_id='$id'";
		$this->Execute($sql);
	}
	
	public function getMovType()
	{
		$sql="SELECT * FROM `inv_movtype`";
		return $this->ExecuteS($sql);
	}
	
	public function getMovTypebyAction($act)
	{
		$sql="SELECT * FROM `inv_movtype` WHERE imt_action='$act'";
		return $this->ExecuteS($sql);
	}
	
	public function getMovTypeById($id)
	{
		$sql="SELECT * FROM `inv_movtype` WHERE imt_id='$id'";
		return $this->ExecuteS($sql);
	}
	
	//Warehouses Methods
	public function newWarehouse()
	{
		$name = $this->postVars('name');
		$desc = $this->postVars('desc');
		$resp = $this->postVars('resp');
		$addr = $this->postVars('addr');
		$phone = $this->postVars('phone');
		$def = $this->postVars('def');
		
		$sql="INSERT INTO `inv_warehouses` (`iw_name`, `iw_desc`, `iw_responsble`, `iw_addr`, `iw_phone`) VALUES
		('$name', '$desc', '$resp', '$addr', '$phone')";
		$this->Execute($sql);
		
		$id = $this->getLastID();
		if($def == 1)
		{
			$this->setDefaultWarehouse($id);
		}
		$this->uploadWhImg($id);
		
		return $id;
	}
	
	public function setDefaultWarehouse($id)
	{
		$sql="UPDATE `inv_warehouses` SET `iw_isdefault`='0'";
		$this->Execute($sql);
		
		$sql="UPDATE `inv_warehouses` SET `iw_isdefault`='1' WHERE iw_id='$id'";
		$this->Execute($sql);
		
	}
	
	public function uploadWhImg($id){
		
		$ok=$this->upLoadFileProccess('arch', 'modules/inventory/whImages');
		
		if($ok){
		 $sql="update `inv_warehouses` set `iw_img`='".$this->fname."' where `iw_id`='$id'";
		 $this->Execute($sql);
		}
	}
	
	public function editWarehouse()
	{
		$id = $this->postVars('id');
		$name = $this->postVars('name');
		$desc = $this->postVars('desc');
		$resp = $this->postVars('resp');
		$addr = $this->postVars('addr');
		$phone = $this->postVars('phone');
		$def = $this->postVars('def');
		
		$sql="UPDATE `inv_warehouses` SET `iw_name`='$name', `iw_desc`='$desc', `iw_responsble`='$resp', `iw_addr`='$addr', `iw_phone`='$phone' WHERE iw_id='$id'";
		$this->Execute($sql);
		if($def == 1)
		{
			$this->setDefaultWarehouse($id);
		}
		$this->uploadWhImg($id);
	}
	
	public function delWarehouse()
	{
		$id = $this->getVars('id');
		
		$sql="DELETE FROM `inv_prod` WHERE id_wh='$id'";
		$this->Execute($sql);
				
		$sql="DELETE FROM `inv_warehouses` WHERE iw_id='$id'";
		$this->Execute($sql);
	}
	
	public function getWarehouse()
	{		
		$sql="SELECT * FROM `inv_warehouses`";
		return $this->ExecuteS($sql);
	}
	
	public function getWarehouseById($id)
	{		
		$sql="SELECT * FROM `inv_warehouses` WHERE iw_id='$id'";
		return $this->ExecuteS($sql);
	}
	
	public function getDefaultWarehouse()
	{		
		$sql="SELECT * FROM `inv_warehouses` WHERE iw_isdefault='1'";
		$row = $this->ExecuteS($sql);
		if(count($row)>0)
		{
			foreach($row as $row)
			{
				return $row['iw_id'];
			}	
		}	
		
	}
	
	//Referrals Method
	public function newReferral($desc, $state = 0, $user, $id_move, $id_invoice, $id_po, $id_customer, $who, $whd, $whnameo, $whnamed, $type)
	{
		$barcode = $this->genBarcode();
		$today = date('Y-m-d H:i:s');
		$track = $this->getConfigValue(2);
		
		$sql="INSERT INTO `inv_referrals` (`ir_date`, `ir_desc`, `ir_track`, `ir_barcode`, `ir_state`, `u_id`, `imt_id`, `id_invoice`, `id_po`, `id_customer`, `iwo_id`, `iwd_id`, `ir_who`, `ir_whd`, `ir_active`, `irt_id`)
		VALUES ('$today', '$desc','$track', '$barcode', '$state', '$user', '$id_move','$id_invoice', '$id_po', '$id_customer', '$who', '$whd', '$whnameo', '$whnamed','1','$type')";
		$this->Execute($sql);
		
		$this->updateConfigValue(2);
		$sql="SELECT * FROM `inv_referrals` ORDER BY ir_id DESC LIMIT 0,1";
		$row = $this->ExecuteS($sql);
		foreach($row as $row)
			{
				return $row["ir_id"];
				
			}
		//echo $id = $this->getLastID();
		//return $id;
	}
	
	public function editReferral($id, $desc, $state = 0, $user, $id_move, $id_invoice, $id_po, $id_customer, $who, $whd, $whnameo, $whnamed,$type)
	{
		$sql="UPDATE `inv_referrals` SET `ir_desc`='$desc', `ir_state`='$state', `imt_id`='$id_move', `iwo_id`='$who', `iwd_id`='$whd', `ir_who`='$whnameo', `ir_whd`='$whnamed', `irt_id`='$type' WHERE ir_id='$id'";
		$this->Execute($sql);
	}
	
	public function editReferralTotal($id, $total)
	{
		$sql="UPDATE `inv_referrals` SET `ir_total`='$total' WHERE ir_id='$id'";
		$this->Execute($sql);
	}
	
	public function delReferral($id, $active = 0)
	{
		$sql="UPDATE `inv_referrals` SET `ir_active`='$active' WHERE ir_id='$id'";
		$this->Execute($sql);
	}
	
	public function genBarcode()
	{
		$cad = "0123456789";
		for($i=0; $i<=10; $i++)
		{
			$pos = rand(0,strlen($cad));
			$bar .= $cad[$pos];
		}
		
		return $bar;
	}
	
	public function getReferrals($act = 1)
	{
		$sql="SELECT * FROM `inv_referrals` WHERE ir_active='$act' order by ir_date DESC limit 10";
		return $this->ExecuteS($sql);
	}
	
	public function getUserReferrals($id_user, $act = 1)
	{
		$sql="SELECT * FROM `inv_referrals` WHERE u_id='$id_user' AND ir_active='$act'";
		return $this->ExecuteS($sql);
	}
	
	public function getReferralsById($id)
	{
		$sql="SELECT * FROM `inv_referrals` WHERE ir_id='$id'";
		return $this->ExecuteS($sql);
	}
	
	public function getReferralsByTrack($track)
	{
		$sql="SELECT * FROM `inv_referrals` WHERE ir_track='$track'";
		return $this->ExecuteS($sql);
	}

	public function getReferralsLazyLoad($limit = null, $limit_max = null, $type, $track, $id_type, $act=1){
		$sql = "SELECT * FROM `inv_referrals` where ir_active='$act' order by ir_date DESC limit $limit,$limit_max";

		if($track && $id_type && $type){
			$sql="SELECT * FROM `inv_referrals` WHERE ir_active='$act' and ir_track='$track' and imt_id='$type' and irt_id='$id_type' order by ir_date DESC limit $limit,$limit_max";
		}else if($track && $id_type && !$type){
			$sql="SELECT * FROM `inv_referrals` WHERE ir_active='$act' and ir_track='$track' and irt_id='$id_type' order by ir_date DESC limit $limit,$limit_max";
		}else if($track && !$id_type && $type){
			$sql="SELECT * FROM `inv_referrals` WHERE ir_active='$act' and ir_track='$track' and imt_id='$type' order by ir_date DESC limit $limit,$limit_max";
		}else if(!$track && $id_type && $type){
			$sql="SELECT * FROM `inv_referrals` WHERE ir_active='$act' and imt_id='$type' and irt_id='$id_type' order by ir_date DESC limit $limit,$limit_max";
		}else if($track && !$id_type && !$type){
			$sql="SELECT * FROM `inv_referrals` WHERE ir_active='$act' and ir_track='$track' order by ir_date DESC limit $limit,$limit_max";
		}else if(!$track && $id_type && !$type){
			$sql="SELECT * FROM `inv_referrals` WHERE ir_active='$act' and irt_id='$id_type' order by ir_date DESC limit $limit,$limit_max";
		}else if(!$track && !$id_type && $type){
			$sql="SELECT * FROM `inv_referrals` WHERE ir_active='$act' and imt_id='$type' order by ir_date DESC limit $limit,$limit_max";
		}		
		return $this->ExecuteS($sql);
	}

	public function getReferralsLazyLoadTotal($limit = null, $limit_max = null, $type, $track, $id_type, $act=1){
		$sql = "SELECT * FROM `inv_referrals` where ir_active='$act' order by ir_date DESC";

		if($track && $id_type && $type){
			$sql="SELECT * FROM `inv_referrals` WHERE ir_active='$act' and ir_track='$track' and imt_id='$type' and irt_id='$id_type' order by ir_date DESC";
		}else if($track && $id_type && !$type){
			$sql="SELECT * FROM `inv_referrals` WHERE ir_active='$act' and ir_track='$track' and irt_id='$id_type' order by ir_date DESC";
		}else if($track && !$id_type && $type){
			$sql="SELECT * FROM `inv_referrals` WHERE ir_active='$act' and ir_track='$track' and imt_id='$type' order by ir_date DESC";
		}else if(!$track && $id_type && $type){
			$sql="SELECT * FROM `inv_referrals` WHERE ir_active='$act' and imt_id='$type' and irt_id='$id_type' order by ir_date DESC";
		}else if($track && !$id_type && !$type){
			$sql="SELECT * FROM `inv_referrals` WHERE ir_active='$act' and ir_track='$track' order by ir_date DESC";
		}else if(!$track && $id_type && !$type){
			$sql="SELECT * FROM `inv_referrals` WHERE ir_active='$act' and irt_id='$id_type' order by ir_date DESC";
		}else if(!$track && !$id_type && $type){
			$sql="SELECT * FROM `inv_referrals` WHERE ir_active='$act' and imt_id='$type' order by ir_date DESC";
		}		
		return $this->ExecuteS($sql);
	}
	
	public function getUserReferralsByTrack($id_user, $track)
	{
		$sql="SELECT * FROM `inv_referrals` WHERE u_id='$id_user' AND ir_track='$track'";
		return $this->ExecuteS($sql);
	}
	
	public function showPrintReferral($id)
	{
		$f=fopen('../tpls/referralformat.html','r');
		$m = fread($f,filesize('../tpls/referralformat.html'));
		
		$row = $this->getReferralsById($id);
		if(count($row)>0)
		{
			foreach($row as $row)
			{
				$date = $row["ir_date"];
				$desc = $row["ir_desc"];
				$track = $row["ir_track"];
				$barcode = $row["ir_barcode"];
				$invoice = $row["id_invoice"];
				$po = $row["id_po"];
				$customer = $row["id_customer"];
				$id_move = $row["imt_id"];
				$whnameo = $row["ir_who"];
				$whnamed = $row["ir_whd"];
				$total = $row["ir_total"];
			}
		}
		
		$row = $this->getMovTypeById($id_move);
		if(count($row)>0)
		{
			foreach($row as $row)
			{
				$tm = $row["imt_name"];
			}
		}
		
		$det = "<table class='tabledetail'>
		<tr>
			<td><strong>PRODUCTOS</strong></td>
			<td><strong>CANTIDAD</strong></td>
			<td><strong>CONTENEDOR</strong></td>
			<td><strong>CANTIDAD TOTAL</strong></td>
			<td><strong>PRECIO</strong></td>
			<td><strong>TOTAL</strong></td>
		</tr>
		";
		
		$row = $this->getReferralDetail($id);
		if(count($row)>0)
		{
			foreach($row as $row)
			{
				$det .= "
				<tr>
				    <td width='20%'>".$row["ird_productname"]."</td>
				    <td width='20%'>".$row["qty3"]."</td>
					<td width='20%'>".$row["qty2"]."</td>
					<td width='20%'>".$row["qty"]."</td>
					<td width='20%'>$".number_format($row["ird_price"],2)."</td>
					<td width='20%'>$".number_format($row["ird_price"]*$row["qty"],2)."</td>
					
				</tr>
				";
			}
		}
		$det .= "</table>";
		
		$crmModelObj=new crmScs();
		 $crmModelObj->connect();
		 
		 $row=$crmModelObj->getCompanyById(2);
		 if(count($row)>0){
		  foreach($row as $row){
		  	$company='<img src="../../crm/Logos/'.$row["e_img"].'" width="200" height="100" /><br />';
		  }
		 }
		 
		
		$m=str_replace('[whnamo]',$whnameo,$m);
		$m=str_replace('[whnamed]',$whnamed,$m);
		$m=str_replace('[mt]',$tm,$m);
		$m=str_replace('[fecha]',$date,$m);
		$m=str_replace('[track]',$track,$m);
		$m=str_replace('[detalle]',$det,$m);
		$m=str_replace('[descripcion]',$desc,$m);
		$m=str_replace('[invoice]',$invoice,$m);
		$m=str_replace('[po]',$po,$m);
		$m=str_replace('[customer]',$customer,$m);
		$m=str_replace('[barcode]',$this->setBarcode($barcode),$m);
		$m=str_replace('[company]',$company,$m);
		$m=str_replace('[total]',"$".number_format($total),$m);
		
		return $m;
	}
	
	public function showPrintReferralUser($id)
	{
		$f=fopen('../tpls/referralformat2.html','r');
		$m = fread($f,filesize('../tpls/referralformat2.html'));
		
		$row = $this->getReferralsById($id);
		if(count($row)>0)
		{
			foreach($row as $row)
			{
				$date = $row["ir_date"];
				$desc = $row["ir_desc"];
				$track = $row["ir_track"];
				$barcode = $row["ir_barcode"];
				$invoice = $row["id_invoice"];
				$po = $row["id_po"];
				$customer = $row["id_customer"];
				$id_move = $row["imt_id"];
				$whnameo = $row["ir_who"];
				$whnamed = $row["ir_whd"];
				$total = $row["ir_total"];
			}
		}
		
		$row = $this->getMovTypeById($id_move);
		if(count($row)>0)
		{
			foreach($row as $row)
			{
				$tm = $row["imt_name"];
			}
		}
		
		$det = "<table class='tabledetail'>
		<tr>
			<td><strong>PRODUCTOS</strong></td>
			<td><strong>CANTIDAD</strong></td>
			<td><strong>CONTENEDOR</strong></td>
			<td><strong>CANTIDAD TOTAL</strong></td>
		</tr>
		";
		
		$row = $this->getReferralDetail($id);
		if(count($row)>0)
		{
			foreach($row as $row)
			{
				$det .= "
				<tr>
				    <td width='40%'>".$row["ird_productname"]."</td>
				    <td width='20%'>".$row["qty3"]."</td>
					<td width='20%'>".$row["qty2"]."</td>
					<td width='20%'>".$row["qty"]."</td>
				</tr>
				";
			}
		}
		$det .= "</table>";
		
		$crmModelObj=new crmScs();
		 $crmModelObj->connect();
		 
		 $row=$crmModelObj->getCompanyById(2);
		 if(count($row)>0){
		  foreach($row as $row){
		  	$company='<img src="../../crm/Logos/'.$row["e_img"].'" width="200" height="100" /><br />';
		  }
		 }
		 
		
		$m=str_replace('[whnamo]',$whnameo,$m);
		$m=str_replace('[whnamed]',$whnamed,$m);
		$m=str_replace('[mt]',$tm,$m);
		$m=str_replace('[fecha]',$date,$m);
		$m=str_replace('[track]',$track,$m);
		$m=str_replace('[detalle]',$det,$m);
		$m=str_replace('[descripcion]',$desc,$m);
		$m=str_replace('[invoice]',$invoice,$m);
		$m=str_replace('[po]',$po,$m);
		$m=str_replace('[customer]',$customer,$m);
		$m=str_replace('[barcode]',$this->setBarcode($barcode),$m);
		$m=str_replace('[company]',$company,$m);
		//$m=str_replace('[total]',"$".number_format($total),$m);
		
		return $m;
	}
	
	public function setBarcode($track)
	{
		if (!isset($output))  $output   = "png";
		if (!isset($type))    $type     = "C128B";
		if (!isset($widthb))   $widthb    = "300";
		if (!isset($heightb))  $heightb   = "100";
		if (!isset($xres))    $xres     = "2";
		if (!isset($font))    $font     = "2";
		
		$drawtext="off";
		$stretchtext="off";
		
		$style |= ($output  == "png" ) ? BCS_IMAGE_PNG  : 0;
		$style |= ($output  == "jpeg") ? BCS_IMAGE_JPEG : 0;
		$style |= ($border  == "on"  ) ? BCS_BORDER           : 0;
		$style |= ($drawtext== "on"  ) ? BCS_DRAW_TEXT  : 0;
		$style |= ($stretchtext== "on" ) ? BCS_STRETCH_TEXT  : 0;
		$style |= ($negative== "on"  ) ? BCS_REVERSE_COLOR  : 0;
	 
		 //$bc = new I25Object(0, 0, $style, $track); 
		 return $barcode = "<img style='height: 50px; width: 300px' src='../barcode/image.php?code=".$track."&style=".$style."&type=".$type."&width=".$widthb."&height=".$heightb."&xres=".$xres."&font=".$font."' />";
	}
	
	public function getReferralReport($start, $end, $who, $whd, $type)
	{
	    $start .= " 00:00:00";
	    $end .= " 23:59:59";
	    
	    $sql = "SELECT * FROM `inv_referrals` WHERE ir_date BETWEEN '$start' AND '$end' AND ir_active = '1'";
	    
	    if($who != '')
	    {
	        $sql .=" AND iwo_id='$who'";
	    }
	    
	    if($whd != '')
	    {
	        $sql .=" AND iwd_id='$whd'";
	    }
	    
	    if($type != '')
	    {
	        $sql .=" AND irt_id='$type'";
	    }
	    
	    $sql .=" ORDER BY ir_date DESC";
	    
	    return $this->ExecuteS($sql);
	}
	
	//Referrals Detail Method 
	public function newReferralDetail($id_referral, $product_name, $id_product, $qty, $price, $qty2, $qty3)
	{
		$sql="INSERT INTO `inv_referrals_detail` (`ird_productname`, `ird_qty`, `id_product`, `ir_id`, `ird_price`, `ird_qty2`, `ird_qty3`)
		VALUES ('$product_name', '$qty', '$id_product', '$id_referral', '$price','$qty2','$qty3')";
		$this->Execute($sql);
	}
	
	public function getReferralDetail($id)
	{
		//$sql="SELECT * FROM `inv_referrals_detail` WHERE ir_id='$id'";
		$sql="SELECT ird_id, ird_productname, SUM(ird_qty) as qty, id_product, ird_price, SUM(ird_qty2) as qty2, SUM(ird_qty3) as qty3 FROM `inv_referrals_detail` WHERE ir_id='$id' GROUP BY id_product ORDER BY ird_id DESC";
		return $this->ExecuteS($sql);
	}
	
	public function delReferralDetail($id)
	{
		$sql="DELETE FROM `inv_referrals_detail` WHERE ird_id='$id'";
		$this->Execute($sql);
	}
	
	//Referrals Detail Temp Method 
	public function newReferralDetailTemp($user, $product_name, $id_product, $qty, $price, $qty2, $qty3)
	{
		$sql="INSERT INTO `inv_referrals_detail_temp` (`irdt_productname`, `irdt_qty`, `id_product`, `u_id`, `irdt_price`, `irdt_qty2`, `irdt_qty3`)
		VALUES ('$product_name', '$qty', '$id_product', '$user', '$price','$qty2','$qty3')";
		$this->Execute($sql);
	}
	
	public function getReferralDetailTemp($user)
	{
		//$sql="SELECT * FROM `inv_referrals_detail_temp` WHERE u_id='$user'";
		$sql="SELECT irdt_id, irdt_productname, SUM(irdt_qty) as qty, id_product, irdt_price, SUM(irdt_qty2) as qty2, SUM(irdt_qty3) as qty3 FROM `inv_referrals_detail_temp` WHERE u_id='$user' GROUP BY irdt_id DESC";
		return $this->ExecuteS($sql);
	}
	
	public function delReferralDetailTemp($id)
	{
		$sql="DELETE FROM `inv_referrals_detail_temp` WHERE irdt_id='$id'";
		$this->Execute($sql);
	}

	public function updateReferralDetailTemp($id, $qty, $qtytotal)
	{
		$sql="UPDATE inv_referrals_detail_temp  SET `irdt_qty3`='$qty', `irdt_qty`='$qtytotal' WHERE irdt_id='$id'";
		$this->Execute($sql);
	}
	
	public function getReferralDetailTempById($id)
	{
		$sql="SELECT * FROM `inv_referrals_detail_temp` WHERE irdt_id='$id'";
		return $this->Execute($sql);
	}

	public function flushReferralDetailTemp($user)
	{
		$sql="DELETE FROM `inv_referrals_detail_temp` WHERE u_id='$user'";
		$this->Execute($sql);
	}
	
	//Referrals states methods
	public function newReferralState($name, $run, $color)
	{
		$sql="INSERT INTO `inv_referrals_states`(`irs_name`, `irs_runmov`, `irs_color`) VALUES ('$name','$run','$color')";
		$this->Execute($sql);
	}
	
	public function editReferralState($id, $name, $run, $color)
	{
		$sql="UPDATE `inv_referrals_states` SET `irs_name`='$name', `irs_runmov`='$run', `irs_color`='$color' WHERE irs_id='$id'";
		$this->Execute($sql);
	}
	
	public function delReferralState($id)
	{
		$sql="DELETE FROM `inv_referrals_states` WHERE irs_id='$id'";
		$this->Execute($sql);
	}
	
	public function getReferralState()
	{
		$sql="SELECT * FROM `inv_referrals_states`";
		return $this->ExecuteS($sql);
	}
	
	public function getReferralStateById($id)
	{
		$sql="SELECT * FROM `inv_referrals_states` WHERE irs_id='$id'";
		return $this->ExecuteS($sql);
	}
	
	//Referrals Type methods
	public function newReferralsType()
	{
	    $name = $this->postVars('name');
	    
	    $sql = "INSERT INTO `inv_referrals_type`(`irt_name`) VALUES ('$name')";
	    $this->Execute($sql);
	}
	
	public function editReferralsType()
	{
	    $id = $this->postVars('id');
	    $name = $this->postVars('name');
	    
	    $sql = "UPDATE `inv_referrals_type` SET `irt_name`='$name' WHERE `irt_id`='$id'";
	    $this->Execute($sql);
	}
	
	public function delReferralsType()
	{
	    $id = $this->getVars('id');
	    
	    $sql = "DELETE FROM `inv_referrals_type` WHERE `irt_id`='$id'";
	    $this->Execute($sql);
	}
	
	public function getReferralsType()
	{
	    $sql = "SELECT * FROM `inv_referrals_type`";
	    return $this->ExecuteS($sql);
	}
	
	public function getReferralsTypeById($id)
	{
	    $sql = "SELECT * FROM `inv_referrals_type` WHERE `irt_id`='$id'";
	    return $this->ExecuteS($sql);
	}
	
	//qreferal methods
	public function newQuickReferral($desc, $id_user)
	{
		$today = date('Y-m-d H:i:s');
		$track = $this->getConfigValue(2);
		$sql = "INSERT INTO `inv_qreferral`(`iqr_date`, `iqr_desc`, `iqr_track`, `u_id`, `iqr_active`) VALUES ('$today','$desc','$track','$id_user','1')";
		$this->Execute($sql);
		$this->updateConfigValue(2);
		
		$sql="SELECT * FROM `inv_qreferral` ORDER BY iqr_id DESC LIMIT 0,1";
		$row = $this->ExecuteS($sql);
		foreach($row as $row)
		{
			return $row['iqr_id'];
		} 
	}
	
	public function editQuickReferral($id, $desc)
	{
		$sql = "UPDATE `inv_qreferral` SET `iqr_desc`='$desc' WHERE iqr_id='$id'";
		$this->Execute($sql);
	}
	
	public function delQuickReferral($id)
	{
		$sql = "DELETE FROM `inv_qreferral` WHERE iqr_id='$id'";
		$this->Execute($sql);
	}
	
	public function getQuickReferralById($id)
	{
		$sql = "SELECT * FROM `inv_qreferral` WHERE iqr_id='$id'";
		return $this->ExecuteS($sql);
	}
	
	public function getQuickReferral()
	{
		$sql = "SELECT * FROM `inv_qreferral` WHERE iqr_active='1'";
		return $this->ExecuteS($sql);
	}
	
	public function editQuickReferralActive($id)
	{
		$sql = "UPDATE `inv_qreferral` SET `iqr_active`='0' WHERE iqr_id='$id'";
		$this->Execute($sql);
	}
	
	public function printQReferral($id)
	{
		$f=fopen('../tpls/qreferralformat.html','r');
		$m = fread($f,filesize('../tpls/qreferralformat.html'));
		
		$row = $this->getQuickReferralById($id);
		foreach($row as $row)
		{
			$m=str_replace('[fecha]',$row['iqr_date'],$m);
			$m=str_replace('[track]',$row['iqr_track'],$m);
			$m=str_replace('[descripcion]',$row['iqr_desc'],$m);
		}
		
		//Detalle de entrada
		$htm = "<table class='tabledetail'>
		<tr>
			<th>PRODUCTO</th>
			<th>CANTIDAD</th>
			<th>T. MOVIMIENTO</th>
			<th>BODEGA ORIGEN</th>
			<th>BODEGA DESTINO</th>
		</tr>";
		$row = $this->getQuickReferralDetailByMovType($id, 'in');
		if(count($row)>0)
		{
			foreach($row as $row)
			{
				$row1 = $this->getMovTypeById($row['id_move']);
				if(count($row1)>0)
				{
					foreach($row1 as $row1)
					{
						$mtname = $row1['imt_name'];
					}
				}
				
				$htm .= "
				<tr>
					<td>".$row['iqrd_name']."</td>
					<td>".$row['iqrd_qty']."</td>
					<td>".$mtname."</td>
					<td>".$row['iqrd_whoname']."</td>
					<td>".$row['iqrd_whdname']."</td>
				</tr>";
				
			}
		}
		
		$htm .= "</table>";
		//Detalle de salida
		$htm1 = "<table class='tabledetail'>
		<tr>
			<th>PRODUCTO</th>
			<th>CANTIDAD</th>
			<th>T. MOVIMIENTO</th>
			<th>BODEGA ORIGEN</th>
			<th>BODEGA DESTINO</th>
		</tr>";
		$row = $this->getQuickReferralDetailByMovType($id, 'out');
		if(count($row)>0)
		{
			foreach($row as $row)
			{
				$row1 = $this->getMovTypeById($row['id_move']);
				if(count($row1)>0)
				{
					foreach($row1 as $row1)
					{
						$mtname = $row1['imt_name'];
					}
				}
				
				$htm1 .= "
				<tr>
					<td>".$row['iqrd_name']."</td>
					<td>".$row['iqrd_qty']."</td>
					<td>".$mtname."</td>
					<td>".$row['iqrd_whoname']."</td>
					<td>".$row['iqrd_whdname']."</td>
				</tr>";
				
			}
		}
		
		$htm1 .= "</table>";
		$m=str_replace('[detallein]',$htm,$m);
		$m=str_replace('[detalleout]',$htm1,$m);
		
		$crmModelObj=new crmScs();
		 $crmModelObj->connect();
		 
		 $row=$crmModelObj->getCompanyById(2);
		 if(count($row)>0){
		  foreach($row as $row){
		  	$company='<img src="../../crm/Logos/'.$row["e_img"].'" width="200" height="100" /><br />';
		  }
		 }
		 $m=str_replace('[company]',$company,$m);
		 return $m;
		
	}
	
	//Quick referral detail temp
	public function newQuickReferralDetailTemp($id_user, $qty, $id_move, $typemov, $id_product, $who, $whd, $whoname, $whdname, $name_product)
	{
		$sql = "INSERT INTO `inv_qreferral_detail_temp`(`u_id`, `iqrdt_qty`, `id_move`, `iqrdt_typemov`, `id_product`, `iqrdt_who`, `iqrdt_whd`, `iqrdt_whoname`, `iqrdt_whdname`, `iqrdt_name`) VALUES ('$id_user','$qty','$id_move','$typemov','$id_product','$who','$whd','$whoname','$whdname','$name_product')";
		$this->Execute($sql);
	}
	
	public function delQuickReferralDetailTemp($id)
	{
		$sql = "DELETE FROM `inv_qreferral_detail_temp` WHERE iqrdt_id='$id'";
		$this->Execute($sql);
	}
	
	public function getQuickReferralDetailTemp($id_user)
	{
		$sql = "SELECT * FROM `inv_qreferral_detail_temp` WHERE u_id='$id_user'";
		return $this->ExecuteS($sql);
	}
	
	public function flushQuickReferralDetailTemp()
	{
		$sql = "DELETE FROM `inv_qreferral_detail_temp` WHERE u_id='".$_SESSION['user_id']."'";
		$this->Execute($sql);
	}
	
	//Quick referral detail
	public function newQuickReferralDetail($id_qreferral, $qty, $id_move, $typemov, $id_product, $who, $whd, $whoname, $whdname, $name)
	{
	//echo $typemov; die();
		$sql = "INSERT INTO `inv_qreferral_detail`(`iqr_id`, `iqrd_qty`, `id_move`, `iqrd_typemov`, `id_product`, `iqrd_who`, `iqrd_whd`, `iqrd_whoname`, `iqrd_whdname`, `iqrd_name`) VALUES ('$id_qreferral','$qty','$id_move','$typemov','$id_product','$who','$whd','$whoname','$whdname','$name')";
		$this->Execute($sql);
	}
	
	public function getQuickReferralItemLastID()
	{
		$sql = "SELECT * FROM `inv_qreferral_detail` ORDER BY iqrd_id DESC LIMIT 0,1";
		$row = $this->ExecuteS($sql);
		foreach($row as $row)
		{
			return $row['iqrd_id'];
		}
	}
	
	public function delQuickReferralDetail($id)
	{
		$sql = "DELETE FROM `inv_qreferral_detail` WHERE iqrd_id='$id'";
		$this->Execute($sql);
	}
	
	public function getQuickReferralDetail($id_qreferral)
	{
		$sql = "SELECT * FROM `inv_qreferral_detail` WHERE iqr_id='$id_qreferral'";
		return $this->ExecuteS($sql);
	}
	
	public function getQuickReferralDetailByMovType($id_qreferral, $type)
	{
		$sql = "SELECT * FROM `inv_qreferral_detail` WHERE iqr_id='$id_qreferral' AND iqrd_typemov='$type'";
		return $this->ExecuteS($sql);
	}
	
	public function getQuickReferralDetailById($id)
	{
		$sql = "SELECT * FROM `inv_qreferral_detail` WHERE iqrd_id='$id'";
		return $this->ExecuteS($sql);
	}
	
	//Micelanius
	public function getUserProfile($id_user)
	{
	    $sql = "SELECT * FROM user WHERE u_id='$id_user'";
	    $row = $this->ExecuteS($sql);
	    if(count($row)>0)
	    {
	        foreach($row as $row)
	        {
	            return $row['up_id'];
	        }
	    }
	}
	
	public function getProductImage($id_product)
	{
	    $sql = "SELECT * FROM products WHERE pro_id='$id_product'";
	    $row = $this->ExecuteS($sql);
	    if(count($row)>0)
	    {
	        foreach($row as $row)
	        {
	            return $row['pro_image'];
	        }
	    }
	}
	
	//Installers
   public function Checker(){
    $product=$this->chkTables('products');
	$caps=$this->chkTables('inv_prod');
	
	if($product && $caps){
	 return true;
	}else{
	 return false;
	}
   }
   
   public function install(){
    $this->importSQLFile('modules/inventory/sql/installer.sql.txt');
	
	//Se instala los submenus
	$sql="select * from menu where m_folder='inventory'";
	$row=$this->ExecuteS($sql);
	foreach($row as $row){
	 $m_id=$row["m_id"];
	}
	
	$sql="insert into `submenu` (`sm_nom`, `sm_link`, `m_id`) values ('Egresos','home.php?p=modules/inventory/view/newCapsInvOut.php','$m_id')";
	$this->Execute($sql);
	$sql="insert into `submenu` (`sm_nom`, `sm_link`, `m_id`) values ('Setup','home.php?p=modules/inventory/view/showSetup.php','$m_id')";
	$this->Execute($sql);
	$sql="insert into `submenu` (`sm_nom`, `sm_link`, `m_id`) values ('Informe','home.php?p=modules/inventory/view/showInfCapsInventory.php','$m_id')";
	$this->Execute($sql);
	$sql="insert into `submenu` (`sm_nom`, `sm_link`, `m_id`) values ('Remisiones','home.php?p=modules/inventory/view/showReferrals.php','$m_id')";
	$this->Execute($sql);
	
   }
   
   public function uninstall(){
    $sql='DROP TABLE `inv_mov`,`inv_prod` ,`inv_emails`, `inv_config`, `inv_warehouses`, `inv_movtype`, `inv_referrals`, `inv_referrals_detail`, `inv_referrals_detail_temp`, `inv_referrals_states`, `inv_qreferral`, `inv_qreferral_detail`, `inv_qreferral_detail_temp`';
	$this->Execute($sql);
	
	$sql="ALTER TABLE `products` DROP `inv_qty`, DROP `inv_id`;";
	
	$sql="select * from menu where m_folder='inventory'";
	$row=$this->ExecuteS($sql);
	foreach($row as $row){
	 $m_id=$row["m_id"]; 
	}
	
	$sql="select * from submenu as sm inner join pro_submenu as psm on sm.sm_id=psm.sm_id where sm.m_id='$m_id'";
	$row=$this->ExecuteS($sql);
	foreach($row as $row){
	 $sql1="delete from pro_submenu where sm_id='".$row["sm_id"]."'";
	 $this->Execute($sql);
	}
	
	$sql1="delete from submenu where m_id='$m_id'";
	$this->Execute($sql);
	
   }
	
};
?>
