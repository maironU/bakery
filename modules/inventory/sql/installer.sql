CREATE TABLE `inv_prod` (
`ip_id` INT NOT NULL AUTO_INCREMENT PRIMARY KEY ,
`ip_name` VARCHAR( 255 ) NOT NULL ,
`ip_desc` LONGTEXT NOT NULL ,
`ip_price` VARCHAR( 255 ) NOT NULL ,
`ip_stock` VARCHAR( 255 ) NOT NULL ,
`ip_minstock` VARCHAR( 255 ) NOT NULL
) ENGINE = MYISAM ;

CREATE TABLE `inv_mov` (
`im_id` INT NOT NULL ,
`im_date` DATETIME NOT NULL ,
`im_type` VARCHAR( 255 ) NOT NULL ,
`im_qty` VARCHAR( 255 ) NOT NULL ,
`id_account` INT NOT NULL ,
`u_id` INT NOT NULL
) ENGINE = MYISAM ;

ALTER TABLE `products` ADD `inv_qty` INT NOT NULL DEFAULT '0';
ALTER TABLE `inv_mov` ADD `ip_id` INT NOT NULL ;
ALTER TABLE `products` ADD `inv_id` INT NOT NULL ;
ALTER TABLE `inv_mov` ADD `im_desc` VARCHAR( 255 ) NOT NULL;
ALTER TABLE `inv_mov` ADD `ip_name` VARCHAR( 255 ) NOT NULL ;
ALTER TABLE `inv_mov` ADD `product_name` VARCHAR( 255 ) NOT NULL ;
ALTER TABLE `inv_mov` ADD `id_product` INT NOT NULL ;
ALTER TABLE `inv_mov` ADD PRIMARY KEY(`im_id`);
ALTER TABLE `inv_mov` CHANGE `im_id` `im_id` INT( 11 ) NOT NULL AUTO_INCREMENT ;
ALTER TABLE `inv_mov` ADD `iwo_id` INT NOT NULL ;
ALTER TABLE `inv_mov` ADD `iwd_id` INT NOT NULL ;
ALTER TABLE `inv_mov` ADD `imt_id` INT NOT NULL ;
ALTER TABLE `inv_mov` ADD `ir_id` INT NOT NULL ;
ALTER TABLE `inv_mov` ADD `iwo_name` VARCHAR( 255 ) NOT NULL ;
ALTER TABLE `inv_mov` ADD `iwd_name` VARCHAR( 255 ) NOT NULL ;

CREATE TABLE `inv_emails` (
  `ie_id` int(11) NOT NULL AUTO_INCREMENT,
  `ie_name` varchar(255) NOT NULL,
  `ie_mails` varchar(255) NOT NULL,
  PRIMARY KEY (`ie_id`)
) ENGINE=MyISAM;

CREATE TABLE `inv_config` (
`ic_id` INT NOT NULL AUTO_INCREMENT PRIMARY KEY ,
`ic_name` VARCHAR( 255 ) NOT NULL ,
`ic_value` LONGTEXT NOT NULL
) ENGINE = MYISAM ;

INSERT INTO `inv_config` (`ic_id`, `ic_name`, `ic_value`) VALUES (1, 'Remitente', '');
INSERT INTO `inv_config` (`ic_id`, `ic_name`, `ic_value`) VALUES (2, 'Track Remisiones', '');

CREATE TABLE `inv_warehouses` (
`iw_id` INT NOT NULL AUTO_INCREMENT PRIMARY KEY ,
`iw_name` VARCHAR( 255 ) NOT NULL ,
`iw_desc` VARCHAR( 255 ) NOT NULL ,
`iw_responsble` VARCHAR( 255 ) NOT NULL ,
`iw_addr` VARCHAR( 255 ) NOT NULL ,
`iw_phone` VARCHAR( 255 ) NOT NULL ,
`iw_img` VARCHAR( 255 ) NOT NULL ,
`iw_isdefault` VARCHAR( 255 ) NOT NULL 
) ENGINE = MYISAM ;

CREATE TABLE `inv_movtype` (
`imt_id` INT NOT NULL AUTO_INCREMENT PRIMARY KEY ,
`imt_name` VARCHAR( 255 ) NOT NULL ,
`imt_action` VARCHAR( 255 ) NOT NULL 
) ENGINE = MYISAM ;

CREATE TABLE `inv_referrals` (
`ir_id` INT NOT NULL AUTO_INCREMENT PRIMARY KEY ,
`ir_date` datetime NOT NULL ,
`ir_desc` VARCHAR( 255 ) NOT NULL ,
`ir_track` VARCHAR( 255 ) NOT NULL ,
`ir_barcode` VARCHAR( 255 ) NOT NULL ,
`ir_state` VARCHAR( 255 ) NOT NULL ,
`u_id` INT NOT NULL ,
`imt_id` INT NOT NULL ,
`id_invoice` INT NOT NULL ,
`id_po` INT NOT NULL ,
`id_customer` INT NOT NULL ,
`iwo_id` INT NOT NULL ,
`iwd_id` INT NOT NULL ,
`ir_who` VARCHAR( 255 ) NOT NULL ,
`ir_whd` VARCHAR( 255 ) NOT NULL ,
`ir_active` VARCHAR( 255 ) NOT NULL 
) ENGINE = MYISAM ;

CREATE TABLE `inv_referrals_detail` (
`ird_id` INT NOT NULL AUTO_INCREMENT PRIMARY KEY ,
`ird_productname` VARCHAR( 255 ) NOT NULL ,
`ird_qty` VARCHAR( 255 ) NOT NULL ,
`id_product` INT NOT NULL ,
`ir_id` INT NOT NULL 
) ENGINE = MYISAM ;

CREATE TABLE `inv_referrals_detail_temp` (
`irdt_id` INT NOT NULL AUTO_INCREMENT PRIMARY KEY ,
`irdt_productname` VARCHAR( 255 ) NOT NULL ,
`irdt_qty` VARCHAR( 255 ) NOT NULL ,
`id_product` INT NOT NULL ,
`u_id` INT NOT NULL 
) ENGINE = MYISAM ;

CREATE TABLE IF NOT EXISTS `inv_referrals_states` (
  `irs_id` int(11) NOT NULL AUTO_INCREMENT,
  `irs_name` varchar(255) NOT NULL,
  `irs_runmov` int(11) NOT NULL,
  `irs_color` varchar(255) NOT NULL,
  PRIMARY KEY (`irs_id`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1 AUTO_INCREMENT=1 ;

CREATE TABLE IF NOT EXISTS `inv_qreferral` (
  `iqr_id` int(11) NOT NULL AUTO_INCREMENT,
  `iqr_date` datetime NOT NULL,
  `iqr_desc` longtext NOT NULL,
  `u_id` int(11) NOT NULL,
  PRIMARY KEY (`iqr_id`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1 AUTO_INCREMENT=1 ;

CREATE TABLE IF NOT EXISTS `inv_qreferral_detail` (
  `iqrd_id` int(11) NOT NULL AUTO_INCREMENT,
  `iqr_id` int(11) NOT NULL,
  `iqrd_qty` int(11) NOT NULL,
  `id_move` int(11) NOT NULL,
  `iqrd_typemov` varchar(255) NOT NULL,
  `id_product` int(11) NOT NULL,
  `iqrd_who` int(11) NOT NULL,
  `iqrd_whd` int(11) NOT NULL,
  `iqrd_whoname` varchar(255) NOT NULL,
  `iqrd_whdname` varchar(255) NOT NULL,
  PRIMARY KEY (`iqrd_id`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1 AUTO_INCREMENT=1 ;

CREATE TABLE IF NOT EXISTS `inv_qreferral_detail_temp` (
  `iqrdt_id` int(11) NOT NULL AUTO_INCREMENT,
  `u_id` int(11) NOT NULL,
  `iqrdt_qty` int(11) NOT NULL,
  `id_move` int(11) NOT NULL,
  `iqrdt_typemov` varchar(255) NOT NULL,
  `id_product` int(11) NOT NULL,
  `iqrdt_who` int(11) NOT NULL,
  `iqrdt_whd` int(11) NOT NULL,
  `iqrdt_whoname` varchar(255) NOT NULL,
  `iqrdt_whdname` varchar(255) NOT NULL,
  PRIMARY KEY (`iqrdt_id`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1 AUTO_INCREMENT=1 ;

ALTER TABLE  `inv_qreferral` ADD  `iqr_track` VARCHAR( 255 ) NOT NULL ;
ALTER TABLE  `inv_qreferral` ADD  `iqr_active` INT NOT NULL ;
ALTER TABLE  `inv_qreferral_detail` ADD  `iqrd_name` VARCHAR( 255 ) NOT NULL ;
ALTER TABLE  `inv_qreferral_detail_temp` ADD  `iqrdt_name` VARCHAR( 255 ) NOT NULL ;
ALTER TABLE `products` ADD `pro_qtycontainer` INT NOT NULL DEFAULT '1' AFTER `pro_taxvalue`;

ALTER TABLE `inv_referrals_detail_temp` ADD `irdt_qty2` INT NOT NULL AFTER `irdt_price`;
ALTER TABLE `inv_referrals_detail` ADD `ird_qty2` INT NOT NULL AFTER `ird_price`;

ALTER TABLE `inv_referrals_detail_temp` ADD `irdt_qty3` INT NOT NULL AFTER `irdt_qty2`;
ALTER TABLE `inv_referrals_detail` ADD `ird_qty3` INT NOT NULL AFTER `ird_qty2`;

CREATE TABLE `inv_referrals_type` (
  `irt_id` int(11) NOT NULL PRIMARY KEY AUTO_INCREMENT,
  `irt_name` varchar(255) NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=latin1;
ALTER TABLE `inv_referrals` ADD `irt_id` INT NOT NULL AFTER `ir_total`;
