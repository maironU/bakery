<?php
 include('modules/content/model/Content.php');
 include("modules/content/fckeditor/fckeditor.php") ;
 
 $obj = new Content();
 $obj->connect();
 
 $msg=false;
 $msg1=false;

 //Se obtiene informacion de la zona
 if($_POST)
 {
  $id=$obj->postVars('id');
  $value=$obj->postVars('value');
  $obj->editContentSetup($id, $value);
  $msg1=true;
 }
?>
<link href="modules/content/css/Content.css" rel="stylesheet" type="text/css" />
<link href="modules/content/css/amsify.suggestags.css" rel="stylesheet" type="text/css" />
<script src="modules/content/js/jquery.amsify.suggestags.js"></script>
<div class="widget3">
 <div class="widgetlegend">Configuracion</div>
 <?php
  if($msg)
  {
  ?>
   <div class="ui-widget">
	<div class="ui-state-highlight ui-corner-all" style="margin-top: 20px; padding: 0 .7em;">
		<p><span class="ui-icon ui-icon-info" style="float: left; margin-right: .3em;"></span>
		<strong>Felicitaciones!</strong> El contenido ha sido creado satisfactoriamente.</p>
	</div>
</div>
  <?php
  }
 ?>
 <?php
  if($msg1)
  {
  ?>
   <div class="ui-widget">
	<div class="ui-state-highlight ui-corner-all" style="margin-top: 20px; padding: 0 .7em;">
		<p><span class="ui-icon ui-icon-info" style="float: left; margin-right: .3em;"></span>
		<strong>Felicitaciones!</strong> El contenido ha sido eliminado satisfactoriamente.</p>
	</div>
</div>
  <?php
  }
 ?>
<p style="width:100">
	<a href="<?php $_SERVER['PHP_SELF'];?>?p=modules/content/view/index.php" class="btn_normal" style="float:left; margin:5px;">Volver</a></p><br />
<br />
<br />

  <table width="100%">
    <tr>
      <td valign="top"><table width="558" height="27" border="0">
        <tr>
          <th>ID</th>
		  <th>Nombre</th>
		  <th>Config</th>
          <th>Acciones</th>
        </tr>
		<?php
		 $row=$obj->getContentSetup();
		 if(count($row)>0)
		 {
		     foreach($row as $row)
		     {
        		?>
        		<form id="form1" name="form1" method="post" action="#">
                <tr>
                  <td><?php echo $row["cs_id"]; ?><input type="hidden" name="id" value="<?php echo $row["cs_id"]; ?>" /></td>
        		  <td><?php echo $row["cs_name"]; ?></td>
        		  <td>
        		      <?php
        		        if($row['cs_type'] == 'textarea')
        		        {
        		            ?>
        		            <textarea cols="40" rows="6" name="value"><?php echo $row["cs_value"]; ?></textarea>
        		            <?php
        		        }
        		        else
        		        {
        		            if($row['cs_tagged'] == '1')
        		            {
        		                ?>
        		                <input type="text" name="value" value="<?php echo $row["cs_value"]; ?>" id="tagged" />
        		                <?php
        		            }
        		            else
        		            {
        		                ?>
        		                <input type="text" name="value" value="<?php echo $row["cs_value"]; ?>"  />
        		                <?php
        		            }
        		        }
        		      ?>
        		      
        		  </td>
                  <td><input type="submit" class="btn_submit" value="Guardar" /></td>
                </tr>
        		</form>
        		<?php
		     }
		 }
		?>
      </table></td>
    </tr>
  </table>

</div>
<script>
    $('#tagged').amsifySuggestags();
</script>
