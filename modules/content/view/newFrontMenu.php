<?php
 include('modules/content/model/Content.php');
 include("modules/content/fckeditor/fckeditor.php") ;
 
 $obj = new Content();
 $obj->connect();
 
 $msg=false;
 $msg1=false;

 
 //Agregar contenido
 if($_POST)
 {
  $obj->newFrontMenu();
  $msg=true;
  $msg1=false;
 }
?>
<link href="modules/content/css/Content.css" rel="stylesheet" type="text/css" />
<div class="widget3">
 <div class="widgetlegend">Nuevo Menu frontal </div>
 <?php
  if($msg)
  {
  ?>
   <div class="ui-widget">
	<div class="ui-state-highlight ui-corner-all" style="margin-top: 20px; padding: 0 .7em;">
		<p><span class="ui-icon ui-icon-info" style="float: left; margin-right: .3em;"></span>
		<strong>Felicitaciones!</strong>Se ha sido creado satisfactoriamente.</p>
	</div>
</div>
  <?php
  }
 ?>
 <?php
  if($msg1)
  {
  ?>
   <div class="ui-widget">
	<div class="ui-state-highlight ui-corner-all" style="margin-top: 20px; padding: 0 .7em;">
		<p><span class="ui-icon ui-icon-info" style="float: left; margin-right: .3em;"></span>
		<strong>Felicitaciones!</strong> El contenido ha sido eliminado satisfactoriamente.</p>
	</div>
</div>
  <?php
  }
 ?>
<p style="width:100">
	<a href="<?php $_SERVER['PHP_SELF'];?>?p=modules/content/view/showFrontMenu.php" class="btn_normal" style="float:left; margin:5px;">Volver</a></p>
 
<form id="form1" name="form1" method="post" action="#">
  <table width="594" border="0" style="float:left;">
    <tr>
      <td width="525" valign="top"><table width="587" border="0">
        <tr>
          <td width="151">Nombre : </td>
          <td width="397"><label>
            <input name="name" type="text" id="name" required/>
          </label></td>
          </tr>
        <tr>
          <td rowspan="3">Link : </td>
          <td>
		   <select name="c_id">
		    <option value="">--Seleccionar--</option>
			<?php
			 $row = $obj->getContent();
			 if(count($row)>0)
			 {
			  foreach($row as $row)
			  {
			   ?>
			   <option value="<?php echo $row["c_id"]?>"><?php echo $row["c_name"]?></option>
			   <?php
			  }
			 }
			?>
		   </select>		  </td>
          </tr>
        <tr>
          <td><input name="link" type="text" id="link" /></td>
        </tr>
        <tr>
          <td><label>
            Usar Enlace externo
            <input type="checkbox" name="useexternal" value="1" /> 
            - Destino 
            <input type="checkbox" name="target" value="1" />
          </label></td>
        </tr>
        <tr>
          <td><label>
           Mostrar en pie de pagina
             
          </label></td>
          <td><input type="checkbox" name="footer" value="1" /></td>
        </tr>
        <tr>
          <td>Padre:</td>
          <td><label>
            <select name="father" id="father">
			 <option value="0">Inicio</option>
			 <?php
			  $row1=$obj->getFrontMenu();
			  if(count($row1)>0){
			   foreach($row1 as $row1){
			   ?>
			   <option value="<?php echo $row1["cfm_id"]?>"><?php echo $row1["cfm_name"]?></option>
			   <?php
			   }
			  }
			 ?>
            </select>
          </label></td>
          </tr>
          <tr>
           <td>Imagen:</td>
           <td><select name="img" id="img">
			 <option value="0">Inicio</option>
			 <?php
			  $row=$obj->getFrontMenuImage();
			  if(count($row)>0){
			   foreach($row as $row){
			   ?>
			   <option value="<?php echo $row["cfmi_id"]?>"><?php echo $row["cfmi_name"]?></option>
			   <?php
			   }
			  }
			 ?>
            </select></td>
          </tr>
        <tr>
          <td>&nbsp;</td>
          <td><input type="submit" class="btn_submit" value="Agregar" /></td>
          </tr>
      </table></td>
    </tr>
  </table>
</form>
</div>
