<?php
 include('modules/content/model/Content.php');
 include("modules/content/fckeditor/fckeditor.php") ;
 
 $obj = new Content();
 $obj->connect();
 
 $msg=false; 
 
 //Editar contenido
 if($_POST)
 {
  $obj->editContent();
  $msg=true;
 }
 
 $id = $obj->getVars('id');
 $row = $obj->getContentById($id);
 foreach($row as $row)
 {
  $name = $row["c_name"];
  $content = $row["c_content"];
  $link = $row["c_link"];
  $delitable = $row["c_isdelitable"];
  $lang = $row["c_lang"];
 }
?>
<link href="modules/content/css/Content.css" rel="stylesheet" type="text/css" />
<?php
 if(!$ins){
 ?>
<div class="widget3">
 <div class="widgetlegend">Editor de Contenidos </div>
 <?php
  if($msg)
  {
  ?>
   <div class="ui-widget">
	<div class="ui-state-highlight ui-corner-all" style="margin-top: 20px; padding: 0 .7em;">
		<p><span class="ui-icon ui-icon-info" style="float: left; margin-right: .3em;"></span>
		<strong>Felicitaciones!</strong> El contenido ha sido modificado satisfactoriamente.</p>
	</div>
</div>
  <?php
  }
 ?>
  
<p style="width:100">
	<a href="<?php $_SERVER['PHP_SELF'];?>?p=modules/content/view/index.php" class="btn_normal" style="float:left; margin:5px;">Volver </a>
</p>
 <br><br><br> 
<form id="form1" name="form1" method="post" action="#">
  <table width="100%">
    <tr>
      <td width="50%" valign="top">
	   <table width="100%">
	    <tr>
		 <td><input type="text" name="name" placeholder="Nombre" value="<?php echo $name?>" required/></td>
		</tr>
		<tr>
		 <td>
		 <input type="text" name="link" placeholder="Enlace" value="<?php echo $link?>"/>
		 <br />
		  Si este campo se deja vacio, se armar� autom�ticamente el link
		 </td>
		</tr>
		<tr>
		 <td>
		 <?php
		  if($delitable == 1)
		  {
		  ?>
		  <input type="checkbox" name="delitable" value="1" checked="checked"/>
		  <?php
		  } else {
		   ?>
		  <input type="checkbox" name="delitable" value="1"/>
		  <?php
		  }
		 ?>
		 <br />
		  Permite borrarse este contenido?
		 </td>
		</tr>
	   </table>
	  </td>
      <td width="50%" valign="top"><?php
                                $oFCKeditor_l = new FCKeditor('content') ;
                                $oFCKeditor_l->BasePath = 'modules/content/fckeditor/';
                                $oFCKeditor_l->Width  = '650' ;
                                $oFCKeditor_l->Height = '600' ;
                                $oFCKeditor_l->Value = $content;
                                $oFCKeditor_l->Create() ;
                            ?>
          <input name="id" type="hidden" id="id" value="<?php echo $obj->getVars('id');?>" /></td>
    </tr>
    <tr>
      <td valign="top"><input name="id" type="hidden" value="<?php echo $id?>" /></td>
      <td valign="top"><div align="right"><input type="submit" class="btn_submit" value="Guardar" /></div></td>
    </tr>
  </table>
</form>
</div>
<?php }else {
 ?><br />
 
 <div class="widget3">
 <div class="widgetlegend">Instalador Editor de Contenidos </div>
 <?php
  if($msgIns)
  {
  ?>
   <div class="ui-widget">
	<div class="ui-state-highlight ui-corner-all" style="margin-top: 20px; padding: 0 .7em;">
		<p><span class="ui-icon ui-icon-info" style="float: left; margin-right: .3em;"></span>
		<strong>Felicitaciones!</strong> El modulo ha sido instalado satisfactoriamente.</p>
	</div>
</div>
  <?php
  }
 ?>
<form id="form1" name="form1" method="post" action="#">
  <table width="804" border="0" style="float:left;">
    <tr>
      <td width="525" valign="top">Bienvenido al instalador del editor de contenidos de SCS. Este modulo le ayudara a editar de forma f&aacute;cil los contenidos de su sitio web. Para proceder a la instalaci&oacute;n de este modulo, debe hacer click sobre el boton instalar y luego proceder a su correspondiente configuracion. </td>
    </tr>
    <tr>
      <td valign="top"><div align="right"><a href="<?php $_SERVER['PHP_SELF'];?>?p=modules/content/view/index.php&install=true" class="btn_normal">Instalar</a></div></td>
    </tr>
  </table>
</form>
</div>
 <?php
}
?>
