<?php
 include('modules/content/model/Content.php');
 include("modules/content/fckeditor/fckeditor.php") ;
 
 $obj = new Content();
 $obj->connect();
 
 $msg=false;
 
 
 //Editar contenido
 if($_POST)
 {
  $obj->newContent();
  $msg=true;
 }
 
 $row = $obj->getContentSetupById(1);
 foreach($row as $row)
 {
  $basic_structure = $row["cs_value"];
 }
 

?>
<link href="modules/content/css/Content.css" rel="stylesheet" type="text/css" />
<div class="widget3">
 <div class="widgetlegend">Creador de Contenidos </div>
 <?php
  if($msg)
  {
  ?>
   <div class="ui-widget">
	<div class="ui-state-highlight ui-corner-all" style="margin-top: 20px; padding: 0 .7em;">
		<p><span class="ui-icon ui-icon-info" style="float: left; margin-right: .3em;"></span>
		<strong>Felicitaciones!</strong> El contenido ha sido creado satisfactoriamente.</p>
	</div>
</div>
  <?php
  }
 ?>
  
<p style="width:100">
	<a href="<?php $_SERVER['PHP_SELF'];?>?p=modules/content/view/index.php" class="btn_normal" style="float:left; margin:5px;">Volver </a>
</p>
<br><br><br> 
<form id="form1" name="form1" method="post" action="#">
  <table width="100%">
    <tr>
      <td valign="top" width="50%">
	   <table width="100%">
	    <tr>
		 <td><input type="text" name="name" placeholder="Nombre" required/></td>
		</tr>
		<tr>
		 <td><input type="text" name="link" placeholder="Enlace"/><br />
		  Si este campo se deja vacio, se armar� autom�ticamente el link
		 </td>
		</tr>
		<tr>
		 <td><input type="checkbox" name="delitable" value="1"/><br />
		  Permite borrarse este contenido?
		 </td>
		</tr>
	   </table>
	  </td>
      <td valign="top" width="50%"><?php
                                $oFCKeditor_l = new FCKeditor('content') ;
                                $oFCKeditor_l->BasePath = 'modules/content/fckeditor/';
                                $oFCKeditor_l->Width  = '650' ;
                                $oFCKeditor_l->Height = '600' ;
                                $oFCKeditor_l->Value = $basic_structure;
                                $oFCKeditor_l->Create() ;
                            ?>
          </td>
    </tr>
    <tr>
      <td valign="top"><div align="right"></div></td>
      <td valign="top"><div align="right"><input type="submit" value="Crear" class="btn_submit" /></div></td>
    </tr>
  </table>
</form>
</div>

