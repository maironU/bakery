<?php
 include('modules/content/model/Content.php');
 include("modules/content/fckeditor/fckeditor.php") ;
 
 $obj = new Content();
 $obj->connect();
 
 //Se verifica si el modulo esta instalado
 if(!$obj->Checker()){
  $ins=true;
 }
 
 $msg=false;
 $msgIns=false;
 
 //Instala el modulo
 if($obj->getVars('install')){
  $obj->install();
  $msgIns=true;
 }
 
 $actionDel = $obj->getVars('actionDel');
 if($actionDel == true)
 {
  $obj->delContent();
 }
 
?>
<link href="modules/content/css/Content.css" rel="stylesheet" type="text/css" />
<?php
 if(!$ins){
 ?>
<div class="widget3">
 <div class="widgetlegend">Editor de Contenidos </div>
 <?php
  if($msg)
  {
  ?>
   <div class="ui-widget">
	<div class="ui-state-highlight ui-corner-all" style="margin-top: 20px; padding: 0 .7em;">
		<p><span class="ui-icon ui-icon-info" style="float: left; margin-right: .3em;"></span>
		<strong>Felicitaciones!</strong> El contenido ha sido modificado satisfactoriamente.</p>
	</div>
</div>
  <?php
  }
 ?>
  
<p style="width:100">
	<a href="<?php $_SERVER['PHP_SELF'];?>?p=modules/content/view/newContent.php" class="btn_normal" style="float:left; margin:5px;">Nuevo </a>
	<a href="<?php $_SERVER['PHP_SELF'];?>?p=modules/content/view/showFrontMenu.php" class="btn_normal" style="float:left; margin:5px;">Front Menu </a>
    <a href="<?php $_SERVER['PHP_SELF'];?>?p=modules/content/view/showFrontMenuImg.php" class="btn_normal" style="float:left; margin:5px;">Img Front Menu </a>
	<a href="<?php $_SERVER['PHP_SELF'];?>?p=modules/content/view/showSetup.php" class="btn_normal" style="float:left; margin:5px;">Setup </a>
</p>
<br />
<br />
<br />
<table width="100%">
 <tr>
  <th>ID</th>
  <th>Nombre</th>
  <th>Link</th>
  <th>Hits</th>
  <th colspan="2">Acciones</th>
 </tr>
 <?php
  $row = $obj->getContent();
  if(count($row)>0)
  {
   foreach($row as $row)
   {
   ?>
   <tr>
  	<td><?php echo $row["c_id"]?></td>
  	<td><?php echo $row["c_name"]?></td>
  	<td><?php echo $row["c_link"]?></td>
  	<td><?php echo $row["c_hitcounter"]?></td>
	<td><a href="<?php echo $_SERVER['PHP_SELF'];?>?id=<?php echo $row["c_id"]; ?>&p=modules/content/view/editContent.php" class="btn_normal">Editar</a></td>
	<td>
	<?php
	 if($row["c_isdelitable"]=='1')
	 {
	?>
	<a href="<?php echo $_SERVER['PHP_SELF'];?>?id=<?php echo $row["c_id"]; ?>&p=modules/content/view/index.php&actionDel=true" class="btn_borrar">Eliminar</a>
	<?php
	 }
	?>
	</td>
   </tr>
   <?php
   }
  }
 ?>
</table> 

</div>
<?php }else {
 ?><br />
 
 <div class="widget3">
 <div class="widgetlegend">Instalador Editor de Contenidos </div>
 <?php
  if($msgIns)
  {
  ?>
   <div class="ui-widget">
	<div class="ui-state-highlight ui-corner-all" style="margin-top: 20px; padding: 0 .7em;">
		<p><span class="ui-icon ui-icon-info" style="float: left; margin-right: .3em;"></span>
		<strong>Felicitaciones!</strong> El modulo ha sido instalado satisfactoriamente.</p>
	</div>
</div>
  <?php
  }
 ?>
<form id="form1" name="form1" method="post" action="#">
  <table width="804" border="0" style="float:left;">
    <tr>
      <td width="525" valign="top">Bienvenido al instalador del editor de contenidos de SCS. Este modulo le ayudara a editar de forma f&aacute;cil los contenidos de su sitio web. Para proceder a la instalaci&oacute;n de este modulo, debe hacer click sobre el boton instalar y luego proceder a su correspondiente configuracion. </td>
    </tr>
    <tr>
      <td valign="top"><div align="right"><a href="<?php $_SERVER['PHP_SELF'];?>?p=modules/content/view/index.php&install=true" class="btn_normal">Instalar</a></div></td>
    </tr>
  </table>
</form>
</div>
 <?php
}
?>
