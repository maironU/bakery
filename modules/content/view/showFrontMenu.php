<?php
 include('modules/content/model/Content.php');
 include("modules/content/fckeditor/fckeditor.php") ;
 
 $obj = new Content();
 $obj->connect();
 
 $msg=false;
 $msg1=false;

 //Se obtiene informacion de la zona
 if($obj->getVars('actionDel'))
 {
  $obj->delFrontMenu();
  $msg1=true;
 }
 
?>
<link href="modules/content/css/Content.css" rel="stylesheet" type="text/css" />
<div class="widget3">
 <div class="widgetlegend">Editor menu pagina frontal </div>
 <?php
  if($msg)
  {
  ?>
   <div class="ui-widget">
	<div class="ui-state-highlight ui-corner-all" style="margin-top: 20px; padding: 0 .7em;">
		<p><span class="ui-icon ui-icon-info" style="float: left; margin-right: .3em;"></span>
		<strong>Felicitaciones!</strong> El contenido ha sido creado satisfactoriamente.</p>
	</div>
</div>
  <?php
  }
 ?>
 <?php
  if($msg1)
  {
  ?>
   <div class="ui-widget">
	<div class="ui-state-highlight ui-corner-all" style="margin-top: 20px; padding: 0 .7em;">
		<p><span class="ui-icon ui-icon-info" style="float: left; margin-right: .3em;"></span>
		<strong>Felicitaciones!</strong> Se ha sido eliminado satisfactoriamente.</p>
	</div>
</div>
  <?php
  }
 ?>
<p style="width:100">
	<a href="<?php $_SERVER['PHP_SELF'];?>?p=modules/content/view/index.php" class="btn_normal" style="float:left; margin:5px;">Volver</a>
	<?php 
		if($obj->getFrontMenuLimiter(20)){
	?>
	<a href="<?php $_SERVER['PHP_SELF'];?>?p=modules/content/view/newFrontMenu.php" class="btn_normal" style="float:left; margin:5px;">Nuevo</a>
	<?php } ?>
</p>
 
<form id="form1" name="form1" method="post" action="#">
  <table width="594" border="0" style="float:left;">
    <tr>
      <td width="525" valign="top"><table width="558" border="0">
        <tr>
          <td width="148">&nbsp;</td>
          <td width="171">&nbsp;</td>
          <td width="225">&nbsp;</td>
        </tr>
      </table></td>
    </tr>
    <tr>
      <td valign="top"><table width="558" height="27" border="0">
        <tr>
          <th width="51">ID</th>
          <th width="141">Nombre</th>
          <th width="83">Link</th>
          <th width="103">Padre</th>
          <th colspan="2">Acciones</th>
        </tr>
		<?php
		 $row=$obj->getFrontMenu();
		 if(count($row)>0){
		 foreach($row as $row)
		 {
		?>
        <tr>
          <td><?php echo $row["cfm_id"]; ?></td>
          <td><?php echo $row["cfm_name"]; ?></td>
          <td><?php echo $row["cfm_link"]; ?></td>
          <td>
		   <?php
		    $row1=$obj->getFrontMenuById($row["father"]);
			if(count($row1)>0){
			 foreach($row1 as $row1){
			  echo $row1["cfm_name"];
			 }
			}
		   ?>
		  </td>
          <td width="73"><a href="<?php echo $_SERVER['PHP_SELF'];?>?id=<?php echo $row["cfm_id"]; ?>&p=modules/content/view/editFrontMenu.php" class="btn_normal">Editar</a></td>
          <td width="81"><a href="<?php echo $_SERVER['PHP_SELF'];?>?id=<?php echo $row["cfm_id"]; ?>&p=modules/content/view/showFrontMenu.php&actionDel=true" class="btn_borrar">Eliminar</a></td>
        </tr>
		<?php
		 }
		}
		?>
      </table></td>
    </tr>
  </table>
</form>
</div>
