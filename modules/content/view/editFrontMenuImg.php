<?php
 include('modules/content/model/Content.php');
 include("modules/content/fckeditor/fckeditor.php") ;
 
 $obj = new Content();
 $obj->connect();
 
 $msg=false;
 $msg1=false;
 
 //Agregar contenido
 if($_POST)
 {
  $obj->editFrontMenuImage();
  $msg=true;
  $msg1=false;
  $id=$obj->postVars('id');
 }else{
  $id=$obj->getVars('id');
 }
 
 $row=$obj->getFrontMenuImageById($id);
 foreach($row as $row){
  $name=$row["cfmi_name"];
  $img=$row["cfmi_img"];
  $awe=$row["cfmi_awe"];
  $useawe=$row["cfmi_useawe"];
 }
?>
<link href="modules/content/css/Content.css" rel="stylesheet" type="text/css" />
<div class="widget3">
 <div class="widgetlegend">Editor Imagen Menu frontal </div>
 <?php
  if($msg)
  {
  ?>
   <div class="ui-widget">
	<div class="ui-state-highlight ui-corner-all" style="margin-top: 20px; padding: 0 .7em;">
		<p><span class="ui-icon ui-icon-info" style="float: left; margin-right: .3em;"></span>
		<strong>Felicitaciones!</strong>Se ha sido guardado satisfactoriamente.</p>
	</div>
</div>
  <?php
  }
 ?>
 <?php
  if($msg1)
  {
  ?>
   <div class="ui-widget">
	<div class="ui-state-highlight ui-corner-all" style="margin-top: 20px; padding: 0 .7em;">
		<p><span class="ui-icon ui-icon-info" style="float: left; margin-right: .3em;"></span>
		<strong>Felicitaciones!</strong> El contenido ha sido eliminado satisfactoriamente.</p>
	</div>
</div>
  <?php
  }
 ?>
<p style="width:100">
	<a href="<?php $_SERVER['PHP_SELF'];?>?p=modules/content/view/showFrontMenuImg.php" class="btn_normal" style="float:left; margin:5px;">Volver</a></p>
 
<form id="form1" name="form1" method="post" action="#">
  <table width="594" border="0" style="float:left;">
    <tr>
      <td width="525" valign="top"><table width="587" border="0">
        <tr>
          <td width="151">Nombre : </td>
          <td width="397"><label>
            <input name="name" type="text" id="name" value="<?php echo $name; ?>" required />
          </label></td>
          </tr>
        <tr>
          <td>Imagen: </td>
          <td><input name="img" type="file" id="img" />
           <img src="modules/content/imgMenu/<?php echo $img; ?>" />          </td>
          </tr>
        
        <tr>
          <td>Awesome Class: </td>
          <td><input name="awe" type="text" id="awe" value="<?php echo $awe?>" /></td>
          </tr>
        <tr>
          <td>Usar Awesome Font </td>
          <td>
		  <?php
		   if($useawe == 1)
		   {
		   ?>
		   <input type="checkbox" name="useawe" value="1" checked="checked" />
		   <?php
		   } else {
		   ?>
		   <input type="checkbox" name="useawe" value="1" />
		   <?php
		   }
		  ?>
		  </td>
        </tr>
        <tr>
          <td><input type="hidden" value="<?php echo $id?>" name="id" /></td>
          <td><input type="submit" class="btn_submit" value="Guardar" /></td>
        </tr>
      </table></td>
    </tr>
  </table>
</form>
</div>
