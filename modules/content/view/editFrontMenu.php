<?php
 include('modules/content/model/Content.php');
 include("modules/content/fckeditor/fckeditor.php") ;
 
 $obj = new Content();
 $obj->connect();
 
 $msg=false;
 $msg1=false;
 
 //Agregar contenido
 if($_POST)
 {
  $obj->editFrontMenu();
  $msg=true;
  $msg1=false;
  $id=$obj->postVars('id');
 }else{
  $id=$obj->getVars('id');
 }
 
 $row=$obj->getFrontMenuById($id);
 foreach($row as $row){
  $name=$row["cfm_name"];
  $link=$row["cfm_link"];
  $father=$row["cfm_father"];
  $img=$row["cfmi_id"];
  $c_id=$row["c_id"];
  $target=$row["cfm_target"];
  $useexternal=$row["cfm_useexternal"];
  $footer = $row["cfm_showfooter"];
 }
?>
<link href="modules/content/css/Content.css" rel="stylesheet" type="text/css" />
<div class="widget3">
 <div class="widgetlegend">Editor Menu frontal </div>
 <?php
  if($msg)
  {
  ?>
   <div class="ui-widget">
	<div class="ui-state-highlight ui-corner-all" style="margin-top: 20px; padding: 0 .7em;">
		<p><span class="ui-icon ui-icon-info" style="float: left; margin-right: .3em;"></span>
		<strong>Felicitaciones!</strong>Se ha sido guardado satisfactoriamente.</p>
	</div>
</div>
  <?php
  }
 ?>
 <?php
  if($msg1)
  {
  ?>
   <div class="ui-widget">
	<div class="ui-state-highlight ui-corner-all" style="margin-top: 20px; padding: 0 .7em;">
		<p><span class="ui-icon ui-icon-info" style="float: left; margin-right: .3em;"></span>
		<strong>Felicitaciones!</strong> El contenido ha sido eliminado satisfactoriamente.</p>
	</div>
</div>
  <?php
  }
 ?>
<p style="width:100">
	<a href="<?php $_SERVER['PHP_SELF'];?>?p=modules/content/view/showFrontMenu.php" class="btn_normal" style="float:left; margin:5px;">Volver</a></p>
 
<form id="form1" name="form1" method="post" action="#">
  <table width="594" border="0" style="float:left;">
    <tr>
      <td width="525" valign="top"><table width="587" border="0">
        <tr>
          <td width="151">Nombre : </td>
          <td width="397"><label>
            <input name="name" type="text" id="name" value="<?php echo $name; ?>" />
          </label></td>
          </tr>
        <tr>
          <td rowspan="3">Link : </td>
          <td><select name="c_id" >
            <option value="">--Seleccionar--</option>
            <?php
			 $row = $obj->getContent();
			 if(count($row)>0)
			 {
			  foreach($row as $row)
			  {
			   if($c_id == $row["c_id"])
			   {
			    ?>
            <option value="<?php echo $row["c_id"]?>" selected="selected"><?php echo $row["c_name"]?></option>
            <?php
			   } else {
			    ?>
            <option value="<?php echo $row["c_id"]?>"><?php echo $row["c_name"]?></option>
            <?php
			   }
			  
			  }
			 }
			?>
          </select></td>
          </tr>
        <tr>
          <td><input name="link" type="text" id="link" value=" <?php echo $link; ?>" /></td>
        </tr>
        <tr>
          <td>Usar Enlace externo
            <?php
			 if($useexternal == 1)
			 {
			 ?>
			 <input type="checkbox" name="useexternal" value="1" checked="checked" />
			 <?php
			 }
			 else
			 {
			 ?>
			 <input type="checkbox" name="useexternal" value="1" />
			 <?php
			 }
			?>
			
- Destino
<?php
			 if($target == 1)
			 {
			 ?>
			 <input type="checkbox" name="target" value="1" checked="checked" />
			 <?php
			 }
			 else
			 {
			 ?>
			 <input type="checkbox" name="target" value="1" />
			 <?php
			 }
			?>
</td>
        </tr>
        <tr>
          <td><label>
           Mostrar en pie de pagina
          </label></td>
          <td><?php
                if($footer == 1)
                {
                    ?>
                    <input type="checkbox" name="footer" value="1" checked/>
                    <?php
                }
                else
                {
                    ?>
                    <input type="checkbox" name="footer" value="1" />
                    <?php
                }
            ?></td>
        </tr>
        <tr>
          <td>Padre:</td>
          <td><label>
            <select name="father" id="father">
			 <option value="0">Inicio</option>
			 <?php
			  $row=$obj->getFrontMenu();
			  if(count($row)>0){
			   foreach($row as $row)
			    if($father==$row["cfm_id"]){
				 ?>
			   <option value="<?php echo $row["cfm_id"]?>" selected="selected"><?php echo $row["cfm_name"]?></option>
			   <?php
				}else{
				?>
			   <option value="<?php echo $row["cfm_id"]?>"><?php echo $row["cfm_name"]?></option>
			   <?php
				}
			   
			  }
			 ?>
            </select>
          </label></td>
          </tr>
          <tr>
           <td>Imagen:</td>
           <td><select name="img" id="img">
			 <option value="0">Inicio</option>
			 <?php
			  $row=$obj->getFrontMenuImage();
			  if(count($row)>0){
			   foreach($row as $row){
			   if($img==$row["cfmi_id"]){
			   ?>
			   <option value="<?php echo $row["cfmi_id"]?>" selected="selected"><?php echo $row["cfmi_name"]?></option>
			   <?php
			   }else
			   {
				 ?>
			   <option value="<?php echo $row["cfmi_id"]?>"><?php echo $row["cfmi_name"]?></option>
			   <?php  
				}
			   }
			  }
			 ?>
            </select></td>
          </tr>
        <tr>
          <td><input type="hidden" value="<?php echo $id?>" name="id" /></td>
          <td><a href="javascript:;" class="btn_normal" onclick="document.form1.submit();">Guardar</a></td>
          </tr>
      </table></td>
    </tr>
  </table>
</form>
</div>
