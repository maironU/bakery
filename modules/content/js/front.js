function post(id_content) {

    var method = "post"; // Set method to post by default if not specified.
    

    // The rest of this code assumes you are not using a library.
    // It can be made less wordy if you use one.
    var form = document.createElement("form");
    form.setAttribute("method", method);
    form.setAttribute("action", 'content.php');
    
    var hiddenField = document.createElement("input");
    hiddenField.setAttribute("type", "hidden");
    hiddenField.setAttribute("name", "id");
    hiddenField.setAttribute("value", id_content);
    
    form.appendChild(hiddenField);
    document.body.appendChild(form);
    form.submit();
    
}