<?php 
 class Content extends GeCore
 {
   public $id;
   public $name;
   public $content;
   
   private static $Key = "Epcsoluciones";
  
   //Content Methods
   public function newContent()
   {
    $name=$this->postVars('name');
	$content=$this->postVars('content');
	$link=$this->postVars('link');
	$delitable=$this->postVars('delitable');
	$lang=$this->postVars('lang');
	$content=str_replace("'","",$content);
	
    $sql="insert into `content` (`c_name`, `c_content`, `c_link`, `c_isdelitable`, `c_lang`, `c_hitcounter`) values ('$name','$content', '$link', '$delitable', '$lang', '0')";
	$this->Execute($sql);
	
	$id = $this->getLastID();
	
	if($link == '')
	{
	 $this->builtContentLink($id);
	} 
	
   }
   
   public function builtContentLink($id)
   {
         //$link = "content.php?id=".$id;
         //$link = "post(".$id.");";
         $link = "content.php?c=".$this->encrypt($id);
	 $sql = "update `content` set `c_link`='$link' where c_id='$id'";
	 $this->Execute($sql);
   }
   
   public function editContent(){
   
   	$id=$this->postVars('id');
	$name=$this->postVars('name');
	$content=$this->postVars('content');
	$name=$this->postVars('name');
	$content=$this->postVars('content');
	$link=$this->postVars('link');
	$delitable=$this->postVars('delitable');
	$lang=$this->postVars('lang');
	
	$content=str_replace("'","",$content);

	 $sql="update `content` set `c_content`='$content', `c_name`='$name', `c_link`='$link', `c_isdelitable`='$delitable', `c_lang`='$lang'  where c_id='$id'";
    $this->Execute($sql);
	
	//if($link == '')
	{
	 $this->builtContentLink($id);
	}
	
   }
   
   public function delContent()
   {
   	$id=$this->getVars('id');
	
    $sql="delete from `content` where c_id='$id'";
	return $this->Execute($sql);
   }
   
   public function getContent()
   {
   	$sql="select * from content";
	return $this->ExecuteS($sql);
   }
   
   public function getContentById($id)
   {
	$sql="select * from content where c_id='$id'";
	return $this->ExecuteS($sql);
   }
   
   public function showContent($id)
   {
	$sql="select * from content where c_id='$id'";
	$row=$this->ExecuteS($sql);
	
	if(count($row)>0)
	{
		 foreach($row as $row)
		{
		 $content=$row["c_content"];
		 $actual=$row["c_hitcounter"];
		}
		$this->contentCounter($id, $actual);
		return $content;
	}
   }
   
   public function showContentCod()
   {
       $c = $this->getVars('c');
       $c = str_replace(' ','+',$c);
       $id = $this->decrypt($c);
       return $this->showContent($id);
   }
   
   public function contentCounter($id, $actual)
   {
    $actual += 1;
	$sql = "update content set c_hitcounter='$actual' where c_id='$id'"; 
	$this->Execute($sql);
   }
   
   //Front Menu
   public function newFrontMenu(){
    $name=$this->postVars('name');
	$c_id=$this->postVars('c_id');
	$father=$this->postVars('father');
	$img=$this->postVars('img');
	$useexternal=$this->postVars('useexternal');
	$link=$this->postVars('link');
	$target=$this->postVars('target');
	$footer=$this->postVars('footer');
	
	if($useexternal != 1) {
		$row = $this->getContentById($c_id);
		foreach($row as $row)
		{
		 $link = $row["c_link"];
		}
	}
	$sql="insert into content_frontmenu (`cfm_name`,`cfm_link`,`cfm_father`,`cfmi_id`,`c_id`, `cfm_target`, `cfm_useexternal`, `cfm_showfooter`) values ('$name','$link','$father','$img', '$c_id', '$target', '$useexternal','$footer')";
	$this->Execute($sql);
   }
   
   public function editFrontMenu(){
    $id=$this->postVars('id');
	$name=$this->postVars('name');
	$c_id=$this->postVars('c_id');
	$father=$this->postVars('father');
	$img=$this->postVars('img');
	$useexternal=$this->postVars('useexternal');
	$link=$this->postVars('link');
	$target=$this->postVars('target');
	$footer=$this->postVars('footer');
	
	if($useexternal != 1) {
		$row = $this->getContentById($c_id);
		foreach($row as $row)
		{
		 $link = $row["c_link"];
		}
	}
	
        $sql="update content_frontmenu set cfm_name='$name', cfm_link='$link', cfm_father='$father', cfmi_id='$img', c_id='$c_id', `cfm_target`='$target', `cfm_useexternal`='$useexternal', `cfm_showfooter`='$footer' where cfm_id='$id'";
	$this->Execute($sql);
   }
   
   public function delFrontMenu(){
    $id=$this->getVars('id');
	
	$sql="delete from content_frontmenu where cfm_id='$id'";
	$this->Execute($sql);
   }
   
   public function getFrontMenu(){
    $sql="select * from content_frontmenu";
	return $this->ExecuteS($sql);
   }
   
   public function getFrontMenuById($id){
    $sql="select * from content_frontmenu where cfm_id='$id'";
	return $this->ExecuteS($sql);
   }
   
   public function getFrontMenuByFather($father){
    $sql="select * from content_frontmenu where cfm_father='$father' order by cfm_id asc";
	return $this->ExecuteS($sql);
   } 
   
   public function getFrontMenuLimiter($limiter=0){
    $sql="select * from content_frontmenu";
	$row=$this->ExecuteS($sql);
	$i=1;
	if(count($row)>0){
	 foreach($row as $row){
	 	$i++;
	 }
	}
	
	if($i<=$limiter){
	 return true;
	}else{
	 return false;	
	}
	
   }
   
   public function getFrontMenuFooter(){
    $sql="select * from content_frontmenu where cfm_showfooter='1'";
	return $this->ExecuteS($sql);
   }
   
   //front Menu Images
   public function newFrontMenuImage(){
   	$name=$this->postVars('name');
	$awe=$this->postVars('awe');
	$useawe=$this->postVars('useawe');
	
	$sql="insert into content_frontmenuimages (`cfmi_name`, `cfmi_awe`, `cfmi_useawe`) values ('$name','$awe','$useawe')";
	$this->Execute($sql);
	
	$id=$this->getLastID();
	
	$this->uploadImgMenu($id);
	
   }
   
   public function uploadImgMenu($id){
		$ok=$this->upLoadFileProccess('img', 'modules/content/imgMenu');
		
		if($ok){
		 $sql="update content_frontmenuimages set cfmi_img='".$this->fname."' where cfmi_id='$id'";
		 $this->Execute($sql);
		}
   }
   
   public function editFrontMenuImage(){
   	$id=$this->postVars('id');
	$name=$this->postVars('name');
	$awe=$this->postVars('awe');
	$useawe=$this->postVars('useawe');
	
	$sql="update content_frontmenuimages set `cfmi_name`='$name', `cfmi_awe`='$awe', `cfmi_useawe`='$useawe' where cfmi_id='$id'";
	$this->Execute($sql);
	
	$this->uploadImgMenu($id);
	
   }
   
   public function delFrontMenuImage(){
   	$id=$this->getVars('id');
	
	$sql="delete from content_frontmenuimages where cfmi_id='$id'";
	$this->Execute($sql);
	
   }
   
   public function getFrontMenuImage(){
	
	$sql='select * from content_frontmenuimages';
	return $this->ExecuteS($sql);
	
   }
   
   public function getFrontMenuImageById($id){
	
	$sql="select * from content_frontmenuimages where cfmi_id='$id'";
	return $this->ExecuteS($sql);
	
   }
   
   //Setup Content methods
   public function editContentSetup($id, $value)
   {
    $sql = "update content_setup set cs_value='$value' where cs_id='$id'";
	$this->Execute($sql);
   }
   
   public function getContentSetupById($id)
   {
     $sql="select * from content_setup where cs_id='$id'";
	 return $this->ExecuteS($sql);
   }
   
   public function getContentSetupValue($id)
   {
       $row = $this->getContentSetupById($id);
       if(count($row)>0)
       {
           foreach($row as $row)
           {
               return $row['cs_value'];
           }
       }
   }
   
   public function getContentSetup()
   {
     $sql="select * from content_setup";
	 return $this->ExecuteS($sql);
   }
   
   //Miselanius
    public static function encrypt ($string) {
     return base64_encode(mcrypt_encrypt(MCRYPT_RIJNDAEL_256, md5(Content::$Key),
     $string, MCRYPT_MODE_CBC, md5(md5(Content::$Key))));
  }

  public static function decrypt ($string) {
     return rtrim(mcrypt_decrypt(MCRYPT_RIJNDAEL_256, md5(Content::$Key), 
     base64_decode($string), MCRYPT_MODE_CBC, md5(md5(Content::$Key))), "\0");
  }
   
   //Installers
   public function Checker(){
    return $this->chkTables('content');
   }
   
   public function install(){
    $sql='CREATE TABLE `content` (
  `c_id` int(11) NOT NULL auto_increment,
  `c_name` varchar(255) NOT NULL,
  `c_link` varchar(255) NOT NULL,
  `c_isdelitable` varchar(255) NOT NULL,
  `c_hitcounter` varchar(255) NOT NULL,
  `c_content` longtext NOT NULL,
  `c_lang` varchar(255) NOT NULL,
  PRIMARY KEY  (`c_id`)
) ENGINE=MyISAM  DEFAULT CHARSET=utf8 AUTO_INCREMENT=1 ;';
	
	$this->Execute($sql);
	
	$sql='CREATE TABLE `content_frontmenu` (
  `cfm_id` int(11) NOT NULL auto_increment,
  `cfm_name` varchar(255) NOT NULL,
  `cfm_link` varchar (255) NOT NULL,
  `cfm_father` varchar (255) NOT NULL,
  `cfm_target` varchar (255) NOT NULL,
  `cfm_useexternal` varchar (255) NOT NULL,
  `c_id` int (11) NOT NULL,
  PRIMARY KEY  (`cfm_id`)
) ENGINE=MyISAM  DEFAULT CHARSET=utf8 AUTO_INCREMENT=1 ;';
$this->Execute($sql);
	
	$sql='CREATE TABLE `content_frontmenuimages` (
  `cfmi_id` int(11) NOT NULL AUTO_INCREMENT,
  `cfmi_name` varchar(255) NULL,
  `cfmi_img` varchar(255) NOT NULL,
  `cfmi_awe` varchar(255) NOT NULL,
  `cfmi_useawe` varchar(255) NOT NULL,
  PRIMARY KEY (`cfmi_id`)
) ENGINE=MyISAM;';

	$this->Execute($sql);
	
	$sql="ALTER TABLE `content_frontmenu` ADD `cfmi_id` INT NOT NULL ;";
	$this->Execute($sql);
	
	$sql='CREATE TABLE `content_setup` (
  `cs_id` int(11) NOT NULL AUTO_INCREMENT,
  `cs_name` varchar(255) NULL,
  `cs_value` longtext NOT NULL,
  PRIMARY KEY (`cs_id`)
) ENGINE=MyISAM;';
	$this->Execute($sql);
	
	$sql = "ALTER TABLE `content_setup` ADD `cs_type` VARCHAR(255) NOT NULL AFTER `cs_value`, ADD `cs_tagged` INT NOT NULL AFTER `cs_type`;";
    $this->Execute($sql);
	
    $sql="insert into `content_setup` (`cs_name`, `cs_value`,`cs_type`,`cs_tagged`) value ('Estructura Base', '<p>&nbsp;</p>','textarea','0')";
    $this->Execute($sql);
    
    $sql="insert into `content_setup` (`cs_name`, `cs_value`,`cs_type`,`cs_tagged`) value ('Meta description', '','text','0')";
    $this->Execute($sql);
    
    $sql="insert into `content_setup` (`cs_name`, `cs_value`,`cs_type`,`cs_tagged`) value ('Meta Author', '','text','0')";
    $this->Execute($sql);
    
    $sql="insert into `content_setup` (`cs_name`, `cs_value`,`cs_type`,`cs_tagged`) value ('Meta Keywords', '','text','1')";
    $this->Execute($sql);
    
    $sql = "ALTER TABLE `content_frontmenu` ADD `cfm_showfooter` INT NOT NULL;";
    $this->Execute($sql);
    
    
    
   }
   
   public function uninstall(){
    $sql='DROP TABLE `content`';
	$this->Execute($sql);
	
	$sql='DROP TABLE content_frontmenu';
	$this->Execute($sql);
	
	$sql='DROP TABLE `content_frontmenuimages`';
	$this->Execute($sql);
	
	$sql='DROP TABLE `content_setup`';
	$this->Execute($sql);
   }
  
 };
 
?>
