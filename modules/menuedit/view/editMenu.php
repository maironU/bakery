<?php
 include('modules/menuedit/view/include.php');
 
 $msg=false;
 //Acciones
 if($_POST)
 {
	 $obj->editMenu();
	 $msg=true;
 }
 
 $id=$obj->getVars('id');
 $row=$obj->getMenuById($id);
 foreach($row as $row)
 {
	 $name = $row['m_nom'];
	 $ico = $row['m_icon'];
	 $link = $row['m_link'];
	 $type = $row['m_isModule'];
	 $model = $row['m_modelMod'];
	 $folder = $row['m_folder'];
	 $color = $row['m_color'];
 }
 
 ?>
<div class="widget3">
 <div class="widgetlegend">Editar Menu </div>
 <?php
  if($msg)
  {
  ?>
   <div class="ui-widget">
	<div class="ui-state-highlight ui-corner-all" style="margin-top: 20px; padding: 0 .7em;">
		<p><span class="ui-icon ui-icon-info" style="float: left; margin-right: .3em;"></span>
		<strong>Felicitaciones!</strong> guardado satisfactoriamente.</p>
	</div>
</div>
  <?php
  }
 ?>
<p style="width:100">
	<a href="<?php $_SERVER['PHP_SELF'];?>?p=modules/<?php echo $obj->folder?>/view/index.php" class="btn_normal" style="float:left; margin:5px;">Volver </a>
</p>
 <br /><br /><br />
 <form action="" method="post">
<table>
    <tr>
		<td>Nombre: </td>
		<td><input type="text" name="name" value="<?php echo $name?>" required /></td>
	</tr>
	<tr>
		<td>Icono: </td>
		<td><input type="text" name="ico" placeholder="fa-icon" value="<?php echo $ico?>" required /></td>
	</tr>
	<tr>
		<td>Enlace: </td>
		<td><input type="text" name="link" placeholder="home.php?p=desk.php" value="<?php echo $link?>" required /></td>
	</tr>
	<tr>
		<td>Tipo: </td>
		<td>
		<?php 
					switch($type)
					{
						case 0: echo 'Principal: <input type="radio" name="type" value="0" checked required />
		Modulo: <input type="radio" name="type" value="1" required />
		Componente: <input type="radio" name="type" value="2" required />';
						break;
						case 1: echo 'Principal: <input type="radio" name="type" value="0" required />
		Modulo: <input type="radio" name="type" value="1" checked required />
		Componente: <input type="radio" name="type" value="2" required />';
						break;
						case 2: echo 'Principal: <input type="radio" name="type" value="0" required />
		Modulo: <input type="radio" name="type" value="1" required />
		Componente: <input type="radio" name="type" value="2" checked required />';
						break;
					}
					?>
		</td>
	</tr>
	<tr>
		<td>Modelo: </td>
		<td><input type="text" name="model" value="<?php echo $model?>" required /></td>
	</tr>
	<tr>
		<td>Carpeta: </td>
		<td><input type="text" name="folder" value="<?php echo $folder?>" required /></td>
	</tr>
	<tr>
		<td>Color: </td>
		<td><input type="color" name="color" value="<?php echo $color?>" required /></td>
	</tr>
	<tr>
		<td colspan="2">
		<input type="hidden" name="id" value="<?php echo $id?>" />
		<input type="submit" class="btn_submit" value="Guardar"</td>
	</tr>
	
</table>
</form>
</div>


