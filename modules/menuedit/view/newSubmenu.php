<?php
 include('modules/menuedit/view/include.php');
 
 $msg=false;
 //Acciones
 if($_POST)
 {
	 $obj->newSubmenu();
	 $msg=true;
 }
 
 $id = $obj->getVars('id');
 ?>
<div class="widget3">
 <div class="widgetlegend">Nuevo Sub - Menu </div>
 <?php
  if($msg)
  {
  ?>
   <div class="ui-widget">
	<div class="ui-state-highlight ui-corner-all" style="margin-top: 20px; padding: 0 .7em;">
		<p><span class="ui-icon ui-icon-info" style="float: left; margin-right: .3em;"></span>
		<strong>Felicitaciones!</strong> guardado satisfactoriamente.</p>
	</div>
</div>
  <?php
  }
 ?>
<p style="width:100">
	<a href="<?php $_SERVER['PHP_SELF'];?>?p=modules/<?php echo $obj->folder?>/view/showSubmenu.php&id=<?php echo $id?>" class="btn_normal" style="float:left; margin:5px;">Volver </a>
</p>
 <br /><br /><br />
 <form action="" method="post">
<table>
    <tr>
		<td>Nombre: </td>
		<td><input type="text" name="name" required /></td>
	</tr>
	<tr>
		<td>Enlace: </td>
		<td><input type="text" name="link" placeholder="home.php?p=desk.php" required /></td>
	</tr>
	<tr>
		<td>Padres: </td>
		<td><select name="id_menu" required>
		<option value="">--Seleccionar--</option>
		<?php
		$row = $obj->getMenu();
		if(count($row)>0)
		{
			foreach($row as $row)
			{
				if($id == $row['m_id'])
				{
				?>
				<option value="<?php echo $row['m_id']?>" selected><?php echo $row['m_nom']?></option>
				<?php	
				}
				else
				{
				?>
				<option value="<?php echo $row['m_id']?>"><?php echo $row['m_nom']?></option>
				<?php		
				}
				
			}
		}
		?>
		</select>
		</td>
	</tr>
	<tr>
		<td colspan="2"><input type="submit" class="btn_submit" value="Guardar"</td>
	</tr>
	
</table>
</form>
</div>


