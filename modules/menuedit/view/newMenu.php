<?php
 include('modules/menuedit/view/include.php');
 
 $msg=false;
 //Acciones
 if($_POST)
 {
	 $obj->newMenu();
	 $msg=true;
 }
 ?>
<div class="widget3">
 <div class="widgetlegend">Nuevo Menu </div>
 <?php
  if($msg)
  {
  ?>
   <div class="ui-widget">
	<div class="ui-state-highlight ui-corner-all" style="margin-top: 20px; padding: 0 .7em;">
		<p><span class="ui-icon ui-icon-info" style="float: left; margin-right: .3em;"></span>
		<strong>Felicitaciones!</strong> guardado satisfactoriamente.</p>
	</div>
</div>
  <?php
  }
 ?>
<p style="width:100">
	<a href="<?php $_SERVER['PHP_SELF'];?>?p=modules/<?php echo $obj->folder?>/view/index.php" class="btn_normal" style="float:left; margin:5px;">Volver </a>
</p>
 <br /><br /><br />
 <form action="" method="post">
<table>
    <tr>
		<td>Nombre: </td>
		<td><input type="text" name="name" required /></td>
	</tr>
	<tr>
		<td>Icono: </td>
		<td><input type="text" name="ico" placeholder="fa-icon" required /></td>
	</tr>
	<tr>
		<td>Enlace: </td>
		<td><input type="text" name="link" placeholder="home.php?p=desk.php" required /></td>
	</tr>
	<tr>
		<td>Tipo: </td>
		<td>Principal: <input type="radio" name="type" value="0" required />
		Modulo: <input type="radio" name="type" value="1" required />
		Componente: <input type="radio" name="type" value="2" required />
		</td>
	</tr>
	<tr>
		<td>Modelo: </td>
		<td><input type="text" name="model" required /></td>
	</tr>
	<tr>
		<td>Carpeta: </td>
		<td><input type="text" name="folder" required /></td>
	</tr>
	<tr>
		<td>Color: </td>
		<td><input type="color" name="color" required /></td>
	</tr>
	<tr>
		<td colspan="2"><input type="submit" class="btn_submit" value="Guardar"</td>
	</tr>
	
</table>
</form>
</div>


