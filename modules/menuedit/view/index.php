<?php
 include('modules/menuedit/view/include.php');
 
 
 //Acciones
 if($obj->getVars('actionDel')==true){
	 $obj->delMenu();
  
 }
 ?>
<div class="widget3">
 <div class="widgetlegend">Menu del sistema </div>
 <?php
  if($msg)
  {
  ?>
   <div class="ui-widget">
	<div class="ui-state-highlight ui-corner-all" style="margin-top: 20px; padding: 0 .7em;">
		<p><span class="ui-icon ui-icon-info" style="float: left; margin-right: .3em;"></span>
		<strong>Felicitaciones!</strong> eliminado satisfactoriamente.</p>
	</div>
</div>
  <?php
  }
 ?>
<p style="width:100">
	<a href="<?php $_SERVER['PHP_SELF'];?>?p=modules/<?php echo $obj->folder?>/view/newMenu.php" class="btn_normal" style="float:left; margin:5px;">Nuevo </a>
</p>
 <br /><br /><br />
<table>
	<tr>
		<th>ID</th>
		<th>Nombre</th>
		<th>Icono</th>
		<th>Color</th>
		<th>Enlace</th>
		<th>Tipo</th>
		<th>Modelo</th>
		<th>Carpeta</th>
		<th colspan="3">Acciones</th>
	</tr>
	<?php
	$row=$obj->getMenu();
	if(count($row)>0)
	{
		foreach($row as $row)
		{
			?>
				<tr>
					<td><?php echo $row["m_id"]?></td>
					<td><?php echo $row["m_nom"]?></td>
					<td><i class="fa <?php echo $row["m_icon"]?>"></i></td>
					<td><i class="fa fa-circle" style="color: <?php echo $row["m_color"]?>"></i></td>
					<td><?php echo $row["m_link"]?></td>
					<td><?php 
					switch($row["m_isModule"])
					{
						case 0: echo "Menu Principal";
						break;
						case 1: echo "Modulo";
						break;
						case 2: echo "Componente";
						break;
					}
					?></td>
					<td><?php echo $row["m_modelMod"]?></td>
					<td><?php echo $row["m_folder"]?></td>
					<td>
					<?php
						if($row["m_isModule"] == 2)
						{
							?>
							<a href="<?php $_SERVER['PHP_SELF'];?>?p=modules/<?php echo $obj->folder?>/view/showSubmenu.php&id=<?php echo $row["m_id"]?>" class="btn_normal">Submenu </a>
							<?php
						}
					?>
					</td>
					<td><a href="<?php $_SERVER['PHP_SELF'];?>?p=modules/<?php echo $obj->folder?>/view/editMenu.php&id=<?php echo $row["m_id"]?>" class="btn_normal">Editar </a></td>
					<td><a href="<?php $_SERVER['PHP_SELF'];?>?p=modules/<?php echo $obj->folder?>/view/index.php&id=<?php echo $row["m_id"]?>&actionDel=true" class="btn_borrar">Eliminar </a></td>
				</tr>
			<?php
		}
	}
	?>
</table>
</div>


