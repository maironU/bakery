<?php
 include('modules/menuedit/view/include.php');
 
 $id = $obj->getVars('id');
 
 //Acciones
 if($obj->getVars('actionDel')==true){
	 $obj->delSubmenu();
  
 }
 ?>
<div class="widget3">
 <div class="widgetlegend">Menu del sistema </div>
 <?php
  if($msg)
  {
  ?>
   <div class="ui-widget">
	<div class="ui-state-highlight ui-corner-all" style="margin-top: 20px; padding: 0 .7em;">
		<p><span class="ui-icon ui-icon-info" style="float: left; margin-right: .3em;"></span>
		<strong>Felicitaciones!</strong> eliminado satisfactoriamente.</p>
	</div>
</div>
  <?php
  }
 ?>
<p style="width:100">
	<a href="<?php $_SERVER['PHP_SELF'];?>?p=modules/<?php echo $obj->folder?>/view/newSubmenu.php&id=<?php echo $id?>" class="btn_normal" style="float:left; margin:5px;">Nuevo </a>
</p>
 <br /><br /><br />
<table>
	<tr>
		<th>ID</th>
		<th>Nombre</th>
		<th>Enlace</th>
		<th>Padre</th>
		<th colspan="2">Acciones</th>
	</tr>
	<?php
	$row=$obj->getSubmenu($id);
	if(count($row)>0)
	{
		foreach($row as $row)
		{
			?>
				<tr>
					<td><?php echo $row["sm_id"]?></td>
					<td><?php echo $row["sm_nom"]?></td>
					<td><?php echo $row["sm_link"]?></td>
					<td><?php 
					$row1 = $obj->getMenuById($id);
					if(count($row1)>0)
					{
						foreach($row1 as $row1)
						{
							echo $row1['sm_nom'];
						}
					}
					?></td>
					<td><a href="<?php $_SERVER['PHP_SELF'];?>?p=modules/<?php echo $obj->folder?>/view/editSubmenu.php&id_sm=<?php echo $row["sm_id"]?>&id=<?php echo $id?>" class="btn_normal">Editar </a></td>
					<td><a href="<?php $_SERVER['PHP_SELF'];?>?p=modules/<?php echo $obj->folder?>/view/index.php&id_sm=<?php echo $row["sm_id"]?>&actionDel=true&id=<?php echo $id?>" class="btn_borrar">Eliminar </a></td>
				</tr>
			<?php
		}
	}
	?>
</table>
</div>


