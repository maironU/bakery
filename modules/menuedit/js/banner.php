<script type="text/javascript" src="js/jquery.js"></script>
<script type="text/javascript" src="js/jquery.cycle.all.js"></script>
<style type="text/css">
  .picsy {    
    padding: 0;  
    margin:  0;
} 
 
.picsy img {   
    top:  0; 
    left: 0 
}
.pager{
	background-color:#FB6733;
	display:block;
	border-radius:5px;	
}
.pager a{
	display:block;
	width:10px;
	height:10px;
	text-decoration:none;
	float:left;
	margin:2px;
	text-align:center;
	color:#FB6733;
	font-size:0px;
	position:relative;
	top: -20px;
	left: 0px;
	z-index:100;
	border-radius:10px;
	background-color:#FB6733;	
}
</style>
<div class="picsy" id="picsy">
 <img src="images/foto01.jpg" width="719" height="398" />
 <img src="images/foto02.jpg" width="719" height="398" />
 <img src="images/foto03.jpg" width="719" height="398" />
 <img src="images/foto04.jpg" width="719" height="398" />
 <img src="images/foto05.jpg" width="719" height="398" />
 
</div>
<script>
		  var x = jQuery.noConflict();
		  x("document").ready(function(){
			 x('#picsy').after('<div class="pager"></div>');  
			  x('#picsy').cycle({ 
      pause: 1,
      pager: ".pager"
    });
			  
			  });
		</script>