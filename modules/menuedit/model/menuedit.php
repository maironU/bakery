<?php
class menuedit extends GeCore{
	
	var $folder = "menuedit";
	//Menu Methods
	public function newMenu()
	{
		$name=$this->postVars('name');
		$ico=$this->postVars('ico');
		$link=$this->postVars('link');
		$type=$this->postVars('type');
		$model=$this->postVars('model');
		$folder=$this->postVars('folder');
		$color=$this->postVars('color');
		
		$sql="INSERT INTO `menu`(`m_nom`, `m_icon`, `m_link`, `m_icon2`, `m_isModule`, `m_modelMod`, `m_folder`, `m_color`) 
		VALUES ('$name','$ico','$link','','$type','$model','$folder','$color')";
		$this->Execute($sql);
	}
	
	public function editMenu()
	{
		$id=$this->postVars('id');
		$name=$this->postVars('name');
		$ico=$this->postVars('ico');
		$link=$this->postVars('link');
		$type=$this->postVars('type');
		$model=$this->postVars('model');
		$folder=$this->postVars('folder');
		$color=$this->postVars('color');
		
		$sql="UPDATE `menu` SET `m_nom`='$name', `m_icon`='$ico', `m_link`='$link', `m_isModule`='$type', `m_modelMod`='$model', `m_folder`='$folder', `m_color`='$color' WHERE m_id='$id'";
		$this->Execute($sql);
	}
	
	public function delMenu()
	{
		$id=$this->getVars('id');
		
		$sql="DELETE FROM `menu` WHERE m_id='$id'";
		$this->Execute($sql);
	}
	
	public function getMenu()
	{
		$sql="SELECT * FROM `menu`";
		return $this->ExecuteS($sql);
	}
	
	public function getMenuById($id)
	{
		$sql="SELECT * FROM `menu` WHERE m_id='$id'";
		return $this->ExecuteS($sql);
	}
	
	//Submenu
	public function newSubmenu()
	{
		$id_menu = $this->postVars('id_menu');
		$name = $this->postVars('name');
		$link = $this->postVars('link');
		
		$sql="INSERT INTO `submenu`(`sm_nom`, `sm_link`, `m_id`) VALUES ('$name','$link','$id_menu')";
		$this->Execute($sql);
		
	}
	
	public function editSubmenu()
	{
		$id_menu = $this->postVars('id_menu');
		$name = $this->postVars('name');
		$link = $this->postVars('link');
		$id = $this->postVars('id');
		
		$sql="UPDATE `submenu`SET `sm_nom`='$name', `sm_link`='$link', `m_id`='$id_menu' WHERE sm_id='$id'";
		$this->Execute($sql);
	}
	
	public function delSubmenu()
	{
		$id = $this->getVars('id_sm');
		
		$sql="DELETE FROM `submenu` WHERE sm_id='$id'";
		$this->Execute($sql);
	}
	
	public function getSubmenu($id_menu)
	{
		$sql="SELECT * FROM `submenu` WHERE m_id='$id_menu'";
		return $this->ExecuteS($sql);
	}
	
	public function getSubmenuById($id)
	{
		$sql="SELECT * FROM `submenu` WHERE sm_id='$id'";
		return $this->ExecuteS($sql);
	}
	
	//Installers
   public function Checker(){
    
   }
   
   public function install(){
    	
   }
   
   public function uninstall(){
    	
   }
	
};
?>
