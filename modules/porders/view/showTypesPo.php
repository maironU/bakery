<?php
 include('modules/porders/view/includes.php');
 $msg=false;
 if($obj->getVars('actionDel') == true)
 {
	 $obj->delPoType($obj->getVars('id'));
	 $msg=true;
 }
?>

<div class="widget3">
 <div class="widgetlegend">Tipos de Ordenes de compra</div>
 <?php
  if($msg)
  {
  ?>
   <div class="ui-widget">
	<div class="ui-state-highlight ui-corner-all" style="margin-top: 20px; padding: 0 .7em;">
		<p><span class="ui-icon ui-icon-info" style="float: left; margin-right: .3em;"></span>
		<strong>Felicitaciones!</strong> Se ha sido eliminada satisfactoriamente.</p>
	</div>
</div>
  <?php
  }
 ?>
<p style="width:100">
	<a href="<?php $_SERVER['PHP_SELF'];?>?p=modules/porders/view/showSetup.php" class="btn_normal" style="float:left; margin:5px;">Volver </a>
    <a href="<?php $_SERVER['PHP_SELF'];?>?p=modules/porders/view/newTypesPo.php" class="btn_normal" style="float:left; margin:5px;">Nuevo</a>
</p>
 <br />
 <br />
<table>
	<tr>
		<th>ID</th>
		<th>Nombre</th>
		<th colspan="2">Acciones</th>
	</tr>
	<?php
		$row = $obj->getPoType();
		if(count($row)>0)
		{
			foreach($row as $row)
			{
				?>
				<tr>
					<td><?php echo $row['pot_id']?></td>
					<td><?php echo $row['pot_name']?></td>
					<td><a href="<?php $_SERVER['PHP_SELF'];?>?p=modules/porders/view/editTypePo.php&id=<?php echo $row["pot_id"]?>" class="btn_normal">Editar</a></td>
					<td><a href="<?php $_SERVER['PHP_SELF'];?>?p=modules/porders/view/showTypesPo.php&id=<?php echo $row["pot_id"]?>&actionDel=true" class="btn_borrar">Eliminar</a></td>
				</tr>
				<?php
			}
		}
	?>
</table>

</div>

<br />
 
 

