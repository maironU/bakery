<?php
    include('../../../core/config.php');
    include('../../../core/ge.php');
	include('../../../usr/model/User.php');
	//Models
	include('../../crm/model/crm.php');
	include('../../crm/model/customerhistoryModel.php');
	include('../../crm/model/customerModel.php');
	include('../../crm/model/ordersModel.php');
	include('../../crm/model/quoteModel.php');
	include('../../products/model/products.php');
	include('../model/porders.php');
	
	//Controllers
	include('../../crm/controller/customerController.php');
	include('../../crm/controller/orderController.php');
	include('../../crm/controller/quoteController.php');
	
	//Model Objects
	$crmModelObj=new crmScs();
	$customerHistoryModelObj=new customerHistoryModelGe();
	$customerModelObj=new customerModelGe();
	$orderModelObj=new orderModelGe();
	$quoteModelObj=new quoteModelGe();
	$products=new products();
	$products->connect();
	$obj=new porders();
	$obj->connect();
	
	//Controllers Objects
	$customerCtrlObj = new customerControllerGe();
	$orderCtrlObj = new orderControllerGe();
	$quoteCtrlObj = new quoteControllerGe();
	

?>
