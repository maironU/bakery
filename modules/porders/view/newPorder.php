<?php
 include('modules/porders/view/includes.php');
 
 $msg=false;
 
 //Actions
 if($_POST){
	 $descdoc = $obj->postVars('descdoc');
	$id = $obj->newPorder();
	$obj->newPorderDoc($id, $descdoc);
	$row=$obj->getPorderDetailTemp($_SESSION['user_id']);
	if(count($row)>0)
	{
		foreach($row as $row)
		{
			$obj->newPorderDetail($id, $row['id_product'], $row['podt_desc'], $row['podt_price'], $row['podt_qty'], $row['podt_total']);
		}
	}
	
	$obj->flushPorderDetailTemp($_SESSION['user_id']);
	$msg=true;
 }
 
 $action=$obj->getVars('action');
 if($action=='DelPoDetailTemp'){
 	$obj->delPorderDetailTemp($obj->getVars('id_item'));
 }
 
 
 
?>
<link href="modules/porders/css/smart_wizard_vertical.css" rel="stylesheet" type="text/css">
<!--<script type="text/javascript" src="modules/crm/js/jquery-2.0.0.min.js"></script>-->
<script type="text/javascript" src="modules/porders/js/jquery.smartWizard.js"></script>
<script type="text/javascript" src="modules/porders/js/jspordersFunctions.js"></script>
<script type="text/javascript">
   
    $(document).ready(function(){
    	// Smart Wizard	
  		$('#wizard').smartWizard({transitionEffect:'slide'});
     
		});
</script>
 <div class="widget3">
 <div class="widgetlegend">Nueva Orden de Compra </div>
 <?php
  if($msg)
  {
  ?>
   <div class="ui-widget">
	<div class="ui-state-highlight ui-corner-all" style="margin-top: 20px; padding: 0 .7em;">
		<p><span class="ui-icon ui-icon-info" style="float: left; margin-right: .3em;"></span>
		<strong>Felicitaciones!</strong> Se ha creado satisfactoriamente.</p>
	</div>
</div>
  <?php
  }
 ?>
<p style="width:100">
	<a href="<?php $_SERVER['PHP_SELF'];?>?p=modules/porders/view/index.php" class="btn_normal" style="float:left; margin:5px;">Volver </a>
</p>
 <br><br><br>
<form name="frmNewPo" method="post" action="" enctype="multipart/form-data"> 
<table border="0">
	<tr>
		<td>
			<!-- Tabs -->
  			<div id="wizard" class="swMain">
  			<ul>
  				<!--<li><a href="#step-0">
                <label class="stepNumber">0</label>
                <span class="stepDesc">
                   Paso 0<br />
                   <small>Seleccione el convenio</small>
                </span></li>-->
				<li><a href="#step-1">
                <label class="stepNumber">1</label>
                <span class="stepDesc">
                   Paso 1<br />
                   <small>Seleccione los Items de la Orden de Compra</small>
                </span>
            </a></li>
  				<li><a href="#step-2">
                <label class="stepNumber">2</label>
                <span class="stepDesc">
                   Paso 2<br />
                   <small>Ingrese los detalles de la orden de compra</small>
                </span>
            </a></li>
  				<li><a href="#step-3">
                <label class="stepNumber">3</label>
                <span class="stepDesc">
                   Paso 3<br />
                   <small>Totales y Observaciones</small>
                </span>                   
             </a></li>
  				<li><a href="#step-4">
                <label class="stepNumber">4</label>
                <span class="stepDesc">
                   Paso 4<br />
                   <small>Documentos</small>
                </span>                   
            </a></li>
			<li><a href="#step-5">
                <label class="stepNumber">5</label>
                <span class="stepDesc">
                   Paso 5<br />
                   <small>Otros detalles y creacion</small>
                </span>                   
            </a></li>
  			</ul>
  			<!--<div id="step-0">	
            <h2 class="StepTitle">Paso 1 Seleccione el convenio</h2>
			<p><?php include('newpordersStep0.php');?></p>
                     			
        </div>-->
			<div id="step-1">	
            <h2 class="StepTitle">Paso 1 Seleccione los Items de la Orden de Compra</h2>
			<!--<iframe frameborder="0" src="modules/crm/view/newOrderStep1.php" allowtransparency="yes" scrolling="auto" width="1000" height="1000"></iframe> -->
             <p><?php include('newPordersStep1.php');?></p>        			
        </div>
  			<div id="step-2">
            <h2 class="StepTitle">Step 2 Ingrese los detalles del anticipo</h2>	
            <p><?php include('newPordersStep2.php');?></p>      
        </div>                      
  			<div id="step-3">
            <h2 class="StepTitle">Step 3 Totales y Observaciones</h2>	
            <p><?php include('newPordersStep3.php');?></p>               				          
        </div>
  			<div id="step-4">
            <h2 class="StepTitle">Step 4 Documentos</h2>	
             <p><?php include('newPordersStep4.php');?></p>            			
        </div>
		<div id="step-5">
            <h2 class="StepTitle">Step 5 Otros detalles y creacion</h2>	
             <p><?php include('newPordersStep5.php');?></p>            			
        </div>
  		</div>
			<!-- End SmartWizard Content -->  
		</td>
	</tr>
</table>
</form>
</div>

