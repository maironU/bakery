<?php
 include('modules/porders/view/includes.php');
$msg=false;
if($_POST)
{
	$obj->editProvider();
	$msg=true;
}

$id = $obj->getVars('id');
$row = $obj->getProviderById($id);
foreach($row as $row)
{
	$name = $row['popr_name'];
	$img = $row['popr_img'];
	$type = $row['popr_type'];
	$phone = $row['popr_phone'];
	$addr = $row['popr_addr'];
	$contactname = $row['popr_contactname'];
	$contactemail = $row['popr_contactemail'];
	$contactphone = $row['popr_contactphone'];
	$identification = $row['popr_identification'];
	$paylimit = $row['popr_limitpay'];
}
?>
<div class="widget3">
 <div class="widgetlegend">Editar Proveedor</div>
 <?php
  if($msg)
  {
  ?>
   <div class="ui-widget">
	<div class="ui-state-highlight ui-corner-all" style="margin-top: 20px; padding: 0 .7em;">
		<p><span class="ui-icon ui-icon-info" style="float: left; margin-right: .3em;"></span>
		<strong>Felicitaciones!</strong> Se ha guardado satisfactoriamente.</p>
	</div>
</div>
  <?php
  }
 ?>
<p style="width:100">
	<a href="<?php $_SERVER['PHP_SELF'];?>?p=modules/porders/view/showProviders.php" class="btn_normal" style="float:left; margin:5px;">Volver </a>
</p>
 <br />
 <br />
 <form action="" method="post" enctype="multipart/form-data">
	<table>
		<tr>
			<td><strong>Nombre:</strong><br>
			<input type="text" name="name" value="<?php echo $name?>" required />
			</td>
		</tr>
		<tr>
			<td><strong>Identificacion:</strong><br>
			<input type="text" name="identification" value="<?php echo $identification?>" required />
			</td>
		</tr>
		<tr>
			<td><strong>Imagen:</strong><br>
			<input type="file" name="arch" /><br />
			<img src="modules/porders/provImg/<?php echo $img?>" />
			</td>
		</tr>
		<tr>
			<td><strong>Tipo:</strong><br>
			<?php
				if($type == 'Natural')
				{
					?>
					Persona Natural: <input type="radio" name="type" value="Natural" required checked />
					Persona Juridica: <input type="radio" name="type" value="Juridica" required />
					<?php
				}
				elseif($type == 'Juridica')
				{
					?>
					Persona Natural: <input type="radio" name="type" value="Natural" required />
					Persona Juridica: <input type="radio" name="type" value="Juridica" required checked />
					<?php
				}
				else
				{
					?>
					Persona Natural: <input type="radio" name="type" value="Natural" required />
					Persona Juridica: <input type="radio" name="type" value="Juridica" required />
					<?php
				}
			?>
			
			</td>
		</tr>
		<tr>
			<td><strong>Direccion:</strong><br>
			<input type="text" name="addr" value="<?php echo $addr?>" required />
			</td>
		</tr>
		<tr>
			<td><strong>Telefono:</strong><br>
			<input type="text" name="phone" value="<?php echo $phone?>" required />
			</td>
		</tr>
		<tr>
			<td><strong>Contacto:</strong><br>
			<input type="text" name="contactname" value="<?php echo $contactname?>" required />
			</td>
		</tr>
		<tr>
			<td><strong>Telefono Contacto:</strong><br>
			<input type="text" name="contactphone"  value="<?php echo $contactphone?>" required />
			</td>
		</tr>
		<tr>
			<td><strong>Email Contacto:</strong><br>
			<input type="text" name="contactemail" value="<?php echo $contactemail?>" required />
			</td>
		</tr>
		<tr>
			<td><strong>Plazo de pago:</strong><br>
			<input type="text" name="paylimit" value="<?php echo $paylimit?>" required />
			</td>
		</tr>
		<tr>
			<td>
			<input type="hidden" name="id" value="<?php echo $id?>" />
			<input type="submit" value="Guardar" class="btn_submit" />
			</td>
		</tr>
	</table>
 </form>


</div>

<br />
 
 

