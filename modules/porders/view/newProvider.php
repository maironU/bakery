<?php
 include('modules/porders/view/includes.php');
$msg=false;
if($_POST)
{
	$obj->newProvider();
	$msg=true;
}	
?>
<div class="widget3">
 <div class="widgetlegend">Nuevo Proveedor</div>
 <?php
  if($msg)
  {
  ?>
   <div class="ui-widget">
	<div class="ui-state-highlight ui-corner-all" style="margin-top: 20px; padding: 0 .7em;">
		<p><span class="ui-icon ui-icon-info" style="float: left; margin-right: .3em;"></span>
		<strong>Felicitaciones!</strong> Se ha guardado satisfactoriamente.</p>
	</div>
</div>
  <?php
  }
 ?>
<p style="width:100">
	<a href="<?php $_SERVER['PHP_SELF'];?>?p=modules/porders/view/showProviders.php" class="btn_normal" style="float:left; margin:5px;">Volver </a>
</p>
 <br />
 <br />
 <form action="" method="post" enctype="multipart/form-data">
	<table>
		<tr>
			<td><strong>Nombre:</strong><br>
			<input type="text" name="name" required />
			</td>
		</tr>
		<tr>
			<td><strong>Identificacion:</strong><br>
			<input type="text" name="identification" required />
			</td>
		</tr>
		<tr>
			<td><strong>Imagen:</strong><br>
			<input type="file" name="arch" />
			</td>
		</tr>
		<tr>
			<td><strong>Tipo:</strong><br>
			Persona Natural: <input type="radio" name="type" value="Natural" required />
			Persona Juridica: <input type="radio" name="type" value="Juridica" required />
			</td>
		</tr>
		<tr>
			<td><strong>Direccion:</strong><br>
			<input type="text" name="addr" required />
			</td>
		</tr>
		<tr>
			<td><strong>Telefono:</strong><br>
			<input type="text" name="phone" required />
			</td>
		</tr>
		<tr>
			<td><strong>Contacto:</strong><br>
			<input type="text" name="contactname" required />
			</td>
		</tr>
		<tr>
			<td><strong>Telefono Contacto:</strong><br>
			<input type="text" name="contactphone" required />
			</td>
		</tr>
		<tr>
			<td><strong>Email Contacto:</strong><br>
			<input type="text" name="contactemail" required />
			</td>
		</tr>
		<tr>
			<td><strong>Plazo de pago:</strong><br>
			<input type="text" name="paylimit" required />
			</td>
		</tr>
		<tr>
			<td>
			<input type="submit" value="Guardar" class="btn_submit" />
			</td>
		</tr>
	</table>
 </form>


</div>

<br />
 
 

