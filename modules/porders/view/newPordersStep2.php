<table width="682" height="121" border="0">
  <tr>
    <td width="226" valign="top"><strong>*Compa&ntilde;&iacute;a:</strong></td>
    <td width="501" valign="top"><label>
      <select name="company" id="company" required>
	   <?php
	    $row=$crmModelObj->getCompany();
		if(count($row)>0){
			foreach($row as $row){
			 ?>
			 <option value="<?php echo $row["e_id"]; ?>"><?php echo $row["e_nom"]; ?></option>
			 <?php
			}
		}
	   ?>
      </select>
    </label></td>
  </tr>
  <tr>
    <td valign="top"><strong>*Termino de pago: </strong></td>
    <td valign="top"><select name="term" id="term" required>
	<?php
	    $row=$crmModelObj->getTerm();
		if(count($row)>0){
			foreach($row as $row){
			 ?>
			 <option value="<?php echo $row["tp_id"]; ?>"><?php echo $row["tp_nom"]; ?></option>
			 <?php
			}
		}
	   ?>
    </select></td>
  </tr>
  <tr>
    <td valign="top"><strong>*Proveedor:</strong></td>
    <td valign="top">
	<select name="id_provider" required>
	<?php
	 $row=$obj->getProviderByEnable(1);
	 foreach($row as $row){
	 ?>
	 <option value="<?php echo $row["popr_id"]?>"><?php echo utf8_encode($row["popr_name"])?></option>
	 <?php
	 }
	?>
    </select>
    </td>
  </tr>
  <tr>
    <td valign="top"><strong>*Descripci&oacute;n breve (Esta informaci&oacute;n aparecera en el detalle de la orden): </strong></td>
    <td valign="top"><label>
      <textarea name="desc" cols="40" rows="8" id="desc" required></textarea>
    </label></td>
  </tr>
</table>
<p>(*) Campos obligatorios</p>
<?php
 
?>
