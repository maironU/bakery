<?php
  session_start();
  include('includes_self.php');
  
  $id = $obj->getVars('id');
?>
<link href="../../../css/scsStyle.css" rel="stylesheet" type="text/css" />
<link href="../../../css/smoothness/jquery-ui-1.9.2.custom.css" rel="stylesheet">
<link href="../../../css/smoothness/jquery-ui-1.9.2.custom.min.css" rel="stylesheet" type="text/css" />
<script src="../../../js/jquery-1.8.3.js"></script>
<script src="../../../js/jquery-ui-1.9.2.custom.js"></script>
<script src="../../../js/jquery-ui-1.9.2.custom.js"></script>
<script src="../js/jsAdvancesFunctions.js"></script>
<?php
if($_POST)
  {
	$id = $obj->postVars('id');
	$approval = $obj->postVars('approval');
	$desc = $obj->postVars('desc');
  	$res = $obj->setApproval($id, $_SESSION['user_id'], $approval, $desc);
	switch($res)
	{
		case '2': echo '<div class="ui-widget">
	<div class="ui-state-error ui-corner-all" style="padding: 0 .7em;">
		<p><span class="ui-icon ui-icon-alert" style="float: left; margin-right: .3em;"></span>
		<strong>No Aprobado:</strong> La orden no ha sido aprobada.</p>
	</div>
</div>'; 
		break;
		case '1':
		echo '<div class="ui-widget">
	<div class="ui-state-highlight ui-corner-all" style="margin-top: 20px; padding: 0 .7em;">
		<p><span class="ui-icon ui-icon-info" style="float: left; margin-right: .3em;"></span>
		<strong>Aprobada!</strong> La orden de compra ha sido aprobada.</p>
	</div>
</div>';
		break;
	}
  }

?>
<div class="widget3">
 <div class="widgetlegend">Autorizar Orden de Compra </div>
  <table width="100%" height="48" border="0">
	<tr>
		<td>
			<form action="" method="post" name="">
				<table width="100%">
					<tr>
						<td><strong>Autorizar?</strong></td>
						<td>SI: <input type="radio" name="approval" value="1" required />
						NO: <input type="radio" name="approval" value="2" required />
						</td>
					</tr>
					<tr>
						<td><strong>Motivo de aprobaci&oacute;n o Rechazo</strong></td>
						<td><textarea cols="40" rows="6" name="desc" required></textarea></td>
					</tr>
					<tr>
						<td colspan="2">
							<input type="hidden" name="id" value="<?php echo $id?>" />
							<input type="submit" value="Autorizar" class="btn_submit" />
						</td>
					</tr>
				</table>
			</form>
		</td>
	</tr>
  </table>
  <br />
</div>
