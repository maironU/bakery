<?php
  include('../../../core/config.php');
  include('../../../core/ge.php');
  include('../model/advances.php');
  include('../../products/model/products.php');
  
  $obj = new advances();
  $obj->connect();
  
  $products=new products();
  $products->connect();
  
  if($_POST){
	$obj->newCalcItem2();
	
  }
  
  $id = $obj->getVars('id');
  
?>
<link href="../../../css/scsStyle.css" rel="stylesheet" type="text/css" />
<link href="../../../css/smoothness/jquery-ui-1.9.2.custom.css" rel="stylesheet">
<link href="../../../css/smoothness/jquery-ui-1.9.2.custom.min.css" rel="stylesheet" type="text/css" />
<script src="../../../js/jquery-1.8.3.js"></script>
<script src="../../../js/jquery-ui-1.9.2.custom.js"></script>
<script src="../../../js/jquery-ui-1.9.2.custom.js"></script>
<script src="../js/jsAdvancesFunctions.js"></script>

<div class="widget3">
 <div class="widgetlegend">Nuevo Item </div>
  <form action="" method="post" name="form1">
    <table width="703" height="88" border="0" align="center">
      <tr>
        <td>P.A :</td>
        <td><label>
          <input name="pa" type="text" id="pa" size="40" />
        </label></td>
      </tr>
      <tr>
        <td>FOB :</td>
        <td><label>
          <input name="fob" type="text" id="fob" size="40" />
        </label></td>
      </tr>
      <tr>
        <td>FLETES :</td>
        <td><label>
          <input name="flete" type="text" id="flete" size="40" />
        </label></td>
      </tr>
      <tr>
        <td>SEGURO :</td>
        <td><label>
          <input name="seg" type="text" id="seg" size="40" />
        </label></td>
      </tr>
      <tr>
        <td>OTROS :</td>
        <td><label>
          <input name="otro" type="text" id="otro" size="40" />
        </label></td>
      </tr>
      <tr>
        <td>% CANCELACION :</td>
        <td><label>
          <input name="cancel" type="text" id="cancel" size="40" />
        </label></td>
      </tr>
      <tr>
        <td>% IVA :</td>
        <td><label>
          <input name="iva" type="text" id="iva" size="40" />
        </label></td>
      </tr>
      <tr>
        <td>TRM:</td>
        <td><input name="trm" type="text" id="trm" value="1" size="40" /></td>
      </tr>
      <tr>
        <td>Cantidad:</td>
        <td><input name="cant" type="hidden" id="cant" value="1" size="40" /></td>
      </tr>
      <tr>
        <td>Categoria:</td>
        <td>
        	<select name="id_category">
        	 <option value="">--Seleccionar--</option>
        	 <?php
        	 	$row = $products->getCategories();
        	 	if(count($row)>0)
        	 	{
        	 		foreach($row as $row)
        	 		{
        	 			?>
        	 			<option value="<?php echo $row["cat_id"]?>"><?php echo $row["cat_name"]?></option>
        	 			<?php
        	 		}
        	 	}
        	 ?>
        	</select>
        </td>
      </tr>
      <tr>
        <td width="136"><input type="hidden" name="id" value="<?php echo $id?>" /></td>
        <td width="238"><a href="#" class="btn_normal" onclick="document.form1.submit();">Agregar</a></td>
      </tr>
    </table>
  </form>
 <br />
 
</div>
