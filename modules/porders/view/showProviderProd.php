<?php
 include('modules/porders/view/includes.php');
$msg=false;
if($_POST)
{
	$obj->editProvider();
	$msg=true;
}

$id = $obj->getVars('id');
$row = $obj->getProviderById($id);
foreach($row as $row)
{
	$name = $row['popr_name'];
	$img = $row['popr_img'];
	$type = $row['popr_type'];
	$phone = $row['popr_phone'];
	$addr = $row['popr_addr'];
	$contactname = $row['popr_contactname'];
	$contactemail = $row['popr_contactemail'];
	$contactphone = $row['popr_contactphone'];
	$identification = $row['popr_identification'];
	$paylimit = $row['popr_limitpay'];
}
?>
<div class="widget3">
 <div class="widgetlegend">Productos Asociados a Proveedor</div>
 <?php
  if($msg)
  {
  ?>
   <div class="ui-widget">
	<div class="ui-state-highlight ui-corner-all" style="margin-top: 20px; padding: 0 .7em;">
		<p><span class="ui-icon ui-icon-info" style="float: left; margin-right: .3em;"></span>
		<strong>Felicitaciones!</strong> Se ha guardado satisfactoriamente.</p>
	</div>
</div>
  <?php
  }
 ?>
<p style="width:100">
	<a href="<?php $_SERVER['PHP_SELF'];?>?p=modules/porders/view/showProviders.php" class="btn_normal" style="float:left; margin:5px;">Volver </a>
</p>
 <br />
 <br />
 <table>
     <tr>
         <td><h1><?php echo $name?></h1></td>
     </tr>
     <tr>
         <td>Productos asociados a este proveedor</td>
     </tr>
 </table>
 <table width="803" height="48" border="0">
  <tr>
    <th width="30">ID</th>
    <th width="30">Imagen</th>
    <th width="213">Nombre</th>
    <th width="125">Descripcion</th>
	<th width="125">Referencia</th>
	<th width="125">Precio Costo</th>
  </tr>
  <?php
   $row=$obj->getProviderProd($id);
   
   if(count($row)>0){
 	foreach($row as $row){
  ?>
  <tr>
    <td><?php echo $row["pro_id"];?></td>
    <td><img src="modules/products/imagesProd/<?php echo $row['pro_image']?>" width="50" height="50" /></td>
    <td><?php echo $row["pro_name"];?></td>
    <td><?php echo $row["pro_sdesc"];?></td>
	<td><?php echo $row["pro_reference"];?></td>
	<td><?php echo $row["pro_costprice"];?></td>
  </tr>
  <?php
   }
  } 
  ?>
</table>



</div>

<br />
 
 

