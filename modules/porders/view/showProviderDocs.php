<?php
 include('modules/porders/view/includes.php');
 
 
 if($_POST)
 {
	 $action = $obj->postVars('action');
	 $id = $obj->postVars('id');
	 $id_doc = $obj->postVars('id_doc');
	 $name = $obj->postVars('name');
	 $desc = $obj->postVars('desc');
	 if($action == 'newdoc')
	 {
		  $obj->newProviderDoc($id, $name, $desc);
	 }
	 
	 if($action == 'editdoc')
	 {
		  $obj->editProviderDoc($id_doc, $id, $name, $desc);
	 }
	
 }
 
 if($obj->getVars('actionDel') == true)
 {
	 $obj->delProviderDoc($obj->getVars('id_doc'));
 }
 
 $id = $obj->getVars('id');
 
?>
<!-- Add mousewheel plugin (this is optional) -->
<script type="text/javascript" src="modules/porders/js/jquery.mousewheel-3.0.6.pack.js"></script>
<!-- Add fancyBox main JS and CSS files -->
<script type="text/javascript" src="modules/porders/js/jquery.fancybox.js?v=2.1.4"></script>
<link rel="stylesheet" type="text/css" href="modules/porders/css/jquery.fancybox.css?v=2.1.4" media="screen" />
<div class="widget3">
 <div class="widgetlegend">Documentos de proveedor</div>
 <?php
  if($msg)
  {
  ?>
   <div class="ui-widget">
	<div class="ui-state-highlight ui-corner-all" style="margin-top: 20px; padding: 0 .7em;">
		<p><span class="ui-icon ui-icon-info" style="float: left; margin-right: .3em;"></span>
		<strong>Felicitaciones!</strong> Se ha sido eliminada satisfactoriamente.</p>
	</div>
</div>
  <?php
  }
 ?>
<p style="width:100">
    <a href="<?php $_SERVER['PHP_SELF'];?>?p=modules/porders/view/showProviders.php" class="btn_normal" style="float:left; margin:5px;">Volver </a>
</p>
<br>
<br>
<form action="" method="post" enctype="multipart/form-data">
<table>
	<tr>
		<td>Nombre Documento</td>
		<td><input type="text" name="name" required /></td>
	</tr>
	<tr>
		<td>Descripcion Documento</td>
		<td><textarea cols="40" rows="6" name="desc"></textarea></td>
	</tr>
	<tr>
		<td>Archivo</td>
		<td><input type="file" name="arch" required /></td>
	</tr>
	<tr>
		<td colspan="2">
		<input type="hidden" name="id" value="<?php echo $id?>" />
		<input type="hidden" name="action" value="newdoc" />
		<input type="submit" value="Agregar" class="btn_submit" /></td>
	</tr>
</table>
</form>
<br>
<br>
 
<table width="803" height="48" border="0">
	<tr>
		<th>Nombre</th>
		<th>Descripcion</th>
		<th>Archivo</th>
		<th colspan="4">Acciones</th>
	</tr>
  <?php
	$i=1;
	$row = $obj->getProviderDoc($id);
	if(count($row)>0)
	{
		foreach($row as $row)
		{
			?>
			<form action="" method="post" enctype="multipart/form-data" name="frm<?php echo $i?>">
			<tr>
				<td><input type="text" name="name" value="<?php echo $row['poprd_name'];?>" /></td>
				<td><textarea cols="30" rows="4" name="desc"><?php echo $row['poprd_desc']?></textarea></td>
				<td><input type="file" name="arch" /></td>
				<td><a href="javascript:;" onClick="document.frm<?php echo $i?>.submit();" class="btn_1">Guardar</a></td>
				<td><a href="modules/porders/provDoc/<?php echo $row['popr_doc']?>" class="btn_3" target="_blank">Ver</a></td>
				<td><a href="<?php $_SERVER['PHP_SELF'];?>?p=modules/porders/view/showProviderDocs.php&id=<?php echo $id?>&id_doc=<?php echo $row['poprd_id']?>&actionDel=true" class="btn_2">Eliminar</a></td>
			</tr>
			<input type="hidden" name="id_doc" value="<?php echo $row['poprd_id']?>" />
			<input type="hidden" name="id" value="<?php echo $id?>" />
			<input type="hidden" name="action" value="editdoc" />
			</form>
			<?php
			$i++;
		}
	}
  ?>
</table>

</div>
<script>
 function showInfo(id,site)
 {
	$.fancybox.open({
					href : site+'?id='+id,
					type : 'iframe',
					padding:5
				}); 
 }
</script>
