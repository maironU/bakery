<?php
 include('modules/porders/view/includes.php');
?>
<!-- Add mousewheel plugin (this is optional) -->
<script type="text/javascript" src="modules/porders/js/jquery.mousewheel-3.0.6.pack.js"></script>
<!-- Add fancyBox main JS and CSS files -->
<script type="text/javascript" src="modules/porders/js/jquery.fancybox.js?v=2.1.4"></script>
<link rel="stylesheet" type="text/css" href="modules/porders/css/jquery.fancybox.css?v=2.1.4" media="screen" />
 
<div class="widget3">
 <div class="widgetlegend">Autorizar Ordenes de compra </div>
 <?php
  if($msg)
  {
  ?>
   <div class="ui-widget">
	<div class="ui-state-highlight ui-corner-all" style="margin-top: 20px; padding: 0 .7em;">
		<p><span class="ui-icon ui-icon-info" style="float: left; margin-right: .3em;"></span>
		<strong>Felicitaciones!</strong> Se ha sido eliminada satisfactoriamente.</p>
	</div>
</div>
  <?php
  }
 ?>
<p style="width:100">
    <!--<a href="<?php $_SERVER['PHP_SELF'];?>?p=modules/porders/view/newPorder.php" class="btn_normal" style="float:left; margin:5px;">Nuevo </a>-->
</p>
 
<table width="803" height="48" border="0">
  <tr>
    <th width="30">Track</th>
	<th width="30">Aprobacion</th>
    <th width="142">Fecha Creacion</th>
	<th width="142">Descripcion</th>
	<th width="142">Total</th>
	<th width="142">Proveedor</th>
	<th width="142">Creador por</th>
    <th colspan="5">Acciones</th>
  </tr>
  <?php
   $row=$obj->getPorderByActive();
   
   if(count($row)>0){
 	foreach($row as $row){
  ?>
  <tr>
    <td><?php echo $row["po_track"];?></td>
	<td><?php
		switch($row["po_approval"])
		{
			case '0':
			?>
			<span style="min-wdth: 100px; height: 50px; display: block; padding: 5px; background:#ff0">Sin Aprobar</span>
			<?php
			break;
			case '1':
			?>
			<span style="min-wdth: 100px; height: 50px; display: block; padding: 5px; background:#0f0">Aprobado</span>
			<?php
			break;
			case '2':
			?>
			<span style="min-wdth: 100px; height: 50px; display: block; padding: 5px; background:#f00">No aprobado</span>
			<?php
			break;
		}
	?></td>
	<td><?php echo $row["po_fecha"];?></td>
	<td><?php echo $row["po_desc"];?></td>
	<td>$<?php echo number_format($row["po_total"]);?></td>
	<td><?php 
	 		 $row1 = $obj->getProviderById($row['popr_id']);
			 if(count($row1)>0)
			 {
				 foreach($row1 as $row1)
				 {
					 echo $row1['popr_name'];
				 }
			 }
	?></td>
	<td><?php 
	 		 $user=new User();
			 $user->connect();
			 $user->getUserById($row["u_id"]);
			 echo $user->name;
	?></td>
	<td><a href="javascript:;" onclick="showInfo(<?php echo $row["po_id"];?>,'modules/porders/view/authPorder.php')" class="btn_4">Autorizar</a></td>
	<td>
	<?php 
	if($row["po_approval"] != 1)
	{
		?>
		<a href="<?php $_SERVER['PHP_SELF'];?>?p=modules/porders/view/editPorder.php&id=<?php echo $row["po_id"]?>" class="btn_1">Editar</a>
		<?php
	}
	?>
	</td>
	<td width="24"><a href="javascript:;" class="btn_2" onclick="showInfo(<?php echo $row["po_id"];?>,'modules/porders/view/delPorder.php')">Anular</a></td>
    <td width="25"><a href="javascript:;" class="btn_3" onclick="showInfo(<?php echo $row["po_id"];?>,'modules/porders/view/showPorder.php')">Ver</a></td>
	<td width="25"><a href="<?php $_SERVER['PHP_SELF'];?>?p=modules/porders/view/showPorderDocs.php&id=<?php echo $row["po_id"]?>" class="btn_4">Documentos</a></td>
  </tr>
  <?php
   }
  } 
  ?>
</table>

</div>
<script>
 function showInfo(id,site)
 {
	$.fancybox.open({
					href : site+'?id='+id,
					type : 'iframe',
					padding:5,
					afterClose: function () { // USE THIS IT IS YOUR ANSWER THE KEY WORD IS "afterClose"
                		parent.location.reload(true);
            		}
				}); 
 }
</script>
