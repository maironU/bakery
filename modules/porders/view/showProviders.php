<?php
 include('modules/porders/view/includes.php');
 $msg=false;
 if($obj->getVars('actionDel') == true)
 {
	 $obj->delProvider($obj->getVars('id'));
	 $msg=true;
 }
?>

<div class="widget3">
 <div class="widgetlegend">Proveedores</div>
 <?php
  if($msg)
  {
  ?>
   <div class="ui-widget">
	<div class="ui-state-highlight ui-corner-all" style="margin-top: 20px; padding: 0 .7em;">
		<p><span class="ui-icon ui-icon-info" style="float: left; margin-right: .3em;"></span>
		<strong>Felicitaciones!</strong> Se ha sido eliminado satisfactoriamente.</p>
	</div>
</div>
  <?php
  }
 ?>
<p style="width:100">
	<a href="<?php $_SERVER['PHP_SELF'];?>?p=modules/porders/view/newProvider.php" class="btn_normal" style="float:left; margin:5px;">Nuevo</a>
</p>
 <br />
 <br />
<table>
	<tr>
		<th>ID</th>
		<th>Imagen</th>
		<th>Nombre</th>
		<th>Direccion</th>
		<th>Telefono</th>
		<th>Contacto</th>
		<th>Email</th>
		<th>Estado</th>
		<th colspan="4">Acciones</th>
	</tr>
	<?php
		$row = $obj->getProvider();
		if(count($row)>0)
		{
			foreach($row as $row)
			{
				?>
				<tr>
					<td><?php echo $row['popr_id']?></td>
					<td><img src="modules/porders/provImg/<?php echo $row['popr_img']?>" width="50" height="50" /></td>
					<td><?php echo $row['popr_name']?></td>
					<td><?php echo $row['popr_addr']?></td>
					<td><?php echo $row['popr_phone']?></td>
					<td><?php echo $row['popr_contactname']?></td>
					<td><?php echo $row['popr_contactemail']?></td>
					<td><?php
						switch($row["popr_enable"])
						{
							case '1':
							?>
							<span style="min-wdth: 100px; height: 25px; display: block; padding: 5px; background:#0f0">Autorizado</span>
							<?php
							break;
							case '0':
							?>
							<span style="min-wdth: 100px; height: 25px; display: block; padding: 5px; background:#f00">No Autorizado</span>
							<?php
							break;
						}
					?></td>
					<td><a href="<?php $_SERVER['PHP_SELF'];?>?p=modules/porders/view/editProvider.php&id=<?php echo $row["popr_id"]?>" class="btn_normal">Editar</a></td>
					<td><a href="<?php $_SERVER['PHP_SELF'];?>?p=modules/porders/view/showProviderProd.php&id=<?php echo $row["popr_id"]?>" class="btn_normal">Productos</a></td>
					<td><a href="<?php $_SERVER['PHP_SELF'];?>?p=modules/porders/view/showProviderDocs.php&id=<?php echo $row["popr_id"]?>" class="btn_normal">Documentos</a></td>
					<td><a href="<?php $_SERVER['PHP_SELF'];?>?p=modules/porders/view/showProviders.php&id=<?php echo $row["popr_id"]?>&actionDel=true" class="btn_borrar">Eliminar</a></td>
				</tr>
				<?php
			}
		}
	?>
</table>

</div>

<br />
 
 

