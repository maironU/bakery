CREATE TABLE `porder` (
`po_id` INT NOT NULL AUTO_INCREMENT PRIMARY KEY ,
`po_fecha` DATETIME NOT NULL ,
`po_subto` VARCHAR( 255 ) NOT NULL ,
`po_iva` VARCHAR( 255 ) NOT NULL ,
`po_total` VARCHAR( 255 ) NOT NULL ,
`po_mod_pago` VARCHAR( 255 ) NOT NULL ,
`u_id` INT NOT NULL ,
`e_id` INT NOT NULL ,
`po_trm` VARCHAR( 255 ) NOT NULL ,
`po_track` VARCHAR( 255 ) NOT NULL ,
`po_obs` LONGTEXT NOT NULL ,
`po_tipo` VARCHAR( 255 ) NOT NULL ,
`po_active` VARCHAR( 100 ) NOT NULL ,
`popr_id` INT NOT NULL ,
`po_approval` INT NOT NULL ,
`po_uapproval` INT NOT NULL ,
`po_desc` LONGTEXT NOT NULL ,
`po_approvaldesc` LONGTEXT NOT NULL 
) ENGINE = MYISAM ;

CREATE TABLE `porder_detail_temp` (
`podt_id` INT NOT NULL AUTO_INCREMENT PRIMARY KEY ,
`u_id` INT NOT NULL ,
`id_product` INT NOT NULL ,
`podt_desc` LONGTEXT NOT NULL ,
`podt_price` VARCHAR( 255 ) NOT NULL ,
`podt_qty` VARCHAR( 255 ) NOT NULL ,
`podt_total` VARCHAR( 255 ) NOT NULL
) ENGINE = MYISAM ;

CREATE TABLE `porder_detail` (
`pod_id` INT NOT NULL AUTO_INCREMENT PRIMARY KEY ,
`po_id` INT NOT NULL ,
`id_product` INT NOT NULL ,
`pod_desc` LONGTEXT NOT NULL ,
`pod_price` VARCHAR( 255 ) NOT NULL ,
`pod_qty` VARCHAR( 255 ) NOT NULL ,
`pod_total` VARCHAR( 255 ) NOT NULL
) ENGINE = MYISAM ;

CREATE TABLE `porder_docs` (
`podo_id` INT NOT NULL AUTO_INCREMENT PRIMARY KEY ,
`po_id` INT NOT NULL ,
`podo_desc` LONGTEXT NOT NULL ,
`podo_file` VARCHAR( 255 ) NOT NULL
) ENGINE = MYISAM ;

CREATE TABLE `porder_nullregister` (
`ponr_id` INT NOT NULL AUTO_INCREMENT PRIMARY KEY ,
`po_id` INT NOT NULL ,
`ponr_desc` LONGTEXT NOT NULL ,
`ponr_date` DATETIME NOT NULL ,
`u_id` INT NOT NULL
) ENGINE = MYISAM ;

CREATE TABLE `porder_param` (
`pop_id` INT NOT NULL AUTO_INCREMENT PRIMARY KEY ,
`pop_name` VARCHAR( 255 ) NOT NULL ,
`pop_value` VARCHAR( 255 ) NOT NULL
) ENGINE = MYISAM ;

INSERT INTO `porder_param` (`pop_id`, `pop_name`, `pop_value`) VALUES ('1', 'Track Orden de Compra',1000);

CREATE TABLE `porder_type` (
`pot_id` INT NOT NULL AUTO_INCREMENT PRIMARY KEY ,
`pot_name` VARCHAR( 255 ) NOT NULL 
) ENGINE = MYISAM ;

ALTER TABLE  `user` ADD  `letPoAuth` INT NOT NULL DEFAULT  '0';

CREATE TABLE `porder_provider` (
`popr_id` INT NOT NULL AUTO_INCREMENT PRIMARY KEY ,
`popr_name` VARCHAR( 255 ) NOT NULL ,
`popr_img` VARCHAR( 255 ) NOT NULL ,
`popr_type` VARCHAR( 255 ) NOT NULL ,
`popr_phone` VARCHAR( 255 ) NOT NULL ,
`popr_addr` VARCHAR( 255 ) NOT NULL ,
`popr_enable` VARCHAR( 255 ) NOT NULL ,
`popr_contactname` VARCHAR( 255 ) NOT NULL ,
`popr_contactphone` VARCHAR( 255 ) NOT NULL ,
`popr_contactemail` VARCHAR( 255 ) NOT NULL
) ENGINE = MYISAM ;

CREATE TABLE `porder_providerdocs` (
`poprd_id` INT NOT NULL AUTO_INCREMENT PRIMARY KEY ,
`poprd_name` VARCHAR( 255 ) NOT NULL ,
`poprd_desc` VARCHAR( 255 ) NOT NULL ,
`popr_doc` VARCHAR( 255 ) NOT NULL ,
`popr_id` INT NOT NULL
) ENGINE = MYISAM ;
ALTER TABLE `porder_provider` ADD `popr_identification` VARCHAR(255) NOT NULL AFTER `popr_contactemail`, ADD `popr_limitpay` VARCHAR(255) NOT NULL AFTER `popr_identification`;
