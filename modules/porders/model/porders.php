<?php
 class porders extends GeCore{
	 
	 //Purchase Order methods
	 public function newPorder()
	 {
	 	$subto = $this->postVars('subto');
		$iva = $this->postVars('iva');
		$total = $this->postVars('total');
		$subto = $this->postVars('subto');
		$modpago = $this->postVars('term');
		$subto = $this->postVars('subto');
		$user = $this->postVars('user');
		$eid = $this->postVars('company');
		$trm = $this->postVars('trm');
		$obs = $this->postVars('obs');
		$type = $this->postVars('type');
		$id_provider = $this->postVars('id_provider');
		$desc = $this->postVars('desc');
		
		$today = date('Y-m-d H:i:s');
		$track = $this->getPoParamsById(1);
		
		$sql = "INSERT INTO `porder` (`po_fecha`, `po_subto`, `po_iva`, `po_total`, `po_mod_pago`, `u_id`, `e_id`, `po_trm`, `po_track`, `po_obs`, `po_tipo`, `po_active`, `popr_id`, `po_desc`) VALUES ('$today','$subto','$iva','$subto','$modpago','$user','$eid','$trm','$track','$obs','$type','1','$id_provider','$desc')";
		$this->Execute($sql);
		
		$this->incrementPoParams(1);
		
		return $this->getPorderLastID();
	 
	 }
	 
	 public function getPorderLastID()
	 {
	 	$sql = "SELECT * FROM `porder` ORDER BY po_id DESC LIMIT 0,1";
	 	$row = $this->ExecuteS($sql);
	 	foreach($row as $row)
	 	{
	 		return $row['po_id'];
	 	}
	 }
	 
	 public function editPorder()
	 {
	 	$id = $this->postVars('id');
		$subto = $this->postVars('subto');
		$iva = $this->postVars('iva');
		$total = $this->postVars('total');
		$subto = $this->postVars('subto');
		$modpago = $this->postVars('term');
		$subto = $this->postVars('subto');
		$user = $this->postVars('user');
		$eid = $this->postVars('company');
		$trm = $this->postVars('trm');
		$obs = $this->postVars('obs');
		$type = $this->postVars('type');
		$id_provider = $this->postVars('id_provider');
		$desc = $this->postVars('desc');
		

		
		$sql = "UPDATE `porder` SET `po_subto`='$subto', `po_iva`='$iva', `po_total`='$subto', `po_mod_pago`='$modpago', `u_id`='$user', `e_id`='$eid', `po_trm`='$trm', `po_obs`='$obs', `po_tipo`='$type', `popr_id`='$id_provider', `po_desc`='$desc' WHERE po_id='$id'";
		$this->Execute($sql);
	 
	 }
	 
	 public function delPorder($id)
	 {
		 $sql = "DELETE FROM `porder` WHERE po_id='$id'";
		$this->Execute($sql);
	 } 
	 
	 public function getPorder()
	 {
		 $sql = "SELECT * FROM `porder`";
		return $this->ExecuteS($sql);
	 }
	 
	 public function getPorderById($id)
	 {
		 $sql = "SELECT * FROM `porder` WHERE po_id='$id'";
		return $this->ExecuteS($sql);
	 } 
	 
	 public function getPorderByActive($active = 1)
	 {
		 $sql = "SELECT * FROM `porder` WHERE po_active='$active'";
		return $this->ExecuteS($sql);
	 }
	 
	 public function editActivePo($id, $act)
	 {
		 $sql = "UPDATE `porder` SET po_active='$act' WHERE po_id='$id'";
		 $this->Execute($sql);
	 }
	 
	 public function setApproval($id_po, $user, $approval, $desc)
	 {
		 $sql = "UPDATE `porder` SET po_uapproval='$user', po_approval='$approval', po_approvaldesc='$desc' WHERE po_id='$id_po'";
		 $this->Execute($sql);
	 }
	 
	 public function printPo($format, $id_po)
	 {
		$f=fopen('../formats/'.$format,'r');
		$m = fread($f,filesize('../formats/'.$format));
		
		$row = $this->getPorderById($id_po);
		if(count($row)>0)
		{
			foreach($row as $row)
			{
				$m=str_replace('[track]',$row['po_track'],$m);
				$m=str_replace('[observaciones]',$row['po_obs'],$m);
				$m=str_replace('[subtotal]',$row['po_total'],$m);
				$m=str_replace('[fecha]',$row['po_fecha'],$m);
				$id_provider = $row['popr_id'];
				$approval = $row['po_approval'];
				$userapproval = $row['po_uapproval'];
			}
		}
		
		switch($approval)
		{
			case '0':
			$m=str_replace('[aprobacion]',"Sin Aprobar",$m);
			break;
			case '1':
			$m=str_replace('[aprobacion]',"Aprobado",$m);
			break;
			case '2':
			$m=str_replace('[aprobacion]',"No Aprobado",$m);
			break;
		}
		
		$sql = "select * from user where u_id='$userapproval'";
		$row = $this->ExecuteS($sql);
		if(count($row)>0)
		{
			foreach($row as $row)
			{
				$m=str_replace('[aprobador]',$row['u_nom'],$m);
			}
		}
		
		$row = $this->getProviderById($id_provider);
		if(count($row)>0)
		{
			foreach($row as $row)
			{
				$m=str_replace('[nombreproveedor]',$row['popr_name'],$m);
				$m=str_replace('[nombrecontacto]',$row['popr_contactname'],$m);
				$m=str_replace('[direccion]',$row['popr_addr'],$m);
				$m=str_replace('[telefono]',$row['popr_phone'],$m);
			}
		}
		
		$row = $this->getPorderDetail($id_po);
		if(count($row)>0)
		{
			foreach($row as $row)
			{
				$htm.='<tr class="txt_destacado">
        <td width="163"><div align="center" class="Estilo4">'.$row['pod_desc'].'</div></td>
        <td width="77"><div align="center" class="Estilo4">$'.number_format($row['pod_price']).'</div></td>
        <td width="72"><div align="center" class="Estilo4">'.$row['pod_qty'].'</div></td>
        <td width="86"><div align="center" class="Estilo4">$'.number_format($row['pod_total']).'</div></td>
      </tr>';
			}
		}
		$m=str_replace('[detallepo]',$htm,$m);
		$crmModelObj=new crmScs();
		 $crmModelObj->connect();
		 
		 $row=$crmModelObj->getCompanyById(2);
		 if(count($row)>0){
		  foreach($row as $row){
		  	$company='<img src="../../crm/Logos/'.$row["e_img"].'" width="200" height="100" /><br />';
		  }
		 }
		 $m=str_replace('[company]',$company,$m);
		 
		 return $m;
		
	 }
	 
	 public function getPordersInf()
	 {
		 $start = $this->postVars('start')." 00:00:00";
		 $end = $this->postVars('start')." 23:59:59";
		 
		 $sql = "SELECT * FROM porder WHERE po_fecha BETWEEN '$start' AND '$end' AND po_approval = '1'";
		 return $this->ExecuteS($sql);
	 }
	 
	 //Purchase Orders Detail Temp
	 public function newPorderDetailTemp($user, $id_product, $desc, $price, $qty, $total)
	 {
		 $sql = "INSERT INTO `porder_detail_temp` (`u_id`, `id_product`, `podt_desc`, `podt_price`, `podt_qty`, `podt_total`)
		 VALUES ('$user','$id_product','$desc','$price','$qty','$total')";
		 $this->Execute($sql);
	 }
	 
	 public function delPorderDetailTemp($id)
	 {
		 $sql = "DELETE FROM `porder_detail_temp` WHERE `podt_id`='$id'";
		 $this->Execute($sql);
	 }
	 
	 public function getPorderDetailTemp($user)
	 {
		 $sql = "SELECT * FROM `porder_detail_temp` WHERE `u_id`='$user'";
		 return $this->ExecuteS($sql);
	 }
	 
	 public function flushPorderDetailTemp($user)
	 {
		 $sql = "DELETE FROM `porder_detail_temp` WHERE `u_id`='$user'";
		 $this->Execute($sql);
	 }
	 
	 //Purchase Orders Detail
	 public function newPorderDetail($id_po, $id_product, $desc, $price, $qty, $total)
	 {
		 $sql = "INSERT INTO `porder_detail` (`po_id`, `id_product`, `pod_desc`, `pod_price`, `pod_qty`, `pod_total`)
		 VALUES ('$id_po','$id_product','$desc','$price','$qty','$total')";
		 $this->Execute($sql);
	 }
	 
	 public function delPorderDetail($id)
	 {
		 $sql = "DELETE FROM `porder_detail` WHERE `pod_id`='$id'";
		 $this->Execute($sql);
	 }
	 
	 public function getPorderDetail($id_po)
	 {
		 $sql = "SELECT * FROM `porder_detail` WHERE `po_id`='$id_po'";
		 return $this->ExecuteS($sql);
	 }
	 
	 //Purchase Order Documents
	 public function newPorderDoc($id_po, $desc)
	 {
		 
		 $sql = "INSERT INTO `porder_docs` (`podo_desc`, `po_id`) VALUES ('$desc', '$id_po')";
		 $this->Execute($sql);
		 $id = $this->getLastID();
		 $this->uploadPoFile($id);

	 }
	 public function uploadPoFile($id){
		
		$ok=$this->upLoadFileProccess('arch', 'modules/porders/poFile');
		
		if($ok){
		 $sql="update `porder_docs` set `podo_file`='".$this->fname."' where `podo_id`='$id'";
		 $this->Execute($sql);
		}
		
	}
	
	public function editPorderDoc($id, $id_po, $desc)
	{
		
		$sql = "UPDATE `porder_docs` SET `podo_desc`='$desc' WHERE `podo_id`='$id'";
		$this->Execute($sql);
		$this->uploadPoFile($id);
	}
	
	public function delPorderDoc($id)
	{
		$sql = "DELETE FROM `porder_docs` WHERE `podo_id`='$id'";
		$this->Execute($sql);
	}
	
	public function getPorderDocByPO($id_po)
	{
		$sql = "SELECT * FROM `porder_docs` WHERE po_id='$id_po'";
		return $this->ExecuteS($sql);
	}
	
	public function getPorderDocById($id)
	{
		$sql = "SELECT * FROM `porder_docs` WHERE podo_id='$id'";
		return $this->ExecuteS($sql);
	}
	
	//Null Register Methods
	public function newNullRegisterPo($id, $desc, $user)
	{		
		$today = date('Y-m-d H:i:s');
		
		$sql = "INSERT INTO `porder_nullregister` (`po_id`, `ponr_desc`, `ponr_date`, `u_id`) 
		VALUES ('$id', '$desc', '$today', '$user')";
		$this->Execute($sql);
	}
	
	public function getNullRegisterPo()
	{
		$start = $this->postVars('start')." 00:00:00";
		$end = $this->postVars('end')." 23:59:59";
		
		$sql = "SELECT * FROM `porder_nullregister` WHERE `ponr_date` BETWEEN '$start' AND '$end'";
		return $this->ExecuteS($sql);
	}
	
	//Param Methods
	public function getPoParams()
	{
		$sql = "SELECT * FROM `porder_param`";
		return $this->ExecuteS($sql);
	}
	
	public function editPoParams($id, $value)
	{
		$sql = "UPDATE `porder_param` SET `pop_value`='$value' WHERE `pop_id`='$id'";
		$this->Execute($sql);
	}
	
	public function getPoParamsById($id)
	{
		$sql = "SELECT * FROM `porder_param` WHERE `pop_id`='$id'";
		$row = $this->ExecuteS($sql);
		if(count($row)>0)
		{
			foreach($row as $row)
			{
				return $row['pop_value'];
			}
		}
	}
	
	public function incrementPoParams($id)
	{
		$value = $this->getPoParamsById($id);
		$value++;
		$this->editPoParams($id, $value);
	}
	
	//Purchase Order Type Methods
	public function newPoType()
	{
		$name = $this->postVars('name');
		$sql = "INSERT INTO `porder_type` (`pot_name`) VALUES ('$name')";
		$this->Execute($sql);
	}
	
	public function editPoType()
	{
		$id = $this->postVars('id');
		$name = $this->postVars('name');
		
		$sql = "UPDATE `porder_type` SET `pot_name`='$name' WHERE pot_id='$id'";
		$this->Execute($sql);
	}
	
	public function delPoType($id)
	{
		$sql = "DELETE FROM `porder_type` WHERE pot_id='$id'";
		$this->Execute($sql);
	}
	
	public function getPoType()
	{
		$sql = "SELECT * FROM `porder_type`";
		return $this->ExecuteS($sql);
	}
	
	public function getPoTypeById($id)
	{
		$sql = "SELECT * FROM `porder_type` WHERE pot_id='$id'";
		return $this->ExecuteS($sql);
	}
	
	//Auth Methods
	public function authorize()
	{
		$user = $this->postVars('user');
		$pass = md5($this->postVars('pass'));
		
		$sql="select * from user where u_user='$user' and u_pass='$pass' and u_active='1'";
		$row=$this->ExecuteS($sql);
		 
		if(count($row)>0)
		{
			foreach($row as $row)
			{
				$auth = $row['letPoAuth'];
			}
			
			if($auth == 1)
			{
				return 'ok';
			}
			else
			{
				return 'noauth';
			}
		}
		else
		{
			return 'npass';
		}
	}
	
	//Auth methods
	public function editAuth($id_user,$auth){
	 $sql="update user set letPoAuth='$auth' where u_id='$id_user'";
	 $this->Execute($sql);
	}
	
	//provider methods
	public function newProvider()
	{
		$name = $this->postVars('name');
		$type = $this->postVars('type');
		$phone = $this->postVars('phone');
		$addr = $this->postVars('addr');
		$contactname = $this->postVars('contactname');
		$contactphone = $this->postVars('contactphone');
		$contactemail = $this->postVars('contactemail');
		$identification = $this->postVars('identification');
		$paylimit = $this->postVars('paylimit');
		
		$sql = "INSERT INTO `porder_provider` (`popr_name`, `popr_type`, `popr_phone`, `popr_addr`, `popr_enable`, `popr_contactname`, `popr_contactphone`, `popr_contactemail`, `popr_identification`, `popr_limitpay`)
		VALUES ('$name', '$type', '$phone', '$addr', '0', '$contactname', '$contactphone', '$contactemail','$identificacion','$paylimit')";
	
		$this->Execute($sql);
		
		$id = $this->getLastID();
		$this->uploadProvImg($id);
	}
	
	public function uploadProvImg($id){
		
		$ok=$this->upLoadFileProccess('arch', 'modules/porders/provImg');
		
		if($ok){
		 $sql="update `porder_provider` set `popr_img`='".$this->fname."' where `popr_id`='$id'";
		 $this->Execute($sql);
		}
	}
	
	public function editProvider()
	{
		$id = $this->postVars('id');
		$name = $this->postVars('name');
		$type = $this->postVars('type');
		$phone = $this->postVars('phone');
		$addr = $this->postVars('addr');
		$contactname = $this->postVars('contactname');
		$contactphone = $this->postVars('contactphone');
		$contactemail = $this->postVars('contactemail');
		$identification = $this->postVars('identification');
		$paylimit = $this->postVars('paylimit');
		
		$sql = "UPDATE `porder_provider` SET `popr_name`='$name', `popr_type`='$type', `popr_phone`='$phone', `popr_addr`='$addr', `popr_contactname`='$contactname', `popr_contactphone`='$contactphone', `popr_contactemail`='$contactemail', `popr_identification`='$identification', `popr_limitpay`='$paylimit'
		WHERE popr_id = '$id'";
	
		$this->Execute($sql);
		
		$this->uploadProvImg($id);
	}
	
	public function delProvider($id)
	{
		$sql = "DELETE FROM `porder_provider` WHERE popr_id = '$id'";
		$this->Execute($sql);
	}
	
	public function getProvider()
	{
		$sql = "SELECT * FROM `porder_provider`";
		return $this->ExecuteS($sql);
	}
	
	public function getProviderById($id)
	{
		$sql = "SELECT * FROM `porder_provider` WHERE popr_id = '$id'";
		return $this->ExecuteS($sql);
	}
	
	public function getProviderByEnable($enable)
	{
		$sql = "SELECT * FROM `porder_provider` WHERE popr_enable = '$enable' order by popr_name asc";
		return $this->ExecuteS($sql);
	}
	
	public function editProviderEnable($id, $enable)
	{
		$sql = "UPDATE `porder_provider` SET `popr_enable`='$enable' WHERE popr_id = '$id'";
		$this->Execute($sql);
	}
	
	public function getProviderProd($id)
	{
	    $sql = "SELECT * FROM products WHERE popr_id='$id'";
	    return $this->ExecuteS($sql);
	}
	
	//Providers Documents Methods
	public function newProviderDoc($id_provider, $name, $desc)
	{
		$sql = "INSERT INTO `porder_providerdocs` (`poprd_name`, `poprd_desc`, `popr_id`) VALUES ('$name', '$desc', '$id_provider')";
		$this->Execute($sql);
		$id = $this->getLastID();
		$this->uploadProvDoc($id);
	}
	
	public function uploadProvDoc($id){
		
		$ok=$this->upLoadFileProccess('arch', 'modules/porders/provDoc');
		
		if($ok){
		 $sql="update `porder_providerdocs` set `popr_doc`='".$this->fname."' where `poprd_id`='$id'";
		 $this->Execute($sql);
		}
	}
	
	public function editProviderDoc($id, $id_provider, $name, $desc)
	{
		$sql = "UPDATE `porder_providerdocs` SET `poprd_name`='$name', `poprd_desc`='$desc' WHERE `poprd_id`='$id'";
		$this->Execute($sql);
		$this->uploadProvDoc($id);
	}
	
	public function delProviderDoc($id)
	{
		$sql = "DELETE FROM `porder_providerdocs` WHERE `poprd_id`='$id'";
		$this->Execute($sql);
	}
	
	public function getProviderDoc($id_provider)
	{
		echo $sql = "SELECT * FROM `porder_providerdocs` WHERE `popr_id`='$id_provider'";
		return $this->ExecuteS($sql);
	}
  
	
	//Installers
   public function Checker(){
    return $this->chkTables('porder');
   }
   
   public function install(){
	$this->importSQLFile('modules/porders/sql/installer.sql');
	//Se instala los submenus
	$sql="select * from menu where m_folder='porders'";
	$row=$this->ExecuteS($sql);
	foreach($row as $row){
	 $m_id=$row["m_id"];
	}
	
	$sql="insert into `submenu` (`sm_nom`, `sm_link`, `m_id`) values ('Proveedores','home.php?p=modules/porders/view/showProviders.php','$m_id')";
	$this->Execute($sql);
	$sql="insert into `submenu` (`sm_nom`, `sm_link`, `m_id`) values ('Setup','home.php?p=modules/porders/view/showSetup.php','$m_id')";
	$this->Execute($sql);
	$sql="insert into `submenu` (`sm_nom`, `sm_link`, `m_id`) values ('Aprobar Proveedores','home.php?p=modules/porders/view/showProviders2.php','$m_id')";
	$this->Execute($sql);
	$sql="insert into `submenu` (`sm_nom`, `sm_link`, `m_id`) values ('Informe','home.php?p=modules/porders/view/showInfPo.php','$m_id')";
	$this->Execute($sql);
	$sql="insert into `submenu` (`sm_nom`, `sm_link`, `m_id`) values ('Aprobar Ordenes','home.php?p=modules/porders/view/showPo.php','$m_id')";
	$this->Execute($sql);
	
   }
   
   public function uninstall(){
	$sql="DROP TABLE `porder`, `porder_detail_temp`, `porder_detail`, `porder_docs`, `porder_nullregister`, `porder_param`, `porder_type`, `porder_provider`, `porder_providerdocs`";
	$this->Execute($sql);
	
	$sql="ALTER TABLE  `user` DROP  `letPoAuth`;";
	$this->Execute($sql);
	
	$sql="select * from menu where m_folder='porders'";
	$row=$this->ExecuteS($sql);
	foreach($row as $row){
	 $m_id=$row["m_id"]; 
	}
	
	$sql="select * from submenu as sm inner join pro_submenu as psm on sm.sm_id=psm.sm_id where sm.m_id='$m_id'";
	$row=$this->ExecuteS($sql);
	foreach($row as $row){
	 $sql1="delete from pro_submenu where sm_id='".$row["sm_id"]."'";
	 $this->Execute($sql);
	}
	
	$sql1="delete from submenu where m_id='$m_id'";
	$this->Execute($sql);
	
   }
	
	
 };
?>
