<?php
class blocksn extends GeCore{
	
	
	public function newBlock(){
		$name=$this->postVars('name');
		$link=$this->postVars('link');
		$awe=$this->postVars('awe');

		$sql="insert into `blocksn` (`blsn_name`, `blsn_link`, `blsn_image`, `blsn_awe`) values ('$name','$link','','$awe')";
		$this->Execute($sql);
		
		$id=$this->getLastID();
		
		$this->uploadImg($id);
	}
	
	public function uploadImg($id){
		
		$ok=$this->upLoadFileProccess('img', 'modules/blocksn/imgBlocks');
		
		if($ok){
		 $sql="update blocksn set blsn_image='".$this->fname."' where blsn_id='$id'";
		 $this->Execute($sql);
		}
		
	}
	
	public function editBlock(){
		$id=$this->postVars('id');
		$name=$this->postVars('name');
		$link=$this->postVars('link');
		$awe=$this->postVars('awe');
		
		$sql="update `blocksn` set `blsn_name`='$name', `blsn_link`='$link', `blsn_awe`='$awe' where `blsn_id`='$id'";
		$this->Execute($sql);
		
		$this->uploadImg($id);
	}
	
	public function delBlock(){
		$id=$this->getVars('id');
		
		$sql="delete from `blocksn` where blsn_id='$id'";
		$this->Execute($sql);
	}
	
	
	public function getBlocks(){
	 $sql="select * from blocksn";
	 return $this->ExecuteS($sql);
	}
	
	public function getBlockById($id){
	 
	  $sql="select * from blocksn where blsn_id='$id'";
	  return $row=$this->ExecuteS($sql);
	}
	
	//Installers
   public function Checker(){
    return $this->chkTables('blocksn');
   }
   
   public function install(){
    $sql='CREATE TABLE `blocksn` (
`blsn_id` INT NOT NULL AUTO_INCREMENT PRIMARY KEY ,
`blsn_name` VARCHAR( 255 ) NOT NULL ,
`blsn_link` VARCHAR( 255 ) NOT NULL ,
`blsn_image` VARCHAR( 255 ) NOT NULL ,
`blsn_awe` VARCHAR( 255 ) NOT NULL
) ENGINE = MYISAM ;';
	
	$this->Execute($sql);
   }
   
   public function uninstall(){
    $sql='DROP TABLE `blocksn`';
	$this->Execute($sql);
   }
	
};
?>
