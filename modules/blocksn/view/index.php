<?php
 include('modules/blocksn/model/blocksn.php');
 
 $obj = new blocksn();
 $obj->connect();
 
  //Se verifica si el modulo esta instalado
 if(!$obj->Checker()){
  $ins=true;
 }
 
 $msg=false;
 
 if($obj->getVars('ActionDel')){
  $obj->delBlock();
  $msg=true;
 }
 
 //Instala el modulo
 if($obj->getVars('install')){
  $obj->install();
  $msgIns=true;
 }

 if(!$ins){
 ?>
 <div class="widget3">
 <div class="widgetlegend">Bloques Redes Sociales</div>
 <?php
  if($msg)
  {
  ?>
   <div class="ui-widget">
	<div class="ui-state-highlight ui-corner-all" style="margin-top: 20px; padding: 0 .7em;">
		<p><span class="ui-icon ui-icon-info" style="float: left; margin-right: .3em;"></span>
		<strong>Felicitaciones!</strong> Eliminado satisfactoriamente.</p>
	</div>
</div>
  <?php
  }
 ?>
<p style="width:100%">
	<a href="<?php $_SERVER['PHP_SELF'];?>?p=modules/blocksn/view/newBlock.php" class="btn_normal" style="float:left; margin:5px;">Nuevo </a>
</p>
 
<table width="100%" height="48" border="0">
  <tr>
    <th width="30">ID</th>
    <th width="142">Nombre</th>
    <th width="213">Url</th>
    <th width="125">Imagen</th>
    <th width="125">Font Awesome</th>
    <th colspan="2">Acciones</th>
  </tr>
  <?php
   $row=$obj->getBlocks();
   
   if(count($row)>0){
 	foreach($row as $row){
  ?>
  <tr>
    <td><?php echo $row["blsn_id"];?></td>
    <td><?php echo $row["blsn_name"];?></td>
    <td><?php echo $row["blsn_link"];?></td>
    <td><a href="modules/blocksn/imgBlocks/<?php echo $row["blsn_image"];?>"><img src="modules/blocksn/imgBlocks/<?php echo $row["blsn_image"];?>" width="100" height="100"  /></a></td>
    <td><i class="fa <?php echo $row["blsn_awe"];?>"></i></td>
    <td width="86"><a href="<?php $_SERVER['PHP_SELF'];?>?p=modules/blocksn/view/editBlock.php&id=<?php echo $row["blsn_id"]?>" class="btn_normal">Editar</a></td>
    <td width="81"><a href="<?php $_SERVER['PHP_SELF'];?>?p=modules/blocksn/view/index.php&id=<?php echo $row["blsn_id"]?>&ActionDel=true" class="btn_borrar">Eliminar</a></td>
  </tr>
  <?php
   }
  } 
  ?>
</table>

</div>

<?php } else {?>
<br />
 <div class="widget3">
 <div class="widgetlegend">Instalador Modulo de Bloques Redes Sociales </div>
 <?php
  if($msgIns)
  {
  ?>
   <div class="ui-widget">
	<div class="ui-state-highlight ui-corner-all" style="margin-top: 20px; padding: 0 .7em;">
		<p><span class="ui-icon ui-icon-info" style="float: left; margin-right: .3em;"></span>
		<strong>Felicitaciones!</strong> El modulo ha sido instalado satisfactoriamente.</p>
	</div>
</div>
  <?php
  }
 ?>
<form id="form1" name="form1" method="post" action="#">
  <table width="804" border="0" style="float:left;">
    <tr>
      <td width="525" valign="top">&nbsp; </td>
    </tr>
    <tr>
      <td valign="top"><div align="right"><a href="<?php $_SERVER['PHP_SELF'];?>?p=modules/blocksn/view/index.php&amp;install=true" class="btn_normal">Instalar</a></div></td>
    </tr>
  </table>
</form>
</div>
<?php } ?>

