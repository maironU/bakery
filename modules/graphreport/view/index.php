<?php
 include('modules/graphreport/model/graphreport.php');
 
 
 $obj = new graphreport();
 $obj->connect();
 
 //Se verifica si el modulo esta instalado
 if(!$obj->Checker()){
  $ins=true;
 }
 
 $msg=false;
 $msgIns=false;
 
 
 //Instala el modulo
 if($obj->getVars('install')){
  $obj->install();
  $msgIns=true;
 }
?>
<link href="modules/graphreport/css/graphreport.css" rel="stylesheet" type="text/css" />
<?php
 if(!$ins){
 ?>
<div class="widget3">
 <div class="widgetlegend">Graficas </div>
 <?php
  if($msg)
  {
  ?>
   <div class="ui-widget">
	<div class="ui-state-highlight ui-corner-all" style="margin-top: 20px; padding: 0 .7em;">
		<p><span class="ui-icon ui-icon-info" style="float: left; margin-right: .3em;"></span>
		<strong>Felicitaciones!</strong> El contenido ha sido modificado satisfactoriamente.</p>
	</div>
</div>
  <?php
  }
 ?>
  
<p style="width:100">
	<a href="<?php $_SERVER['PHP_SELF'];?>?p=modules/graphreport/view/AdmReports.php" class="btn_normal" style="float:left; margin:5px;">Adm Reportes </a>
</p>
 
<form id="form1" name="form1" method="post" action="#">
  <table width="800" border="0" style="float:left;">
    <tr>
      <td width="265" valign="top"><div id="menur">
          <ul>
            <li>
              <h2 class="thead">Lista de Reportes</h2>
            </li>
            <?php
	    $row = $obj->getGraphReports();
		
		if(count($row)>0)
		{
		 foreach($row as $row)
		{
	   ?>
            <li><a href="<?php echo $_SERVER['PHP_SELF'];?>?rep=modules/graphreport/view/<? echo $row["gr_file"]; ?>&amp;p=modules/graphreport/view/index.php"><? echo $row["gr_name"]; ?></a></li>
            <?php
		 }
		}
		?>
          </ul>
      </div></td>
      <td width="525" valign="top"><?php
	  							$rep=$obj->getVars('rep');
                                if($rep==''){
								?>
                                 <div class="ui-widget">
                                    <div class="ui-state-highlight ui-corner-all" style="margin-top: 20px; padding: 0 .7em;">
                                        <p><span class="ui-icon ui-icon-info" style="float: left; margin-right: .3em;"></span>
                                         Por favor selecciona uno de los reportes que deseas visualizar.</p>
                                    </div>
                                </div>
                                <?php
								}else{
								 include($rep);
								}
                            ?>
          </td>
    </tr>
    <tr>
      <td valign="top"><div align="right"></div></td>
      <td valign="top"><div align="right"></div></td>
    </tr>
  </table>
</form>
</div>
<?php }else {
 ?><br />
 
 <div class="widget3">
 <div class="widgetlegend">Instalador reportes </div>
 <?php
  if($msgIns)
  {
  ?>
   <div class="ui-widget">
	<div class="ui-state-highlight ui-corner-all" style="margin-top: 20px; padding: 0 .7em;">
		<p><span class="ui-icon ui-icon-info" style="float: left; margin-right: .3em;"></span>
		<strong>Felicitaciones!</strong> El modulo ha sido instalado satisfactoriamente.</p>
	</div>
</div>
  <?php
  }
 ?>
<form id="form1" name="form1" method="post" action="#">
  <table width="804" border="0" style="float:left;">
    <tr>
      <td width="525" valign="top"> </td>
    </tr>
    <tr>
      <td valign="top"><div align="right"><a href="<?php $_SERVER['PHP_SELF'];?>?p=modules/graphreport/view/index.php&install=true" class="btn_normal">Instalar</a></div></td>
    </tr>
  </table>
</form>
</div>
 <?php
}
?>
