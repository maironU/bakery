<?php
 include('modules/graphreport/model/graphreport.php');
 
 
 $obj = new graphreport();
 $obj->connect();
 
 $msg=false;
 $msg1=false;

 //Se obtiene informacion de la zona
 
 if($obj->getVars('actionDel')=='true')
 {
  $obj->delGraphReport();
  $msg1=true;
 }
 
 //Agregar contenido
 if($_POST)
 {
  $obj->newGraph();
  $msg=true;
  $msg1=false;
 }
?>
<link href="modules/graphreport/css/graphreport.css" rel="stylesheet" type="text/css" />
<div class="widget3">
 <div class="widgetlegend">Adm. Reportes </div>
 <?php
  if($msg)
  {
  ?>
   <div class="ui-widget">
	<div class="ui-state-highlight ui-corner-all" style="margin-top: 20px; padding: 0 .7em;">
		<p><span class="ui-icon ui-icon-info" style="float: left; margin-right: .3em;"></span>
		<strong>Felicitaciones!</strong> El reporte ha sido creado satisfactoriamente.</p>
	</div>
</div>
  <?php
  }
 ?>
 <?php
  if($msg1)
  {
  ?>
   <div class="ui-widget">
	<div class="ui-state-highlight ui-corner-all" style="margin-top: 20px; padding: 0 .7em;">
		<p><span class="ui-icon ui-icon-info" style="float: left; margin-right: .3em;"></span>
		<strong>Felicitaciones!</strong> El reporte ha sido eliminado satisfactoriamente.</p>
	</div>
</div>
  <?php
  }
 ?>
<p style="width:100">
	<a href="<?php $_SERVER['PHP_SELF'];?>?p=modules/graphreport/view/index.php" class="btn_normal" style="float:left; margin:5px;">Volver</a></p>
 
<form id="form1" name="form1" method="post" action="#" enctype="multipart/form-data">
  <table width="594" border="0" style="float:left;">
    <tr>
      <td width="525" valign="top"><table width="558" border="0">
        <tr>
          <td width="148">Nombre del reporte: </td>
          <td width="171"><input name="name" type="text" id="name" /></td>
          <td width="148">Archivo: </td>
          <td width="171"><input name="fname" type="text" id="fname" /></td>
          <td width="225"><a href="javascript:;" class="btn_normal" onclick="document.form1.submit();">Agregar</a></td>
        </tr>
      </table></td>
    </tr>
    <tr>
      <td valign="top"><table width="558" height="27" border="0">
        <tr>
          <th>Reporte</th>
          <th>Acciones</th>
        </tr>
		<?php
		 $row=$obj->getGraphReports();
		 if(count($row)>0){
		 foreach($row as $row)
		 {
		?>
        <tr>
          <td><?php echo $row["gr_name"]; ?></td>
          <td><a href="<?php echo $_SERVER['PHP_SELF'];?>?id=<?php echo $row["gr_id"]; ?>&p=modules/graphreport/view/AdmReports.php&actionDel=true" class="btn_borrar">Eliminar</a></td>
        </tr>
		<?php
		 }
		}
		?>
      </table></td>
    </tr>
  </table>
</form>
</div>
