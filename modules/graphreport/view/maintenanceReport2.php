<?php
session_start();
include('../../../core/config.php');
include('../../../core/ge.php');
include('../../maintenance/model/maintenance.php');

$maintenance = new maintenance();
$maintenance->connect();




$colors = array('#00ff00', '#ff0000', '#0000ff',"#FFFF00", "#FF00FF", "#CCCCCC");
$row = $maintenance->getProblems();
foreach($row as $row)
{
	$labels[] = utf8_decode($row['mp_name']);	
	$num = $maintenance->getNumProblems($row["mp_id"]);
	$values[] = $num;
	
}
$datas = array(
		'labels' => $labels,
		'datasets' => array(
			array(
				'data' => $values,
				'backgroundColor' => $colors
			)
		)
	);

echo json_encode($datas);

?>

