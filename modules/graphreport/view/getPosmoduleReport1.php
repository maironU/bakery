<?php
session_start();
include('../../../core/config.php');
include('../../../core/ge.php');
include('../../posmodule/model/posmodule.php');

$posmodule = new posmodule();
$posmodule->connect();

$y = $posmodule->postVars('y');

if($y==''){
 $y=date('Y');
}

$labels = array('Enero', 'Febrero', 'Marzo',"Abril", "Mayo", "Junio", "Julio", "Agosto", "Septiembre", "Octubre", "Diciembre");

for($i=1; $i<=12; $i++)
{
	if($i<10)
	{
	 $m="0".$i;
	}
	else
	{
	 $m=$i;
	}
	
	$total=0;
	$sql="select * from posmodule where pos_dateadd like '".$y."-".$m."%'";
	$row = $posmodule->ExecuteS($sql);
	if(count($row)>0){
		foreach($row as $row)
		{
			$total+=$row['pos_subtotal'];
		}
		$d[]=$total;
	}else{
	$d[]=0;
	}
	
	
	
}

$datas = array(
		'labels' => $labels,
		'datasets' => 
		array(array(
			'label'=> 'Flujo de ventas '.$y,
			'fillColor'=> "rgba(220,220,220,0.2)",
			'strokeColor'=> "rgba(220,220,220,1)",
            'pointColor'=> "rgba(220,220,220,1)",
            'pointStrokeColor'=> "#fff",
            'pointHighlightFill'=> "#fff",
            'pointHighlightStroke'=> "rgba(220,220,220,1)",
			'data'=> $d
		))
	);

echo json_encode($datas);

?>

