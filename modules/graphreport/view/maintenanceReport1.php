<?php
session_start();
include('../../../core/config.php');
include('../../../core/ge.php');
include('../../maintenance/model/maintenance.php');

$maintenance = new maintenance();
$maintenance->connect();


$row = $maintenance->getElements();
foreach($row as $row)
{
 $labels[] = $row['me_name'];
 $row1 = $maintenance->getElementHistory($row['me_id']);
 $d[] = count($row1);
}

$datas = array(
		'labels' => $labels,
		'datasets' => 
		array(array(
			'label'=> 'Elementos con mas probelmas',
			'fillColor'=> "rgba(220,220,220,0.2)",
			'strokeColor'=> "rgba(220,220,220,1)",
            'pointColor'=> "rgba(220,220,220,1)",
            'pointStrokeColor'=> "#fff",
            'pointHighlightFill'=> "#fff",
            'pointHighlightStroke'=> "rgba(220,220,220,1)",
			'data'=> $d
		))
	);

echo json_encode($datas);

?>

