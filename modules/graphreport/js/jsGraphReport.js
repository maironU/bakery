

function getBarChart()
{
	$.ajax({
	 type:'POST',
	 url:'admin/modules/graphreport/view/getTeamPerformance.php',
	 async:false,
	 success:function(data){
		
		var ctx = document.getElementById("customertypecanvas").getContext("2d");
		
		ctx.canvas.width = "100";
		ctx.canvas.height = "100";
		window.myBar = new Chart(ctx).Pie(JSON.parse(data), {
			responsive : true
		});
	 }
	});
}