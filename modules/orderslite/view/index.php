<?php
 include('includes.php');
 
  //Se verifica si el modulo esta instalado
 if(!$obj->Checker()){
  $ins=true;
 }
 
 $msg=false;
 $msg2=false;
 
 if($obj->getVars('ActionDel')){
  $obj->delOrderLite();
  $msg=true;
  $msg2=false;
 }
 
 if($obj->getVars('ActionState'))
 {
    $state = $obj->getVars('state');
    $id = $obj->getVars('id');
    $obj->editOrderLiteState($id, $state);
    $msg=false;
    $msg2=true;
 }
 
 //Instala el modulo
 if($obj->getVars('install')){
  $obj->install();
  $msgIns=true;
 }

?>
<?php
 if(!$ins){
 ?>
 <!-- Add mousewheel plugin (this is optional) -->
<script type="text/javascript" src="modules/orderslite/js/jquery.mousewheel-3.0.6.pack.js"></script>
<!-- Add fancyBox main JS and CSS files -->
<script type="text/javascript" src="modules/orderslite/js/jquery.fancybox.js?v=2.1.4"></script>
<link rel="stylesheet" type="text/css" href="modules/orderslite/css/jquery.fancybox.css?v=2.1.4" media="screen" />
<script type="text/javascript" src="modules/orderslite/js/jsOlFunctions.js"></script>
 <div class="widget3">
 <div class="widgetlegend">Pedidos Lite </div>
 <?php
  if($msg)
  {
  ?>
   <div class="ui-widget">
	<div class="ui-state-highlight ui-corner-all" style="margin-top: 20px; padding: 0 .7em;">
		<p><span class="ui-icon ui-icon-info" style="float: left; margin-right: .3em;"></span>
		<strong>Felicitaciones!</strong> El slide ha sido eliminado satisfactoriamente.</p>
	</div>
</div>
  <?php
  }
 ?>
 <?php
  if($msg2)
  {
  ?>
   <div class="ui-widget">
	<div class="ui-state-highlight ui-corner-all" style="margin-top: 20px; padding: 0 .7em;">
		<p><span class="ui-icon ui-icon-info" style="float: left; margin-right: .3em;"></span>
		<strong>Felicitaciones!</strong> Pedido entregado.</p>
	</div>
</div>
  <?php
  }
 ?>
<p style="width:100">
	<a href="<?php $_SERVER['PHP_SELF'];?>?p=modules/orderslite/view/newOrderLite.php" class="btn_normal" style="float:left; margin:5px;">Nuevo </a>
</p>
 
<table width="803" height="48" border="0">
  <tr>
    <th width="30">ID</th>
    <th width="30">Estado</th>
    <th width="30">Fecha</th>
    <th width="142">Track</th>
    <th width="213">Cliente</th>
    <th width="125">Email</th>
    <th width="125">Telefono</th>
    <th width="125">Descripcion</th>
    <th width="125">Tienda</th>
    <th width="96">Fecha/Hora Entrega</th>
    <th width="96">Tipo pago</th>
    <th width="96">Total</th>
    <th width="96">Abono</th>
    <th width="96">Pendiente</th>
    <th colspan="2">Acciones</th>
  </tr>
  <?php
   $row=$obj->getOrderLite();
   
   if(count($row)>0){
 	foreach($row as $row){
  ?>
  <tr>
    <td><?php echo $row["ol_id"];?></td>
    <td width="86">
        <?php
            if($row['ol_state'] == 1)
            {
                ?>
                <span style="color: #f00">Pendiente</span>
                <?php
            }
            else
            {
                ?>
                <span style="color: #0f0">Terminado</span>
                <?php
            }
        ?>
    </td>
    <td><?php echo $row["ol_date"];?></td>
    <td><?php echo $row["ol_track"];?></td>
    <td><?php echo $row["ol_customername"];?></td>
    <td><?php echo $row["ol_customeremail"];?></td>
    <td><?php echo $row["ol_customerphone"];?></td>
    <td><?php echo $row["ol_desc"];?></td>
    <td>
        <?php
            $row1 = $pos->getStoreById($row['id_store']);
            if(count($row1)>0)
            {
                foreach($row1 as $row1)
                {
                    echo $row1['poss_name'];
                }
            }
        ?>
    </td>
    <td><?php echo $row["ol_deliverdate"]." ".$row['ol_deliverhour'];?></td>
    <td><?php echo $obj->getPaymentTypeValue($row["ol_paymenttype"]);?></td>
    <td>$<?php echo number_format($row["ol_total"],2,',','.');?></td>
    <td>$<?php echo number_format($row["ol_prepayment"],2,',','.');?></td>
    <td>$<?php echo number_format($row["ol_total"]-$row["ol_prepayment"],2,',','.');?></td>
    <td width="86"><a href="<?php $_SERVER['PHP_SELF'];?>?p=modules/orderslite/view/editOrderLite.php&id=<?php echo $row["ol_id"]?>" class="btn_1">Editar</a></td>
    <td width="86"><a href="javascript:;" onClick="showInfo(<?php echo $row["ol_id"]?>,'modules/orderslite/view/printOrderLite.php')"; class="btn_3">Imprimir</a></td>
    <td width="86">
        <?php
            if($row['ol_state'] == 1)
            {
                ?>
                <a href="<?php $_SERVER['PHP_SELF'];?>?p=modules/orderslite/view/index.php&id=<?php echo $row["ol_id"]?>&ActionState=true&state=0" class="btn_4">Terminado</a>
                <?php
            }
        ?>
    </td>    
    <td width="81"><a href="<?php $_SERVER['PHP_SELF'];?>?p=modules/orderslite/view/index.php&id=<?php echo $row["ol_id"]?>&ActionDel=true" class="btn_2">Eliminar</a></td>
  </tr>
  <?php
   }
  } 
  ?>
</table>

</div>

<?php } else {?>
<br />
 <div class="widget3">
 <div class="widgetlegend">Instalador Modulo de Ordenes Lite </div>
 <?php
  if($msgIns)
  {
  ?>
   <div class="ui-widget">
	<div class="ui-state-highlight ui-corner-all" style="margin-top: 20px; padding: 0 .7em;">
		<p><span class="ui-icon ui-icon-info" style="float: left; margin-right: .3em;"></span>
		<strong>Felicitaciones!</strong> El modulo ha sido instalado satisfactoriamente.</p>
	</div>
</div>
  <?php
  }
 ?>
<form id="form1" name="form1" method="post" action="#">
  <table width="804" border="0" style="float:left;">
    <tr>
      <td width="525" valign="top">&nbsp; </td>
    </tr>
    <tr>
      <td valign="top"><div align="right"><a href="<?php $_SERVER['PHP_SELF'];?>?p=modules/orderslite/view/index.php&amp;install=true" class="btn_normal">Instalar</a></div></td>
    </tr>
  </table>
</form>
</div>
<?php } ?>

