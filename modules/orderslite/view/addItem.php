<?php
    include('includes_self.php');
	
		
 	$id=$obj->postVars('id');
	$qty=$obj->postVars('qty');
	$id_order=$obj->postVars('id_order');
	
	$row=$products->getProductsById($id);
	
	if(count($row)>0)
	{
	     foreach($row as $row)
    	 {
    	     $product_id=$row["pro_id"];
    	     $product_name=$row["pro_name"];
    	     $product_price=$row["pro_price"];
    	     $id_tax=$row["prot_id"];
    	 }
    	 $tax = $products->calcTaxValue($product_price, $id_tax);
    	 
    	 if($id_order == '')
    	 {
    	     $obj->newOrderLiteDetailTmp($product_id, $product_name, $qty, $product_price, $tax, $_SESSION['user_id']);
    	 }
    	 else
    	 {
    	     $obj->newOrderLiteDetail($id_order, $product_id, $product_name, $qty, $product_price, $tax, $id_user);
    	 }
	}
	
	if($id_order == '')
	{
    	$row=$obj->getOrderLiteDetailTemp();
    	$html.='
        	 <table>
        	    <tr>
                    <th>Producto</th>
                    <th>Cantidad</th>
                    <th>Precio</th>
                    <th>Tax</th>
                    <th>Total</th>
                    <th>&nbsp;</th>
                </tr>
        	';
        	
        	if(count($row)>0)
        	{
        	    foreach($row as $row)
        	    {
        	        $html.="
        	            <tr>
                            <td>".$row['oldt_productname']."</td>
                            <td>".$row['oldt_qty']."</td>
                            <td>$".number_format($row['oldt_price'],2)."</td>
                            <td>$".number_format($row['oldt_tax'],2)."</td>
                            <td>$".number_format($row['oldt_qty']*($row['oldt_price']+$row['oldt_tax']),2)."</td>
                            <td><a href='javascript:;' class='btn_2' onClick='delItem(".$row['oldt_id'].")'>Eliminar</a></td>
                        </tr>";
        	    }
        	}
        	
        	$html.="</table>";
        	echo $html;
	}
	else
	{
	    $row=$obj->getOrderLiteDetail($id_order);
    	$html.='
        	 <table>
        	    <tr>
                    <th>Producto</th>
                    <th>Cantidad</th>
                    <th>Precio</th>
                    <th>Tax</th>
                    <th>Total</th>
                    <th>&nbsp;</th>
                </tr>
        	';
        	
        	if(count($row)>0)
        	{
        	    foreach($row as $row)
        	    {
        	        $html.="
        	            <tr>
                            <td>".$row['old_productname']."</td>
                            <td>".$row['old_qty']."</td>
                            <td>$".number_format($row['old_price'],2)."</td>
                            <td>$".number_format($row['old_tax'],2)."</td>
                            <td>$".number_format($row['old_qty']*($row['old_price']+$row['old_tax']),2)."</td>
                            <td><a href='javascript:;' class='btn_2' onClick='delItem2(".$row['old_id'].",".$id_order.")'>Eliminar</a></td>
                        </tr>";
        	    }
        	}
        	
        	$html.="</table>";
        	echo $html;
	}

?>
