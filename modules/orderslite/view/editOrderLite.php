<?php
 include('includes.php');
 
 $msg=false;
 
 if($_POST)
 {
     $obj->editOrderLite();
     $msg=true;
 }
 
 $id = $obj->getVars('id');
 $row = $obj->getOrderLiteById($id);
 foreach($row as $row)
 {
     $name = $row['ol_customername'];
     $email = $row['ol_customeremail'];
     $phone = $row['ol_customerphone'];
     $desc = $row['ol_desc'];
     $obs = $row['ol_obs'];
     $deliverdate = $row['ol_deliverdate'];
     $deliverhour = $row['ol_deliverhour'];
     $id_store = $row['id_store'];
     $abono = $row['ol_prepayment'];
     $id_pt = $row['ol_paymenttype'];
 }

?>
<script type="text/javascript" src="modules/orderslite/js/jsOlFunctions.js"></script>
<div class="widget3">
 <div class="widgetlegend">Editar Orden </div>
 <?php
  if($msg)
  {
  ?>
   <div class="ui-widget">
	<div class="ui-state-highlight ui-corner-all" style="margin-top: 20px; padding: 0 .7em;">
		<p><span class="ui-icon ui-icon-info" style="float: left; margin-right: .3em;"></span>
		<strong>Felicitaciones!</strong> El slide ha sido creado satisfactoriamente.</p>
	</div>
</div>
  <?php
  }
 ?>
<p style="width:100">
	<a href="<?php $_SERVER['PHP_SELF'];?>?p=modules/orderslite/view/index.php" class="btn_normal" style="float:left; margin:5px;">Volver </a>
</p>
 
<form action="#" method="post" enctype="multipart/form-data" name="form1" id="form1">
<table width="803" height="48" border="0" style="float:left">
  <tr>
      <td valign="top">
          <table>
              <tr>
                  <td><input type="text" name="prodname" id="prodname"></td>
                  <td><a href="javascript:;" class="btn_1" onClick="findProductByName2(<?php echo $id?>)">Buscar</a></td>
              </tr>
              <tr>
                  <td colspan="2"><div id="listprod2"></div></td>
              </tr>
          </table>
      </td>
      <td valign="top">
          <div id="listprod">
              <table>
                  <tr>
                      <th>Producto</th>
                      <th>Cantidad</th>
                      <th>Precio</th>
                      <th>Tax</th>
                      <th>Total</th>
                      <th>&nbsp;</th>
                  </tr>
                  <?php
                    $row = $obj->getOrderLiteDetail($id);
                    if(count($row)>0)
                    {
                        foreach($row as $row)
                        {
                            ?>
                            <tr>
                                <td><?php echo $row['old_productname']?></td>
                                <td><?php echo $row['old_qty']?></td>
                                <td>$<?php echo number_format($row['old_price'],2)?></td>
                                <td>$<?php echo number_format($row['old_tax'],2)?></td>
                                <td>$<?php echo number_format($row['old_qty']*($row['old_price']+$row['old_tax']),2)?></td>
                                <td><a href="javascript:;" class="btn_2" onClick="delItem2(<?php echo $row['old_id']?>, <?php echo $id?>)">Eliminar</a></td>
                            </tr>
                            <?php
                        }
                    }
                  ?>
                  <tr>
                      
                  </tr>
              </table>
          </div>
      </td>
      <td valign="top">
          <table>
              <tr>
                  <td><input type="text" name="name" placeholder="Nombre de cliente" value="<?php echo $name?>" required/></td>
              </tr>
              <tr>
                  <td><input type="text" name="email" placeholder="Email de cliente" value="<?php echo $email?>" required/></td>
              </tr>
              <tr>
                  <td><input type="text" name="phone" placeholder="Telefono de cliente" value="<?php echo $phone?>" required/></td>
              </tr>
              <tr>
                  <td><textarea name="desc" cols="40" rows="6" placeholder="Descripcion del pedido" required><?php echo $desc?></textarea></td>
              </tr>
              <tr>
                  <td><textarea name="obs" cols="40" rows="6" placeholder="Observaciones del pedido"><?php echo $obs?></textarea></td>
              </tr>
              <tr>
                  <td><input type="text" name="deliverdate" placeholder="fecha de entrega" value="<?php echo $deliverdate?>" required/></td>
              </tr>
              <tr>
                  <td><input type="time" name="deliverhour" placeholder="Hora de entrega" value="<?php echo $deliverhour?>" required/></td>
              </tr>
              <tr>
                  <td><input type="text" name="abono" placeholder="Abono al pedido" value="<?php echo $abono?>" required/></td>
              </tr>
              <tr>
                  <td>
                      <select name="id_pt" id="id_pt" required>
                         <option>Tipo de pago</option>
                         <?php
                            $row = $obj->getPaymentType();
                            if(count($row)>0)
                            {
                                foreach($row as $row)
                                {
                                    if($id_pt == $row['olpt_id'])
                                    {
                                        ?>
                                        <option value="<?php echo $row['olpt_id']?>" selected><?php echo $row['olpt_name']?></option>
                                        <?php
                                    }
                                    else
                                    {
                                        ?>
                                        <option value="<?php echo $row['olpt_id']?>"><?php echo $row['olpt_name']?></option>
                                        <?php 
                                    }
                                    
                                }
                            }
                         ?> 
                      </select>
                  </td>
              </tr>
              <tr>
                  <td><select name="id_store" required>
                      <option value="">--Pedido para---</option>
                      <?php
                        $row = $pos->getStore();
                        if(count($row)>0)
                        {
                            foreach($row as $row)
                            {
                                if($row['poss_id'] == $id_store)
                                {
                                   ?>
                                    <option value="<?php echo $row['poss_id']?>" selected><?php echo $row['poss_name']?></option>
                                    <?php 
                                }
                                else
                                {
                                   ?>
                                    <option value="<?php echo $row['poss_id']?>"><?php echo $row['poss_name']?></option>
                                    <?php 
                                }
                                
                            }
                        }
                      ?>
                  </select></td>
              </tr>
              <tr>
                  <td>
                      <input type="hidden" name="id" value="<?php echo $id?>" />
                      <input type="submit" value="Guardar" class="btn_submit"/></td>
              </tr>
          </table>
      </td>
  </tr>
</table>
</form>


</div>
<script>
    function delItem2(id, id_order){

	$.ajax({
	 type:'POST',
	 url:'modules/orderslite/view/delItem.php',
	 async:false,
	 data: 'id='+id+"&id_order="+id_order,
	 success:function(data){
	     
		document.getElementById('listprod').innerHTML=data;
	 }
	});
 }
</script>
