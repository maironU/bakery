<?php
class orderslite extends GeCore{

	public function newOrderLite()
	{
		$name = $this->postVars('name');
		$email = $this->postVars('email');
		$phone = $this->postVars('phone');
		$desc = $this->postVars('desc');
		$obs = $this->postVars('obs');
		$deliverdate = $this->postVars('deliverdate');
		$deliverhour = $this->postVars('deliverhour');
		$id_store = $this->postVars('id_store');
		$id_pt = $this->postVars('id_pt');
		$abono = $this->postVars('abono');
		
		$track = $this->getTrack();
		$today = date('Y-m-d H:i:s');
		
		$sql="insert into `orderslite` (`ol_customername`, `ol_customeremail`, `ol_customerphone`, `ol_desc`, `ol_obs`, `ol_track`, `ol_date`, `ol_deliverdate`, `ol_deliverhour`, `u_id`, `ol_state`, `id_store`, `ol_prepayment`, `ol_paymentype`) values ('$name','$email','$phone','$desc','$obs','$track','$today','$deliverdate','$deliverhour','".$_SESSION['user_id']."','1','$id_store','$abono','$id_pt')";
		$this->Execute($sql);
		
		$id=$this->getLastID();
		
		return $id;
	}
	
	public function getTrack()
	{
	    $str = "ABCDEFGHIJKLMNOPQRSTXYZ0987654321";
	    for($i=1; $i <= 6; $i++)
	    {
	        $pos = rand(0, strlen($str));
	        $cad .= $str[$pos];
	    }
	    
	    return $cad;
		
	}
	
	public function editOrderLiteTotal($id, $total, $tax=0)
	{
	    $sql="update `orderslite` set `ol_total`='$total', `ol_tax`='$tax' where ol_id='$id'";
		$this->Execute($sql);
	}
	
	public function editOrderLite()
	{
		$id=$this->postVars('id');
		$name = $this->postVars('name');
		$email = $this->postVars('email');
		$phone = $this->postVars('phone');
		$desc = $this->postVars('desc');
		$obs = $this->postVars('obs');
		$deliverdate = $this->postVars('deliverdate');
		$deliverhour = $this->postVars('deliverhour');
		$id_store = $this->postVars('id_store');
		$id_pt = $this->postVars('id_pt');
		$abono = $this->postVars('abono');
		
		$sql="update `orderslite` set `ol_customername`='$name', `ol_customeremail`='$email', `ol_customerphone`='$phone', `ol_desc`='$desc', `ol_obs`='$obs', `ol_deliverdate`='$deliverdate', `ol_deliverhour`='$deliverhour', `id_store`='$id_store', `ol_prepayment`='$abono', `ol_paymentype`='$id_pt' where ol_id='$id'";
		$this->Execute($sql);
		
		$t = 0;
		$tax = 0;
		
		$row = $this->getOrderLiteDetail($id);
		if(count($row)>0)
		{
		    foreach($row as $row)
		    {
		        $t += $row['old_qty'] * ($row['old_price']+$row['old_tax']);
		        $tax += $row['old_qty'] * ($row['old_tax']);
		    }
		}
		
		$this-> editOrderLiteTotal($id, $t, $tax);
		
	}
	
	public function editOrderLiteState($id, $state)
	{
	    $sql="update `orderslite` set `ol_state`='$state' where ol_id='$id'";
		$this->Execute($sql);
	}
	
	public function delOrderLite()
	{
		$id=$this->getVars('id');
		
		$sql="delete from `orderslite_detail` where ol_id='$id'";
		$this->Execute($sql);
		
		$sql="delete from `orderslite` where ol_id='$id'";
		$this->Execute($sql);
	}
	
	
	public function getOrderLite()
	{
	    $sql="select * from `orderslite` order by ol_date desc";
	    return $this->ExecuteS($sql);
	}
	
	public function getOrderLiteById($id)
	{
	  $sql="select * from `orderslite` where ol_id='$id'";
	  return $this->ExecuteS($sql);
	}
	
	public function getOrderLiteByStoreAndState($id_store, $state)
	{
	    $sql="select * from `orderslite` where id_store='$id_store' and ol_state='$state' order by ol_deliverdate asc, ol_deliverhour asc";
	    return $this->ExecuteS($sql);
	}
	
	public function printOrdersLite($id, $path='../formats/')
	{
	    $row = $this->getOrderLiteById($id);
	    if(count($row)>0)
	    {
	        foreach($row as $row)
	        {
	            $name = $row['ol_customername'];
	            $email = $row['ol_customeremail'];
	            $phone = $row['ol_customerphone'];
	            $track = $row['ol_track'];
	            $date = $row['ol_date'];
	            $desc = $row['ol_desc'];
	            $obs = $row['ol_obs'];
	            $deliverdate = $row['ol_deliverdate'];
	            $deliverhour = $row['ol_deliverhour'];
	            $id_user = $row['u_id'];
	        }
	    }
	    
	    $sql = "select * from user where u_id='$id_user'";
	    $row = $this->ExecuteS($sql);
	    if(count($row)>0)
	    {
	        foreach($row as $row)
	        {
	            $name_user = $row['u_nom'];
	        }
	    }
	    
	    $row = $this->getOrderLiteDetail($id);
	    if(count($row)>0)
	    {
	        foreach($row as $row)
	        {
	            $det .= '
	                <tr>
	                    <td>'.$row['old_productname'].'</td>
	                    <td>'.$row['old_qty'].'</td>
	                    <td>$'.number_format($row['old_price'],2).'</td>
	                    <td>$'.number_format($row['old_tax'],2).'</td>
	                    <td>$'.number_format(($row['old_price']+$row['old_tax'])*$row['old_qty'],2).'</td>
	                </tr>
	            ';
	        }
	    }
	    
	    $f=fopen($path.'orderformat.html','r');
	    $html=fread($f,filesize($path.'/orderformat.html'));
	    
	    $html=str_replace('[name]',$name,$html);
	    $html=str_replace('[phone]',$phone,$html);
	    $html=str_replace('[email]',$email,$html);
	    $html=str_replace('[track]',$track,$html);
	    $html=str_replace('[date]',$date,$html);
	    $html=str_replace('[desc]',$desc,$html);
	    $html=str_replace('[obs]',$obs,$html);
	    $html=str_replace('[deliverdate]',$deliverdate,$html);
	    $html=str_replace('[deliverhour]',$deliverhour,$html);
	    $html=str_replace('[detalle]',$det,$html);
	    $html=str_replace('[usrname]',$name_user,$html);
	    
	    $crmModelObj=new crmScs();
	    $crmModelObj->connect();
	 
    	 $row=$crmModelObj->getCompanyById(2);
    	 if(count($row)>0)
    	 {
    	     foreach($row as $row)
    	     {
    	        $comInf.='<table width="163" border="0" align="center">
                  <tr>
                    <td><div align="center"><img src="../../crm/Logos/'.$row["e_img"].'" /><br />
                    </div></td>
                  </tr>
                  <tr>
                    <td><div align="center">'.$row["e_info"].'</div></td>
                  </tr>
                </table>';
    	    }
	    }
	    
	    $html=str_replace('[company]',$comInf,$html);
	    
	    return $html;
	    
	    
	}
	
	//Order Lite Detail Temp Methods
	public function newOrderLiteDetailTmp($id_product, $product_name, $qty, $price, $tax, $id_user)
	{
	    $sql = "insert into `orderslite_detail_temp`(`oldt_productname`, `id_product`, `oldt_qty`, `oldt_price`, `oldt_tax`, `u_id`) values ('$product_name','$id_product','$qty','$price','$tax','".$_SESSION['user_id']."')";
	    $this->Execute($sql);
	}
	
	public function delOrderLiteDetailTemp($id)
	{
	    $sql = "delete from `orderslite_detail_temp` where oldt_id='$id'";
	    $this->Execute($sql);
	}
	
	
	public function getOrderLiteDetailTemp()
	{
	    $sql = "select * from `orderslite_detail_temp` where u_id='".$_SESSION['user_id']."'";
	    return $this->ExecuteS($sql);
	}
	
	public function flushOrderLiteDetailTemp()
	{
	    $sql = "delete from `orderslite_detail_temp` where u_id='".$_SESSION['user_id']."'";
	    $this->Execute($sql);
	}
	
	//Order Lite Detail Methods
	public function newOrderLiteDetail($id, $id_product, $product_name, $qty, $price, $tax, $id_user)
	{
	    $sql = "insert into `orderslite_detail`(`ol_id`, `old_productname`, `id_product`, `old_qty`, `old_price`, `old_tax`) values ('$id','$product_name','$id_product','$qty','$price','$tax')";
	    $this->Execute($sql);
	}
	
	public function delOrderLiteDetail($id)
	{
	    $sql = "delete from `orderslite_detail` where old_id='$id'";
	    $this->Execute($sql);
	}
	
	public function getOrderLiteDetail($id)
	{
	    $sql = "select * from `orderslite_detail` where ol_id='$id'";
	    return $this->ExecuteS($sql);
	}
	
	//miselanio
	public function formatHour($hour)
	{
	   
	    $h = explode(':', $hour);

	    $h1 = (int) $h[0];
	    if($h1>=0 && $h1<=11)
	    {
	        $t1 = "a.m.";
	    }
	    
	    if($h1>=12 && $h1<=23)
	    {
	        $t1 = "p.m.";
	    }
	    
	    switch($h[0])
	    {
	        case '00': $h2 = '12';
	        break;
	        case '01': $h2 = '01';
	        break;
	        case '02': $h2 = '02';
	        break;
	        case '03': $h2 = '03';
	        break;
	        case '04': $h2 = '04';
	        break;
	        case '05': $h2 = '05';
	        break;
	        case '06': $h2 = '06';
	        break;
	        case '07': $h2 = '07';
	        break;
	        case '08': $h2 = '08';
	        break;
	        case '09': $h2 = '09';
	        break;
	        case '10': $h2 = '10';
	        break;
	        case '11': $h2 = '11';
	        break;
	        case '12': $h2 = '12';
	        break;
	        case '13': $h2 = '01';
	        break;
	        case '14': $h2 = '02';
	        break;
	        case '15': $h2 = '03';
	        break;
	        case '16': $h2 = '04';
	        break;
	        case '17': $h2 = '05';
	        break;
	        case '18': $h2 = '06';
	        break;
	        case '19': $h2 = '07';
	        break;
	        case '20': $h2 = '08';
	        break;
	        case '21': $h2 = '09';
	        break;
	        case '22': $h2 = '10';
	        break;
	        case '23': $h2 = '11';
	        break;
	    }
	    
	    return $h2.":".$h[1]." ".$t1;
	    
	    
	}
	
	public function getPaymentType()
	{
	    $sql = "SELECT * FROM `orderslite_paymenttype`";
	    return $this->ExecuteS($sql);
	}
	
	public function getPaymentTypeValue($id)
	{
	    $sql = "SELECT * FROM `orderslite_paymenttype` WHERE olpt_id='$id'";
	    $row = $this->ExecuteS($sql);
	    if(count($row)>0)
	    {
	        foreach($row as $row)
	        {
	            return $row['olpt_name'];
	        }
	    }
	}
	
	
	//Installers
   public function Checker(){
    return $this->chkTables('orderslite');
   }
   
   public function install(){
    $sql='CREATE TABLE `orderslite` (
  `ol_id` int(11) NOT NULL auto_increment,
  `ol_customername` varchar(255) NOT NULL,
  `ol_customeremail` varchar(255) NOT NULL,
  `ol_customerphone` varchar(255) NOT NULL,
  `ol_desc` varchar(255) NOT NULL,
  `ol_obs` varchar(255) NOT NULL,
  `ol_track` varchar(255) NOT NULL,
  `ol_date` datetime NOT NULL,
  `ol_deliverdate` date NOT NULL,
  `ol_deliverhour` varchar(255) NOT NULL,
  `u_id` int(11) NOT NULL,
  PRIMARY KEY  (`ol_id`)
) ENGINE=MyISAM  DEFAULT CHARSET=utf8 AUTO_INCREMENT=1 ;';
	
	$this->Execute($sql);
	
	$sql='CREATE TABLE `orderslite_detail` (
  `old_id` int(11) NOT NULL auto_increment,
  `old_productname` varchar(255) NOT NULL,
  `id_product` int(11) NOT NULL,
  `old_qty` varchar(255) NOT NULL,
  `old_price` varchar(255) NOT NULL,
  `old_tax` varchar(255) NOT NULL,
  `ol_id` int(11) NOT NULL,
  PRIMARY KEY  (`old_id`)
) ENGINE=MyISAM  DEFAULT CHARSET=utf8 AUTO_INCREMENT=1 ;';

$this->Execute($sql);

$sql='CREATE TABLE `orderslite_detail_temp` (
  `oldt_id` int(11) NOT NULL auto_increment,
  `oldt_productname` varchar(255) NOT NULL,
  `id_product` int(11) NOT NULL,
  `oldt_qty` varchar(255) NOT NULL,
  `oldt_price` varchar(255) NOT NULL,
  `oldt_tax` varchar(255) NOT NULL,
  `u_id` int(11) NOT NULL,
  PRIMARY KEY  (`oldt_id`)
) ENGINE=MyISAM  DEFAULT CHARSET=utf8 AUTO_INCREMENT=1 ;';
	
	$this->Execute($sql);
	
	$sql = "ALTER TABLE `orderslite` ADD `ol_state` INT NOT NULL AFTER `u_id`, ADD `id_store` INT NOT NULL AFTER `ol_state`;";
	$this->Execute($sql);
	
	$sql = "ALTER TABLE `orderslite` ADD `ol_total` VARCHAR(255) NOT NULL AFTER `id_store`, ADD `ol_prepayment` VARCHAR(255) NOT NULL AFTER `ol_total`, ADD `ol_paymentype` VARCHAR(255) NOT NULL AFTER `ol_prepayment`;";
	$this->Execute($sql);
	
	$sql = "CREATE TABLE `orderslite_paymenttype` (
      `olpt_id` int(11) NOT NULL PRIMARY KEY auto_increment,
      `olpt_name` varchar(255) NOT NULL
    ) ENGINE=MyISAM DEFAULT CHARSET=latin1;";
    $this->Execute($sql);
    
    $sql = "INSERT INTO `orderslite_paymenttype` (`olpt_id`, `olpt_name`) VALUES
    (1, 'Efectivo'),
    (2, 'Tarjeta de Credito'),
    (3, 'Tarjeta debito'),
    (4, 'Otros');";
    $this->Execute($sql);
    
    $sql = "ALTER TABLE `orderslite` ADD `ol_tax` VARCHAR(255) NOT NULL AFTER `ol_paymentype`;";
    $this->Execute($sql);
	
   }
   
   public function uninstall(){
    $sql='DROP TABLE `orderslite`, `orderslite_detail`, `orderslite_detail_temp`';
	$this->Execute($sql);
   }
	
};
?>
