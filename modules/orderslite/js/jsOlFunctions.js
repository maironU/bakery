// JavaScript Document

 function showInfo(id,site)
 {
	$.fancybox.open({
					href : site+'?id='+id,
					type : 'iframe',
					padding:5,
					afterClose: function () { // USE THIS IT IS YOUR ANSWER THE KEY WORD IS "afterClose"
                		parent.location.reload(true);
					}
	}); 
 }
 
 function findPosList(){
	document.getElementById('gauch2').style.display='block';
	var barcode=document.getElementById('barcode').value;
	var qty=document.getElementById('qty').value;
	document.getElementById('cash').value = 0;
	document.getElementById('devol').value = 0;
	document.getElementById('devol1').value = 0;
	$.ajax({
	 type:'POST',
	 url:'modules/posmodule/view/addPosItem.php',
	 async:false,
	 data: 'barcode='+barcode+"&qty="+qty,
	 success:function(data){
	     
		 document.getElementById('gauch2').style.display='none';
		 document.getElementById('showposL').innerHTML=data;
		 findPosTotal();
		 findPosTotal2();
	 }
	});
 }
 
 function findPosList2(id, qty1)
 {
    var qty = document.getElementById('qty'+id).value
	$.ajax({
	 type:'POST',
	 url:'modules/orderslite/view/addItem.php',
	 async:false,
	 data: 'id='+id+"&qty="+qty,
	 success:function(data){
		 document.getElementById('listprod').innerHTML=data;
	 }
	});
 }
 
 function findPosList3(id, qty1, id_order)
 {
    var qty = document.getElementById('qty'+id).value
	$.ajax({
	 type:'POST',
	 url:'modules/orderslite/view/addItem.php',
	 async:false,
	 data: 'id='+id+"&qty="+qty+"&id_order="+id_order,
	 success:function(data){
		 document.getElementById('listprod').innerHTML=data;
	 }
	});
 }
 
 function delItem(id){
	$.ajax({
	 type:'POST',
	 url:'modules/orderslite/view/delItem.php',
	 async:false,
	 data: 'id='+id,
	 success:function(data){
		 document.getElementById('listprod').innerHTML=data;
	 }
	});
 }
 
 function editQtyItem(id, qty, action){
     //alert(action);
	document.getElementById('gauch2').style.display='block';
	document.getElementById('cash').value = 0;
	document.getElementById('devol').value = 0;
	document.getElementById('devol1').value = 0;
	$.ajax({
	 type:'POST',
	 url:'modules/posmodule/view/editPosQtyItem.php',
	 async:false,
	 data: 'id='+id+'&qty='+qty+'&action='+action,
	 success:function(data){
	     //alert(data);
		 document.getElementById('gauch2').style.display='none';
		 document.getElementById('showposL').innerHTML=data;
		 findPosTotal();
		 findPosTotal2();
	 }
	});
 }
 
 function findProductByCategory(id_cat)
 {
    $.ajax({
	 type:'POST',
	 url:'modules/posmodule/view/findProdCat.php',
	 async:false,
	 data: 'id_cat='+id_cat,
	 success:function(data){
		 document.getElementById('listprod').innerHTML=data;
	 }
	}); 
 }
 
 function findProductByName()
 {
    var name = document.getElementById('prodname').value; 
    $.ajax({
	 type:'POST',
	 url:'modules/orderslite/view/findProdName.php',
	 async:false,
	 data: 'name='+name,
	 success:function(data){
		 document.getElementById('listprod2').innerHTML=data;
	 }
	}); 
 }
 
 function findProductByName2(id)
 {
    var name = document.getElementById('prodname').value; 
    $.ajax({
	 type:'POST',
	 url:'modules/orderslite/view/findProdName.php',
	 async:false,
	 data: 'name='+name+"&id="+id,
	 success:function(data){
		 document.getElementById('listprod2').innerHTML=data;
	 }
	}); 
 }
 
  function findPosTotal(){
	
	$.ajax({
	 type:'POST',
	 url:'modules/posmodule/view/findPosTotal.php',
	 async:false,
	 success:function(data){
		 document.getElementById('totalpos').innerHTML=data;
		 document.getElementById('totalpos2').innerHTML=data;
	 }
	});
 }
 
 function findPosTotal2(){
	
	$.ajax({
	 type:'POST',
	 url:'modules/posmodule/view/findPosTotal2.php',
	 async:false,
	 success:function(data){
		 document.getElementById('total').value=data;
	 }
	});
 }
 
 function closePos(id_payment)
 {
     var cash = document.getElementById('cash').value;
     var devol = document.getElementById('devol').value;
     var u_id = document.getElementById('u_id').value;
     var txs = document.getElementById('txs'+id_payment).value;
	
	$.ajax({
	 type:'POST',
	 url:'modules/posmodule/view/closePos.php',
	 async:false,
	 data: 'cash='+cash+"&devol="+devol+"&u_id="+u_id+"&txs="+txs+"&id_payment="+id_payment,
	 success:function(data){
		 if(data !== '')
		 {
		     showInfo(data,'modules/posmodule/view/showReceipt.php');
		 }
	 }
	});
 }
 
 function enableBtn(id)
 {
     document.getElementById('payment'+id).style.display = 'block';
 }
 
