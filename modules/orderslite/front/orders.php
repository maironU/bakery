<?php
    include('../view/includes_self.php');
    
    if($obj->getVars('ActionState'))
    {
        $state = $obj->getVars('state');
        $id = $obj->getVars('id');
        $obj->editOrderLiteState($id, $state);
    }
    
    $id_store = $obj->getVars('id_store');
    
    $row = $pos->getStoreById($id_store);
    if(count($row)>0)
    {
        foreach($row as $row)
        {
            $store_name = $row['poss_name'];
        }
    }
?>
<!DOCTYPE HTML>
<!--
	Strata by HTML5 UP
	html5up.net | @n33co
	Free for personal and commercial use under the CCA 3.0 license (html5up.net/license)
-->
<html>
	<head>
		<title>Pedidos para <?php echo $store_name?></title>
		<meta http-equiv="content-type" content="text/html; charset=utf-8" />
		<meta name="description" content="" />
		<meta name="keywords" content="" />
		<!--[if lte IE 8]><script src="css/ie/html5shiv.js"></script><![endif]-->
		<script src="js/jquery.min.js"></script>
		<script src="js/jquery.poptrox.min.js"></script>
		<script src="js/skel.min.js"></script>
		<script src="js/init.js"></script>
		<noscript>
			<link rel="stylesheet" href="css/skel.css" />
			<link rel="stylesheet" href="css/style.css" />
			<link rel="stylesheet" href="css/style-xlarge.css" />
		</noscript>
		<!--[if lte IE 8]><link rel="stylesheet" href="css/ie/v8.css" /><![endif]-->
		<meta http-equiv="refresh" content="60">
	</head>
	<body id="top">
	        <section>
	                        <p>&nbsp;</p>
							<h4><?php echo $store_name?></h4>
							<div class="table-wrapper">
								<table>
									<thead>
										<tr>
											<th>ID</th>
											<th>Estado</th>
											<th>Fecha</th>
											<th>Track</th>
											<th>Cliente</th>
											<th>Descripcion</th>
											<th>Fecha/Hora Entrega</th>
											<th>Realizado por</th>
											<th>&nbsp;</th>
										</tr>
									</thead>
									<tbody>
										<?php
										    $row = $obj->getOrderLiteByStoreAndState($id_store, 1);
										    if(count($row)>0)
										    {
										        foreach($row as $row)
										        {
										            ?>
										                <tr>
                											<td><?php echo $row["ol_id"];?></td>
                											<td>
                											    <?php
                                                                    if($row['ol_state'] == 1)
                                                                    {
                                                                        ?>
                                                                        <span style="color: #f00">Pendiente</span>
                                                                        <?php
                                                                    }
                                                                    else
                                                                    {
                                                                        ?>
                                                                        <span style="color: #0f0">Terminado</span>
                                                                        <?php
                                                                    }
                                                                ?>
                                                            </td>
                											<td><?php echo $row["ol_date"];?></td>
                											<td><?php echo $row["ol_track"];?></td>
                											<td><?php echo $row["ol_customername"];?></td>
                											<td><?php echo $row["ol_desc"];?></td>
                											<td><?php echo $row["ol_deliverdate"]." ".$obj->formatHour($row['ol_deliverhour']);?></td>
                											<td><?php $usr->getUserById($row['u_id']);
                											    echo $usr->name;
                											?></td>
                											<td><a href="orders.php?id_store=<?php echo $id_store?>&id=<?php echo $row["ol_id"]?>&ActionState=true&state=0" class="button special">Terminado</a></td>
                										</tr>
										            <?php
										        }
										    }
										?>
										
									</tbody>
									<tfoot>
										
									</tfoot>
								</table>
							</div>

						</section>

		
			

	</body>
</html>