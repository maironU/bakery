<?php
class linker extends GeCore{
	
	
	public function newLinker(){
		$name=$this->postVars('name');
		$link=$this->postVars('link');
		$awe=$this->postVars('awe');
		$color=$this->postVars('color');

		$sql="insert into `linker` (`l_name`, `l_link`, `l_image`, `l_awe`, `l_color`) values ('$name','$link','','$awe','$color')";
		$this->Execute($sql);
		
		$id=$this->getLastID();
		
		$this->uploadImg($id);
	}
	
	public function uploadImg($id){
		
		$ok=$this->upLoadFileProccess('img', 'modules/linker/imgLinker');
		
		if($ok){
		 $sql="update linker set l_image='".$this->fname."' where l_id='$id'";
		 $this->Execute($sql);
		}
		
	}
	
	public function editLinker(){
		$id=$this->postVars('id');
		$name=$this->postVars('name');
		$link=$this->postVars('link');
		$awe=$this->postVars('awe');
		$color=$this->postVars('color');
		
		$sql="update `blocksn` set `l_name`='$name', `l_link`='$link', `l_awe`='$awe', `l_color`='$color' where `l_id`='$id'";
		$this->Execute($sql);
		
		$this->uploadImg($id);
	}
	
	public function delLinker(){
		$id=$this->getVars('id');
		
		$sql="delete from `linker` where l_id='$id'";
		$this->Execute($sql);
	}
	
	
	public function getLinker(){
	 $sql="select * from linker";
	 return $this->ExecuteS($sql);
	}
	
	public function geLinkerById($id){
	 
	  $sql="select * from linker where l_id='$id'";
	  return $row=$this->ExecuteS($sql);
	}
	
	//Installers
   public function Checker(){
    return $this->chkTables('linker');
   }
   
   public function install(){
    $sql='CREATE TABLE `linker` (
    `l_id` INT NOT NULL AUTO_INCREMENT PRIMARY KEY ,
    `l_name` VARCHAR( 255 ) NOT NULL ,
    `l_link` VARCHAR( 255 ) NOT NULL ,
    `l_image` VARCHAR( 255 ) NOT NULL ,
    `l_awe` VARCHAR( 255 ) NOT NULL
    ) ENGINE = MYISAM ;';
	
	$this->Execute($sql);
	
	$sql = "ALTER TABLE `linker` ADD `l_color` VARCHAR(255) NOT NULL AFTER `l_awe`;";
	$this->Execute($sql);
   }
   
   public function uninstall(){
    $sql='DROP TABLE `linker`';
	$this->Execute($sql);
   }
	
};
?>
