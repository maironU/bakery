<?php
   include('modules/linker/model/linker.php');
 
 $linker = new linker();
 $linker->connect();
?>
<hr>
<h3>Enlaces de Interes</h3>
<div id="desk">
        <?php
            $row=$linker->getLinker();
            if(count($row)>0)
            {
              foreach($row as $row)
              {
                ?>
                <a href="<?php echo $row['l_link']; ?>" target="_blank">
                  <div class="box" style="background: <?php echo $row['l_color']; ?>">
                      <p class="m-0 d-flex flex-column justify-content-center align-items-center"><i class="fa <?php echo $row['l_awe']; ?> fa-4x"></i><br><br>
                        <span><?php echo $row['l_name']; ?></span>
                      </p>
                  </div>
                </a>
                    
                <?php
              }  
            }
        ?>
</div>