<?php
  include('modules/linker/model/linker.php');
 
 $obj = new linker();
 $obj->connect();
 
 $msg=false;
 
 if($_POST){
  $obj->editLinker();
  $msg=true;
 }
 
 $id=$obj->getVars('id');
 $row=$obj->getLinkerById($id);
 foreach($row as $row){
  $name=$row['l_name'];
  $link=$row['l_link'];
  $image=$row['l_image'];
  $awe=$row["l_awe"];
  $color=$row["l_color"];
 }
?>
<div class="widget3">
 <div class="widgetlegend">Editar Enlace</div>
<?php 
  if($msg)
  {
  ?>
   <div class="ui-widget">
	<div class="ui-state-highlight ui-corner-all" style="margin-top: 20px; padding: 0 .7em;">
		<p><span class="ui-icon ui-icon-info" style="float: left; margin-right: .3em;"></span>
		<strong>Felicitaciones!</strong> Se ha sido guardado satisfactoriamente.</p>
	</div>
</div>
  <?php
  }
 ?>
<p style="width:100">
	<a href="<?php $_SERVER['PHP_SELF'];?>?p=modules/linker/view/index.php" class="btn_normal" style="float:left; margin:5px;">Volver </a>
</p>
 <br /><br /><br /><br />
<form action="#" method="post" enctype="multipart/form-data" name="form1" id="form1">
<table width="803" height="48" border="0" align="center" style="float:left">
  <tr>
    <td><label>Nombre: </label><br />
      <input name="name" type="text" id="name" value="<?php echo $name; ?>" required/>    </td>
  </tr>
  <tr>
    <td><label>Url: </label><br />
      <input name="link" type="text" id="link" value="<?php echo $link; ?>" required/>    </td>
  </tr>
  <tr>
    <td><label>Color: </label><br />
      <input name="color" type="color" id="color" value="<?php echo $color; ?>" required/>    </td>
  </tr>
  <!--<tr>
    <td><label>Imagen: </label><br />
      <label>
      <input name="img" type="file" id="img" />
      <br />
      <a href="modules/blocksn/imgBlocks/<?php echo $image;?>"><img src="modules/blocksn/imgBlocks/<?php echo $image;?>" width="100" height="100"></a>      </label></td>
  </tr>-->
  <tr>
    <td><label>Font Awesome: </label><br />
      <label>
      <input name="awe" type="text" id="awe" value="<?php echo $awe?>" required/>
      </label></td>
  </tr>
  <tr>
    <td>
      <input name="id" type="hidden" id="id" value="<?php echo $id;?>" />
      <input type="submit" value="Guardar" class="btn_submit" />
    </td>
  </tr>
</table>
</form>


</div>
