<?php
class Banner extends GeCore{

	public $id;
	public $title;
	public $titlerng;
	public $url;
	public $image;
	public $text;
	public $texteng;
	public $active;
	public $block;
	
	
	//Banner's Methods
	public function newBanner(){
		$title=$this->postVars('title');
		$titleeng=$this->postVars('titleeng');
		$url=$this->postVars('url');
		$text=$this->postVars('text');
		$texteng=$this->postVars('texteng');
		$active=$this->postVars('active');
		$block=$this->postVars('block');
		
		$sql="insert into `banner` (`b_title`, `b_url`, `b_text`, `b_active`, `b_block`, `b_titleeng`, `b_texteng`) values ('$title','$url','$text','$active','$block','$titleeng','$texteng')";
		$this->Execute($sql);
		
		$id=$this->getLastID();
		
		$this->uploadBanner($id);
	}
	
	public function uploadBanner($id){
		
		$ok=$this->upLoadFileProccess('img', 'modules/banner/imgBanner');
		
		if($ok){
		 $sql="update banner set b_image='".$this->fname."' where b_id='$id'";
		 $this->Execute($sql);
		}
		
	}
	
	public function editBanner(){
		$id=$this->postVars('id');
		$title=$this->postVars('title');
		$titleeng=$this->postVars('titleeng');
		$url=$this->postVars('url');
		$text=$this->postVars('text');
		$texteng=$this->postVars('texteng');
		$active=$this->postVars('active');
		$block=$this->postVars('block');
		
		$sql="update `banner` set `b_title`='$title', `b_url`='$url', `b_text`='$text', `b_active`='$active', `b_block`='$block', `b_titleeng`='$titleeng', `b_texteng`='$texteng' where b_id='$id'";
		$this->Execute($sql);
		
		$this->uploadBanner($id);
	}
	
	public function delBanner(){
		$id=$this->getVars('id');
		
		$sql="delete from `banner` where b_id='$id'";
		$this->Execute($sql);
	}
	
	
	public function getBanner(){
	 $sql="select * from banner";
	 return $this->ExecuteS($sql);
	}
	
	public function getBannerById(){
	  $id=$this->getVars('id');
	  $sql="select * from banner where b_id='$id'";
	  $row=$this->ExecuteS($sql);
	  
	  foreach($row as $row){
	   $this->title=$row["b_title"];
	   $this->url=$row["b_url"];
	   $this->image=$row["b_image"];
	   $this->text=$row["b_text"];
	   $this->active=$row["b_active"];
	   $this->block=$row["b_block"];
	   $this->titleeng=$row["b_titleeng"];
	   $this->texteng=$row["b_texteng"];
	  }
	}
	
	public function getActiveBanner($block){
	    
	    $sql = "select * from banner as b 
	    inner join bannerblock as bb
	    on b.b_block = bb.bb_id
	    WHERE b.b_active='1' and b.b_block = '$block'";
	 	return $this->ExecuteS($sql);
	}
	
	//Block's Methods
	public function newBlock(){
	  $nom=$this->postVars('nom');
	  $w=$this->postVars('w');
	  $h=$this->postVars('h');
	  $content=$this->postVars('content');
	  
	  $sql="insert into `bannerblock` (`bb_nom`, `bb_w`, `bb_h`, `bb_content`) values ('$nom','$w','$h','$content')";
	  $this->Execute($sql);
	  
	  $id=$this->getLastID();
		
	  $this->uploadBannerBlock($id);
	  
	}
	
	public function uploadBannerBlock($id){
		
		$ok=$this->upLoadFileProccess('img', 'modules/banner/imgBannerBlock');
		
		if($ok){
		 $sql="update bannerblock set bb_image='".$this->fname."' where bb_id='$id'";
		 $this->Execute($sql);
		}
		
	}
	
	public function editBlock(){
	  $id=$this->postVars('id');
	  $nom=$this->postVars('nom');
	  $w=$this->postVars('w');
	  $h=$this->postVars('h');
	  $content=$this->postVars('content');
	  
	  $sql="update `bannerblock` set `bb_nom`='$nom', `bb_w`='$w', `bb_h`='$h', `bb_content`='$content' where bb_id='$id'";
	  $this->Execute($sql);
		
	  $this->uploadBannerBlock($id);
	  
	}
	
	public function delBlock(){
	 $id=$this->getVars('id');
	  
	  $sql="delete from `bannerblock` where bb_id='$id'";
	  $this->Execute($sql);
	}
	
	public function getBannerBlock(){
		$sql="select * from bannerblock";
	    return $this->ExecuteS($sql);
	}
	
	public function getBannerBlockById($id){
		$sql="select * from bannerblock where bb_id='$id'";
	    return $this->ExecuteS($sql);
	}
	
	//Installers
   public function Checker(){
    return $this->chkTables('banner');
   }
   
   public function install(){
    $sql='CREATE TABLE `banner` (
  `b_id` int(11) NOT NULL auto_increment,
  `b_title` varchar(255) NOT NULL,
  `b_url` varchar(255) NOT NULL,
  `b_image` varchar(255) NOT NULL,
  `b_text` longtext NOT NULL,
  `b_active` int(11) NOT NULL,
  `b_block` int(11) NOT NULL,
  PRIMARY KEY  (`b_id`)
) ENGINE=MyISAM  DEFAULT CHARSET=utf8 AUTO_INCREMENT=1 ;';
	
	$this->Execute($sql);
	
	$sql="CREATE TABLE `bannerblock` (
`bb_id` INT NOT NULL AUTO_INCREMENT PRIMARY KEY ,
`bb_nom` VARCHAR( 255 ) NOT NULL ,
`bb_w` VARCHAR( 255 ) NOT NULL ,
`bb_h` VARCHAR( 255 ) NOT NULL ,
`bb_image` VARCHAR( 255 ) NOT NULL
) ENGINE = MYISAM ;";

		$this->Execute($sql);
		
		$sql="ALTER TABLE `banner` ADD `b_titleeng` VARCHAR( 255 ) NOT NULL ,
ADD `b_texteng` LONGTEXT NOT NULL ;";
		$this->Execute($sql);
		
		$sql = "ALTER TABLE `bannerblock` ADD `bb_content` LONGTEXT NOT NULL AFTER `bb_image`;";
		$this->Execute($sql);
   }
   
   public function uninstall(){
    $sql='DROP TABLE `banner`';
	$this->Execute($sql);
	
	$sql='DROP TABLE `bannerblock`';
	$this->Execute($sql);
   }
	
};
?>
