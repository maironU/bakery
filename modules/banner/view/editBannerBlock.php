<?php
 include('modules/banner/model/banner.php');
 
 $obj = new Banner();
 $obj->connect();
 
 $msg=false;
 
 if($_POST){
  $obj->editBlock();
  $msg=true;
 }
 
 $id=$obj->getVars('id');
 $row=$obj->getBannerBlockById($id);
 foreach($row as $row){
  $nom=$row["bb_nom"];
  $w=$row["bb_w"];
  $h=$row["bb_h"];
  $image=$row["bb_image"];
  $content=$row["bb_content"];
 }

?>
<script src="modules/banner/js/prototype.js" type="text/javascript"></script>
<script src="modules/banner/js/scriptaculous.js?load=effects,builder" type="text/javascript"></script>
<script src="modules/banner/js/lightbox.js" type="text/javascript"></script>
<link rel="stylesheet" href="modules/banner/css/lightbox.css" type="text/css" media="screen" />
<div class="widget3">
 <div class="widgetlegend">Editar Bloque </div>
 <?php
  if($msg)
  {
  ?>
   <div class="ui-widget">
	<div class="ui-state-highlight ui-corner-all" style="margin-top: 20px; padding: 0 .7em;">
		<p><span class="ui-icon ui-icon-info" style="float: left; margin-right: .3em;"></span>
		<strong>Felicitaciones!</strong> El bloque ha sido creado satisfactoriamente.</p>
	</div>
</div>
  <?php
  }
 ?>
<p style="width:100">
	<a href="<?php $_SERVER['PHP_SELF'];?>?p=modules/banner/view/showBannerBlocks.php" class="btn_normal" style="float:left; margin:5px;">Volver </a>
</p>
 
<form action="#" method="post" enctype="multipart/form-data" name="form1" id="form1">
<table width="803" height="48" border="0" style="float:left">
  <tr>
    <td><label>Nombre: </label>
      <br />
      <input name="nom" type="text" id="nom" value="<?php echo $nom; ?>" />    </td>
  </tr>
  <tr>
    <td><label>Ancho: </label>
      <br />
      <input name="w" type="text" id="w" value="<?php echo $w; ?>"/>    </td>
  </tr>
  <tr>
    <td><label>Alto: </label>
      <br />
      <label>
      <input name="h" type="text" id="h" value="<?php echo $h; ?>"/>
      </label></td>
  </tr>
  <tr>
    <td><label>Imagen: </label>
      <br />
      <label>
      <input name="img" type="file" id="img" /><br />
      <img src="modules/banner/imgBannerBlock/<?php echo $image;?>" width="86" height="132"  />
      </label></td>
  </tr>
  <tr>
    <td><label>Contenido: </label>
      <br />
      <label>
      <textarea name="content" cols="40" rows="6"><?php echo $content?></textarea>
      </label></td>
  </tr>
  <tr>
    <td><input name="id" type="hidden" value="<?php echo $id; ?>" /><a href="javascript:;" class="btn_normal" onclick="document.form1.submit();">Guardar</a></td>
  </tr>
</table>
</form>


</div>
