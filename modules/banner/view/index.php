<?php
 include('modules/banner/model/banner.php');
 
 $obj = new Banner();
 $obj->connect();
 
  //Se verifica si el modulo esta instalado
 if(!$obj->Checker()){
  $ins=true;
 }
 
 $msg=false;
 
 //Instala el modulo
 if($obj->getVars('install')){
  $obj->install();
  $msgIns=true;
 }
 
 //Actions
 if($obj->getVars('ActionDel')==true){
  $obj->delBanner();
  $msg=true;
 }

?>
<script src="modules/banner/js/prototype.js" type="text/javascript"></script>
<script src="modules/banner/js/scriptaculous.js?load=effects,builder" type="text/javascript"></script>
<script src="modules/banner/js/lightbox.js" type="text/javascript"></script>
<link rel="stylesheet" href="modules/banner/css/lightbox.css" type="text/css" media="screen" />
<?php
 if(!$ins){
 ?>
 <div class="widget3">
 <div class="widgetlegend">Banner </div>
 <?php
  if($msg)
  {
  ?>
   <div class="ui-widget">
	<div class="ui-state-highlight ui-corner-all" style="margin-top: 20px; padding: 0 .7em;">
		<p><span class="ui-icon ui-icon-info" style="float: left; margin-right: .3em;"></span>
		<strong>Felicitaciones!</strong> se ha sido eliminado satisfactoriamente.</p>
	</div>
</div>
  <?php
  }
 ?>
<p style="width:100">
	<a href="<?php $_SERVER['PHP_SELF'];?>?p=modules/banner/view/newBanner.php" class="btn_normal" style="float:left; margin:5px;">Nuevo </a>
	<a href="<?php $_SERVER['PHP_SELF'];?>?p=modules/banner/view/showBannerBlocks.php" class="btn_normal" style="float:left; margin:5px;">Bloques </a>
</p>
 
<table width="803" height="48" border="0">
  <tr>
    <th width="30">ID</th>
    <th width="142">Titulo</th>
    <th width="213">Url</th>
    <th width="125">Imagen</th>
    <th width="96">Estado</th>
	<th width="96">Bloque</th>
    <th colspan="2">Acciones</th>
  </tr>
  <?php
   $row=$obj->getBanner();
   
   if(count($row)>0){
 	foreach($row as $row){
  ?>
  <tr>
    <td><?php echo $row["b_id"];?></td>
    <td><?php echo $row["b_title"];?></td>
    <td><?php echo $row["b_url"];?></td>
    <td><a href="modules/banner/imgBanner/<?php echo $row["b_image"];?>" rel="lightbox"><img src="modules/banner/imgBanner/<?php echo $row["b_image"];?>" width="100" height="100"  /></a></td>
    <td><?php 
	if($row["b_active"]==1){
		echo "Publicada";
	}else{
	 echo "Por fuera de publicacion";
	}?></td>
	<td><?php 
	$row1=$obj->getBannerBlockById($row["b_block"]);
	if(count($row1)>0){
	 foreach($row1 as $row1){
	  ?>
	 <img src="modules/banner/imgBannerBlock/<?php echo $row1["bb_image"];?>" width="86" height="132"  />
	  <?php
	 }
	}
	?></td>
    <td width="86"><a href="<?php $_SERVER['PHP_SELF'];?>?p=modules/banner/view/editBanner.php&id=<?php echo $row["b_id"]?>" class="btn_normal">Editar</a></td>
    <td width="81"><a href="<?php $_SERVER['PHP_SELF'];?>?p=modules/banner/view/index.php&id=<?php echo $row["b_id"]?>&ActionDel=true" class="btn_borrar">Eliminar</a></td>
  </tr>
  <?php
   }
  } 
  ?>
</table>

</div>

<?php } else {?>
<br />
 <div class="widget3">
 <div class="widgetlegend">Instalador Modulo de Banners </div>
 <?php
  if($msgIns)
  {
  ?>
   <div class="ui-widget">
	<div class="ui-state-highlight ui-corner-all" style="margin-top: 20px; padding: 0 .7em;">
		<p><span class="ui-icon ui-icon-info" style="float: left; margin-right: .3em;"></span>
		<strong>Felicitaciones!</strong> El modulo ha sido instalado satisfactoriamente.</p>
	</div>
</div>
  <?php
  }
 ?>
<form id="form1" name="form1" method="post" action="#">
  <table width="804" border="0" style="float:left;">
    <tr>
      <td width="525" valign="top">Bienvenido al instalador de Banner de SCS. Este modulo le ayudar&aacute; a integrar de forma facil un conjunto de banners a traves de tu sitio web. Este modulo es altamente flexible para la muestra de publicidad dentro de tu web. </td>
    </tr>
    <tr>
      <td valign="top"><div align="right"><a href="<?php $_SERVER['PHP_SELF'];?>?p=modules/banner/view/index.php&amp;install=true" class="btn_normal">Instalar</a></div></td>
    </tr>
  </table>
</form>
</div>
<?php } ?>

