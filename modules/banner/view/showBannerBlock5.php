<?php
// include('pwadmin/modules/banner/model/banner.php');
 
 $obj = new Banner();
 $obj->connect();
?>
<script type="text/javascript" src="pwadmin/modules/banner/js/jquery.js"></script>
<script type="text/javascript" src="pwadmin/modules/banner/js/jquery.cycle.all.js"></script>
<style type="text/css">
  .picsy5 {    
    padding: 0;  
    margin:  0;
} 
 
.picsy5 img {   
    top:  0; 
    left: 0 
}
.pager{
	/*background-color:#FB6733;*/
	display:block;
	border-radius:5px;	
}
.pager a{
	display:block;
	width:10px;
	height:10px;
	text-decoration:none;
	float:left;
	margin:2px;
	text-align:center;
	color:#FB6733;
	font-size:0px;
	position:relative;
	top: -20px;
	left: 0px;
	z-index:100;
	border-radius:10px;
	background-color:#FB6733;	
}
</style>
<div class="picsy5" id="picsy5">
<?php
 $row=$obj->getActiveBanner(5);
 if(count($row)>0){
  foreach($row as $row){
 ?>
  <a href="<?php echo $row["b_url"];?>" target="_blank"><img src="pwadmin/modules/banner/imgBanner/<?php echo $row["b_image"];?>" width="<?php echo $row["bb_w"];?>" height="<?php echo $row["bb_h"];?>" /></a>
 <?php
  }
 }
?> 
</div>
<script>
  var x = jQuery.noConflict();
  x("document").ready(function(){ 
			  x('#picsy5').cycle({ 
      fx:      'fade',
	  pause: 1
    });
			  
 });
</script>