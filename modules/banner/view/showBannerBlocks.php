<?php
 include('modules/banner/model/banner.php');
 
 $obj = new Banner();
 $obj->connect();
 
  //Se verifica si el modulo esta instalado
 if(!$obj->Checker()){
  $ins=true;
 }
 
 $msg=false;
 
 //Actions
 if($obj->getVars('ActionDel')==true){
  $obj->delBlock();
  $msg=true;
 }

?>
<script src="modules/banner/js/prototype.js" type="text/javascript"></script>
<script src="modules/banner/js/scriptaculous.js?load=effects,builder" type="text/javascript"></script>
<script src="modules/banner/js/lightbox.js" type="text/javascript"></script>
<link rel="stylesheet" href="modules/banner/css/lightbox.css" type="text/css" media="screen" />
 <div class="widget3">
 <div class="widgetlegend">Bloques </div>
 <?php
  if($msg)
  {
  ?>
   <div class="ui-widget">
	<div class="ui-state-highlight ui-corner-all" style="margin-top: 20px; padding: 0 .7em;">
		<p><span class="ui-icon ui-icon-info" style="float: left; margin-right: .3em;"></span>
		<strong>Felicitaciones!</strong> se ha sido eliminado satisfactoriamente.</p>
	</div>
</div>
  <?php
  }
 ?>
<p style="width:100">
	<a href="<?php $_SERVER['PHP_SELF'];?>?p=modules/banner/view/newBannerBlock.php" class="btn_normal" style="float:left; margin:5px;">Nuevo </a>
	<a href="<?php $_SERVER['PHP_SELF'];?>?p=modules/banner/view/index.php" class="btn_normal" style="float:left; margin:5px;">Volver </a>
</p>
 
<table width="803" height="48" border="0">
  <tr>
    <th width="30">ID</th>
    <th width="142">Nombre</th>
    <th width="213">Ancho</th>
    <th width="125">Alto</th>
    <th width="96">Imagen</th>
    <th colspan="2">Acciones</th>
  </tr>
  <?php
   $row=$obj->getBannerBlock();
   
   if(count($row)>0){
 	foreach($row as $row){
  ?>
  <tr>
    <td><?php echo $row["bb_id"];?></td>
    <td><?php echo $row["bb_nom"];?></td>
    <td><?php echo $row["bb_w"];?></td>
	<td><?php echo $row["bb_h"];?></td>
    <td><img src="modules/banner/imgBannerBlock/<?php echo $row["bb_image"];?>" width="86" height="132"  /></td>
    <td width="86"><a href="<?php $_SERVER['PHP_SELF'];?>?p=modules/banner/view/editBannerBlock.php&id=<?php echo $row["bb_id"]?>" class="btn_normal">Editar</a></td>
    <td width="81"><a href="<?php $_SERVER['PHP_SELF'];?>?p=modules/banner/view/showBannerBlocks.php&id=<?php echo $row["bb_id"]?>&ActionDel=true" class="btn_borrar">Eliminar</a></td>
  </tr>
  <?php
   }
  } 
  ?>
</table>

</div>