<?php
 include('modules/banner/model/banner.php');
 
 $obj = new Banner();
 $obj->connect();
 
 $msg=false;
 
 if($_POST){
  $obj->editBanner();
  $msg=true;
 }
 
 $obj->getBannerById();

?>
<script src="modules/banner/js/prototype.js" type="text/javascript"></script>
<script src="modules/banner/js/scriptaculous.js?load=effects,builder" type="text/javascript"></script>
<script src="modules/banner/js/lightbox.js" type="text/javascript"></script>
<link rel="stylesheet" href="modules/banner/css/lightbox.css" type="text/css" media="screen" />
<div class="widget3">
 <div class="widgetlegend">Editar Banner </div>
 <?php
  if($msg)
  {
  ?>
   <div class="ui-widget">
	<div class="ui-state-highlight ui-corner-all" style="margin-top: 20px; padding: 0 .7em;">
		<p><span class="ui-icon ui-icon-info" style="float: left; margin-right: .3em;"></span>
		<strong>Felicitaciones!</strong>  se ha sido guardado satisfactoriamente.</p>
	</div>
</div>
  <?php
  }
 ?>
<p style="width:100">
	<a href="<?php $_SERVER['PHP_SELF'];?>?p=modules/banner/view/index.php" class="btn_normal" style="float:left; margin:5px;">Volver </a>
</p>
 
<form action="#" method="post" enctype="multipart/form-data" name="form1" id="form1">
<table width="803" height="48" border="0" style="float:left">
  <tr>
    <td><label>Titulo: </label><br />
      <input name="title" type="text" id="title" value="<?php echo $obj->title;?>" />    </td>
  </tr>
  <!--<tr>
    <td><label>Titulo Ingles: </label>
      <br />
      <input name="titleeng" type="text" id="titleeng" value="<?php echo $obj->titleeng;?>" /></td>
  </tr>-->
  <tr>
    <td><label>Url: </label><br />
      <input name="url" type="text" id="url" value="<?php echo $obj->url;?>" />    </td>
  </tr>
  <tr>
    <td><label>Imagen: </label><br />
      <label>
      <input name="img" type="file" id="img" />
      <br />
      <a href="modules/banner/imgBanner/<?php echo $obj->image;?>" rel="lightbox"><img src="modules/banner/imgBanner/<?php echo $obj->image;?>" width="400" height="400" /></a>      </label></td>
  </tr>
  <tr>
    <td><label>Texto: </label><br />
      <textarea name="text" cols="50" rows="5" id="text"><?php echo $obj->text;?></textarea>    </td>
  </tr>
  <!--<tr>
    <td><label>Texto Ingles: </label>
      <br />
      <textarea name="texteng" cols="50" rows="5" id="texteng"><?php echo $obj->texteng;?></textarea></td>
  </tr>-->
  <tr>
    <td><label>Activar: </label><br />
      <label>
	  <?php if($obj->active==1){?>
      <input name="active" type="checkbox" id="active" value="1" checked="checked"/>
	  <?php }else{?>
	  <input name="active" type="checkbox" id="active" value="1"/>
	  <?php } ?>
      </label></td>
  </tr>
  <tr>
    <td><label>Bloque: </label><br />
      <label>
      <select name="block">
        <option value="">--Select--</option>
        <?php
		    $row=$obj->getBannerBlock();
			if(count($row)>0)
			{
			 foreach($row as $row){
			  if($obj->block==$row["bb_id"]){
			  ?><option value="<?php echo $row["bb_id"]; ?>" selected="selected"><?php echo $row["bb_nom"]; ?></option><?php
			  }
			  else
			  {
			   ?><option value="<?php echo $row["bb_id"]; ?>"><?php echo $row["bb_nom"]; ?></option><?php
			  }
			 }
			}
		   ?>
      </select>
      </label></td>
  </tr>
  <tr>
    <td><a href="javascript:;" class="btn_normal" onclick="document.form1.submit();">Guardar
      <input name="id" type="hidden" id="id" value="<?php echo $obj->getVars('id');?>" />
    </a></td>
  </tr>
</table>
</form>


</div>
