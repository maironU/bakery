<?php 
    include('../view/includes_self.php');
    
    if($_GET)
    {
        $block = $obj->getVars('block');
    }
    else
    {
        $block = 1;
    }
    
    $row = $obj->getBannerBlockById($block);
    if(count($row)>0)
    {
        foreach($row as $row)
        {
            $name = $row['bb_nom'];
            $content = $row['bb_content'];
        }
    }
    
?>
<!DOCTYPE HTML>
<!--
	Lens by HTML5 UP
	html5up.net | @ajlkn
	Free for personal and commercial use under the CCA 3.0 license (html5up.net/license)
-->
<html>
	<head>
		<title><?php echo $name?></title>
		<meta charset="utf-8" />
		<meta name="viewport" content="width=device-width, initial-scale=1, user-scalable=no" />
		<link rel="stylesheet" href="assets/css/main.css" />
		<noscript><link rel="stylesheet" href="assets/css/noscript.css" /></noscript>
		
	</head>
	<body class="is-preload-0 is-preload-1 is-preload-2">

		<!-- Main -->
			<div id="main">

				<!-- Header -->
					<header id="header">
						<h1><?php echo $name?></h1>
						<p><?php echo $content?></p>
					</header>

				<!-- Thumbnail -->
					<section id="thumbnails">
					    
					    <?php
					        $row = $obj->getActiveBanner($block);
					        if(count($row)>0)
					        {
					            foreach($row as $row)
					            {
					                ?>
					                    <article>
                							<a class="thumbnail" href="../imgBanner/<?php echo $row['b_image']?>" data-position="left center"><img src="../imgBanner/<?php echo $row['b_image']?>" width="160" height="100" alt="" /></a>
                							<?php
                							    if($row['b_title'] != '')
                							    {
                							        ?>
                							            <h2><?php echo utf8_encode($row['b_title'])?></h2>
                							        <?php
                							    }
                							
                							    if($row['b_text'] != '')
                							    {
                							        ?>
                							            <p><?php echo utf8_encode($row['b_text'])?></p>
                							        <?php
                							    }
                							?>
                						</article>
					                <?php
					            }
					        }
					    ?>
					</section>

				<!-- Footer -->
					<footer id="footer">
						<!--<ul class="copyright">
							<li>&copy; Untitled.</li><li>Design: <a href="http://html5up.net">HTML5 UP</a>.</li>
						</ul>-->
					</footer>

			</div>

		<!-- Scripts -->
		
		
			<script src="assets/js/jquery.min.js"></script>
			<script src="assets/js/browser.min.js"></script>
			<script src="assets/js/breakpoints.min.js"></script>
			<script src="assets/js/main.js"></script>
			<script type="text/javascript" src="../js/jquery.cycle.all.js"></script>
			
			<script>
  $("document").ready(function(){ 
			  $('#main1').cycle({ 
      fx:      'scrollRight',
	  pause: 1
    });
			  
 });
</script>

	</body>
</html>