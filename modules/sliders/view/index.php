<?php
 include('modules/sliders/model/Sliders.php');
 
 $obj = new Slider();
 $obj->connect();
 
  //Se verifica si el modulo esta instalado
 if(!$obj->Checker()){
  $ins=true;
 }
 
 $msg=false;
 
 if($obj->getVars('ActionDel')){
  $obj->delSlide();
  $msg=true;
 }
 
 //Instala el modulo
 if($obj->getVars('install')){
  $obj->install();
  $msgIns=true;
 }

?>
<script src="modules/sliders/js/prototype.js" type="text/javascript"></script>
<script src="modules/sliders/js/scriptaculous.js?load=effects,builder" type="text/javascript"></script>
<script src="modules/sliders/js/lightbox.js" type="text/javascript"></script>
<link rel="stylesheet" href="modules/sliders/css/lightbox.css" type="text/css" media="screen" />
<?php
 if(!$ins){
 ?>
 <div class="widget3">
 <div class="widgetlegend">Slider </div>
 <?php
  if($msg)
  {
  ?>
   <div class="ui-widget">
	<div class="ui-state-highlight ui-corner-all" style="margin-top: 20px; padding: 0 .7em;">
		<p><span class="ui-icon ui-icon-info" style="float: left; margin-right: .3em;"></span>
		<strong>Felicitaciones!</strong> El slide ha sido eliminado satisfactoriamente.</p>
	</div>
</div>
  <?php
  }
 ?>
<p style="width:100">
	<a href="<?php $_SERVER['PHP_SELF'];?>?p=modules/sliders/view/newSlide.php" class="btn_normal" style="float:left; margin:5px;">Nuevo Slide </a>
</p>
 
<table width="803" height="48" border="0">
  <tr>
    <th width="30">ID</th>
    <th width="142">Titulo</th>
    <th width="213">Url</th>
    <th width="125">Imagen</th>
    <th width="96">Estado</th>
    <th colspan="2">Acciones</th>
  </tr>
  <?php
   $row=$obj->getSlides();
   
   if(count($row)>0){
 	foreach($row as $row){
  ?>
  <tr>
    <td><?php echo $row["s_id"];?></td>
    <td><?php echo $row["s_title"];?></td>
    <td><?php echo $row["s_url"];?></td>
    <td><a href="modules/sliders/imgSlider/<?php echo $row["s_image"];?>" rel="lightbox"><img src="modules/sliders/imgSlider/<?php echo $row["s_image"];?>" width="100" height="100"  /></a></td>
    <td><?php 
	if($row["s_active"]==1){
		echo "Publicada";
	}else{
	 echo "Por fuera de publicacion";
	}?></td>
    <td width="86"><a href="<?php $_SERVER['PHP_SELF'];?>?p=modules/sliders/view/editSlide.php&id=<?php echo $row["s_id"]?>" class="btn_normal">Editar</a></td>
    <td width="81"><a href="<?php $_SERVER['PHP_SELF'];?>?p=modules/sliders/view/index.php&id=<?php echo $row["s_id"]?>&ActionDel=true" class="btn_borrar">Eliminar</a></td>
  </tr>
  <?php
   }
  } 
  ?>
</table>

</div>

<?php } else {?>
<br />
 <div class="widget3">
 <div class="widgetlegend">Instalador Modulo de Slider </div>
 <?php
  if($msgIns)
  {
  ?>
   <div class="ui-widget">
	<div class="ui-state-highlight ui-corner-all" style="margin-top: 20px; padding: 0 .7em;">
		<p><span class="ui-icon ui-icon-info" style="float: left; margin-right: .3em;"></span>
		<strong>Felicitaciones!</strong> El modulo ha sido instalado satisfactoriamente.</p>
	</div>
</div>
  <?php
  }
 ?>
<form id="form1" name="form1" method="post" action="#">
  <table width="804" border="0" style="float:left;">
    <tr>
      <td width="525" valign="top">Bienvenido al instalador de sliders de SCS. Este modulo le ayudar&aacute; a integrar de forma facil slider que cambiar&aacute; de forma interactiva dentro de su sitio web. Este modulo le ayudar&aacute; a potenciar comercialmente su sitio web de una forma muy grafica. Presione el boton de instalar, y comience a disfrutar de este fant&aacute;stico modulo. </td>
    </tr>
    <tr>
      <td valign="top"><div align="right"><a href="<?php $_SERVER['PHP_SELF'];?>?p=modules/sliders/view/index.php&amp;install=true" class="btn_normal">Instalar</a></div></td>
    </tr>
  </table>
</form>
</div>
<?php } ?>

