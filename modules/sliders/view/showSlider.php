<?php
 include("modules/sliders/model/Sliders.php");
 $slider=new Slider();
 $slider->connect();
?>
<script type="text/javascript" src="modules/sliders/js/jquery.js"></script>
<script type="text/javascript" src="modules/sliders/js/jquery.cycle.all.js"></script>
<style type="text/css">
  .picsy {    
    padding: 0;  
    margin:  0;
	width:100%;
	z-index: 0;
} 
 
.picsy img {   
    top:  0; 
    left: 0;
    width: 300px;
    height: 200px; 
}
.pager{
	background-color:#FB6733;
	display:block;
	border-radius:5px;
	position: relative;
    top: 0px;	
}
.pager a{
	display:block;
	width:10px;
	height:10px;
	text-decoration:none;
	float:left;
	margin:2px;
	text-align:center;
	color:#000;
	font-size:0px;
	position:relative;
	top: -20px;
	left: 10px;
	z-index:100;
	border-radius:0px;
	background-color:#000;	
}
.pager a.activeSlide { background: #FB6733 }
.pager a:focus { outline: none; }
.stext{
	background-color: #333333;
    color: #ffffff;
    font-family: Arial,Helvetica,sans-serif;
    font-size: 13px;
    height: 12em;
    left: -5px;
    opacity: 0.5;
    padding-left: 27px;
    position: relative;
    top: -23em;
    width: 36em;

} 
</style>
<div class="picsy" id="picsy">
 <?php
  $row=$slider->getActiveSlides();
  if(count($row)>0){
	  foreach($row as $row){
 ?>
 <a href="<?php echo $row["s_url"];?>" target="_blank"><img src="modules/sliders/imgSlider/<?php echo $row["s_image"];?>" /></a>
 <?php
   }
  }
 ?>
</div>
<script>
		  var x = jQuery.noConflict();
		  x("document").ready(function(){
			 x('#picsy').after('<div class="pager"></div>');  
			  x('#picsy').cycle({ 
      pause: 1,
      pager: ".pager",
	  speed: 2000
    });
			  
			  });
		</script>