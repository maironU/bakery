<?php
 include('modules/sliders/model/Sliders.php');
 
 $obj = new Slider();
 $obj->connect();
 
 $msg=false;
 
 if($_POST){
  $obj->newSlide();
  $msg=true;
 }

?>
<script src="modules/sliders/js/prototype.js" type="text/javascript"></script>
<script src="modules/sliders/js/scriptaculous.js?load=effects,builder" type="text/javascript"></script>
<script src="modules/sliders/js/lightbox.js" type="text/javascript"></script>
<link rel="stylesheet" href="modules/sliders/css/lightbox.css" type="text/css" media="screen" />
<div class="widget3">
 <div class="widgetlegend">Slider </div>
 <?php
  if($msg)
  {
  ?>
   <div class="ui-widget">
	<div class="ui-state-highlight ui-corner-all" style="margin-top: 20px; padding: 0 .7em;">
		<p><span class="ui-icon ui-icon-info" style="float: left; margin-right: .3em;"></span>
		<strong>Felicitaciones!</strong> El slide ha sido creado satisfactoriamente.</p>
	</div>
</div>
  <?php
  }
 ?>
<p style="width:100">
	<a href="<?php $_SERVER['PHP_SELF'];?>?p=modules/sliders/view/index.php" class="btn_normal" style="float:left; margin:5px;">Volver </a>
</p>
 
<form action="#" method="post" enctype="multipart/form-data" name="form1" id="form1">
<table width="803" height="48" border="0" style="float:left">
  <tr>
    <td><label>Titulo: </label><br />
      <input name="title" type="text" id="title" />    </td>
  </tr>
  <tr>
    <td><label>Url: </label><br />
      <input name="url" type="text" id="url" value="http://" />    </td>
  </tr>
  <tr>
    <td><label>Imagen: </label><br />
      <label>
      <input name="img" type="file" id="img" />
      </label></td>
  </tr>
  <tr>
    <td><label>Texto: </label><br />
      <textarea name="text" cols="50" rows="5" id="text"></textarea>    </td>
  </tr>
  <tr>
    <td><label>Activar: </label><br />
      <label>
      <input name="active" type="checkbox" id="active" value="1" />
      </label></td>
  </tr>
  <tr>
    <td><a href="javascript:;" class="btn_normal" onclick="document.form1.submit();">Guardar</a></td>
  </tr>
</table>
</form>


</div>
