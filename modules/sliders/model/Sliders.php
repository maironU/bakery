<?php
class Slider extends GeCore{

	public $id;
	public $title;
	public $url;
	public $image;
	public $text;
	public $active;
	
	
	public function newSlide(){
		$title=$this->postVars('title');
		$url=$this->postVars('url');
		$text=$this->postVars('text');
		$active=$this->postVars('active');
		
		$sql="insert into `slider` (`s_title`, `s_url`, `s_text`, `s_active`) values ('$title','$url','$text','$active')";
		$this->Execute($sql);
		
		$id=$this->getLastID();
		
		$this->uploadSlide($id);
	}
	
	public function uploadSlide($id){
		
		$ok=$this->upLoadFileProccess('img', 'modules/sliders/imgSlider');
		
		if($ok){
		 $sql="update slider set s_image='".$this->fname."' where s_id='$id'";
		 $this->Execute($sql);
		}
		
	}
	
	public function editSlide(){
		$id=$this->postVars('id');
		$title=$this->postVars('title');
		$url=$this->postVars('url');
		$text=$this->postVars('text');
		$active=$this->postVars('active');
		
		$sql="update `slider` set `s_title`='$title', `s_url`='$url', `s_text`='$text', `s_active`='$active' where s_id='$id'";
		$this->Execute($sql);
		
		$this->uploadSlide($id);
	}
	
	public function delSlide(){
		$id=$this->getVars('id');
		
		$sql="delete from `slider` where s_id='$id'";
		$this->Execute($sql);
	}
	
	
	public function getSlides(){
	 $sql="select * from slider";
	 return $this->ExecuteS($sql);
	}
	
	public function getSlide(){
	  $id=$this->getVars('id');
	  $sql="select * from slider where s_id='$id'";
	  $row=$this->ExecuteS($sql);
	  
	  foreach($row as $row){
	   $this->title=$row["s_title"];
	   $this->url=$row["s_url"];
	   $this->image=$row["s_image"];
	   $this->text=$row["s_text"];
	   $this->active=$row["s_active"];
	  }
	}
	
	public function getActiveSlides(){
		$sql="select * from slider where s_active=1";
	 	return $this->ExecuteS($sql);
	}
	
	//Installers
   public function Checker(){
    return $this->chkTables('slider');
   }
   
   public function install(){
    $sql='CREATE TABLE `slider` (
  `s_id` int(11) NOT NULL auto_increment,
  `s_title` varchar(255) NOT NULL,
  `s_url` varchar(255) NOT NULL,
  `s_image` varchar(255) NOT NULL,
  `s_text` longtext NOT NULL,
  `s_active` int(11) NOT NULL,
  PRIMARY KEY  (`s_id`)
) ENGINE=MyISAM  DEFAULT CHARSET=utf8 AUTO_INCREMENT=1 ;';
	
	$this->Execute($sql);
   }
   
   public function uninstall(){
    $sql='DROP TABLE `slider`';
	$this->Execute($sql);
   }
	
};
?>
