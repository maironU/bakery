<?php
    include('../view/include_self.php');
    
    $method = $_GET['method'];
    $value = $_GET['value'];
    $employee_code = $_POST['employee_code'];

    if(isset($method)){
        switch($method){
            case "getPaymentMethodsById":
                $method_ind = $obj->getPaymentMethodsById($value);
                echo json_encode($method_ind);
                break;
            case "getPosTotal":
                $total_pay = $obj->getPosTotal();
                echo number_format($total_pay,2);
                break;
            case "getCoins":
                $coins = $obj->getCoins();
                echo json_encode($coins);
                break;
            case "changeAccount":
                $user = $usr->changeAccount($employee_code);
                echo json_encode($user);
                break;
            case "getUserById":
                $user = $usr->getUserById($value);
                echo json_encode($usr);
                break;
        }
    }
?>