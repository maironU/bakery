<?php
class posmodule extends GeCore{
	private $store_online = 20;
	//Posmodule
	public function newPos(){
		$id_customer=$this->postVars('id_customer');
		$u_id=$this->postVars('u_id');
		$cash=$this->postVars('cash');
		$devol=$this->postVars('devol');
		$txs=$this->postVars('txs');
		$id_payment=$this->postVars('id_payment');
		$facturado = $this->postVars('facturado');
		$track=$this->trackGenerator();
		
		$today=date('Y-m-d H:i:s');
		$today2=date('Y-m-d');
		
		$id_customer=explode(':',$id_customer);
		
		$sql="insert into posmodule (pos_track, id_customer, u_id, pos_dateadd, pos_cash, pos_deliver, id_store, pos_txs, pospm_id, pos_date, pos_active, pos_facture) values ('$track','".$id_customer[1]."','$u_id','$today','$cash','$devol','".$_SESSION['id_store']."','$txs','$id_payment','$today2','1','$facturado')";
		$this->Execute($sql);
		
		$id_pos=$this->getLastID();
		$this->newPosDetail($id_pos);
		
		return $id_pos;
	}
	
	public function editPosActive($id, $act)
	{
	    $sql = "UPDATE posmodule SET pos_active='$act' WHERE pos_id='$id'";
	    $this->Execute($sql);
	}
	
	public function trackGenerator(){
	 
	 $str="0123456789";
	 for($i=0;$i<=6;$i++){
	  $pos=rand(0,strlen($str));
	  $track.=$str[$pos];
	 }
	 
	 $sql="select * from accounts where a_track='$track'";
	 $row=$this->ExecuteS($sql);
	 if(count($row)>0){
	  $this->trackGenerator();
	 }else{
	  return $track;
	 }
	 
	}
	
	public function getPosById($id){
	 	$sql="select * from posmodule where pos_id='$id'";
		return $this->ExecuteS($sql);
	}
	
	public function getPosSales($start='',$end='',$facturado='', $id_store=''){
		$start.=' 00:00:00';
		$end.=' 23:59:59';
		$sql = null;
		if($id_store == $this->store_online && ($facturado == 1 || $facturado == "")){
			$sql = $this->getPedidosWoocomerceShowSalesInf($start,$end);
		}else{
			if($facturado==''){
				$sql="select * from posmodule where pos_dateadd between '$start' and '$end' and id_store='$id_store' and pos_active='1'";
			}else{
				$sql="select * from posmodule where pos_dateadd between '$start' and '$end' and id_store='$id_store' and pos_facture='$facturado' and pos_active='1'";
			}

			$sql = $this->ExecuteS($sql);
		}
		
		return $sql;
	}
	
	public function getMyPosSales($start='',$end='',$id_store=''){
		$start.=' 00:00:00';
		$end.=' 23:59:59';
		$sql = null;
		
		$sql="select * from posmodule where pos_dateadd between '$start' and '$end' and id_store='$id_store' and u_id='".$_SESSION['user_id']."' and pos_active='1'";
		return $this->ExecuteS($sql);
	}
	
	public function getGlobalPosSales($start='',$end='',$facturado='',$id_store=''){
		$start.=' 00:00:00';
		$end.=' 23:59:59';
		if($id_store == $this->store_online && ($facturado == 1 || $facturado == "")){
			$sql = $this->getPedidosWoocomerceShowSalesInf2($start,$end);
		}else{
			if($facturado != ''){
				$sql="SELECT pos_date, SUM(pos_total) AS total, SUM(pos_tax) AS tax, SUM(pos_subtotal) AS subtotal, pos_facture FROM posmodule WHERE pos_dateadd BETWEEN '$start' AND '$end' AND pos_facture='$facturado' AND id_store='$id_store' AND pos_active='1' GROUP BY pos_date ORDER BY pos_date ASC";
			}else{
				$sql="SELECT pos_date, SUM(pos_total) AS total, SUM(pos_tax) AS tax, SUM(pos_subtotal) AS subtotal, pos_facture FROM posmodule WHERE pos_dateadd BETWEEN '$start' AND '$end' AND id_store='$id_store' AND pos_active='1' GROUP BY pos_date ORDER BY pos_date ASC";
			}
			$sql = $this->ExecuteS($sql);
		}

		return $sql;
	}
	
	public function getGlobalPosSalesByProduct($start='',$end='', $facturado='',$id_store=''){
		$start.=' 00:00:00';
		$end.=' 23:59:59';
		$sql = [];

		if($id_store == $this->store_online && ($facturado == 1 || $facturado == "")){
			$sql = $this->getPedidosWoocomerceShowSalesInf3($start, $end);
		}else{
			if($facturado != ''){
				$sql="SELECT * FROM posmodule AS p 
				INNER JOIN posmodule_detail AS pd ON p.pos_id = pd.pos_id
				WHERE p.pos_dateadd BETWEEN '$start' AND '$end' AND p.id_store='$id_store' AND p.pos_facture='$facturado' AND p.pos_active='1' GROUP BY pd.id_product ORDER BY p.pos_date ASC";
			}else{
				$sql="SELECT * FROM posmodule AS p 
				INNER JOIN posmodule_detail AS pd ON p.pos_id = pd.pos_id
				WHERE p.pos_dateadd BETWEEN '$start' AND '$end' AND p.id_store='$id_store' AND p.pos_active='1' GROUP BY pd.id_product ORDER BY p.pos_date ASC";
			}

			$sql = $this->ExecuteS($sql);
		}
		
		return $sql;
	}

	public function getPedidosWoocomerceShowSalesInf($start, $end){
		$data = $this->getPedidosWoocomerce($start, $end);
		$new_data = $this->getPedidosCompletedShowSalesInf($data);
		
		return $new_data;
	}

	public function getPedidosWoocomerceShowSalesInf3($start, $end){
		$data = $this->getPedidosWoocomerce($start, $end);
		$new_data = $this->getPedidosCompletedShowSalesInf3($data);
		
		return $new_data;
	}

	public function getPedidosWoocomerceShowSalesInf2($start, $end){
		$data = $this->getPedidosWoocomerce($start, $end);
		$new_data = $this->getPedidosCompletedShowSalesInf2($data);
		
		return $new_data;
	}

	public function getTotalForMethodWoocomerce($id_method_pay, $start, $end){
		$method = $this->getPaymentMethodsById($id_method_pay);
		$data = $this->getPedidosWoocomerce($start, $end);
		$new_data = $this->getTotalMethod($data, $method[0]['pospm_name']);
		
		return $new_data;
	}

	public function getTotalMethod($result, $method){
		$data = [];
		$new_result = $this->groupBy($result, 'payment_method_title');
		foreach($new_result as $pedido){
			if($method == $pedido->payment_method_title){
				if($pedido->status == "completed"){
					$objPedido = array(
						"pos_subtotal" => $pedido->total,
						"pos_total" => ($pedido->total + $pedido->total_tax),
						"pos_tax" => $pedido->total_tax,
						"pos_cash" => 0,
						"pos_devol" => 0,
						"type" => "woocomerce"
					);
					array_push($data, $objPedido);
				}
			}
		}
		return $data;
	}

	public function getPedidosWoocomerce($start, $end){
		$login = 'ck_25d94b358c28d2396837d67e909d7e7e78b6c060';
		$password = 'cs_a0d515d4e9b20e22b2ac5c9d2aad468bd5280095';
		$url = 'https://columbuswholesale.com/wp-json/wc/v3/orders';
		$ch = curl_init();
		curl_setopt($ch, CURLOPT_URL,$url);
		curl_setopt($ch, CURLOPT_RETURNTRANSFER,true);
		curl_setopt($ch, CURLOPT_HTTPAUTH, CURLAUTH_BASIC);
		curl_setopt($ch, CURLOPT_USERPWD, "$login:$password");
		$result = curl_exec($ch);
		curl_close($ch); 
		$result = json_decode($result);
		if($start != '' && $end != ''){
			$result = $this->getDataForRange($result, $start, $end);
		}

		return $result;
	}

	
	public function getPedidosWoocomerceById($id){
		$login = 'ck_25d94b358c28d2396837d67e909d7e7e78b6c060';
		$password = 'cs_a0d515d4e9b20e22b2ac5c9d2aad468bd5280095';
		$url = 'https://columbuswholesale.com/wp-json/wc/v3/orders/'.$id;
		$ch = curl_init();
		curl_setopt($ch, CURLOPT_URL,$url);
		curl_setopt($ch, CURLOPT_RETURNTRANSFER,true);
		curl_setopt($ch, CURLOPT_HTTPAUTH, CURLAUTH_BASIC);
		curl_setopt($ch, CURLOPT_USERPWD, "$login:$password");
		$result = curl_exec($ch);
		curl_close($ch); 
		$result = json_decode($result);

		return $result;
	}

	public function getCouponWoocomerce($cupon){
		$login = 'ck_25d94b358c28d2396837d67e909d7e7e78b6c060';
		$password = 'cs_a0d515d4e9b20e22b2ac5c9d2aad468bd5280095';
		$url = 'https://columbuswholesale.com/wp-json/wc/v3/coupons?code='.$cupon;
		$ch = curl_init();
		curl_setopt($ch, CURLOPT_URL,$url);
		curl_setopt($ch, CURLOPT_RETURNTRANSFER,true);
		curl_setopt($ch, CURLOPT_HTTPAUTH, CURLAUTH_BASIC);
		curl_setopt($ch, CURLOPT_USERPWD, "$login:$password");
		$result = curl_exec($ch);
		curl_close($ch); 

		return $result;
	}

	public function getDataForRange($result, $start, $end){
		$date_min = strtotime($start);
		$date_max = strtotime($end);
		$new_data = [];
		foreach($result as $elem){
			$date = date('Y-m-d', strtotime($elem->date_created));
			$date_elem = strtotime($date);
			if($date_elem >= $date_min && $date_elem <= $date_max){
				array_push($new_data, $elem);
			}
		}

		return $new_data;
	}

	public function cuponIsAvailable($objCoupon){
		if($objCoupon[0]->date_expires){
			$date_expires_formatter = date("Y-m-d H:m:s", strtotime($objCoupon[0]->date_expires));
			$date_expires = strtotime($date_expires_formatter);
			$today_formatter = date("Y-m-d H:m:s");
			$today = strtotime($today_formatter);
	
			if($date_expires > $today) return 1;
			return 0;
		}else{
			return 1;
		}
		
	}

	public function getPedidosCompletedShowSalesInf($result){
		$data = [];
		foreach($result as $pedido){
				$method = $this->getPaymentMethodsByName($pedido->payment_method_title);
				if($pedido->status == "completed"){
					$objPedido = array(
						"pos_id" => $pedido->id,
						"pos_dateadd" => date('Y-m-d H:m:s', strtotime($pedido->date_created)),
						"pos_date" => date('Y-m-d H:m:s', strtotime($pedido->date_created)),
						"pos_track" => $pedido->id,
						"pos_total" => $pedido->total,
						"total" => ($pedido->total + $pedido->total_tax),
						"subtotal" => $pedido->total,
						"tax" => $pedido->total_tax,
						"id_store" => 20,
						"u_id" => 50,
						"pospm_id" => $method[0]['pospm_id'],
						"pospm_name" => $method[0]['pospm_name'],
						"pos_facture" => 1,
						"type" => "woocomerce"
					);
					array_push($data, $objPedido);
			}
		}
		return $data;
	}

	public function getPedidosCompletedShowSalesInf3($result){
		$data = [];
		foreach($result as $pedido){
			if($pedido->status == "completed"){
				foreach($pedido->line_items as $item){
					$objPedido = array(
						"pos_date" => date('Y-m-d', strtotime($pedido->date_created)),
						"posd_productname" => $item->name,
						"posd_cant" => $item->quantity,
						"subtotal_tax" => $item->subtotal_tax,
						"subtotal" => ($item->subtotal/$item->quantity),
						"total" => $item->total,
						"type" => "woocomerce"
					);
					array_push($data, $objPedido);
				}
			}
		}
		return $data;
	}

	function groupBy($array,$groupkey){
		$new_array = [];
		if (count($array)>0){
			foreach($array as $elem){
				if($elem->status == "completed"){
					if(!$this->isInArray($new_array, $elem, $groupkey)){
						array_push($new_array, $elem);
					}else{
						foreach($new_array as $ped){
							if($groupkey == "date_created"){
								$ped_created = date('Y-m-d', strtotime($ped->date_created));
								$elem_created = date('Y-m-d', strtotime($elem->date_created));
	
								if($ped_created == $elem_created){
									$ped->total+=$elem->total;
									$ped->total_tax+=$elem->total_tax;
								}
							}else{
								if($ped->$groupkey== $elem->$groupkey){
									$ped->total+=$elem->total;
									$ped->total_tax+=$elem->total_tax;
								}
							}
						}
					}
				}	
			}
		}
		return $new_array;
	}

	function isInArray($array, $elem, $groupkey){
		$sw = false;
		foreach($array as $ped){
			if($groupkey == "date_created"){
				$ped_created = date('Y-m-d', strtotime($ped->date_created));
				$elem_created = date('Y-m-d', strtotime($elem->date_created));
	
				if($ped_created == $elem_created){
					$sw = true;
				}
			}else{
				if($ped->$groupkey == $elem->$groupkey){
					$sw = true;
				}
			}
			
		}

		return $sw;
	}

	public function getPedidosCompletedShowSalesInf2($result){
		$data = [];
		$new_result = $this->groupBy($result, 'date_created');		
		foreach($new_result as $pedido){
			$method = $this->getPaymentMethodsByName($pedido->payment_method_title);
			if($pedido->status == "completed"){
				$objPedido = array(
					"pos_id" => $pedido->id,
					"pos_dateadd" => date('Y-m-d', strtotime($pedido->date_created)),
					"pos_date" => date('Y-m-d', strtotime($pedido->date_created)),
					"pos_track" => $pedido->order_key,
					"pos_total" => $pedido->total,
					"total" => ($pedido->total + $pedido->total_tax),
					"subtotal" => $pedido->total,
					"tax" => $pedido->total_tax,
					"id_store" => 19,
					"ud_id" => 50,
					"pospm_id" => $method[0]['pospm_id'],
					"pospm_name" => $method[0]['pospm_name'],
					"pos_facture" => 1,
					"type" => "woocomerce"
				);
				array_push($data, $objPedido);
			}
		}
		return $data;
	}
	
	public function getGlobalPosSalesProductDet($start='',$end='', $facturado='',$id_store='',$id_product='',$flag)
	{
	  
	    $start.=' 00:00:00';
		$end.=' 23:59:59';
		
		$qty = 0;
		$price = 0;
		$tax = 0;
		$total = 0;
		
		if($facturado != ''){
			$sql="SELECT * FROM posmodule AS p 
			INNER JOIN posmodule_detail AS pd ON p.pos_id = pd.pos_id
			WHERE p.pos_dateadd BETWEEN '$start' AND '$end' AND p.id_store='$id_store' AND p.pos_facture='$facturado' AND p.pos_active='1' AND pd.id_product='$id_product'";
		}else{
			$sql="SELECT * FROM posmodule AS p 
			INNER JOIN posmodule_detail AS pd ON p.pos_id = pd.pos_id
			WHERE p.pos_dateadd BETWEEN '$start' AND '$end' AND p.id_store='$id_store' AND p.pos_active='1' AND pd.id_product='$id_product'";
		}
		
		$row = $this->ExecuteS($sql);
		if(count($row)>0)
		{
		    foreach($row as $row)
		    {
		        $qty += $row['posd_qty'];
		        $price = $row['posd_price'];
		        $tax = $row['posd_tax'];
		    }
		}
		
		$total = (($price + $tax) * $qty);
		
		switch($flag)
		{
		    case '0'; $res = $qty;
		    break;
		    
		    case '1'; $res = $price;
		    break;
		    
		    case '2'; $res = $tax;
		    break;
		    
		    case '3'; $res = $total;
		    break;
		}
		
		return $res;
	}
	
	
	public function printReceipt($id_pos, $type)
	{
		if($type == "woocomerce"){
			$row = $this->getPedidosWoocomerceById($id_pos);
			if($row){
				$track=$row->order_key;
				$id_customer=0;
				$pos_date=date('Y-m-d H:m:s', strtotime($row->date_created));
				$total=($row->total + $row->total_tax);
				$tax=$row->total_tax;
				$subto=$row->total;
				$id_store=$this->store_online;
				$cash=0;
				$devol=0;

				foreach($row->line_items as $elem)
				{
					$detail.='
					<tr>
						<td>'.$elem->quantity.'</td>
						<td>'.$elem->name.'</td>
						<td>$'.number_format(($elem->subtotal/$elem->quantity),2).'</td>
						<td>$'.number_format($elem->subtotal_tax,2).'</td>
						<td>$'.number_format($elem->total,2).'</td>
					</tr>
					';
				}
			}
		}else{
			$row=$this->getPosById($id_pos);
			if(count($row)>0)
			{
				foreach($row as $row)
				{
					$track=$row["pos_track"];
					$id_customer=$row["id_customer"];
					$pos_date=$row["pos_dateadd"];
					$total=$row["pos_total"];
					$tax=$row["pos_tax"];
					$subto=$row["pos_subtotal"];
					$id_store=$row["id_store"];
					$cash=$row["pos_cash"];
					$devol=$row["pos_deliver"];
				}
			}
			
			$row=$this->getPosDetailByPosId($id_pos);
			if(count($row)>0)
			{
				foreach($row as $row)
				{
					$detail.='
					 <tr>
					  <td>'.$row["posd_qty"].'</td>
					  <td>'.$row["posd_productname"].'</td>
					  <td>$'.number_format($row["posd_price"],2).'</td>
					  <td>$'.number_format($row["posd_tax"]*$row["posd_qty"],2).'</td>
					  <td>$'.number_format($row["posd_price"]*$row["posd_qty"],2).'</td>
					 </tr>
					';
				}
			}
		}
	    
	    
	    $customer = new customerModelGe();
	    $customer->connect();
	    
	    $row1=$customer->getCustomerById2($id_customer);
	    if(count($row1)>0)
	    {
	        foreach($row1 as $row1)
    	    {
    	        $customer_name=$row1["c_nom"];
    	        $customer_nit=$row1["c_nit"];
    	        $customer_dir=$row1["c_dir"];
    	        $customer_tel=$row1["c_tel"];
    	    }
	    }
	    
	    
	    $f=fopen('../formats/receipt.html','r');
	    $html=fread($f,filesize('../formats/receipt.html')); 
	    
	    //$header=$this->getPosSetupById(1);
	    //$footer=$this->getPosSetupById(2);
	    
	    $row=$this->getStoreById($id_store);
	    if(count($row)>0)
	    {
	        foreach($row as $row)
	        {
    	        $header = $row['poss_header'];
    	        $footer = $row['poss_footer'];
	        }
	    }
	    
	    
	    
	    $html=str_replace('[track]',$track,$html);
	    $html=str_replace('[header]',$header,$html);
	    $html=str_replace('[footer]',$footer,$html);
	    $html=str_replace('[detalle]',$detail,$html);
	    $html=str_replace('[date]',$pos_date,$html);
	    $html=str_replace('[tax]',number_format($tax,2),$html);
	    $html=str_replace('[total]',number_format($total,2),$html);
	    $html=str_replace('[subto]',number_format($subto,2),$html);
	    $html=str_replace('[cash]',number_format($cash,2),$html);
	    $html=str_replace('[devol]',number_format($devol,2),$html);
	    
	    
	    if (!isset($output))  $output   = "png";
		if (!isset($type))    $type     = "C128B";
		if (!isset($widthb))   $widthb    = "300";
		if (!isset($heightb))  $heightb   = "100";
		if (!isset($xres))    $xres     = "2";
		if (!isset($font))    $font     = "2";
		
		$drawtext="off";
		$stretchtext="off";
		$border  == "off";
		
		$style |= ($output  == "png" ) ? BCS_IMAGE_PNG  : 0;
		$style |= ($output  == "jpeg") ? BCS_IMAGE_JPEG : 0;
		$style |= ($border  == "on"  ) ? BCS_BORDER           : 0;
		$style |= ($drawtext== "on"  ) ? BCS_DRAW_TEXT  : 0;
		$style |= ($stretchtext== "on" ) ? BCS_STRETCH_TEXT  : 0;
		$style |= ($negative== "on"  ) ? BCS_REVERSE_COLOR  : 0;
	    
	    
	    
	    $html=str_replace('[barcode]',"<img style='height: 50px; width: 300px' src='../barcode/image.php?code=".$track."&style=".$style."&type=".$type."&width=".$widthb."&height=".$heightb."&xres=".$xres."&font=".$font."' />",$html);
	    
	    if($id_customer=='')
	    {
	        $html=str_replace('[cnom]','',$html);
	        $html=str_replace('[ident]','',$html);
	        $html=str_replace('[dir]','',$html);
	        $html=str_replace('[tel]','',$html);	
	    }
	    else
	    {
	        $html=str_replace('[cnom]',$customer_name,$html);
	        $html=str_replace('[ident]',$customer_nit,$html);
	        $html=str_replace('[dir]',$customer_dir,$html);
	        $html=str_replace('[tel]',$customer_tel,$html);
	    }
	    
	    return $html;
	
	}
	
	//POS Detail
	public function newPosDetail($id_pos){
		
		$row=$this->getPosDetailTemp();
		if(count($row)>0){
			foreach($row as $row){
				$sql="insert into posmodule_detail (pos_id,id_product,posd_productname,posd_price,posd_qty,id_store,posd_tax) values ('$id_pos','".$row["id_product"]."','".$row["posdt_productname"]."','".$row["posdt_price"]."','".$row["posdt_qty"]."','".$row["id_store"]."','".$row["posdt_tax"]."')";
				$this->Execute($sql);
				
				$taxes+=$row["posdt_tax"]*$row["posdt_qty"];
				$total+=$row["posdt_price"]*$row["posdt_qty"];
			}
		}
		
		$gtotal = $taxes+$total;
		$sql="update posmodule set pos_total='$gtotal', pos_tax='$taxes', pos_subtotal='$total' where pos_id='$id_pos'";
		$this->Execute($sql);
		
		$this->flushPosDetail();
	}
	
	public function getPosDetailById($id){
	 $sql="select * from posmodule_detail where posd_id='$id'";
	 return $this->ExecuteS($sql);
	}
	
	public function getPosDetailByPosId($id){
	 $sql="select * from posmodule_detail where pos_id='$id'";
	 return $this->ExecuteS($sql);
	} 
	
	
	//POS Detail Temp
	public function newposDetailTemp($id_product=NULL,$product_name=NULL,$price=NULL,$qty=NULL,$tax=NULL, $type=NULL, $id_coupon_woocomerce = NULL, $cant_percent = NULL){
	 $sql="insert into posmodule_detail_temp(`u_id`,`id_product`,`posdt_productname`,`posdt_price`,`posdt_qty`,`id_store`,`posdt_tax`,`type`,`id_coupon_woocomerce`, `cant_percent`) values ('".$_SESSION["user_id"]."','$id_product','$product_name','$price','$qty','".$_SESSION['id_store']."','$tax', '$type', '$id_coupon_woocomerce', '$cant_percent')";
	 $this->Execute($sql);
	}
	
	public function delposDetailTemp($id){
	 $sql="delete from posmodule_detail_temp where posdt_id='$id'";
	 $this->Execute($sql);
	}
	
	public function flushPosDetail(){
	 $sql="delete from posmodule_detail_temp where u_id='".$_SESSION["user_id"]."' and id_store='".$_SESSION['id_store']."'";
	 $this->Execute($sql);
	}
	
	public function getPosDetailTemp(){
	 $sql="select * from posmodule_detail_temp where u_id='".$_SESSION["user_id"]."' and id_store='".$_SESSION['id_store']."' order by posdt_id desc";
	 return $this->ExecuteS($sql);
	}
	
	public function getPosDetailTempById($id){
	 $sql="select * from posmodule_detail_temp where posdt_id='$id'";
	 return $this->ExecuteS($sql);
	}
	
	public function getPosTotal(){
	 $row=$this->getPosDetailTemp();
	 if(count($row)>0){
	  foreach($row as $row){
	  	$taxes+=$row["posdt_tax"]*$row["posdt_qty"];
	  	$total+=$row["posdt_price"]*$row["posdt_qty"];
	  }
	 }
	 $gtotal = $total + $taxes;
	 return $gtotal;
	}
	
	public function editQtyItem($id, $qty)
	{
	     $sql="update posmodule_detail_temp set `posdt_qty`='$qty' where posdt_id='$id'";
	     $this->Execute($sql);
	}
	
	public function editPriceItem($id, $price)
	{
	     $sql="update posmodule_detail_temp set `posdt_price`='$price' where posdt_id='$id'";
	     $this->Execute($sql);
	}
	
	public function getPosDetailTemp2($id_user, $id_store){
	 $sql="select * from posmodule_detail_temp where u_id='".$id_user."' and id_store='".$id_store."' order by posdt_id desc";
	 return $this->ExecuteS($sql);
	}
	
	//Auth methods
	public function editAuth($id_user,$auth){
	 $sql="update user set letAuthpos='$auth' where u_id='$id_user'";
	 $this->Execute($sql);
	}
	
	public function letAuth($id_user){
	 $sql="select * from user where u_id='$id_user'";
	 $row=$this->ExecuteS($sql);
	 foreach($row as $row){
	 	$letAuth=$row["letAuthpos"];
	 }
	 
	 if($letAuth==1){
	  return true;
	 }else{
	  return false;
	 }
	}
	
	public function getUserId($user,$pass){
	 $pass=md5($pass);	
	 $sql="select * from user where u_user='$user' and u_pass='$pass'";
	 $row=$this->ExecuteS($sql);
	 if(count($row)>0){
	 	foreach($row as $row){
	  		return $row["u_id"];
	 	}
	 }
	 
	}
	
	public function hasCoupon($id_coupon){
		$carrito=$this->getPosDetailTemp();
		$sw = 0;
		if(count($carrito) > 0){
			foreach($carrito as $product){
				if($product['id_coupon_woocomerce'] == $id_coupon){
					$sw = 1;
				}
			}
		}

		return $sw;
	}

	public function updateCouponsIfHas(){
		$carrito=$this->getPosDetailTemp();
		$taxes = 0;
		$total = 0;
		$gtotal = 0;

		if(count($carrito) > 0){
			foreach($carrito as $product){
				if($product['type'] == null){
					$taxes+=$product["posdt_tax"]*$product["posdt_qty"];
	  				$total+=$product["posdt_price"]*$product["posdt_qty"];
				}
			}
		 $gtotal = $total + $taxes;
		}

		if(count($carrito) > 0){
			foreach($carrito as $product){
				if($product['type'] == "percent"){
                    $discount = $product['cant_percent']/100;
					$new_value = $gtotal * $discount * (-1);
					$posdt_id = $product['posdt_id'];

					$sql="update `posmodule_detail_temp` set `posdt_price`='$new_value' where posdt_id='$posdt_id'";
					$this->Execute($sql);
				}
			}
		}
	}

	//Log Auth Methods
	public function newAuthLog($id_user=NULL,$desc=NULL,$value=NULL,$obs=NULL){
		$today=date('Y-m-d H:i:s');
		$sql="insert into `posmodule_logauth` (`posl_date`, `u_id`, `posl_desc`, `posl_value`, `posl_obs`) values ('$today','$id_user','$desc','$value','$obs')";
		$this->Execute($sql);
	}
	
	public function getAuthLog($start,$end){
	 $start.=' 00:00:00';
	 $end.=' 23:59:59';
	 
	 $sql="select * from `posmodule_logauth` where posl_date between '$start' and '$end'";
	 return $this->ExecuteS($sql);
	}
	
	//Setup
	public function editPosSetup()
	{
	    $id=$this->postVars('id');
	    $value=$this->postVars('value');
	    $type=$this->postVars('type');
	    
	    if($type != 'file')
	    {
	        $sql="update posmodule_config set posc_value='$value' where posc_id='$id'";
	        $this->Execute($sql);
	    }
	    else
	    {
	        $this->uploadParamFile($id);
	    }
	}
	
	public function uploadParamFile($id, $path = 'modules/posmodule/paramFiles')
	{
		
		$ok=$this->upLoadFileProccess('arch', $path);
		
		if($ok)
		{
		     $sql="update posmodule_config set posc_value='".$this->fname."' where posc_id='$id'";
		     $this->Execute($sql);
		}
		
	}
	
	public function getPosSetupById($id){
	 $sql="select * from posmodule_config where posc_id='$id'";
	 $row=$this->ExecuteS($sql);
	 if(count($row)>0){
	  foreach($row as $row){
	  	$value=$row["posc_value"];
	  }
	 }
	 
	 return $value;
	}
	
	public function getPosSetup(){
	 $sql="select * from posmodule_config";
	 return $this->ExecuteS($sql);
	}
	
	//Stores methods
	public function newStore()
	{
	    $name = $this->postVars('name');
	    $addr = $this->postVars('addr');
	    $phone = $this->postVars('phone');
	    $header = $this->postVars('header');
	    $footer = $this->postVars('footer');
	    $id_wh = $this->postVars('id_wh');
	    
	    $sql = "INSERT INTO `posmodule_stores`(`poss_name`, `poss_addr`, `poss_phone`, `poss_image`, `poss_header`, `poss_footer`, `id_warehouse`) VALUES ('$name','$addr','$phone','default.png','$header','$footer','$id_wh')";
	    
	    $this->Execute($sql);
	    
	}
	
	public function editStore()
	{
	    $id = $this->postVars('id');
	    $name = $this->postVars('name');
	    $addr = $this->postVars('addr');
	    $phone = $this->postVars('phone');
	    $header = $this->postVars('header');
	    $footer = $this->postVars('footer');
	    $id_wh = $this->postVars('id_wh');
	    
	    $sql = "UPDATE `posmodule_stores` SET `poss_name`='$name', `poss_addr`='$addr', `poss_phone`='$phone', `poss_header`='$header', `poss_footer`='$footer', `id_warehouse`='$id_wh' WHERE poss_id='$id'";
	    
	    $this->Execute($sql);
	    
	}
	
	public function delStore()
	{
	    $id = $this->getVars('id');
	    
	    $sql = "DELETE FROM `posmodule_stores` WHERE poss_id='$id'";
	    $this->Execute($sql);
	}
	
	public function getStoreById($id)
	{
	    $sql = "SELECT * FROM `posmodule_stores` WHERE poss_id='$id'";
	    return $this->ExecuteS($sql);
	}
	
	public function getStore()
	{
	    $sql = "SELECT * FROM `posmodule_stores`";
	    return $this->ExecuteS($sql);
	}
	
	//User Store Methods
	public function newUserStores()
	{
	    $usr = $_POST['usrs'];
	    $id = $this->postVars('id');
	    
	    for($i=0; $i<count($usr); $i++)
	    {
	        $sql = "INSERT INTO `posmodule_userstores`(`u_id`, `poss_id`) VALUES ('".$usr[$i]."','$id')";
	        $this->Execute($sql);
	    }
	}
	
	public function delUserStores()
	{
	    $id = $this->getVars('id_us');
	    $sql = "DELETE FROM `posmodule_userstores` WHERE `posus_id`='$id'";
	    $this->Execute($sql);
	}
	
	public function isInStore($id, $id_user)
	{
	    $sql = "SELECT * FROM `posmodule_userstores` WHERE `u_id`='$id_user' AND poss_id='$id'";
	    $row = $this->ExecuteS($sql);
	    if(count($row)>0)
	    {
	        return true;
	    }
	    else
	    {
	        return false;
	    }
	}
	
	public function getUserInStore($id, $id_user)
	{
	    $sql = "SELECT * FROM `posmodule_userstores` WHERE `u_id`='$id_user' AND poss_id='$id'";
	    return $this->ExecuteS($sql);
	}
	
	public function getUserStore($id_user)
	{
	    $sql = "SELECT * FROM `posmodule_userstores` AS posu
	    INNER JOIN `posmodule_stores` AS poss ON poss.poss_id=posu.poss_id
	    WHERE `u_id`='$id_user'";
	    return $this->ExecuteS($sql);
	}
	
	//Miselanius methods
	public function getInventoryProductId($id_wh, $id_product)
	{
	    $sql = "SELECT * FROM inv_prod WHERE id_wh='$id_wh' AND id_product='$id_product'";
	    $row = $this->ExecuteS($sql);
	    if(count($row)>0)
	    {
	        foreach($row as $row)
	        {
	            return $row['ip_id'];
	        }
	    }
	}
	
	public function setBarcode($track)
	{
		if (!isset($output))  $output   = "png";
		if (!isset($type))    $type     = "C128B";
		if (!isset($widthb))   $widthb    = "300";
		if (!isset($heightb))  $heightb   = "100";
		if (!isset($xres))    $xres     = "2";
		if (!isset($font))    $font     = "2";
		
		$drawtext="off";
		$stretchtext="off";
		
		$style |= ($output  == "png" ) ? BCS_IMAGE_PNG  : 0;
		$style |= ($output  == "jpeg") ? BCS_IMAGE_JPEG : 0;
		$style |= ($border  == "on"  ) ? BCS_BORDER           : 0;
		$style |= ($drawtext== "on"  ) ? BCS_DRAW_TEXT  : 0;
		$style |= ($stretchtext== "on" ) ? BCS_STRETCH_TEXT  : 0;
		$style |= ($negative== "on"  ) ? BCS_REVERSE_COLOR  : 0;
	 
		 //$bc = new I25Object(0, 0, $style, $track); 
		 return "<img style='height: 50px; width: 300px' src='../barcode/image.php?code=".$track."&style=".$style."&type=".$type."&width=".$widthb."&height=".$heightb."&xres=".$xres."&font=".$font."' />";
	}
	
	public function fillPosDate()
	{
	    $sql = "SELECT * FROM posmodule";
	    $row = $this->ExecuteS($sql);
	    if(count($row)>0)
	    {
	        foreach($row as $row)
	        {
	            $d1 = $row['pos_dateadd'];
	            $d = explode(" ",$d1);
	            $sql1 = "UPDATE posmodule SET pos_date='".$d[0]."' WHERE pos_id='".$row['pos_id']."'";
	            $this->Execute($sql1);
	            echo $sql1."<br>";
	        }
	    }
	}
	
	
	//Coins Method
	public function newCoins()
	{
	    $name = $this->postVars('name');
	    $value = $this->postVars('value');
	    
	    $sql = "INSERT INTO `posmodule_coins`(`posc_name`, `posc_value`) VALUES ('$name','$value')";
	    $this->Execute($sql);
	    
	    $id = $this->getLastID();
	    
	    $this->uploadImageCoins($id);
	    
	    return $id;
	}
	
	public function uploadImageCoins($id, $path = 'modules/posmodule/imagesCoins')
	{
		
		$ok=$this->upLoadFileProccess('img', $path);
		
		if($ok){
		 $sql="update `posmodule_coins` set posc_image='".$this->fname."' where posc_id='$id'";
		 $this->Execute($sql);
		}
		
	}
	
	public function editCoins()
	{
	    $id = $this->postVars('id');
	    $name = $this->postVars('name');
	    $value = $this->postVars('value');
	    
	    $sql = "UPDATE `posmodule_coins` SET `posc_name`='$name', `posc_value`='$value' WHERE posc_id='$id'";
	    $this->Execute($sql);
	    
	    $this->uploadImageCoins($id);
	}
	
	public function delCoins()
	{
	    $id = $this->getVars('id');
	    
	    $sql = "DELETE FROM `posmodule_coins` WHERE posc_id='$id'";
	    $this->Execute($sql);
	}
	
	public function getCoins()
	{
	    $sql = "SELECT * FROM `posmodule_coins`";
	    return $this->ExecuteS($sql);
	}
	
	public function getCoinsById($id)
	{
	    $sql = "SELECT * FROM `posmodule_coins` WHERE posc_id='$id'";
	    return $this->ExecuteS($sql);
	}
	
	//Payment Methods
	public function newPaymentMethod()
	{
	    $name = $this->postVars('name');
	    $sql = "INSERT INTO `posmodule_paymethods`(`pospm_name`) VALUES ('$name')";
	    $this->Execute($sql);
	    
	    $id = $this->getLastID();
	    
	    $this->uploadImagePm($id);
	}
	
	public function uploadImagePm($id, $path = 'modules/posmodule/imagesPm')
	{
		
		$ok=$this->upLoadFileProccess('img', $path);
		
		if($ok){
		 $sql="update `posmodule_paymethods` set pospm_image='".$this->fname."' where pospm_id='$id'";
		 $this->Execute($sql);
		}
		
	}
	
	public function editPaymentMethod()
	{
	    $id = $this->postVars('id');
	    $name = $this->postVars('name');
	    $sql = "UPDATE `posmodule_paymethods` SET `pospm_name`='$name' WHERE `pospm_id`='$id'";
	    $this->Execute($sql);
	    
	    $this->uploadImagePm($id);
	}
	
	public function delPaymentMethods()
	{
	    $id = $this->getVars('id');
	    $sql = "DELETE FROM `posmodule_paymethods` WHERE `pospm_id`='$id'";
	    $this->Execute($sql);
	}
	
	public function getPaymentMethodsWithoutStoreVirtual()
	{
	    $sql = "SELECT * FROM `posmodule_paymethods` limit 4";
	    return $this->ExecuteS($sql);
	}

	public function getPaymentMethods()
	{
	    $sql = "SELECT * FROM `posmodule_paymethods`";
	    return $this->ExecuteS($sql);
	}
	
	
	public function getPaymentMethodsById($id)
	{
	    $sql = "SELECT * FROM `posmodule_paymethods` WHERE `pospm_id`='$id'";
	    return $this->ExecuteS($sql);
	}
	
	public function getPaymentMethodsByName($name)
	{
	    $sql = "SELECT * FROM `posmodule_paymethods` WHERE `pospm_name`='$name'";
	    return $this->ExecuteS($sql);
	}
	public function gePaymmentMethodTotals($start, $end, $facturado='',$id_store, $id_pm)
	{
	    $start .= " 00:00:00";
	    $end .= " 23:59:59";
		$sql = null;
		if($id_store == $this->store_online && ($facturado == 1 || $facturado == "")){
			$sql = $this->getTotalForMethodWoocomerce($id_pm, $start,$end);
		}else{
			if($facturado == ''){
				$sql = "SELECT * FROM posmodule WHERE pos_dateadd BETWEEN '$start' AND '$end' AND id_store='$id_store' AND pospm_id='$id_pm'";
			}else{
				$sql = "SELECT * FROM posmodule WHERE pos_dateadd BETWEEN '$start' AND '$end' AND id_store='$id_store' AND pos_facture='$facturado' AND pospm_id='$id_pm'";
			}

			$sql = $this->ExecuteS($sql);
		}
	    if(count($sql)>0)
	    {
	        foreach($sql as $row)
	        {
	            $total += $row['pos_total'];
	            $tax += $row['pos_tax'];
	            $subto += $row['pos_subto'];
	            $cash += $row['pos_cash'];
	            $devol += $row['pos_devol'];
	        }
	    }
	    
	    return array($total, $tax, $subto, $cash, $devol);
	}
	
	//Open BOX Methods
	public function newOpenBox()
	{
	    $id_user = $this->postVars('id_user');
	    $desc = $this->postVars('desc');
	    $today = date('Y-m-d H:i:s');
	    
	    $sql = "INSERT INTO `posmodule_openbox`(`u_id`, `poso_date`, `poso_desc`) VALUES ('$id_user','$today','$desc')";
	    $this->Execute($sql);
	}
	
	public function getOpenBoxLog($start, $end)
	{
	    $start .= " 00:00:00";
	    $end .= " 23:59:59";
	    
	    $sql = "SELECT * FROM `posmodule_openbox` WHERE poso_date BETWEEN '$start' AND '$end'";
	    return $this->ExecuteS($sql);
	}

	
	//Installers
   public function Checker(){
    return $this->chkTables('posmodule');
   }
   
   public function install(){
	
 $sql='CREATE TABLE `posmodule_detail` (
  `posd_id` int(11) NOT NULL AUTO_INCREMENT,
  `pos_id` int(11) NOT NULL,
  `id_product` int(11) NOT NULL,
  `posd_productname` varchar(255) NOT NULL,
  `posd_price` varchar(255) NOT NULL,
  `posd_qty` varchar(255) NOT NULL,
  PRIMARY KEY (`posd_id`)
) ENGINE=MyISAM ;';
	
	$this->Execute($sql);
	
	$sql="CREATE TABLE `posmodule` (
  `pos_id` int(11) NOT NULL AUTO_INCREMENT,
  `pos_total` varchar(255) NOT NULL,
  `pos_tax` varchar(255) NOT NULL,
  `pos_subtotal` varchar(255) NOT NULL,
  `pos_cash` varchar(255) NOT NULL,
  `pos_deliver` varchar(255) NOT NULL,
  `pos_track` varchar(255) NOT NULL,
  `id_customer` int(11) NOT NULL,
  `pos_dateadd` datetime NOT NULL,
  `u_id` int(11) NOT NULL,
  PRIMARY KEY (`pos_id`)
) ENGINE=MyISAM ;";

$this->Execute($sql);

$sql='CREATE TABLE `posmodule_detail_temp` (
  `posdt_id` int(11) NOT NULL AUTO_INCREMENT,
  `u_id` int(11) NOT NULL,
  `id_product` int(11) NOT NULL,
  `posdt_productname` varchar(255) NOT NULL,
  `posdt_price` varchar(255) NOT NULL,
  `posdt_qty` varchar(255) NOT NULL,
  PRIMARY KEY (`posdt_id`)
) ENGINE=MyISAM ;';
	
	$this->Execute($sql);
	
	$sql="ALTER TABLE  `user` ADD  `letAuthpos` INT NOT NULL DEFAULT  '0';";
	$this->Execute($sql);
	
	$sql='CREATE TABLE `posmodule_logauth` (
  `posl_id` int(11) NOT NULL AUTO_INCREMENT,
  `posl_date` datetime NOT NULL,
  `u_id` int(11) NOT NULL,
  `posl_desc` varchar(255) NOT NULL,
  `posl_value` varchar(255) NOT NULL,
  `posl_obs` longtext NOT NULL,
  PRIMARY KEY (`posl_id`)
) ENGINE=MyISAM;';

	$this->Execute($sql);
	
		$sql='CREATE TABLE `posmodule_config` (
  `posc_id` int(11) NOT NULL AUTO_INCREMENT,
  `posc_name` varchar(255) NOT NULL,
  `posc_value` longtext NOT NULL,
  `posc_type` varchar(255) NOT NULL,
  PRIMARY KEY (`posc_id`)
) ENGINE=MyISAM;';

	$this->Execute($sql);
	
	$sql="insert into posmodule_config(posc_name,posc_value) values ('Encabezado','','text')";
	$this->Execute($sql);
	
	$sql="insert into posmodule_config(posc_name,posc_value) values ('Pie de Pagina','','text')";
	$this->Execute($sql);
	
	$sql = "CREATE TABLE `posmodule_coins` ( 
	    `posc_id` INT NOT NULL AUTO_INCREMENT ,
	    `posc_name` VARCHAR(255) NOT NULL ,
	    `posc_value` VARCHAR(255) NOT NULL ,
	    `posc_image` VARCHAR(255) NOT NULL ,
	    PRIMARY KEY (`posc_id`)) ENGINE = MyISAM;";
	$this->Execute($sql);
	
	$sql = "CREATE TABLE `posmodule_paymethods` (
  `pospm_id` int(11) NOT NULL PRIMARY KEY AUTO_INCREMENT,
  `pospm_name` varchar(255) NOT NULL,
  `pospm_image` varchar(255) NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=latin1;";
$this->Execute($sql);

$sql = "ALTER TABLE `posmodule` ADD `pos_txs` VARCHAR(255) NOT NULL AFTER `id_store`;";
$this->Execute($sql);

$sql = "ALTER TABLE `posmodule` ADD `pospm_id` INT NOT NULL AFTER `pos_txs`;";
$this->Execute($sql);

$sql = "ALTER TABLE `posmodule` ADD `pos_date` DATE NOT NULL AFTER `pospm_id`;";
$this->Execute($sql);

$sql = "CREATE TABLE posmodule_openbox (
  poso_id int(11) NOT NULL PRIMARY KEY AUTO_INCREMENT,
  u_id int(11) NOT NULL,
  poso_date datetime NOT NULL,
  poso_desc longtext NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=latin1;";
$this->Execute($sql);

$sql = "ALTER TABLE `posmodule` ADD `pos_active` INT NOT NULL AFTER `pos_date`;";
$this->Execute($sql);
	
	////////
	//Se instala los submenus
	$sql="select * from menu where m_folder='posmodule'";
	$row=$this->ExecuteS($sql);
	foreach($row as $row){
	 $m_id=$row["m_id"];
	}
	
	$sql="insert into `submenu` (`sm_nom`, `sm_link`, `m_id`) values ('Inf. Ventas','home.php?p=modules/posmodule/view/showSalesInf.php','$m_id')";
	$this->Execute($sql);
	$sql="insert into `submenu` (`sm_nom`, `sm_link`, `m_id`) values ('Autorizaciones','home.php?p=modules/posmodule/view/showUsers.php','$m_id')";
	$this->Execute($sql);
	$sql="insert into `submenu` (`sm_nom`, `sm_link`, `m_id`) values ('Log Autorizaciones','home.php?p=modules/posmodule/view/showInfAuthLog.php','$m_id')";
	$this->Execute($sql);
	$sql="insert into `submenu` (`sm_nom`, `sm_link`, `m_id`) values ('Setup','home.php?p=modules/posmodule/view/showSetup.php','$m_id')";
	$this->Execute($sql);
	
	
   }
   
   public function uninstall(){
    $sql='DROP TABLE `posmodule_detail`';
	$this->Execute($sql);
	
	$sql='DROP TABLE `posmodule`';
	$this->Execute($sql);
	
	$sql='DROP TABLE `posmodule_detail_temp`';
	$this->Execute($sql);
	
	$sql='DROP TABLE `posmodule_logauth`';
	$this->Execute($sql);
	
	$sql='DROP TABLE `posmodule_config`, posmodule_openbox';
	$this->Execute($sql);
	
	$sql='ALTER TABLE  `user` DROP  `letAuthpos` INT NOT NULL DEFAULT  "0";';
	$this->Execute($sql);
	
	$sql="select * from menu where m_folder='posmodule'";
	$row=$this->ExecuteS($sql);
	foreach($row as $row){
	 $m_id=$row["m_id"]; 
	}
	
	$sql="select * from submenu as sm inner join pro_submenu as psm on sm.sm_id=psm.sm_id where sm.m_id='$m_id'";
	$row=$this->ExecuteS($sql);
	foreach($row as $row){
	 $sql1="delete from pro_submenu where sm_id='".$row["sm_id"]."'";
	 $this->Execute($sql);
	}
	
	$sql1="delete from submenu where m_id='$m_id'";
	$this->Execute($sql);
	

   }
};
?>
