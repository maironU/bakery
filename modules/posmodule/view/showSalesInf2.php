<?php
 include('modules/posmodule/view/include.php');
 
 
 $msg=false;
 
 //Acciones
 ?>
<div class="widget3">
 <div class="widgetlegend">Informe de ventas Global</div>
 <?php
  if($msg)
  {
  ?>
   <div class="ui-widget">
	<div class="ui-state-highlight ui-corner-all" style="margin-top: 20px; padding: 0 .7em;">
		<p><span class="ui-icon ui-icon-info" style="float: left; margin-right: .3em;"></span>
		<strong>Felicitaciones!</strong> eliminado satisfactoriamente.</p>
	</div>
</div>
  <?php
  }
 ?>
<p style="width:100">
	<a href="<?php $_SERVER['PHP_SELF'];?>?p=modules/posmodule/view/index.php" class="btn_normal" style="float:left; margin:5px;">Volver </a>
    <a href="javascript:;" class="btn_normal" onClick="PrintElem('salesGReport')" style="float:left; margin:5px;">Imprimir </a>
</p>
 <br /><br /><br />

<form action="" method="post">
<table width="100%" height="48" border="0">
 <tr>
  <td width="25%">
    <input type="text" name="start" id="start" placeholder="Inicio" class="datepicker" />
  </td>
  <td width="25%">
    <input type="text" name="end" id="end" placeholder="Fin" class="datepicker2" />
  </td>
  <td width="25%">
      <select name="facturado" id="facturado">
          <option value="">--Tipo Facturacion--</option>
          <option value="1">Facturado</option>
          <option value="0">No Facturado</option>
      </select>
  </td>
  <td width="25%">
      <input type="submit" name="button" id="button" value="Buscar" class="btn_submit" />
    </td>
 </tr>
</table>
</form>
<br /><br /><br />
<div id="salesGReport">
<?php
    if($_POST)
    {
        $totalg = 0;
        $taxg = 0;
        $subtog = 0;
        $row = $obj->getStore();
        if(count($row)>0)
        {
            foreach($row as $row)
            {
                $totals = 0;
                $taxs = 0;
                $subtos = 0;
                ?>
                <table>
                    <tr>
                        <td><h1><?php echo $row['poss_name']?></h1></td>
                    </tr>
                    <tr>
                        <td>
                            <table>
                              <tr>
                                <th width="30">Fecha</th>
                                <th width="142">Total</th>
                                <th width="142">Tax</th>
                                <th width="142">Subtotal</th>
                              </tr>
                              <?php
                                
                                
                                $row1=$obj->getGlobalPosSales($obj->postVars('start'),$obj->postVars('end'),$obj->postVars('facturado'),$row['poss_id']);                                
                                echo "<script>console.log($row1)</script>";
                                
                                if(count($row1)>0)
                                {
                                    foreach($row1 as $row1)
                                    {
                                        ?>
                                        <tr>
                                            <td><?php echo $row1["pos_date"]?></td>
                                            <td>$<?php echo number_format($row1["total"],2)?></td>
                                            <td>$<?php echo number_format($row1["tax"],2)?></td>
                                            <td>$<?php echo number_format($row1["subtotal"],2)?></td>
                                        </tr>
                                        <?php
                                        $totals += $row1["total"];
                                        $taxs += $row1["tax"];
                                        $subtos += $row1["subtotal"];
                                        
                                    }
                                    
                                    $totalg += $totals;
                                    $taxg += $taxs;
                                    $subtog += $subtos;
                                    
                                    ?>
                                    <tr>
                                        <td><strong>Total</strong></td>
                                        <td><strong>$<?php echo number_format($totals,2)?></strong></td>
                                        <td><strong>$<?php echo number_format($taxs,2)?></strong></td>
                                        <td><strong>$<?php echo number_format($subtos,2)?></strong></td>
                                    </tr>
                                    <tr>
                                        <td colspan="4">
                                            <table>
                                                <tr>
                                                    <th>Efectivo</th>
                                                    <?php
                                                        $row2 = $obj->getPaymentMethods();
                                                        if(count($row2)>0)
                                                        {
                                                            foreach($row2 as $row2)
                                                            {
                                                                ?>
                                                                <th><?php echo $row2['pospm_name']?></th>
                                                                <?php
                                                            }
                                                        }
                                                    ?>
                                                </tr>
                                                <tr>
                                                    <td>$
                                                        <?php
                                                            $t = $obj->gePaymmentMethodTotals($obj->postVars('start'), $obj->postVars('end'),$obj->postVars('facturado'), $row['poss_id'],0);
                                //echo "<script>console.log($t)</script>";
                                                            
                                                            echo number_format($t[0],2);
                                                        ?>
                                                    </td>
                                                    <?php
                                                        $row2 = $obj->getPaymentMethods();
                                                        if(count($row2)>0)
                                                        {
                                                            foreach($row2 as $row2)
                                                            {
                                                                ?>
                                                                <td>$
                                                                    <?php
                                                                        $t = $obj->gePaymmentMethodTotals($obj->postVars('start'), $obj->postVars('end'),$obj->postVars('facturado'), $row['poss_id'],$row2['pospm_id']);
                                                                        
                                                                        echo number_format($t[0],2);
                                                                    ?>
                                                                </td>
                                                                <?php
                                                            }
                                                        }
                                                    ?>
                                                </tr>
                                            </table>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td colspan="4"><hr></td>
                                    </tr>
                                    <?php
                                }
                                else
                                {
                                    ?>
                                    <tr>
                                        <td colspan="4">No hay ventas</td>
                                    </tr>
                                    <?php
                                }
                                ?>
                                </table>
                                <?php
                
                            }
                            ?>
                        </td>
                    </tr>
                </table>
                <?php
            }
        }
?>
<table>
    <tr>
        <th width="142">Total</th>
        <th width="142">Tax</th>
        <th width="142">Subtotal</th>
    </tr>
    <tr align="center">
        <td><h1>$<?php echo number_format($totalg,2)?></h1></td>
        <td><h1>$<?php echo number_format($taxg,2)?></h1></td>
        <td><h1>$<?php echo number_format($subtog,2)?></h1></td>
    </tr>
</table>
</div>
</div>
<script>
function PrintElem(elem)
{
    var mywindow = window.open('', 'PRINT', 'height=400,width=800');

    mywindow.document.write('<html><head><meta http-equiv="Content-Type" content="text/html; charset=utf-8"><title>Reporte Global de Ventas</title>');
    mywindow.document.write('</head><body style=\'font-family: Arial; font-size: 12px\'>');
    mywindow.document.write('<h1>' + document.title  + '</h1>');
    mywindow.document.write(document.getElementById(elem).innerHTML);
    mywindow.document.write('</body></html>');

    mywindow.document.close(); // necessary for IE >= 10
    mywindow.focus(); // necessary for IE >= 10*/

    mywindow.print();
    mywindow.close();

    return true;
}
</script>


