<?php
 include('modules/posmodule/view/include.php');
 
 
 $id = $obj->getVars('id');
 $_SESSION['id_store'] = $id;
 
?>
<script type="text/javascript" src="modules/posmodule/js/jsPosFunctions.js"></script>
<!-- Add mousewheel plugin (this is optional) -->
<script type="text/javascript" src="modules/posmodule/js/jquery.mousewheel-3.0.6.pack.js"></script>
<!-- Add fancyBox main JS and CSS files -->
<script type="text/javascript" src="modules/posmodule/js/jquery.fancybox.js?v=2.1.4"></script>
<link rel="stylesheet" type="text/css" href="modules/posmodule/css/jquery.fancybox.css?v=2.1.4" media="screen" />
<link rel="stylesheet" type="text/css" href="modules/posmodule/css/stylePos.css" />

<style>
    	ul{
			list-style: none outside none;
		    padding-left: 0;
            margin: 0;
		}
        .demo .item{
            margin-bottom: 60px;
        }
		.content-slider li{
		    background-color: #ed3020;
		    text-align: center;
		    color: #FFF;
		}
		
		.content-slider li > a{
		    text-align: center;
		    color: #FFF;
		    text-decoration: none;
		}
		.content-slider h4 {
		    margin: 0;
		    padding: 70px 0;
		}
		.demo{
			width: 800px;
		}
    </style>
    <script>
    	 $(document).ready(function() {
			$("#content-slider").lightSlider({
                loop:true,
                keyPress:true
            });
            $('#image-gallery').lightSlider({
                gallery:true,
                item:1,
                thumbItem:9,
                slideMargin: 0,
                speed:500,
                auto:true,
                loop:true,
                onSliderLoad: function() {
                    $('#image-gallery').removeClass('cS-hidden');
                }  
            });
		});
		$( function() {
            $( "#tabs" ).tabs();
          } );
    </script>

<div class="widget3">
 <div class="widgetlegend">Punto de venta </div>
 <?php
  //Acciones
 $action=$obj->postVars('action');
 
 //DelItem
 if($obj->getVars('ActionDel')=='true')
 {
     $user=$obj->postVars('user');
     $pass=$obj->postVars('pass');
     $desc=$obj->postVars('desc');
     $posdt_id=$obj->getVars('posdt_id');
     
     $id_user=$obj->getUserId($user,$pass);
  	 //if(!empty($id_user))
  	 {
  	     $letAuth=$obj->letAuth($id_user);
  	     //if($letAuth)
  	     {
  	         $row=$obj->getPosDetailTempById($posdt_id);
  	         foreach($row as $row)
  	         {
  	             $value=$row["posdt_price"] * $row["posdt_qty"];
  	             $id_product=$row["id_product"];
  	             $qty=$row["posdt_qty"];
  	         }
  	         
  	         $obj->delposDetailTemp($posdt_id);
  	         $obj->newAuthLog($id_user,"Eliminacion de Item punto de venta",$value,$desc);
  	         
  	         $row = $obj->getStoreById($_SESSION['id_store']);
        	if(count($row)>0)
        	{
        	    foreach($row as $row)
        	    {
        	        $id_wh = $row['id_warehouse'];
        	        $store_name = $row['poss_name'];
        	    }
        	}
        	
        	$inv_id = $obj->getInventoryProductId($id_wh, $id_product);
        	if($inv_id != 0 || $inv_id != '')
        	{
        	    $caps->registerMov($inv_id,$qty,$_SESSION['user_id'],'',1,$product_id,$desc,$id_wh,$whd,$referral, 'no');
        	}
		
    		?>
              <div class="ui-widget">
               <div class="ui-state-highlight ui-corner-all" style="margin-top: 20px; padding: 0 .7em;">
                <p><span class="ui-icon ui-icon-info" style="float: left; margin-right: .3em;"></span>
                   <strong>Felicitaciones!</strong> Item Retirado.</p>
                </div>
              </div>
            <?php
	  }
	  /*else
	  {
		?>
          <div class="ui-widget">
             <div class="ui-state-error ui-corner-all" style="margin-top: 20px; padding: 0 .7em;">
               <p><span class="ui-icon ui-icon-alert" style="float: left; margin-right: .3em;"></span>
               <strong>Error!</strong> Este usuario no tiene permiso para autorizar</p>
            </div>
          </div>
        <?php  
	  }*/
	 }
	 /*else
	 {
	    ?>
          <div class="ui-widget">
             <div class="ui-state-error ui-corner-all" style="margin-top: 20px; padding: 0 .7em;">
               <p><span class="ui-icon ui-icon-alert" style="float: left; margin-right: .3em;"></span>
               <strong>Error!</strong> Usuario no existe</p>
            </div>
          </div>
        <?php 
	 }*/
  }
 
 
 
 
 if($action=='closepos'){
  $cash=$obj->postVars('cash');
  $devol=$obj->postVars('devol');
  
  if($cash=='' || $devol=='')
  {
      ?>
      <div class="ui-widget">
                 <div class="ui-state-error ui-corner-all" style="margin-top: 20px; padding: 0 .7em;">
                   <p><span class="ui-icon ui-icon-alert" style="float: left; margin-right: .3em;"></span>
                   <strong>Error!</strong> Para cerrar debe ingresar el dinero entregado</p>
                </div>
              </div>
      <?php
  }
  else
  {
      $id=$obj->newPos();
   
       ?>
       <script>
        showInfo(<?php echo $id; ?>,'modules/posmodule/view/showReceipt.php')
       </script>
      <?php
    
  }
  
 }
  
 ?>
<p style="width:100">
    <!--<a href="<?php $_SERVER['PHP_SELF'];?>?p=modules/tracker/view/showMsg.php" class="btn_normal" style="float:left; margin:5px;">Mensajes </a> -->
</p>
 
<table width="800" border="0">
    <tr>
        <td valign="top">
            <table width="100%">
         	    <tr>
         	        <td>
         	            
         	            <div id="tabs">
         	                <ul>
                                <li><a href="#tabs-1">Categorias</a></li>
                                <li><a href="#tabs-2">Codigo de Barras</a></li>
                                <li><a href="#tabs-3">Buscar por nombre</a></li>
                            </ul>
         	            <div id="tabs-1">
         	                <p>
             	            <div class="demo">
                                <div class="item">
                                    <ul id="content-slider" class="content-slider">
                                        <?php
                                            $row = $products->getCategories();
                                            if(count($row)>0)
                                            {
                                                foreach($row as $row)
                                                {
                                                    ?>
                                                     <li>
                                                        <a href="javascript:;" onClick="findProductByCategory(<?php echo $row['cat_id']?>)"><h4><?php echo $row['cat_name']?></h4></a>
                                                    </li>
                                                    <?php
                                                }
                                            }
                                        ?>
                                    </ul>
                                </div>
            
                            </div>
                            </p>
         	            </div>
         	            <div id="tabs-2">
         	                <p>
         	                    <form action="#" method="post">
                 	                <table width="550">
                 	                    <tr>
                 	                        <td><strong>Cantidad:</strong><input type="text" name="qty" id="qty" value="1" /></td>
                 	                        <td><strong>Producto:</strong><input type="text" name="barcode" id="barcode" autofocus="autofocus" /></td>
                 	                        <td><input type="button" value="Agregar" onclick="findPosList()" class="btn_submit" /></td>
                 	                    </tr>
                 	                </table>
                 	            </form>
         	                </p>
         	            </div>
         	            <div id="tabs-3">
         	                <p>
         	                    <form action="#" method="post">
                 	                <table width="550">
                 	                    <tr>
                 	                        <td><strong>Buscar Producto:</strong><input type="text" name="prodname" id="prodname" /></td>
                 	                        <td><input type="button" value="Buscar" onclick="findProductByName()" class="btn_submit" /></td>
                 	                    </tr>
                 	                </table>
                 	            </form>
         	                </p>
         	            </div>
         	          </div>     
         	        </td>
         	    </tr>
	            <tr>
	                <td>
	                    <div id="listprod"></div>
	                </td>
	            </tr>
	       </table>
	   </td>
	   <td valign="top">
	       <form action="#" name="formpos1" method="post">
	           <table width="100%">
	               <tr>
	                   <td>
	                       <span style="float:left">Cliente:</span> <input type="text" id="finder" value="<?php echo $_SESSION["id_customer"]?>" style="float:left"/>
	                       <a href="javascript:;" onclick="findCustomer()" class="btn_2" style="float:left">Buscar</a>
	                       <a href="javascript:;" onclick="showInfo(0,'modules/posmodule/view/newSimpleCustomer.php')" class="btn_1" style="float:left">Nuevo</a>
	                       <br /><br /><br />
	                       <div id="gauch" style="display:none"><img src="modules/posmodule/images/loading.gif" /></div>
	                       <div id="showCl">&nbsp;</div>
	                   </td>
	               </tr>
	               <tr>
	                   <td>
	                       <div id="showposL">
                    	    <?php
                    		 $row=$obj->getPosDetailTemp();
                    		 $html.='
                    			 <table border="0" width="100%">
                    			  <tr>
                    				<th>Producto</th>
                    				<th>Cantidad</th>
                    				<th>Precio</th>
                    				<th>Tax</th>
                    				<th>Total</th>
                    				<th>&nbsp;</th>
                    			  </tr>
                    		  ';
                    		  
                    		if(count($row)>0){
                    		 foreach($row as $row){
                    		  $html.="
                    		   <tr>
                    			<td>".($row["posdt_productname"])."</td>
                    			<td>".$row["posdt_qty"]."</td>
                    			<td>$".number_format($row["posdt_price"],2)."</td>
                    			<td>$".number_format($row["posdt_tax"]*$row["posdt_qty"],2)."</td>
                    			<td>$".number_format($row["posdt_price"]*$row["posdt_qty"],2)."</td>
                    			<td>
                    			    <a href='".$_SERVER['PHP_SELF']."?p=modules/posmodule/view/pos.php&posdt_id=".$row["posdt_id"]."&ActionDel=true&id=".$id."' class='btn_2'>Eliminar</a>
                    			</td>
                    		   </tr>
                    		  ";
                    		 }
                    		}
                    	
                    		$html.="</table>";
                    		echo $html;
                    		?>
                    	   </div>
                    	   <div id="gauch2" style="display:none"><img src="modules/posmodule/images/loading.gif" /></div>
	                   </td>
	               </tr>
	               <tr>
	                   <td>
	                       <strong>TOTAL :</strong>
	                       <?php $total=$obj->getPosTotal();?>
	                       <div id="totalpos"><h3>$<?php echo number_format($total,2)?></h3></div>
	                       <input name="total" id="total" type="hidden" value="<?php echo $total; ?>" />
	                   </td>
	               </tr>
	               <tr>
	                   <td>
	                       <strong>EFECTIVO :</strong>
	                       $<input name="cash" id="cash" type="text" value="0" onchange="calcDelvol()" />
	                   </td>
	               </tr>
	               <tr>
	                   <td>
	                       <strong>CAMBIO :</strong>
	                       $<input name="devol1" id="devol1" type="text" value="0" disabled="disabled" />
	                       <input name="devol" id="devol" type="hidden" value="0" />
	                   </td>
	               </tr>
	               <tr>
	                   <td>
	                       <input type="hidden" name="action" id="action" value="closepos" />
	                       <input name="u_id" type="hidden" id="u_id" value="<?php echo $_SESSION["user_id"];?>" />
	                       <a href="javascript:;" class="btn_normal" onclick="document.formpos1.submit();">Pagar</a>
	                   </td>
	               </tr>
	           </table>
	       </form>
	       </td>
		</tr>
</table>

</div>


