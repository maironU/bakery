<?php
 include('modules/posmodule/view/include.php');
 
 
 $msg=false;
 
 //Acciones
 ?>
<div class="widget3">
 <div class="widgetlegend">Informe de ventas Global de Productos</div>
 <?php
  if($msg)
  {
  ?>
   <div class="ui-widget">
	<div class="ui-state-highlight ui-corner-all" style="margin-top: 20px; padding: 0 .7em;">
		<p><span class="ui-icon ui-icon-info" style="float: left; margin-right: .3em;"></span>
		<strong>Felicitaciones!</strong> eliminado satisfactoriamente.</p>
	</div>
</div>
  <?php
  }
 ?>
<p style="width:100">
	<a href="<?php $_SERVER['PHP_SELF'];?>?p=modules/posmodule/view/index.php" class="btn_normal" style="float:left; margin:5px;">Volver </a>
    <a href="javascript:;" class="btn_normal" onClick="PrintElem('salesGReport')" style="float:left; margin:5px;">Imprimir </a>
</p>
 <br /><br /><br />

<form action="" method="post">
<table width="100%" height="48" border="0">
 <tr>
  <td width="20%">
    <input type="text" name="start" id="start" placeholder="Inicio" class="datepicker" autocomplete="off"/>
  </td>
  <td width="20%">
    <input type="text" name="end" id="end" placeholder="Fin" class="datepicker2" autocomplete="off"/>
  </td>
  <td width="20%">
    <select name="id_store" required>
        <option value="">--Tienda--</option>
        <?php
            $row = $row=$obj->getUserStore($_SESSION['user_id']);
            if(count($row)>0)
            {
                foreach($row as $row)
                {
                    ?>
                    <option value="<?php echo $row['poss_id']?>"><?php echo $row['poss_name']?></option>
                    <?php
                }
            }
        ?>
        <option value="todas">Todas</option>
    </select>
  </td>
  <td width="20%">
      <select name="facturado" id="facturado">
          <option value="">--Tipo Facturacion--</option>
          <option value="1">Facturado</option>
          <option value="0">No Facturado</option>
      </select>
  </td>
  <td width="20%">
      <input type="submit" name="button" id="button" value="Buscar" class="btn_submit" />
    </td>
 </tr>
</table>
</form>
<br /><br /><br />
<div id="salesGReport">
<?php
    if($_POST)
    {
        $totalg = 0;
        $taxg = 0;
        $subtog = 0;
        $id_store = $obj->postVars('id_store');
        if($id_store != "todas"){
            $stores = $obj->getStoreById($obj->postVars('id_store'));
        }else{
            $stores = $obj->getUserStore($_SESSION['user_id']);
        }

        if(count($stores)>0)
        {
            foreach($stores as $row)
            {
                $totals = 0;
                $taxs = 0;
                $subtos = 0;
                $t = 0;
                ?>
                <table>
                    <tr>
                        <td><h1><?php echo $row['poss_name']?></h1></td>
                    </tr>
                            <table>
                              <tr>
                                <th width="30">Fecha</th>
                                <th width="30">Producto</th>
                                <th width="30">Cantidad</th>
                                <th width="30">Precio</th>
                                <th width="142">Tax</th>
                                <th width="142">Total</th>
                              </tr>
                              <?php
                                
                                
                                $row1=$obj->getGlobalPosSalesByProduct($obj->postVars('start'),$obj->postVars('end'),$obj->postVars('facturado'),$row['poss_id']);
                                
                                if(count($row1)>0)
                                {
                                    foreach($row1 as $row1)
                                    {
                                        
                                        ?>
                                        <tr>
                                            <td><?php echo $row1["pos_date"]?></td>
                                            <td><?php echo $row1["posd_productname"]?></td>
                                            <?php 
                                                $cantidad = 0;
                                                $precio = 0;
                                                $tax = 0;
                                                $total = 0;

                                                if(isset($row1["type"])){
                                                    $cantidad = $row1['posd_cant'];
                                                    $precio = $row1['subtotal'];
                                                    $tax = $row1['subtotal_tax'];
                                                    $total = $row1['total'];
                                                }else{
                                                    $cantidad = $obj->getGlobalPosSalesProductDet($obj->postVars('start'),$obj->postVars('end'), $obj->postVars('facturado'),$row['poss_id'],$row1['id_product'],0);
                                                    $precio = $obj->getGlobalPosSalesProductDet($obj->postVars('start'),$obj->postVars('end'), $obj->postVars('facturado'),$row['poss_id'],$row1['id_product'],1);
                                                    $tax = $obj->getGlobalPosSalesProductDet($obj->postVars('start'),$obj->postVars('end'), $obj->postVars('facturado'),$row['poss_id'],$row1['id_product'],2);
                                                    $total = $obj->getGlobalPosSalesProductDet($obj->postVars('start'),$obj->postVars('end'), $obj->postVars('facturado'),$row['poss_id'],$row1['id_product'],3);
                                                }
                                            ?>
                                            <td><?php echo $cantidad?></td>
                                            <td>$<?php echo number_format($precio,2)?></td>
                                            <td>$<?php echo number_format($tax,2)?></td>
                                            <td>$<?php echo number_format($total,2)?></td>
                                        </tr>
                                        <?php
                                        
                                        $t += $total;
                                    }
                                
                                    
                                    ?>
                                    <tr>
                                        <td colspan="5">&nbsp;</td>
                                        <td><strong>$<?php echo number_format($t,2)?></strong></td>
                                    </tr>
                                    <tr>
                                        <td colspan="6"><hr></td>
                                    </tr>
                                    <?php
                                }
                                else
                                {
                                    ?>
                                    <tr>
                                        <td colspan="4">No hay ventas</td>
                                    </tr>
                                    <?php
                                }
                                ?>
                                </table>
                                <?php
                
                            }
                            ?>
                </table>
            <?php
        }
    }
?>
<!--<table>
    <tr>
        <th width="142">Total</th>
        <th width="142">Tax</th>
        <th width="142">Subtotal</th>
    </tr>
    <tr align="center">
        <td><h1>$<?php echo number_format($totalg,2)?></h1></td>
        <td><h1>$<?php echo number_format($taxg,2)?></h1></td>
        <td><h1>$<?php echo number_format($subtog,2)?></h1></td>
    </tr>
</table>-->
</div>
</div>
<script>
function PrintElem(elem)
{
    var mywindow = window.open('', 'PRINT', 'height=400,width=800');

    mywindow.document.write('<html><head><title>Reporte Global de Ventas</title>');
    mywindow.document.write('</head><body style=\'font-family: Arial; font-size: 12px\'>');
    mywindow.document.write('<h1>' + document.title  + '</h1>');
    mywindow.document.write(document.getElementById(elem).innerHTML);
    mywindow.document.write('</body></html>');

    mywindow.document.close(); // necessary for IE >= 10
    mywindow.focus(); // necessary for IE >= 10*/

    mywindow.print();
    mywindow.close();

    return true;
}
</script>


