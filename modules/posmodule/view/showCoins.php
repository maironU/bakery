<?php
 include('modules/posmodule/view/include.php');
 
 
 $msg=false;
 
 //Acciones
 if($obj->getVars('actionDel')=='true')
 {
     $obj->delCoins();
     $msg=true;
 }
 
 ?>
 <div class="widget3">
 <div class="widgetlegend">Monedas </div>
 <?php
  if($msg)
  {
  ?>
   <div class="ui-widget">
	<div class="ui-state-highlight ui-corner-all" style="margin-top: 20px; padding: 0 .7em;">
		<p><span class="ui-icon ui-icon-info" style="float: left; margin-right: .3em;"></span>
		<strong>Felicitaciones!</strong> se ha eliminado satisfactoriamente.</p>
	</div>
</div>
  <?php
  }
 ?>
<p style="width:100">
	<a href="<?php $_SERVER['PHP_SELF'];?>?p=modules/posmodule/view/newCoins.php" class="btn_normal" style="float:left; margin:5px;">Nuevo </a>
</p><br /><br /><br />
<table width="100%" height="48" border="0">
  <tr>
    <th>ID</th>
    <th>Imagen</th>
    <th>Nombre</th>
    <th>Valor</th>
    <th colspan="2">&nbsp;</th>
  </tr>
  <?php
   $row=$obj->getCoins();
   if(count($row)>0)
   {
       foreach($row as $row)
       {
           ?>
            <tr>
                <td><?php echo $row['posc_id']?></td>
                <td><img src="modules/posmodule/imagesCoins/<?php echo $row['posc_image']?>" width="50" height="50" /></td>
                <td><?php echo $row['posc_name']?></td>
                <td><?php echo $row['posc_value']?></td>
                <td width="86"><a href="<?php $_SERVER['PHP_SELF'];?>?p=modules/posmodule/view/editCoins.php&id=<?php echo $row["posc_id"]?>" class="btn_normal">Editar</a></td>
                <td width="86"><a href="<?php $_SERVER['PHP_SELF'];?>?p=modules/posmodule/view/showCoins.php&id=<?php echo $row["posc_id"]?>&actionDel=true" class="btn_borrar">Eliminar</a></td>
              </tr>
           <?php
       }
   } 
  ?>
</table>

</div>
