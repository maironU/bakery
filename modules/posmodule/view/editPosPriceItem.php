<?php
    include('include_self.php');
	
		
 	$id=$obj->postVars('id');
	$price=$obj->postVars('price');
	
	$obj->editPriceItem($id, $price);
	
	
	$row=$obj->getPosDetailTemp();
	
	$html.='
	 <table border="0" width="100%">
	  <tr>
	    <th>Cantidad</th>
	  	<th>Producto</th>
		<th>Precio</th>
		<th>Tax</th>
		<th>Total</th>
		<th>&nbsp;</th>
	  </tr>
	';
	
	if(count($row)>0){
	 foreach($row as $row){
	  $html.="
	   <tr>
	   <td><table>
                    		            <tr>
                    		                <td><a href='javascript:;' onClick='editQtyItem(".$row["posdt_id"].", ".$row["posdt_qty"].", 1)'><i class='fa fa-plus-circle' style='font-size:24px'></i></a></td>
                    		            </tr>
                    		            <tr>
                    		                <td><input type='text' name='qty' value='".$row["posdt_qty"]."' onChange='editQtyItem(".$row["posdt_id"].", this.value, 2)' /></td>
                    		            </tr>
                    		            <tr>
                    		                <td><a href='javascript:;' onClick='editQtyItem(".$row["posdt_id"].", ".$row["posdt_qty"].", 0)'><i class='fa fa-minus-circle' style='font-size:24px'></i></a></td>
                    		            </tr>
                    		        </table></td>
	    <td>".utf8_encode($row["posdt_productname"])."</td>";
		if($products->isVariablePrice($row['id_product']))
                    			{
                    			    $html.="<td><input type='text' name='price' value='".$row["posdt_price"]."' onChange='editPriceItem(".$row["posdt_id"].", this.value)'/></td>";
                    			}
                    			else
                    			{
                    			    $html.="<td>$".number_format($row["posdt_price"],2)."</td>";
                    			}
		$html.="<td>$".number_format($row["posdt_tax"]*$row["posdt_qty"],2)."</td>
		<td>$".number_format(($row["posdt_price"]+$row["posdt_tax"])*$row["posdt_qty"],2)."</td>
		<td>
		 <a href='javascript:;' onClick='delPosItem(".$row["posdt_id"].")' class='btn_2'>Eliminar</a>
		</td>
	   </tr>
	  ";
	 }
	}
	
	$html.="</table>";
	echo $html;
?>
