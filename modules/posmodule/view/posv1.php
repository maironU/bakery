<?php
 include('modules/posmodule/view/include.php');
 
 
 $id = $obj->getVars('id');
 $_SESSION['id_store'] = $id;
 
?>
<script type="text/javascript" src="modules/posmodule/js/jsPosFunctions.js"></script>
<!-- Add mousewheel plugin (this is optional) -->
<script type="text/javascript" src="modules/posmodule/js/jquery.mousewheel-3.0.6.pack.js"></script>
<!-- Add fancyBox main JS and CSS files -->
<script type="text/javascript" src="modules/posmodule/js/jquery.fancybox.js?v=2.1.4"></script>
<link rel="stylesheet" type="text/css" href="modules/posmodule/css/jquery.fancybox.css?v=2.1.4" media="screen" />
<link rel="stylesheet" type="text/css" href="modules/posmodule/css/stylePos.css" />

<div class="widget3">
 <div class="widgetlegend">Punto de venta </div>
 <?php
  //Acciones
 $action=$obj->postVars('action');
 
 ///Add Item
 if($action=='additem')
 {
     $barcode=$obj->postVars('barcode');
     $qty=$obj->postVars('qty');
     $row=$products->getProductsByReference($barcode);
     foreach($row as $row)
     {
         $product_id=$row["pro_id"];
         $product_name=$row["pro_name"];
         $product_price=$row["pro_price"];
	 }
	 
	$row = $obj->getStoreById($_SESSION['id_store']);
	if(count($row)>0)
	{
	    foreach($row as $row)
	    {
	        $id_wh = $row['id_warehouse'];
	        $store_name = $row['poss_name'];
	    }
	}
	
	$inv_id = $obj->getInventoryProductId($id_wh, $product_id);
	if($inv_id != 0 || $inv_id != '')
	{
	    $desc = $obj->getPosSetupById(6)." ".$store_name;
	    $caps->registerMov($inv_id,$qty,$_SESSION['user_id'],'',2,$product_id,$desc,$id_wh,$whd,$referral, 'no');
	    $obj->newposDetailTemp($product_id,$product_name,$product_price,$qty);
	}
	 
	
 }
 
 //DelItem
 if($action=='delitem')
 {
     $user=$obj->postVars('user');
     $pass=$obj->postVars('pass');
     $desc=$obj->postVars('desc');
     $posdt_id=$obj->postVars('posdt_id');
     
     $id_user=$obj->getUserId($user,$pass);
  	 //if(!empty($id_user))
  	 {
  	     $letAuth=$obj->letAuth($id_user);
  	     //if($letAuth)
  	     {
  	         $row=$obj->getPosDetailTempById($posdt_id);
  	         foreach($row as $row)
  	         {
  	             $value=$row["posdt_price"] * $row["posdt_qty"];
  	             $id_product=$row["id_product"];
  	             $qty=$row["posdt_qty"];
  	         }
  	         
  	         $obj->delposDetailTemp($posdt_id);
  	         $obj->newAuthLog($id_user,"Eliminacion de Item punto de venta",$value,$desc);
  	         
  	         $row = $obj->getStoreById($_SESSION['id_store']);
        	if(count($row)>0)
        	{
        	    foreach($row as $row)
        	    {
        	        $id_wh = $row['id_warehouse'];
        	        $store_name = $row['poss_name'];
        	    }
        	}
        	
        	$inv_id = $obj->getInventoryProductId($id_wh, $id_product);
        	if($inv_id != 0 || $inv_id != '')
        	{
        	    $caps->registerMov($inv_id,$qty,$_SESSION['user_id'],'',1,$product_id,$desc,$id_wh,$whd,$referral, 'no');
        	    $obj->newposDetailTemp($product_id,$product_name,$product_price,$qty);
        	}
		
    		?>
              <div class="ui-widget">
               <div class="ui-state-highlight ui-corner-all" style="margin-top: 20px; padding: 0 .7em;">
                <p><span class="ui-icon ui-icon-info" style="float: left; margin-right: .3em;"></span>
                   <strong>Felicitaciones!</strong> Item Retirado.</p>
                </div>
              </div>
            <?php
	  }
	  /*else
	  {
		?>
          <div class="ui-widget">
             <div class="ui-state-error ui-corner-all" style="margin-top: 20px; padding: 0 .7em;">
               <p><span class="ui-icon ui-icon-alert" style="float: left; margin-right: .3em;"></span>
               <strong>Error!</strong> Este usuario no tiene permiso para autorizar</p>
            </div>
          </div>
        <?php  
	  }*/
	 }
	 /*else
	 {
	    ?>
          <div class="ui-widget">
             <div class="ui-state-error ui-corner-all" style="margin-top: 20px; padding: 0 .7em;">
               <p><span class="ui-icon ui-icon-alert" style="float: left; margin-right: .3em;"></span>
               <strong>Error!</strong> Usuario no existe</p>
            </div>
          </div>
        <?php 
	 }*/
  }
 
 
 
 
 if($action=='closepos'){
  $cash=$obj->postVars('cash');
  $devol=$obj->postVars('devol');
  
  if($cash=='' || $devol=='')
  {
      ?>
      <div class="ui-widget">
                 <div class="ui-state-error ui-corner-all" style="margin-top: 20px; padding: 0 .7em;">
                   <p><span class="ui-icon ui-icon-alert" style="float: left; margin-right: .3em;"></span>
                   <strong>Error!</strong> Para cerrar debe ingresar el dinero entregado</p>
                </div>
              </div>
      <?php
  }
  else
  {
      $id=$obj->newPos();
   
       ?>
       <script>
        showInfo(<?php echo $id; ?>,'modules/posmodule/view/showReceipt.php')
       </script>
      <?php
    
  }
  
 }
  
 ?>
<p style="width:100">
    <!--<a href="<?php $_SERVER['PHP_SELF'];?>?p=modules/tracker/view/showMsg.php" class="btn_normal" style="float:left; margin:5px;">Mensajes </a> -->
</p>
 
<table width="800" border="0">
 <td valign="top">
 	<table width="100%">
	 <tr>
	  <td>
        <form action="#" method="post">
	  	<table width="550">
		 <tr>
		  <td><strong>Cantidad:</strong><input type="text" name="qty" id="qty" value="1" /></td>
		  <td><strong>Producto:</strong><input type="text" name="barcode" id="barcode" autofocus="autofocus" /></td>
		  <td><input type="button" value="Agregar" onclick="findPosList()" class="btn_submit" /></td>
		 </tr>
		</table>
        </form>
	  </td>
	 </tr>
	 <tr>
	  <td>
	   <div id="showposL">
	    <?php
		 $row=$obj->getPosDetailTemp();
		 $html.='
			 <table border="0" width="100%">
			  <tr>
				<th>Producto</th>
				<th>Cantidad</th>
				<th>Precio</th>
				<th>Total</th>
				<th>&nbsp;</th>
			  </tr>
		  ';
		  
		if(count($row)>0){
		 foreach($row as $row){
		  $html.="
		   <tr>
			<td>".($row["posdt_productname"])."</td>
			<td>".$row["posdt_qty"]."</td>
			<td>$".number_format($row["posdt_price"])."</td>
			<td>$".number_format($row["posdt_price"]*$row["posdt_qty"])."</td>
			<td>
			 <form action='#' name='formDI' method='post'>
			  <input type='hidden' name='user' id='user' placeholder='usuario' />
			  <input type='hidden' name='pass' id='pass' placeholder='contraseņa' />
			  <input type='hidden' name='desc' id='desc' placeholder='descripcion' />
			  <input type='hidden' name='posdt_id' id='posdt_id' value='".$row["posdt_id"]."' />
			  <input type='hidden' name='action' id='action' value='delitem' />
			  <input type='submit' value='Eliminar' />
			 </form>
			</td>
		   </tr>
		  ";
		 }
		}
	
		$html.="</table>";
		echo $html;
		?>
	   </div>
	   <div id="gauch2" style="display:none"><img src="modules/posmodule/images/loading.gif" /></div>
	  </td>
	 </tr>
	</table>
 </td>
 <td valign="top"><form action="#" name="formpos1" method="post">
			<table width="100%">
		 <tr>
		  <td><span style="float:left">Cliente:</span> <input type="text" id="finder" value="<?php echo $_SESSION["id_customer"]?>" style="float:left"/>
 <a href="javascript:;" onclick="findCustomer()" class="btn_2" style="float:left">Buscar</a>
 <a href="javascript:;" onclick="showInfo(0,'modules/posmodule/view/newSimpleCustomer.php')" class="btn_1" style="float:left">Nuevo</a>
 <br /><br /><br />
<div id="gauch" style="display:none"><img src="modules/posmodule/images/loading.gif" /></div>
 <div id="showCl">&nbsp;</div></td>
		 </tr>
		 <tr>
		  <td>
		  <strong>TOTAL :</strong>
		  <?php $total=$obj->getPosTotal();?>
		  <div id="totalpos"><h3>$<?php echo number_format($total)?></h3></div>
		  <input name="total" id="total" type="hidden" value="<?php echo $total; ?>" />
		  </td>
		 </tr>
		 <tr>
		  <td>
		  <strong>EFECTIVO :</strong>
		  $
		    <input name="cash" id="cash" type="text" value="0" onchange="calcDelvol()" /></td>
		 </tr>
		 <tr>
		  <td>
		  <strong>CAMBIO :</strong>
		  $
		    <input name="devol1" id="devol1" type="text" value="0" disabled="disabled" />
			<input name="devol" id="devol" type="hidden" value="0" /></td>
		 </tr>
		 <tr>
		  <td><input type="hidden" name="action" id="action" value="closepos" /><input name="u_id" type="hidden" id="u_id" value="<?php echo $_SESSION["user_id"];?>" /><a href="javascript:;" class="btn_normal" onclick="document.formpos1.submit();">Pagar</a></td>
		 </tr>
		</table>
		</form></td>
</table>

</div>

