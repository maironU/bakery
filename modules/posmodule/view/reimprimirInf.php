<?php
 include('modules/posmodule/view/include.php');
 
 
 $msg=false;
 
 //Acciones
 ?>
<div class="widget3">
 <div class="widgetlegend">Informe de ventas </div>
 <?php
  if($msg)
  {
  ?>
   <div class="ui-widget">
	<div class="ui-state-highlight ui-corner-all" style="margin-top: 20px; padding: 0 .7em;">
		<p><span class="ui-icon ui-icon-info" style="float: left; margin-right: .3em;"></span>
		<strong>Felicitaciones!</strong> eliminado satisfactoriamente.</p>
	</div>
</div>
  <?php
  }
 ?>
<p style="width:100">
	<a href="<?php $_SERVER['PHP_SELF'];?>?p=modules/posmodule/view/index.php" class="btn_normal" style="float:left; margin:5px;">Volver </a>
    </p>
 <br /><br /><br />

<form action="" method="post">
<table width="100%" height="48" border="0">
 <tr>
  <td width="20%">
    <input type="text" name="start" id="start" placeholder="Inicio" class="datepicker" />
  </td>
  <td width="20%">
    <input type="text" name="end" id="end" placeholder="Fin" class="datepicker2" />
  </td>
  <td width="20%">
    <select name="id_store">
        <option value="">--Tienda--</option>
        <?php
            $row=$obj->getUserStore($_SESSION['user_id']);
            if(count($row)>0)
            {
                foreach($row as $row)
                {
                    ?>
                    <option value="<?php echo $row['poss_id']?>"><?php echo $row['poss_name']?></option>            
                    <?php
                }
            }
        ?>
    </select>
  </td>
  <td width="20%">
      <select name="facturado" id="facturado">
          <option value="">--Tipo Facturacion--</option>
          <option value="1">Facturado</option>
          <option value="0">No Facturado</option>
      </select>
  </td>
  <td width="20%">
      <input type="submit" name="button" id="button" value="Buscar" class="btn_submit" />
    </td>
 </tr>
</table>
</form>
<br /><br /><br />
<table width="912" height="48" border="0">
  <tr>
    <th width="30">ID</th>
    <th width="30">Fecha</th>
    <th width="142">Track</th>
    <th width="142">Total</th>
    <th width="70">Tienda</th>
    <th width="70">Usuario</th>
    <th width="70">Medio de Pago</th>
    <th width="70">Tipo de Facturacion</th>
    <th width="70">&nbsp;</th>
  </tr>
<?php
 if($_POST){
  $row=$obj->getPosSales($obj->postVars('start'),$obj->postVars('end'),$obj->postVars('facturado'), $obj->postVars('id_store'));  
  if(count($row)>0){
  foreach($row as $row){
   ?>
   <tr>
    <td><?php echo $row["pos_id"]?></td>
    <td><?php echo $row["pos_dateadd"]?></td>
    <td><?php echo $row["pos_track"]?></td>
    <td>$<?php echo number_format($row["pos_total"],2)?></td>
    <td><?php 
	$row1=$obj->getStoreById($row["id_store"]);
	foreach($row1 as $row1){
	 echo $row1["poss_name"];
	}
	?></td>
    <td><?php
     $usr->getUserById($row["u_id"]);
	 echo $usr->name;
	?></td>
	<td>
	    <?php
	        if($row['pospm_id'] == 0)
	        {
	            ?>
	            Efectivo
	            <?php
	        }
	        else
	        {
	            $row1 = $obj->getPaymentMethodsById($row['pospm_id']);
	            if(count($row1)>0)
	            {
	                foreach($row1 as $row1)
	                {
	                    echo $row1['pospm_name'];
	                }
	            }
	        }
	    ?>
    </td>
    <td><?php echo $row['pos_facture'] == 1 ? "Facturado" : "No Facturado"?></td>
    <td><a href="modules/posmodule/view/showReceipt.php?id=<?php echo $row["pos_id"]; ?>&type=<?php echo $row["type"] ?>" class="btn_normal" target="_blank">Imprimir </a></td>
   </tr>
   <?php
   $total+=$row["pos_total"];
   $totalcash+=$row["pos_cash"];
   $totaldevol+=$row["pos_deliver"];
   $subto+=$row["pos_subtotal"];
   $tax+=$row["pos_tax"];
  }
  }
  else{
   ?>
    <tr>
     <td colspan="8">No hay ventas</td>
    </tr>
   <?php
  }
 }
?> 

 
 </tr>
 <?php
    $row = $obj->getPaymentMethods1();
    if(count($row)>0)
    {
        foreach($row as $row)
        {
            ?>
            <tr>
                <td colspan="8">
                   <strong><?php echo $row['pospm_name']?>: $<?php 
                   $t = $obj->gePaymmentMethodTotals($obj->postVars('start'), $obj->postVars('end'), $obj->postVars('id_store'),$row['pospm_id']);
                   echo number_format($t[0],2); ?></strong>
                </td>
             </tr>
            <?php
        }
    }
 ?>
</table>

</div>


