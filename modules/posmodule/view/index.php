<?php
 include('modules/posmodule/view/include.php');
 
 
 $msg=false;
 
 //Se verifica si el modulo esta instalado
 if(!$obj->Checker()){
  $ins=true;
 }
 
 $msg=false;
 $msgIns=false;
  
 //Instala el modulo
 if($obj->getVars('install')){
  $obj->install();
  $msgIns=true;
 }
 ?>
 <?php
 if(!$ins){
 ?>
 <div class="widget3">
 <div class="widgetlegend">Tiendas </div>
 <?php
  if($msg)
  {
  ?>
   <div class="ui-widget">
	<div class="ui-state-highlight ui-corner-all" style="margin-top: 20px; padding: 0 .7em;">
		<p><span class="ui-icon ui-icon-info" style="float: left; margin-right: .3em;"></span>
		<strong>Felicitaciones!</strong> se ha eliminado satisfactoriamente.</p>
	</div>
</div>
  <?php
  }
 ?>
<p style="width:100">
	<!--<a href="<?php $_SERVER['PHP_SELF'];?>?p=modules/posmodule/view/newStore.php" class="btn_normal" style="float:left; margin:5px;">Nuevo </a>-->
</p><br /><br /><br />
<table width="100%" height="48" border="0">
  <tr>
    <th>ID</th>
    <th>Imagen</th>
    <th>Nombre</th>
    <th colspan="1">&nbsp;</th>
  </tr>
  <?php
   $row=$obj->getUserStore($_SESSION['user_id']);
   if(count($row)>0)
   {
       foreach($row as $row)
       {
           ?>
            <tr>
                <td><?php echo $row['poss_id']?></td>
                <td><i class="fa fa-building"></i></td>
                <td><?php echo $row['poss_name']?></td>
                <td width="86"><a href="<?php $_SERVER['PHP_SELF'];?>?p=modules/posmodule/view/pos.php&id=<?php echo $row["poss_id"]?>" class="btn_normal">Entrar</a></td>
              </tr>
           <?php
       }
   } 
  ?>
</table>

</div>
<?php } else {?>
<br />
 
 <div class="widget3">
 <div class="widgetlegend">Instalador Punto de venta </div>
 <?php
  if($msgIns)
  {
  ?>
   <div class="ui-widget">
	<div class="ui-state-highlight ui-corner-all" style="margin-top: 20px; padding: 0 .7em;">
		<p><span class="ui-icon ui-icon-info" style="float: left; margin-right: .3em;"></span>
		<strong>Felicitaciones!</strong> El modulo ha sido instalado satisfactoriamente.</p>
	</div>
</div>
  <?php
  }
 ?>
<form id="form1" name="form1" method="post" action="#">
  <table width="804" border="0" style="float:left;">
    <tr>
      <td width="525" valign="top">&nbsp;</td>
    </tr>
    <tr>
      <td valign="top"><div align="right"><a href="<?php $_SERVER['PHP_SELF'];?>?p=modules/posmodule/view/index.php&amp;install=true" class="btn_normal">Instalar</a></div></td>
    </tr>
  </table>
</form>
</div>
<?php } ?>
