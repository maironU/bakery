
<?php
    include('modules/posmodule/view/include.php');
    $id = $obj->getVars('id');
    $store = $obj->getStoreById($id);
    $_SESSION['id_store'] = $id;
    $_SESSION['mode_list']=0;

    $methodsPay = $obj->getPaymentMethodsWithoutStoreVirtual();
?>

<script type="text/javascript" src="modules/posmodule/js/jsPosFunctions.js"></script>
<!-- Add mousewheel plugin (this is optional) -->
<script type="text/javascript" src="modules/posmodule/js/jquery.mousewheel-3.0.6.pack.js"></script>
<!-- Add fancyBox main JS and CSS files -->
<script type="text/javascript" src="modules/posmodule/js/jquery.fancybox.js?v=2.1.4"></script>
<link rel="stylesheet" type="text/css" href="modules/posmodule/css/jquery.fancybox.css?v=2.1.4" media="screen" />
<link rel="stylesheet" type="text/css" href="modules/posmodule/css/stylePos.css" />
<script>
    $(document).ready(function() {
        $("#name-store").text("<?php echo $store[0]['poss_name'] ?>")
    })
</script>
<style>
    /*mis estilos*/
    table {
            border: 0;
        }
        table thead {
            font-size: 19px;
            height: 40px;
        }

        table thead tr th, td{
            padding: 8px 12px 10px 8px; 
            text-align: center;
        }

        table thead tr th, td span{
            font-size: 12px !important;
            padding-right: 10px; 
        }

        #table-carrito tbody tr td{
            background: white !important;
        }

        table tbody tr td{
            max-width: 40px;
        }

        table tbody tr td img{
            margin-bottom: 2px;
            cursor: pointer;
            float: right;
        }

        table tbody {
            font-size: 10px;
        }

        .content__products li{
            list-style: none;
        }

        a{
            text-decoration: none;
            color: #858796;
        }

        #add-product:focus {
            outline: none;
        }
</style>

<div class="mb-3 d-flex flex-column justify-content-between h-100">

    <div class="content__parent justify-content-between">
        <div class="d-flex flex-column justify-content-between radius w-xl-40 bg-white h-100 content__table-parent h-100">   
            <div class="content__table" id="content__table">
            <div class="w-90" style="border-bottom: 1px solid #ddd !important;padding: 8px; margin: 0 auto">
                <input type="text" class="border-0 w-90" id="add-product" placeholder="AGREGAR PRODUCTO">
                <img src="images/icons/search.png" width="20px" height="21px" alt="" onclick="addProductByReference()">
            </div> 
            <?php
                $carrito=$obj->getPosDetailTemp();
            ?>
                <table class="w-100" id="table-carrito">
                    <thead>
                        <tr>
                            <th>CANTIDAD</th>
                            <th>PRODUCTO</th>
                            <th>PRECIO</th>
                            <th>TAX</th>
                            <th>TOTAL</th>
                        </tr>
                    </thead>
                    <tbody>
                        <?php 
                            if(count($carrito)>0){
                                foreach($carrito as $product){
                                    $html.="
                                        <tr>
                                        <td>
                                            <input type='text' name='qty' value='".$product['posdt_qty']."' onChange='editQtyItem(".$product["posdt_id"].", this.value, 2)' class='border-0 w-25' style='font-size: 15px; height: 20px'>
                                            <img src='images/icons/more.png' alt='' width='22px' class='m-0' onClick='editQtyItem(".$product['posdt_id'].", ".$product['posdt_qty'].", 1)'>
                                            <img src='images/icons/less.png' alt='' width='22px' class='m-0' onClick='editQtyItem(".$product['posdt_id'].", ".$product['posdt_qty'].", 0)'> 
                                        </td>
                                    <td>".utf8_encode($product["posdt_productname"])."</td>";
                                    if($products->isVariablePrice($product['id_product']))
                                                            {
                                                                $html.="<td><input type='text' name='price' value='".$product["posdt_price"]."' onChange='editPriceItem(".$product["posdt_id"].", this.value)'/></td>";
                                                            }
                                                            else
                                                            {
                                                                $html.="<td>$".number_format($product["posdt_price"],2)."</td>";
                                                            }
                                    $html.="<td>$".number_format($product["posdt_tax"]*$product["posdt_qty"],2)."</td>
                                    <td>$".number_format(($product["posdt_price"]+$product["posdt_tax"])*$product["posdt_qty"],2)."</td>
                                    <td>
                                        <img src='images/icons/eliminar.png' alt='Eliminar' width= '20px' class='pointer' onClick='delPosItem(".$product["posdt_id"].")'>
                                    </td>
                                </tr>
                                ";
                                }
                                }
	
                            $html.="</table>";
                            echo $html;
                        ?>
                    </tbody>
                </table>
            </div>
            <div style="border-top: 1px solid #5D6060" class="d-flex justify-content-end align-items-center content__total">
                <?php $total=$obj->getPosTotal();?>
                <h4 class="mr-5 text-dark">TOTAL</h4>
                <h4 class="text-color text-bold" id="totalpos">$<?php echo number_format($total,2)?></h4>
                <input name="total" id="total" type="hidden" value="<?php echo $total; ?>" />
            </div>
        </div>
        

        <div class="w-xl-58 content__products p-0 radius h-100">
            <div class="d-flex flex-column content__categories">
                <?php
                    $categories = $products->getCategoriesFiltered();
                    foreach($categories as $categorie) { ?>
                        <li id="li_<?php echo $categorie['cat_id']?>" class="card_li_product">
                            <div class="aux_sup">
                                <div id="sup_<?php echo $categorie['cat_id']?>" class="aux_sup-son"></div>
                            </div>
                            <div class="w-100 aux_sup_may">
                                <div class="content__categories-son" onClick="findProductByCategory2(<?php echo $categorie['cat_id']?>)" id="cat_<?php echo $categorie['cat_id']?>">   
                                    <div class="content__categories-into">
                                        <img src="modules/products/imagesCat/<?php echo $categorie['cat_image'] ?>" alt="<?php echo $categorie['cat_name']?>" width="35px">
                                    </div>
                                </div>
                            </div>
                            <div class="aux_sup">
                                <div id="down_<?php echo $categorie['cat_id']?>" class="aux_sup-son"></div>
                            </div>
                        </li>
                <?php } ?>
            </div>
            <div class="d-flex flex-column px-4 content__products-parent"> 
                <div class="mb-3 row justify-content-between align-items-end py-2">
                    <div class="col-9 d-flex justify-content-between" style="border-bottom: 1px solid #BFBFBF !important;">
                        <span id="aux_prodname" class="border-0 w-90" style="padding: 1px 2px 1px 2px; cursor: text">Buscar</span>
                        <input type="text" placeholder="Buscar" class="border-0 w-90 d-none" name="prodname" id="prodname" onkeyup="findProductByName()" autocomplete="off">
                    </div>
                    <div class="col-2">
                        <input type="hidden" id="id_cat" value="1">
                        <div id="icon_mode">
                            <img src="images/icons/options_content.png" alt="" width="25px" onclick="modeList()">  
                        </div>
                    </div>
                </div>
                <div class="row m-0 content__products" id="products_card">
                    
                </div>
            </div>
        </div>
    </div> 

    <!-- -->

    <div class="content__parent-footer mt-3">
        <div class="w-40 d-flex flex-column content__parent-footer-prom">
            <div class="d-flex">
            <?php 
                if(count($carrito)>0){
            ?>
                <div class="w-49 card_footer-codigo mr-3" data-toggle="modal" data-target="#discount-employee" data-backdrop="false" class="btn_normal" onMouseOver="modal('desce', 'btndesce')">
                    <span class="text-center">DESCUENTO EMPLEADOS</span>
                </div>
                <div class="w-49 card_footer-codigo" id="view_cliente">
                    <a href="<?php $_SERVER['PHP_SELF'];?>/modules/posmodule/front/index.php?id_store=<?php echo $_SESSION['id_store']?>" target="_blank" class="text-center">
                        <span class="text-center">VISTA CLIENTE</span>
                    </a>
                </div>
            <?php } else { ?>
                <div class="w-49 card_footer-codigo mr-3" data-toggle="modal" data-target="#discount-employee" data-backdrop="false" class="btn_normal" onMouseOver="modal('desce', 'btndesce')" style="display: none">
                    <span class="text-center">DESCUENTO EMPLEADOS</span>
                </div>
                <div class="card_footer-codigo" id="view_cliente" style="width: 100%">
                    <a href="<?php $_SERVER['PHP_SELF'];?>/modules/posmodule/front/index.php?id_user=<?php echo $_SESSION['user_id']?>&id_store=<?php echo $_SESSION['id_store']?>" target="_blank" class="text-center">
                        <span class="text-center">VISTA CLIENTE</span>
                    </a>
                </div>
            <?php } ?>
            </div>
            <div class="card_footer-codigo mt-2">
                <span class="text-center" data-toggle="modal" data-target="#codigo-promocional" data-backdrop="false">CODIGO PROMOCIONAL</span>
            </div>
        </div>
            <div class="w-58 content__parent-methodspay" id="content__parent-methodspay" style="display: none">
                <div class="d-flex flex-wrap justify-content-between content_card_footer-codigo w-xl-75">
                    <input name="u_id" type="hidden" id="u_id" value="<?php echo $_SESSION["user_id"];?>" />
                    <div class="w-32 card_footer-codigo d-flex flex-column mb-2 method-pay" id="method-0" onclick="selectMethodPay(0)">
                        <img width="60px" src="images/methods_pay/Recurso 8.png" alt="">
                        <span><small>Efectivo</small></span>
                    </div>
                    <?php
                        $i = 1;
                        if(count($methodsPay)>0){
                            foreach($methodsPay as $methodPay){
                        ?>
                                <div class="w-32 card_footer-codigo mb-2 method-pay" id="method-<?php echo $methodPay['pospm_id'] ?>" onclick="selectMethodPay(<?php echo $methodPay['pospm_id'] ?>)">
                                    <img width="70px" src="modules/posmodule/imagesPm/<?php echo $methodPay["pospm_image"] ?>" alt="<?php echo $methodPay['pospm_name'] ?>">
                                </div>
								<input name="txs" id="txs<?php echo $methodPay['pospm_id'] ?>" type="hidden" value="" onChange="enableBtn<?php echo $method['pospm_id'] ?>"  /><br>
                            <?php 
                                    $i++;
                            } 
                        }
                        ?>
                        <div class="w-32 card_footer-codigo">
                            <span style="font-size: 30px; cursor: pointer">+</span>
                        </div>
                    <input type="hidden" name="i" id="i" value="<?php echo $i?>" ?>
                </div>
                <div class="w-24 button-pay mb-2 d-flex flex-column" id="content-button-pay">
                    <input type="button" value="PAGAR" class="form-control bg-color-bottom text-white" onclick="pay()" id="button-pay">
                    <input type="button" value="SIN FACTURA" class="form-control bg-color-bottom text-white" onclick="withoutFacture()" id="button-sin-factura">
                </div>
            </div>
    </div>
</div>

<!-- MODAL SELECT METODO DE PAGO Y PAGAR -->
<div class="modal fade" id="pay-method" tabindex="-1" role="dialog" aria-labelledby="pay-method" aria-hidden="true">
  <div class="modal-dialog" style="max-width: 90%" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <h5 class="modal-title" id="method-pay-name"></h5>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <div class="modal-body d-flex justify-content-between" id="body-pay">
        
      </div>
    </div>
  </div>
</div>

<!-- MODAL CODIGO PROMOCIONAL -->
<div class="modal fade" id="codigo-promocional" tabindex="-1" role="dialog" aria-labelledby="codigo-promocional" aria-hidden="true">
  <div class="modal-dialog" style="width: 40%; min-width: 360px !important; margin: 0 auto" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <h5 class="modal-title" id="method-pay-name">A&ntildeadir Cup&oacuten</h5>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <div class="modal-body d-flex flex-column align-items-center mt-3">
        <div class="form-group">
            <input type="text" id="cupon" name="cupon" placeholder="Escribe el cupon" class="form-control w-100">
        </div>

        <div class="form-group">
            <input type="button" value="Anadir" class="btn bg-color-bottom text-white" onClick="validateCoupon()"/>
        </div>
      </div>
    </div>
  </div>
</div>

<!-- MODAL DESCUENTO EMPLEADOS -->
<div class="modal fade" id="discount-employee" tabindex="-1" role="dialog" aria-labelledby="discount-employee" aria-hidden="true">
  <div class="modal-dialog" style="width: 50%; min-width: 360px !important; margin: 0 auto" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <h5 class="modal-title" id="method-pay-name">Descuento para empleados</h5>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <div class="modal-body">
            <div class="form-group">
                <label for="">Usuario que autoriza</label>
                <input type="text" name='usr' id='usr' placeholder="Usuario" class="form-control"/>
            </div>
            <div class="form-group">
                <input type="password" name='pass' id='pass' placeholder="Contrase&ntilde;a" class="form-control"/>
            </div>
            <div class="form-group">
                <label for="Uusario que autoriza">Porcentaje de descuento a aplicar</label>
                <input type="text" name='por' id='por' placeholder="(%) Porcentaje de descuento" class="form-control"/>
            </div>
            <div class="form-group">
                <label for="Uusario que autoriza">Descripcion</label>
                <input type="text" name='desc' id='desc1' value="Descuento para empleado" class="form-control"/>
            </div>
        </div>
        <div class="modal-footer">
            <button type="button" class="btn btn-secondary" data-dismiss="modal">Cerrar</button>
            <input type="button" value="Registrar" onClick="addDiscount()" class="btn bg-color-bottom text-white" />
        </div>
    </div>
  </div>
</div>

<!-- MODAL PAGO EFECTIVO -->
<div class="modal fade" id="pay-method-efectivo" tabindex="-1" role="dialog" aria-labelledby="pay-method" aria-hidden="true">
  <div class="modal-dialog mt-0" style="max-width: 90%; margin: 0 auto" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <h5 class="modal-title" id="method-pay-name">Pago efectivo</h5>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <div class="modal-body d-flex justify-content-between mt-3 mb-3" id="body-pay-efectivo">
        <div id="modal-content-coins" class="d-flex flex-wrap w-75">

        </div>
        <div id="modal-content-resumen" class="d-flex flex-column align-items-start w-25" style="min-width:230px">

        </div>
      </div>
    </div>
  </div>
</div>

<script>
    $(document).ready(function(){
        $("#aux_prodname").addClass('d-none')
        $("#prodname").addClass("d-block")
        $("#prodname").focus()
    })
</script>



