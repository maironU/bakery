<?php
	include('include_self.php');
?>
<table width="803" height="48" border="0" style="border: #999 1px solid; font-family:Arial, Helvetica, sans-serif; font-size:11px">
  <tr>
     <td colspan="5"><h1>Reporte de ventas sistema de <?php echo $obj->customername; ?></h1></td>
    </tr>
  <tr>
    <tr>
    <th width="30">ID</th>
    <th width="30">Fecha</th>
    <th width="142">Track</th>
    <th width="142">Total</th>
    <th width="70">Tienda</th>
    <th width="70">Usuario</th>
    <th width="70">Medio Pago</th>
  </tr>
  
<?php
 if($_GET){
  $row=$obj->getPosSales($obj->getVars('start'),$obj->getVars('end'),$obj->getVars('id_store'));
  if(count($row)>0){
  foreach($row as $row){
   ?>
   <tr>
    <td><?php echo $row["pos_id"]?></td>
    <td><?php echo $row["pos_dateadd"]?></td>
    <td><?php echo $row["pos_track"]?></td>
    <td>$<?php echo number_format($row["pos_total"],2)?></td>
    <td><?php 
	$row1=$obj->getStoreById($row["id_store"]);
	foreach($row1 as $row1){
	 echo $row1["poss_name"];
	}
	?></td>
    <td><?php
     $usr->getUserById($row["u_id"]);
	 echo $usr->name;
	?></td>
	<td>
	    <?php
	        if($row['pospm_id'] == 0)
	        {
	            ?>
	            Efectivo
	            <?php
	        }
	        else
	        {
	            $row1 = $obj->getPaymentMethodsById($row['pospm_id']);
	            if(count($row1)>0)
	            {
	                foreach($row1 as $row1)
	                {
	                    echo $row1['pospm_name'];
	                }
	            }
	        }
	    ?>
	</td>
    <td><a href="modules/posmodule/view/showReceipt.php?id=<?php echo $row["pos_id"]; ?>" class="btn_normal" target="_blank">Imprimir </a></td>
   </tr>
   <?php
  $total+=$row["pos_total"];
   $totalcash+=$row["pos_cash"];
   $totaldevol+=$row["pos_deliver"];
   $subto+=$row["pos_subtotal"];
   $tax+=$row["pos_tax"];
  }
  }
  else{
   ?>
    <tr>
     <td colspan="7">No hay ventas</td>
    </tr>
   <?php
  }
 }
?> 
<tr>
   <td colspan="7"><strong>Total: $<?php echo number_format($total,2); ?></strong></td>
 </tr>
 <tr>
   <td colspan="7"><strong>Impuestos: $<?php echo number_format($tax,2); ?></strong></td>
 </tr>
 <tr>
   <td colspan="7"><strong>Total antes de Impuestos: $<?php echo number_format($subto,2); ?></strong></td>
 </tr>
 <tr>
    <td colspan="7">
       <strong>Efectivo: $<?php 
       $t = $obj->gePaymmentMethodTotals($obj->postVars('start'), $obj->postVars('end'), $obj->getVars('id_store'),0);
       echo number_format($t[0],2); ?></strong>
    </td>
 </tr>
 <?php
    $row = $obj->getPaymentMethods();
    if(count($row)>0)
    {
        foreach($row as $row)
        {
            ?>
            <tr>
                <td colspan="7">
                   <strong><?php echo $row['pospm_name']?>: $<?php 
                   $t = $obj->gePaymmentMethodTotals($obj->postVars('start'), $obj->postVars('end'), $obj->getVars('id_store'),$row['pospm_id']);
                   echo number_format($t[0],2); ?></strong>
                </td>
             </tr>
            <?php
        }
    }
 ?>
</table>
</table>
<script>
 onload=function(){
  window.print() ;
 }
</script>

