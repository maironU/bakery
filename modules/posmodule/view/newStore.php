<?php
 include('modules/posmodule/view/include.php');
 
 $msg=false;
 
 if($_POST)
 {
     $obj->newStore();
     $msg = true;
 }

?>
<div class="widget3">
 <div class="widgetlegend">Nueva Tienda </div>
 <?php
  if($msg)
  {
  ?>
   <div class="ui-widget">
	<div class="ui-state-highlight ui-corner-all" style="margin-top: 20px; padding: 0 .7em;">
		<p><span class="ui-icon ui-icon-info" style="float: left; margin-right: .3em;"></span>
		<strong>Felicitaciones!</strong> Se ha guardado satisfactoriamente.</p>
	</div>
</div>
  <?php
  }
 ?>
<p style="width:100">
	<a href="<?php $_SERVER['PHP_SELF'];?>?p=modules/posmodule/view/showStores.php" class="btn_normal" style="float:left; margin:5px;">Volver </a>
</p>
 
<form action="#" method="post" enctype="multipart/form-data" name="form1" id="form1">
<table width="803" height="48" border="0" style="float:left">
  <tr>
    <td><label>Nombre: </label><br />
      <input name="name" type="text" id="name" required />   </td>
  </tr>
  <tr>
    <td><label>Direccion: </label><br />
      <input name="addr" type="text" id="addr" required />   </td>
  </tr>
  <tr>
    <td><label>Telefono: </label><br />
      <input name="phone" type="text" id="phone" required />   </td>
  </tr>
  <tr>
    <td><label>Encabezado: </label><br />
      <textarea name="header" cols="40" rows="6"></textarea>  </td>
  </tr>
  <tr>
    <td><label>Pie de pagina: </label><br />
      <textarea name="footer" cols="40" rows="6"></textarea>  </td>
  </tr>
  <tr>
    <td><label>Bodega asociada: </label><br />
      <select name="id_wh" required>
          <option value="">--Seleccionar--</option>
          <?php
            $row = $caps->getWarehouse();
            if(count($row)>0)
            {
                foreach($row as $row)
                {
                    ?>
                    <option value="<?php echo $row['iw_id']?>"><?php echo $row['iw_name']?></option>
                    <?php
                }
            }
          ?>
      </select>
      </td>
  </tr>
  <tr>
    <td><input type="submit" class="btn_submit" value="Guardar" /></td>
  </tr>
</table>
</form>


</div>
