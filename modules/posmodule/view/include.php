<?php
    include('modules/posmodule/model/posmodule.php');
    include('modules/inventory/model/inventory.php');
    include('modules/products/model/products.php');
    include('modules/crm/model/customerModel.php');
    include('usr/model/User.php');
     
    $obj = new posmodule();
    $obj->connect();
     
    $caps=new inventory();
    $caps->connect();
     
    $products=new products();
    $products->connect();
    
    $usr = new User();
    $usr->connect();
    
    $customer = new customerModelGe();
    $customer->connect();
?>