<?php
    include('include_self.php');
	
		
    $cupon=$obj->postVars('cupon');	
    $total=$obj->postVars('total');

    $objCoupon = $obj->getCouponWoocomerce($cupon);
    $objCoupon = json_decode($objCoupon);
    if(count($objCoupon) > 0){
        if($obj->cuponIsAvailable($objCoupon) == 1){   
            if($obj->hasCoupon($objCoupon[0]->id) == 0){
                $val = 0;
                $detail = "";
                $type = null;
                $id_coupon_woocomerce = null;
                $cant_percent = null;

                if($objCoupon[0]->discount_type == "percent"){
                    $discount = $objCoupon[0]->amount/100;
                    $val = $total * $discount * (-1);
                    $detail = "descuento cupon ".$objCoupon[0]->amount."%";
                    $type = "percent";
                    $id_coupon_woocomerce = $objCoupon[0]->id;
                    $cant_percent = $objCoupon[0]->amount;
                }else{
                    $val = $objCoupon[0]->amount * (-1);
                    $detail = "descuento cupon ".$cupon;
                    $type = "valor";
                    $id_coupon_woocomerce = $objCoupon[0]->id;
                }
                
                $obj->newposDetailTemp(0,$detail,$val,1,0, $type, $id_coupon_woocomerce, $cant_percent);
        
                $carrito=$obj->getPosDetailTemp();
                $html.='
                <div class="w-90" style="border-bottom: 1px solid #ddd !important;padding: 8px; margin: 0 auto">
                    <input type="text" class="border-0 w-90" id="add-product" placeholder="AGREGAR PRODUCTO">
                    <img src="images/icons/search.png" width="20px" height="21px" alt="" onclick="addProductByReference()">
                </div> 
                    <table class="w-100">
                        <thead>
                            <tr>
                                <th>CANTIDAD</th>
                                <th>PRODUCTO</th>
                                <th>PRECIO</th>
                                <th>TAX</th>
                                <th>TOTAL</th>
                            </tr>
                        </thead>
                    ';
                    
                    if(count($carrito)>0){
                        foreach($carrito as $product){
                            $html.="
                            <tr>
                            <td>
                                <input type='text' name='qty' value='".$product['posdt_qty']."' onChange='editQtyItem(".$product["posdt_id"].", this.value, 2)' class='border-0 w-25' style='font-size: 15px; height: 20px'>
                                <img src='images/icons/more.png' alt='' width='22px' class='m-0' onClick='editQtyItem(".$product['posdt_id'].", ".$product['posdt_qty'].", 1)'>
                                <img src='images/icons/less.png' alt='' width='22px' class='m-0' onClick='editQtyItem(".$product['posdt_id'].", ".$product['posdt_qty'].", 0)'> 
                            </td>
                        <td>".utf8_encode($product["posdt_productname"])."</td>";
                        if($products->isVariablePrice($product['id_product']))
                                                {
                                                    $html.="<td><input type='text' name='price' value='".$product["posdt_price"]."' onChange='editPriceItem(".$product["posdt_id"].", this.value)'/></td>";
                                                }
                                                else
                                                {
                                                    $html.="<td>$".number_format($product["posdt_price"],2)."</td>";
                                                }
                        $html.="<td>$".number_format($product["posdt_tax"]*$product["posdt_qty"],2)."</td>
                        <td>$".number_format(($product["posdt_price"]+$product["posdt_tax"])*$product["posdt_qty"],2)."</td>
                        <td>
                            <img src='images/icons/eliminar.png' alt='Eliminar' width= '20px' class='pointer' onClick='delPosItem(".$product["posdt_id"].")'>
                        </td>
                    </tr>
                    ";
                    }
                }
                
                $html.="</table>";
                echo json_encode(["success"=> true, "data" => $html, "message" => ""]);
            }else{
                echo json_encode(["success"=> false, "message" => "El Cupon ".$cupon." ya está en el carrito"]);
            }      
            
        }else{
            echo json_encode(["success"=> false, "message" => "El Cupon ".$cupon." ha expirado"]);
        }
        
    }else{
        echo json_encode(["success"=> false, "message" => "El Cupon ".$cupon." no existe"]);
    }
?>
