<?php
 include('modules/posmodule/view/include.php');
 
 $msg=false;
 
 if($_POST)
 {
     $obj->editPaymentMethod();
     $msg = true;
 }
 
 $id = $obj->getVars('id');
 
 $row = $obj->getPaymentMethodsById($id);
 foreach($row as $row)
 {
     $name = $row['pospm_name'];
     $img = $row['pospm_image'];
 }

?>
<div class="widget3">
 <div class="widgetlegend">Editar metodo de pago </div>
 <?php
  if($msg)
  {
  ?>
   <div class="ui-widget">
	<div class="ui-state-highlight ui-corner-all" style="margin-top: 20px; padding: 0 .7em;">
		<p><span class="ui-icon ui-icon-info" style="float: left; margin-right: .3em;"></span>
		<strong>Felicitaciones!</strong> Se ha guardado satisfactoriamente.</p>
	</div>
</div>
  <?php
  }
 ?>
<p style="width:100">
	<a href="<?php $_SERVER['PHP_SELF'];?>?p=modules/posmodule/view/showPaymentmethods.php" class="btn_normal" style="float:left; margin:5px;">Volver </a>
</p>
 
<form action="#" method="post" enctype="multipart/form-data" name="form1" id="form1">
<table width="803" height="48" border="0" style="float:left">
  <tr>
    <td><label>Nombre: </label><br />
      <input name="name" type="text" id="name" value="<?php echo $name?>" required />   </td>
  </tr>
  <tr>
    <td><label>Imagen: </label><br />
      <input name="img" type="file" id="img" /><br>
      <img src="modules/posmodule/imagesPm/<?php echo $img?>" />
      </td>
  </tr>
  <tr>
    <td>
        <input name="id" type="hidden" id="id" value="<?php echo $id?>"/>
        <input type="submit" class="btn_submit" value="Guardar" /></td>
  </tr>
</table>
</form>


</div>
