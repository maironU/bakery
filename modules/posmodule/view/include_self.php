<?php
    session_start();
    
    include('../../../core/config.php');
    include('../../../core/ge.php');
	
	//Models
	include('../../crm/model/customerModel.php');
	include('../model/posmodule.php');
	include('../../inventory/model/inventory.php');
 	include('../../products/model/products.php');
 	include('../../../usr/model/User.php');
	
	//Model Objects
 	$customer = new customerModelGe();
 	$customer->connect();
 	
 	//Model Objects
 	$obj = new posmodule();
 	$obj->connect();
	
	$caps=new inventory();
 	$caps->connect();
 
 	$products=new products();
 	$products->connect();
 	
 	$usr = new User();
 	$usr->connect();
?>