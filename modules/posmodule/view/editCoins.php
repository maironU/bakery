<?php
 include('modules/posmodule/view/include.php');
 
 $msg=false;
 
 if($_POST)
 {
     $obj->editCoins();
     $msg = true;
 }
 
 $id = $obj->getVars('id');
 
 $row = $obj->getCoinsById($id);
 foreach($row as $row)
 {
     $name = $row['posc_name'];
     $value = $row['posc_value'];
     $img = $row['posc_image'];
 }

?>
<div class="widget3">
 <div class="widgetlegend">Editar Moneda </div>
 <?php
  if($msg)
  {
  ?>
   <div class="ui-widget">
	<div class="ui-state-highlight ui-corner-all" style="margin-top: 20px; padding: 0 .7em;">
		<p><span class="ui-icon ui-icon-info" style="float: left; margin-right: .3em;"></span>
		<strong>Felicitaciones!</strong> Se ha guardado satisfactoriamente.</p>
	</div>
</div>
  <?php
  }
 ?>
<p style="width:100">
	<a href="<?php $_SERVER['PHP_SELF'];?>?p=modules/posmodule/view/showCoins.php" class="btn_normal" style="float:left; margin:5px;">Volver </a>
</p>
 
<form action="#" method="post" enctype="multipart/form-data" name="form1" id="form1">
<table width="803" height="48" border="0" style="float:left">
  <tr>
    <td><label>Nombre: </label><br />
      <input name="name" type="text" id="name" value="<?php echo $name?>" required />   </td>
  </tr>
  <tr>
    <td><label>Valor: </label><br />
      <input name="value" type="text" id="value" value="<?php echo $value?>" required />   </td>
  </tr>
  <tr>
    <td><label>Imagen: </label><br />
      <input name="img" type="file" id="img" /><br>
      <img src="modules/posmodule/imagesCoins/<?php echo $img?>" />
      </td>
  </tr>
  <tr>
    <td>
        <input name="id" type="hidden" id="id" value="<?php echo $id?>"/>
        <input type="submit" class="btn_submit" value="Guardar" /></td>
  </tr>
</table>
</form>


</div>
