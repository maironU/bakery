<?php
 include('modules/posmodule/view/include.php');
 
 
 $msg=false;
 
 //Acciones
 if($_POST)
 {
   $obj->newUserStores();
   $msg=true;
 }
  
  if($obj->getVars('actionDel')=='true')
  {
      $obj->delUserStores();
  }
  
  $id = $obj->getVars('id');
 
 ?>
 <div class="widget3">
 <div class="widgetlegend">Asignar usuarios a tiendas </div>
 <?php
  if($msg)
  {
  ?>
   <div class="ui-widget">
	<div class="ui-state-highlight ui-corner-all" style="margin-top: 20px; padding: 0 .7em;">
		<p><span class="ui-icon ui-icon-info" style="float: left; margin-right: .3em;"></span>
		<strong>Felicitaciones!</strong> guardado satisfactoriamente.</p>
	</div>
</div>
  <?php
  }
 ?>
<p style="width:100">
	<a href="<?php $_SERVER['PHP_SELF'];?>?p=modules/posmodule/view/showStores.php" class="btn_normal" style="float:left; margin:5px;">Volver </a>
</p><br /><br /><br />
 <form action="" method="post" name="form1">
<table width="803" height="48" border="0">
  <tr>
    <th width="30">ID</th>
    <th width="30">Nombre</th>
    <th width="142">Usuario</th>
    <th width="142">Perfil</th>
    <th width="142">Asignar</th>
  </tr>
  <?php
   $i=1;
   $row=$usr->getUsers();
   if(count($row)>0)
   {
 	foreach($row as $row)
 	{
      ?>
      <tr>
        <td><?php echo $row["u_id"];?></td>
        <td><?php echo $row["u_nom"];?></td>
        <td><?php echo $row["u_user"];?></td>
        <td><?php $usr->getProfileById($row["up_id"]);
    	 echo $usr->profile;
    	?></td>
        <td>
            <?php
                if(!$obj->isInStore($id, $row['u_id']))
                {
                    ?>
                    <input type="checkbox" name="usrs[]" value="<?php echo $row['u_id']?>" />
                    <?php
                }
                else
                {
                    $row1 = $obj->getUserInStore($id, $row['u_id']);
                    if(count($row1)>0)
                    {
                        foreach($row1 as $row1)
                        {
                            $id_posus = $row1['posus_id'];
                        }
                    }
                    ?>
                    <a href="<?php $_SERVER['PHP_SELF'];?>?p=modules/posmodule/view/showUserStore.php&id_us=<?php echo $id_posus?>&id=<?php echo $id?>&actionDel=true" class="btn_borrar">Eliminar</a>
                    <?php
                }
            ?>
        </td>
      </tr>
  <?php
   }
  } 
  ?>
  <tr>
   <td colspan="5">
       <input type="hidden" name="id" value="<?php echo $id?>" />
       <div align="left"><input type="submit" value="Guardar" class="btn_submit" /></div></td>
  </tr>
</table>
</form>

</div>
