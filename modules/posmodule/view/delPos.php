<?php
  include('include_self.php');
  
  $msg = false;
  $msg1 = false;
  $msg2 = false;

  
  if($_POST)
  {
      $user = $obj->postVars('user');
      $pass = $obj->postVars('pass');
      $id = $obj->postVars('id');
      $desc = trim($obj->postVars('desc'));
      $value = $obj->postVars('value');
      
      $id_user=$obj->getUserId($user,$pass);
      
      if($id_user != '')
      {
          if($obj->letAuth($id_user))
          {
              $obj->editPosActive($id, 0);
              $obj->newAuthLog($id_user,$desc,$value,$obs);
              $msg=true;
          }
          else
          {
              $msg2=true;
          }
      }
      else
      {
          $msg1=true;
      }
      
      
  }
  
  $id = $obj->getVars('id');
  $row = $obj->getPosById($id);
  foreach($row as $row)
  {
      $track = $row['pos_track'];
      $value = $row['pos_total'];
  }
?>
<link href="../../../css/scsStyle.css" rel="stylesheet" type="text/css" />
<link href="../../../css/smoothness/jquery-ui-1.9.2.custom.css" rel="stylesheet">
<link href="../../../css/smoothness/jquery-ui-1.9.2.custom.min.css" rel="stylesheet" type="text/css" />
<div class="widget3">
 <div class="widgetlegend">Autorizacion Anulacion </div>
 <?php
  if($msg)
  {
  ?>
   <div class="ui-widget">
	<div class="ui-state-highlight ui-corner-all" style="margin-top: 20px; padding: 0 .7em;">
		<p><span class="ui-icon ui-icon-info" style="float: left; margin-right: .3em;"></span>
		<strong>Felicitaciones!</strong> Se ha anulado satisfactoriamente.</p>
	</div>
</div>
  <?php
  }
 ?>
 <?php
  if($msg1)
  {
  ?>
   <div class="ui-widget">
	<div class="ui-state-highlight ui-corner-all" style="margin-top: 20px; padding: 0 .7em;">
		<p><span class="ui-icon ui-icon-error" style="float: left; margin-right: .3em;"></span>
		<strong>Error!</strong> Usuario no existe.</p>
	</div>
</div>
  <?php
  }
 ?>
 <?php
  if($msg2)
  {
  ?>
   <div class="ui-widget">
	<div class="ui-state-highlight ui-corner-all" style="margin-top: 20px; padding: 0 .7em;">
		<p><span class="ui-icon ui-icon-error" style="float: left; margin-right: .3em;"></span>
		<strong>Error!</strong> No esta autorizado.</p>
	</div>
</div>
  <?php
  }
 ?>
 <form action="" method="post" enctype="multipart/form-data" name="formadd">
     <table width="100%" border="0">
        <tr>
             <td><input type="text" name="user" id="user" placeholder="Usuario" required/></td>
        </tr>
        <tr>
             <td><input type="password" name="pass" id="pass" placeholder="Contrase&ntilde;a" required/></td>
        </tr>
        <tr>
            <td>
                 <textarea name="desc" cols="30" rows="6" required>
                    Anulacion recibo <?php echo $track;?>
                    Valor $<?php echo number_format($value,2);?>
                 </textarea>
            </td>
        </tr>
        <tr>
            <td>
                <input type="hidden" name="id" value="<?php echo $id?>" />
                <input type="hidden" name="value" value="<?php echo $value?>" />
                <input type="submit" value="Anular" class="btn_submit" />
            </td>
        </tr>
    </table>
 </form> 
</div>