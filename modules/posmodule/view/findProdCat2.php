<?php
    include('include_self.php');
	
		
	$id_cat=$obj->postVars('id_cat');
	if(isset($_POST['mode_list'])){
		$mode_list=$obj->postVars('mode_list');
		$_SESSION['mode_list']=$mode_list;
	}else{
		$mode_list=$_SESSION['mode_list'];
	}
	$productos = $products->getProductsByCategory($id_cat);
	if(count($productos)>0)
	{
		if($mode_list == 1){
			$html.="
				<table class='w-100' id='table-products-mode-list'>
					<thead>
						<tr>
							<th>ID</th>
							<th>NOMBRE</th>
							<th>PRECIO</th>
						</tr>
					</thead>
					<tbody>
				";
			foreach($productos as $product){
				$html.="
					<tr onClick='findPosList2(\"".$product['pro_reference']."\", 1)' style='cursor:pointer'>
						<td>".utf8_encode($product["pro_id"])."</td>
						<td>".utf8_encode($product["pro_name"])."</td>
						<td>".utf8_encode($product["pro_price"])."</td>
					</tr>
				";
			}
			$html.="</table>";
		}else{
			foreach($productos as $product)
			{
				$html.="
					<div class='col-6 col-sm-4 col-lg-4 col-xl-3 m-0 card__product' onClick='findPosList2(\"".$product['pro_reference']."\", 1)'>
						<img width='90%' src='modules/products/imagesProd/".$product['pro_image']."' alt=''>
						<span><b>".utf8_encode($product["pro_name"])."</b></span>
						<span>".$product['pro_price']."</span>
					</div>
				";
			}
		}
	}
	else
	{
	    $html.="
			<h3 class='text-danger'>
				No hay productos
			</h3>
		";
	}
	
	echo $html;
?>
