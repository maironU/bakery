<?php
  include('include_self.php');
  
  $f = false;
  
  if($_POST)
  {
      $obj->newOpenBox();
      $f = true;
  }
  
  
  $id = $obj->getVars('id');
?>
<link href="../../../css/scsStyle.css" rel="stylesheet" type="text/css" />

<?php
    if(!$f)
    {
?>
<div class="widget3">
 <div class="widgetlegend">Registro apertura de caja</div>
 <form action="" method="post" enctype="multipart/form-data" name="formadd">
    <table border="0">
         <tr>
            <td>
                Indique el motivo de apertura de la caja
            </td>
        </tr>
        <tr>
            <td>
                <textarea name="desc" id="desc" cols="40" rows="6" required></textarea>
            </td>
        </tr>
        <tr>
            <td>
                <input name="id_user" type="hidden" id="id_user" value="<?php echo $id?>" />
                <input type="submit" value="Guardar" class="btn_submit" />
            </td>
        </tr>
    </table>
 </form>
</div>
<?php
    }
    else
    {
        ?>
        <table>
            <tr>
                <td><h2>Caja Abierta</h2></td>
            </tr>
            <tr>
                <td><?php echo $obj->postVars('desc')?></td>
            </tr>
            <tr>
                <td><?php echo date('Y-m-d H:i:s')?></td>
            </tr>
            <tr>
                <td>
                    <?php
                        $usr->getUserById($obj->postVars('id_user'));
                        echo $usr->name;
                    ?>
                </td>
            </tr>
        </table>
        <script>
            window.print();
        </script>
        <?php
    }
?>