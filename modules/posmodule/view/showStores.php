<?php
 include('modules/posmodule/view/include.php');
 
 
 $msg=false;
 
 //Acciones
 if($obj->getVars('actionDel')=='true')
 {
     $obj->delStore();
     $msg=true;
 }
 
 ?>
 <div class="widget3">
 <div class="widgetlegend">Tiendas </div>
 <?php
  if($msg)
  {
  ?>
   <div class="ui-widget">
	<div class="ui-state-highlight ui-corner-all" style="margin-top: 20px; padding: 0 .7em;">
		<p><span class="ui-icon ui-icon-info" style="float: left; margin-right: .3em;"></span>
		<strong>Felicitaciones!</strong> se ha eliminado satisfactoriamente.</p>
	</div>
</div>
  <?php
  }
 ?>
<p style="width:100">
	<a href="<?php $_SERVER['PHP_SELF'];?>?p=modules/posmodule/view/newStore.php" class="btn_normal" style="float:left; margin:5px;">Nuevo </a>
</p><br /><br /><br />
<table width="100%" height="48" border="0">
  <tr>
    <th>ID</th>
    <th>Imagen</th>
    <th>Nombre</th>
    <th>Direccion</th>
    <th>Telefono</th>
    <th>Bodega</th>
    <th colspan="3">&nbsp;</th>
  </tr>
  <?php
   $row=$obj->getStore();
   if(count($row)>0)
   {
       foreach($row as $row)
       {
           ?>
            <tr>
                <td><?php echo $row['poss_id']?></td>
                <td><i class="fa fa-building"></i></td>
                <td><?php echo $row['poss_name']?></td>
                <td><?php echo $row['poss_addr']?></td>
                <td><?php echo $row['poss_phone']?></td>
                <td><?php
                    $row1 = $caps->getWarehouseById($row['id_warehouse']);
                    if(count($row1)>0)
                    {
                        foreach($row1 as $row1)
                        {
                            echo $row1['iw_name'];
                        }
                    }
                    ?>
                </td>
                <td width="86"><a href="<?php $_SERVER['PHP_SELF'];?>?p=modules/posmodule/view/editStore.php&id=<?php echo $row["poss_id"]?>" class="btn_normal">Editar</a></td>
                <td width="86"><a href="<?php $_SERVER['PHP_SELF'];?>?p=modules/posmodule/view/showUserStore.php&id=<?php echo $row["poss_id"]?>" class="btn_normal">Usuarios</a></td>
                <td width="86"><a href="<?php $_SERVER['PHP_SELF'];?>?p=modules/posmodule/view/showStores.php&id=<?php echo $row["poss_id"]?>&actionDel=true" class="btn_borrar">Eliminar</a></td>
              </tr>
           <?php
       }
   } 
  ?>
</table>

</div>
