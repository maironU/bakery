<?php
    if (!isset($output))  $output   = "png";
		if (!isset($type))    $type     = "C128B";
		if (!isset($widthb))   $widthb    = "300";
		if (!isset($heightb))  $heightb   = "100";
		if (!isset($xres))    $xres     = "2";
		if (!isset($font))    $font     = "2";
		
		$drawtext="off";
		$stretchtext="off";
		
		$style |= ($output  == "png" ) ? BCS_IMAGE_PNG  : 0;
		$style |= ($output  == "jpeg") ? BCS_IMAGE_JPEG : 0;
		$style |= ($border  == "on"  ) ? BCS_BORDER           : 0;
		$style |= ($drawtext== "on"  ) ? BCS_DRAW_TEXT  : 0;
		$style |= ($stretchtext== "on" ) ? BCS_STRETCH_TEXT  : 0;
		$style |= ($negative== "on"  ) ? BCS_REVERSE_COLOR  : 0;
?>