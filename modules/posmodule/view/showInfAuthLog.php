<?php
    include('modules/posmodule/view/include.php');
    
 $msg=false;
 
 //Acciones
 
 ?>
<div class="widget3">
 <div class="widgetlegend">Informe de autorizaciones </div>
 <?php
  if($msg)
  {
  ?>
   <div class="ui-widget">
	<div class="ui-state-highlight ui-corner-all" style="margin-top: 20px; padding: 0 .7em;">
		<p><span class="ui-icon ui-icon-info" style="float: left; margin-right: .3em;"></span>
		<strong>Felicitaciones!</strong> eliminado satisfactoriamente.</p>
	</div>
</div>
  <?php
  }
 ?>
<p style="width:100">
	<a href="<?php $_SERVER['PHP_SELF'];?>?p=modules/posmodule/view/index.php" class="btn_normal" style="float:left; margin:5px;">Volver </a>
    <a href="modules/posmodule/view/showInfAthLogPrint.php?start=<?php echo $obj->postVars('start')?>&end=<?php echo $obj->postVars('end')?>" class="btn_normal" style="float:left; margin:5px;" target="_blank">Imprimir </a>
</p>
 <br /><br /><br />

<form action="" method="post">
<table width="803" height="48" border="0">
 <tr>
  <td width="95"><strong>Desde:</strong></td>
  <td width="222"><label for="date"></label>
    <input type="text" name="start" id="start" class="datepicker" /></td>
  <td width="222"><strong>Hasta:</strong></td>
  <td width="446"><input type="text" name="end" id="end" class="datepicker2" /></td>
  <td width="248"><input type="submit" name="button" id="button" value="Buscar" /></td>
 </tr>
</table>
</form>
<br /><br /><br />
<table width="807" height="48" border="0">
  <tr>
    <th width="30">ID</th>
    <th width="30">Fecha</th>
    <th width="30">Usuario</th>
    <th width="142">Descripcion</th>
    <th width="142">Valor</th>
    <th width="142">Observaciones</th>
  </tr>
<?php
 if($_POST){
  $row=$obj->getAuthLog($obj->postVars('start'),$obj->postVars('end'));
  if(count($row)>0){
  foreach($row as $row){
   ?>
   <tr>
    <td><?php echo $row["posl_id"]?></td>
    <td><?php echo $row["posl_date"]?></td>
    <td><?php 
	 $usr->getUserById($row["u_id"]);
	 echo $usr->name;
	?></td>
    <td><?php echo $row["posl_desc"]?></td>
    <td>$<?php echo number_format($row["posl_value"])?></td>
    <td><?php echo $row["posl_obs"]?></td>
   </tr>
   <?php
   $total+=$row["posl_value"];
  }
  }
  else{
   ?>
    <tr>
     <td colspan="5">No hay autorizaciones</td>
    </tr>
   <?php
  }
 }
?> 
<tr>
   <td colspan="5"><strong>Total : $<?php echo number_format($total); ?></strong></td>
 </tr>
</table>

</div>


