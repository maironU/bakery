<?php
    include('include_self.php');
	
		
 	$posdt_id=$obj->postVars('id');
	$row=$obj->getPosDetailTempById($posdt_id);
  	foreach($row as $row)
  	{
  	    $value=$row["posdt_price"] * $row["posdt_qty"];
  	    $id_product=$row["id_product"];
  	    $qty=$row["posdt_qty"];
  	}
  	
  	$obj->delposDetailTemp($posdt_id);
  	$obj->newAuthLog($id_user,"Eliminacion de Item punto de venta",$value,$desc);
	$obj->updateCouponsIfHas();
	
	$carrito=$obj->getPosDetailTemp();
	
	/*$html.='
	 <table border="0" width="100%">
	  <tr>
	   <th>Cantidad</th>
	  	<th>Producto</th>
		<th>Precio</th>
		<th>Tax</th>
		<th>Total</th>
		<th>&nbsp;</th>
	  </tr>
	';
	
	if(count($row)>0){
	 foreach($row as $row){
	  $html.="
	   <tr>
	    <td><table>
                    		            <tr>
                    		                <td><a href='javascript:;' onClick='editQtyItem(".$row["posdt_id"].", ".$row["posdt_qty"].", 1)'><i class='fa fa-plus-circle' style='font-size:24px'></i></a></td>
                    		            </tr>
                    		            <tr>
                    		                <td><input type='text' name='qty' value='".$row["posdt_qty"]."' onChange='editQtyItem(".$row["posdt_id"].", this.value, 2)' /></td>
                    		            </tr>
                    		            <tr>
                    		                <td><a href='javascript:;' onClick='editQtyItem(".$row["posdt_id"].", ".$row["posdt_qty"].", 0)'><i class='fa fa-minus-circle' style='font-size:24px'></i></a></td>
                    		            </tr>
                    		        </table></td>
	    <td>".utf8_encode($row["posdt_productname"])."</td>";
		if($products->isVariablePrice($row['id_product']))
                    			{
                    			    $html.="<td><input type='text' name='price' value='".$row["posdt_price"]."' onChange='editPriceItem(".$row["posdt_id"].", this.value)'/></td>";
                    			}
                    			else
                    			{
                    			    $html.="<td>$".number_format($row["posdt_price"],2)."</td>";
                    			}
		$html.="<td>$".number_format($row["posdt_tax"]*$row["posdt_qty"],2)."</td>
		<td>$".number_format(($row["posdt_price"]+$row["posdt_tax"])*$row["posdt_qty"],2)."</td>
		<td>
		 <a href='javascript:;' onClick='delPosItem(".$row["posdt_id"].")' class='btn_2'>Eliminar</a>
		</td>
	   </tr>
	  ";
	 }
	}
	
	$html.="</table>";*/
	$html.='
	<div class="w-90" style="border-bottom: 1px solid #ddd !important;padding: 8px; margin: 0 auto">
		<input type="text" class="border-0 w-90" id="add-product" placeholder="AGREGAR PRODUCTO">
		<img src="images/icons/search.png" width="20px" height="21px" alt="" onclick="addProductByReference()">
	</div>  
	 <table class="w-100" id="table-carrito">
		<thead>
			<tr>
				<th>CANTIDAD</th>
				<th>PRODUCTO</th>
				<th>PRECIO</th>
				<th>TAX</th>
				<th>TOTAL</th>
			</tr>
		</thead>
	';
	
	if(count($carrito)>0){
		foreach($carrito as $product){
			$html.="
			<tr>
			<td>
				<input type='text' name='qty' value='".$product['posdt_qty']."' onChange='editQtyItem(".$product["posdt_id"].", this.value, 2)' class='border-0 w-25' style='font-size: 15px; height: 20px'>
				<img src='images/icons/more.png' alt='' width='22px' class='m-0' onClick='editQtyItem(".$product['posdt_id'].", ".$product['posdt_qty'].", 1)'>
				<img src='images/icons/less.png' alt='' width='22px' class='m-0' onClick='editQtyItem(".$product['posdt_id'].", ".$product['posdt_qty'].", 0)'> 
			</td>
	    <td>".utf8_encode($product["posdt_productname"])."</td>";
		if($products->isVariablePrice($product['id_product']))
                    			{
                    			    $html.="<td><input type='text' name='price' value='".$product["posdt_price"]."' onChange='editPriceItem(".$product["posdt_id"].", this.value)'/></td>";
                    			}
                    			else
                    			{
                    			    $html.="<td>$".number_format($product["posdt_price"],2)."</td>";
                    			}
		$html.="<td>$".number_format($product["posdt_tax"]*$product["posdt_qty"],2)."</td>
		<td>$".number_format(($product["posdt_price"]+$product["posdt_tax"])*$product["posdt_qty"],2)."</td>
		<td>
			<img src='images/icons/eliminar.png' alt='Eliminar' width= '20px' class='pointer' onClick='delPosItem(".$product["posdt_id"].")'>
		</td>
	   </tr>
	  ";
	 }
	}
	
	$html.="</table>";
	echo $html;
?>
