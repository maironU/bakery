<?php
    include('include_self.php');
	
		
 	$name=$obj->postVars('name');
	if(isset($_POST['mode_list'])){
		$mode_list=$obj->postVars('mode_list');
		$_SESSION['mode_list']=$mode_list;
	}else{
		$mode_list=$_SESSION['mode_list'];
	}
	
	$html.='<table border="0" width="100%">';
	$productos = $products->findProductByNameReferenceOrDescription($name);
	
	/*if(count($row)>0)
	{
	    foreach($row as $row)
	    {
    	    $html.="
        	   <tr>
        	    <td>".utf8_encode($row["pro_name"])."</td>
        		<td><a href='javascript:;' onClick='findPosList2(&quot;".$row['pro_reference']."&quot;, 1)' class='btn_1'>Agregar</a></td>
        	   </tr>
    	    ";
	    }
	}
	else
	{
	    $html.="
        	   <tr>
        	    <td>No hay productos</td>
        	   </tr>
    	    ";
	}*/
	
	if(count($productos)>0){
		if($mode_list == 1){
			$html.="
				<table class='w-100' id='table-products-mode-list'>
					<thead>
						<tr>
							<th>ID</th>
							<th>NOMBRE</th>
							<th>PRECIO</th>
						</tr>
					</thead>
					<tbody>
				";
			foreach($productos as $product){
				$html.="
					<tr onClick='findPosList2(\"".$product['pro_reference']."\", 1)' style='cursor:pointer'>
						<td>".utf8_encode($product["pro_id"])."</td>
						<td>".utf8_encode($product["pro_name"])."</td>
						<td>".utf8_encode($product["pro_price"])."</td>
					</tr>
				";
			}
			$html.="</table>";
		}else{
			foreach($productos as $product)
			{
				$html.="
					<div class='col-6 col-sm-4 col-lg-4 col-xl-3 m-0 card__product' onClick='findPosList2(\"".$product['pro_reference']."\", 1)'>
						<img width='90%' src='modules/products/imagesProd/".$product['pro_image']."' alt=''>
						<span><b>".utf8_encode($product["pro_name"])."</b></span>
						<span>".$product['pro_price']."</span>
					</div>
				";
			}
		}	
	}
	else
	{
	    $html.="
			   <span class='text-danger'>
			  	 	No se encontraron coincidencias
			   </span>
    	    ";
	}

	//$html.="</table>";
	echo $html;
?>
