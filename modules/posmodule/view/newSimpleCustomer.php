<?php
    include('include_self.php');
	
	if($_POST){
		$nit=$customer->postVars('nit')."-".$customer->postVars('nit2');
		if(!$customer->checkCustomerExists($nit)){
			$id=$customer->newCustomer();
			$customer->newCustomerContact($id);
			
			$res="ok";
				
		}else{
			$res="nok";
		}
		
		$msg=true;
	}
		
?>
<link href="../../../css/scsStyle.css" rel="stylesheet" type="text/css" />
<link href="../../../css/smoothness/jquery-ui-1.9.2.custom.css" rel="stylesheet">
<link href="../../../css/smoothness/jquery-ui-1.9.2.custom.min.css" rel="stylesheet" type="text/css" />
<div class="widget3">
  <div class="widgetlegend">Nuevo Cliente</div>
  <?php
  if($msg && $res=='ok')
  {
  ?>
   <div class="ui-widget">
	<div class="ui-state-highlight ui-corner-all" style="margin-top: 20px; padding: 0 .7em;">
		<p><span class="ui-icon ui-icon-info" style="float: left; margin-right: .3em;"></span>
		<strong>Felicitaciones!</strong> Se ha sido creado satisfactoriamente. Ahora puede buscarlo</p>
	</div>
</div>
  <?php
  }
 ?>
 
 <?php
  if($msg && $res=='nok')
  {
  ?>
   <div class="ui-widget">
	<div class="ui-state-error ui-corner-all" style="margin-top: 20px; padding: 0 .7em;">
		<p><span class="ui-icon ui-icon-alert" style="float: left; margin-right: .3em;"></span>
		<strong>Error!</strong> el cliente ya existe</p>
	</div>
</div>
  <?php
  }
 ?>
  <form name="form1" method="post" action="#"><table width="452" border="0">
  <tr>
    <th width="446"><div align="left">Informacion de cliente </div></th>
    </tr>
  <tr>
    <td><table width="384" border="0" align="center">
      <tr>
        <td width="136">Nombre:</td>
        <td width="238"><label>
          <input name="nom" type="text" id="nom" onchange="document.form1.nomc.value=document.form1.nom.value" />
          </label></td>
        </tr>
      <tr>
        <td>Identificaci&oacute;n:</td>
        <td><input name="nit" type="text" id="nit" size="10" /> 
          - 
          <label>
            <input name="nit2" type="text" id="nit2" size="5" value="0" />
            </label></td>
        </tr>
    </table></td>
    </tr>
  <tr>
    <td><input name="action" type="hidden" id="action" value="NewCustomer" />
      <table width="384" border="0" align="center">
        <tr>
          <td width="136">Nombre:</td>
          <td width="238"><input name="nomc" type="text" id="nomc" /></td>
          </tr>
        <tr>
          <td>Email:</td>
          <td><input name="email" type="text" id="email" /></td>
          </tr>
        <tr>
          <td>Celular: </td>
          <td><input name="cel" type="text" id="cel" /></td>
          </tr>
      </table></td>
    </tr>
  <tr>
    <td><div align="right"><a href="#" class="btn_normal" onclick="document.form1.submit();">Guardar</a></div></td>
    </tr>
  </table>
  </form>
</div>
