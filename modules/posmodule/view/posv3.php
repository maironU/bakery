<?php
 include('modules/posmodule/view/include.php');
 
 
 $id = $obj->getVars('id');
 $_SESSION['id_store'] = $id;
 
?>
<script type="text/javascript" src="modules/posmodule/js/jsPosFunctions.js"></script>
<!-- Add mousewheel plugin (this is optional) -->
<script type="text/javascript" src="modules/posmodule/js/jquery.mousewheel-3.0.6.pack.js"></script>
<!-- Add fancyBox main JS and CSS files -->
<script type="text/javascript" src="modules/posmodule/js/jquery.fancybox.js?v=2.1.4"></script>
<link rel="stylesheet" type="text/css" href="modules/posmodule/css/jquery.fancybox.css?v=2.1.4" media="screen" />
<link rel="stylesheet" type="text/css" href="modules/posmodule/css/stylePos.css" />

<style>
    	.demo > ul{
			list-style: none outside none;
		    padding-left: 0;
            margin: 0;
            background-color: #ccc;
		}
        .demo .item{
            margin-bottom: 60px;
            background-color: #ccc;
            
        }
		.content-slider li{
		    background-color: #ccc;
		    text-align: center;
		    color: #000;
		    width: 800;
            height: 600px;
            overflow: auto;
            padding-left: 10px;
            
            
		}
		
		.content-slider li > a{
		    text-align: center;
		    color: #fff;
		    text-decoration: none;
		    display: block;
		    width: 150px;
		    height: 46px;
		    background-color: #f00;
		    float: left;
		    margin: 8px;
		    padding-top: 35px;
		    font-size: 12px;
		    
		}
		.content-slider h4 {
		    margin: 0;
		    padding: 10px 0;
		}
		.demo{
			width: 600px;
			//height: 200px;
			//display: inline;
		}
		
		/* slider actions */
        .lSAction > a {
            width: 32px;
            display: block;
            top: 50%;
            height: 32px;
            background-image: url('modules/posmodule/images/controls.png');
            cursor: pointer;
            position: absolute;
            z-index: 99;
            margin-top: -16px;
            opacity: 0.5;
            -webkit-transition: opacity 0.35s linear 0s;
            transition: opacity 0.35s linear 0s;
        }
        .lSAction > a:hover {
            opacity: 1;
        }
        .lSAction > .lSPrev {
            background-position: 0 0;
            left: 10px;
        }
        .lSAction > .lSNext {
            background-position: -32px 0;
            right: 10px;
        }
        .lSAction > a.disabled {
            pointer-events: none;
        }
        .cS-hidden {
            height: 1px;
            opacity: 0;
            filter: alpha(opacity=0);
            overflow: hidden;
        }
		
		
    </style>
    <script>
    	 $(document).ready(function() {
			$("#content-slider").lightSlider({
                loop:true,
                keyPress:true,
                enableTouch:true
            });
            $('#image-gallery').lightSlider({
                gallery:true,
                item:1,
                thumbItem:9,
                slideMargin: 0,
                speed:500,
                auto:true,
                loop:true,
                onSliderLoad: function() {
                    $('#image-gallery').removeClass('cS-hidden');
                }  
            });
		});
		$( function() {
            $( "#tabs" ).tabs();
          } );
    </script>

<div class="widget3">
 <div class="widgetlegend">Punto de venta Columbus Bakery </div>
<p style="width:100">
    <a href="<?php $_SERVER['PHP_SELF'];?>/colbakery/modules/posmodule/front/index.php?id_user=<?php echo $_SESSION['user_id']?>&id_store=<?php echo $_SESSION['id_store']?>" target="_blank" class="btn_normal" style="float:left; margin:5px;">Vista Cliente </a>
    <a href="javascript:;" class="btn_normal" style="float:left; margin:5px;" onClick="showInfo(<?php echo $_SESSION['user_id']?>, 'modules/posmodule/view/newOpenBox.php')">Abrir Caja</a>
</p>
 
<table style="min-height: 800px">
    <tr>
        <td valign="top">
            <table>
                <tr>
                    <td valign="top">
                        <div id="showposL">
                    	    <?php
                    		 $row=$obj->getPosDetailTemp();
                    		 $html.='
                    			 <table border="0" width="100%">
                    			  <tr>
                    				<th>Cantidad</th>
                    				<th>Producto</th>
                    				<th>Precio</th>
                    				<th>Tax</th>
                    				<th>Total</th>
                    				<th>&nbsp;</th>
                    			  </tr>
                    		  ';
                    		  
                    		if(count($row)>0){
                    		 foreach($row as $row){
                    		  $html.="
                    		   <tr>
                    		    <td>
                    		        <table>
                    		            <tr>
                    		                <td><a href='javascript:;' onClick='editQtyItem(".$row["posdt_id"].", ".$row["posdt_qty"].", 1)'><i class='fa fa-plus-circle' style='font-size:24px'></i></a></td>
                    		            </tr>
                    		            <tr>
                    		                <td><input type='text' name='qty' value='".$row["posdt_qty"]."' onChange='editQtyItem(".$row["posdt_id"].", this.value, 2)' /></td>
                    		            </tr>
                    		           <tr>
                    		                <td><a href='javascript:;' onClick='editQtyItem(".$row["posdt_id"].", ".$row["posdt_qty"].", 0)'><i class='fa fa-minus-circle' style='font-size:24px'></i></a></td>
                    		            </tr>
                    		        </table>
                    		        </td>
                    			<td>".($row["posdt_productname"])."</td>";
                    			
                    			if($products->isVariablePrice($row['id_product']))
                    			{
                    			    $html.="<td><input type='text' name='price' value='".$row["posdt_price"]."' onChange='editPriceItem(".$row["posdt_id"].", this.value)'/></td>";
                    			}
                    			else
                    			{
                    			    $html.="<td>$".number_format($row["posdt_price"],2)."</td>";
                    			}
                    			
                    			
                    			$html .= "<td>$".number_format($row["posdt_tax"]*$row["posdt_qty"],2)."</td>
                    			<td>$".number_format(($row["posdt_price"]+$row["posdt_tax"])*$row["posdt_qty"],2)."</td>
                    			<td>
                    			    <a href='javascript:;' onClick='delPosItem(".$row["posdt_id"].")' class='btn_2'>Eliminar</a>
                    			</td>
                    		   </tr>
                    		  ";
                    		 }
                    		}
                    	
                    		$html.="</table>";
                    		echo $html;
                    		?>
                    	   </div>
                    	   <div id="gauch2" style="display:none"><img src="modules/posmodule/images/loading.gif" /></div>
                    </td>
                </tr>
            </table>
        </td>
        <td valign="top">
            <div id="tabs">
                <ul>
                    <li><a href="#tabs-1">Categorias</a></li>
                    <li><a href="#tabs-2">Codigo de Barras</a></li>
                    <li><a href="#tabs-3">Buscar por nombre</a></li>
                </ul>
                <div id="tabs-1">
                    <p>
                        <div class="demo">
                            <div class="item">
                                <ul id="content-slider" class="content-slider">
                                <?php
                                    $row = $products->getCategoriesFiltered();
                                    if(count($row)>0)
                                    {
                                        foreach($row as $row)
                                        {
                                            ?>
                                                <li>
                                                    <h4><?php echo $row['cat_name']?></h4>
                                                    <?php
                                                        $row1 = $products->getProductsByCategory($row['cat_id']);
                                                        if(count($row1)>0)
                                                        {
                                                            foreach($row1 as $row1)
                                                            {
                                                                ?>
                                                                    <a href="javascript:;" onClick="findPosList2('<?php echo $row1['pro_reference']?>', 1)">
                                                                        <?php echo $row1['pro_name']?>
                                                                    </a>
                                                                    <?php
                                                            }
                                                        }
                                                    ?>
                                                </li>
                                            <?php
                                        }
                                    }
                                ?>
                                </ul>
                            </div>
                        </div>
                    </p>
            </div>
            <div id="tabs-2">
                <p>
         	        <form action="#" method="post">
         	            <table width="550">
         	                <tr>
         	                    <td><strong>Producto:</strong><input type="text" name="barcode" id="barcode" onchange="findPosList()" autofocus/></td>
         	                    <td><input type="hidden" name="qty" id="qty" value="1" />
         	                    <input type="button" value="Agregar" onclick="findPosList()" class="btn_submit" /></td>
                 	        </tr>
                 	        <tr>
                 	            <td colspan="2"><div id="listprod"></div></td>
                 	        </tr>
                 	  </table>
                 	</form>
         	    </p>
         	</div>
         	<div id="tabs-3">
         	    <p>
         	        <form action="#" method="post">
         	            <table width="550">
         	                <tr>
         	                    <td><strong>Buscar Producto:</strong><input type="text" name="prodname" id="prodname" /></td>
         	                    <td><input type="button" value="Buscar" onclick="findProductByName()" class="btn_submit" /></td>
                 	        </tr>
                 	        <tr>
                 	            <td colspan="2"><div id="listprod2"></div></td>
                 	        </tr>
                 	    </table>
                 	</form>
         	    </p>
         	</div>
         </div>
         	
        </td>
        <td valign="top">
            <?php
                $total=$obj->getPosTotal();
                if($total == 0)
                {
                    ?>
                    <form action="#" id="formpos1" name="formpos1" method="post" style="display:none">
                    <?php
                }
                else
                {
                    ?>
                    <form action="#" id="formpos1" name="formpos1" method="post" style="display:block">
                    <?php
                }
                
            ?>
            
                            <table>
                                <tr>
                                    <td>
                                        <h2>TOTAL: </h3>
                                        <?php $total=$obj->getPosTotal();?>
                                        <div id="totalpos"><h1>$<?php echo number_format($total,2)?></h1></div>
                                        <input name="total" id="total" type="hidden" value="<?php echo $total; ?>" />
    	                            </td>
                                </tr>
                                <tr>
                                    <td>
                                        <a href="javascript:;" id="btndesce"  class="btn_normal" onMouseOver="modal('desce', 'btndesce')">Descuento Empleados</a>
                                        <div id="desce" class="modal">
                                            <!-- Modal content -->
                                            <div class="modal-content">
                                                <div class="modal-header">
                                                    <span class="close">&times;</span>
                                                    <h4>Descuento para empleados</h4>
                                                </div>
                                                <div class="modal-body">
                                                    <table>
                                                        <tr>
                                                            <td>
                                                                Usuario que autoriza <br>
                                                                <input type="text" name='usr' id='usr' placeholder="Usuario" /><br>
                                                                <input type="password" name='pass' id='pass' placeholder="Contrase&ntilde;a" /><br>
                                                            </td>
                                                        </tr>
                                                        <tr>
                                                            <td>
                                                                Porcentaje de descuento a aplicar <br>
                                                                <input type="text" name='por' id='por' placeholder="(%) Porcentaje de descuento" /><br>
                                                            </td>
                                                        </tr>
                                                        <tr>
                                                            <td>
                                                                Descripcion <br>
                                                                <input type="text" name='desc' id='desc1' value="Descuento para empleado" /><br>
                                                            </td>
                                                        </tr>
                                                        <tr>
                                                            <td>
                                                                <input type="button" value="Registrar" onClick="addDiscount()" class="btn_submit" />
                                                            </td>
                                                        </tr>
                                                    </table>
                                                </div>
                                                <div class="modal-footer">
                                                </div>
                                            </div>
                                        </div>
                                    </td>
                                </tr>
        
                                <tr>
                                    <td>
                                        <input type="hidden" name="action" id="action" value="closepos" />
            	                       <input name="u_id" type="hidden" id="u_id" value="<?php echo $_SESSION["user_id"];?>" />
            	                       <a href="javascript:;" id="btnmsgx"  class="btn_normal" onMouseOver="modal('msgx', 'btnmsgx')">Efectivo</a>
            	                       
            	                       <div id="msgx" class="modal">
                                      <!-- Modal content -->
                                      <div class="modal-content">
                                         
                                        <div class="modal-header">
                                            <span class="close">&times;</span>
                                            <h4>Pagar</h4>
                                        </div>
                                        <div class="modal-body">
                                             <table>
                                                 <tr>
                                                     <td valign="top">
                                                         <div style="width: 800px">
                                                             <?php
                                                                $row = $obj->getCoins();
                                                                if(count($row)>0)
                                                                {
                                                                    foreach($row as $row)
                                                                    {
                                                                        ?>
                                                                        <span style="float:left; margin: 5px">
                                                                            <img src="modules/posmodule/imagesCoins/<?php echo $row['posc_image']?>" onClick="builtCash(<?php echo $row['posc_value']?>)"/>
                                                                        </span>
                                                                        <?php
                                                                    }
                                                                }
                                                             ?>
                                                         </div>
                                                     </td>
                                                     <td valign="top">
                                                          <h1>TOTAL</h1>
                                                          <?php $total=$obj->getPosTotal();?>
                                                          <div id="totalpos0"><h2>$<?php echo number_format($total,2)?></h2></div>
                                                          <h4>EFECTIVO $:</h4>
                                                          <input name="cash" id="cash" type="text" value="0" onchange="calcDelvol()" /><br>
                                                          <h4>CAMBIO $:</h4>
                                                          <input name="devol1" id="devol1" type="text" value="0" disabled="disabled" />
                        	                              <input name="devol" id="devol" type="hidden" value="0" /><br>
                        	                              <input name="txs" id="txs0" type="hidden" value="" /><br>
                        	                              <input type="button" value="Pagar" onClick="closePos(0)" class="btn_submit" />
                        	                              <br>
                        	                              <input type="button" value="Limpiar" onClick="clearCash()" class="btn_borrar" />
                                                     </td>
                                                 </tr>
                                             </table>
                                        </div>
                                        <div class="modal-footer">
                                        </div> 
                                        
                                      </div>
                                    </div>

    	                            </td>
                                </tr>
                                <tr>
                                    <td>
                                        <h2>Pagos con Tarjeta</h2>
                                        <?php
                                            $i = 1;
                                            $row = $obj->getPaymentMethods();
                                            if(count($row)>0)
                                            {
                                                foreach($row as $row)
                                                {
                                                    ?>
                                                    <a href="javascript:;" id="btnmsg<?php echo $row['pospm_id']?>"  class="btn_normal" onMouseOver="modal('msg<?php echo $row['pospm_id']?>', 'btnmsg<?php echo $row['pospm_id']?>')"><?php echo $row['pospm_name']?></a>
                                                    <div id="msg<?php echo $row['pospm_id']?>" class="modal">
                                      <!-- Modal content -->
                                      <div class="modal-content">
                                         
                                        <div class="modal-header">
                                            <span class="close">&times;</span>
                                            <h4><?php echo $row['pospm_name']?></h4>
                                        </div>
                                        <div class="modal-body">
                                             <table>
                                                 <tr>
                                                     <td valign="top">
                                                         <img src="modules/posmodule/imagesPm/<?php echo $row['pospm_image']?>" width="200" height="100" />
                                                     </td>
                                                     <td valign="top">
                                                          <h1>TOTAL</h1>
                                                          <?php $total=$obj->getPosTotal();?>
                                                          <div id="totalpos<?php echo $i?>"><h2>$<?php echo number_format($total,2)?></h2></div>
                                                          <!--<h4>NO.TRANSACCION:</h4> -->
                                                          <input name="txs" id="txs<?php echo $row['pospm_id']?>" type="hidden" value="" onChange="enableBtn(<?php echo $row['pospm_id']?>)"  /><br>
                        	                              <input type="button" value="Pagar" id="payment<?php echo $row['pospm_id']?>" onClick="closePos(<?php echo $row['pospm_id']?>)" class="btn_submit" style="display:block"/>
                                                     </td>
                                                 </tr>
                                             </table>
                                        </div>
                                        <div class="modal-footer">
                                        </div> 
                                        
                                      </div>
                                    </div><br>
                                                    <?php
                                                    $i++;
                                                }
                                            }
                                        ?>
                                        <input type="hidden" name="i" id="i" value="<?php echo $i?>" ?>
                                    </td>
                                </tr>
                            </table>
                        </form>
            
        </td>
    </tr>
</table>

</div>


