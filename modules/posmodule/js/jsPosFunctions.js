// JavaScript Document
var ok;

function findCustomer() {
    document.getElementById('gauch').style.display = 'block';
    var term = document.getElementById('finder').value;
    $.ajax({
        type: 'POST',
        url: 'modules/posmodule/view/findCustomer.php',
        async: false,
        data: 'term=' + term,
        success: function(data) {
            document.getElementById('gauch').style.display = 'none';
            document.getElementById('showCl').innerHTML = data;
        }
    });
}

function showInfo(id, site) {
    $.fancybox.open({
        href: site + '?id=' + id,
        type: 'iframe',
        padding: 5,
        afterClose: function() { // USE THIS IT IS YOUR ANSWER THE KEY WORD IS "afterClose"
            parent.location.reload(true);
        }
    });
}

function calcDelvol() {
    var cash = document.getElementById('cash').value;
    var total = document.getElementById('total').value;

    if (cash === '' || isNaN(cash)) {
        document.getElementById('devol1').value = 0;
        document.getElementById('devol').value = 0;
    } else {
        var devol = parseFloat(cash) - parseFloat(total);
        devol = devol.toFixed(2);

        document.getElementById('devol1').value = devol;
        document.getElementById('devol').value = devol;
    }


}

function builtCash(value) {
    var cash = document.getElementById('cash').value;

    if (cash === '' || isNaN(cash)) {
        document.getElementById('cash').value = 0;
    } else {
        cash = parseFloat(cash) + parseFloat(value);
        cash = cash.toFixed(2);
        document.getElementById('cash').value = cash;
        calcDelvol();
    }
}

function clearCash() {
    //alert('hola');
    document.getElementById('cash').value = 0;
    calcDelvol();
}

function findPosList() {
    //document.getElementById('gauch2').style.display='block';
    var barcode = document.getElementById('barcode').value;
    var qty = document.getElementById('qty').value;
    document.getElementById('cash').value = 0;
    document.getElementById('devol').value = 0;
    document.getElementById('devol1').value = 0;
    $.ajax({
        type: 'POST',
        url: 'modules/posmodule/view/addPosItem.php',
        async: false,
        data: 'barcode=' + barcode + "&qty=" + qty,
        success: function(data) {

            //document.getElementById('gauch2').style.display='none';
            document.getElementById('showposL').innerHTML = data;
            findPosTotal();
            findPosTotal2();

            document.getElementById('barcode').value = "";
            document.getElementById('barcode').focus();
        }
    });
}

function findPosList2(barcode, qty) {

    //document.getElementById('gauch2').style.display='block';
    //document.getElementById('cash').value = 0;
    //document.getElementById('devol').value = 0;
    //document.getElementById('devol1').value = 0;

    $.ajax({
        type: 'POST',
        url: 'modules/posmodule/view/addPosItem.php',
        async: false,
        data: 'barcode=' + barcode + "&qty=" + qty,
        success: function(data) {
            //document.getElementById('gauch2').style.display='none';
            $("#prodname").val("")
            $("#prodname").focus()
            document.getElementById('content__table').innerHTML = data;
            findPosTotal();
            findPosTotal2();
        }
    });
}

function addProductByReference() {
    let barcode = document.getElementById("add-product").value
    $.ajax({
        type: 'POST',
        url: 'modules/posmodule/view/addPosItem.php',
        async: false,
        data: { barcode, qty: 1 },
        success: function(data) {
            if (data) {
                //document.getElementById('gauch2').style.display='none';
                document.getElementById('content__table').innerHTML = data;
                findPosTotal();
                findPosTotal2();
            } else {
                Toastify({
                    text: "Producto no encontrado",
                    duration: 2000,
                    newWindow: true,
                    close: true,
                    position: "center", // `left`, `center` or `right`
                    stopOnFocus: true, // Prevents dismissing of toast on hover
                    style: {
                        background: "#dc3545",
                    },
                    onClick: function() {} // Callback after click
                }).showToast();
            }
        }
    });
}

function delPosItem(id) {
    //document.getElementById('gauch2').style.display='block';
    //document.getElementById('cash').value = 0;
    //document.getElementById('devol').value = 0;
    //document.getElementById('devol1').value = 0;
    $.ajax({
        type: 'POST',
        url: 'modules/posmodule/view/delPosItem.php',
        async: false,
        data: 'id=' + id,
        success: function(data) {

            //document.getElementById('gauch2').style.display='none';
            document.getElementById('content__table').innerHTML = data;
            findPosTotal();
            findPosTotal2();
        }
    });
}

/* edit viejo
 function editQtyItem(id, qty, action){
     //alert(action);
	document.getElementById('gauch2').style.display='block';
	document.getElementById('cash').value = 0;
	document.getElementById('devol').value = 0;
	document.getElementById('devol1').value = 0;
	$.ajax({
	 type:'POST',
	 url:'modules/posmodule/view/editPosQtyItem.php',
	 async:false,
	 data: 'id='+id+'&qty='+qty+'&action='+action,
	 success:function(data){
		 console.log(data);
		 document.getElementById('gauch2').style.display='none';
		 document.getElementById('showposL').innerHTML=data;
		 findPosTotal();
		 findPosTotal2();
	 }
	});
 }*/

function editQtyItem(id, qty, action) {
    //alert(action);
    //document.getElementById('gauch2').style.display='block';
    //document.getElementById('cash').value = 0;
    //document.getElementById('devol').value = 0;
    //document.getElementById('devol1').value = 0;
    $.ajax({
        type: 'POST',
        url: 'modules/posmodule/view/editPosQtyItem.php',
        async: false,
        data: 'id=' + id + '&qty=' + qty + '&action=' + action,
        success: function(data) {
            //document.getElementById('gauch2').style.display='none';
            document.getElementById('content__table').innerHTML = data;
            findPosTotal();
            findPosTotal2();
        }
    });
}

function editPriceItem(id, price) {
    //alert(action);
    //document.getElementById('gauch2').style.display='block';
    document.getElementById('cash').value = 0;
    document.getElementById('devol').value = 0;
    document.getElementById('devol1').value = 0;
    $.ajax({
        type: 'POST',
        url: 'modules/posmodule/view/editPosPriceItem.php',
        async: false,
        data: 'id=' + id + '&price=' + price,
        success: function(data) {
            //alert(data);
            //document.getElementById('gauch2').style.display='none';
            document.getElementById('showposL').innerHTML = data;
            findPosTotal();
            findPosTotal2();
        }
    });
}

function findProductByCategory(id_cat) {
    $.ajax({
        type: 'POST',
        url: 'modules/posmodule/view/findProdCat.php',
        async: false,
        data: 'id_cat=' + id_cat,
        success: function(data) {
            document.getElementById('listprod').innerHTML = data;
        }
    });
}

$(document).ready(function() {
    findProductByCategory2(1)
    findPosTotal2()
})

function selectCategory(id_cat) {
    let elemCat = $("#cat_" + id_cat)
    reinitAll()

    elemCat.children('div').addClass('active')
    $("#sup_" + id_cat).addClass('radius_sup')
    $("#down_" + id_cat).addClass('radius_down')

    $("#li_" + id_cat).children('div').each(function() {
        if ($(this).hasClass('aux_sup')) {
            $(this).addClass('active')
        }
    })
}

function reinitAll() {
    $(".content__categories-into").removeClass('active')
    $(".aux_sup-son").removeClass('radius_sup radius_down')
    $(".card_li_product").children('div').each(function() {
        if ($(this).hasClass('aux_sup')) {
            $(this).removeClass('active')
        }
    })
}

function modeList() {
    let id_cat = $("#id_cat").val()
    $("#icon_mode").html(`
		<img src="images/icons/options_content.png" alt="" width="25px" onclick="modeImage()">  
	`)
    if (prod_name) {
        findProductByNameModeList(1)
    } else {
        findProductByCategoryModeList(id_cat, 1)
    }
}

function modeImage() {
    let id_cat = $("#id_cat").val()
    $("#icon_mode").html(`
		<img src="images/icons/options_content.png" alt="" width="25px" onclick="modeList()">  
	`)
    if (prod_name) {
        findProductByNameModeList(0)
    } else {
        findProductByCategoryModeList(id_cat, 0)
    }
}

function findProductByCategoryModeList(id_cat, mode_list = 0) {
    //document.getElementById('showCat').style.display='none';
    selectCategory(id_cat)
    $("#products_card").animate({ scrollTop: 0 }, 'slow')

    $.ajax({
        type: 'POST',
        url: 'modules/posmodule/view/findProdCat2.php',
        async: false,
        data: { id_cat, mode_list },
        success: function(data) {
            //document.getElementById('showProd').style.display='inline-block';
            document.getElementById('products_card').innerHTML = data;
            document.getElementById('id_cat').value = id_cat;
        }
    });
}

function findProductByCategory2(id_cat) {
    //document.getElementById('showCat').style.display='none';
    selectCategory(id_cat)
    $("#products_card").animate({ scrollTop: 0 }, 'slow')
    prod_name = null
    $("#prodname").val("")

    $.ajax({
        type: 'POST',
        url: 'modules/posmodule/view/findProdCat2.php',
        async: false,
        data: { id_cat },
        success: function(data) {
            //document.getElementById('showProd').style.display='inline-block';
            document.getElementById('products_card').innerHTML = data;
            document.getElementById('id_cat').value = id_cat;
        }
    });
}

function restoreCat() {
    document.getElementById('showCat').style.display = 'inline-block';
    document.getElementById('showProd').style.display = 'none';
}
let prod_name = null

function findProductByName() {
    var name = document.getElementById('prodname').value;
    prod_name = name
    $.ajax({
        type: 'POST',
        url: 'modules/posmodule/view/findProdName.php',
        async: false,
        data: 'name=' + name,
        success: function(data) {
            document.getElementById('products_card').innerHTML = data;
            selectCategory(-1)
                //document.getElementById('listprod2').innerHTML=data;
        }
    });
}

function findProductByNameModeList(mode_list) {
    var name = document.getElementById('prodname').value;
    prod_name = name
    $.ajax({
        type: 'POST',
        url: 'modules/posmodule/view/findProdName.php',
        async: false,
        data: { name, mode_list },
        success: function(data) {
            document.getElementById('products_card').innerHTML = data;
            selectCategory(-1)
                //document.getElementById('listprod2').innerHTML=data;
        }
    });
}

function findPosTotal() {

    $.ajax({
        type: 'POST',
        url: 'modules/posmodule/view/findPosTotal.php',
        async: false,
        success: function(data) {
            document.getElementById('totalpos').innerHTML = data;
            //document.getElementById('totalpos0').innerHTML=data;

            /*var i = document.getElementById('i').value;
            i = parseFloat(i);
            //alert(i);
            for(var j = 1; j<i; j++)
            {
               document.getElementById('totalpos'+j).innerHTML=data; 
            }*/
        }
    });
}

function findPosTotal2() {
    $.ajax({
        type: 'POST',
        url: 'modules/posmodule/view/findPosTotal2.php',
        async: false,
        success: function(data) {
            if (data == 0) {
                //document.getElementById('formpos1').style.display="none";
                document.getElementById('content__parent-methodspay').style.display = "none";
                //document.getElementById('btndesce').style.display = "none";
                document.getElementById('view_cliente').style.width = "100%";

            } else {
                //document.getElementById('formpos1').style.display="block";
                document.getElementById('content__parent-methodspay').style.display = "flex";
                //document.getElementById('btndesce').style.display = "flex";
                document.getElementById('view_cliente').style.width = "49%";

            }
            document.getElementById('total').value = data;
        }
    });
}

function closePos(id_payment, facturado = 1) {
    var cash = document.getElementById('cash') ? document.getElementById('cash').value : 0;
    var devol = document.getElementById('devol') ? document.getElementById('devol').value : 0;
    var u_id = document.getElementById('u_id').value;
    var txs = document.getElementById('txs' + id_payment).value;

    $.ajax({
        type: 'POST',
        url: 'modules/posmodule/view/closePos.php',
        async: false,
        data: 'cash=' + cash + "&devol=" + devol + "&u_id=" + u_id + "&txs=" + txs + "&id_payment=" + id_payment + "&facturado=" + facturado,
        success: function(data) {
            if (data !== '') {
                if (facturado == 1) {
                    showInfo(data, 'modules/posmodule/view/showReceipt.php');
                } else {
                    Toastify({
                        text: "Pago Procesado correctamente",
                        duration: 2000,
                        newWindow: true,
                        close: true,
                        position: "center", // `left`, `center` or `right`
                        stopOnFocus: true, // Prevents dismissing of toast on hover
                        style: {
                            background: "#1cc88a",
                        },
                        onClick: function() {} // Callback after click
                    }).showToast();
                    setTimeout(function() {
                        location.reload()
                    }, 1500)
                }
            }
        }
    });
}

function enableBtn(id) {
    document.getElementById('payment' + id).style.display = 'block';
}

let method_id = null

function selectMethodPay(id) {
    if ($(`#method-${id}`).hasClass('active-method')) {
        method_id = null
        $(".method-pay").removeClass('active-method')
        changeButtonPay(method_id)
    } else {
        method_id = id
        $(".method-pay").removeClass('active-method')
        changeButtonPay(method_id)
        $(`#method-${method_id}`).addClass('active-method')
    }
}

function changeButtonPay(method_id) {
    if (method_id == 0) {
        $("#content-button-pay").html(`
			<input type="button" value="PAGAR" class="form-control bg-color-bottom text-white"  onclick="getMethodPayEfectivo()" data-toggle="modal" data-target="#pay-method-efectivo" data-backdrop="false">
			<input type="button" value="SIN FACTURA" class="form-control bg-color-bottom text-white" onclick="getMethodPayEfectivo()" data-toggle="modal" data-target="#pay-method-efectivo" data-backdrop="false">
		`)
    } else {
        $("#content-button-pay").html(`
			<input type="button" value="PAGAR" class="form-control bg-color-bottom text-white" onclick="pay()" id="button-pay">
			<input type="button" value="SIN FACTURA" class="form-control bg-color-bottom text-white" onclick="withoutFacture()" id="button-sin-factura">
		`)
    }
}

function pay() {
    if (!method_id) {
        Toastify({
            text: "Por favor escoge un metodo de pago",
            duration: 2000,
            newWindow: true,
            close: true,
            position: "center", // `left`, `center` or `right`
            stopOnFocus: true, // Prevents dismissing of toast on hover
            style: {
                background: "#dc3545",
            },
            onClick: function() {} // Callback after click
        }).showToast();
    } else {
        closePos(method_id, 1)
    }
}

function withoutFacture() {
    if (!method_id) {
        Toastify({
            text: "Por favor escoge un metodo de pago",
            duration: 2000,
            newWindow: true,
            close: true,
            position: "center", // `left`, `center` or `right`
            stopOnFocus: true, // Prevents dismissing of toast on hover
            style: {
                background: "#dc3545",
            },
            onClick: function() {} // Callback after click
        }).showToast();
    } else {
        closePos(method_id, 0)
    }
}

async function getMethodPayEfectivo() {
    if (method_id != null) {
        console.log("method_id", method_id)
        showEfectivo()
    }
}

async function getMethodPay() {
    if (method_id != null) {
        if (method_id != 0) {
            $.get(`modules/posmodule/model/api.php?method=getPaymentMethodsById&value=${method_id}`).then(response => {
                if (response.length > 0) {
                    getTotal().then(total => {
                        let methods = JSON.parse(response)
                        let method = methods[0]
                        $("#body-pay").html(`
							<img src="modules/posmodule/imagesPm/${method.pospm_image}"width="200px" height="100px" />
							<div class="d-flex flex-column justify-content-start w-50" id="body-pay-content">
								<h4>TOTAL</h4>
								<span>$${total}</span>
								<input name="txs" id="txs${method['pospm_id']}" type="hidden" value="" onChange="enableBtn(${method['pospm_id']})"  /><br>
								<div class="d-flex">
									<input type="button" value="Pagar" id="totalpos${method.pospm_id}" onClick="closePos(${method['pospm_id']}, 1)" class="btn btn-dark mr-2" style="display:block; width: 25%; min-width:84px; font-size: 16px; border-radius:5px !important;"/>
									<input type="button" value="Sin Factura" id="totalpos${method.pospm_id}" onClick="closePos(${method['pospm_id']}, 0)" class="btn btn-dark" style="display:block; font-size: 16px; border-radius:5px !important;"/>
								</div>
								
							</div>
						`)
                    })
                }
            })
        } else {
            showEfectivo()
        }
    }
}

async function showEfectivo() {
    let coins = JSON.parse(await $.get('modules/posmodule/model/api.php?method=getCoins&value=`'))
    let total = JSON.parse(await $.get(`modules/posmodule/model/api.php?method=getPosTotal`))
    $("#modal-content-coins").html("")
    $("#modal-content-resumen").html("")

    for (let coin of coins) {
        $("#modal-content-coins").append(`
			<div>
				<img src="modules/posmodule/imagesCoins/${coin.posc_image}" onClick="builtCash(${coin.posc_value})"/>
			</div>
		`)
    }

    $("#modal-content-resumen").append(`
		<div id="body-total-efectivo">
			<div class="d-flex flex-column">
				<h4>TOTAL</h4>
				<span>$${total}</span>
			</div>
			
			<div class="d-flex flex-column">
				<span>EFECTIVO $:</span>
				<input name="cash" id="cash" type="text" value="0" class="form-control" onchange="calcDelvol()" /><br>
			</div>

			<div class="d-flex flex-column">
				<span>CAMBIO $:</span>
				<input name="devol1" id="devol1" type="text" value="0" disabled="disabled" class="form-control"/>
			</div>
			
			<input name="devol" id="devol" type="hidden" value="0" /><br>
			<input name="txs" id="txs0" type="hidden" value="" /><br>
			<div class="d-flex">
				<input type="button" value="Pagar" onClick="closePos(0, 1)" class="btn btn-success mr-1" style="display:block; width: 25%; min-width:84px; font-size: 16px; border-radius:5px !important;" />
				<input type="button" value="Sin Factura" onClick="closePos(0, 0)" class="btn btn-success" style="display:block; min-width:120px; font-size: 16px; border-radius:5px !important;" />
			</div>
			<br>
			<input type="button" value="Limpiar" onClick="clearCash()" class="btn btn-danger" style="display:block; width: 25%; min-width:84px; font-size: 16px; border-radius:5px !important;" />
		</div>
	`)
}

function getTotal() {
    return $.get(`modules/posmodule/model/api.php?method=getPosTotal`)
}


function validateCoupon() {
    var cupon = document.getElementById('cupon').value;
    var total = document.getElementById('total').value;

    if (cupon) {
        $.ajax({
            type: 'POST',
            url: 'modules/posmodule/view/addCoupon.php',
            async: false,
            data: { cupon, total },
            success: function(data) {
                let dataParse = JSON.parse(data)
                console.log("data", data)
                if (dataParse.success) {
                    document.getElementById('content__table').innerHTML = dataParse.data;
                    findPosTotal();
                    findPosTotal2();
                    document.getElementById('cupon').value = "";
                    Toastify({
                        text: "Cupon Agregado Correctamente",
                        duration: 2000,
                        newWindow: true,
                        close: true,
                        position: "center", // `left`, `center` or `right`
                        stopOnFocus: true, // Prevents dismissing of toast on hover
                        style: {
                            background: "#1cc88a",
                        },
                        onClick: function() {} // Callback after click
                    }).showToast();
                } else {
                    Toastify({
                        text: dataParse.message,
                        duration: 2000,
                        newWindow: true,
                        close: true,
                        position: "center", // `left`, `center` or `right`
                        stopOnFocus: true, // Prevents dismissing of toast on hover
                        style: {
                            background: "#e74a3b",
                        },
                        onClick: function() {} // Callback after click
                    }).showToast();
                }
            }
        });
    } else {
        Toastify({
            text: "Escribe un cupon",
            duration: 2000,
            newWindow: true,
            close: true,
            position: "center", // `left`, `center` or `right`
            stopOnFocus: true, // Prevents dismissing of toast on hover
            style: {
                background: "#e74a3b",
            },
            onClick: function() {} // Callback after click
        }).showToast();
    }

}


function addDiscount() {
    console.log("siis")
    var usr = document.getElementById('usr').value;
    var pass = document.getElementById('pass').value;
    var por = document.getElementById('por').value;
    var desc = document.getElementById('desc1').value;
    var total = document.getElementById('total').value;

    valAuthDiscount(usr, pass);

    if (ok == 'ok') {

        document.getElementById('usr').value = '';
        document.getElementById('pass').value = '';
        document.getElementById('por').value = '';

        $.ajax({
            type: 'POST',
            url: 'modules/posmodule/view/addDiscount.php',
            async: false,
            data: 'usr=' + usr + "&pass=" + pass + "&por=" + por + "&desc=" + desc + "&total=" + total,
            success: function(data) {

                document.getElementById('content__table').innerHTML = data;
                findPosTotal();
                findPosTotal2();

                Toastify({
                    text: 'Descuento agregado correctamente',
                    duration: 3000,
                    newWindow: true,
                    close: true,
                    position: "center", // `left`, `center` or `right`
                    stopOnFocus: true, // Prevents dismissing of toast on hover
                    style: {
                        background: "#49bf9d",
                    },
                    onClick: function() {} // Callback after click
                    }).showToast();
            }
        });
    } else {
        Toastify({
            text: 'No esta autorizado para agregar descuentos',
            duration: 6000,
            newWindow: true,
            close: true,
            position: "center", // `left`, `center` or `right`
            stopOnFocus: true, // Prevents dismissing of toast on hover
            style: {
                background: "#dc3545",
            },
            onClick: function() {} // Callback after click
        }).showToast();
    }
}


function valAuthDiscount(usr, pass) {

    $.ajax({
        type: 'POST',
        url: 'modules/posmodule/view/valAddDiscount.php',
        async: false,
        data: 'usr=' + usr + "&pass=" + pass,
        success: function(data) {
            ok = data;
        }
    });
}