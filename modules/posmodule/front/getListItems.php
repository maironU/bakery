<?php
    include('../view/include_self.php');
    
    $id_user = $_SESSION['user_id'];
    $id_store = $obj->postVars('id_store');
    
    $html = '
    <h1>Items</h1>
		<div class="table-wrapper">
			<table>
				<thead>
				    <tr>
					    <th>Qty</th>
                    	<th>Item</th>
                    	<th>Price</th>
                    	<th>Tax</th>
                    	<th>Total</th>
					</tr>
				</thead>
				<tbody>';
					$row = $obj->getPosDetailTemp2($id_user, $id_store);
					if(count($row)>0)
					{
					    foreach($row as $row)
					    {
					        $html .= '
					            <tr>
					                <td>'.$row['posdt_qty'].'</td>
					                <td>'.$row['posdt_productname'].'</td>
					                <td>$'.number_format($row["posdt_price"],2).'</td>
					                <td>$'.number_format($row["posdt_tax"]*$row["posdt_qty"],2).'</td>
					                <td>$'.number_format(($row["posdt_price"]+$row["posdt_tax"])*$row["posdt_qty"],2).'</td>
					            </tr>
					        ';
					        $tax += $row["posdt_tax"]*$row["posdt_qty"];
					        $total += ($row["posdt_price"]+$row["posdt_tax"])*$row["posdt_qty"];
					    }
					}
				$html.='</tbody>
				<tfoot>
					<tr>
						<td colspan="3"></td>
						<td>$'.number_format($tax,2).'</td>
						<td>$'.number_format($total,2).'</td>
					</tr>
				</tfoot>
			</table>
		</div>';
		
		echo $html;
?>