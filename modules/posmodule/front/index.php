<?php
    include('../view/include_self.php');
    
    $id_user = $_SESSION['user_id'];
    $id_store = $obj->getVars('id_store');
?>
<!DOCTYPE HTML>
<!--
	Strata by HTML5 UP
	html5up.net | @n33co
	Free for personal and commercial use under the CCA 3.0 license (html5up.net/license)
-->
<html>
	<head>
		<title>Punto de venta.</title>
		<meta http-equiv="content-type" content="text/html; charset=utf-8" />
		<meta name="description" content="" />
		<meta name="keywords" content="" />
		<!--[if lte IE 8]><script src="css/ie/html5shiv.js"></script><![endif]-->
		<script src="../js/jquery-1.8.3.js"></script>
		<script src="js/jquery.poptrox.min.js"></script>
		<script src="js/skel.min.js"></script>
		<script src="js/init.js"></script>
		<noscript>
			<link rel="stylesheet" href="css/skel.css" />
			<link rel="stylesheet" href="css/style.css" />
			<link rel="stylesheet" href="css/style-xlarge.css" />
		</noscript>
		<!--[if lte IE 8]><link rel="stylesheet" href="css/ie/v8.css" /><![endif]-->
	</head>
	<body id="top" onLoad="startGetItems(<?php echo $id_user?>, <?php echo $id_store?>)">

		<!-- Header -->
			<header id="header" style="background-image: url('../paramFilesP/<?php echo $obj->getPosSetupById(6)?>')">
				
			</header>

		<!-- Main -->
			<div id="main">

				<!-- Four -->
				
					<section id="four">

						<section>
							<div id="listitems"></div>
							
						</section>


					</section>
				

			</div>

		<!-- Footer -->

	</body>
	<script>
	    function getItems(id_user, id_store)
	    {
        	$.ajax({
        	 type:'POST',
        	 url:'getListItems.php',
        	 async:false,
        	 data: 'id_user='+id_user+"&id_store="+id_store,
        	 success:function(data){
        		 document.getElementById('listitems').innerHTML=data;
        	 }
        	});
         }
         
         function startGetItems(id_user, id_store)
         {
			setInterval(() => {
				getItems(id_user, id_store);
			}, 2000);
         }
         
	</script>
</html>