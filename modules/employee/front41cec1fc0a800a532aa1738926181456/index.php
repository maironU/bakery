<?php
    include('../view/includes_self.php');
    
    $res = "";
    
    if($_POST)
    {
        $code = trim($obj->postVars('code'));
		$res = $obj->runAccessCtrl($code);
		echo '<script>console.log('.json_encode($res).')</script>';
        $row = $obj->getEmployeeByCode($code);
        if(count($row)>0 && $res['success'] === true)
        {
            foreach($row as $row)
            {
                $name = $row['em_name'];
				$img = $row['em_image'];
				$date = $res["register"];
            }
        }
    }
?>
<!DOCTYPE HTML>
<!--
	Strata by HTML5 UP
	html5up.net | @n33co
	Free for personal and commercial use under the CCA 3.0 license (html5up.net/license)
-->
<html>
	<head>
		<title>Registro de empleados</title>
		<meta http-equiv="content-type" content="text/html; charset=utf-8" />
		<meta name="description" content="" />
		<meta name="keywords" content="" />
		<!--[if lte IE 8]><script src="css/ie/html5shiv.js"></scrip><![endif]-->
		<link href="//cdn.jsdelivr.net/npm/@sweetalert2/theme-dark@4/dark.css" rel="stylesheet">
		<link rel="stylesheet" type="text/css" href="https://cdn.jsdelivr.net/npm/toastify-js/src/toastify.min.css">
		<script src="js/jquery.min.js"></script>
		<script src="js/jquery.poptrox.min.js"></script>
		<script src="js/skel.min.js"></script>
		<script src="js/init.js"></script>
		<script src="//cdn.jsdelivr.net/npm/sweetalert2@11/dist/sweetalert2.min.js"></script>
		<script type="text/javascript" src="https://cdn.jsdelivr.net/npm/toastify-js"></script>
		<noscript>
			<link rel="stylesheet" href="css/skel.css" />
			<link rel="stylesheet" href="css/style.css" />
			<link rel="stylesheet" href="css/style-xlarge.css" />
		</noscript>
		<!--[if lte IE 8]><link rel="stylesheet" href="css/ie/v8.css" /><![endif]-->
	</head>
	<style>
		@media (max-width: 756px){
			.message-register{
				width:90%;
				margin: 15px auto;
			}

			#card-registro {
				min-width: 300px;
			}
		}
		
		.swal2-popup {
			background: #fff;
		}
	</style>
	<body id="top" style="display:flex; justify-content:center;align-items:center; height: 100vh !important; background-image: url('../../../images/fondoregistro.jpeg')">
		<div id="card-registro" style="width: 40%; max-width: 400px; margin: 0 auto; margin-top: 25px; border: solid 2px transparent; background: white; padding: 25px; border-radius: 0.35rem">
			<div style="
					border-bottom: 1px solid;
					height: 34px;
					margin-bottom: 23px;
				">
				<h4>Registro de ingreso y salida</h4>
			</div>
			<section id="four" style="display:flex; justify-content:center; width: 100%; margin: 0 auto">
				<section id="section-top" style="width: 100%">
					<form method="post" action="" id="register-codigo">
						<div class="row uniform 50%">
							<div class="6u 12u$(xsmall)" style="width: 100%" >
								<input type="password" name="code" id="code" value="" placeholder="Codigo" autofocus required/>
							</div>
							<div class="12u$" style="width: 100%" >
								<ul class="actions">
									<li style="width: 100%" ><input style="width: 100%"  type="submit" value="Registrar" class="special" /></li>
								</ul>
							</div>
						</div>
					</form>
				</section>
			</section>
		</div>

		<!-- Footer -->

	</body>

	<script>
		$("#register-codigo").submit(function(e){
			e.preventDefault();
			let code = $("#code").val();
			$.get(`../model/api.php?method=getTypeRegister&value=${code}`).then(response => {
				response = JSON.parse(response)
				console.log("repsonse", response)
				if(response.success){
					if(response.type == "cod"){
						$("#message-register").remove()
						$("#message-register-two").remove()
						$("#code").val("");
						Toastify({
							text: response.message,
							duration: 3000,
							newWindow: true,
							close: true,
							position: "center", // `left`, `center` or `right`
							stopOnFocus: true, // Prevents dismissing of toast on hover
							style: {
								background: "#49bf9d",
								fontSize: "25px",
								width: "60%",
								textAlign: "center"
						},
						onClick: function() {} // Callback after click
						}).showToast();
					}else{
						Swal.fire({
							title: response.message,
							showCancelButton: true,
							confirmButtonColor: '#49bf9d',
							confirmButtonText: 'Save',
							}).then((result) => {
							/* Read more about isConfirmed, isDenied below */
							if (result.isConfirmed) {
								fetchRegister(response.type)
							}
						})
					}

					$("#swal2-title").css("color", "#a2a2a2")
				}else{
					$("#message-register").remove()
					$("#message-register-two").remove()

					Toastify({
						text: response.message,
						duration: 2000,
						newWindow: true,
						close: true,
						position: "center", // `left`, `center` or `right`
						stopOnFocus: true, // Prevents dismissing of toast on hover
						style: {
							background: "#dc3545",
						},
						onClick: function() {} // Callback after click
					}).showToast();
				}
			});
		})

		function fetchRegister(type){
			let code = $("#code").val();
			$.post(`../model/api.php?method=register&value=${code}`, { type }).then(response => {
				response = JSON.parse(response)
				console.log("response crear", response)
				$("#code").val("");
				if(response.success){
					$("#message-register").remove()
					$("#message-register-two").remove()

					$("#section-top").prepend(`
						<div id="message-register-two" style="text-align: center; background : #d2f4e8; border-radius: 0.35rem; margin-top: 10px; padding: 8px; margin-bottom: 10px">
							${type == "in" ? 
								`
									<h4>Se ha registrado tu ENTRADA satisfactoriamente</h4>
								`
								:
								`
									<h4>Se ha registrado tu SALIDA satisfactoriamente</h4>
								`
							}
						</div>
						
					`)

					$("#section-top").prepend(`
						<header class="message-register" id="message-register" style="border-radius: 0.35rem; padding: 15px;background: #d2f4e8; display:flex; flex-direction: column; align-items:center;justify-content:center; width: 100%; margin:0 auto; margin-top: 15px">
							<div>
								<img src="../employeeImages/${response.data.img}" alt="" style="width: 80px; height: 80px; border-radius: 300px; display: block; border: solid 3px #ccc" />
							</div>
							<div style="margin: 0; text-align: center"><strong>Hola!</strong>, ${response.data.name}
								<div>
								${type == "in" ? `
										<span>Fecha de registro: </span>
										<span>${response.data.date}</span>
									` 
									: 
									`
										<span>Fecha de salida: </span>
										<span>${response.data.date}></span>
									`
								}
								</div>
							</div>
						</header>
					`)
				}else{
					$("#message-register").remove()
					$("#message-register-two").remove()

					Toastify({
						text: response.message,
						duration: 6000,
						newWindow: true,
						close: true,
						position: "center", // `left`, `center` or `right`
						stopOnFocus: true, // Prevents dismissing of toast on hover
						style: {
							background: "#dc3545",
							fontSize: "25px",
							width: "60%",
							textAlign: "center"
						},
						onClick: function() {} // Callback after click
					}).showToast();
				}
			})

		}

	</script>
</html>