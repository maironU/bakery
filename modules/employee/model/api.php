<?php
    include('../view/includes_self.php');
    
    $method = $_GET['method'];
    $value = $_GET['value'];
    $type = $_POST['type'];

    if(isset($method)){
        switch($method){
            case "getTypeRegister":
                $getTypeRegister = $obj->getTypeRegister($value);
                echo json_encode($getTypeRegister);
                break;
            case "register":
                $register = $obj->register($value, $type);
                echo json_encode($register);
                break;
        }
    }
?>