<?php
class employee extends GeCore{
	
	//player Methods	
	public function newEmployee(){
		$name=$this->postVars('name');
		$birth=$this->postVars('birth');
		$addr=$this->postVars('addr');
		$phone=$this->postVars('phone');
		$code=$this->postVars('code');
		$indate=$this->postVars('indate');
		$profile=$this->postVars('profile');
		$salary=$this->postVars('salary');
		$hsalary=$this->postVars('hsalary');
		$email=$this->postVars('email');
		$position=$this->postVars('position');
		$id_user=$this->postVars('id_user');
		$place=$this->postVars('place');
		$id_credential=$this->postVars('id_credential');
		$schedin=$this->postVars('schedin');
		$schedout=$this->postVars('schedout');
		
		
		$sql="INSERT INTO `employee`(`em_name`, `em_birth`, `em_addr`, `em_phone`, `em_code`, `em_indate`, `em_image`, `em_profile`, `em_salary`, `em_hsalary`, `em_active`, `em_state`, `em_email`, `em_position`, `u_id`, `c_id`, `em_place`, `em_schedulein`, `em_scheduleout`) VALUES ('$name','$birth','$addr','$phone','$code','$indate','default.png','$profile','$salary','$hsalary','1','0','$email','$position','$id_user','$id_credential','$place','$schedin','$schedout')";
		$this->Execute($sql);
		
		$id=$this->getLastID();
		
		$this->uploadImage1($id);
	}
	
	public function uploadImage1($id, $path = 'modules/employee/employeeImages'){
		
		$ok=$this->upLoadFileProccess('img', $path);
		
		if($ok){
            $sql="update employee set em_image='".$this->fname."' where em_id='$id'";
            $this->Execute($sql);
		}
		
	}
	
	public function editEmployee(){
		$id=$this->postVars('id');
		$name=$this->postVars('name');
		$birth=$this->postVars('birth');
		$addr=$this->postVars('addr');
		$phone=$this->postVars('phone');
		$code=$this->postVars('code');
		$indate=$this->postVars('indate');
		$profile=$this->postVars('profile');
		$salary=$this->postVars('salary');
		$hsalary=$this->postVars('hsalary');
		$email=$this->postVars('email');
		$position=$this->postVars('position');
		$id_user=$this->postVars('id_user');
		$place=$this->postVars('place');
		$schedin=$this->postVars('schedin');
		$schedout=$this->postVars('schedout');
		
		
		$sql="UPDATE `employee` SET `em_name`='$name', `em_birth`='$birth', `em_addr`='$addr', `em_phone`='$phone', `em_code`='$code', `em_indate`='$indate', `em_profile`='$profile', `em_salary`='$salary', `em_hsalary`='$hsalary', `em_email`='$email', `em_position`='$position', `u_id`='$id_user', `em_place`='$place', `em_schedulein`='$schedin', `em_scheduleout`='$schedout' WHERE em_id='$id'";
		$this->Execute($sql);
		
		$this->uploadImage1($id);
	}
	
	public function delEmployee(){
		$id=$this->getVars('id');
		
		$sql="delete from `employee` where em_id='$id'";
		$this->Execute($sql);
		
	}
	
	public function getEmployee(){
	 $sql="select * from employee";
	 return $this->ExecuteS($sql);
	}
	
	public function getEmployeeActive($act = 1){
	 $sql="select * from employee where em_active='$act'";
	 return $this->ExecuteS($sql);
	}
	
	public function getEmployeeById($id){
	  $sql="select * from employee where em_id='$id'";
	  return $this->ExecuteS($sql);
	}
	
	public function getEmployeeByCode($code){
	  $sql="select * from employee where em_code='$code'";
	  return $this->ExecuteS($sql);
	}
	
	public function getEmployeeByName($name){
	  $sql="select * from employee where em_name like '%$name%'";
	  return $this->ExecuteS($sql);
	}
	
	public function editEmployeeStatus($id, $state)
	{
        $sql="UPDATE `employee` SET `em_state`='$state' WHERE em_id='$id'";
		$this->Execute($sql);
	}
	

    //Credentials Methods
    public function newCredential()
    {
            $name = $this->postVars('name');
            $desc = $this->postVars('desc');
            
            $sql="insert into `credential` (`c_name`, `c_desc`) values ('$name','$desc')";
            $this->Execute($sql);
            
            $id=$this->getLastID();
            
            $this->uploadPhotoCredential($id);
            
    }

    public function uploadPhotoCredential($id){
            
            $ok=$this->upLoadFileProccess('img', 'modules/employee/credentialPhoto');
            
            if($ok){
            $sql="update `credential` set c_image='".$this->fname."' where c_id='$id'";
            $this->Execute($sql);
            }
            
        }
        
    public function editCredential()
    {
            $id = $this->postVars('id');
            $name = $this->postVars('name');
            $desc = $this->postVars('desc');

        $sql="update `credential` set `c_name`='$name', `c_desc`='$desc' where c_id='$id'";
        $this->Execute($sql);

            $this->uploadPhotoCredential($id);
            
    }

    public function delCredential()
    {
            $id = $this->getVars('id');
            
            $sql="delete from `credential` where c_id='$id'";
            $this->Execute($sql);		
    }

    public function getCredential()
    {		
            $sql = $sql="select * from `credential`";
            return $this->ExecuteS($sql);		
    }

    public function getCredentialById($id)
    {		
            $sql="select * from `credential` where c_id='$id'";
            return $this->ExecuteS($sql);		
    }

    //Employee Access Ctrl
    public function newAccessCtrl()
    {
        $datein = $this->postVars('datein');
        $dateout = $this->postVars('dateout');
        $hourin = $this->postVars('hourin');
        $hourout = $this->postVars('hourout');
        $id_employee = $this->postVars('id_employee');
        
        $today = date('Y-m-d H:i:s');
        
        $sql = "INSERT INTO `employee_accessctrl`(`eac_date`, `eac_datein`, `eac_dateout`, `eac_hourin`, `eac_hourout`, `em_id`) VALUES ('$today','$datein','$dateout','$hourin','$hourout','$id_employee')";
        
        $this->Execute($sql);
        $id = $this->getLastID();
        
        if($dateout != '' && $hourout != '')
        {
            $this->calculateWorkedHours($id);
        }
        
    }

    public function newAccessCtrlIn($datein, $hourin, $id_employee)
    {
        $today = date('Y-m-d H:i:s');
        $sql = "INSERT INTO `employee_accessctrl`(`eac_date`, `eac_datein`, `eac_hourin`, `em_id`, `eac_state`) VALUES ('$today','$datein','$hourin','$id_employee','1')";
        $this->Execute($sql);

        return $datein." ".$hourin;
    }

    public function newAccessCtrlOut($dateout, $hourout, $id_employee)
    {
        $sql = "SELECT * FROM `employee_accessctrl` WHERE em_id='$id_employee' AND eac_state='1'";
        $row = $this->ExecuteS($sql);
        if(count($row)>0)
        {
            foreach($row as $row)
            {
                $id_eac = $row['eac_id'];
            }
        }
        
        $sql = "UPDATE `employee_accessctrl` SET `eac_dateout`='$dateout', `eac_hourout`='$hourout', `eac_state`='0' WHERE eac_id='$id_eac'";
        $this->Execute($sql);
        
        return $dateout." ".$hourout;
    }

    public function editAccessCtrl()
    {
        $id = $this->postVars('id');
        $datein = $this->postVars('datein');
        $dateout = $this->postVars('dateout');
        $hourin = $this->postVars('hourin');
        $hourout = $this->postVars('hourout');
        $hw = $this->postVars('hw');
        $cod = $this->postVars('cod');
        $id_employee = $this->postVars('id_employee');
        
        $sql = "UPDATE `employee_accessctrl` SET `eac_datein`='$datein', `eac_dateout`='$dateout', `eac_hourin`='$hourin', `eac_hourout`='$hourout', `em_id`='$id_employee', `eac_hourworked`='$hw', `eac_codaux`='$cod' WHERE eac_id='$id'";
        
        $this->Execute($sql);
        
        /*if($dateout != '' && $hourout != '')
        {
            $this->calculateWorkedHours($id);
        }*/
    }

    public function editAccessCtrlHw()
    {
        $id = $this->postVars('id');
        $hw = $this->postVars('hw');
        
        $sql = "UPDATE `employee_accessctrl` SET `eac_hourworked`='$hw' WHERE eac_id='$id'";
        
        $this->Execute($sql);
        
    }

    public function editAccessCtrlDateIn()
    {
        $id = $this->postVars('id');
        $datein = $this->postVars('datein');
        
        $sql = "UPDATE `employee_accessctrl` SET `eac_datein`='$datein' WHERE eac_id='$id'";
        
        $this->Execute($sql);
        
    }

    public function editAccessCtrlHourIn()
    {
        $id = $this->postVars('id');
        $hourin = $this->postVars('hourin');
        
        $sql = "UPDATE `employee_accessctrl` SET `eac_hourin`='$hourin' WHERE eac_id='$id'";
        
        $this->Execute($sql);
        
    }

    public function editAccessCtrlDateOut()
    {
        $id = $this->postVars('id');
        $dateout = $this->postVars('dateout');
        
        $sql = "UPDATE `employee_accessctrl` SET `eac_dateout`='$dateout' WHERE eac_id='$id'";
        
        $this->Execute($sql);
        
    }

    public function editAccessCtrlHourOut()
    {
        $id = $this->postVars('id');
        $hourout = $this->postVars('hourout');
        
        $sql = "UPDATE `employee_accessctrl` SET `eac_hourout`='$hourout' WHERE eac_id='$id'";
        
        $this->Execute($sql);
        
    }

    public function register($code, $type){
        $lastRegisterBeforeNow = $this->getLastRegisterBeforeNow($code);
        $lastRegister = $this->getLastRegister($code);
        $employee = $this->getEmployeeByCode($code);
        $id_em = null;
        $name = null;
        $img = null;

        if(count($employee)>0)
        {
            foreach($employee as $elem)
            {
                $id_em = $elem['em_id'];
                $name = $elem['em_name'];
                $img = $elem['em_image'];
            }
        }

        $date = date('Y-m-d');
        $hourin = date('H:i:s');
        if($lastRegisterBeforeNow){
            if($this->ifTheRecordsAreUpToDate($lastRegisterBeforeNow) == 1){
                if($lastRegister){
                    if($lastRegister["eac_blockedcod"] == 1){
                        if($lastRegister["eac_codaux"] != null){
                            if($lastRegister["eac_codaux"] == $code){
                                $eac_id = $lastRegister["eac_id"];
        
                                $sql = "UPDATE `employee_accessctrl` SET `eac_dateout`='$date', `eac_hourout`= $hourin, `eac_blocked`= 0, `eac_codeaux`=null WHERE eac_id='$eac_id'";
                                $this->Execute($sql);
                                return ["success" => true, "message" => "Cuenta desbloqueada correctamente ya puedes registrar el ingreso"];
                            }else{
                                return ["success" => false, "message" => "El código de registro no coincide con el que te facilitó el administrador"];
                            }
                        }else{
                            return ["success" => false, "message" => "Por favor contacta a tu administrador para que te genere un nuevo código y puedas registrar la salida faltante"];
                        }
                    }else{
                        if($type == "in"){
                            $register = $this->newAccessCtrlIn($date, $hourin, $id_em);
        
                            $data = array(
                                "name" => $name,
                                "img" => $img,
                                "date" => $register
                            );
                    
                            if($register) return ["success" => true, "data" => $data];
                        }else if($type == "out"){
                            $eac_id = $lastRegister["eac_id"];
                            $register = $this->newAccessCtrlOut($date, $hourin, $id_em);
                            $this->calculateWorkedHours($eac_id);
        
                            $data = array(
                                "name" => $name,
                                "img" => $img,
                                "date" => $register
                            );
                    
                            if($register) return ["success" => true, "data" => $data];
                        }
                    }  
                }else{
                    $register = $this->newAccessCtrlIn($date, $hourin, $id_em);
                    $data = array(
                        "name" => $name,
                        "img" => $img,
                        "date" => $register
                    );
            
                    if($register) return ["success" => true, "data" => $data];
                }
            }else{
                $eac_id = $lastRegister["eac_id"];
                $sql = "UPDATE `employee_accessctrl` SET `eac_blocked`= 1, `eac_blockedcod`= 1 WHERE eac_id='$eac_id'";
                $this->Execute($sql);
                return ["success" => false, "data" => "", "message" => "Olvidó registrar la salida el dia ".$lastRegister["eac_datein"]." por este motivo debe solicitar al administrador un nuevo codido para poder registrar la salida faltante"];
            }
        }else{
            if($type == "in"){
                $register = $this->newAccessCtrlIn($date, $hourin, $id_em);

                $data = array(
                    "name" => $name,
                    "img" => $img,
                    "date" => $register
                );
        
                if($register) return ["success" => true, "data" => $data];
            }else if($type == "out"){
                $eac_id = $lastRegister["eac_id"];
                $register = $this->newAccessCtrlOut($date, $hourin, $id_em);
                $this->calculateWorkedHours($eac_id);

                $data = array(
                    "name" => $name,
                    "img" => $img,
                    "date" => $register
                );
        
                if($register) return ["success" => true, "data" => $data];
            }
        }
    }

    public function ifTheRecordsAreUpToDate($lastRegister){
        $date = date('Y-m-d');
        $date_register_time = strtotime($lastRegister["eac_datein"]);
        $date_time = strtotime($date);

        if($date_register_time < $date_time){
            if($lastRegister["eac_dateout"] != null || $lastRegister["eac_blockedcod"] == 1){
                return 1;
            }else{
                return 0;
            }
        }
    }

    public function getTypeRegister($code){
        $today = date('Y-m-d');
        $today_time = strtotime($today);
        $type = "in";

        $lastRegister = $this->getLastRegister($code);
        $employee = $this->getEmployeeByCode($code);
        if($employee){
            if($lastRegister){
                $date_register_time = strtotime($lastRegister["eac_datein"]);
                if($today_time > $date_register_time){
                    $type = "in";
                }else if($today_time == $date_register_time){
                    if($lastRegister["eac_datein"] != null && $lastRegister["eac_dateout"] == null){
                        $type = "out";
                    }else if($lastRegister["eac_datein"] == null && $lastRegister["eac_dateout"] == null){
                        $type = "in";
                    }else {
                        $type = "inout";
                    }
                }
        
                if($type == "in"){
                    return ["success" => true, "type" => $type, "message" => "Está seguro que quiere registrar la entrada?"];
                }else if($type == "out"){
                    return ["success" => true, "type" => $type, "message" => "Está seguro que quiere registrar la salida?"];
                }else{
                    return ["success" => false, "message" => "Usted ya ha regisrado la entrada y la salida el día de hoy"];
                }
            }else{
                return ["success" => true, "type" => $type, "message" => "Está seguro que quiere registrar la entrada?"];
            }
        }else{
            $date = date('Y-m-d');
            $hourin = date('H:i:s');
            $register = null;
            $sql = "SELECT * FROM `employee_accessctrl` WHERE eac_codaux='$code' ORDER BY eac_id DESC LIMIT 1";
            $lastRegister = $this->Execute($sql);
            
            if(count($lastRegister)>0){
                foreach($lastRegister as $row){
                    $register = $row;
                }   
            }
            if($register){
                $eac_id = $register["eac_id"];
                $sql = "UPDATE `employee_accessctrl` SET `eac_dateout`='$date', `eac_hourout`= '$hourin', `eac_blockedcod`= '0', `eac_codaux`=NULL WHERE eac_id='$eac_id'";
                $this->Execute($sql);
                return ["success" => true, "message" => "Cuenta desbloqueada correctamente ya puedes registrar el ingreso", "type" => "cod"];
            }else{
                return ["success" => false, "message" => "No existe ningun usuario con ese Código"];
            }
        }
    }

    public function getLastRegister($code){
        $row = $this->getEmployeeByCode($code);
        $id_em = null;
        $register = null;

        if(count($row)>0){
            foreach($row as $row){
                $id_em = $row['em_id'];
            }
        }

        $sql = "SELECT * FROM `employee_accessctrl` WHERE em_id='$id_em' ORDER BY eac_id DESC LIMIT 1";
        $lastRegister = $this->Execute($sql);

        if(count($lastRegister)>0){
            foreach($lastRegister as $row){
                $register = $row;
            }   
        }
        return $register;
    }

    public function getLastRegisterBeforeNow($code){
        $row = $this->getEmployeeByCode($code);
        $id_em = null;
        $register = null;

        if(count($row)>0){
            foreach($row as $row){
                $id_em = $row['em_id'];
            }
        }

        $today = date('Y-m-d');
        $sql = "SELECT * FROM `employee_accessctrl` WHERE em_id='$id_em' AND eac_datein != '$today' ORDER BY eac_id DESC LIMIT 1";
        $lastRegister = $this->Execute($sql);

        if(count($lastRegister)>0){
            foreach($lastRegister as $row){
                $register = $row;
            }   
        }
        return $register;
    }

    public function delAccessCtrl(){
        $id = $this->getVars('id');

        $sql = "DELETE FROM `employee_accessctrl` WHERE eac_id='$id'";
        $this->Execute($sql);
    }

    public function getAccessCtrl(){
        $sql = "SELECT * FROM `employee_accessctrl` ORDER BY eac_date DESC";
        return $this->ExecuteS($sql);
    }

    public function getAccessCtrlById($id){
        $sql = "SELECT * FROM `employee_accessctrl` WHERE eac_id='$id'";
        return $this->ExecuteS($sql);
    }

    public function getAccessCtrlByDateEmployee($id, $start, $end)
    {
        $start .= " 00:00:00";
        $end .= " 23:59:59";
        
        $sql = "SELECT * FROM `employee_accessctrl` WHERE em_id='$id' AND eac_date BETWEEN '$start' AND '$end' ORDER BY eac_date ASC";
        return $this->ExecuteS($sql);
    }

    public function getEmployeeDev($id, $start, $end, $hsalary)
    {
        $row = $this->getAccessCtrlByDateEmployee($id, $start, $end);
        if(count($row)>0)
        {
            foreach($row as $row)
            {
                $hw += $row['eac_hourworked'];
            }
        }
        
        return $hw * $hsalary;
    }

    public function getEmployeeHw($id, $start, $end)
    {
        $row = $this->getAccessCtrlByDateEmployee($id, $start, $end);
        if(count($row)>0)
        {
            foreach($row as $row)
            {
                $hw += $row['eac_hourworked'];
            }
        }
        
        return $hw;
    }

    public function hasRegistered($id)
    {
        $sql = "SELECT * FROM `employee_accessctrl` WHERE em_id='$id' AND eac_state='1'";
        $row = $this->ExecuteS($sql);
        if(count($row)>0)
        {
            return true;
        }
        else
        {
            return false;
        }
    }

    public function runAccessCtrl($code)
    {
        $row = $this->getEmployeeByCode($code);
        if(count($row)>0)
        {
            foreach($row as $row)
            {
                $id_em = $row['em_id'];
            }

            if(!$this->hasRegistered($id_em))
        {
            $datein = date('Y-m-d');
            $hourin = date('H:i:s');
            $register = $this->newAccessCtrlIn($datein, $hourin, $id_em);
            
            return ["type" => "in", "register" => $register, "success" => true];
        }
        else
        {
            $dateout = date('Y-m-d');
            $hourout = date('H:i:s');
            $id_eac = $this->newAccessCtrlOut($dateout, $hourout, $id_em);
            $this->calculateWorkedHours($id_eac);

            return ["type" => "out", "register" => $id_eac, "success" => true];
        }
        }else{
            return ["type" => "", "register" => "Usuario no encontrado", "success" => false];
        }
        
        
    }

    public function calculateWorkedHours($id)
    {
        $sql = "SELECT * FROM `employee_accessctrl` WHERE eac_id='$id'";
        $row = $this->ExecuteS($sql);
        if(count($row)>0)
        {
            foreach($row as $row)
            {
                $in = $row['eac_datein']." ".$row['eac_hourin'];
                $out = $row['eac_dateout']." ".$row['eac_hourout'];
                $id_em = $row['em_id'];
            }
        }
        
        $row = $this->getEmployeeById($id_em);
        if(count($row)>0)
        {
            foreach($row as $row)
            {
                $schedin = $row['em_schedin'];
                $schedout = $row['em_schedout'];
            }
        }
        
        
        if($this->getValueById(1) == '1')
        {
            $a = date_create($hora1);
            $b = date_create($hora2);
            $intervalo = date_diff($b, $a);
            $hw = (float) $intervalo->format('%h');  
        }
        else
        {
            $din = new DateTime($in);
            $dout = new DateTime($out);
            $interval = $din->diff($dout);
            $hw = (float) $interval->format('%H').".".$interval->format('%i');
            //$hw = ceil($hw);
        }
        
        $sql = "UPDATE `employee_accessctrl` SET `eac_hourworked`='$hw' WHERE eac_id='$id'";
        $row = $this->Execute($sql);
    }

    //Discounts Methods
    public function newDiscount()
    {
        $date = $this->postVars('date');
        $name = $this->postVars('name');
        $value = $this->postVars('value');
        $type = $this->postVars('type'); //1=descuento 2=Extras
        $id_employee = $this->postVars('id_employee');
        
        $sql = "INSERT INTO `employee_discounts`(`ed_date`, `ed_name`, `ed_desc`, `ed_value`, `em_id`, `ed_type`) VALUES ('$date','$name','$desc','$value','$id_employee','$type')";
        $this->Execute($sql);
    }

    public function editDiscount()
    {
        $id = $this->postVars('id_ed');
        $date = $this->postVars('date');
        $name = $this->postVars('name');
        $type = $this->postVars('type'); //1=descuento 2=Extras
        $value = $this->postVars('value');
        
        $sql = "UPDATE `employee_discounts` SET `ed_date`='$date', `ed_name`='$name', `ed_desc`='$desc', `ed_value`='$value', , `ed_type`='$type' WHERE ed_id='$id'";
        $this->Execute($sql);
    }

    public function delDiscount()
    {
        $id = $this->getVars('id_ed');
        
        $sql = "DELETE FROM `employee_discounts` WHERE ed_id='$id'";
        $this->Execute($sql);
    }

    public function getDiscountByEmployee($id, $type = 1)
    {
        $sql = "SELECT * FROM `employee_discounts` WHERE em_id='$id' AND ed_type='$type'";
        return $this->ExecuteS($sql);
    }

    public function getDiscountById($id)
    {
        $sql = "SELECT * FROM `employee_discounts` WHERE ed_id='$id'";
        return $this->ExecuteS($sql);
    }

    public function getDiscountByIntervalDates($id, $start, $end, $type = 1)
    {
        $sql = "SELECT * FROM `employee_discounts` WHERE em_id='$id' AND ed_type='$type' AND ed_date BETWEEN '$start' AND '$end'";
        return $this->ExecuteS($sql);
    }

    public function getTotalDiscountByIntervalDates($id, $start, $end, $type = 1)
    {
        $sql = "SELECT * FROM `employee_discounts` WHERE em_id='$id' AND ed_type='$type' AND ed_date BETWEEN '$start' AND '$end'";
        $row = $this->ExecuteS($sql);
        if(count($row)>0)
        {
            foreach($row as $row)
            {
                $t += $row['ed_value'];
            }
        }
        
        return $t;
    }

    //Params Methods
    public function editParam($id, $value)
    {
        $sql = "UPDATE `employee_params` SET `ep_value`='$value' WHERE `ep_id`='$id'";
        $this->Execute($sql);
    }

    public function getValueById($id)
    {
        $sql = "SELECT * FROM `employee_params` WHERE `ep_id`='$id'";
        $row = $this->ExecuteS($sql);
        if(count($row)>0)
        {
            foreach($row as $row)
            {
                return $row['ep_value'];
            }
        }
    }

    public function getParams()
    {
        $sql = "SELECT * FROM `employee_params`";
        return $this->ExecuteS($sql);
    }

        
        //Installers
    public function Checker(){
    return $this->chkTables('employee');
    }

    public function install(){
    $sql='CREATE TABLE `employee` (
    `em_id` int(11) NOT NULL,
    `em_name` varchar(255) NOT NULL,
    `em_birth` date NOT NULL,
    `em_addr` varchar(255) NOT NULL,
    `em_phone` varchar(255) NOT NULL,
    `em_code` varchar(255) NOT NULL,
    `em_indate` date NOT NULL,
    `em_outdate` date NOT NULL,
    `em_image` varchar(255) NOT NULL,
    `em_profile` longtext NOT NULL,
    `em_salary` varchar(255) NOT NULL,
    `em_hsalary` varchar(255) NOT NULL,
    `em_active` int(11) NOT NULL,
    `em_state` int(11) NOT NULL,
    `em_email` varchar(255) NOT NULL,
    `em_position` varchar(255) NOT NULL,
    `u_id` varchar(100) NOT NULL,
    `c_id` varchar(255) NOT NULL,
    `em_place` varchar(255) NOT NULL
    ) ENGINE=MyISAM DEFAULT CHARSET=latin1;';
        $this->Execute($sql);
        
        $sql='CREATE TABLE `credential` (
    `c_id` int(11) NOT NULL,
    `c_name` varchar(255) NOT NULL,
    `c_image` varchar(255) NOT NULL,
    `c_desc` longtext NOT NULL
    ) ENGINE=MyISAM DEFAULT CHARSET=latin1;';
        $this->Execute($sql);
        
        $sql='CREATE TABLE `employee_accessctrl` (
    `eac_id` int(11) NOT NULL,
    `eac_date` datetime NOT NULL,
    `eac_datein` date NOT NULL,
    `eac_dateout` date NOT NULL,
    `eac_hourin` varchar(255) NOT NULL,
    `eac_hourout` varchar(255) NOT NULL,
    `eac_hourworked` double NOT NULL,
    `eac_state` varchar(255) NOT NULL,
    `em_id` int(11) NOT NULL
    ) ENGINE=MyISAM DEFAULT CHARSET=latin1;';
        $this->Execute($sql);
        
        $sql='CREATE TABLE `employee_discounts` (
    `ed_id` int(11) NOT NULL,
    `ed_date` date NOT NULL,
    `ed_name` varchar(255) NOT NULL,
    `ed_desc` longtext NOT NULL,
    `ed_value` varchar(255) NOT NULL,
    `ed_state` int(11) NOT NULL,
    `em_id` int(11) NOT NULL
    ) ENGINE=MyISAM DEFAULT CHARSET=latin1;';
        $this->Execute($sql);
        
        $sql = "ALTER TABLE `employee` ADD `em_schedulein` VARCHAR(255) NOT NULL AFTER `em_place`;";
        $this->Execute($sql);
        $sql = "ALTER TABLE `employee` ADD `em_scheduleout` VARCHAR(255) NOT NULL AFTER `em_schedulein`;";
        $this->Execute($sql);
        
        $sql='CREATE TABLE `employee_params` (
    `ep_id` int(11) NOT NULL PRIMARY KEY AUTO_INCREMENT,
    `ep_name` varchar(255) NOT NULL,
    `ep_value` varchar(255) NOT NULL
    ) ENGINE=MyISAM DEFAULT CHARSET=latin1;';
        $this->Execute($sql);
        
        $sql = "INSERT INTO `employee_params` (`ep_id`, `ep_name`, `ep_value`) VALUES (NULL, 'Calculo de Horas (1)=por Horario de trabajo, (2)=por tiempo corrido', '1');";
        $this->Execute($sql);
        
        $sql = "ALTER TABLE `employee_discounts` ADD `ed_type` INT NOT NULL AFTER `em_id`;";
        $this->Execute($sql);
        
        
    }

    public function uninstall(){
    $sql='DROP TABLE `employee`, `employee_params`';
        $this->Execute($sql);
        
        $sql='DROP TABLE `credential`';
        $this->Execute($sql);
        
        $sql='DROP TABLE `employee_accessctrl`';
        $this->Execute($sql);
        
        $sql='DROP TABLE `employee_discounts`';
        $this->Execute($sql);

    }
        
    };
?>