<?php
include("modules/employee/view/includes.php");
 
 
 $msg=false;
 
 //Acciones
 
 if($obj->getVars('ActionDel')==true){
  $obj-> delCredential();
  $msg=true;
 }
 
 ?>
 <div class="widget3">
 <div class="widgetlegend">Credenciales </div>
 <?php
  if($msg)
  {
  ?>
   <div class="ui-widget">
	<div class="ui-state-highlight ui-corner-all" style="margin-top: 20px; padding: 0 .7em;">
		<p><span class="ui-icon ui-icon-info" style="float: left; margin-right: .3em;"></span>
		<strong>Felicitaciones!</strong> Se ha sido eliminado satisfactoriamente.</p>
	</div>
</div>
  <?php
  }
 ?>
<p style="width:100">
	<a href="<?php $_SERVER['PHP_SELF'];?>?p=modules/employee/view/newCredential.php" class="btn_normal" style="float:left; margin:5px;">Nuevo </a>
	<a href="<?php $_SERVER['PHP_SELF'];?>?p=modules/employee/view/index.php" class="btn_normal" style="float:left; margin:5px;">Volver </a>
</p>
 
<table width="803" height="48" border="0">
  <tr>
    <th width="30">ID</th>
    <th width="142">Imagen</th>
    <th width="213">Nombre</th>
    <th colspan="2">Acciones</th>
  </tr>
  <?php
   $row=$obj->getCredential();
   
   if(count($row)>0){
 	foreach($row as $row){
  ?>
  <tr>
    <td><?php echo $row["c_id"];?></td>
	<td><img src="modules/employee/credentialPhoto/<?php echo $row["c_image"];?>" width="200" height="100" /></td>
	<td><?php echo $row["c_name"];?></td>
    <td width="86"><a href="<?php $_SERVER['PHP_SELF'];?>?p=modules/employee/view/editCredential.php&id=<?php echo $row["c_id"]?>" class="btn_normal">Editar</a></td>
    <td width="81"><a href="<?php $_SERVER['PHP_SELF'];?>?p=modules/employee/view/showCredentials.php&id=<?php echo $row["c_id"]?>&ActionDel=true" class="btn_borrar">Eliminar</a></td>
  </tr>
  <?php
   }
  } 
  ?>
</table>

</div>


