<?php
 require("../barcode/barcode.php");
 require("../barcode/i25object.php");
 require("../barcode/c39object.php");
 require("../barcode/c128aobject.php");
 require("../barcode/c128bobject.php");
 require("../barcode/c128cobject.php");
 
 /* Default value */
if (!isset($output))  $output   = "png";
if (!isset($type))    $type     = "C128B";
if (!isset($widthb))   $widthb    = "350";
if (!isset($heightb))  $heightb   = "50";
if (!isset($xres))    $xres     = "2";
if (!isset($font))    $font     = "2";

$drawtext="on";
$stretchtext="on";

$style |= ($output  == "png" ) ? BCS_IMAGE_PNG  : 0;
$style |= ($output  == "jpeg") ? BCS_IMAGE_JPEG : 0;
$style |= ($border  == "off"  ) ? BCS_BORDER           : 0;
$style |= ($drawtext== "off"  ) ? BCS_DRAW_TEXT  : 0;
$style |= ($stretchtext== "on" ) ? BCS_STRETCH_TEXT  : 0;
$style |= ($negative== "on"  ) ? BCS_REVERSE_COLOR  : 0;
?>
