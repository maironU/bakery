<?php
 include("modules/employee/view/includes.php");

 
 $msg=false;
 
 if($_POST){
  $obj->editDiscount();
  $msg=true;
 }
 
 $id = $obj->getVars('id');
 $id_ed = $obj->getVars('id_ed');
 
 $row = $obj->getDiscountById($id_ed);
 foreach($row as $row)
 {
     $name = $row['ed_name'];
     $desc = $row['ed_desc'];
     $value = $row['ed_value'];
     $date = $row['ed_date'];
     $type = $row['ed_type'];
 }

?>
<script>
    var $datepicker = $('.datepicker');
    $datepicker.datepicker();
    $datepicker.datepicker('setDate', <?php echo $date?>);
</script>
<div class="widget3">
 <div class="widgetlegend">Editar Descuento </div>
 <?php
  if($msg)
  {
  ?>
   <div class="ui-widget">
	<div class="ui-state-highlight ui-corner-all" style="margin-top: 20px; padding: 0 .7em;">
		<p><span class="ui-icon ui-icon-info" style="float: left; margin-right: .3em;"></span>
		<strong>Felicitaciones!</strong> Se ha sido creado satisfactoriamente.</p>
	</div>
</div>
  <?php
  }
 ?>
<p style="width:100">
	<a href="<?php $_SERVER['PHP_SELF'];?>?p=modules/employee/view/showDiscount.php&id=<?php echo $id?>" class="btn_normal" style="float:left; margin:5px;">Volver </a>
</p>
 
<form action="#" method="post" enctype="multipart/form-data" name="form1" id="form1">
<table width="803" height="48" border="0" style="float:left">
  <tr>
    <td><label>Nombre: </label>
      <br />
      <input name="name" type="text" id="name" value="<?php echo $name?>" required/>   </td>
  </tr>
  <tr>
    <td><label>Descripcion: </label>
      <br />
      <textarea cols="40" rows="5" name="desc"><?php echo $desc?></textarea></td>
  </tr>
  <tr>
    <td><label>Valor: </label>
      <br />
      <input name="value" type="text" id="value" value="<?php echo $value?>" required/>   </td>
  </tr>
  <tr>
    <td><label>Fecha: </label>
      <br />
      <input name="date" type="text" id="date" class="datepicker" required/>   </td>
  </tr>
  <tr>
    <td><label>Tipo: </label>
      <br />
      
      <?php
        if($type == 1)
        {
            ?>
            Descuento: <input name="type" type="radio" id="type" value="1" checked required/>&nbsp;
      Extras: <input name="type" type="radio" id="type" value="2" required/>
            <?php
        }
        elseif($type == 2)
        {
            ?>
            Descuento: <input name="type" type="radio" id="type" value="1" required/>&nbsp;
      Extras: <input name="type" type="radio" id="type" value="2" checked required/>
            <?php
        }
        else
        {
            ?>
            Descuento: <input name="type" type="radio" id="type" value="1" required/>&nbsp;
      Extras: <input name="type" type="radio" id="type" value="2" required/>
            <?php
        }
      ?>
      
      </td>
  </tr>
  <tr>
    <td>
        <input name="id" type="hidden" id="id" value="<?php echo $id?>" />
        <input name="id_ed" type="hidden" id="id" value="<?php echo $id_ed?>" />
        <input type="submit" value="Guardar" class="btn_submit"/></td>
  </tr>
</table>
</form>


</div>
