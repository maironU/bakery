<?php
 include("modules/employee/view/includes.php");
 
 $msg=false;
 
 //Acciones
 if($obj->getVars('actionDel') == 'true')
 {
    $obj->delAccessCtrl();
    $_SESSION['flash_message'] = "Se ha sido eliminado satisfactoriamente.";
 }
 
 ?>
 <script type="text/javascript" src="modules/player/js/jsPlayerFunctions.js"></script>
<!-- Add mousewheel plugin (this is optional) -->
<script type="text/javascript" src="modules/player/js/jquery.mousewheel-3.0.6.pack.js"></script>
<!-- Add fancyBox main JS and CSS files -->
<script type="text/javascript" src="modules/player/js/jquery.fancybox.js?v=2.1.4"></script>
<link rel="stylesheet" type="text/css" href="modules/player/css/jquery.fancybox.css?v=2.1.4" media="screen" />

<div class="card shadow mb-4">
 <div class="widgetlegend" style="margin: 1rem">Control de Acceso </div>
 <?php
  if(isset($_SESSION['flash_message']))
  {
  ?>
    <div class="alert alert-success alert-dismissible fade show" style="margin: 1rem" role="alert">
      <strong>Felicitaciones!</strong> <?php echo $_SESSION['flash_message'] ?>
      <button type="button" class="close" data-dismiss="alert" aria-label="Close">
        <span aria-hidden="true">&times;</span>
      </button>
    </div>
  <?php
    unset($_SESSION['flash_message']);
  }
 ?>
  <div class="card-header py-3">
    <a class="btn bg-color-bottom text-white" href="<?php $_SERVER['PHP_SELF'];?>?p=modules/employee/view/index.php" class="btn_normal" style="float:left; margin:5px;">Volver </a>
	<a class="btn bg-color-bottom text-white" href="<?php $_SERVER['PHP_SELF'];?>?p=modules/employee/view/newAccessCtrl.php" class="btn_normal" style="float:left; margin:5px;">Nuevo </a>
	<a class="btn bg-color-bottom text-white" href="<?php $_SERVER['PHP_SELF'];?>?p=modules/employee/view/editAccesCtrlMasive.php" class="btn_normal" style="float:left; margin:5px;">Masivo </a>
  </div>
  <div class="card-body">
      <div class="table-responsive">
          <table class="table table-bordered" id="dataTable" width="100%" cellspacing="0">
            <thead>
                <tr class="bg-white">
                <th>ID</th>
                <th>Fecha Registro</th>
                <th>Empleado</th>
                <th>Entrada</th>
                <th>Salida</th>
                <th>Horas/Trabajadas,</th>
                <th colspan="2">Acciones</th>
                </tr>
            </thead>
            <tfoot>
                <tr class="bg-white">
                <th>ID</th>
                <th>Fecha Registro</th>
                <th>Empleado</th>
                <th>Entrada</th>
                <th>Salida</th>
                <th>Horas/Trabajadas,</th>
                <th colspan="2">Acciones</th>
                </tr>
            </tfoot>
            <tbody>
                <?php
                    $row = $obj->getAccessCtrl();
                    if(count($row)>0)
                    {
                        foreach($row as $row)
                        {
                            ?>
                            <tr>
                                <td><?php echo $row['eac_id']?></td>
                                <td><?php echo $row['eac_date']?></td>
                                <td><?php $row1 = $obj->getEmployeeById($row['em_id']);
                                    if(count($row1)>0)
                                    {
                                        foreach($row1 as $row1)
                                        {
                                            echo $row1['em_name'];
                                            
                                        }
                                    }
                                ?></td>
                                <td><?php echo $row['eac_datein']." ".$row['eac_hourin']?></td>
                                <td><?php echo $row['eac_dateout']." ".$row['eac_hourout']?></td>
                                <td><?php echo $row['eac_hourworked']?></td>
                                <td><a class="btn bg-color-bottom text-white" href="<?php $_SERVER['PHP_SELF'];?>?p=modules/employee/view/editAccessCtrl.php&id=<?php echo $row["eac_id"]?>" class="btn_normal">Editar</a></td>
                                <td><a class="btn btn-danger" href="<?php $_SERVER['PHP_SELF'];?>?p=modules/employee/view/showAccessCtrl.php&id=<?php echo $row["eac_id"]?>&actionDel=true" class="btn_borrar">Eliminar</a></td>
                            </tr>
                            <?php
                        }
                    }
                ?>
            </tbody>
          </table>
        </div>
    </div>
</div>
<script>
 function showInfo(id,site, id_customer)
 {
	$.fancybox.open({
					href : site+'?id='+id+'&id_customer='+id_customer,
					type : 'iframe',
					padding:2,
					'afterShow': function() {
						
        				$('#fancybox-frame').contents().find("#barcode").focus();
						//document.frm1.barcode.focus();
					},
					'afterClose' : function(){
					 	//parent.location.reload(true);
					}
					
	}); 
 }
 
 function PrintElem(elem)
{
    var mywindow = window.open('', 'PRINT', 'height=400,width=800');

    mywindow.document.write('<html><head><meta http-equiv="Content-Type" content="text/html; charset=utf-8"><title>Reporte de Pago</title>');
    mywindow.document.write('</head><body >');
    mywindow.document.write('<h1>' + document.title  + '</h1>');
    mywindow.document.write(document.getElementById(elem).innerHTML);
    mywindow.document.write('</body></html>');

    mywindow.document.close(); // necessary for IE >= 10
    mywindow.focus(); // necessary for IE >= 10*/

    mywindow.print();
    mywindow.close();

    return true;
}
</script>


