<?php
 include("modules/employee/view/includes.php");
 
 $id=$obj->getVars('id');
 
 $msg=false;
 
 //Acciones
 
 
 ?>
 <script type="text/javascript" src="modules/player/js/jsPlayerFunctions.js"></script>
<!-- Add mousewheel plugin (this is optional) -->
<script type="text/javascript" src="modules/player/js/jquery.mousewheel-3.0.6.pack.js"></script>
<!-- Add fancyBox main JS and CSS files -->
<script type="text/javascript" src="modules/player/js/jquery.fancybox.js?v=2.1.4"></script>
<link rel="stylesheet" type="text/css" href="modules/player/css/jquery.fancybox.css?v=2.1.4" media="screen" />
 <div class="widget3">
 <div class="widgetlegend">Liquidacion de Pago Detallado</div>
 <?php
  if($msg)
  {
  ?>
   <div class="ui-widget">
	<div class="ui-state-highlight ui-corner-all" style="margin-top: 20px; padding: 0 .7em;">
		<p><span class="ui-icon ui-icon-info" style="float: left; margin-right: .3em;"></span>
		<strong>Felicitaciones!</strong> Se ha sido eliminado satisfactoriamente.</p>
	</div>
</div>
  <?php
  }
 ?>
<p style="width:100">
	<a href="<?php $_SERVER['PHP_SELF'];?>?p=modules/employee/view/index.php" class="btn_normal" style="float:left; margin:5px;">Volver </a>
</p>
<br><br><br>
<form action="#" method="post">
    <table>
        <tr>
            <td><input type="text" name="start" class="datepicker" placeholder="Fecha Inicio" required /></td>
            <td><input type="text" name="end" class="datepicker2" placeholder="Fecha Fin" required /></td>
            <td>
                <input type="hidden" value="<?php echo $id?>" name="id" />
                <input type="submit" value="Liquidar" class="btn_submit" /></td>
        </tr>
    </table>
</form>
<br><br><br>
<?php
    if($_POST)
    {
        $start = $obj->postVars('start');
        $end = $obj->postVars('end');
        $id = $obj->postVars('id');
        
        //Valor horas empleado
        $row = $obj->getEmployeeById($id);
        if(count($row)>0)
        {
            foreach($row as $row)
            {
                $hsalary = $row['em_hsalary'];
            }
        }
        
        ?>
        <table width="100%">
            <tr>
                <th>Horas Trabajas</th>
                <th>Descuentos</th>
                <th>Extras</th>
            </tr>
            <tr>
                <td valign="top">
                    <table>
                        <tr>
                            <th>Fecha de registro</th>
                            <th>Entrada</th>
                            <th>Salida</th>
                            <th>Horas trabajadas</th>
                            <th>Total a pagar</th>
                        </tr>
                        <?php
                            $row = $obj->getAccessCtrlByDateEmployee($id, $start, $end);
                            if(count($row)>0)
                            {
                                foreach($row as $row)
                                {
                                    ?>
                                    <tr>
                                        <td><?php echo $row['eac_date']?></td>
                                        <td><?php echo $row['eac_datein']." ".$row['eac_hourin']?></td>
                                        <td><?php echo $row['eac_dateout']." ".$row['eac_hourout']?></td>
                                        <td><?php echo $row['eac_hourworked']?></td>
                                        <td>$<?php echo number_format($row['eac_hourworked']*$hsalary,2)?></td>
                                    </tr>
                                    <?php
                                    $sum_hw += $row['eac_hourworked'];
                                    $sum_dev += $row['eac_hourworked']*$hsalary;
                                }
                            }
                        ?>
                    </table>
                </td>
                <td valign="top">
                    <table>
                        <tr>
                            <th>ID</th>
                            <th>Nombre</th>
                            <th>Descripcion</th>
                            <th>Valor</th>
                        </tr>
                        <?php
                            $row = $obj->getDiscountByIntervalDates($id, $start, $end, 1);
                            if(count($row)>0)
                            {
                                foreach($row as $row)
                                {
                                    ?>
                                    <tr>
                                        <td><?php echo $row['ed_id']?></td>
                                        <td><?php echo $row['ed_name']?></td>
                                        <td><?php echo $row['ed_desc']?></td>
                                        <td>$<?php echo number_format($row['ed_value'],2)?></td>
                                    </tr>
                                    <?php
                                    $sum_dis += $row['ed_value'];
                                }
                            }
                        ?>
                    </table>
                </td>
                <td valign="top">
                    <table>
                        <tr>
                            <th>ID</th>
                            <th>Nombre</th>
                            <th>Descripcion</th>
                            <th>Valor</th>
                        </tr>
                        <?php
                            $row = $obj->getDiscountByIntervalDates($id, $start, $end, 2);
                            if(count($row)>0)
                            {
                                foreach($row as $row)
                                {
                                    ?>
                                    <tr>
                                        <td><?php echo $row['ed_id']?></td>
                                        <td><?php echo $row['ed_name']?></td>
                                        <td><?php echo $row['ed_desc']?></td>
                                        <td>$<?php echo number_format($row['ed_value'],2)?></td>
                                    </tr>
                                    <?php
                                    $sum_extra += $row['ed_value'];
                                }
                            }
                        ?>
                    </table>
                </td>
            </tr>
        </table>
        <br>
        <table>
            <tr>
                <th>Horas Trabajas</th>
                <th>Total Devengado</th>
                <th>Total Descuento</th>
                <th>Total Extras</th>
                <th>Neto a Pagar</th>
            </tr>
            <tr>
                <td><h2><?php echo $sum_hw?></h2></td>
                <td><h2>$<?php echo number_format($sum_dev,2)?></h2></td>
                <td><h2>$<?php echo number_format($sum_dis,2)?></h2></td>
                <td><h2>$<?php echo number_format($sum_extra,2)?></h2></td>
                <td><h2>$<?php echo number_format($sum_dev-$sum_dis+$sum_extra,2)?></h2></td>
            </tr>
        </table>
        <?php
    }
?>
</div>
<script>
 function showInfo(id,site, id_customer)
 {
	$.fancybox.open({
					href : site+'?id='+id+'&id_customer='+id_customer,
					type : 'iframe',
					padding:2,
					'afterShow': function() {
						
        				$('#fancybox-frame').contents().find("#barcode").focus();
						//document.frm1.barcode.focus();
					},
					'afterClose' : function(){
					 	//parent.location.reload(true);
					}
					
	}); 
 }
</script>


