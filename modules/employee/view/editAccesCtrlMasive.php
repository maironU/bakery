<?php
 include("modules/employee/view/includes.php");
 
 $id=$obj->getVars('id');
 
 $msg=false;
 
 //Acciones
 
 
 ?>
 <script type="text/javascript" src="modules/player/js/jsPlayerFunctions.js"></script>
<!-- Add mousewheel plugin (this is optional) -->
<script type="text/javascript" src="modules/player/js/jquery.mousewheel-3.0.6.pack.js"></script>
<!-- Add fancyBox main JS and CSS files -->
<script type="text/javascript" src="modules/player/js/jquery.fancybox.js?v=2.1.4"></script>
<link rel="stylesheet" type="text/css" href="modules/player/css/jquery.fancybox.css?v=2.1.4" media="screen" />
 <div class="widget3">
 <div class="widgetlegend">Edicion horas masiva</div>
 <?php
  if($msg)
  {
  ?>
   <div class="ui-widget">
	<div class="ui-state-highlight ui-corner-all" style="margin-top: 20px; padding: 0 .7em;">
		<p><span class="ui-icon ui-icon-info" style="float: left; margin-right: .3em;"></span>
		<strong>Felicitaciones!</strong> Se ha sido eliminado satisfactoriamente.</p>
	</div>
</div>
  <?php
  }
 ?>
<p style="width:100">
	<a href="<?php $_SERVER['PHP_SELF'];?>?p=modules/employee/view/index.php" class="btn_normal" style="float:left; margin:5px;">Volver </a>
</p>
<br><br><br>
<form action="#" method="post">
    <table>
        <tr>
            <td><input type="text" name="start" class="datepicker" placeholder="Fecha Inicio" required /></td>
            <td><input type="text" name="end" class="datepicker2" placeholder="Fecha Fin" required /></td>
            <td>
                <input type="hidden" value="<?php echo $id?>" name="id" />
                <input type="submit" value="Liquidar" class="btn_submit" /></td>
        </tr>
    </table>
</form>
<br><br><br>
<div id="paymentReport">
<?php
    if($_POST)
    {
        $start = $obj->postVars('start');
        $end = $obj->postVars('end');
        
        $row = $obj->getEmployeeActive();
        if(count($row)>0)
        {
            foreach($row as $row)
            {
                ?>
                
                <table width="100%">
                    <tr>
                        <td>Empleado: <?php echo $row['em_name']?>
                            <br>Lugar de Trabajo: <?php echo $row['em_place']?>
                            <br>Hora de Entrada: <?php echo $row['em_schedulein']?>
                            <br>Hora de Salida: <?php echo $row['em_scheduleout']?>
                        </td>
                    </tr>
                    <tr>
                        <td>
                            <table>
                                <tr>
                                    <td colspan="5"><hr></td>
                                </tr>
                                <tr>
                                    <th>Devengado</th>
                                    <th>Descuentos</th>
                                    <th>Extras</th>
                                    <th>Valor Hora</th>
                                    <th>Total horas Trabajadas</th>
                                    <th>Total a Pagar</th>
                                </tr>
                                <tr>
                                    <td>$<?php
                                        $sum_dev = $obj->getEmployeeDev($row['em_id'], $start, $end, $row['em_hsalary']);
                                        echo number_format($sum_dev,2);
                                    ?></td>
                                    <td>$<?php
                                        $sum_dis = $obj->getTotalDiscountByIntervalDates($row['em_id'], $start, $end, 1);
                                        echo number_format($sum_dis,2);
                                    ?></td>
                                    <td>$<?php
                                        $sum_extras = $obj->getTotalDiscountByIntervalDates($row['em_id'], $start, $end, 2);
                                        echo number_format($sum_extras,2);
                                    ?></td>
                                    <td>$<?php echo number_format($row['em_hsalary'],2)?></td>
                                    <td><?php echo $obj->getEmployeeHw($row['em_id'], $start, $end);?></td>
                                    <td>$<?php echo number_format($sum_dev-$sum_dis+$sum_extras,2)?></td>
                                </tr>
                            </table>
                        </td>
                    </tr>
                    <tr>
                        <td>
                            <table>
                                <tr>
                                    <th>Fecha de registro</th>
                                    <th>Entrada</th>
                                    <th>Salida</th>
                                    <th>Horas trabajadas</th>
                                </tr>
                                <?php
                                    $row = $obj->getAccessCtrlByDateEmployee($row['em_id'], $start, $end);
                                    if(count($row)>0)
                                    {
                                        foreach($row as $row)
                                        {
                                            ?>
                                            <tr>
                                                <td><?php echo $row['eac_date']?></td>
                                                <td><input type="text" name="datein" id="datein<?php echo $row['eac_id']?>" value="<?php echo $row['eac_datein']?>" onChange="editDatein(<?php echo $row['eac_id']?>, this.value)" />&nbsp;<input type="text" name="hourin" id="hourin<?php echo $row['eac_id']?>" value="<?php echo $row['eac_hourin']?>" onChange="editHourin(<?php echo $row['eac_id']?>, this.value)" /></td>
                                                <td><input type="text" name="dateout" id="dateout<?php echo $row['eac_id']?>" value="<?php echo $row['eac_dateout']?>" onChange="editDateout(<?php echo $row['eac_id']?>, this.value)" />&nbsp;<input type="text" name="hourout" id="hourout<?php echo $row['eac_id']?>" value="<?php echo $row['eac_hourout']?>" onChange="editHourout(<?php echo $row['eac_id']?>, this.value)" /></td>
                                                <td><input type="text" name="hw" id="hw<?php echo $row['eac_id']?>" value="<?php echo $row['eac_hourworked']?>" onChange="editHw(<?php echo $row['eac_id']?>, this.value)" /></td>
                                            </tr>
                                            <?php
                                        }
                                    }
                                ?>
                            </table>
                        </td>
                    </tr>
                </table>
                
                <?php
            }
        }
    }
?>
</div>
</div>
<script>
 function showInfo(id,site, id_customer)
 {
	$.fancybox.open({
					href : site+'?id='+id+'&id_customer='+id_customer,
					type : 'iframe',
					padding:2,
					'afterShow': function() {
						
        				$('#fancybox-frame').contents().find("#barcode").focus();
						//document.frm1.barcode.focus();
					},
					'afterClose' : function(){
					 	//parent.location.reload(true);
					}
					
	}); 
 }
 
 function PrintElem(elem)
{
    var mywindow = window.open('', 'PRINT', 'height=400,width=800');

    mywindow.document.write('<html><head><title>Reporte de Pago</title>');
    mywindow.document.write('</head><body style=\'font-family: Arial; font-size: 12px\'>');
    mywindow.document.write('<h1>' + document.title  + '</h1>');
    mywindow.document.write(document.getElementById(elem).innerHTML);
    mywindow.document.write('</body></html>');

    mywindow.document.close(); // necessary for IE >= 10
    mywindow.focus(); // necessary for IE >= 10*/

    mywindow.print();
    mywindow.close();

    return true;
}

function editHw(id, hw){
	$.ajax({
	 type:'POST',
	 url:'modules/employee/view/editHw.php',
	 async:false,
	 data: 'id='+id+"&hw="+hw,
	 success:function(data){
		 if(data == 'ok')
		 {
		     document.getElementById('hw'+id).style.background="#0f0";
		 }
		 else
		 {
		     document.getElementById('hw'+id).style.background="#f00";
		     alert(data);
		 }
	 }
	});
 }
 
 function editDatein(id, datein){
	$.ajax({
	 type:'POST',
	 url:'modules/employee/view/editDatein.php',
	 async:false,
	 data: 'id='+id+"&datein="+datein,
	 success:function(data){
		 if(data == 'ok')
		 {
		     document.getElementById('datein'+id).style.background="#0f0";
		 }
		 else
		 {
		     document.getElementById('datein'+id).style.background="#f00";
		     alert(data);
		 }
	 }
	});
 }
 
 function editHourin(id, hourin){
	$.ajax({
	 type:'POST',
	 url:'modules/employee/view/editHourin.php',
	 async:false,
	 data: 'id='+id+"&hourin="+hourin,
	 success:function(data){
		 if(data == 'ok')
		 {
		     document.getElementById('hourin'+id).style.background="#0f0";
		 }
		 else
		 {
		     document.getElementById('hourin'+id).style.background="#f00";
		     alert(data);
		 }
	 }
	});
 }
 
 function editDateout(id, dateout){
	$.ajax({
	 type:'POST',
	 url:'modules/employee/view/editDateout.php',
	 async:false,
	 data: 'id='+id+"&dateout="+dateout,
	 success:function(data){
		 if(data == 'ok')
		 {
		     document.getElementById('dateout'+id).style.background="#0f0";
		 }
		 else
		 {
		     document.getElementById('dateout'+id).style.background="#f00";
		     alert(data);
		 }
	 }
	});
 }
 
 function editHourin(id, hourout){
	$.ajax({
	 type:'POST',
	 url:'modules/employee/view/editHourout.php',
	 async:false,
	 data: 'id='+id+"&hourout="+hourout,
	 success:function(data){
		 if(data == 'ok')
		 {
		     document.getElementById('hourout'+id).style.background="#0f0";
		 }
		 else
		 {
		     document.getElementById('hourout'+id).style.background="#f00";
		     alert(data);
		 }
	 }
	});
 }
</script>


