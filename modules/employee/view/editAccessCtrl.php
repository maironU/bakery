<?php
 include("modules/employee/view/includes.php");

 
 $msg=false;
 
 if($_POST){
  $obj->editAccessCtrl();
  $msg=true;
 }
 
 $id = $obj->getVars('id');
 
 $row = $obj->getAccessCtrlById($id);
 foreach($row as $row)
 {
     $datein = $row['eac_datein'];
     $dateout = $row['eac_dateout'];
     $hourin = $row['eac_hourin'];
     $hourout = $row['eac_hourout'];
     $hw = $row['eac_hourworked'];
     $id_em = $row['em_id'];
     $cod = $row['eac_codaux'];
 }

?>
<script>
    var $datepicker = $('.datepicker');
    $datepicker.datepicker();
    $datepicker.datepicker('setDate', <?php echo $datein?>);
    
    var $datepicker2 = $('.datepicker2');
    $datepicker2.datepicker();
    $datepicker2.datepicker('setDate', <?php echo $dateout?>);
</script>
<div class="widget3">
 <div class="widgetlegend">Editar Control de Acceso </div>
 <?php
  if($msg)
  {
  ?>
   <div class="ui-widget">
	<div class="ui-state-highlight ui-corner-all" style="margin-top: 20px; padding: 0 .7em;">
		<p><span class="ui-icon ui-icon-info" style="float: left; margin-right: .3em;"></span>
		<strong>Felicitaciones!</strong> Se ha sido creado satisfactoriamente.</p>
	</div>
</div>
  <?php
  }
 ?>
<p style="width:100">
	<a href="<?php $_SERVER['PHP_SELF'];?>?p=modules/employee/view/showAccessCtrl.php" class="btn_normal" style="float:left; margin:5px;">Volver </a>
</p>
 
<form action="#" method="post" enctype="multipart/form-data" name="form1" id="form1">
<table width="803" height="48" border="0" style="float:left">
  <tr>
    <td><label>Fecha de Entrada: </label>
      <br />
      <input name="datein" type="text" id="datein" value="<?php echo $datein?>" required/>   </td>
  </tr>
  <tr>
    <td><label>Hora de Entrada: </label>
      <br />
      <input name="hourin" type="text" id="hourin" placeholder="23:59:59" value="<?php echo $hourin?>" required/>   </td>
  </tr>
  <tr>
    <td><label>Fecha de Salida: </label>
      <br />
      <input name="dateout" type="text" id="dateout" value="<?php echo $dateout?>"/>   </td>
  </tr>
  <tr>
    <td><label>Hora de Salida: </label>
      <br />
      <input name="hourout" type="text" id="hourout" placeholder="23:59:59" value="<?php echo $hourout?>"/>   </td>
  </tr>
  <tr>
    <td><label>Total Horas Trabajada: </label>
      <br />
      <input name="hw" type="text" value="<?php echo $hw?>"/>   </td>
  </tr>
  <tr>
    <td><label>Codigo para registrar salida: </label>
      <br />
      <div>
        <input type="text" name="cod" id="cod" value="<?php echo $cod ?>">
        <i id="gen-cod" class="fa fa-refresh" aria-hidden="true" title="Generar codigo aleatorio" style="cursor: pointer" onclick="genCode()"></i>
      </div>
  </tr>
  <tr>
    <td><label>Empleado: </label>
      <br />
     <select name="id_employee" required>
         <option value="">--Seleccionar--</option>
         <?php
            $row = $obj->getEmployeeActive();
            if(count($row) > 0)
            {
                foreach($row as $row)
                {
                    if($id_em == $row['em_id'])
                    {
                       ?>
                    <option value="<?php echo $row['em_id']?>" selected><?php echo $row['em_name']?></option>
                    <?php 
                    }
                    else
                    {
                        ?>
                    <option value="<?php echo $row['em_id']?>"><?php echo $row['em_name']?></option>
                    <?php
                    }
                    
                }
            }
         ?>
     </select>
     </td>
  </tr>
  <tr>
    <td>
        <input type="hidden" name="id" value="<?php echo $id?>" />
        <input type="submit" value="Guardar" class="btn_submit"/></td>
  </tr>
</table>
</form>


</div>

<script>
  function genCode(){
    const characters ='ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789';
    let result1= ' ';
    const charactersLength = characters.length;
    for ( let i = 0; i < 15; i++ ) {
        result1 += characters.charAt(Math.floor(Math.random() * charactersLength));
    }
    console.log(result1.trim())

    $("#cod").val(result1.trim())
  }
</script>