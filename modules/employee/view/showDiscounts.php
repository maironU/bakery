<?php
 include("modules/employee/view/includes.php");
 
 $id=$obj->getVars('id');
 
 $msg=false;
 
 //Acciones
 
 if($obj->getVars('ActionDel')==true){
  $obj->delDiscount();
  $msg=true;
 }
 
 ?>
 <script type="text/javascript" src="modules/player/js/jsPlayerFunctions.js"></script>
<!-- Add mousewheel plugin (this is optional) -->
<script type="text/javascript" src="modules/player/js/jquery.mousewheel-3.0.6.pack.js"></script>
<!-- Add fancyBox main JS and CSS files -->
<script type="text/javascript" src="modules/player/js/jquery.fancybox.js?v=2.1.4"></script>
<link rel="stylesheet" type="text/css" href="modules/player/css/jquery.fancybox.css?v=2.1.4" media="screen" />
 <div class="widget3">
 <div class="widgetlegend">Descuentos</div>
 <?php
  if($msg)
  {
  ?>
   <div class="ui-widget">
	<div class="ui-state-highlight ui-corner-all" style="margin-top: 20px; padding: 0 .7em;">
		<p><span class="ui-icon ui-icon-info" style="float: left; margin-right: .3em;"></span>
		<strong>Felicitaciones!</strong> Se ha sido eliminado satisfactoriamente.</p>
	</div>
</div>
  <?php
  }
 ?>
<p style="width:100">
	<a href="<?php $_SERVER['PHP_SELF'];?>?p=modules/employee/view/newDiscount.php&id_em=<?php echo $id;?>" class="btn_normal" style="float:left; margin:5px;">Nuevo </a>
	<a href="<?php $_SERVER['PHP_SELF'];?>?p=modules/employee/view/index.php" class="btn_normal" style="float:left; margin:5px;">Volver </a>
</p>
 
<table width="100%">
  <tr>
    <th width="30">ID</th>
    <th width="30">Fecha</th>
    <th width="213">Nombre</th>
    <th width="213">Descripcion</th>
    <th width="125">Valor</th>
    <th colspan="2">Acciones</th>
  </tr>
  <?php
   $row=$obj->getDiscountByEmployee($id);
   
   if(count($row)>0){
 	foreach($row as $row){
  ?>
  <tr>
    <td><?php echo $row["ed_id"];?></td>
    <td><?php echo $row["ed_date"];?></td>
	<td><?php echo $row["ed_name"];?></td>
	<td><?php echo $row["ed_desc"];?></td>
	<td>$<?php echo number_format($row["ed_value"],2);?></td>
	<td width="86"><a href="<?php $_SERVER['PHP_SELF'];?>?p=modules/employee/view/editDiscount.php&id_ed=<?php echo $row["ed_id"]?>&id=<?php echo $id?>" class="btn_normal">Editar</a></td>
	<td width="86"><a href="<?php $_SERVER['PHP_SELF'];?>?p=modules/employee/view/showDiscounts.php&id_ed=<?php echo $row["ed_id"]?>&ActionDel=true&id=<?php echo $id?>" class="btn_borrar">Eliminar</a></td>
  </tr>
  <?php
   }
  } 
  ?>
</table>

</div>
<script>
 function showInfo(id,site, id_customer)
 {
	$.fancybox.open({
					href : site+'?id='+id+'&id_customer='+id_customer,
					type : 'iframe',
					padding:2,
					'afterShow': function() {
						
        				$('#fancybox-frame').contents().find("#barcode").focus();
						//document.frm1.barcode.focus();
					},
					'afterClose' : function(){
					 	//parent.location.reload(true);
					}
					
	}); 
 }
</script>


