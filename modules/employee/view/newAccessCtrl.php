<?php
 include("modules/employee/view/includes.php");

 
 $msg=false;
 
 if($_POST){
  $obj->newAccessCtrl();
  $msg=true;
 }

?>
<div class="widget3">
 <div class="widgetlegend">Nuevo Control de Acceso </div>
 <?php
  if($msg)
  {
  ?>
   <div class="ui-widget">
	<div class="ui-state-highlight ui-corner-all" style="margin-top: 20px; padding: 0 .7em;">
		<p><span class="ui-icon ui-icon-info" style="float: left; margin-right: .3em;"></span>
		<strong>Felicitaciones!</strong> Se ha sido creado satisfactoriamente.</p>
	</div>
</div>
  <?php
  }
 ?>
<p style="width:100">
	<a href="<?php $_SERVER['PHP_SELF'];?>?p=modules/employee/view/showAccessCtrl.php" class="btn_normal" style="float:left; margin:5px;">Volver </a>
</p>
 
<form action="#" method="post" enctype="multipart/form-data" name="form1" id="form1">
<table width="803" height="48" border="0" style="float:left">
  <tr>
    <td><label>Fecha de Entrada: </label>
      <br />
      <input name="datein" type="text" id="datein" class="datepicker" required/>   </td>
  </tr>
  <tr>
    <td><label>Hora de Entrada: </label>
      <br />
      <input name="hourin" type="text" id="hourin" placeholder="23:59:59" required/>   </td>
  </tr>
  <tr>
    <td><label>Fecha de Salida: </label>
      <br />
      <input name="dateout" type="text" id="dateout" class="datepicker2" required/>   </td>
  </tr>
  <tr>
    <td><label>Hora de Salida: </label>
      <br />
      <input name="hourout" type="text" id="hourout" placeholder="23:59:59" required/>   </td>
  </tr>
  <tr>
    <td><label>Empleado: </label>
      <br />
     <select name="id_employee" required>
         <option value="">--Seleccionar--</option>
         <?php
            $row = $obj->getEmployeeActive();
            if(count($row) > 0)
            {
                foreach($row as $row)
                {
                    ?>
                    <option value="<?php echo $row['em_id']?>"><?php echo $row['em_name']?></option>
                    <?php
                }
            }
         ?>
     </select>
     </td>
  </tr>
  <tr>
    <td><input type="submit" value="Guardar" class="btn_submit"/></td>
  </tr>
</table>
</form>


</div>
