<?php
    include("modules/employee/view/includes.php");
 $msg=false;
 
 if($_POST){
  $obj->newEmployee();
  $msg=true;
 }

?>
<div class="widget3">
 <div class="widgetlegend">Nuevo Empleado </div>
 <?php
  if($msg)
  {
  ?>
   <div class="ui-widget">
	<div class="ui-state-highlight ui-corner-all" style="margin-top: 20px; padding: 0 .7em;">
		<p><span class="ui-icon ui-icon-info" style="float: left; margin-right: .3em;"></span>
		<strong>Felicitaciones!</strong> Se ha sido creado satisfactoriamente.</p>
	</div>
</div>
  <?php
  }
 ?>
<p style="width:100">
	<a href="<?php $_SERVER['PHP_SELF'];?>?p=modules/employee/view/index.php" class="btn_normal" style="float:left; margin:5px;">Volver </a>
</p>
 
<form action="#" method="post" enctype="multipart/form-data" name="form1" id="form1">
<table width="803" height="48" border="0" style="float:left">
  <tr>
    <td><label>Nombres y Apellidos: </label>
      <br />
      <input name="name" type="text" id="name" required />   </td>
  </tr>
  <tr>
    <td><label>Foto : </label>
      <br />
      <input name="img" type="file" id="img" />  </td>
  </tr>
  <tr>
    <td><label>Cumplea&ntilde;os: </label>
      <br />
      <input name="birth" type="text" id="birth" class="datepicker" /></td>
  </tr>
  <tr>
    <td><label>Direccion: </label>
      <br />
      <input name="addr" type="text" id="addr" /></td>
  </tr>
  <tr>
    <td><label>Telefono: </label>
      <br />
      <input name="phone" type="text" id="phone" required /></td>
  </tr>
  <tr>
    <td><label>Codigo: </label>
      <br />
      <input name="code" type="text" id="code" required /></td>
  </tr>
  <tr>
    <td><label>Fecha de Ingreso: </label>
      <br />
      <input name="indate" type="text" id="indate" class="datepicker2" /></td>
  </tr>
  <tr>
    <td><label>Salario: </label>
      <br />
      <input name="salary" type="text" id="salary" required/></td>
  </tr>
  <tr>
    <td><label>Salario por hora: </label>
      <br />
      <input name="hsalary" type="text" id="hsalary" required/></td>
  </tr>
  <tr>
    <td><label>Email: </label>
      <br />
      <input name="email" type="text" id="email" required /></td>
  </tr>
  <tr>
    <td><label>Cargo: </label>
      <br />
      <input name="position" type="text" id="position" required /></td>
  </tr>
  <tr>
    <td><label>Lugar de trabajo: </label>
      <br />
      <input name="place" type="text" id="place" required /></td>
  </tr>
  <tr>
    <td><label>Hora Entrada: </label>
      <br />
      <input name="schedin" type="text" id="schedin" pattern="(([0-1]?[0-9])|(2[0-3])):[0-5][0-9]:[0-5][0-9]" placeholder="HH:MM:SS" required /></td>
  </tr>
  <tr>
    <td><label>Hora Salida: </label>
      <br />
      <input name="schedout" type="time" id="schedout" pattern="(([0-1]?[0-9])|(2[0-3])):[0-5][0-9]:[0-5][0-9]" placeholder="HH:MM:SS" required /></td>
  </tr>
  <tr>
    <td><label>Perfil: </label>
      <br />
      <textarea name="profile" cols="50" rows="6" id="profile"></textarea></td>
  </tr>
  <tr>
    <td><input type="submit" value="Guardar" class="btn_submit" /><td>
  </tr>
</table>
</form>


</div>
