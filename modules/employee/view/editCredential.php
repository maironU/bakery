<?php
 include("modules/employee/view/includes.php");

 
 $msg=false;
 
 if($_POST){
  $obj->editCredential();
  $msg=true;
 }
 
 $id = $obj->getVars('id');
 $row = $obj->getCredentialById($id);
 foreach($row as $row)
 {
     $name = $row['c_name'];
     $desc = $row['c_desc'];
     $img = $row['c_image'];
 }

?>
<div class="widget3">
 <div class="widgetlegend">Editar Credencial </div>
 <?php
  if($msg)
  {
  ?>
   <div class="ui-widget">
	<div class="ui-state-highlight ui-corner-all" style="margin-top: 20px; padding: 0 .7em;">
		<p><span class="ui-icon ui-icon-info" style="float: left; margin-right: .3em;"></span>
		<strong>Felicitaciones!</strong> Se ha sido creado satisfactoriamente.</p>
	</div>
</div>
  <?php
  }
 ?>
<p style="width:100">
	<a href="<?php $_SERVER['PHP_SELF'];?>?p=modules/employee/view/showCredentials.php" class="btn_normal" style="float:left; margin:5px;">Volver </a>
</p>
 
<form action="#" method="post" enctype="multipart/form-data" name="form1" id="form1">
<table width="803" height="48" border="0" style="float:left">
  <tr>
    <td><label>Nombre: </label>
      <br />
      <input name="name" type="text" id="name" value="<?php echo $name?>" required/>   </td>
  </tr>
  <tr>
    <td><label>Imagen: </label>
      <br />
      <input name="img" type="file" id="img" />  
      <img src="modules/employee/credentialPhoto/<?php echo $img;?>" width="200" height="100" />
      </td>
  </tr>
  <tr>
    <td><label>Descripcion: </label>
      <br />
      <?php
                                $oFCKeditor_l = new FCKeditor('desc') ;
                                $oFCKeditor_l->BasePath = 'modules/employee/fckeditor/';
                                $oFCKeditor_l->Width  = '650' ;
                                $oFCKeditor_l->Height = '600' ;
                                $oFCKeditor_l->Value = $desc;
                                $oFCKeditor_l->Create() ;
                            ?></td>
  </tr>
  <tr>
    <td><input type="hidden" name="id" value="<?php echo $id?>" />
        <input type="submit" value="Guardar" class="btn_submit"/></td>
  </tr>
</table>
</form>


</div>
