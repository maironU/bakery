<?php
 include("modules/employee/view/includes.php");
 
 $id=$obj->getVars('id');
 
 $msg=false;
 
 //Acciones
 
 
 ?>
 <script type="text/javascript" src="modules/player/js/jsPlayerFunctions.js"></script>
<!-- Add mousewheel plugin (this is optional) -->
<script type="text/javascript" src="modules/player/js/jquery.mousewheel-3.0.6.pack.js"></script>
<!-- Add fancyBox main JS and CSS files -->
<script type="text/javascript" src="modules/player/js/jquery.fancybox.js?v=2.1.4"></script>
<link rel="stylesheet" type="text/css" href="modules/player/css/jquery.fancybox.css?v=2.1.4" media="screen" />
 <div class="widget3">
 <div class="widgetlegend">Liquidacion de Pagos</div>
 <?php
  if($msg)
  {
  ?>
   <div class="ui-widget">
	<div class="ui-state-highlight ui-corner-all" style="margin-top: 20px; padding: 0 .7em;">
		<p><span class="ui-icon ui-icon-info" style="float: left; margin-right: .3em;"></span>
		<strong>Felicitaciones!</strong> Se ha sido eliminado satisfactoriamente.</p>
	</div>
</div>
  <?php
  }
 ?>
<p style="width:100">
	<a href="<?php $_SERVER['PHP_SELF'];?>?p=modules/employee/view/index.php" class="btn_normal" style="float:left; margin:5px;">Volver </a>
	<a href="javascript:;" class="btn_normal" onClick="PrintElem('paymentReport')" style="float:left; margin:5px;">Imprimir </a>
</p>
<br><br><br>
<form action="#" method="post">
    <table>
        <tr>
            <td><input type="text" name="start" class="datepicker" placeholder="Fecha Inicio" required /></td>
            <td><input type="text" name="end" class="datepicker2" placeholder="Fecha Fin" required /></td>
            <td>
                <input type="hidden" value="<?php echo $id?>" name="id" />
                <input type="submit" value="Liquidar" class="btn_submit" /></td>
        </tr>
    </table>
</form>
<br><br><br>
<div id="paymentReport">
<?php
    if($_POST)
    {
        $start = $obj->postVars('start');
        $end = $obj->postVars('end');
        
        $row = $obj->getEmployeeActive();
        if(count($row)>0)
        {
            foreach($row as $row)
            {
                ?>
                
                <table width="100%">
                    <tr>
                        <td>Empleado: <?php echo $row['em_name']?>
                            <br>Lugar de Trabajo: <?php echo $row['em_place']?>
                            <br>Hora de Entrada: <?php echo $row['em_schedulein']?>
                            <br>Hora de Salida: <?php echo $row['em_scheduleout']?>
                        </td>
                    </tr>
                    <tr>
                        <td>
                            <table>
                                <tr>
                                    <td colspan="5"><hr></td>
                                </tr>
                                <tr>
                                    <th>Devengado</th>
                                    <th>Descuentos</th>
                                    <th>Extras</th>
                                    <th>Valor Hora</th>
                                    <th>Total horas Trabajadas</th>
                                    <th>Total a Pagar</th>
                                </tr>
                                <tr>
                                    <td>$<?php
                                        $sum_dev = $obj->getEmployeeDev($row['em_id'], $start, $end, $row['em_hsalary']);
                                        echo number_format($sum_dev,2);
                                    ?></td>
                                    <td>$<?php
                                        $sum_dis = $obj->getTotalDiscountByIntervalDates($row['em_id'], $start, $end, 1);
                                        echo number_format($sum_dis,2);
                                    ?></td>
                                    <td>$<?php
                                        $sum_extras = $obj->getTotalDiscountByIntervalDates($row['em_id'], $start, $end, 2);
                                        echo number_format($sum_extras,2);
                                    ?></td>
                                    <td>$<?php echo number_format($row['em_hsalary'],2)?></td>
                                    <td><?php echo $obj->getEmployeeHw($row['em_id'], $start, $end);?></td>
                                    <td>$<?php echo number_format($sum_dev-$sum_dis+$sum_extras,2)?></td>
                                </tr>
                            </table>
                        </td>
                    </tr>
                    <tr>
                        <td>
                            <table>
                                <tr>
                                    <th>Fecha de registro</th>
                                    <th>Entrada</th>
                                    <th>Salida</th>
                                    <th>Horas trabajadas</th>
                                </tr>
                                <?php
                                    $row = $obj->getAccessCtrlByDateEmployee($row['em_id'], $start, $end);
                                    if(count($row)>0)
                                    {
                                        foreach($row as $row)
                                        {
                                            ?>
                                            <tr>
                                                <td><?php echo $row['eac_date']?></td>
                                                <td><?php echo $row['eac_datein']." ".$row['eac_hourin']?></td>
                                                <td><?php echo $row['eac_dateout']." ".$row['eac_hourout']?></td>
                                                <td><?php echo $row['eac_hourworked']?></td>
                                            </tr>
                                            <?php
                                        }
                                    }
                                ?>
                            </table>
                        </td>
                    </tr>
                </table>
                
                <?php
            }
        }
    }
?>
</div>
</div>
<script>
 function showInfo(id,site, id_customer)
 {
	$.fancybox.open({
					href : site+'?id='+id+'&id_customer='+id_customer,
					type : 'iframe',
					padding:2,
					'afterShow': function() {
						
        				$('#fancybox-frame').contents().find("#barcode").focus();
						//document.frm1.barcode.focus();
					},
					'afterClose' : function(){
					 	//parent.location.reload(true);
					}
					
	}); 
 }
 
 function PrintElem(elem)
{
    var mywindow = window.open('', 'PRINT', 'height=400,width=800');

    mywindow.document.write('<html><head><title>Reporte de Pago</title>');
    mywindow.document.write('</head><body style=\'font-family: Arial; font-size: 12px\'>');
    mywindow.document.write('<h1>' + document.title  + '</h1>');
    mywindow.document.write(document.getElementById(elem).innerHTML);
    mywindow.document.write('</body></html>');

    mywindow.document.close(); // necessary for IE >= 10
    mywindow.focus(); // necessary for IE >= 10*/

    mywindow.print();
    mywindow.close();

    return true;
}
</script>


