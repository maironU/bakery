<?php
 include("modules/employee/view/includes.php");
 
  //Se verifica si el modulo esta instalado
 if(!$obj->Checker()){
  $ins=true;
 }
 
 $msg=false;
 
 //Instala el modulo
 if($obj->getVars('install')){
  $obj->install();
  $msgIns=true;
 }
 
 //Acciones
 
 if($obj->getVars('ActionDel')==true){
  $obj->delEmployee();
  $_SESSION['flash_message'] = "Se ha sido eliminado satisfactoriamente.";
 }
 

 if(!$ins){
 ?>
 <script type="text/javascript" src="modules/employee/js/jsPlayerFunctions.js"></script>
<!-- Add mousewheel plugin (this is optional) -->
<script type="text/javascript" src="modules/employee/js/jquery.mousewheel-3.0.6.pack.js"></script>
<!-- Add fancyBox main JS and CSS files -->
<script type="text/javascript" src="modules/employee/js/jquery.fancybox.js?v=2.1.4"></script>
<link rel="stylesheet" type="text/css" href="modules/employee/css/jquery.fancybox.css?v=2.1.4" media="screen" />

<div class="card shadow mb-4">
 <div class="widgetlegend" style="margin: 1rem">Empleados </div>
 <?php
  if(isset($_SESSION['flash_message']))
  {
  ?>
    <div class="alert alert-success alert-dismissible fade show" style="margin: 1rem" role="alert">
      <strong>Felicitaciones!</strong> <?php echo $_SESSION['flash_message'] ?>
      <button type="button" class="close" data-dismiss="alert" aria-label="Close">
        <span aria-hidden="true">&times;</span>
      </button>
    </div>
  <?php
    unset($_SESSION['flash_message']);
  }
 ?>
  <div class="card-header py-3">
    <a class="btn btn-primary" href="<?php $_SERVER['PHP_SELF'];?>?p=modules/employee/view/newEmployee.php" class="btn_normal" style="float:left; margin:5px;">Nuevo </a>
    <a class="btn btn-primary" href="<?php $_SERVER['PHP_SELF'];?>?p=modules/employee/view/showCredentials.php" class="btn_normal" style="float:left; margin:5px;">Credenciales </a>
    <a class="btn btn-primary"href="<?php $_SERVER['PHP_SELF'];?>?p=modules/employee/view/showPayments.php" class="btn_normal" style="float:left; margin:5px;">Liq. Pago </a>
  </div>
  <div class="card-body">
      <div class="table-responsive">
          <table class="table table-bordered" id="empleados" width="100%" cellspacing="0">
            <thead>
                <tr class="bg-white">
                  <th>ID</th>
                  <th>Imagen</th>
                  <th>Nombre</th>
                  <th>Codigo</th>
                  <th style="min-width: 150px">Lugar de trabajo</th>
                  <th>Posicion</th>
                  <th style="min-width: 100px">Valor Hora</th>
                  <th>Acciones</th>
                </tr>
            </thead>
            <tfoot>
                <tr class="bg-white">
                  <th>ID</th>
                  <th>Imagen</th>
                  <th>Nombre</th>
                  <th>Codigo</th>
                  <th style="min-width: 150px">Lugar de trabajo</th>
                  <th>Posicion</th>
                  <th style="min-width: 100px">Valor Hora</th>
                  <th>Acciones</th>
                </tr>
            </tfoot>
            <tbody>
            <?php
                $row=$obj->getEmployee();
                
                if(count($row)>0){
                foreach($row as $row){
                ?>
                <tr>
                  <td><?php echo $row["em_id"];?></td>
                <td><img src="modules/employee/employeeImages/<?php echo $row["em_image"];?>" width="50" height="50" /></td>
                <td><?php echo $row["em_name"];?></td>
                <td><?php echo $row["em_code"];?></td>
                  <td><?php echo $row["em_place"];?></td>
                  <td><?php echo $row["em_position"];?></td>
                  <td>$<?php echo number_format($row["em_hsalary"],2);?></td>
                  <td style="min-width: 540px">
                    <a class="btn btn-primary" href="<?php $_SERVER['PHP_SELF'];?>?p=modules/employee/view/editEmployee.php&id=<?php echo $row["em_id"]?>" class="btn_1">Editar</a>
                    <a class="btn btn-secondary" href="javascript:;" class="btn_5" onClick="showInfo('modules/employee/view/createId.php',<?php echo $row["em_id"]?>)">Credencial</a>
                    <a class="btn btn-dark" href="<?php $_SERVER['PHP_SELF'];?>?p=modules/employee/view/showDiscounts.php&id=<?php echo $row["em_id"]?>" class="btn_3">Descuentos</a>
                    <a class="btn btn-success" href="<?php $_SERVER['PHP_SELF'];?>?p=modules/employee/view/showPaymentDetail.php&id=<?php echo $row["em_id"]?>" class="btn_4">Liquidar</a>
                    <a class="btn btn-danger" href="<?php $_SERVER['PHP_SELF'];?>?p=modules/employee/view/index.php&id=<?php echo $row["em_id"]?>&ActionDel=true" class="btn_2">Eliminar</a>
                  </td>
                </tr>
                <?php
                }
                } 
                ?>
            </tbody>
          </table>
        </div>
    </div>
</div>
<script>

$(document).ready(function() {
        $('#empleados').DataTable({
            "aaSorting": [],
            language: {
                "sProcessing":     "Procesando...",
                "sLengthMenu":     "Mostrar _MENU_ registros",
                "sZeroRecords":    "No se encontraron resultados",
                "sEmptyTable":     "Ningun dato disponible en esta tabla",
                "sInfo":           "Mostrando registros del _START_ al _END_ de un total de _TOTAL_ registros",
                "sInfoEmpty":      "Mostrando registros del 0 al 0 de un total de 0 registros",
                "sInfoFiltered":   "(filtrado de un total de _MAX_ registros)",
                "sInfoPostFix":    "",
                "sSearch":         "Buscar:",
                "sUrl":            "",
                "sInfoThousands":  ",",
                "sLoadingRecords": "Cargando...",
                "oPaginate": {
                    "sFirst":    "Primero",
                    "sLast":     "Ultimo",
                    "sNext":     "Siguiente",
                    "sPrevious": "Anterior"
                },
                "oAria": {
                    "sSortAscending":  ": Activar para ordenar la columna de manera ascendente",
                    "sSortDescending": ": Activar para ordenar la columna de manera descendente"
                },
                "buttons": {
                    "copy": "Copiar",
                    "colvis": "Visibilidad"
                }
            }
        });
    } );

 function showInfo(id,site)
 {
	$.fancybox.open({
					href : site+'?id='+id,
					type : 'iframe',
					padding:2,
					'afterShow': function() {
						//document.frm1.barcode.focus();
					},
					'afterClose' : function(){
					 	//parent.location.reload(true);
					}
					
	}); 
 }
</script>

<?php } else {?>
<br />
 <div class="widget3">
 <div class="widgetlegend">Instalador Modulo de Empleados </div>
 <?php
  if($msgIns)
  {
  ?>
   <div class="ui-widget">
	<div class="ui-state-highlight ui-corner-all" style="margin-top: 20px; padding: 0 .7em;">
		<p><span class="ui-icon ui-icon-info" style="float: left; margin-right: .3em;"></span>
		<strong>Felicitaciones!</strong> El modulo ha sido instalado satisfactoriamente.</p>
	</div>
</div>
  <?php
  }
 ?>
<form id="form1" name="form1" method="post" action="#">
  <table width="804" border="0" style="float:left;">
    <tr>
      <td width="525" valign="top">Bienvenido al instalador Modulo de Empleados.  </td>
    </tr>
    <tr>
      <td valign="top"><div align="right"><a href="<?php $_SERVER['PHP_SELF'];?>?p=modules/employee/view/index.php&amp;install=true" class="btn_normal">Instalar</a></div></td>
    </tr>
  </table>
</form>
</div>
<?php } ?>

