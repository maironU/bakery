<?php
    include("modules/employee/view/includes.php");
 $msg=false;
 
 if($_POST){
  $obj->editEmployee();
  $msg=true;
 }
 
 $id = $obj->getVars('id');
 $row = $obj->getEmployeeById($id);
 foreach($row as $row)
 {
     $name = $row['em_name'];
     $img = $row['em_image'];
     $birth = $row['em_birth'];
     $addr = $row['em_addr'];
     $phone = $row['em_phone'];
     $code = $row['em_code'];
     $indate = $row['em_indate'];
     $salary = $row['em_salary'];
     $hsalary = $row['em_hsalary'];
     $email = $row['em_email'];
     $position = $row['em_position'];
     $place = $row['em_place'];
     $profile = $row['em_profile'];
     $schedin = $row['em_schedulein'];
     $schedout = $row['em_scheduleout'];
 }

?>
<script>
    var $datepicker = $('.datepicker');
    $datepicker.datepicker();
    $datepicker.datepicker('setDate', <?php echo $birth?>);
    
    var $datepicker2 = $('.datepicker');
    $datepicker2.datepicker();
    $datepicker2.datepicker('setDate', <?php echo $indate?>);
</script>
<div class="widget3">
 <div class="widgetlegend">Editar Empleado </div>
 <?php
  if($msg)
  {
  ?>
   <div class="ui-widget">
	<div class="ui-state-highlight ui-corner-all" style="margin-top: 20px; padding: 0 .7em;">
		<p><span class="ui-icon ui-icon-info" style="float: left; margin-right: .3em;"></span>
		<strong>Felicitaciones!</strong> Se ha sido creado satisfactoriamente.</p>
	</div>
</div>
  <?php
  }
 ?>
<p style="width:100">
	<a href="<?php $_SERVER['PHP_SELF'];?>?p=modules/employee/view/index.php" class="btn_normal" style="float:left; margin:5px;">Volver </a>
</p>
 
<form action="#" method="post" enctype="multipart/form-data" name="form1" id="form1">
<table width="803" height="48" border="0" style="float:left">
  <tr>
    <td><label>Nombres y Apellidos: </label>
      <br />
      <input name="name" type="text" id="name" value="<?php echo $name?>" required />   </td>
  </tr>
  <tr>
    <td><label>Foto : </label>
      <br />
      <input name="img" type="file" id="img" /><br>
      <img src="modules/employee/employeeImages/<?php echo $img;?>" width="50" height="50" />
      </td>
  </tr>
  <tr>
    <td><label>Cumplea&ntilde;os: </label>
      <br />
      <input name="birth" type="text" id="birth" class="datepicker" /></td>
  </tr>
  <tr>
    <td><label>Direccion: </label>
      <br />
      <input name="addr" type="text" id="addr" value="<?php echo $addr?>" /></td>
  </tr>
  <tr>
    <td><label>Telefono: </label>
      <br />
      <input name="phone" type="text" id="phone" value="<?php echo $phone?>" required /></td>
  </tr>
  <tr>
    <td><label>Codigo: </label>
      <br />
      <input name="code" type="text" id="code" value="<?php echo $code?>" required /></td>
  </tr>
  <tr>
    <td><label>Fecha de Ingreso: </label>
      <br />
      <input name="indate" type="text" id="indate" class="datepicker2" /></td>
  </tr>
  <tr>
    <td><label>Salario: </label>
      <br />
      <input name="salary" type="text" id="salary" value="<?php echo $salary?>" required/></td>
  </tr>
  <tr>
    <td><label>Salario por hora: </label>
      <br />
      <input name="hsalary" type="text" id="hsalary" value="<?php echo $hsalary?>" required/></td>
  </tr>
  <tr>
    <td><label>Email: </label>
      <br />
      <input name="email" type="text" id="email" value="<?php echo $email?>" required /></td>
  </tr>
  <tr>
    <td><label>Cargo: </label>
      <br />
      <input name="position" type="text" id="position" value="<?php echo $position?>" required /></td>
  </tr>
  <tr>
    <td><label>Lugar de trabajo: </label>
      <br />
      <input name="place" type="text" id="place" value="<?php echo $place?>" required /></td>
  </tr>
  <tr>
    <td><label>Hora Entrada: </label>
      <br />
      <input name="schedin" type="text" id="schedin" value="<?php echo $schedin?>" pattern="(([0-1]?[0-9])|(2[0-3])):[0-5][0-9]:[0-5][0-9]" placeholder="HH:MM:SS" required /></td>
  </tr>
  <tr>
    <td><label>Hora Salida: </label>
      <br />
      <input name="schedout" type="text" id="schedout" value="<?php echo $schedout?>" pattern="(([0-1]?[0-9])|(2[0-3])):[0-5][0-9]:[0-5][0-9]" placeholder="HH:MM:SS" required /></td>
  </tr>
  <tr>
    <td><label>Descripcion actividades: </label>
      <br />
      <textarea name="profile" cols="50" rows="6" id="profile"><?php echo $profile?></textarea></td>
  </tr>
  <tr>
    <td>
        <input name="id" type="hidden" id="id" value="<?php echo $id?>" />
        <input type="submit" value="Guardar" class="btn_submit" /><td>
  </tr>
</table>
</form>


</div>
