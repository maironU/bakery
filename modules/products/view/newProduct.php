<?php
 include('modules/products/model/products.php');
 include("modules/products/fckeditor/fckeditor.php") ;
 include ('modules/inventory/model/inventory.php');
 include ('modules/porders/model/porders.php');
 
 
 $obj = new products();
 $obj->connect();
 
 $inventory = new inventory();
 $inventory->connect();
 
 $po = new porders();
 $po->connect();
 
 $msg=false;
 
 if($_POST)
 {
    $id_product = $obj->newProduct();
    $msg=true;
    
    $name=$obj->postVars('name');
    $sdesc=$obj->postVars('sdesc');
    $price=$obj->postVars('price');
    $minstock=$obj->postVars('minstock');
    
    //$id_pinv = $inventory->newCapsInvProd2($name, $sdesc, $price, $minstock);
    //$inventory->editProduct($id_product,$id_pinv,1);
    
    $row = $inventory->getWarehouse();
    if(count($row)>0)
    {
        foreach($row as $row)
        {
            $inventory->newCapsInvProd3($name, $desc, $price, $minstock, $id_product, $row['iw_id']);
        }
    }
  }

?>
<div class="widget3">
 <div class="widgetlegend">Productos </div>
 <?php
  if($msg)
  {
  ?>
   <div class="ui-widget">
	<div class="ui-state-highlight ui-corner-all" style="margin-top: 20px; padding: 0 .7em;">
		<p><span class="ui-icon ui-icon-info" style="float: left; margin-right: .3em;"></span>
		<strong>Felicitaciones!</strong> El Producto ha sido creado satisfactoriamente.</p>
	</div>
</div>
  <?php
  }
 ?>
<p style="width:100">
	<a href="<?php $_SERVER['PHP_SELF'];?>?p=modules/products/view/index.php" class="btn_normal" style="float:left; margin:5px;">Volver </a>
</p>
 
<form action="#" method="post" enctype="multipart/form-data" name="form1" id="form1">
  <table width="800" border="0" style="float:left;">
    <tr>
      <td width="265" rowspan="2" valign="top"><table width="235" height="127" border="0">
        <tr>
          <td width="229"><label>Nombre</label><br />

            <input name="name" type="text" id="name" value="" />          </td>
        </tr>
       <tr>
          <td width="229"><label>Referencia</label><br />

            <input name="reference" type="text" id="reference" value="" /><a href="javascript:;" onclick="generate()" class="btn_borrar">Generar</a>          </td>
        </tr>
        <tr>
            <td width="229"><label>Imagen</label><br />

            <input name="img" type="file" id="img" value="" /></td>
        </tr>
        <tr>
          <td width="229"><label>EAN</label><br />

            <input name="ean" type="text" id="ean" value="" />          </td>
        </tr>
		<tr>
          <td width="229"><label>UPC</label><br />

            <input name="upc" type="text" id="upc" value="" />          </td>
        </tr>
        <tr>
          <td width="229"><label>Cantidad de Contenedor</label><br />

            <input name="qtycontainer" type="text" id="qtycontainer" value="1" />          </td>
        </tr>
		<tr>
          <td width="229"><label>Precio Costo</label> 
          :<br />

            <input name="costprice" type="text" id="costprice" value="" />          </td>
        </tr>
		<tr>
          <td width="229"><label>Precio Variable</label> 
          :<br />

            <input name="varprice" type="checkbox" class="toggle" id="varprice" value="1" />          </td>
        </tr>
		<tr>
          <td width="229"><label>Precio Venta</label> 
          :<br />

            <input name="price" type="text" id="price" value="" />          </td>
        </tr>
        <tr>
          <td><label>Impuestos</label>
            <br />
            <label>
            <select name="prot_id" id="prot_id">
              <option value="">-- Select --</option>
              <?php
			   $row=$obj->getTax();
			   if(count($row)>0)
			   {
			    foreach($row as $row){
				?>
              <option value="<?php echo $row["prot_id"];?>"><?php echo $row["prot_name"];?></option>
              <?php
				}
			   }
			  ?>
            </select>
            </label></td>
        </tr>
        <tr>
          <td width="229"><label>Garantia (Meses):</label><br />

            <input name="warranty" type="text" id="warranty" value="" />          </td>
        </tr>
		<tr>
          <td width="229"><label>Ancho:</label><br />

            <input name="width" type="text" id="width" value="" />          </td>
        </tr>
		<tr>
          <td width="229"><label>Alto</label><br />

            <input name="height" type="text" id="height" value="" />          </td>
        </tr>
		<tr>
          <td width="229"><label>Profundo</label><br />

            <input name="depth" type="text" id="depth" value="" />          </td>
        </tr>
		<tr>
          <td width="229"><label>Peso</label><br />

            <input name="weight" type="text" id="weight" value="" />          </td>
        </tr>
        <tr>
          <td width="229"><label>Minimo de Stock</label><br />

            <input name="minstock" type="text" id="minstock" value="" />          </td>
        </tr>
        <tr>
          <td><label>Activar</label>
            <input name="active" type="checkbox" id="active" value="1" class="toggle"/>
		  </td>
        </tr>
        <tr>
            <td>
                Activar en Ecommerce
                <input type="checkbox" name="activefront" value="1" class="toggle">
            </td>
        </tr>
        <tr>
          <td><label>Producto Asociado</label>
            <br />
            <label>
            <select name="assoc" id="assoc">
              <option value="">-- Select --</option>
              <?php
			   $row=$obj->getProducts();
			   if(count($row)>0)
			   {
			    foreach($row as $row){
				?>
              <option value="<?php echo $row["pro_id"];?>"><?php echo $row["pro_name"];?></option>
              <?php
				}
			   }
			  ?>
            </select>
            </label></td>
        </tr>
        <tr>
          <td><label>Categoria</label>
            <br />
            <label>
            <select name="cat" id="cat">
              <option value="">-- Select --</option>
              <?php
			   $row=$obj->getCategories();
			   if(count($row)>0)
			   {
			    foreach($row as $row){
				?>
              <option value="<?php echo $row["cat_id"];?>"><?php echo $row["cat_name"];?></option>
              <?php
				}
			   }
			  ?>
            </select>
            </label></td>
        </tr>
        <tr>
          <td><label>Proveedores</label>
            <br />
            <label>
            <select name="id_provider" id="id_provider">
              <option value="">-- Select --</option>
              <?php
			   $row=$po->getProvider();
			   if(count($row)>0)
			   {
			    foreach($row as $row){
				?>
              <option value="<?php echo $row["popr_id"];?>"><?php echo $row["popr_name"];?></option>
              <?php
				}
			   }
			  ?>
            </select>
            </label></td>
        </tr>
      </table></td>
      <td width="525" valign="top"><label>Descripci&oacute;n Corta </label>
        <br />
        <textarea name="sdesc" cols="100" rows="5" id="sdesc"></textarea>
      </td>
    </tr>
    <tr>
      <td valign="top"><?php
                                $oFCKeditor_l = new FCKeditor('desc') ;
                                $oFCKeditor_l->BasePath = 'modules/products/fckeditor/';
                                $oFCKeditor_l->Width  = '650' ;
                                $oFCKeditor_l->Height = '600' ;
                                $oFCKeditor_l->Value = '';
                                $oFCKeditor_l->Create() ;
                            ?></td>
    </tr>
    <tr>
      <td valign="top"><div align="right"></div></td>
      <td valign="top"><div align="right"><a href="javascript:;" class="btn_normal" onclick="document.form1.submit();">
        Guardar</a></div></td>
    </tr>
  </table>
</form>

</div>
<script>
function generate(){
	$.ajax({
	url: 'modules/products/view/ajax_generate_reference.php',
	async: true,
	success: function(data){
		 document.form1.reference.value=data;
	}
 });
}
</script>
