<?php
 include('modules/products/model/products.php');
 
 $obj = new products();
 $obj->connect();
 
 //Se verifica si el modulo esta instalado
 if(!$obj->Checker()){
  $ins=true;
 }
 
 $msg=false;
 $msgIns=false;
 
 if($obj->getVars('ActionDel')){
  $obj->delTax();
  $msg=true;
 }
 

?>
<div class="widget3">
 <div class="widgetlegend">Impuestos </div>
 <?php
  if($msg)
  {
  ?>
   <div class="ui-widget">
	<div class="ui-state-highlight ui-corner-all" style="margin-top: 20px; padding: 0 .7em;">
		<p><span class="ui-icon ui-icon-info" style="float: left; margin-right: .3em;"></span>
		<strong>Felicitaciones!</strong> se ha sido eliminado satisfactoriamente.</p>
	</div>
</div>
  <?php
  }
 ?>
<p style="width:100">
	<a href="<?php $_SERVER['PHP_SELF'];?>?p=modules/products/view/newTax.php" class="btn_normal" style="float:left; margin:5px;">Nuevo </a>
	<a href="<?php $_SERVER['PHP_SELF'];?>?p=modules/products/view/index.php" class="btn_normal" style="float:left; margin:5px;">Volver </a>
</p>
 
<table width="803" height="48" border="0">
  <tr>
    <th width="30">ID</th>
    <th width="213">Nombre</th>
    <th width="125">Valor</th>
    <th colspan="2">Acciones</th>
  </tr>
  <?php
   $row=$obj->getTax();
   
   if(count($row)>0){
 	foreach($row as $row){
  ?>
  <tr>
    <td><?php echo $row["prot_id"];?></td>
	<td><?php echo $row["prot_name"];?></td>
    <td><?php echo $row["prot_value"];?>%</td>
    <td width="86"><a href="<?php $_SERVER['PHP_SELF'];?>?p=modules/products/view/editTax.php&id=<?php echo $row["prot_id"]?>" class="btn_normal">Editar</a>&nbsp;</td>
    <td width="81"><a href="<?php $_SERVER['PHP_SELF'];?>?p=modules/products/view/showTax.php&id=<?php echo $row["prot_id"]?>&ActionDel=true" class="btn_borrar">Eliminar</a> &nbsp;</td>
  </tr>
  <?php
   }
  } 
  ?>
</table>

</div>