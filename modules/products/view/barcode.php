<?php
 require("modules/products/barcode/barcode.php");
 require("modules/products/barcode/i25object.php");
 require("modules/products/barcode/c39object.php");
 require("modules/products/barcode/c128aobject.php");
 require("modules/products/barcode/c128bobject.php");
 require("modules/products/barcode/c128cobject.php");
 
 /* Default value */
if (!isset($output))  $output   = "png";
if (!isset($type))    $type     = "C128B";
if (!isset($widthb))   $widthb    = "260";
if (!isset($heightb))  $heightb   = "60";
if (!isset($xres))    $xres     = "2";
if (!isset($font))    $font     = "2";

$drawtext="on";
$stretchtext="on";

$style |= ($output  == "png" ) ? BCS_IMAGE_PNG  : 0;
$style |= ($output  == "jpeg") ? BCS_IMAGE_JPEG : 0;
$style |= ($border  == "on"  ) ? BCS_BORDER           : 0;
$style |= ($drawtext== "on"  ) ? BCS_DRAW_TEXT  : 0;
$style |= ($stretchtext== "on" ) ? BCS_STRETCH_TEXT  : 0;
$style |= ($negative== "on"  ) ? BCS_REVERSE_COLOR  : 0;
?>
