<?php
 include('modules/products/model/products.php');
 
 $obj = new products();
 $obj->connect();
 
 $msg=false;
 
 if($_POST){
  $obj->newCategories();
  $msg=true;
 }

?>
<div class="widget3">
 <div class="widgetlegend">Categorias </div>
 <?php
  if($msg)
  {
  ?>
   <div class="ui-widget">
	<div class="ui-state-highlight ui-corner-all" style="margin-top: 20px; padding: 0 .7em;">
		<p><span class="ui-icon ui-icon-info" style="float: left; margin-right: .3em;"></span>
		<strong>Felicitaciones!</strong>Se ha creado satisfactoriamente.</p>
	</div>
</div>
  <?php
  }
 ?>
<p style="width:100">
	<a href="<?php $_SERVER['PHP_SELF'];?>?p=modules/products/view/showCategories.php" class="btn_normal" style="float:left; margin:5px;">Volver </a>
</p>
 
<form action="#" method="post" enctype="multipart/form-data" name="form1" id="form1">
<table width="803" height="48" border="0" style="float:left">
  <tr>
    <td><label>Nombre: </label>
      <br />
      <input name="name" type="text" id="name" />    </td>
  </tr>
  <tr>
    <td><label>Imagen: </label><br />
      <label>
      <input name="imgCat" type="file" id="imgCat" />
      </label></td>
  </tr>
  <tr>
    <td><label>Descripci&oacute;n: </label><br />
      <textarea name="desc" cols="50" rows="5" id="desc"></textarea>    </td>
  </tr>
  <tr>
    <td><label>Categoria Padre </label>
      <br />
      <label>
      <select name="father" id="father">
        <option value="">-- Select --</option>
        <?php
			   $row=$obj->getCategories();
			   if(count($row)>0)
			   {
			    foreach($row as $row){
				?>
        <option value="<?php echo $row["cat_id"];?>"><?php echo $row["cat_name"];?></option>
        <?php
				}
			   }
			  ?>
      </select>
      </label></td>
  </tr>
  <tr>
    <td><label>Activar: </label><br />
      <label>
      <input name="active" type="checkbox" id="active" value="1" class="toogle"/>
      </label></td>
  </tr>
  <tr>
    <td><label>Activar en Ecommerce: </label><br />
      <label>
      <input name="activefront" type="checkbox" id="activefront" value="1" class="toogle"/>
      </label></td>
  </tr>
  <tr>
    <td><label>Categoria Destacada: </label><br />
      <label>
      <input name="activeout" type="checkbox" id="activeout" value="1" class="toogle"/>
      </label></td>
  </tr>
  <tr>
    <td><label>Mejor categoria: </label><br />
      <label>
      <input name="best" type="checkbox" id="best" value="1" class="toogle"/>
      </label></td>
  </tr>
  <tr>
    <td><a href="javascript:;" class="btn_normal" onclick="document.form1.submit();">Guardar</a></td>
  </tr>
</table>
</form>


</div>
