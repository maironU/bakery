<?php
 include('modules/products/model/products.php');
 include ('modules/inventory/model/inventory.php');
 
 $obj = new products();
 $obj->connect();
 
 $inventory = new inventory();
 $inventory->connect();
 
 $msg=false;
 
 if($_POST)
 {
	 $id = $obj->postVars('id');
	 $value = $obj->postVars('value');
	 $obj->editProductParams($id, $value);
	 $msg=true;
 }
?>

 
<div class="widget3">
 <div class="widgetlegend">Parametros</div>
 <?php
  if($msg)
  {
  ?>
   <div class="ui-widget">
	<div class="ui-state-highlight ui-corner-all" style="margin-top: 20px; padding: 0 .7em;">
		<p><span class="ui-icon ui-icon-info" style="float: left; margin-right: .3em;"></span>
		<strong>Felicitaciones!</strong> Se ha guardado satisfactoriamente.</p>
	</div>
</div>
  <?php
  }
 ?>
<p style="width:100">
	<a href="<?php $_SERVER['PHP_SELF'];?>?p=modules/products/view/index.php" class="btn_normal" style="float:left; margin:5px;">Volver </a>
</p>
 <br />
 <br />
<table>
	<?php
		$i=1;
		$row = $obj->getProductsParams();
		if(count($row)>0)
		{
			foreach($row as $row)
			{
				?>
				<form action="" method="post" name="frm<?php echo $i?>">
				<tr>
					<td><?php echo $row['propa_name']?></td>
					<td><input type="text" name="value" value="<?php echo $row['propa_value']?>" /></td>
					<td><input type="hidden" name="id" value="<?php echo $row['propa_id']?>" /><input type="submit" value="Guardar" class="btn_submit" /></td>
				</tr>
				</form>
				<?php
			}
		}
	?>
</table>

</div>

<br />
 
 

