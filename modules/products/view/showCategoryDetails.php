<?php
 include('modules/products/model/products.php');
 
 $obj = new products();
 $obj->connect();
 
 
 $msg=false;
 $msgIns=false;
 
 if($obj->getVars('ActionDel')){
  //$obj->delCompany();
  //$msg=true;
 }
 
 $id=$obj->getVars('id');
 $row=$obj->getProductsById($id);
 if(count($row)>0){
  foreach($row as $row){
   $name=$row["pro_name"];
   $sdesc=$row["pro_sdesc"];
   $desc=$row["pro_desc"];
   $reference=$row["pro_reference"];
   $ean=$row["pro_ean"];
   $upc=$row["pro_upc"];
   $active=$row["pro_active"];
   $price=$row["pro_price"];
   $pricefob=$row["pro_pricefob"];
   $wholesaleprice=$row["pro_wholesaleprice"];
   $height=$row["pro_height"];
   $width=$row["pro_width"];
   $depth=$row["pro_depth"];
   $weight=$row["pro_weight"];
   $comid=$row["com_id"];
   $cliid=$row["cli_id"];
   $catid=$row["cat_id"];
  }
 }
 
 //Verifico si el modulo de clientes esta instalado y obtengo la informacion de la compa�ia y el cliente
 if($obj->CheckerMod('company')){
  include('modules/clients/model/clientes.php');
  
  $clients = new clients();
  
  $row=$clients->getCompanyById($comid);
  if(count($row)>0){
   foreach($row as $row){
    $comname=$row["com_name"];
	$comsigla=$row["com_sigla"];
	$comcountry=$row["com_country"];
	$comcity=$row["com_city"];
	$industry=$row["com_industry"];
   }
  }
  
  $row=$clients->getClientById($cliid);
  if(count($row)>0){
   foreach($row as $row){
    $cliname=$row["cli_name"];
	$clisurname=$row["cli_surname"];
	$clicountry=$row["cli_country"];
	$clicity=$row["cli_city"];
	$cliindustry=$row["cli_industry"];
	$cliposition=$row["cli_position"];
	$cliemail=$row["cli_email"];
   }
  }
  
 }

?>
<style>
 .block1{
 width:500px;
 }
 
 .block1 img{
	background-color: #CCCCCC;
	border: 1px solid #999999;
	padding: 3px;
	width:150;
	height:150px;
 }
 
 ul{
 padding:0px;
 margin:0px;
 }
 ul li{
	float:left;
	list-style-type: none;  
 }
</style>
<div class="widget3">
 <div class="widgetlegend"> Productos </div>
 <?php
  if($msg)
  {
  ?>
   <div class="ui-widget">
	<div class="ui-state-highlight ui-corner-all" style="margin-top: 20px; padding: 0 .7em;">
		<p><span class="ui-icon ui-icon-info" style="float: left; margin-right: .3em;"></span>
		<strong>Felicitaciones!</strong> se ha sido eliminado satisfactoriamente.</p>
	</div>
</div>
  <?php
  }
 ?>
<p style="width:100">
	<a href="<?php $_SERVER['PHP_SELF'];?>?p=modules/products/view/index.php" class="btn_normal" style="float:left; margin:5px;">Volver </a>
</p>
 <fieldset>
  <legend>DETALLE PRODUCTO</legend>
  <table border="0" width="697">
   <tr>
    <td width="171" valign="top"><strong>Nombre Producto : </strong></td>
	<td width="161" valign="top"><?php echo $name; ?></td>
	<td width="180" valign="top"><strong>Referencia : </strong></td>
	<td width="167" valign="top"><?php echo $reference; ?></td>
   </tr>
   <tr>
     <td valign="top"><strong>EAN : </strong></td>
     <td valign="top"><?php echo $ean; ?></td>
     <td valign="top"><strong>UPC : </strong></td>
     <td valign="top"><?php echo $upc; ?></td>
   </tr>
   <tr>
     <td valign="top"><strong>Categoria : </strong></td>
     <td valign="top"></td>
     <td valign="top"><strong>Precio :</strong></td>
     <td valign="top">$<?php echo number_format($price); ?></td>
   </tr>
   <tr>
     <td valign="top"><strong>Precio FOB: </strong></td>
     <td valign="top">$<?php echo number_format($pricefob); ?></td>
     <td valign="top"><strong>Precio (Al por Mayor): </strong></td>
     <td valign="top">$<?php echo number_format($wholesaleprice); ?></td>
   </tr>
   <tr>
     <td valign="top">&nbsp;</td>
     <td valign="top"></td>
     <td valign="top">&nbsp;</td>
     <td valign="top"></td>
   </tr>
   <tr>
     <td valign="top"><strong>Alto:</strong></td>
     <td valign="top"><?php echo $width; ?></td>
     <td valign="top"><strong>Ancho</strong></td>
     <td valign="top"><?php echo $height; ?></td>
   </tr>
   <tr>
     <td valign="top"><strong>Profundo:</strong></td>
     <td valign="top"><?php echo $depth; ?></td>
     <td valign="top"><strong>Peso:</strong></td>
     <td valign="top"><?php echo $weight; ?></td>
   </tr>
   <tr>
     <td valign="top">&nbsp;</td>
     <td valign="top"></td>
     <td valign="top">&nbsp;</td>
     <td valign="top"></td>
   </tr>
   <tr>
     <td valign="top"><strong>Descripcion Corta: </strong></td>
     <td colspan="3" valign="top"><?php echo $sdesc; ?></td>
    </tr>
   <tr>
     <td valign="top"><strong>Descripcion: </strong></td>
     <td colspan="3" valign="top"><?php echo $desc; ?></td>
   </tr>
  </table>
 </fieldset>
 <fieldset>
  <legend>IMAGENES</legend>
  <div class="block1">
   <ul>
    <?php 
	 $row=$obj->getProductImages($id);
	 if(count($row)>0){
	 	foreach($row as $row){
		?>
		 <li><img src="modules/products/imagesProd/<?php $row["proimg_file"]?>" /></li>
		<?php
		}
	 }
	?>
   </ul>
  </div>
 </fieldset>
 <fieldset>
  <legend>DOCUMENTOS</legend>
  <div class="block1">
   <ul>
    <?php 
	 $row=$obj->getProductImages($id);
	 if(count($row)>0){
	 	foreach($row as $row){
		?>
		 <li><a href="modules/products/filesProd/<?php $row["profile_file"]?>" target="_blank" ><img src="modules/products/images/Document-icon.png" /></a></li>
		<?php
		}
	 }
	?>
   </ul>
  </div>
 </fieldset>
</div>