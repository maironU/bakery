<?php
 include('../../../core/config.php');
 include('../../../core/ge.php');
 include('../model/products.php');
 
 require("../barcode/barcode.php");
 require("../barcode/i25object.php");
 require("../barcode/c39object.php");
 require("../barcode/c128aobject.php");
 require("../barcode/c128bobject.php");
 require("../barcode/c128cobject.php");
 
 /* Default value */
if (!isset($output))  $output   = "png";
if (!isset($type))    $type     = "C128B";
if (!isset($widthb))   $widthb    = "260";
if (!isset($heightb))  $heightb   = "60";
if (!isset($xres))    $xres     = "2";
if (!isset($font))    $font     = "2";

$drawtext="on";
$stretchtext="on";

$style |= ($output  == "png" ) ? BCS_IMAGE_PNG  : 0;
$style |= ($output  == "jpeg") ? BCS_IMAGE_JPEG : 0;
$style |= ($border  == "on"  ) ? BCS_BORDER           : 0;
$style |= ($drawtext== "on"  ) ? BCS_DRAW_TEXT  : 0;
$style |= ($stretchtext== "on" ) ? BCS_STRETCH_TEXT  : 0;
$style |= ($negative== "on"  ) ? BCS_REVERSE_COLOR  : 0;
 
 $obj = new products();
 $obj->connect();
 
?>

<table width="1008" height="47" border="0">
  <?php
   $row=$obj->getProducts();
   
   if(count($row)>0){
 	foreach($row as $row){
	
	if($row["pro_active"]==0){
	 $class="btn_normal";
	 $act=1;
	 $label="Activar";
	}
	else{
	 $class="btn_borrar";
	 $act=0;
	 $label="Desactivar";
	}
  ?>
  <tr>
    <td><?php echo $row["pro_name"];?></td>
    <td><? $bc = new I25Object(250, 120, $style, $row["pro_reference"]); 
		 echo "<img src='../barcode/image.php?code=".$row["pro_reference"]."&style=".$style."&type=".$type."&width=".$widthb."&height=".$heightb."&xres=".$xres."&font=".$font."' >";
		?></td>
  </tr>
  <?php
   }
  } 
  ?>
</table>
<script>
 window.print();
</script>
