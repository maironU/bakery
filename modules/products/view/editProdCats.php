<?php
  include('../../../core/config.php');
  include('../../../core/ge.php');
  include('../model/products.php');
 
  $obj = new products();
  $obj->connect();
  
  $id_product = $obj->getVars('id');
  
  
  if($_POST)
  {
      $obj->newProductsCategories();
  }
  
  if($obj->getVars('actionDel')==true)
  {
      $obj->delProductsCategories();
  }
?>
<link href="../../../css/scsStyle.css" rel="stylesheet" type="text/css" />
<div class="widget3">
 <div class="widgetlegend">Multiples categorias</div>
 
<form action="#" method="post">
  <table width="100%" border="0">
  <?php
   $row=$obj->getCategories();
   if(count($row)>0)
   {
	foreach($row as $row)
	{
	   
      ?>
      <tr>
        <td>
            <?php
                if($obj->isProductInCategory($id_product, $row['cat_id']))
                {
                    $id_pc = $obj->getProductCategoryId($id_product, $row['cat_id']);
                    ?>
                    <a href="<?php $_SERVER['PHP_SELF'];?>?id_pc=<?php echo $id_pc?>&id=<?php echo $id_product;?>&actionDel=true" class="btn_borrar">Quitar</a>
                    <?php
                }
                else
                {
                    ?>
                    <input type="checkbox" name="cat[]" value="<?php echo $row['cat_id']?>" class="toggle"/>
                    <?php
                }
            ?>
        </td>
        <td><?php echo $row['cat_name']?></td>
      </tr>
      <?php
  	}
   }
  ?>
  <tr>
      <td colspan="2">
          <input type="hidden" name="id_product" value="<?php echo $id_product?>" />
          <input type="submit" class="btn_submit" value="Guardar" />
      </td>
  </tr>
</table>
</form>

</div>