<?php
 include('modules/products/model/products.php');
 include ('modules/inventory/model/inventory.php');
 
 $obj = new products();
 $obj->connect();
 
 $inventory = new inventory();
 $inventory->connect();
 
 //Se verifica si el modulo esta instalado
 if(!$obj->Checker()){
  $ins=true;
 }
 
 $msg=false;
 $msgIns=false;
 
 //Instala el modulo
 if($obj->getVars('install')){
  $obj->install();
  $msgIns=true;
 }
 
 //Actions
 if($obj->getVars('ActionDel')){
  $obj->delProduct();
  $id = $obj->getVars('id');
  $inventory->delCapsInvProd2($id);
  $msg=true;
 }

?>
<?php
 if(!$ins){
 ?> 
<div class="widget3">
 <div class="widgetlegend">Productos </div>
 <?php
  if($msg)
  {
  ?>
   <div class="ui-widget">
	<div class="ui-state-highlight ui-corner-all" style="margin-top: 20px; padding: 0 .7em;">
		<p><span class="ui-icon ui-icon-info" style="float: left; margin-right: .3em;"></span>
		<strong>Felicitaciones!</strong> se ha sido eliminado satisfactoriamente.</p>
	</div>
</div>
  <?php
  }
 ?>
<p style="width:100">
	<a href="<?php $_SERVER['PHP_SELF'];?>?p=modules/products/view/newProduct.php" class="btn_normal" style="float:left; margin:5px;">Nuevo </a>
	<a href="<?php $_SERVER['PHP_SELF'];?>?p=modules/products/view/showCategories.php" class="btn_normal" style="float:left; margin:5px;">Categorias </a>
    <a href="<?php $_SERVER['PHP_SELF'];?>?p=modules/products/view/showCurrency.php" class="btn_normal" style="float:left; margin:5px;">Monedas </a>
	<a href="modules/products/view/exportCodes.php" class="btn_normal" style="float:left; margin:5px;" target="_blank">Codigos </a>
	<a href="<?php $_SERVER['PHP_SELF'];?>?p=modules/products/view/showTax.php" class="btn_normal" style="float:left; margin:5px;">Impuestos </a>
	<a href="<?php $_SERVER['PHP_SELF'];?>?p=modules/products/view/showParams.php" class="btn_normal" style="float:left; margin:5px;">Parametros </a>
</p>
 
<table width="803" height="48" border="0">
  <tr>
    <th width="30">ID</th>
    <th width="30">Imagen</th>
    <th width="213">Nombre</th>
    <th width="125">Descripcion</th>
	<th width="125">Referencia</th>
	<th width="125">Precio</th>
	<th width="125">Precio Costo</th>
	<th width="125">Tax</th>
    <th colspan="2">Acciones</th>
  </tr>
  <?php
   $row=$obj->getProducts();
   
   if(count($row)>0){
 	foreach($row as $row){
	
	if($row["pro_active"]==0){
	 $class="btn_normal";
	 $act=1;
	 $label="Activar";
	}
	else{
	 $class="btn_borrar";
	 $act=0;
	 $label="Desactivar";
	}
  ?>
  <tr>
    <td><?php echo $row["pro_id"];?></td>
    <td><img src="modules/products/imagesProd/<?php echo $row['pro_image']?>" width="50" height="50" /></td>
    <td><?php echo $row["pro_name"];?></td>
    <td><?php echo $row["pro_sdesc"];?></td>
	<td><?php echo $row["pro_reference"];?></td>
	<td><?php echo $row["pro_price"];?></td>
	<td><?php echo $row["pro_costprice"];?></td>
	<td><?php echo $row["prot_id"];?></td>
    <td width="86"><a href="<?php $_SERVER['PHP_SELF'];?>?p=modules/products/view/editProduct.php&id=<?php echo $row["pro_id"]?>" class="btn_normal">Editar</a>&nbsp;</td>
    <td width="81"><a href="<?php $_SERVER['PHP_SELF'];?>?p=modules/products/view/index.php&id=<?php echo $row["pro_id"]?>&ActionDel=true" class="btn_borrar">Eliminar</a>&nbsp;</td>
  </tr>
  <?php
   }
  } 
  ?>
</table>

</div>
<?php } else {?>
<br />
 
 <div class="widget3">
 <div class="widgetlegend">Instalador Modulo Productos </div>
 <?php
  if($msgIns)
  {
  ?>
   <div class="ui-widget">
	<div class="ui-state-highlight ui-corner-all" style="margin-top: 20px; padding: 0 .7em;">
		<p><span class="ui-icon ui-icon-info" style="float: left; margin-right: .3em;"></span>
		<strong>Felicitaciones!</strong> El modulo ha sido instalado satisfactoriamente.</p>
	</div>
</div>
  <?php
  }
 ?>
<form id="form1" name="form1" method="post" action="#">
  <table width="804" border="0" style="float:left;">
    <tr>
      <td width="525" valign="top">Bienvenido al instalador Modulo de Productos . </td>
    </tr>
    <tr>
      <td valign="top"><div align="right"><a href="<?php $_SERVER['PHP_SELF'];?>?p=modules/products/view/index.php&amp;install=true" class="btn_normal">Instalar</a></div></td>
    </tr>
  </table>
</form>
</div>
<?php } ?>
