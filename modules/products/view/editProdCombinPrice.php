<?php
  include('../../../core/config.php');
  include('../../../core/ge.php');
  include('../model/products.php');
 
  $obj = new products();
  $obj->connect();
  
  $id_product = $obj->getVars('id');
  
  if($_POST){
   if($obj->postVars('action')=='add'){
   	$obj->newCombinedPrice();
   }
   if($obj->postVars('action')=='edit'){
    $obj->editCombinedPrice();
   }
  }
  
  if($obj->getVars('actionDel')==true){
   	$obj->delCombinedPrice();
  }
  
  $row = $obj->getCombinedPriceByProduct($id_product);
  if(count($row)>0)
  {
	foreach($row as $row)
	{
		$total += $row["pcp_total"];
	}
  }
?>
<link href="../../../css/scsStyle.css" rel="stylesheet" type="text/css" />
<div class="widget3">
 <div class="widgetlegend">Precio Costo Construido </div>
 <form action="" method="post" enctype="multipart/form-data" name="formadd"><table width="100%" border="0">
  <tr>
  	<td>Nombre:<br>
  	<select name="id_priceitem" required>
  		<option value="">--Seleccionar--</option>
  		<?php
  			$row=$obj->getProductsByCategory(14);
  			if(count($row)>0)
  			{
  				foreach($row as $row)
  				{
  				?>
  					<option value="<?php echo $row["pro_id"]?>"><?php echo $row["pro_name"]?></option>
  				<?php
  				}
  			}
  		?>
  	</select>
  	</td>
  </tr>
  
  <!--<tr>
  	<td>Precio costo:<br><input type="text" name="value" id="value" required /></td>
  </tr>
  <tr>
  	<td>Porcentaje:<br><input type="text" name="por" id="por" required />%</td>
  </tr>-->
  <tr>
  	<td>Peso requerido:<br><input type="text" name="weight" id="weight" required /></td>
  </tr>
  <tr>
    <td>
      <input name="action" type="hidden" id="action" value="add" />
	  <input name="id_product" type="hidden" id="id_product" value="<?php echo $obj->getVars('id');?>" />
	  <input type="submit" value="Agregar" class="btn_submit" />
	   </td>
  </tr>
</table>
 </form> 

  <table width="100%" border="0">
  <?php
   $row=$obj->getCombinedPriceByProduct($obj->getVars('id'));
   if(count($row)>0){
    $i=1;
	foreach($row as $row){
	
  ?>
  <form action="" method="post" name="frmedit<?php echo $i?>">
  <tr>
    <td>Nombre:<br>
    <select name="id_priceitem" required>
  		<option value="">--Seleccionar--</option>
  		<?php
  			$row1=$obj->getProductsByCategory(14);
  			if(count($row1)>0)
  			{
  				foreach($row1 as $row1)
  				{
  					if($row["propi_id"]==$row1["pro_id"])
  					{
  					?>
  						<option value="<?php echo $row1["pro_id"]?>" selected><?php echo $row1["pro_name"]?></option>
  						<?php
  					}
  					else
  					{
  						?>
  						<option value="<?php echo $row1["pro_id"]?>"><?php echo $row1["pro_name"]?></option>
  						<?php
  					}
  				
  				}
  			}
  		?>
  	</select>
    </td>
    <!--<td>Precio costo:<br><input type="text" name="value" value="<?php //echo $row['pcp_value']?>" required /></td>
    <td>Porcentaje:<br><input type="text" name="por" value="<?php //echo $row['pcp_percentage']?>" required /></td>-->
    <td>Peso requerido:<br><input type="text" name="weight" value="<?php echo $row['pcp_weight']?>" required /></td>
    <td>Total:<br>$<?php echo number_format($row['pcp_total'],2)?></td>
    <td>
    <input type="hidden" name="id_combinprice" value="<?php echo $row['pcp_id']?>" />
    <input type="hidden" name="id_product" value="<?php echo $row['id_product']?>" />
    <input name="action" type="hidden" id="action" value="edit" />
    <input type="submit" value="Guardar" class="btn_submit" />
    <a href="<?php $_SERVER['PHP_SELF'];?>?id_combinprice=<?php echo $row['pcp_id']?>&id=<?php echo $row['id_product']?>&actionDel=true" class="btn_borrar">Eliminar</a></td>
  </tr>
  </form>
  <?php
  	$i++;
  	}
   }
  ?>
  <tr>
   <td colspan="5">Precio Costo Final: $<?php echo number_format($total,2)?></td>
  </tr>
</table>

</div>