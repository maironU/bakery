<?php
  include('../../../core/config.php');
  include('../../../core/ge.php');
  include('../model/products.php');
 
  $obj = new products();
  $obj->connect();
  
  
  if($_POST){
   if($obj->postVars('action')=='add'){
	$obj->newPrice();
   }
   if($obj->postVars('action')=='edit'){
    $obj->editPrice();
   }
  }
  
  if($obj->getVars('actionDel')==true){
   	$obj->delPrice();
  }
?>
<link href="../../../css/scsStyle.css" rel="stylesheet" type="text/css" />
<div class="widget3">
 <div class="widgetlegend">Precios</div>
 <form action="" method="post" enctype="multipart/form-data" name="formadd"><table width="100%" border="0">
  <tr>
    <td><label>
      <input name="name" type="text" id="name" placeholder="Descripcion" />
      <input name="price" type="text" id="price" placeholder="Precio Base" />
    </label>
    	<select name="id_currency">
         <?php
          $row=$obj->getCurrency();
		  foreach($row as $row){
		  ?>
          	<option value="<?php echo $row["pc_id"]?>"><?php echo $row["pc_code"]." ".$row["pc_name"]?></option>
          <?php
		  }
		 ?>
        </select>
        <select name="id_tax">
         <?php
          $row=$obj->getTax();
		  foreach($row as $row){
		  ?>
          	<option value="<?php echo $row["prot_id"]?>"><?php echo $row["prot_name"]?></option>
          <?php
		  }
		 ?>
        </select>
      <input name="action" type="hidden" id="action" value="add" />
	  <input name="id_product" type="hidden" id="id_product" value="<?php echo $obj->getVars('id');?>" />	  </td>
    <td><a href="javascript:;" class="btn_4" onclick="document.formadd.submit();">Agregar</a></td>
  </tr>
</table>
 </form> 

  <table width="100%" border="0">
  <?php
   $row=$obj->getProductPrices($obj->getVars('id'));
   if(count($row)>0){
    $i=1;
	foreach($row as $row){
	$frmname="form".$i;
  ?>
  <form action="" method="post" enctype="multipart/form-data" name="<?php echo $frmname; ?>">
  <tr>
    <td><label>
      <input name="name" type="text" id="name" placeholder="descripcion" value="<?php echo $row["pp_name"]?>" />
      <input name="price" type="text" id="price" placeholder="Precio Base" value="<?php echo $row["pp_base"]?>" />
      <?php echo "Precio final calculado: $".number_format($row["pp_price"])?><br>
      <?php echo "Valor Impuesto calculado: $".number_format($row["pp_tax"])?><br>
      <select name="id_currency">
         <?php
          $row1=$obj->getCurrency();
		  foreach($row1 as $row1){
			  if($row["pc_id"]==$row1["pc_id"]){
			   		?>
          				<option value="<?php echo $row1["pc_id"]?>" selected="selected"><?php echo $row1["pc_code"]." ".$row1["pc_name"]?></option>
          			<?php
			  }else{
			  	?>
          			<option value="<?php echo $row1["pc_id"]?>"><?php echo $row1["pc_code"]." ".$row1["pc_name"]?></option>
          		<?php
			  }
		  
		  }
		 ?>
        </select>
        <select name="id_tax">
         <?php
          $row1=$obj->getTax();
		  foreach($row1 as $row1){
			  if($row["prot_id"]==$row1["prot_id"]){
			   		?>
          				<option value="<?php echo $row1["prot_id"]?>" selected="selected"><?php echo $row1["prot_name"]?></option>
          			<?php
			  }else{
			  	?>
          			<option value="<?php echo $row1["prot_id"]?>"><?php echo $row1["prot_name"]?></option>
          		<?php
			  }
		  
		  }
		 ?>
        </select>
      <input name="id" type="hidden" value="<?php echo $row["pp_id"];?>" />
	  <input name="action" type="hidden" value="edit" />
    </label></td>
    <td><a href="javascript:;" class="btn_normal" onclick="document.<?php echo $frmname; ?>.submit();">Guardar</a></td>
    <td><a href="<?php $_SERVER['PHP_SELF'];?>?id=<?php echo $obj->getVars('id')?>&id2=<?php echo $row["pp_id"];?>&actionDel=true" class="btn_borrar">Eliminar</a></td>
  </tr>
  </form>
  <?php
     $i++;
  	}
   }
  ?>
</table>

</div>