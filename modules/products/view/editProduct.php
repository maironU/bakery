<?php
 include('modules/products/model/products.php');
 include("modules/products/fckeditor/fckeditor.php") ;
 include('modules/products/view/barcode.php');
 include ('modules/inventory/model/inventory.php');
 include ('modules/porders/model/porders.php');
 
 $obj = new products();
 $obj->connect();
 
 $inventory = new inventory();
 $inventory->connect();
 
 $po = new porders();
 $po->connect();
 
 $msg=false;
 
 if($_POST){
  $obj->editProduct();
  $msg=true;
      
    $id=$obj->postVars('id');
    $inv_id=$obj->postVars('inv_id');
    $name=$obj->postVars('name');
    $sdesc=$obj->postVars('sdesc');
    $price=$obj->postVars('price');
    $minstock=$obj->postVars('minstock');
    
    $inventory->editCapsInvProd3($id, $name, $desc, $price, $minstock);
    
  
 }
 
 $id=$obj->getVars('id');
 $row=$obj->getProductsById($id);
 foreach($row as $row){
 	$name=$row["pro_name"];
	$sdesc=$row["pro_sdesc"];
	$desc=$row["pro_desc"];
	$reference=$row["pro_reference"];
	$ean=$row["pro_ean"];
	$upc=$row["pro_upc"];
	$price=$row["pro_price"];
	$width=$row["pro_width"];
	$height=$row["pro_height"];
	$depth=$row["pro_depth"];
	$weight=$row["pro_weight"];
	$active=$row["pro_active"];
	$cat=$row["cat_id"];
	$warranty=$row["pro_warranty"];
	$inv_id=$row["inv_id"];//inventory
	$costprice=$row["pro_costprice"];
	$prot_id=$row["prot_id"];
	$qtycontainer=$row["pro_qtycontainer"];
	$varprice = $row['pro_varprice'];
	$assoc = $row['pro_assoc'];
	$image = $row['pro_image'];
	$id_provider = $row['popr_id'];
	$activefront = $row['pro_activefront'];
 }
 
    
    $row = $inventory->getCapsInvProdByProduct($id);
    if(count($row)>0)
    {
    	foreach($row as $row)
    	{
    		$minstock = $row['ip_minstock'];
    	}
    }
    
  

?>
<!-- Add mousewheel plugin (this is optional) -->
<script type="text/javascript" src="modules/products/js/jquery.mousewheel-3.0.6.pack.js"></script>
<!-- Add fancyBox main JS and CSS files -->
<script type="text/javascript" src="modules/products/js/jquery.fancybox.js?v=2.1.4"></script>
<link rel="stylesheet" type="text/css" href="modules/products/css/jquery.fancybox.css?v=2.1.4" media="screen" />
<div class="widget3">
 <div class="widgetlegend">Productos </div>
 <?php
  if($msg)
  {
  ?>
   <div class="ui-widget">
	<div class="ui-state-highlight ui-corner-all" style="margin-top: 20px; padding: 0 .7em;">
		<p><span class="ui-icon ui-icon-info" style="float: left; margin-right: .3em;"></span>
		<strong>Felicitaciones!</strong> El producto ha sido editado satisfactoriamente.</p>
	</div>
</div>
  <?php
  }
 ?>
<p style="width:100">
	<a href="<?php $_SERVER['PHP_SELF'];?>?p=modules/products/view/index.php" class="btn_normal" style="float:left; margin:5px;">Volver </a>
	<a href="javascript:;" class="btn_normal"  onclick="showInfo(<?php echo $obj->getVars('id');?>,'modules/products/view/editProdImages.php');" style="float:left; margin:5px;">Imagenes </a>	<a href="javascript:;" onclick="showInfo(<?php echo $obj->getVars('id');?>,'modules/products/view/editProdDocuments.php');" class="btn_normal" style="float:left; margin:5px;">Archivos </a>
    <a href="javascript:;" onclick="showInfo(<?php echo $obj->getVars('id');?>,'modules/products/view/editProdPrices.php');" class="btn_normal" style="float:left; margin:5px;">Precios </a>
    <a href="javascript:;" onclick="showInfo(<?php echo $obj->getVars('id');?>,'modules/products/view/editProdCats.php');" class="btn_normal" style="float:left; margin:5px;">Categorias </a>
</p>
 
<form action="#" method="post" enctype="multipart/form-data" name="form1" id="form1">
  <table width="800" border="0" style="float:left;">
    <tr>
      <td width="265" rowspan="2" valign="top"><table width="235" height="127" border="0">
        <tr>
          <td width="229"><label>Nombre</label><br />

            <input name="name" type="text" id="name" value="<?php echo $name; ?>" />          </td>
        </tr>
       <tr>
          <td width="229"><label>Referencia</label><br />

            <input name="reference" type="text" id="reference" value="<?php echo $reference; ?>" /><a href="javascript:;" onclick="generate()" class="btn_borrar">Generar</a>
            <br />
			<? $bc = new I25Object(250, 120, $style, $reference); 
		 echo "<img src='modules/products/barcode/image.php?code=".$reference."&style=".$style."&type=".$type."&width=".$widthb."&height=".$heightb."&xres=".$xres."&font=".$font."' >";
		?>	
			</td>
        </tr>
        <tr>
            <td width="229"><label>Imagen</label><br />

            <input name="img" type="file" id="img" value="" /><br>
            <img src="modules/products/imagesProd/<?php echo $image?>" width="100" height="100" />
            
            </td>
        </tr>
        <tr>
          <td width="229"><label>EAN</label><br />

            <input name="ean" type="text" id="ean" value="<?php echo $ean; ?>" />          </td>
        </tr>
		<tr>
          <td width="229"><label>UPC</label><br />

            <input name="upc" type="text" id="upc" value="<?php echo $upc; ?>" />          </td>
        </tr>
        <tr>
          <td width="229"><label>Cantidad de Contenedor</label><br />

            <input name="qtycontainer" type="text" id="qtycontainer" value="<?php echo $qtycontainer; ?>" />          </td>
        </tr>
        <tr>
          <td width="229"><label>Precio Costo</label> 
          :<br />

            <input name="costprice" type="text" id="costprice" value="<?php echo $costprice; ?>" /> <br>
            <a href="javascript:;" onclick="showInfo(<?php echo $obj->getVars('id');?>,'modules/products/view/editProdCombinPrice.php');" class="btn_3" style="float:left; margin:5px;">Construir </a>
            </td>
        </tr>
        <tr>
          <td width="229"><label>Precio Variable</label> 
          :<br />
          <?php
            if($varprice == 1)
            {
                ?>
                <input name="varprice" type="checkbox" class="toggle" id="varprice" value="1" checked/>
                <?php
            }
            else
            {
                ?>
                <input name="varprice" type="checkbox" class="toggle" id="varprice" value="1" />
                <?php
            }
          ?>

            </td>
        </tr>
		<tr>
          <td width="229"><label>Precio venta</label> 
          <br />

            <input name="price" type="text" id="price" value="<?php echo $price; ?>" />          </td>
        </tr>
        <tr>
          <td><label>Impuestos</label>
            <br />
            <label>
            <select name="prot_id" id="prot_id">
              <option value="">-- Select --</option>
              <?php
			   $row=$obj->getTax();
			   if(count($row)>0)
			   {
			    foreach($row as $row){
			        
			        if($prot_id == $row["prot_id"])
			        {
			          ?>
              <option value="<?php echo $row["prot_id"];?>" selected><?php echo $row["prot_name"];?></option>
              <?php  
			        }
			        else
			        {
			           ?>
              <option value="<?php echo $row["prot_id"];?>"><?php echo $row["prot_name"];?></option>
              <?php 
			        }
				
				}
			   }
			  ?>
            </select>
            </label></td>
        </tr>
        <tr>
          <td width="229"><label>Garantia (Meses):</label><br />

            <input name="warranty" type="text" id="warranty" value="<?php echo $warranty; ?>" />          </td>
        </tr>
		<tr>
          <td width="229"><label>Ancho:</label><br />

            <input name="width" type="text" id="width" value="<?php echo $width; ?>" />          </td>
        </tr>
		<tr>
          <td width="229"><label>Alto</label><br />

            <input name="height" type="text" id="height" value="<?php echo $height; ?>" />          </td>
        </tr>
		<tr>
          <td width="229"><label>Profundo</label><br />

            <input name="depth" type="text" id="depth" value="<?php echo $depth; ?>" />          </td>
        </tr>
		<tr>
          <td width="229"><label>Peso</label><br />

            <input name="weight" type="text" id="weight" value="<?php echo $weight; ?>" />          </td>
        </tr>
        <tr>
          <td width="229"><label>Minimo de Stock</label><br />

            <input name="minstock" type="text" id="minstock" value="<?php echo $minstock?>" />          </td>
        </tr>
        <tr>
          <td><label>Activar</label>
		   <?php
		    if($active==1){
				?>
				<input name="active" type="checkbox" id="active" value="1" checked="checked" />
				<?php
			}
			else
			{
				?>
				<input name="active" type="checkbox" id="active" value="1" />
				<?php
			}
		   ?>          </td>
        </tr>
        <tr>
          <td><label>Activar en Ecommerce</label>
		   <?php
		    if($activefront==1){
				?>
				<input name="activefront" type="checkbox" id="active" value="1" checked="checked" class="toggle"/>
				<?php
			}
			else
			{
				?>
				<input name="activefront" type="checkbox" id="activefront" value="1" class="toggle"/>
				<?php
			}
		   ?>          </td>
        </tr>
        <tr>
          <td><label>Producto Asociado</label>
            <br />
            <label>
            <select name="assoc" id="assoc">
              <option value="0">-- Select --</option>
              <?php
			   $row=$obj->getProducts();
			   if(count($row)>0)
			   {
			    foreach($row as $row){
			        
			        if($assoc == $row["pro_id"])
			        {
			            ?>
                          <option value="<?php echo $row["pro_id"];?>" selected><?php echo $row["pro_name"];?></option>
                        <?php
			        }
			        else
			        {
			            ?>
                          <option value="<?php echo $row["pro_id"];?>"><?php echo $row["pro_name"];?></option>
                        <?php
			        }
				
				}
			   }
			  ?>
            </select>
            </label></td>
        </tr>
        <tr>
          <td><label>Categoria</label>
            <br />
            <label>
            <select name="cat" id="cat">
              <option value="">-- Select --</option>
              <?php
			   $row=$obj->getCategories();
			   if(count($row)>0)
			   {
			    foreach($row as $row){
				 if($cat==$row["cat_id"]){
				?>
              <option value="<?php echo $row["cat_id"];?>" selected="selected"><?php echo $row["cat_name"];?></option>
              <?php 
				 }else{
				?>
              <option value="<?php echo $row["cat_id"];?>"><?php echo $row["cat_name"];?></option>
              <?php
			  	 }	
				}
			   }
			  ?>
            </select>
            </label></td>
        </tr>
        <tr>
          <td><label>Proveedores</label>
            <br />
            <label>
            <select name="id_provider" id="id_provider">
              <option value="">-- Select --</option>
              <?php
			   $row=$po->getProvider();
			   if(count($row)>0)
			   {
			    foreach($row as $row){
			        if($id_provider == $row["popr_id"])
			        {
			           ?>
              <option value="<?php echo $row["popr_id"];?>" selected><?php echo $row["popr_name"];?></option>
              <?php 
			        }
			        else
			        {
			         ?>
              <option value="<?php echo $row["popr_id"];?>"><?php echo $row["popr_name"];?></option>
              <?php   
			        }
				
				}
			   }
			  ?>
            </select>
            </label></td>
        </tr>
      </table></td>
      <td width="525" valign="top"><label>Descripci&oacute;n Corta </label>
        <br />
        <textarea name="sdesc" cols="100" rows="5" id="sdesc"><?php echo $sdesc; ?></textarea>
      </td>
    </tr>
    <tr>
      <td valign="top"><?php
                                $oFCKeditor_l = new FCKeditor('desc') ;
                                $oFCKeditor_l->BasePath = 'modules/products/fckeditor/';
                                $oFCKeditor_l->Width  = '650' ;
                                $oFCKeditor_l->Height = '600' ;
                                $oFCKeditor_l->Value = $desc;
                                $oFCKeditor_l->Create() ;
                            ?></td>
    </tr>
    <tr>
      <td valign="top"><div align="right"></div></td>
      <td valign="top"><div align="right"><a href="javascript:;" class="btn_normal" onclick="document.form1.submit();">
        <input type="hidden" id="id" name="id" value="<?php echo $id; ?>" />
        <input type="hidden" id="inv_id" name="inv_id" value="<?php echo $inv_id; ?>" />
		Guardar</a></div></td>
    </tr>
  </table>
</form>

</div>
<script>
 function showInfo(id,site)
 {
	$.fancybox.open({
					href : site+'?id='+id,
					type : 'iframe',
					padding:5
				}); 
 }

</script>
<script>
function generate(){
	$.ajax({
	url: 'modules/products/view/ajax_generate_reference.php',
	async: true,
	success: function(data){
		 document.form1.reference.value=data;
	}
 });
}
</script>

