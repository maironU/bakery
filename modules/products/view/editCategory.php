<?php
 include('modules/products/model/products.php');
 
 $obj = new products();
 $obj->connect();
 
 $msg=false;
 
 if($_POST){
  $obj->editCategories();
  $msg=true;
 }
 
 $id=$obj->getVars('id');
 $row=$obj->getCategoriesById($id);
 foreach($row as $row){
 	$name=$row["cat_name"];
	$desc=$row["cat_desc"];
	$father=$row["cat_father"];
	$image=$row["cat_image"];
	$active=$row["cat_active"];
	$activefront=$row["cat_activefront"];
	$activeout=$row["cat_activeoutstanding"];
	$best=$row["cat_best"];
 }

?>
<div class="widget3">
 <div class="widgetlegend">Categorias </div>
 <?php
  if($msg)
  {
  ?>
   <div class="ui-widget">
	<div class="ui-state-highlight ui-corner-all" style="margin-top: 20px; padding: 0 .7em;">
		<p><span class="ui-icon ui-icon-info" style="float: left; margin-right: .3em;"></span>
		<strong>Felicitaciones!</strong>Se ha sido creado satisfactoriamente.</p>
	</div>
</div>
  <?php
  }
 ?>
<p style="width:100">
	<a href="<?php $_SERVER['PHP_SELF'];?>?p=modules/products/view/showCategories.php" class="btn_normal" style="float:left; margin:5px;">Volver </a>
</p>
 
<form action="#" method="post" enctype="multipart/form-data" name="form1" id="form1">
<table width="803" height="48" border="0" style="float:left">
  <tr>
    <td><label>Nombre: </label>
      <br />
      <input name="name" type="text" id="name" value="<?php echo $name; ?>" />    </td>
  </tr>
  <tr>
    <td><label>Imagen: </label><br />
      <label>
      <input name="imgCat" type="file" id="imgCat" />
      <br />
      <img src="modules/products/imagesCat/<?php echo $image;?>" width="100" height="100" /></label></td>
  </tr>
  <tr>
    <td><label>Descripci&oacute;n: </label><br />
      <textarea name="desc" cols="50" rows="5" id="desc"><?php echo $desc; ?></textarea>    </td>
  </tr>
  <tr>
    <td><label>Categoria Padre </label>
      <br />
      <label>
      <select name="father" id="father">
        <option value="">-- Select --</option>
        <?php
			   $row=$obj->getCategories();
			   if(count($row)>0)
			   {
			    foreach($row as $row){
				if($father==$row["cat_id"]){
				?>
        <option value="<?php echo $row["cat_id"];?>" selected="selected"><?php echo $row["cat_name"];?></option>
        <?php	 
				}else{
				?>
        <option value="<?php echo $row["cat_id"];?>"><?php echo $row["cat_name"];?></option>
        <?php	 
				 }
				}
			   }
			  ?>
      </select>
      </label></td>
  </tr>
  <tr>
    <td><label>Activar: </label><br />
      <?php
		    if($active==1){
				?>
                  <input name="active" type="checkbox" id="active" value="1" checked="checked" class="toogle"/>
                 <?php
			}
			else
			{
				?>
                  <input name="active" type="checkbox" id="active" value="1" class="toogle"/>
                <?php
			}
		   ?></td>
  </tr>
  <tr>
    <td><label>Activar en Ecommerce: </label><br />
      <?php
		    if($activefront==1){
				?>
                  <input name="activefront" type="checkbox" id="activefront" value="1" checked="checked" class="toogle"/>
                 <?php
			}
			else
			{
				?>
                  <input name="activefront" type="checkbox" id="activefront" value="1" class="toogle"/>
                <?php
			}
		   ?></td>
  </tr>
  <tr>
    <td><label>Categoria Destacada: </label><br />
      <?php
		    if($activeout==1){
				?>
                  <input name="activeout" type="checkbox" id="activeout" value="1" checked="checked" class="toogle"/>
                 <?php
			}
			else
			{
				?>
                  <input name="activeout" type="checkbox" id="activeout" value="1" class="toogle"/>
                <?php
			}
		   ?></td>
  </tr>
  <tr>
    <td><label>Mejor categoria: </label><br />
      <?php
		    if($best==1){
				?>
                  <input name="best" type="checkbox" id="best" value="1" checked="checked" class="toogle"/>
                 <?php
			}
			else
			{
				?>
                  <input name="best" type="checkbox" id="best" value="1" class="toogle"/>
                <?php
			}
		   ?></td>
  </tr>
  <tr>
    <td><a href="javascript:;" class="btn_normal" onclick="document.form1.submit();">Guardar
      <input name="id" type="hidden" id="id" value="<?php echo $id; ?>" />
    </a></td>
  </tr>
</table>
</form>


</div>
