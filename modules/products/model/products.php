<?php
class products extends GeCore{
	
	
	//Categorias
	public function newCategories(){
		
		$name=$this->postVars('name');
		$desc=$this->postVars('desc');
		$father=$this->postVars('father');
		$active=$this->postVars('active');
		$activefront=$this->postVars('activefront');
		$activeout=$this->postVars('activeout');
		$best=$this->postVars('best');
		
		$sql="insert into `categories` (`cat_name`, `cat_desc`, `cat_father`, `cat_active`, `cli_id`, `com_id`, `cat_activefront`, `cat_activeoutstading`, `cat_best`) values ('$name','$desc','$father','$active','$cli_id','$com_id','$activefront','$activeout','$best')";
		$this->Execute($sql);
		
		$id=$this->getLastID();
		
		$this->uploadImageCat($id);
	}
	
	public function uploadImageCat($id){
		
		$ok=$this->upLoadFileProccess('imgCat', 'modules/products/imagesCat');
		
		if($ok){
		 $sql="update categories set cat_image='".$this->fname."' where cat_id='$id'";
		 $this->Execute($sql);
		}
		
	}
	
	
	
	public function editCategories(){
		$id=$this->postVars('id');
		$name=$this->postVars('name');
		$desc=$this->postVars('desc');
		$father=$this->postVars('father');
		$active=$this->postVars('active');
		$activefront=$this->postVars('activefront');
		$activeout=$this->postVars('activeout');
		$best=$this->postVars('best');
		
		
		$sql="update `categories` set `cat_name`='$name', `cat_desc`='$desc', `cat_father`='$father', `cat_active`='$active', `cat_activefront`='$activefront', `cat_activeoutstanding`='$activeout', `cat_best`='$best' where cat_id='$id'";
		$this->Execute($sql);
		
		
		$this->uploadImageCat($id);
	}
	
	
	
	public function catActive($id,$act){
		
		$sql="update `categories` set `cat_active`='$act' where cat_id='$id'";
		$this->Execute($sql);
		
	}
	
	public function delCategories(){
		$id=$this->getVars('id');
		
		$sql="delete from `categories` where cat_id='$id'";
		$this->Execute($sql);
		
		$this->MassProdDel($id);
	}
	
	public function getCategories(){
	 $sql="select * from categories";
	 return $this->ExecuteS($sql);
	}
	
	public function getCategoriesById($id){
	 
	 $sql="select * from categories where cat_id='$id'";
	 return $this->ExecuteS($sql);
	}
	
	public function getCategoriesByComId($id){
	 
	 $sql="select * from categories where com_id='$id'";
	 return $this->ExecuteS($sql);
	}
	
	public function getCategoriesByCliId($id){
	 
	 $sql="select * from categories where cli_id='$id'";
	 return $this->ExecuteS($sql);
	}
	
	public function getCategoriesFiltered(){
	 $sql="select * from categories where cat_id != '14' AND cat_active='1'";
	 return $this->ExecuteS($sql);
	}
	
	public function getCategoriesActive(){
	 $sql="select * from categories where cat_active='1'";
	 return $this->ExecuteS($sql);
	}
	
	public function getCategoriesActiveFront(){
	 $sql="select * from categories where cat_activefront='1'";
	 return $this->ExecuteS($sql);
	}
	
	public function getCategoriesActiveOutStanding(){
	 $sql="select * from categories where cat_activefront='1' AND cat_activeoutstanding='1'";
	 return $this->ExecuteS($sql);
	}
	
	public function getCategoriesBest(){
	 $sql="select * from categories where cat_activefront='1' AND cat_best='1'";
	 return $this->ExecuteS($sql);
	}
	
	//Productos
	public function newProduct(){
	 	$name=$this->postVars('name');
		$sdesc=$this->postVars('sdesc');
		$desc=$this->postVars('desc');
		$reference=$this->postVars('reference');
		$ean=$this->postVars('ean');
		$upc=$this->postVars('upc');
		$price=$this->postVars('price');
		$width=$this->postVars('width');
		$height=$this->postVars('height');
		$depth=$this->postVars('depth');
		$weight=$this->postVars('weight');
		$active=$this->postVars('active');
		$cat_id=$this->postVars('cat');
		$warranty=$this->postVars('warranty');
		$costprice=$this->postVars('costprice');
		$prot_id=$this->postVars('prot_id');
		$qtycontainer=$this->postVars('qtycontainer');
		$varprice=$this->postVars('varprice');
		$assoc=$this->postVars('assoc');
		$id_provider=$this->postVars('id_provider');
		$activefront=$this->postVars('activefront');
		
		$sql="insert into `products` (`pro_name`, `pro_sdesc`, `pro_desc`, `pro_reference`, `pro_ean`, `pro_upc`, `pro_active`, `pro_price`, `pro_pricefob`, `pro_wholesaleprice`, `pro_height`, `pro_weight`, `pro_depth`, `pro_width`, `com_id`, `cli_id`, `cat_id`, `pro_warranty`, `pro_costprice`, `prot_id`, `pro_qtycontainer`,`pro_varprice`, `pro_assoc`, `pro_image`, `popr_id`, `pro_activefront`) values ('$name','$sdesc','$desc','$reference','$ean','$upc','$active','$price','$pricefob','$pricewholesale','$height','$weight','$depth','$width','$com_id','$cli_id','$cat_id','$warranty','$costprice','$prot_id','$qtycontainer','$varprice','$assoc','product-icon.png','$id_provider','$activefront')";
		$this->Execute($sql);
		
		$id = $this->getLastID();
		$this->uploadProductsImage($id);
		return $id;
		
		
	}
	
	public function uploadProductsImage($id){
		
		$ok=$this->upLoadFileProccess('img', 'modules/products/imagesProd');
		
		if($ok){
		 $sql="update `products` set `pro_image`='".$this->fname."' where pro_id='$id'";
		 $this->Execute($sql);
		}
		
	}
	
	public function productActivate($id,$act){
		
		$sql="update `products` set `pro_active`='$act' where pro_id='$id'";
		$this->Execute($sql);
		
	}
	
	public function calcTaxValue($price, $id_tax)
	{
	    $row = $this->getTaxById($id_tax);
	    if(count($row)>0)
	    {
	        foreach($row as $row)
	        {
	            $por = $row['prot_value'];
	        }
	    }
	    
	    $por /= 100;
	    $tax = $price * $por;
	    return $tax;
	}
	
	public function editProduct(){
	 	$id=$this->postVars('id');
		$name=$this->postVars('name');
		$sdesc=$this->postVars('sdesc');
		$desc=$this->postVars('desc');
		$reference=$this->postVars('reference');
		$ean=$this->postVars('ean');
		$upc=$this->postVars('upc');
		$price=$this->postVars('price');
		$width=$this->postVars('width');
		$height=$this->postVars('height');
		$depth=$this->postVars('depth');
		$weight=$this->postVars('weight');
		$active=$this->postVars('active');
		$cat_id=$this->postVars('cat');
		$warranty=$this->postVars('warranty');
		$costprice=$this->postVars('costprice');
		$prot_id=$this->postVars('prot_id');
		$qtycontainer=$this->postVars('qtycontainer');
		$varprice=$this->postVars('varprice');
		$assoc=$this->postVars('assoc');
		$id_provider=$this->postVars('id_provider');
		$activefront=$this->postVars('activefront');
		
	 	$sql="update `products` set `pro_name`='$name', `pro_sdesc`='$sdesc', `pro_desc`='$desc', `pro_reference`='$reference', `pro_ean`='$ean', `pro_upc`='$upc', `pro_price`='$price', `pro_pricefob`='$pricefob', `pro_wholesaleprice`='$pricewholesale', `pro_height`='$height', `pro_weight`='$weight', `pro_depth`='$depth', `pro_width`='$width', `cat_id`='$cat_id', `pro_active`='$active', `pro_warranty`='$warranty', `pro_costprice`='$costprice', `prot_id`='$prot_id', `pro_qtycontainer`='$qtycontainer', `pro_varprice`='$varprice', `pro_assoc`='$assoc',`popr_id`='$id_provider',`pro_activefront`='$activefront' where pro_id='$id'";
		$this->Execute($sql);
		
		$this->uploadProductsImage($id);
		
		//$this->uploadClientImage($id);
		
	}
	
	public function delProduct(){
	 $id=$this->getVars('id');
	 $sql="delete from products where pro_id='$id'";
	 $this->Execute($sql);
	 
	}
	
	public function getProductsByCom($id){
	 $sql="select * from products where com_id='$id'";
	 return $this->ExecuteS($sql);
	}
	
	public function getProductsByClient($id){
	 $sql="select * from products where cli_id='$id'";
	 return $this->ExecuteS($sql);
	}
	
	public function getProductsByCategory($id){
	 $sql="select * from products where cat_id='$id' order by pro_name asc";
	 return $this->ExecuteS($sql);
	}
	
	public function getProductsByCategoryAndActiveFront($id){
	 $sql="select * from products where cat_id='$id' and pro_activefront='1' order by pro_name asc";
	 return $this->ExecuteS($sql);
	}
	
	public function getProductsByReference($barcode){
	 $sql="select * from products where pro_reference='$barcode'";
	 return $this->ExecuteS($sql);
	}
	
	public function getProducts(){
	 $sql="select * from products order by pro_name";
	 return $this->ExecuteS($sql);
	}
	
	public function getProductsById($id){
	 $sql="select * from products where pro_id='$id'";
	 return $this->ExecuteS($sql);
	}
	
	public function MassProdDel($id){
		$sql="update products set cat_id='' where cat_id='$id'";
		$this->Execute($sql);
	}
	
	public function findProduct($term){
	 $sql="select * from products where pro_sdesc like '%$term%'";
	 return $this->ExecuteS($sql);
	}
	
	public function findProduct2($term){
	 $sql="select * from products where pro_reference like '%$term%'";
	 return $this->ExecuteS($sql);
	}
	
	public function findProductByName($term){
	 $sql="select * from products where pro_name like '%$term%'";
	 return $this->ExecuteS($sql);
	}

	public function findProductByNameReferenceOrDescription($term){
		$sql="select * from products where pro_name like '%$term%' or pro_reference like '%$term%' or pro_sdesc like '%$term%'";
		return $this->ExecuteS($sql);
	   }
	
	public function isVariablePrice($id)
	{
	    $row = $this->getProductsById($id);
	    if(count($row)>0)
	    {
	        foreach($row as $row)
	        {
	            $varprice = $row['pro_varprice'];
	        }
	    }
	    
	    if($varprice == '1')
	    {
	        return true;
	    }
	    else
	    {
	        return false;
	    }
	}
	
	public function getProductAssoc($id)
	{
	    $row = $this->getProductsById($id);
	    if(count($row)>0)
	    {
	        foreach($row as $row)
	        {
	            return $row['pro_assoc'];
	        }
	    }
	}
	
	//Imagenes de productos
	public function getProductImages($id){
	 $sql="select * from products_images where pro_id='$id'";
	 return $this->ExecuteS($sql);
	}
	
	public function getProductFirstImages($id){
	 $sql="select * from products_images where pro_id='$id' order by proimg_id asc LIMIT 0,1";
	 return $this->ExecuteS($sql);
	}
	
	public function getProductImagesMain($id){
	 $sql="select * from products_images where pro_id='$id' and proimg_main='1'";
	 return $this->ExecuteS($sql);
	}
	
	public function newProductsImage($id){
		
		$ok=$this->upLoadFileProccess('imgProd', '../imagesProd');
		
		if($ok){
		 $sql="insert `products_images` (`proimg_file`, `proimg_active`, `proimg_main`, `pro_id`) values ('".$this->fname."','1','0','$id')";
		 $this->Execute($sql);
		}
		
	}
	
	public function editProductsImage($id){
	 $ok=$this->upLoadFileProccess('imgProd', '../imagesProd');
		
		if($ok){
		 $sql="update `products_images` set `proimg_file`='".$this->fname."' where proimg_id='$id'";
		 $this->Execute($sql);
		}
	}
	
	public function delProductsImage($id){
	 $sql="delete from products_images where proimg_id='$id'";
	 $this->Execute($sql);
	}
	
	public function productImageActivate($id,$act){
		
		$sql="update `products_images` set `proimg_active`='$act' where proimg_id='$id'";
		$this->Execute($sql);
		
	}
	
	public function setProductImageMain($id){
		
		$sql="update `products` set `proimg_main`='0'";
		$this->Execute($sql);
		
		$sql="update `products` set `proimg_main`='1' where pro_id='$id'";
		$this->Execute($sql);
		
	}
	
	//Archivos de Productos
	public function getProductFiles($id){
	 $sql="select * from products_files	 where pro_id='$id'";
	 return $this->ExecuteS($sql);
	}
	
	public function getProductFirstFiles($id){
	 $sql="select * from products_files	 where pro_id='$id' order by profile_id asc LIMIT 0,1";
	 return $this->ExecuteS($sql);
	}
	
	public function newProductsFiles($id){
		
		$ok=$this->upLoadFileProccess('filesProd', '../filesProd');
		
		if($ok){
		 $sql="insert `products_files` (`profile_file`, `profile_active`, `pro_id`) values ('".$this->fname."','1','$id')";
		 $this->Execute($sql);
		}
		
	}
	
	public function editProductsFiles($id){
	 $ok=$this->upLoadFileProccess('filesProd', '../filesProd');
		
		if($ok){
		 $sql="update `products_files` set `profile_file`='".$this->fname."' where profile_id='$id'";
		 $this->Execute($sql);
		}
	}
	
	public function delProductsFiles($id){
	 $sql="delete from products_files where profile_id='$id'";
	 $this->Execute($sql);
	}
	
	//Currencies Methods
	public function newCurrency(){
	 $name=$this->postVars('name');
	 $code=$this->postVars('code');
	 
	 $sql="INSERT INTO `product_currency`(`pc_name`, `pc_code`) VALUES ('$name','$code')";
	 $this->Execute($sql);	
	
	}
	
	public function editCurrency(){
	 $id=$this->postVars('id');
	 $name=$this->postVars('name');
	 $code=$this->postVars('code');
	 
	 $sql="UPDATE `product_currency` SET `pc_name`='$name',`pc_code`='$code' WHERE `pc_id`='$id'";
	 $this->Execute($sql);	
	
	}
	
	public function delCurrency(){
	 $id=$this->getVars('id');
	 
	 $sql="delete from `product_currency` WHERE `pc_id`='$id'";
	 $this->Execute($sql);	
	
	}
	
	public function getCurrency(){
	 $sql="select * from `product_currency`";
	 return $this->ExecuteS($sql);
	}
	
	public function getCurrencyById($id){
		$sql="select * from `product_currency` where pc_id='$id'";
	 	return $this->ExecuteS($sql);
	}
	
	//Product Price
	public function newPrice(){
	 $id_product=$this->postVars('id_product');
	 $id_currency=$this->postVars('id_currency');
	 $id_tax=$this->postVars('id_tax');
	 $price=$this->postVars('price');
	 $name=$this->postVars('name');
	 
	 $row = $this->getTaxById($id_tax);
	 foreach($row as $row)
	 {
	     $por = $row['prot_value'];
	 }
	 
	 $por /= 100;
	 $tax = $price * $por;
	 $final_price = $price + $tax;
	 
	 
	 $sql="INSERT INTO `product_prices`(`pp_price`, `pc_id`, `pro_id`, `pp_name`, `prot_id`, `pp_base`, `pp_tax`) VALUES ('$final_price','$id_currency','$id_product','$name','$id_tax','$price','$tax')";
	 $this->Execute($sql);
	 
	}
	
	public function editPrice(){
	 $id=$this->postVars('id');
	 $id_currency=$this->postVars('id_currency');
	 $price=$this->postVars('price');
	 $name=$this->postVars('name');
	 $id_tax=$this->postVars('id_tax');
	 
	 $row = $this->getTaxById($id_tax);
	 foreach($row as $row)
	 {
	     $por = $row['prot_value'];
	 }
	 
	 $por /= 100;
	 $tax = $price * $por;
	 $final_price = $price + $tax;
	 
	 $sql="update `product_prices` set `pp_price`='$final_price', `pc_id`='$id_currency', `pp_name`='$name', `prot_id`='$id_tax', `pp_base`='$price', `pp_tax`='$tax' where pp_id='$id'";
	 $this->Execute($sql);
	 
	}
	
	public function delPrice(){
	 $id=$this->getVars('id2');
	 
	 $sql="delete from `product_prices` where pp_id='$id'";
	 $this->Execute($sql);
	 
	}
	
	public function getProductPrices($id_product){
		$sql="select * from product_prices where pro_id='$id_product'";
		return $this->ExecuteS($sql);
	}
	
	public function getPriceById($id){
		$sql="select * from product_prices where pp_id='$id'";
		return $this->ExecuteS($sql);
	}
	
	//Taxes Methods
	public function newTax()
	{
	    $name = $this->postVars('name');
	    $value = $this->postVars('value');
	    
	    $sql="INSERT INTO `products_taxes`(`prot_name`, `prot_value`) VALUES ('$name','$value')";
	    $this->Execute($sql);
	}
	
	public function editTax()
	{
	    $id = $this->postVars('id');
	    $name = $this->postVars('name');
	    $value = $this->postVars('value');
	    
	    $sql="UPDATE `products_taxes` SET `prot_name`='$name', `prot_value`='$value' WHERE prot_id='$id'";
	    $this->Execute($sql);
	}
	
	public function delTax()
	{
	    $id = $this->getVars('id');
	    
	    $sql="DELETE FROM `products_taxes` WHERE prot_id='$id'";
	    $this->Execute($sql);
	}
	
	public function getTaxById($id)
	{
	    $sql="SELECT * FROM `products_taxes` WHERE prot_id='$id'";
	    return $this->ExecuteS($sql);
	}
	
	public function getTax()
	{
	    $sql="SELECT * FROM `products_taxes`";
	    return $this->ExecuteS($sql);
	}
	
	//Product Combined
	public function newCombinedPrice()
	{
	    $name = $this->postVars('name');
	    $value = $this->postVars('value');
	    $por = $this->postVars('por');
	    $id_product = $this->postVars('id_product');
	    $id_priceitem = $this->postVars('id_priceitem');
	    $weight = $this->postVars('weight');
	    $opt = $this->postVars('opt');
	    
	    //$total = ($por/100) * $value;
	    
	    $row = $this->getProductsById($id_priceitem);
	    if(count($row)>0)
	    {
	        foreach($row as $row)
	        {
	            $w = $row['pro_weight'];
	            $cost = $row['pro_costprice'];
	        }
	    }
	    
	    $w1 = $w / $weight;
	    $total = $cost * $w1;
	    
	    
	    $sql = "INSERT INTO `products_combinprice`(`pcp_name`, `pcp_value`, `pcp_percentage`, `id_product`, `pcp_total`, `propi_id`, `pcp_weight`) VALUES ('$name','$value','$por','$id_product','$total','$id_priceitem','$weight')";
	    $this->Execute($sql);
	    
	    $this->editCostPrice($id_product);
	    
	}
	
	public function editCombinedPrice()
	{
	    $id = $this->postVars('id_combinprice');
	    $name = $this->postVars('name');
	    $value = $this->postVars('value');
	    $por = $this->postVars('por');
	    $id_product = $this->postVars('id_product');
	    $id_priceitem = $this->postVars('id_priceitem');
	    $weight = $this->postVars('weight');
	    
	    $row = $this->getProductsById($id_priceitem);
	    if(count($row)>0)
	    {
	        foreach($row as $row)
	        {
	            $w = $row['pro_weight'];
	            $cost = $row['pro_costprice'];
	        }
	    }
	    
	    $w1 = $w / $weight;
	    $total = $cost * $w1;
	    
	    $sql = "UPDATE `products_combinprice` SET `pcp_name`='$name',`pcp_value`='$value',`pcp_percentage`='$por',`pcp_total`='$total',`propi_id`='$id_priceitem', `pcp_weight`='$weight' WHERE `pcp_id`='$id'";
	    $this->Execute($sql);
	    
	    $this->editCostPrice($id_product);
	}
	
	public function delCombinedPrice()
	{
	    $id_combinprice = $this->getVars('id_combinprice');
	    $id = $this->getVars('id');
	    $sql = "DELETE FROM `products_combinprice` WHERE `pcp_id`='$id_combinprice'";
	    $this->Execute($sql);
	    
	    $this->editCostPrice($id);
	}
	
	public function getCombinedPriceByProduct($id_product)
	{
	    $sql = "SELECT * FROM `products_combinprice` WHERE `id_product`='$id_product'";
	    return $this->ExecuteS($sql);
	}
	
	public function editCostPrice($id_product)
	{
	    $row = $this->getCombinedPriceByProduct($id_product);
        if(count($row)>0)
        {
            foreach($row as $row)
        	{
        		$total += $row["pcp_total"];
        	}
        }
        
        $sql = "UPDATE `products` SET `pro_costprice`='$total' WHERE `pro_id`='$id_product'";
	    $this->Execute($sql);
	}
	
	//Product Multiple categories
	public function newProductsCategories()
	{
	    $id_product = $this->postVars('id_product');
	    $cat = $this->postVars('cat');
	    
	    for($i = 0; $i<count($cat); $i++)
	    {
	        $sql = "INSERT INTO `products_categories`(`id_product`, `id_category`) VALUES ('$id_product','".$cat[$i]."')";
	        $this->Execute($sql);
	    }
	}
	
	public function delProductsCategories()
	{
	    $id = $this->getVars('id_pc');
	    $sql = "DELETE FROM `products_categories` WHERE pc_id='$id'";
	    $this->Execute($sql);
	}
	
	public function isProductInCategory($id_product, $id_category)
	{
	    $sql = "SELECT * FROM `products_categories` WHERE `id_product`='$id_product' AND `id_category`='$id_category'";
	    $row = $this->ExecuteS($sql);
	    if(count($row)>0)
	    {
	        return true;
	    }
	    else
	    {
	        return false;
	    }
	}
	public function getProductCategoryId($id_product, $id_category)
	{
	    $sql = "SELECT * FROM `products_categories` WHERE `id_product`='$id_product' AND `id_category`='$id_category'";
	    $row = $this->ExecuteS($sql);
	    if(count($row)>0)
	    {
	        foreach($row as $row)
	        {
	            return $row['pc_id'];
	        }
	    }
	}
	
	public function getProductCategories($id_product)
	{
	    $sql = "SELECT * FROM `products_categories` WHERE `id_product`='$id_product'";
	    return $this->ExecuteS($sql);
	}
	
	public function getCategoriesProduct($id_category)
	{
	    $sql = "SELECT * FROM `products_categories` AS pc
	    INNER JOIN products AS p ON p.pro_id = pc.id_product
	    WHERE pc.id_category='$id_category' AND p.pro_active='1' AND p.pro_activefront='1'";
	    return $this->ExecuteS($sql);
	}
	
	//Product Params Methods
	public function getProductsParams()
	{
		$sql = "SELECT * FROM `products_params`";
		return $this->ExecuteS($sql);
	}
	
	public function editProductParams($id, $value)
	{
		$sql = "UPDATE `products_params` SET `propa_value`='$value' WHERE `propa_id`='$id'";
		$this->Execute($sql);
	}
	
	public function getProductParamsById($id)
	{
		$sql = "SELECT * FROM `products_params` WHERE `propa_id`='$id'";
		$row = $this->ExecuteS($sql);
		if(count($row)>0)
		{
			foreach($row as $row)
			{
				return $row['propa_value'];
			}
		}
	}
	
	public function incrementProductParams($id)
	{
		$value = $this->getProductParamsById($id);
		$value++;
		$this->editProductParams($id, $value);
	}
	
	//Installers
    public function CheckerMod($tb_module){
    return $this->chkTables($tb_module);
   }
   
   public function Checker(){
    return $this->chkTables('products');
   }
   
   public function install(){
    $f=fopen('modules/products/sql/db.txt','r');
	$query=fread($f,filesize('modules/products/sql/db.txt'));
	$this->Execute($query);
	fclose($f);
	
	$f=fopen('modules/products/sql/db1.txt','r');
	$query=fread($f,filesize('modules/products/sql/db1.txt'));
	$this->Execute($query);
	fclose($f);
	
	$f=fopen('modules/products/sql/db2.txt','r');
	$query=fread($f,filesize('modules/products/sql/db2.txt'));
	$this->Execute($query);
	fclose($f);
	
	$f=fopen('modules/products/sql/db3.txt','r');
	$query=fread($f,filesize('modules/products/sql/db3.txt'));
	$this->Execute($query);
	fclose($f);
   }
   
   public function uninstall(){
    $sql='DROP TABLE `categories`';
	$this->Execute($sql);
	
	$sql='DROP TABLE `products`';
	$this->Execute($sql);
	
	$sql='DROP TABLE `products_files`';
	$this->Execute($sql);
	
	$sql='DROP TABLE `products_images`';
	$this->Execute($sql);
	
	$sql='DROP TABLE `products_currency`';
	$this->Execute($sql);
	
	$sql='DROP TABLE `products_prices`';
	$this->Execute($sql);

   }
};
?>
