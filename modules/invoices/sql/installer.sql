CREATE TABLE `invoices` (
`in_id` INT NOT NULL AUTO_INCREMENT PRIMARY KEY ,
`in_fecha` DATETIME NOT NULL ,
`in_subto` VARCHAR( 255 ) NOT NULL ,
`in_iva` VARCHAR( 255 ) NOT NULL ,
`in_total` VARCHAR( 255 ) NOT NULL ,
`in_total_real` VARCHAR( 255 ) NOT NULL ,
`in_mod_pago` VARCHAR( 255 ) NOT NULL ,
`in_fecha_fin` DATE NOT NULL ,
`u_id` INT NOT NULL ,
`e_id` INT NOT NULL ,
`in_trm` VARCHAR( 255 ) NOT NULL ,
`in_subto_base` VARCHAR( 255 ) NOT NULL ,
`in_track` VARCHAR( 255 ) NOT NULL ,
`in_obs` LONGTEXT NOT NULL ,
`in_active` VARCHAR( 100 ) NOT NULL ,
`in_state` VARCHAR( 100 ) NOT NULL ,
`in_barcode` VARCHAR( 255 ) NOT NULL ,
`c_id` INT NOT NULL ,
`in_desc` LONGTEXT NOT NULL ,
`in_approval` INT NOT NULL ,
`in_uapproval` INT NOT NULL 
) ENGINE = MYISAM ;

CREATE TABLE `invoices_detail_temp` (
`indt_id` INT NOT NULL AUTO_INCREMENT PRIMARY KEY ,
`u_id` INT NOT NULL ,
`id_product` INT NOT NULL ,
`indt_desc` LONGTEXT NOT NULL ,
`indt_price` VARCHAR( 255 ) NOT NULL ,
`indt_qty` VARCHAR( 255 ) NOT NULL ,
`indt_total` VARCHAR( 255 ) NOT NULL
) ENGINE = MYISAM ;

CREATE TABLE `invoices_detail` (
`ind_id` INT NOT NULL AUTO_INCREMENT PRIMARY KEY ,
`in_id` INT NOT NULL ,
`id_product` INT NOT NULL ,
`ind_desc` LONGTEXT NOT NULL ,
`ind_price` VARCHAR( 255 ) NOT NULL ,
`ind_qty` VARCHAR( 255 ) NOT NULL ,
`ind_total` VARCHAR( 255 ) NOT NULL
) ENGINE = MYISAM ;

CREATE TABLE `invoices_param` (
`inp_id` INT NOT NULL AUTO_INCREMENT PRIMARY KEY ,
`inp_name` VARCHAR( 255 ) NOT NULL ,
`inp_value` VARCHAR( 255 ) NOT NULL 
) ENGINE = MYISAM ;

INSERT INTO `invoices_param` (`inp_id`, `inp_name`, `inp_value`) VALUES ('1', 'Track factura','1000');
INSERT INTO `invoices_param` (`inp_id`, `inp_name`, `inp_value`) VALUES ('2', 'Resolucion ','');
INSERT INTO `invoices_param` (`inp_id`, `inp_name`, `inp_value`) VALUES ('3', 'Tope No. Facturacion','1000');
INSERT INTO `invoices_param` (`inp_id`, `inp_name`, `inp_value`) VALUES ('4', 'IVA','19');
INSERT INTO `invoices_param` (`inp_id`, `inp_name`, `inp_value`) VALUES ('5', 'Nombre de Documento','FACTURA');

CREATE TABLE `invoices_collect` (
`inc_id` INT NOT NULL AUTO_INCREMENT PRIMARY KEY ,
`inc_date` DATETIME NOT NULL ,
`inc_txsnumber` VARCHAR( 255 ) NOT NULL ,
`inc_txsdate` DATE NOT NULL ,
`inc_txsvalue` VARCHAR( 255 ) NOT NULL , 
`in_id` INT( 11 ) NOT NULL ,
`u_id` INT( 11 ) NOT NULL , 
`inpt_id` INT( 11 ) NOT NULL  
) ENGINE = MYISAM ;

CREATE TABLE `invoices_paymenttype` (
`inpt_id` INT NOT NULL AUTO_INCREMENT PRIMARY KEY ,
`inpt_name` VARCHAR( 255 ) NOT NULL  
) ENGINE = MYISAM ;

ALTER TABLE  `user` ADD  `letInvAuth` INT NOT NULL DEFAULT  '0';

CREATE TABLE `invoice_nullregister` (
`innr_id` INT NOT NULL AUTO_INCREMENT PRIMARY KEY ,
`in_id` INT NOT NULL ,
`innr_desc` LONGTEXT NOT NULL ,
`innr_date` DATETIME NOT NULL ,
`u_id` INT NOT NULL
) ENGINE = MYISAM ;
ALTER TABLE `invoices` ADD `cc_id` INT NOT NULL;
ALTER TABLE `invoices_detail_temp` ADD `indt_tax` VARCHAR(255) NOT NULL;
ALTER TABLE `invoices_detail` ADD `ind_tax` VARCHAR(255) NOT NULL;

