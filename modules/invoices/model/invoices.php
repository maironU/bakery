<?php
 class invoices extends GeCore{
 
	//Invoices Methods
	public function newInvoices()
	{
		$subto = $this->postVars('subto');
		$iva = $this->postVars('iva');
		$total = $this->postVars('total');
		$modpago = $this->postVars('term');
		$fechafin = $this->postVars('fechafin');
		
		$company = $this->postVars('company');
		$trm = $this->postVars('trm');
		$obs = $this->postVars('obs');
		$customer = $this->postVars('customer');
		$desc = $this->postVars('desc');
		$contact = $this->postVars('contact');
		
		
		$today = date('Y-m-d H:i:s');
		
		
		$sql = "INSERT INTO `invoices` (`in_fecha`, `in_subto`, `in_iva`, `in_total`, `in_total_real`, `in_mod_pago`, `in_fecha_fin`, `u_id`, 
		`e_id`, `in_trm`, `in_subto_base`, `in_track`, `in_obs`, `in_active`, `in_state`, `in_barcode`, `c_id`, `in_desc`, `in_approval`, `cc_id`)
		VALUES ('$today', '$subto', '$iva', '$total', '$total', '$modpago', '$fechafin', '".$_SESSION['user_id']."', '$company', '$trm', '$subto', '$track',
		'$obs', '1', '1', '$barcode', '$customer', '$desc','0','$contact')";
		$this->Execute($sql);
		
		
		
		return $id = $this->getLastInID();
		
	}
	
	public function newInvoices2($subto, $iva, $total, $modpago, $fechafin, $company, $trm, $obs, $customer, $desc, $contact)
	{
		$today = date('Y-m-d H:i:s');
		
		
		$sql = "INSERT INTO `invoices` (`in_fecha`, `in_subto`, `in_iva`, `in_total`, `in_total_real`, `in_mod_pago`, `in_fecha_fin`, `u_id`, 
		`e_id`, `in_trm`, `in_subto_base`, `in_track`, `in_obs`, `in_active`, `in_state`, `in_barcode`, `c_id`, `in_desc`, `in_approval`, `cc_id`)
		VALUES ('$today', '$subto', '$iva', '$total', '$total', '$modpago', '$fechafin', '".$_SESSION['user_id']."', '$company', '$trm', '$subto', '$track',
		'$obs', '1', '1', '$barcode', '$customer', '$desc','0','$contact')";
		$this->Execute($sql);
		
		
		
		return $id = $this->getLastInID();
		
	}
	
	public function editInvoices()
	{
		$id = $this->postVars('id');
		$subto = $this->postVars('subto');
		$iva = $this->postVars('iva');
		$total = $this->postVars('total');
		$modpago = $this->postVars('term');
		$fechafin = $this->postVars('fechafin');
		$subto = $this->postVars('subto');
		$company = $this->postVars('company');
		$trm = $this->postVars('trm');
		$obs = $this->postVars('obs');
		$customer = $this->postVars('customer');
		$desc = $this->postVars('desc');
		$subto = $this->postVars('subto');
		$contact = $this->postVars('contact');
		
		
		$sql = "UPDATE `invoices` SET `in_subto`='$subto', `in_iva`='$iva', `in_total`='$total', `in_total_real`='$total', `in_mod_pago`='$modpago', `in_fecha_fin`='$fechafin', 
		`e_id`='$company', `in_trm`='$trm', `in_subto_base`='$subto', `in_obs`='$obs', `in_desc`='$desc', `cc_id`='$contact' WHERE in_id='$id'";
		$this->Execute($sql);	
	}
	
	public function editTotalReal($id_invoice, $real_value)
	{
		$sql = "UPDATE `invoices` SET `in_total_real`='$real_value' WHERE in_id='$id_invoice'";
		$this->Execute($sql);	
	}
	
	public function editActiveInvoice($id_invoice, $act)
	{	
		$sql = "UPDATE `invoices` SET `in_active`='$act' WHERE in_id='$id_invoice'";
		$this->Execute($sql);	
	}
	
	public function approvalInvoice($id_invoice)
	{
		$track = $this->getParamsById(1);
		$barcode = $this->genBarcode($track);
		$sql = "UPDATE `invoices` SET `in_track`='$track',`in_barcode`='$barcode',`in_approval`='1', `in_uapproval`='".$_SESSION['user_id']."'  WHERE in_id='$id_invoice'";
		$this->Execute($sql);	
		$this->incrementParams(1);
	}
	
	public function delInvoices($id)
	{
		$sql = "UPDATE `invoices` SET `in_active`='0' WHERE in_id='$id'";
		$this->Execute($sql);
	}
	
	public function getInvoices($act = 1)
	{
		$sql = "SELECT * FROM `invoices` WHERE in_active='$act'";
		return $this->ExecuteS($sql);
	}
	
	public function getInvoicesByCustomer($id_customer)
	{
		$sql = "SELECT * FROM `invoices` WHERE c_id='$id_customer' and in_active='1'";
		return $this->ExecuteS($sql);
	}
	
	public function getInvoicesById($id)
	{
		$sql = "SELECT * FROM `invoices` WHERE in_id='$id'";
		return $this->ExecuteS($sql);
	}
	
	public function getLastInID()
	{
		$sql = "SELECT * FROM `invoices` ORDER BY in_id DESC LIMIT 0,1";
		$row = $this->ExecuteS($sql);
		foreach($row as $row)
		{
			return $row['in_id'];
		}
	}
	
	public function genBarcode($track)
	{
		$cad = "0123456789";
		for($i=0; $i<=10; $i++)
		{
			$pos = rand(0,strlen($cad));
			$bar .= $cad[$pos];
		}
		
		return $bar.$track.date('YmdHis');
	}
	
	public function setBarcode($track)
	{
		if (!isset($output))  $output   = "png";
		if (!isset($type))    $type     = "C128B";
		if (!isset($widthb))   $widthb    = "300";
		if (!isset($heightb))  $heightb   = "100";
		if (!isset($xres))    $xres     = "2";
		if (!isset($font))    $font     = "2";
		
		$drawtext="off";
		$stretchtext="off";
		
		$style |= ($output  == "png" ) ? BCS_IMAGE_PNG  : 0;
		$style |= ($output  == "jpeg") ? BCS_IMAGE_JPEG : 0;
		$style |= ($border  == "on"  ) ? BCS_BORDER           : 0;
		$style |= ($drawtext== "on"  ) ? BCS_DRAW_TEXT  : 0;
		$style |= ($stretchtext== "on" ) ? BCS_STRETCH_TEXT  : 0;
		$style |= ($negative== "on"  ) ? BCS_REVERSE_COLOR  : 0;
	 
		 //$bc = new I25Object(0, 0, $style, $track); 
		 return $barcode = "<img style='height: 50px; width: 300px' src='../barcode/image.php?code=".$track."&style=".$style."&type=".$type."&width=".$widthb."&height=".$heightb."&xres=".$xres."&font=".$font."' />";
	}
	
	public function printInvoice($format, $id)
	{
		$f=fopen('../formats/'.$format,'r');
		$m = fread($f,filesize('../formats/'.$format));
		
		$row = $this->getInvoicesById($id);
		if(count($row)>0)
		{
			foreach($row as $row)
			{
				$m=str_replace('[track]',$row['in_track'],$m);
				$m=str_replace('[observaciones]',$row['in_obs'],$m);
				$m=str_replace('[subtotal]',number_format($row['in_subto']),$m);
				$m=str_replace('[iva]',number_format($row['in_iva']),$m);
				$m=str_replace('[total]',number_format($row['in_total']),$m);
				$m=str_replace('[date]',$row['in_fecha'],$m);
				$id_customer = $row['c_id'];
				$id_contact = $row['cc_id'];
				$barcode = $row['in_barcode'];
			}
		}
		
		$customer = new customerModelGe();
		$customer->connect();
		
		$row = $customer->getCustomerById2($id_customer);
		if(count($row)>0)
		{
			foreach($row as $row)
			{
				$m=str_replace('[nombrecliente]',$row['c_nom'],$m);
				//$m=str_replace('[direccion]',$row['c_dir'],$m);
				//$m=str_replace('[telefono]',$row['c_tel'],$m);
				$m=str_replace('[identificacion]',$row['c_nit'],$m);
			}
		}
		
		$row = $customer->getCustomerContactById($id_contact);
		if(count($row)>0)
		{
			foreach($row as $row)
			{
				//$m=str_replace('[nombrecliente]',$row['c_nom'],$m);
				$m=str_replace('[direccion]',$row['c_dir'],$m);
				$m=str_replace('[telefono]',$row['c_tel'],$m);
				//$m=str_replace('[identificacion]',$row['c_nit'],$m);
			}
		}
		
		$row = $this->getInvoicesDetail($id);
		if(count($row)>0)
		{
			foreach($row as $row)
			{
				$htm.='<tr>
					<td width="163">'.$row['ind_desc'].'</td>
					<td width="77">$'.number_format($row['ind_price']).'</td>
					<td width="72">'.$row['ind_qty'].'</td>
					<td width="86">$'.number_format($row['ind_total']).'</td>
				</tr>';
			}
		}
		$m=str_replace('[detalleinv]',$htm,$m);
		$crmModelObj=new crmScs();
		 $crmModelObj->connect();
		 
		 $row=$crmModelObj->getCompanyById(2);
		 if(count($row)>0){
		  foreach($row as $row){
		  	$company='<img src="../../crm/Logos/'.$row["e_img"].'" width="300" height="150"/><br />'.$row["e_info"];
		  }
		 }
		 $m=str_replace('[company]',$company,$m);
		 
		 $m = str_replace('[legal]', $this->getParamsById(2),$m);
		 $m = str_replace('[docname]', $this->getParamsById(5),$m);
		 $m=str_replace('[barcode]',$this->setBarcode($barcode),$m);
		 
		 return $m;
		
	 }
	 
	 public function getInvoiceInf()
	 {
		 $start = $this->postVars('start')." 00:00:00";
		 $end = $this->postVars('end')." 23:59:59";
		 $state = $this->postVars('state');
		 
		 $sql="SELECT * FROM invoices WHERE in_active='1' AND in_fecha BETWEEN '$start' AND '$end'";
		 
		 if($status != '')
		 {
			 $sql .= " AND in_state = '$state'";
		 }
		 
		 return $this->ExecuteS($sql);
		 
	 }
	
	//Invoice Detail Methods Temp
	public function newInvoicesDetailTemp($id_product, $desc, $price, $qty, $total,$tax)
	{
		$sql = "INSERT INTO `invoices_detail_temp` (`u_id`, `id_product`, `indt_desc`, `indt_price`, `indt_qty`, `indt_total`, `indt_tax`)
		VALUES ('".$_SESSION['user_id']."', '$id_product', '$desc', '$price','$qty','$total','$tax')";
		$this->Execute($sql);
	}
	
	public function getInvoicesDetailTemp()
	{
		$sql = "SELECT * FROM `invoices_detail_temp` WHERE u_id='".$_SESSION['user_id']."'";
		return $this->ExecuteS($sql);
	}
	
	public function delInvoicesDetailTemp($id)
	{
		$sql = "DELETE FROM `invoices_detail_temp` WHERE indt_id='$id'";
		$this->Execute($sql);
	}
	
	public function flushInvoicesDetailTemp()
	{
		$sql = "DELETE FROM `invoices_detail_temp` WHERE u_id='".$_SESSION['user_id']."'";
		$this->Execute($sql);
	}
	
	//Invoice Detail Methods
	public function newInvoicesDetail($id_invoice, $id_product, $desc, $price, $qty, $total,$tax)
	{
		$sql = "INSERT INTO `invoices_detail` (`in_id`, `id_product`, `ind_desc`, `ind_price`, `ind_qty`, `ind_total`, `ind_tax`)
		VALUES ('$id_invoice', '$id_product', '$desc', '$price','$qty','$total','$tax')";
		$this->Execute($sql);
	}
	
	public function getInvoicesDetail($id)
	{
		$sql = "SELECT * FROM `invoices_detail` WHERE in_id='$id'";
		return $this->ExecuteS($sql);
	}
	
	public function delInvoicesDetail($id)
	{
		$sql = "DELETE FROM `invoices_detail` WHERE ind_id='$id'";
		$this->Execute($sql);
	}
	
	//Params Methods
	public function editParams($id, $value)
	{
		$sql = "UPDATE `invoices_param` SET `inp_value`='$value' WHERE `inp_id`='$id'";
		$this->Execute($sql);
	}
	
	public function getParams()
	{
		$sql = "SELECT * FROM `invoices_param`";
		return $this->ExecuteS($sql);
	}
	
	public function getParamsById($id)
	{
		$sql = "SELECT * FROM `invoices_param` WHERE `inp_id`='$id'";
		$row = $this->ExecuteS($sql);
		foreach($row as $row)
		{
			return $row['inp_value'];
		}
	}
	
	public function incrementParams($id)
	{
		$value = $this->getParamsById($id);
		$value++;
		$this->editParams($id, $value);
	}
	
	//Collect Invoices Methods
	public function newCollectInvoice($txsnumber, $txsdate, $txsvalue, $id_invoice, $id_paymenttype)
	{
		$today = date('Y-m-d H:i:s');
		$sql = "INSERT INTO `invoices_collect` (`inc_date`, `inc_txsnumber`, `inc_txsdate`, `inc_txsvalue`, `in_id`, `inpt_id`, `u_id`)
		VALUES ('$today', '$txsnumber', '$txsdate', '$txsvalue', '$id_invoice', '$id_paymenttype', '".$_SESSION['user_id']."')";
		$this->Execute($sql);
	}
	
	public function editCollectInvoice($id, $txsnumber, $txsdate, $txsvalue, $id_invoice, $id_paymenttype)
	{
		$sql = "UPDATE `invoices_collect` SET `inc_txsnumber`='$txsnumber', `inc_txsdate`='$txsdate', `inc_txsvalue`='$txsvalue', `in_id`='$id_invoice', `inpt_id`='$id_paymenttype' WHERE inc_id='$id'";
		$this->Execute($sql);
	}
	
	public function delCollectInvoice($id)
	{
		$sql = "DELETE FROM `invoices_collect` WHERE inc_id='$id'";
		$this->Execute($sql);
	}
	
	public function getCollectInvoice($id)
	{
		$sql = "SELECT * FROM `invoices_collect` WHERE in_id='$id'";
		return $this->ExecuteS($sql);
	}
	
	public function getCollectInvoiceTotal($id)
	{
		$sql = "SELECT * FROM `invoices_collect` WHERE in_id='$id'";
		$row = $this->ExecuteS($sql);
		$total = 0;
		if(count($row)>0)
		{
			foreach($row as $row)
			{
				$total += $row['inc_txsvalue'];
			}
		}
		
		return $total;
	}
	
	//Invoice Payment Methods Type
	public function newInvoicePaymentType($name)
	{
		$sql = "INSERT INTO `invoices_paymenttype` (`inpt_name`) VALUES ('$name')";
		$this->Execute($sql);
	}
	
	public function editInvoicePaymentType($id, $name)
	{
		$sql = "UPDATE `invoices_paymenttype` SET `inpt_name`='$name' WHERE inpt_id='$id'";
		$this->Execute($sql);
	}
	
	public function delInvoicePaymentType($id)
	{
		$sql = "DELETE FROM `invoices_paymenttype` WHERE inpt_id='$id'";
		$this->Execute($sql);
	}
	
	public function getInvoicePaymentType()
	{
		$sql = "SELECT * FROM `invoices_paymenttype`";
		return $this->ExecuteS($sql);
	}
	
	public function getInvoicePaymentTypeById($id)
	{
		$sql = "SELECT * FROM `invoices_paymenttype` WHERE inpt_id='$id'";
		return $this->ExecuteS($sql);
	}
	
	//Invoice State Method
	public function updateInvoiceState($id_invoice, $total, $total_real)
	{
		//echo $total;
		if($total == 0)
		{
			//Pendiente de Pago
			$sql = "UPDATE `invoices` SET `in_state` = '1' WHERE in_id='$id_invoice'";
			$this->Execute($sql);
			return;
		}
		
		if(($total > 0) && ($total < $total_real))
		{
			//Parcialmente pagado
			$sql = "UPDATE `invoices` SET `in_state` = '2' WHERE in_id='$id_invoice'";
			$this->Execute($sql);
			return;
		}
		
		if(($total > 0) && ($total > $total_real))
		{
			//Parcialmente pagado
			$sql = "UPDATE `invoices` SET `in_state` = '3' WHERE in_id='$id_invoice'";
			$this->Execute($sql);
			return;
		}
		
		if($total == $total_real)
		{
			//Pagado
			$sql = "UPDATE `invoices` SET `in_state` = '0' WHERE in_id='$id_invoice'";
			$this->Execute($sql);
			return;
		}
	}
	
	public function getInvoiceStateName($value)
	{
		switch($value)
		{
			case '0': $msg = '<span style="min-wdth: 100px; height: 50px; display: block; padding: 5px; background:#0f0">Pagado</span>';
			break;
			case '1': $msg = '<span style="min-wdth: 100px; height: 50px; display: block; padding: 5px; background:#f00">Pendiente de pago</span>';
			break;
			case '2': $msg = '<span style="min-wdth: 100px; height: 50px; display: block; padding: 5px; background:#ff0">Parcialmente Pagado</span>';
			break;
			case '3': $msg = '<span style="min-wdth: 100px; height: 50px; display: block; padding: 5px; background:#6633FF">Recaudo Excedido</span>';
			break;
		}
		
		return $msg;
	}
	
	//Null Register Methods
	public function newNullRegisterInvoice($id, $desc, $user)
	{		
		$today = date('Y-m-d H:i:s');
		
		$sql = "INSERT INTO `invoice_nullregister` (`in_id`, `innr_desc`, `innr_date`, `u_id`) 
		VALUES ('$id', '$desc', '$today', '$user')";
		$this->Execute($sql);
	}
	
	public function getNullRegisterInvoice()
	{
		$start = $this->postVars('start')." 00:00:00";
		$end = $this->postVars('end')." 23:59:59";
		
		$sql = "SELECT * FROM `invoice_nullregister` WHERE `innr_date` BETWEEN '$start' AND '$end'";
		return $this->ExecuteS($sql);
	}
	
	//Auth Methods
	public function authorize()
	{
		$user = $this->postVars('user');
		$pass = md5($this->postVars('pass'));
		
		$sql="select * from user where u_user='$user' and u_pass='$pass' and u_active='1'";
		$row=$this->ExecuteS($sql);
		 
		if(count($row)>0)
		{
			foreach($row as $row)
			{
				$auth = $row['letInvAuth'];
			}
			
			if($auth == 1)
			{
				return 'ok';
			}
			else
			{
				return 'noauth';
			}
		}
		else
		{
			return 'npass';
		}
	}
	
	//Auth methods
	public function editAuth($id_user,$auth){
	 $sql="update user set letInvAuth='$auth' where u_id='$id_user'";
	 $this->Execute($sql);
	}
	
	//Installers
   public function Checker(){
    return $this->chkTables('invoices');
   }
   
   public function install(){
	$this->importSQLFile('modules/invoices/sql/installer.sql');
	//Se instala los submenus
	$sql="select * from menu where m_folder='invoices'";
	$row=$this->ExecuteS($sql);
	foreach($row as $row){
	 $m_id=$row["m_id"];
	}
	
	$sql="insert into `submenu` (`sm_nom`, `sm_link`, `m_id`) values ('Informe','home.php?p=modules/invoices/view/showInfInvoice.php','$m_id')";
	$this->Execute($sql);
	$sql="insert into `submenu` (`sm_nom`, `sm_link`, `m_id`) values ('Setup','home.php?p=modules/invoices/view/showSetup.php','$m_id')";
	$this->Execute($sql);
	$sql="insert into `submenu` (`sm_nom`, `sm_link`, `m_id`) values ('Aprobaciones','home.php?p=modules/invoices/view/showApprovalInvoice.php','$m_id')";
	$this->Execute($sql);
   }
   
   public function uninstall(){
	$sql="DROP TABLE `invoices`, `invoices_detail`, `invoices_detail_temp`, `invoices_param`, `invoices_collect`, `invoices_paymenttype`, `invoice_nullregister`";
	$this->Execute($sql);
	
	$sql="ALTER TABLE  `user` DROP  `letInvAuth`;";
	$this->Execute($sql);
	
	$sql="select * from menu where m_folder='invoices'";
	$row=$this->ExecuteS($sql);
	foreach($row as $row){
	 $m_id=$row["m_id"]; 
	}
	
	$sql="select * from submenu as sm inner join pro_submenu as psm on sm.sm_id=psm.sm_id where sm.m_id='$m_id'";
	$row=$this->ExecuteS($sql);
	foreach($row as $row){
	 $sql1="delete from pro_submenu where sm_id='".$row["sm_id"]."'";
	 $this->Execute($sql);
	}
	
	$sql1="delete from submenu where m_id='$m_id'";
	$this->Execute($sql);
	
   }
	
	
 };
?>
