<?php
 include('modules/invoices/view/includes.php');
 
 if($obj->getVars('actionApp') == true)
 {
	 $id=$obj->getVars('id');
	 $obj->approvalInvoice($id);
 }
?>
<!-- Add mousewheel plugin (this is optional) -->
<script type="text/javascript" src="modules/invoices/js/jquery.mousewheel-3.0.6.pack.js"></script>
<!-- Add fancyBox main JS and CSS files -->
<script type="text/javascript" src="modules/invoices/js/jquery.fancybox.js?v=2.1.4"></script>
<link rel="stylesheet" type="text/css" href="modules/invoices/css/jquery.fancybox.css?v=2.1.4" media="screen" />
 
<div class="widget3">
 <div class="widgetlegend">Facturas para aprobar </div>
 <?php
  if($msg)
  {
  ?>
   <div class="ui-widget">
	<div class="ui-state-highlight ui-corner-all" style="margin-top: 20px; padding: 0 .7em;">
		<p><span class="ui-icon ui-icon-info" style="float: left; margin-right: .3em;"></span>
		<strong>Felicitaciones!</strong> Se ha sido eliminada satisfactoriamente.</p>
	</div>
</div>
  <?php
  }
 ?>
<p style="width:100">
    <!--<a href="<?php $_SERVER['PHP_SELF'];?>?p=modules/invoices/view/newPorder.php" class="btn_normal" style="float:left; margin:5px;">Nuevo </a>-->
</p>

<table width="100%" height="48" border="0">
  <tr>
	<th width="30">Track</th>
	<th width="142">Aprobacion</th>
	<th width="142">Fecha Creacion</th>
	<th width="142">Cliente</th>
	<th width="142">Total</th>
	<th width="142">Recaudo Total</th>
	<th width="142">Estado</th>
	<th width="142">Creador por</th>
	<th width="142">Aprobado por</th>
    <th colspan="5">Acciones</th>
  </tr>
  <?php
   $row=$obj->getInvoices();
   
   if(count($row)>0){
 	foreach($row as $row){
  ?>
  <tr>
    <td><?php echo $row["in_track"];?></td>
    <td><?php 
	if($row["in_approval"] == 1)
	{
	?>
	<span style="min-wdth: 100px; height: 50px; display: block; padding: 5px; background:#0f0">Aprobado</span>
	<?php
	}
	else
	{
	?>
	<span style="min-wdth: 100px; height: 50px; display: block; padding: 5px; background:#f00">Sin Aprobar</span>
	<?php
	}
    ?></td>
	<td><?php echo $row["in_fecha"];?></td>
	<td><?php 
	$row1 = $customer->getCustomerById2($row['c_id']);
	if(count($row1)>0)
	{
		foreach($row1 as $row1)
		{
			echo $row1['c_nom'];
		}
	}
	
	?></td>
	<td>$<?php echo number_format($row["in_total"]);?></td>
	<td>$<?php echo $obj->getCollectInvoiceTotal($row["in_id"]);?></td>
	<td><?php echo $obj->getInvoiceStateName($row["in_state"]);?></td>
	<td><?php 
	 		 $user=new User();
			 $user->connect();
			 $user->getUserById($row["u_id"]);
			 echo $user->name;
	?></td>
	<td><?php 
	 		 $user=new User();
			 $user->connect();
			 $user->getUserById($row["in_uapproval"]);
			 echo $user->name;
	?></td>
	<td>
	<?php 
	if($row["in_approval"] != 1)
	{
	?>
	<a href="<?php $_SERVER['PHP_SELF'];?>?p=modules/invoices/view/showApprovalInvoice.php&id=<?php echo $row["in_id"]?>&actionApp=true" class="btn_5">Aprobar</a>
	<?php
	}
	
    ?>
	</td>
	<td><a href="<?php $_SERVER['PHP_SELF'];?>?p=modules/invoices/view/editInvoice.php&id=<?php echo $row["in_id"]?>" class="btn_1">Editar</a></td>
	<td><a href="<?php $_SERVER['PHP_SELF'];?>?p=modules/invoices/view/collectInvoice.php&id=<?php echo $row["in_id"]?>" class="btn_4">Recaudar</a></td>
	<td width="24"><a href="javascript:;" class="btn_3" onclick="showInfo(<?php echo $row["in_id"];?>,'modules/invoices/view/printInvoice.php')">Imprimir</a></td>
	<td width="24"><a href="javascript:;" class="btn_2" onclick="showInfo(<?php echo $row["in_id"];?>,'modules/invoices/view/delInvoice.php')">Anular</a></td>
  </tr>
  <?php
   }
  } 
  ?>
</table>

</div>
<script>
 function showInfo(id,site)
 {
	$.fancybox.open({
					href : site+'?id='+id,
					type : 'iframe',
					padding:5
				}); 
 }
</script>
