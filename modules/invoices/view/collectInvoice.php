<?php
 include('modules/invoices/view/includes.php');
 
 $msg = false;
 
 if($_POST)
 {
	$id = $obj->postVars('id');
	$real = $obj->postVars('real');
	$txsnumber = $obj->postVars('txsnumber');
	$txsdate = $obj->postVars('txsdate');
	$txsvalue = $obj->postVars('txsvalue');
	$id_paymenttype = $obj->postVars('id_payment');
	
	$obj->editTotalReal($id, $real);
	$obj->newCollectInvoice($txsnumber, $txsdate, $txsvalue, $id, $id_paymenttype);
	$total_collect = $obj->getCollectInvoiceTotal($id);
	$obj->updateInvoiceState($id, $total_collect, $real);
 }
 
 
 $id = $obj->getVars('id');
 
 
 
 $row = $obj->getInvoicesById($id);
 foreach($row as $row)
 {
	 $real = $row['in_total_real'];
 }
 
 $total_collect = $obj->getCollectInvoiceTotal($id);
 $pending = $real - $total_collect;
 
 if($obj->getVars('actionDel') == true)
 {
	 $obj->delCollectInvoice($obj->getVars('id_collect'));
	 $total_collect = $obj->getCollectInvoiceTotal($id);
	 $obj->updateInvoiceState($id, $total_collect, $real);
	 $msg = true;
 }
 
 
 
?>
<!-- Add mousewheel plugin (this is optional) -->
<script type="text/javascript" src="modules/invoices/js/jquery.mousewheel-3.0.6.pack.js"></script>
<!-- Add fancyBox main JS and CSS files -->
<script type="text/javascript" src="modules/invoices/js/jquery.fancybox.js?v=2.1.4"></script>
<link rel="stylesheet" type="text/css" href="modules/invoices/css/jquery.fancybox.css?v=2.1.4" media="screen" />

<div class="widget3">
 <div class="widgetlegend">Recaudo </div>
 <?php
  if($msg)
  {
  ?>
   <div class="ui-widget">
	<div class="ui-state-highlight ui-corner-all" style="margin-top: 20px; padding: 0 .7em;">
		<p><span class="ui-icon ui-icon-info" style="float: left; margin-right: .3em;"></span>
		<strong>Felicitaciones!</strong> Se ha sido eliminado satisfactoriamente.</p>
	</div>
</div>
  <?php
  }
 ?>
<p style="width:100">
    <a href="<?php $_SERVER['PHP_SELF'];?>?p=modules/invoices/view/index.php" class="btn_normal" style="float:left; margin:5px;">Volver </a>
</p>
<br>
<br>
<br>
<form action="" method="post">
<table>
	<tr>
		<td>Valor real a recaudar:</td>
		<td><input type="text" name="real" value="<?php echo $real?>" required /></td>
	</tr>
	<tr>
		<td>Numero de Transaccion:</td>
		<td><input type="text" name="txsnumber" required /></td>
	</tr>
	<tr>
		<td>Fecha de Transaccion:</td>
		<td><input type="text" name="txsdate" class="datepicker" required /></td>
	</tr>
	<tr>
		<td>Valor de recaudo:</td>
		<td><input type="text" name="txsvalue" value="<?php echo $pending?>" required /></td>
	</tr>
	<tr>
		<td>Medio de Pago:</td>
		<td><select name="id_payment" required>
		<option value="">--Seleccionar--</option>
		<?php
			$row = $obj->getInvoicePaymentType();
			if(count($row)>0)
			{
				foreach($row as $row)
				{
					?>
					<option value="<?php echo $row['inpt_id']?>"><?php echo utf8_encode($row['inpt_name'])?></option>
					<?php
				}
			}
		?>
		</select></td>
	</tr>
	<tr>
		<td colspan="2">
		<input type="hidden" value="<?php echo $id?>" name="id" />
		<input type="submit" value="Recaudar" class="btn_submit" /></td>
	</tr>
</table>
</form>
 <br>
<br>
<br>
<table width="100%" height="48" border="0">
  <tr>
	<th width="30">ID</th>
	<th width="142">Fecha</th>
	<th width="142">Fecha recaudo</th>
	<th width="142">No. Transaccion</th>
	<th width="142">Valor</th>
	<th width="142">Medio de pago</th>
	<th width="142">Registrador por</th>
	<th colspan="1">Acciones</th>
  </tr>
  <?php
   $row=$obj->getCollectInvoice($id);
   
   if(count($row)>0){
 	foreach($row as $row){
  ?>
  <tr>
	<td><?php echo $row["inc_id"];?></td>
	<td><?php echo $row["inc_date"];?></td>
	<td><?php echo $row["inc_txsdate"];?></td>
	<td><?php echo $row["inc_txsnumber"];?></td>
	<td>$<?php echo number_format($row["inc_txsvalue"]);?></td>
	<td><?php 
	$row1 = $obj->getInvoicePaymentTypeById($row['inpt_id']);
	if(count($row1)>0)
	{
		foreach($row1 as $row1)
		{
			echo $row1['inpt_name'];
		}
	}
	
	?></td>
	<td><?php 
		$user=new User();
		$user->connect();
		$user->getUserById($row["u_id"]);
		echo $user->name;
	?></td>
	<td><a href="<?php $_SERVER['PHP_SELF'];?>?p=modules/invoices/view/collectInvoice.php&id_collect=<?php echo $row["inc_id"]?>&id=<?php echo $id?>&actionDel=true" class="btn_2">Eliminar</a></td>
  </tr>
  <?php
   }
  } 
  ?>
</table>

</div>
