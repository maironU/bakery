<?php
  session_start();
  include('includes_self.php');
  
  $id = $obj->getVars('id');
  $ot = $obj->getVars('ot');
  
  if($_POST)
  {
		$id_order = $obj->postVars('id_order');
		$row = $orderModelObj->getOrdersDetail($id_order);
		if(count($row)>0)
		{
		    foreach($row as $row)
		    {
		        $desc = $row['od_desc'];
		        $obj->newInvoicesDetailTemp($row['id_product'], $desc, $row['od_price'], $row['od_qty'], $row['od_total'], $row['od_tax']);
		    }
		}
		
  }
  
?>
<link href="../../../css/scsStyle.css" rel="stylesheet" type="text/css" />
<link href="../../../css/smoothness/jquery-ui-1.9.2.custom.css" rel="stylesheet">
<link href="../../../css/smoothness/jquery-ui-1.9.2.custom.min.css" rel="stylesheet" type="text/css" />
<script src="../../../js/jquery-1.8.3.js"></script>
<script src="../../../js/jquery-ui-1.9.2.custom.js"></script>
<script src="../../../js/jquery-ui-1.9.2.custom.js"></script>
<script src="../js/jsInvoicesFunctions.js"></script>

<div class="widget3">
 <div class="widgetlegend">Pedidos </div>
  
  <form action="" method="get" name="form2">
      <table>
          <tr>
              <td>Tipo de Orden</td>
              <td>
                  <select name="ot">
                      <option value="">--seleccionar--</option>
                      <?php
                        $row = $orderModelObj->getOrderType();
                        if(count($row)>0)
                        {
                            foreach($row as $row)
                            {
                                ?>
                                <option value="<?php echo $row['ot_id']?>"><?php echo $row['ot_name']?></option>
                                <?php
                            }
                        }
                        
                      ?>
                  </select>
              </td>
              <td>
                  <input type="hidden" name="id" value="<?php echo $id?>">
                  <input type="submit" value="Filtar" class="btn_submit" />
              </td>
          </tr>
      </table>
      
  </form>
  
 
  <form action="" method="post" name="form1">
    <table width="703" height="88" border="0" align="center">
     <tr> 
      <th>track</th>
      <th>Fecha</th>
      <th>Total</th>
      <th>Descripcion</th>
      <th>Estado</th>
      <th>Acciones</th>
     </tr>
     <?php
      $row = $orderModelObj->getOrderByCustomerAndOrderType($id, $ot);
      if(count($row)>0)
      {
          foreach($row as $row)
          {
              ?>
               <tr> 
                  <td><?php echo $row['o_track']?></td>
                  <td><?php echo $row["o_fecha"];?></td>
                  <td>$<?php echo number_format($row["o_subto"]);?></td>
                  <td><?php echo $row["o_desc"];?></td>
                  <td><?php
        			 $orderModelObj->connect();
                     $row1=$orderModelObj->getLastOrderHistory($row["o_id"]);
        			 if(count($row1)>0){
        			 	foreach($row1 as $row1){
        				 ?>
                         <span style="width:100px; height:25px; display:block; color:#000; background-color:#<?php echo $row1["oh_color"]?>; padding:4px;"><?php echo $row1["oh_name"]?></span>
                         <?php
        				}
        			 }
        			?></td>
                  <td><form action="" method="post">
                      <input type="hidden" name="id_order" value="<?php echo $row['o_id']?>" >
                      <input type="submit" value="Agregar" class="btn_submit" >
                  </form></td>
                </tr>
              <?php
          }
      }
     ?>
    </table>
  </form>
 <br />
 <div id="gauch" style="display:none"><img src="../images/loading.gif" /></div>
 <div id="showPl">&nbsp;</div>
</div>
