<?php
 include('modules/invoices/view/includes.php');
$msg=false;
if($_POST)
{
	$id = $obj->postVars('id');
	$name = $obj->postVars('name');
	$obj->editInvoicePaymentType($id, $name);
	$msg=true;
}
$id=$obj->getVars('id');
$row = $obj->getInvoicePaymentTypeById($id);
foreach($row as $row)
{
	$name = $row['inpt_name'];
}
?>
<div class="widget3">
 <div class="widgetlegend">Editar Tipo de Pago</div>
 <?php
  if($msg)
  {
  ?>
   <div class="ui-widget">
	<div class="ui-state-highlight ui-corner-all" style="margin-top: 20px; padding: 0 .7em;">
		<p><span class="ui-icon ui-icon-info" style="float: left; margin-right: .3em;"></span>
		<strong>Felicitaciones!</strong> Se ha guardado satisfactoriamente.</p>
	</div>
</div>
  <?php
  }
 ?>
<p style="width:100">
	<a href="<?php $_SERVER['PHP_SELF'];?>?p=modules/invoices/view/showPaymentTypes.php" class="btn_normal" style="float:left; margin:5px;">Volver </a>
</p>
 <br />
 <br />
 <form action="" method="post">
	<table>
		<tr>
			<td><strong>Nombre:</strong><br>
			<input type="text" name="name" value="<?php echo $name?>" required />
			</td>
		</tr>
		<tr>
			<td>
			<input type="hidden" name="id" value="<?php echo $id?>" />
			<input type="submit" value="Guardar" class="btn_submit" />
			</td>
		</tr>
	</table>
 </form>


</div>

<br />
 
 

