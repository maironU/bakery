<?php
 include('modules/invoices/view/includes.php');
 
 if($_POST)
 {
	 $obj->editInvoices();
 }
 
 //action
 if($obj->getVars('actionDel') == true)
 {
	 $obj->delInvoicesDetail($obj->getVars('id_item'));
 }
 
 $id = $obj->getVars('id');
 $row = $obj->getInvoicesById($id);
 foreach($row as $row)
 {
	 $modpago = $row['in_mod_pago'];
	 $fechafin = $row['in_fecha_fin'];
	 $company = $row['e_id'];
	 $trm = $row['in_trm'];
	 $obs = $row['in_obs'];
	 $state = $row['in_state'];
	 $id_customer = $row['c_id'];
	 $desc = $row['in_desc'];
	 $id_contact = $row['cc_id'];
 }
?>
<link href="modules/invoices/css/smart_wizard_vertical.css" rel="stylesheet" type="text/css">
<!--<script type="text/javascript" src="modules/crm/js/jquery-2.0.0.min.js"></script>-->
<script type="text/javascript" src="modules/invoices/js/jquery.smartWizard.js"></script>
<script type="text/javascript" src="modules/invoices/js/jsinvoicesFunctions.js"></script>
<script type="text/javascript">
   
    $(document).ready(function(){
    	// Smart Wizard	
  		$('#wizard').smartWizard({transitionEffect:'slide'});
     
		});
</script>
 <div class="widget3">
 <div class="widgetlegend">Editar Factura </div>
 <?php
  if($msg)
  {
  ?>
   <div class="ui-widget">
	<div class="ui-state-highlight ui-corner-all" style="margin-top: 20px; padding: 0 .7em;">
		<p><span class="ui-icon ui-icon-info" style="float: left; margin-right: .3em;"></span>
		<strong>Felicitaciones!</strong> Se ha creado satisfactoriamente.</p>
	</div>
</div>
  <?php
  }
 ?>
<p style="width:100">
	<!--<a href="<?php $_SERVER['PHP_SELF'];?>?p=modules/sliders/view/newSlide.php" class="btn_normal" style="float:left; margin:5px;">Nuevo Slide </a>-->
</p>
 
<form name="frmNewAdv" method="post" action=""> 
<table border="0">
	<tr>
		<td>
			<!-- Tabs -->
  			<div id="wizard" class="swMain">
  			<ul>
  				<!--<li><a href="#step-0">
                <label class="stepNumber">0</label>
                <span class="stepDesc">
                   Paso 0<br />
                   <small>Seleccione el convenio</small>
                </span></li>-->
				<li><a href="#step-1">
                <label class="stepNumber">1</label>
                <span class="stepDesc">
                   Paso 1<br />
                   <small>Seleccione los Items para la factura</small>
                </span>
            </a></li>
  				<li><a href="#step-2">
                <label class="stepNumber">2</label>
                <span class="stepDesc">
                   Paso 2<br />
                   <small>Ingrese los detalles la factura</small>
                </span>
            </a></li>
  				<li><a href="#step-3">
                <label class="stepNumber">3</label>
                <span class="stepDesc">
                   Paso 3<br />
                   <small>Valores y Observaciones</small>
                </span>                   
             </a></li>
  				<li><a href="#step-4">
                <label class="stepNumber">4</label>
                <span class="stepDesc">
                   Paso 4<br />
                   <small>otro datos informativos</small>
                </span>                   
            </a></li>
  			</ul>
  			<!--<div id="step-0">	
            <h2 class="StepTitle">Paso 1 Seleccione el convenio</h2>
			<p><?php include('newinvoicesStep0.php');?></p>
                     			
        </div>-->
			<div id="step-1">	
            <h2 class="StepTitle">Paso 1 Items la factura</h2>
			<!--<iframe frameborder="0" src="modules/crm/view/newOrderStep1.php" allowtransparency="yes" scrolling="auto" width="1000" height="1000"></iframe> -->
             <p><?php include('editInvoicesStep1.php');?></p>        			
        </div>
  			<div id="step-2">
            <h2 class="StepTitle">Step 2 Ingrese los detalles de la factura</h2>	
            <p><?php include('editInvoicesStep2.php');?></p>      
        </div>                      
  			<div id="step-3">
            <h2 class="StepTitle">Step 3 Resumen de precios</h2>	
            <p><?php include('editInvoicesStep3.php');?></p>               				          
        </div>
  			<div id="step-4">
            <h2 class="StepTitle">Step 4 Datos complementarios</h2>	
             <p><?php include('editInvoicesStep4.php');?></p>            			
        </div>
  		</div>
			<!-- End SmartWizard Content -->  
		</td>
	</tr>
</table>
</form>
</div>

