<?php
  session_start();
  include('includes_self.php');
  
  $id = $obj->getVars('id');
?>
<link href="../../../css/scsStyle.css" rel="stylesheet" type="text/css" />
<link href="../../../css/smoothness/jquery-ui-1.9.2.custom.css" rel="stylesheet">
<link href="../../../css/smoothness/jquery-ui-1.9.2.custom.min.css" rel="stylesheet" type="text/css" />
<script src="../../../js/jquery-1.8.3.js"></script>
<script src="../../../js/jquery-ui-1.9.2.custom.js"></script>
<script src="../../../js/jquery-ui-1.9.2.custom.js"></script>
<?php
if($_POST)
  {
  	$res = $obj->authorize();
	switch($res)
	{
		case 'npass': echo '<div class="ui-widget">
	<div class="ui-state-error ui-corner-all" style="padding: 0 .7em;">
		<p><span class="ui-icon ui-icon-alert" style="float: left; margin-right: .3em;"></span>
		<strong>Error:</strong> Usuario y/o Contrase&ntilde;a erroneo.</p>
	</div>
</div>'; 
		break;
		case 'noauth': echo '<div class="ui-widget">
	<div class="ui-state-error ui-corner-all" style="padding: 0 .7em;">
		<p><span class="ui-icon ui-icon-alert" style="float: left; margin-right: .3em;"></span>
		<strong>Error:</strong> Usted no esta autorizado para anular ordenes de compra.</p>
	</div>
</div>';
		break;
		case 'ok':
		$id = $obj->postVars('id');
		$desc = $obj->postVars('desc');
		$obj->editActiveInvoice($id, 0);
		$obj->newNullRegisterInvoice($id, $desc, $_SESSION['user_id']);
		echo '<div class="ui-widget">
	<div class="ui-state-highlight ui-corner-all" style="margin-top: 20px; padding: 0 .7em;">
		<p><span class="ui-icon ui-icon-info" style="float: left; margin-right: .3em;"></span>
		<strong>Felicitaciones!</strong> Usted ha anulado la factura.</p>
	</div>
</div>';
		break;
	}
  }

?>
<div class="widget3">
 <div class="widgetlegend">Anular Factura </div>
  <table width="100%" height="48" border="0">
    <tr>
		<td>Usted esta apunto de anular una factura. Para poder hacer la anulaci&oacute;n, usted debe estar autorizado para anularla. Por tanto, debe ingresar su usuario y contrase&ntilde;a del sistema, y dar las razones de anulaci&oacute;n.</td>
	</tr>
	<tr>
		<td>
			<form action="" method="post" name="">
				<table width="100%">
					<tr>
						<td><strong>Usuario</strong></td>
						<td><input type="text" name="user" id="user" required /></td>
					</tr>
					<tr>
						<td><strong>Contrase&ntilde;a</strong></td>
						<td><input type="password" name="pass" id="pass" required /></td>
					</tr>
					<tr>
						<td><strong>Motivo de anulaci&oacute;n</strong></td>
						<td><textarea cols="40" rows="6" name="desc" required></textarea></td>
					</tr>
					<tr>
						<td colspan="2">
							<input type="hidden" name="id" value="<?php echo $id?>" />
							<input type="submit" value="Anular" />
						</td>
					</tr>
				</table>
			</form>
		</td>
	</tr>
  </table>
  <br />
</div>
