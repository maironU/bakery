<?php
    include('../../../core/config.php');
    include('../../../core/ge.php');
	include('../../../usr/model/User.php');
	//Models
	include('../../crm/model/crm.php');
	include('../../crm/model/customerhistoryModel.php');
	include('../../crm/model/customerModel.php');
	include('../../crm/model/ordersModel.php');
	include('../../crm/model/quoteModel.php');
	include('../../products/model/products.php');
	//include('../../inventory/model/inventory.php');
	include('../model/invoices.php');
	
	//Controllers
	include('../../crm/controller/customerController.php');
	include('../../crm/controller/orderController.php');
	include('../../crm/controller/quoteController.php');
	
	//Model Objects
	$crmModelObj=new crmScs();
	$crmModelObj->connect();
	$customerHistoryModelObj=new customerHistoryModelGe();
	$customer=new customerModelGe();
	$customer->connect();
	$orderModelObj=new orderModelGe();
	$orderModelObj->connect();
	$quoteModelObj=new quoteModelGe();
	$quoteModelObj->connect();
	$products=new products();
	$products->connect();
	$obj=new invoices();
	$obj->connect();
	//$inventory = new inventory();
	//$inventory->connect();
	
	//Controllers Objects
	$customerCtrlObj = new customerControllerGe();
	$orderCtrlObj = new orderControllerGe();
	$quoteCtrlObj = new quoteControllerGe();
	

?>
