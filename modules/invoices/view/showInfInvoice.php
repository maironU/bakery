<?php
 include('modules/invoices/view/includes.php');
?>
<!-- Add mousewheel plugin (this is optional) -->
<script type="text/javascript" src="modules/invoices/js/jquery.mousewheel-3.0.6.pack.js"></script>
<!-- Add fancyBox main JS and CSS files -->
<script type="text/javascript" src="modules/invoices/js/jquery.fancybox.js?v=2.1.4"></script>
<link rel="stylesheet" type="text/css" href="modules/invoices/css/jquery.fancybox.css?v=2.1.4" media="screen" />
 
<div class="widget3">
 <div class="widgetlegend">Informe Facturacion </div>
 <?php
  if($msg)
  {
  ?>
   <div class="ui-widget">
	<div class="ui-state-highlight ui-corner-all" style="margin-top: 20px; padding: 0 .7em;">
		<p><span class="ui-icon ui-icon-info" style="float: left; margin-right: .3em;"></span>
		<strong>Felicitaciones!</strong> Se ha sido eliminada satisfactoriamente.</p>
	</div>
</div>
  <?php
  }
 ?>
<p style="width:100">
    <!--<a href="<?php $_SERVER['PHP_SELF'];?>?p=modules/invoices/view/newPorder.php" class="btn_normal" style="float:left; margin:5px;">Nuevo </a>-->
</p>
 <br>
 <br>
 <form action="" method="post">
 <table>
	<tr>
		<td><input type="text" class="datepicker" name="start" placeholder="Inicio" required /></td>
		<td><input type="text" class="datepicker2" name="end" placeholder="Fin" required /></td>
		<td><select name="state">
			<option value="">--Estado--</option>
			<option value="0">Pagado</option>
			<option value="1">Pendiente de pago</option>
			<option value="2">Parcialmente Pagado</option>
			<option value="3">Recaudo Excedido</option>
		</select></td>
		<td><input type="submit" class="btn_submit" value="Consultar" /></td>
	</tr>
 </table>
 </form>
 <br>
 <br>
<table width="803" height="48" border="0">
  <tr>
    <th width="30">Track</th>
    <th width="142">Fecha Creacion</th>
	<th width="142">Cliente</th>
	<th width="142">Total</th>
	<th width="142">Recaudo Total</th>
	<th width="142">Estado</th>
	<th width="142">Creador por</th>
    <th colspan="1">Acciones</th>
  </tr>
  <?php
   $row=$obj->getInvoiceInf();
   
   if(count($row)>0){
 	foreach($row as $row){
  ?>
  <tr>
    <td><?php echo $row["in_track"];?></td>
	<td><?php echo $row["in_fecha"];?></td>
	<td><?php 
	$row1 = $customer->getCustomerById2($row['c_id']);
	if(count($row1)>0)
	{
		foreach($row1 as $row1)
		{
			echo $row1['c_nom'];
		}
	}
	
	?></td>
	<td>$<?php echo number_format($row["in_total"]);?></td>
	<td>$<?php echo $obj->getCollectInvoiceTotal($row["in_id"]);?></td>
	<td><?php echo $obj->getInvoiceStateName($row["in_state"]);?></td>
	<td><?php 
	 		 $user=new User();
			 $user->connect();
			 $user->getUserById($row["u_id"]);
			 echo $user->name;
	?></td>
	<td width="24"><a href="javascript:;" class="btn_3" onclick="showInfo(<?php echo $row["in_id"];?>,'modules/invoices/view/printInvoice.php')">Imprimir</a></td>
  </tr>
  <?php
	
   }
  } 
  ?>
</table>

</div>
<script>
 function showInfo(id,site)
 {
	$.fancybox.open({
					href : site+'?id='+id,
					type : 'iframe',
					padding:5
				}); 
 }
</script>
