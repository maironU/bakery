<?php
class logos extends GeCore{
	
	
	public function newLogo(){
		$name=$this->postVars('name');
		$link=$this->postVars('link');

		$sql="insert into `logos` (`l_name`, `l_link`, `l_image`) values ('$name','$link','noimg.png')";
		$this->Execute($sql);
		
		$id=$this->getLastID();
		
		$this->uploadImg($id);
	}
	
	public function uploadImg($id){
		
		$ok=$this->upLoadFileProccess('img', 'modules/logos/imgLogos');
		
		if($ok){
		 $sql="update logos set l_image='".$this->fname."' where l_id='$id'";
		 $this->Execute($sql);
		}
		
	}
	
	public function editLogo(){
		$id=$this->postVars('id');
		$name=$this->postVars('name');
		$link=$this->postVars('link');
		
		$sql="update `logos` set `l_name`='$name', `l_link`='$link' where `l_id`='$id'";
		$this->Execute($sql);
		
		$this->uploadImg($id);
	}
	
	public function delLogos(){
		$id=$this->getVars('id');
		
		$sql="delete from `logos` where l_id='$id'";
		$this->Execute($sql);
	}
	
	
	public function getLogos(){
	 $sql="select * from logos";
	 return $this->ExecuteS($sql);
	}
	
	public function getLogoById($id){
	 
	  $sql="select * from logos where l_id='$id'";
	  return $row=$this->ExecuteS($sql);
	}
	
	public function getRanLogo($limit = 5){
	 
	 
	  $row = $this->getLogos();
	  if(count($row)>0)
	  {
	      foreach($row as $row)
	      {
	          /*$img[] = array(
	               $row['l_image'],
	               $row['l_link']
	              );*/
	           $ids[] = $row['l_id'];
	      }
	  }
	  
	  $i = 0;
	  
	  while($i < $limit)
	  {
	      $pos = rand(0, count($ids));
	      if(!in_array($ids[$pos], $toshow))
	      {
	         $toshow[$i] = $ids[$pos];
	         $i++;
	      }
	  }
	  
	  for($i=0; $i<count($toshow); $i++)
	  {
	      $row = $this->getLogoById($toshow[$i]);
	      if(count($row)>0)
	      {
	          foreach($row as $row)
	          {
	              $image = $row['l_image'];
	              $link = $row['l_link'];
	          }
	          
	          $img[] = array(
	               $image,
	               $link
	              );
	      }
	  }
	  
	  return $img;
	}
	
	//Installers
   public function Checker(){
    return $this->chkTables('logos');
   }
   
   public function install(){
    $sql='CREATE TABLE `logos` (
`l_id` INT NOT NULL AUTO_INCREMENT PRIMARY KEY ,
`l_name` VARCHAR( 255 ) NOT NULL ,
`l_link` VARCHAR( 255 ) NOT NULL ,
`l_image` VARCHAR( 255 ) NOT NULL
) ENGINE = MYISAM ;';
	
	$this->Execute($sql);
   }
   
   public function uninstall(){
    $sql='DROP TABLE `logos`';
	$this->Execute($sql);
   }
	
};
?>
