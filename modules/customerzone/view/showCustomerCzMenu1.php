<?php
 include('modules/customerzone/model/customerzone.php');
 
 $msg = false;
 
 $obj = new customerzone();
 $obj->connect();
 
 $id=$obj->getVars('id');
 $id_menu=$obj->getVars('id_menu');
 $action=$obj->getVars('action');
 
 if($action=='del')
 {
  $obj->delCzCustomerMenu($id, $id_menu);
 }
 
 if($action=='add')
 {
  $obj->newCzCustomerMenu($id, $id_menu);
 }
 
?>
<div class="widget3">
 <div class="widgetlegend"> Menus para zona clientes </div>
 <?php
  if($msg)
  {
  ?>
   <div class="ui-widget">
	<div class="ui-state-highlight ui-corner-all" style="margin-top: 20px; padding: 0 .7em;">
		<p><span class="ui-icon ui-icon-info" style="float: left; margin-right: .3em;"></span>
		<strong>Felicitaciones!</strong> Se ha sido eliminado satisfactoriamente.</p>
	</div>
</div>
  <?php
  }
 ?>
<p style="width:100">
	<a href="<?php $_SERVER['PHP_SELF'];?>?p=modules/customerzone/view/index.php" class="btn_normal" style="float:left; margin:5px;">Volver </a>
</p><br />
<br />
<br />
<br />
<table width="100%">
<tr>
 <th>ID</th>
 <th>Nombre</th>
 <th>Link</th>
 <th colspan="1">Acciones</th>
</tr>
<?php
 $row = $obj->getCzMenu();
 if(count($row)>0){
  foreach($row as $row){
   ?>
   <tr>
	 <td><?php echo $row["czm_id"];?></td>
	 <td><?php echo $row["czm_name"];?></td>
	 <td><?php echo $row["czm_link"];?></td>
	 <td><?php
	  if($obj->checkCzCustomerMenu($id, $row['czm_id']))
	  {
	  ?>
	   <a href="<?php $_SERVER['PHP_SELF'];?>?id=<? echo $id; ?>&id_menu=<?php echo $row['czm_id']?>&p=modules/customerzone/view/showCustomerCzMenu1.php&action=del" class="btn_2">Quitar</a>
	  <?php
	  }
	  else
	  {
	  ?>
	   <a href="<?php $_SERVER['PHP_SELF'];?>?id=<? echo $id; ?>&id_menu=<?php echo $row['czm_id']?>&p=modules/customerzone/view/showCustomerCzMenu1.php&action=add" class="btn_1">Agregar</a>
	  <?php
	  }
	 ?></td>
	 
	 
   </tr>
  
   <?php
  }
 }
?>
 
</table>

</div>