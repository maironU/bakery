<?php
 include('modules/customerzone/model/customerzone.php');
 
 $msg = false;
 
 $obj = new customerzone();
 $obj->connect();
 
 $id=$obj->getVars('id');
 $row=$obj->getCzMenuById($id);
 foreach($row as $row)
 {
  $name = $row['czm_name'];
  $link = $row['czm_link'];
 }
 
 if($_POST)
 {
  $obj->editCzMenu();
  $msg=true;
 }
 
 
?>
<div class="widget3">
 <div class="widgetlegend"> Editar Menu </div>
 <?php
  if($msg)
  {
  ?>
   <div class="ui-widget">
	<div class="ui-state-highlight ui-corner-all" style="margin-top: 20px; padding: 0 .7em;">
		<p><span class="ui-icon ui-icon-info" style="float: left; margin-right: .3em;"></span>
		<strong>Felicitaciones!</strong> Se ha sido guardado satisfactoriamente.</p>
	</div>
</div>
  <?php
  }
 ?>
<p style="width:100">
	<a href="<?php $_SERVER['PHP_SELF'];?>?p=modules/customerzone/view/showCzMenu.php" class="btn_normal" style="float:left; margin:5px;">Volver </a>
	
</p><br />
<br />
<br />
<br />
<form action="" method="post"> 
<table width="100%">
<tr>
 <td><strong>Nombre:</strong></td>
 <td><input type="text" name="name" id="name" value="<?php echo $name?>" required /></td>
</tr>
<tr>
 <td><strong>link:</strong></td>
 <td><input type="text" name="link" id="link" value="<?php echo $link?>" required /></td>
</tr>
<tr>
 <td colspan="2">
 <input type="hidden" name="id" id="id" value="<?php echo $id?>" />
 <input type="submit" value="Guardar" class="btn_submit" /></td>
</tr>
</table>
</form>
</div>