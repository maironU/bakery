<?php
 include("admin/modules/customerzone/model/customerzone.php");
 include("admin/modules/crm/model/ordersModel.php");
 
 $cz=new customerzone();
 $cz->connect();
 
 $orderModelObj = new orderModelGe();
 
?>
<!-- Add mousewheel plugin (this is optional) -->
<script type="text/javascript" src="admin/modules/crm/js/jquery.mousewheel-3.0.6.pack.js"></script>
<!-- Add fancyBox main JS and CSS files -->
<script type="text/javascript" src="admin/modules/crm/js/jquery.fancybox.js?v=2.1.4"></script>
<link rel="stylesheet" type="text/css" href="admin/modules/crm/css/jquery.fancybox.css?v=2.1.4" media="screen" />
<style type="text/css">
<!--
.Estilo1 {font-weight: bold}
-->
</style>
<table width="1073" border="0">
  <tr>
    <th colspan="2"><div align="left">Mis Pedidos</div>
      <div align="left"></div></th>
  </tr>
  <tr>
    <td colspan="2"><table width="599" height="48" border="0" style="font-size:11px">
          <tr>
            <th width="65" scope="col">Track</th>
            <th width="141" scope="col">Fecha creacion </th>
            <th width="114" scope="col">Total</th>
            <th width="91" scope="col">Estado</th>
            <th colspan="2" scope="col">Acciones</th>
          </tr>
          <?php
		 $row=$cz->showCustomerOrders($_SESSION["c_id"]);
		 if(count($row)>0){
		 	foreach($row as $row){
		?>
          <tr>
            <td><?php echo $row["o_track"];?></td>
            <td><?php echo $row["o_fecha"];?></td>
            <td>$<?php echo number_format($row["o_total"]);?></td>
            <td><?php
			 $orderModelObj->connect();
             $row1=$orderModelObj->getLastOrderHistory($row["o_id"]);
			 if(count($row1)>0){
			 	foreach($row1 as $row1){
				 ?>
                 <span style="width:100px; height:50px; display:block; color:#000; background-color:#<?php echo $row1["oh_color"]?>; padding:4px; margin-right:5px;"><?php echo utf8_encode($row1["oh_name"])?></span>
                 <?php
				}
			 }
			?></td>
            	<td width="23"><a href="javascript:;" class="link-1" onclick="showInfo(<?php echo $row["o_id"];?>,'admin/modules/crm/view/showOrder.php')">Ver</a></td>
				
				<?php
				 if($orderModelObj->chkTables('track')){
				  ?>
				  <td width="52"><a href="javascript:;" onclick="showInfo(<?php echo $row["o_id"];?>,'admin/modules/tracker/view/showTrack.php')" class="link-1">Rastreo</a></td>
				  <?php
				 }
				?>
                <tr>
                 <td colspan="6">&nbsp;</td>
                </tr>
            
          </tr>
          <?php
			}
		 }
		?>
      </table></td>
  </tr>
  <tr>
    <td width="446"><div align="right"></td>
    <td width="459">&nbsp;</td>
  </tr>
</table>
<script>
 function showInfo(id,site)
 {
	$.fancybox.open({
					href : site+'?id='+id,
					type : 'iframe',
					padding:5
				}); 
 }
</script>

