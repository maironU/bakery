<?php include('lib/glob.php');?>
<!doctype html>
<!--[if lt IE 7]>      <html class="no-js lt-ie9 lt-ie8 lt-ie7" lang=""> <![endif]-->
<!--[if IE 7]>         <html class="no-js lt-ie9 lt-ie8" lang=""> <![endif]-->
<!--[if IE 8]>         <html class="no-js lt-ie9" lang=""> <![endif]-->
<!--[if gt IE 8]><!--> <html class="no-js" lang=""> <!--<![endif]-->
    <head>
        
       <?php include('lib/header.php');?>
		<link href="css/Accordion.css" rel="stylesheet" type="text/css" />
		<script type="text/javascript" src="js/Accordion.js"></script>
    </head>
    <body>

        <div class="header-container">
            <?php include('lib/top.php');?>
        </div>

        <div class="main-container">
            <div class="main wrapper clearfix">
				<?php 
		  			include("admin/modules/customerzone/view/validateSession.php");
		  		?>
                <article>
                   
                    <section>
                        <p><?php include('admin/modules/customerzone/view/newBooking.php');?></p>
                    </section>
                    
                </article>

                <bside>
                	<?php //include('lib/czmenu.php');?>
                    
                </bside>

            </div> <!-- #main -->
        </div> <!-- #main-container -->

        <div class="footer-container">
            <footer class="wrapper">
               <?php include('lib/footer.php');?>
            </footer>
        </div>

       
    </body>
</html>
