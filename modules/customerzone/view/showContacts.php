<?php
 include('modules/crm/view/includes.php');
 
 $crmModelObj->connect();
 
  //Se verifica si el modulo esta instalado
 if(!$crmModelObj->Checker()){
  $ins=true;
 }
 
 $msg=false;
 
 //Instala el modulo
 if($crmModelObj->getVars('install')){
  $crmModelObj->install();
  $msgIns=true;
 }
 
 //Actions
 if($_POST){
 $action=$crmModelObj->postVars('action');
 if(!empty($action)){
	 $res=$customerCtrlObj->_init($action);
 }
}

$action=$crmModelObj->getVars('action');
 if(!empty($action)){
	 $res=$customerCtrlObj->_init($action);
 }

//Get the customer inf
$row=$customerCtrlObj->_init('getCustomerById');
if(count($row)>0){
 foreach($row as $row){
 	$nom=$row["c_nom"];
	$nit=$row["c_nit"];
	$dir=$row["c_dir"];
	$tel=$row["c_tel"];
	$web=$row["c_web"];
	$ti_id=$row["ti_id"];
	$ccl_id=$row["ccl_id"];
	$id=$row["c_id"];
 }
}
$nit=explode('-',$nit);

?>
<link type="text/css" href="modules/crm/css/crmStyles.css" rel="stylesheet"  />
<!-- Add mousewheel plugin (this is optional) -->
<script type="text/javascript" src="modules/crm/js/jquery.mousewheel-3.0.6.pack.js"></script>
<!-- Add fancyBox main JS and CSS files -->
<script type="text/javascript" src="modules/crm/js/jquery.fancybox.js?v=2.1.4"></script>
<link rel="stylesheet" type="text/css" href="modules/crm/css/jquery.fancybox.css?v=2.1.4" media="screen" />
<style type="text/css">
<!--
.Estilo1 {font-weight: bold}
-->
</style>
<div class="widget3">
 <div class="widgetlegend">GE &reg; CRM - Clientes </div>
 <?php

  if(count($res)==0 && $_POST)
  {
  ?>
   <div class="ui-widget">
	<div class="ui-state-highlight ui-corner-all" style="margin-top: 20px; padding: 0 .7em;">
		<p><span class="ui-icon ui-icon-info" style="float: left; margin-right: .3em;"></span>
		<strong>Felicitaciones!</strong> El cliente se han actualizado satisfactoriamente.</p>
	</div>
</div>
  <?php
  }else{
  if(count($res)>0){
    for($i=0;$i<count($res);$i++){
	 ?>
	  <div class="ui-widget">
		<div class="ui-state-error ui-corner-all" style="margin-top: 20px; padding: 0 .7em;">
			<p><span class="ui-icon ui-icon-info" style="float: left; margin-right: .3em;"></span>
			<strong>Error</strong> <?php echo $res[$i]; ?></p>
		</div>
   		</div>
	 <?php
	}
   }
  }
  
 ?>
<p style="width:100">
	<a href="<?php $_SERVER['PHP_SELF'];?>?p=modules/crm/view/showCustomers.php" class="btn_normal" style="float:left; margin:5px;">Volver </a>
</p><br />
<br />
<br />

<form name="form1" method="post" action="#"><table width="900" border="0">
  <tr>
    <th width="459"><div align="left">Contacto</div></th>
  </tr>
  <tr>
    <td valign="top"><table width="881" border="0">
      
      <tr>
        <th width="114">Nombre</th>
        <th width="323">Usuario</th>
        <th width="246">Contrase&ntilde;a</th>
        <th width="180">Acciones</th>
        </tr>
      <?php
	   $rowe=$customerCtrlObj->_init('GetContact');
	   if(count($rowe)>0){
	    foreach($rowe as $row){
		?>
      <tr>
        <td><?php echo $row["cc_nom"];?></td>
        <td><?php echo $row["cc_user"];?></td>
        <td><?php 
		if($row["cc_pass"]==''){
		 echo "Contrase&ntilde;a no configurada";	
		}else{
		 echo "Contrase&ntilde;a configurada";	
		}?></td>
        <td><a href="#" class="btn_1" onclick="showInfo(<?php echo $row["cc_id"]; ?>,'modules/customerzone/view/editContactUser.php')">Editar</a></td>
        </tr>
      <?php
		}
	   }
	  ?>
      
    </table></td>
  </tr>
  <tr>
    <td>&nbsp;</td>
  </tr>
</table>
</form>
</div>
<script>
 function showInfo(id,site)
 {
	$.fancybox.open({
					href : site+'?id='+id,
					type : 'iframe',
					padding:5
				}); 
 }
</script>

