<?php
 include('modules/crm/view/includes.php');
 include('modules/customerzone/model/customerzone.php');
 
 $obj = new customerzone();
 $obj->connect();
 
 $crmModelObj->connect();
 $customerModelObj->connect();
 
 $id=$obj->getVars('id');
 $id_customer=$obj->getVars('id_customer');
 $action=$obj->getVars('action');
 
 $msg=false;
 $msgIns=false;
 
 if($action=='del')
 {
  $obj->delCzCustomerMenu($id_customer, $id);
 }
 
 if($action=='add')
 {
  $obj->newCzCustomerMenu($id_customer, $id);
 }
 
 
?>
<script type="text/javascript" src="modules/crm//js/jquery-1.10.1.js"></script>
<script type="text/javascript" src="modules/crm//js/jquery-ui-1.9.2.custom.js"></script>
<script type="text/javascript" src="modules/crm//js/jsCrmFunctions.js"></script>
<link href="modules/crm//css/jquery-ui.css" rel="stylesheet" type="text/css" />
<link type="text/css" href="modules/crm/css/crmStyles.css" rel="stylesheet"  />
<div class="widget3">
 <div class="widgetlegend">Asignar menu a Clientes </div>
 <?php
  if($msg)
  {
  ?>
   <div class="ui-widget">
	<div class="ui-state-highlight ui-corner-all" style="margin-top: 20px; padding: 0 .7em;">
		<p><span class="ui-icon ui-icon-info" style="float: left; margin-right: .3em;"></span>
		<strong>Felicitaciones!</strong> Se ha sido eliminado satisfactoriamente.</p>
	</div>
</div>
  <?php
  }
 ?>
<p style="width:100">
	<a href="<?php $_SERVER['PHP_SELF'];?>?p=modules/customerzone/view/showCzMenu.php" class="btn_normal" style="float:left; margin:5px;">Volver </a>
</p><br />
<br />
<br />

<form action="" method="post" name="frmSearch">
<table width="100%" border="0">
  <tr>
    <td width="6%">Buscar</td>
    <td width="20%"><label>
      <input type="text" name="finder" id="finder" nkeyup="myAutoComplete('finder','modules/crm/view/findCustomer.php')" onkeydown="myAutoComplete('finder','modules/crm/view/findCustomer.php')"/>
      <input name="action" type="hidden" id="action" value="FindCustomer" />
    </label></td>
    <td width="74%"><a href="javascript:;" class="btn_1" onclick="document.frmSearch.submit();">Buscar</a></td>
  </tr>
</table>
</form>

<br />
<table width="100%" border="0">
<tr>
 <th>ID</th>
 <th>Nombre</th>
 <th>Identificacion</th>
 <th>Direccion</th>
 <th>Telefono</th>
 <th colspan="1">Actions</th>
</tr>
<?php
 $action=$crmModelObj->postVars('action');
 if($action=='FindCustomer'){
 	$row=$customerCtrlObj->_init($action);
 }else{
 	$row=$customerCtrlObj->_init();
 }
 if(count($row)>0){
  foreach($row as $row){
   ?>
   <tr>
	 <td><?php echo $row["c_id"];?></td>
	 <td><?php echo $row["c_nom"];?></td>
	 <td><?php echo $row["c_nit"];?></td>
	 <td><?php echo $row["c_dir"];?></td>
	 <td><?php echo $row["c_tel"];?></td>
	 <td>
	 <?php
	  if($obj->checkCzCustomerMenu($row["c_id"], $id))
	  {
	  ?>
	   <a href="<?php $_SERVER['PHP_SELF'];?>?id_customer=<? echo $row['c_id']; ?>&id=<?php echo $id;?>&p=modules/customerzone/view/showCustomerCzMenu.php&action=del" class="btn_2">Quitar</a>
	  <?php
	  }
	  else
	  {
	  ?>
	   <a href="<?php $_SERVER['PHP_SELF'];?>?id_customer=<? echo $row['c_id']; ?>&id=<?php echo $id;?>&p=modules/customerzone/view/showCustomerCzMenu.php&action=add" class="btn_1">Agregar</a>
	  <?php
	  }
	 ?>
	 </td>
   </tr>
  
   <?php
  }
 }
?>
 <tr>
     <td colspan="12"><?php
	    $customerModelObj->startPage($customerModelObj->postVars('page'));
		$num=$customerModelObj->getTotalCustomers();
		echo $customerModelObj->getPages($num);
	   ?></td>
    </tr>
</table>

</div>