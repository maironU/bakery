<?php
 include('modules/customerzone/model/customerzone.php');
 
 $msg = false;
 
 $obj = new customerzone();
 $obj->connect();
 
 if($obj->getVars('actionDel')=='true')
 {
  $obj->delCzMenu();
  $msg = true;
 }
?>
<div class="widget3">
 <div class="widgetlegend"> Menus para zona clientes </div>
 <?php
  if($msg)
  {
  ?>
   <div class="ui-widget">
	<div class="ui-state-highlight ui-corner-all" style="margin-top: 20px; padding: 0 .7em;">
		<p><span class="ui-icon ui-icon-info" style="float: left; margin-right: .3em;"></span>
		<strong>Felicitaciones!</strong> Se ha sido eliminado satisfactoriamente.</p>
	</div>
</div>
  <?php
  }
 ?>
<p style="width:100">
	<a href="<?php $_SERVER['PHP_SELF'];?>?p=modules/customerzone/view/index.php" class="btn_normal" style="float:left; margin:5px;">Volver </a>
	<a href="<?php $_SERVER['PHP_SELF'];?>?p=modules/customerzone/view/newCzMenu.php" class="btn_normal" style="float:left; margin:5px;">Nuevo </a>
</p><br />
<br />
<br />
<br />
<table width="100%">
<tr>
 <th>ID</th>
 <th>Nombre</th>
 <th>Link</th>
 <th colspan="3">Acciones</th>
</tr>
<?php
 $row = $obj->getCzMenu();
 if(count($row)>0){
  foreach($row as $row){
   ?>
   <tr>
	 <td><?php echo $row["czm_id"];?></td>
	 <td><?php echo $row["czm_name"];?></td>
	 <td><?php echo $row["czm_link"];?></td>
	 <td><a href="<?php $_SERVER['PHP_SELF'];?>?id=<? echo $row['c_id']; ?>&p=modules/customerzone/view/editCzMenu.php&id=<?php echo $row['czm_id']?>" class="btn_normal">Editar</a></td>
	 <td><a href="<?php $_SERVER['PHP_SELF'];?>?id=<? echo $row['c_id']; ?>&p=modules/customerzone/view/showCustomerCzMenu.php&id=<?php echo $row['czm_id']?>" class="btn_normal">Asignar</a></td>
	 <td><a href="<?php $_SERVER['PHP_SELF'];?>?id=<? echo $row['c_id']; ?>&p=modules/customerzone/view/showCzMenu.php&id=<?php echo $row['czm_id']?>&actionDel=true" class="btn_borrar">Eliminar</a></td>
   </tr>
  
   <?php
  }
 }
?>
 <tr>
     <td colspan="12"><?php
	    $customerModelObj->startPage($customerModelObj->postVars('page'));
		$num=$customerModelObj->getTotalCustomers();
		echo $customerModelObj->getPages($num);
	   ?></td>
    </tr>
</table>

</div>