<?php
  include('../../../core/config.php');
  include('../../../core/ge.php');
  include('../model/customerzone.php');
  
  $obj=new customerzone();
  $obj->connect();
  
  $msg=false;
  
  $id=$obj->getVars('id');
  $row=$obj->getContactUser($id);
  foreach($row as $row){
   $nom=$row["cc_nom"];
   $mail=$row["cc_mail"];
   $user=$row["cc_user"];
  }
  
  if($_POST){
   $obj->editUserPass();
   $msg=true;
  }
  
  
?>
<link href="../../../css/scsStyle.css" rel="stylesheet" type="text/css" />
<link href="../../../css/smoothness/jquery-ui-1.9.2.custom.css" rel="stylesheet">
<link href="../../../css/smoothness/jquery-ui-1.9.2.custom.min.css" rel="stylesheet" type="text/css" />
<script src="../../../js/jquery-1.8.3.js"></script>
<script src="../../../js/jquery-ui-1.9.2.custom.js"></script>
<script type="text/javascript">
 $(function() {
		$( ".datepicker" ).datepicker();
		$('.datepicker').datepicker('option', {dateFormat: 'yy-mm-dd'});
       
	});
</script>
<?php
 if($msg){
 ?>
  <div class="ui-widget">
	<div class="ui-state-highlight ui-corner-all" style="margin-top: 20px; padding: 0 .7em;">
		<p><span class="ui-icon ui-icon-info" style="float: left; margin-right: .3em;"></span>
		<strong>Felicitaciones!</strong> El usuario y contrase&ntilde;a han sido modificados satisfactoriamente.</p>
	</div>
</div>
 <?php
 }
?>

<div class="widget3">
 <div class="widgetlegend">Editar Usuario </div>
  <form action="" method="post" name="form1">
    <table width="696" height="185" border="0" align="center">
      <tr>
        <td width="136">Nombre:</td>
        <td width="238"><?php echo $nom; ?></td>
      </tr>
      <tr>
        <td width="136">Email:</td>
        <td width="238"><?php echo $mail; ?></td>
      </tr>
      <tr>
        <td>Usuario:</td>
        <td><input name="user" type="text" id="user" value="<?php echo $user; ?>" /></td>
      </tr>
      <tr>
        <td>Contrase&ntilde;a:</td>
        <td>
        	<input name="pass" type="password" id="pass" /><br />
        	Si la contrase&ntilde;a ya esta configurada, y no desea colocar una nueva contrase&ntilde;a, deje este espacio en blanco.
        </td>
      </tr>
      <tr>
        <td><input name="id" type="hidden" id="id" value="<?php echo $id; ?>" /></td>
        <td><a href="#" class="btn_normal" onclick="document.form1.submit();">Guardar</a></td>
      </tr>
    </table>
  </form>

</div>