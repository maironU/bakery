<?php
 include("admin/modules/customerzone/model/customerzone.php");
 $cz=new customerzone();
 $cz->connect();
?>
<!-- Add mousewheel plugin (this is optional) -->
<script type="text/javascript" src="admin/modules/crm/js/jquery.mousewheel-3.0.6.pack.js"></script>
<!-- Add fancyBox main JS and CSS files -->
<script type="text/javascript" src="admin/modules/crm/js/jquery.fancybox.js?v=2.1.4"></script>
<link rel="stylesheet" type="text/css" href="admin/modules/crm/css/jquery.fancybox.css?v=2.1.4" media="screen" />
<style type="text/css">
<!--
.Estilo1 {font-weight: bold}
-->
</style>
<table width="1073" border="0">
  <tr>
    <th colspan="2"><div align="left">Mis Cotizaciones</div>
      <div align="left"></div></th>
  </tr>
  <tr>
    <td colspan="2"><table width="599" height="48" border="0" style="font-size:12px;">
      <tr>
        <th width="83" scope="col">Track</th>
        <th width="196" scope="col">Fecha creacion </th>
        <th width="216" scope="col">Total</th>
        <th width="301" scope="col">Fecha Vencimiento </th>
        <th scope="col">&nbsp;</th>
      </tr>
      <?php
		 $row=$cz->showCustomerQuotes($_SESSION["c_id"]);
		 $i=0;
		 if(count($row)>0){
		 	foreach($row as $row){
				if($i%2==0){
				 $bg="#f5f5f5";
				}else{
				 $bg="#fff";
				}
				$i++;
		?>
      <tr bgcolor="<?php echo $bg; ?>">
        <td><?php echo $row["q_track"];?></td>
        <td><?php echo $row["q_fecha"];?></td>
        <td>$<?php echo number_format($row["q_total"]);?></td>
        <td><?php echo $row["q_fecha_fin"];?></td>
        <td><a href="javascript:;" class="link-1" onclick="showInfo(<?php echo $row["q_id"];?>,'admin/modules/crm/view/showQuote.php')">Ver</a></td>
      </tr>
      <tr>
       <td colspan="5">&nbsp;</td>
      </tr>
      <?php
			}
		 }
		?>
    </table></td>
  </tr>
  <tr>
    <td width="446"><div align="right"></td>
    <td width="459">&nbsp;</td>
  </tr>
</table>
<script>
 function showInfo(id,site)
 {
	$.fancybox.open({
					href : site+'?id='+id,
					type : 'iframe',
					padding:5
				}); 
 }
</script>

