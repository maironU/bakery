<?php
 include('modules/crm/view/includes.php');
 
 $crmModelObj->connect();
 $customerModelObj->connect();
 
  //Se verifica si el modulo esta instalado
 if(!$crmModelObj->Checker()){
  $ins=true;
 }
 
 $msg=false;
 $msgIns=false;
 
 //Instala el modulo
 if($crmModelObj->getVars('install')){
  $crmModelObj->install();
  $msgIns=true;
 }
 
 //Actions
 $action=$crmModelObj->getVars('action');
 if(!empty($action)){
	 $customerCtrlObj->_init($action);
 }
 
?>
<script type="text/javascript" src="modules/crm//js/jquery-1.10.1.js"></script>
<script type="text/javascript" src="modules/crm//js/jquery-ui-1.9.2.custom.js"></script>
<script type="text/javascript" src="modules/crm//js/jsCrmFunctions.js"></script>
<link href="modules/crm//css/jquery-ui.css" rel="stylesheet" type="text/css" />
<link type="text/css" href="modules/crm/css/crmStyles.css" rel="stylesheet"  />
<?php
 if(!$ins){
 ?>
<div class="widget3">
 <div class="widgetlegend"> Clientes </div>
 <?php
  if($msg)
  {
  ?>
   <div class="ui-widget">
	<div class="ui-state-highlight ui-corner-all" style="margin-top: 20px; padding: 0 .7em;">
		<p><span class="ui-icon ui-icon-info" style="float: left; margin-right: .3em;"></span>
		<strong>Felicitaciones!</strong> Se ha sido eliminado satisfactoriamente.</p>
	</div>
</div>
  <?php
  }
 ?>
<p style="width:100">
	<a href="<?php $_SERVER['PHP_SELF'];?>?p=modules/crm/view/newCustomer.php" class="btn_normal" style="float:left; margin:5px;">Nuevo Cliente </a>
	<a href="<?php $_SERVER['PHP_SELF'];?>?p=modules/customerzone/view/showCzMenu.php" class="btn_normal" style="float:left; margin:5px;">Menus </a>
</p><br />
<br />
<br />

<form action="" method="post" name="frmSearch">
<table width="100%" border="0">
  <tr>
    <td width="6%">Buscar</td>
    <td width="20%"><label>
      <input type="text" name="finder" id="finder" nkeyup="myAutoComplete('finder','modules/crm/view/findCustomer.php')" onkeydown="myAutoComplete('finder','modules/crm/view/findCustomer.php')"/>
      <input name="action" type="hidden" id="action" value="FindCustomer" />
    </label></td>
    <td width="74%"><a href="javascript:;" class="btn_1" onclick="document.frmSearch.submit();">Buscar</a></td>
  </tr>
</table>
</form>

<br />
<table width="100%" border="0">
<tr>
 <th>ID</th>
 <th>Nombre</th>
 <th>Identificacion</th>
 <th>Direccion</th>
 <th>Telefono</th>
 <th colspan="2">Actions</th>
</tr>
<?php
 $action=$crmModelObj->postVars('action');
 if($action=='FindCustomer'){
 	$row=$customerCtrlObj->_init($action);
 }else{
 	$row=$customerCtrlObj->_init();
 }
 if(count($row)>0){
  foreach($row as $row){
   ?>
   <tr>
	 <td><?php echo $row["c_id"];?></td>
	 <td><?php echo $row["c_nom"];?></td>
	 <td><?php echo $row["c_nit"];?></td>
	 <td><?php echo $row["c_dir"];?></td>
	 <td><?php echo $row["c_tel"];?></td>
	 <td><div align="center"><a href="<?php $_SERVER['PHP_SELF'];?>?id=<? echo $row['c_id']; ?>&p=modules/customerzone/view/showContacts.php" class="btn_1">Contactos</a></div></td>
	 <td><div align="center"><a href="<?php $_SERVER['PHP_SELF'];?>?id=<? echo $row['c_id']; ?>&p=modules/customerzone/view/showCustomerCzMenu1.php" class="btn_3">Menu</a></div></td>
   </tr>
  
   <?php
  }
 }
?>
 <tr>
     <td colspan="12"><?php
	    $customerModelObj->startPage($customerModelObj->postVars('page'));
		$num=$customerModelObj->getTotalCustomers();
		echo $customerModelObj->getPages($num);
	   ?></td>
    </tr>
</table>

</div>
<?php } else {?>
<br />
 
 <div class="widget3">
 <div class="widgetlegend">Instalador Modulo Zona de Clientes </div>
 <?php
  if($msgIns)
  {
  ?>
   <div class="ui-widget">
	<div class="ui-state-highlight ui-corner-all" style="margin-top: 20px; padding: 0 .7em;">
		<p><span class="ui-icon ui-icon-info" style="float: left; margin-right: .3em;"></span>
		<strong>Felicitaciones!</strong> El modulo ha sido instalado satisfactoriamente.</p>
	</div>
</div>
  <?php
  }
 ?>
<form id="form1" name="form1" method="post" action="#">
  <table width="804" border="0" style="float:left;">
    <tr>
      <td width="525" valign="top">Bienvenido al instalador Modulo Zona de Clientes . </td>
    </tr>
    <tr>
      <td valign="top"><div align="right"><a href="<?php $_SERVER['PHP_SELF'];?>?p=modules/customezone/view/index.php&amp;install=true" class="btn_normal">Instalar</a></div></td>
    </tr>
  </table>
</form>
</div>
<?php } ?>
