<?php
 
 if($action="newbooking"){
  $res=$booking->ValidateDisp();
  
	  if($res=='okall'){
	  	$booking->newBooking();
	  	$msg=true;
	  }
	
 }else{
 	 $msg=false;
 }
 

?>

<script type="text/javascript" src="../../booking/js/jsFunctions.js"></script>
<!-- Add mousewheel plugin (this is optional) -->
<script type="text/javascript" src="../../booking/js/jquery.mousewheel-3.0.6.pack.js"></script>
<!-- Add fancyBox main JS and CSS files -->
<script type="text/javascript" src="../../booking/js/jquery.fancybox.js?v=2.1.4"></script>
<link rel="stylesheet" type="text/css" href="../../booking/css/jquery.fancybox.css?v=2.1.4" media="screen" />
<script type="text/javascript" src="../../../js/jquery-1.8.3.js"></script>
<script type="text/javascript" src="../../../js/jquery-ui-1.9.2.custom.js"></script>
<link href="../../../css/smoothness/jquery-ui-1.9.2.custom.css" rel="stylesheet">
<script type="text/javascript">

 $.datepicker.regional['es'] = {
 closeText: 'Cerrar',
 prevText: '<Ant',
 nextText: 'Sig>',
 currentText: 'Hoy',
 monthNames: ['Enero', 'Febrero', 'Marzo', 'Abril', 'Mayo', 'Junio', 'Julio', 'Agosto', 'Septiembre', 'Octubre', 'Noviembre', 'Diciembre'],
 monthNamesShort: ['Ene','Feb','Mar','Abr', 'May','Jun','Jul','Ago','Sep', 'Oct','Nov','Dic'],
 dayNames: ['Domingo', 'Lunes', 'Martes', 'Miércoles', 'Jueves', 'Viernes', 'Sábado'],
 dayNamesShort: ['Dom','Lun','Mar','Mié','Juv','Vie','Sáb'],
 dayNamesMin: ['Do','Lu','Ma','Mi','Ju','Vi','Sa'],
 weekHeader: 'Sm',
 firstDay: 1,
 isRTL: false,
 showMonthAfterYear: false,
 yearSuffix: ''
 };
 $.datepicker.setDefaults($.datepicker.regional['es']);
 $(function() {
		$( ".datepicker" ).datepicker();
		$('.datepicker').datepicker('option', {dateFormat: 'yy-mm-dd'});
		$( ".datepicker2" ).datepicker();
		$('.datepicker2').datepicker('option', {dateFormat: 'yy-mm-dd'});
       
	});
</script>
 
 <?php
  
  if($res=='okall')
  {
  ?>
       <p>Su reserva se hizo satisfactoriamente.</p>
  <?php
  }
 ?>
 
 <?php
  if($res=='nob')
  {
  ?>
       <p>No hay disponibilidad de campos</p>
  <?php
  }
 ?>
 
 <?php
  if($res=='nop')
  {
  ?>
       <p>El cupo se encuentra completamente lleno en la fecha y hora indicada</p>
  <?php
  }
 ?>
 
 <?php
  if($res=='noadd')
  {
  ?>
       <p>No hay disponibilidad del adicional q esta solicitando. Puede realizar la reserva sin este adicional</p>
  <?php
  }
 ?>

<form method="post" action="#" name="frmbooking">
<input type='hidden' name='id_customer' value='<?php echo $_SESSION['ccuser_name'].":".$_SESSION['c_id']; ?>' />
<input type='hidden' name='action' value='newbooking' />
								<div class="row uniform 50%">
									<div class="6u 12u$(xsmall)">
										<input type="text" name="date" id="date" value="" placeholder="Fecha" class="datepicker" />
									</div>
									<div class="6u$ 12u$(xsmall)">
										<input type="text" name="nump" value="<?php echo $_SESSION["nump"]?>" id="nump" value="<?php echo $_SESSION["nump"]?>" placeholder="No. Personas" />
									</div>
									<div class="12u$">
										<div class="select-wrapper">
											<select name="hour" id="demo-category">
												<option value="">- Hora -</option>
												<?php
        if(!empty($_SESSION["hour"])){
		?>
         <option value="<?php echo $_SESSION["hour"]?>" selected="selected"><?php echo $_SESSION["hour"]?></option>
        <?php
		}
	   ?>
       <option value="8">8 a.m.</option>
       <option value="9">9 a.m.</option>
       <option value="10">10 a.m.</option>
       <option value="11">11 a.m.</option>
       <option value="12">12 p.m.</option>
       <option value="13">1 p.m.</option>
       <option value="14">2 p.m.</option>
       <option value="15">3 p.m.</option>
       <option value="16">4 p.m.</option>
       <option value="17">5 p.m.</option>
       <!--<option value="18">6 p.m.</option>
       <option value="19">7 p.m.</option>
       <option value="20">8 p.m.</option>
       <option value="21">9 p.m.</option>
       <option value="22">10 p.m.</option> -->
											</select>
										</div>
									</div>
									<div class="12u$">
										<div class="select-wrapper">
											<select name="id_event" id="id_event">
												<option value="">- Evento -</option>
												<?php
         $row=$booking->getBookingEvent();
		 foreach($row as $row){
			 if($_SESSION["id_event"]==$row["be_id"]){
			 ?>
        		<option value="<?php echo $row["be_id"]?>" selected="selected"><?php echo utf8_encode($row["be_name"])?></option>
        		<?php
            
			 }else{
			 	?>
        		<option value="<?php echo $row["be_id"]?>"><?php echo utf8_encode($row["be_name"])?></option>
        		<?php
			 }
			 
		 }
		?>
											</select>
										</div>
									</div>
									<div class="12u$">
										<div class="select-wrapper">
											<select name="id_add" id="id_add">
											<option value="">- Adicionales -</option>
												<?php
         $row=$booking->getBookingAdd();
		 foreach($row as $row){
			 if($_SESSION["id_add"]==$row["ba_id"]){
			 ?>
        <option value="<?php echo $row["ba_id"]?>" selected="selected"><?php echo utf8_encode($row["ba_name"])?></option>
        <?php
			 }else{
			 ?>
        <option value="<?php echo $row["ba_id"]?>"><?php echo utf8_encode($row["ba_name"])?></option>
        <?php
			 }
			 
		 }
		?>
											</select>
										</div>
									</div>
									<div class="12u$">
										<textarea name="desc" id="desc" placeholder="Detalles" rows="6"><?php echo $_SESSION["desc"]?></textarea>
									</div>
									<div class="12u$">
										<ul class="actions">
											<li><input type="button" onclick="document.frmbooking.submit()" value="Reservar" class="special" /></li>
											<li><input type="reset" value="Borrar" class="special"/></li>
										</ul>
									</div>
								</div>
							</form>


 

<script>
 function showInfo(id,site)
 {
	$.fancybox.open({
					href : site+'?id='+id,
					type : 'iframe',
					padding:5
	}); 
 }
 
 function findCustomer(){
	document.getElementById('gauch').style.display='block';
	var term=document.getElementById('finder').value;
	$.ajax({
	 type:'POST',
	 url:'modules/booking/view/findCustomer.php',
	 async:false,
	 data: 'term='+term,
	 success:function(data){
		 document.getElementById('gauch').style.display='none';
		 document.getElementById('showCl').innerHTML=data;
	 }
	});
}
</script>