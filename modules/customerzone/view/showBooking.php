<?php
 include("admin/modules/customerzone/model/customerzone.php");
 include("admin/modules/booking/model/booking.php");
 include("admin/modules/crm/model/customerModel.php");

include('admin/includes/class.phpmailer.php');
 include('admin/includes/class.pop3.php');
 include('admin/includes/class.smtp.php');
 
 $cz=new customerzone();
 $cz->connect();
 
 $booking = new booking();
 $booking->connect();
 
 
 
 if($cz->getVars('ActionDel')=='true' && $cz->getVars('confirm')!='true'){
	 $id=$cz->getVars('id');
	 $actionDel=$cz->getVars('ActionDel');
	 ?>
      <div style="width:90%; display:block; height:90%; background-color:#F90; border: solid 1px #999">
       <p>Si confirma anular la reserva, esta quedara disponible para que otro cliente pueda usar este espacio. Si usted anular su reserva, tomaría el riesgo de no encontrar disponibilidad de nuestro campo en la fecha y hora deseada. Confirma anular su actual reserva? <a href="showMyBooking.php?id=<?php echo $id;?>&ActionDel=<?php echo $actionDel?>&confirm=true">Confirmar</a></p>
      </div>
     <?php
 }
 
 if($cz->getVars('ActionDel')=='true' && $cz->getVars('confirm')=='true'){
  $id=$cz->getVars('id');
  $booking->delBooking2($id);
  ?>
  <div style="width:90%; display:block; height:90%; background-color:#F90; border: solid 1px #999">
       <p>Su reserva fue anulada satisfactoriamente</p>
      </div>
  <?php
  
 }
 
?>
<!-- Add mousewheel plugin (this is optional) -->
<script type="text/javascript" src="admin/modules/crm/js/jquery.mousewheel-3.0.6.pack.js"></script>
<!-- Add fancyBox main JS and CSS files -->
<script type="text/javascript" src="admin/modules/crm/js/jquery.fancybox.js?v=2.1.4"></script>
<link rel="stylesheet" type="text/css" href="admin/modules/crm/css/jquery.fancybox.css?v=2.1.4" media="screen" />
<style type="text/css">
<!--
.Estilo1 {font-weight: bold}
-->
</style>
<table>
  <tr>
   <td colspan="10"><a href="newMyBooking.php" class="btn_normal">Nuevo</a></td>
  </tr>
  <tr>
    <th width="142">Fecha</th>
    <th width="142">Hora</th>
    <th width="142">Perso.</th>
    <th width="213">Evento</th>
    <th width="213">Adicional</th>
    <th>Acciones</th>
    <?php
	 $i=0;
     $row=$cz->showCustomerBooking($_SESSION['c_id']);
	 if(count($row)>0){
	  foreach($row as $row){
	   if($i%2==0){
		 //$bg="#f5f5f5";
		}else{
		 //$bg="#fff";
		}
		$i++;
	  
	  
	  ?>
      <tr bgcolor="<?php echo $bg; ?>">
    <td><?php echo $booking->displayDate($row["b_date"]);?></td>
    <td><?php echo $booking->displayHour($row["b_hour"]);?></td>
   <td><?php echo $row["b_numpeople"];?></td>
   <td><?php 
	$row1=$booking->getBookingEventById($row["event_id"]);
	if(count($row1)>0){
	foreach($row1 as $row1){
	 echo utf8_encode($row1["be_name"]);
	}
	}
	?></td>
    <td><?php 
	$row1=$booking->getBookingAddById($row["add_id"]);
	if(count($row1)>0){
	foreach($row1 as $row1){
	 echo utf8_encode($row1["ba_name"]);
	}
	}
	?></td>
    <td width="39"><a href="showMyBooking.php?id=<?php echo $row["b_id"]?>&ActionDel=true" class="btn_normal">Anular</a></td>
    
  </tr>
<tr>
 <td colspan="6"><hr></td>
</tr>
      <?php

	 }
	 }
	?>
  </tr>
</table>
<script>
 function showInfo(id,site)
 {
	$.fancybox.open({
					href : site+'?id='+id,
					type : 'iframe',
					padding:5
				}); 
 }
</script>

