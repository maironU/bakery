<?php
 include('core/menu.php');
 
 $obj = new menuCore();
 $msg=false;
 $msg2=false;
 
 if($_POST){
  $obj->connect();
  $obj->newMenuWithFile('mod', '/modules/');
  $obj->close();
  $msg=true;
 }
 
 if($obj->getVars('ActionDel')){
 	$obj->connect();
  	$model=$obj->delMenu();
  	$obj->close();
	
	$folder=strtolower($model);
	
	//Se desinstala modulo
	$path="modules/".$folder."/view/uninstaller.php";
	include($path);
	
	$obj = new menuCore();
	$msg2=true;
 }
?>

<div class="widget3">
 <div class="widgetlegend">Modulos</div>
 <?php
  if($msg)
  {
  ?>
   <div class="ui-widget">
	<div class="ui-state-highlight ui-corner-all" style="margin-top: 20px; padding: 0 .7em;">
		<p><span class="ui-icon ui-icon-info" style="float: left; margin-right: .3em;"></span>
		<strong>Felicitaciones!</strong> El modulo ha sido agregado. Por favor proceda a su correspondiente instalacion</p>
	</div>
</div>
  <?
  }
 
  if($msg2)
  {
  ?>
   <div class="ui-widget">
	<div class="ui-state-highlight ui-corner-all" style="margin-top: 20px; padding: 0 .7em;">
		<p><span class="ui-icon ui-icon-info" style="float: left; margin-right: .3em;"></span>
		<strong>Felicitaciones!</strong> El modulo ha sido eliminado. </p>
	</div>
</div>
  <?php
  }
 ?>
<p>
 <a href="home.php?p=adminMod.php" class="btn_normal" style="float:left; margin-right: 5px;">Volver</a>
 <a href="home.php?p=adminMod2.php" class="btn_normal" style="float:left">Usar ZIP</a>
</p>
<br />
<br />
<br /> 
<form id="forma" name="forma" method="post" action="#" enctype="multipart/form-data">
      <table width="100%"  border="0" align="center">
        <tr>
          <td width="139">Cargue el archivp ZIP del modulo </td>
          <td width="188"><label>
            <input name="mod" type="file" id="mod"/>
          </label></td>
        </tr>
        <tr>
          <td>&nbsp;</td>
          <td><label>
            <input type="submit" name="Submit" value="Agregar" class="btn_submit"/>
          </label></td>
        </tr>
        <tr>
          <td colspan="2"><h2>Modulos </h2>  </td>
          <td colspan="2"><h2>Complementos Instalados</h2></td>
        </tr>
        <tr>
          <td colspan="2" valign="top"><table width="355" border="0" align="center">
            <?
			 $i=1;
			 $obj->connect();
			 $row=$obj->getModules();
			 $obj->close();
			 
			 if(count($row)>0){
			 foreach($row as $row)
			 {
			  
			  if($i%2==0)
			  {
			   $bg="#f5f5f5";
			  }
			  else
			  {
			   $bg="#ffffff";
			  }
			?>
            <tr bgcolor="<? echo $bg; ?>">
              <td width="97"><label><a href="<?php $_SERVER['PHP_SELF']; ?>?p=adminMod2.php&id=<? echo $row["m_id"]; ?>&ActionDel=true" class="btn_borrar">Desinstalar</a></label></td>
              <td width="171"><i class="fa <? echo $row["m_icon"]; ?> fa-lg"></i>&nbsp;<? echo $row["m_nom"]; ?></td>
            </tr>
            <?
		    $i++;
		   }
		  }
		  ?>
          </table></td>
        
          <td colspan="2" valign="top"><table width="355" border="0" align="center">
            <?
			 $i=1;
			 $obj->connect();
			 $row=$obj->getModules(2);
			 $obj->close();
			 
			 if(count($row)>0){
			 foreach($row as $row)
			 {
			  
			  if($i%2==0)
			  {
			   $bg="#f5f5f5";
			  }
			  else
			  {
			   $bg="#ffffff";
			  }
			?>
            <tr bgcolor="<? echo $bg; ?>">
              <td width="97"><label><a href="<?php $_SERVER['PHP_SELF']; ?>?p=adminMod2.php&id=<? echo $row["m_id"]; ?>&ActionDel=true" class="btn_borrar">Desinstalar</a></label></td>
              <td width="171"><i class="fa <? echo $row["m_icon"]; ?> fa-lg"></i>&nbsp;<? echo $row["m_nom"]; ?></td>
            </tr>
            <?
		    $i++;
		   }
		  }
		  ?>
          </table></td>
        </tr>
      </table>
  </form>
</div>