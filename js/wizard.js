let active = null;

$(document).ready(function(){
    active = $(".tab-active").data('wizard')
})

$(".wizard-navigation ul li").on('click', function(){
    if(!fullFields()) return;

    active = $(this).data('wizard')

    if(active == "producto"){
        movingTab("Producto", 100)
    }else{
        movingTab("Bodega", 0)
    }

    activeTab(active)
    showButtonsFooter()
})

$(".btn-next").on('click', function(){
    if(!fullFields()) return;
    active = "producto"

    activeTab(active)
    showButtonsFooter()
    movingTab("Producto", 100)
})

$(".btn-previous").on('click', function(){
    active = "bodega"

    activeTab(active)
    showButtonsFooter()
    movingTab("Bodega", 0)
})

function movingTab(text, moving){
    $(".moving-tab").text(text)
    $(".moving-tab").css({'transform' : `translate3d(${moving}%, 0px, 0px)`})
}

function activeTab(active){
    $(".wizard-navigation ul li").removeClass("tab-active")
    $(`#tab-${active}`).addClass("tab-active")

    $(".tab-content-wizard .tab-pane-wizard").removeClass("tab-pane-active")
    $(`#${active}`).addClass('tab-pane-active')
}

function fullFields(){
    let tipo_movimiento = $("#type").val()
    let bodega_origen = $("#who").val()
    let bodega_destino = $("#whd").val()

    if(!tipo_movimiento || !bodega_origen || !bodega_destino){
        Toastify({
            text: "Por favor llene los campos",
            duration: 2000,
            newWindow: true,
            close: true,
            position: "center", // `left`, `center` or `right`
            stopOnFocus: true, // Prevents dismissing of toast on hover
            style: {
                background: "#dc3545",
            },
            onClick: function() {} // Callback after click
        }).showToast();

        return false
    }

    return true
}

function showButtonsFooter(){
    if(active == "bodega"){
        $(".pull").removeClass("pull-btn-active")
        $(".btn-next").addClass("pull-btn-active")
    }else {
        $(".pull").removeClass("pull-btn-active")
        $(".btn-previous").addClass("pull-btn-active")
        $(".btn-finish").addClass("pull-btn-active")
    }
}