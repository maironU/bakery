function getChartCustomerType()
{
	$.ajax({
	 type:'POST',
	 url:'modules/graphreport/view/getChartCustomerType.php',
	 async:false,
	 success:function(data){
		
		var ctx = document.getElementById("customertype").getContext("2d");
		
		ctx.canvas.width = "200";
		ctx.canvas.height = "200";
		/*window.myBar = new Chart(ctx).Pie(JSON.parse(data), {
			responsive : false
		});*/
		var myLineChart = new Chart(ctx, {
		    type: 'doughnut',
		    data: JSON.parse(data),
		});
	 }
	});
}

function getChartCustomerInd()
{
	$.ajax({
	 type:'POST',
	 url:'modules/graphreport/view/getChartCustomerIndustries.php',
	 async:false,
	 success:function(data){
		
		var ctx = document.getElementById("customerind").getContext("2d");
		
		ctx.canvas.width = "300";
		ctx.canvas.height = "300";
		/*window.myBar = new Chart(ctx).Doughnut(JSON.parse(data), {
			responsive : false
		});*/
		var myLineChart = new Chart(ctx, {
		    type: 'pie',
		    data: JSON.parse(data),
		});
	 }
	});
}
function getChartBestProducts()
{
	$.ajax({
	 type:'POST',
	 url:'modules/graphreport/view/getBestProducts.php',
	 async:false,
	 success:function(data){
		
		var ctx = document.getElementById("bestproducts").getContext("2d");
		
		ctx.canvas.width = "300";
		ctx.canvas.height = "300";
		/*window.myBar = new Chart(ctx).Doughnut(JSON.parse(data), {
			responsive : false
		});*/
		var myLineChart = new Chart(ctx, {
		    type: 'pie',
		    data: JSON.parse(data),
		});
	 }
	});
}
function getChartInventoryInfo()
{
	$.ajax({
	 type:'POST',
	 url:'modules/graphreport/view/getInventoryInfo.php',
	 async:false,
	 success:function(data){
		
		var ctx = document.getElementById("inventoryinfo").getContext("2d");
		
		ctx.canvas.width = "300";
		ctx.canvas.height = "200";
		/*window.myBar = new Chart(ctx).Bar(JSON.parse(data), {
			responsive : false
		});*/
		var myLineChart = new Chart(ctx, {
		    type: 'bar',
		    data: JSON.parse(data),
		});
	 }
	});
}