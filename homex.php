<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head><meta http-equiv="Content-Type" content="text/html; charset=windows-1252">
<meta name="viewport" content="width=device-width, user-scalable=no, initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0">

<title>GOE - Admin ...</title>
<link href="css/scsStyle.css" rel="stylesheet" type="text/css" />
<link href="css/font-awesome.css" rel="stylesheet" type="text/css" />
<link href="css/preloader.css" rel="stylesheet" type="text/css" />
<link href="css/modal.css" rel="stylesheet" type="text/css" />
<link href="css/smoothness/jquery-ui-1.9.2.custom.css" rel="stylesheet">
<link href="css/smoothness/jquery-ui-1.9.2.custom.min.css" rel="stylesheet" type="text/css" />
<script src="js/jquery-1.8.3.js"></script>
<script src="js/jquery-ui-1.9.2.custom.js"></script>
<script src="js/graph.js"></script>
<script src="modules/graphreport/js/Chart.js"></script>
<script src="js/modal.js"></script>
<script type="text/javascript">

 $.datepicker.regional['es'] = {
 closeText: 'Cerrar',
 prevText: '<Ant',
 nextText: 'Sig>',
 currentText: 'Hoy',
 monthNames: ['Enero', 'Febrero', 'Marzo', 'Abril', 'Mayo', 'Junio', 'Julio', 'Agosto', 'Septiembre', 'Octubre', 'Noviembre', 'Diciembre'],
 monthNamesShort: ['Ene','Feb','Mar','Abr', 'May','Jun','Jul','Ago','Sep', 'Oct','Nov','Dic'],
 dayNames: ['Domingo', 'Lunes', 'Martes', 'Miércoles', 'Jueves', 'Viernes', 'Sabado'],
 dayNamesShort: ['Dom','Lun','Mar','Mié','Juv','Vie','Sab'],
 dayNamesMin: ['Do','Lu','Ma','Mi','Ju','Vi','Sa'],
 weekHeader: 'Sm',
 firstDay: 1,
 isRTL: false,
 showMonthAfterYear: false,
 yearSuffix: ''
 };
 $.datepicker.setDefaults($.datepicker.regional['es']);
 $(function() {
		$( ".datepicker" ).datepicker();
		$('.datepicker').datepicker('option', {dateFormat: 'yy-mm-dd'});
		$( ".datepicker2" ).datepicker();
		$('.datepicker2').datepicker('option', {dateFormat: 'yy-mm-dd'});
       
	});
 $( function() {
    $( "#accordion" ).accordion({
      heightStyle: "content"
    });
  } );
  
  $( function() {
    $( "#dialog" ).dialog({
      autoOpen: false,
      show: {
        effect: "blind",
        duration: 1000
      },
      hide: {
        effect: "explode",
        duration: 1000
      }
    });
 
    $( ".opener" ).on( "click", function() {
      $( ".dialog" ).dialog( "open" );
    });
  } );
</script>
</head>

<body>
<div id="menu" class="main">
 <ul>
  <li><a href="#">inicio</a></li>
  <li><a href="#">cerrar</a></li>
 </ul>
</div><br /><br />
<br />
<br />

<div class="widget">
  <div class="widgetlegend">XX</div>
  <p>xxxxx</p>
  <p><a href="#" class="btn_normal">BOTON</a>
  <a href="#" class="btn_borrar">BOTON</a></p>
  <p>
    <label>
    <input type="text" name="textfield" />
    </label>
  </p>
  <p>
    <label>
    <textarea name="textarea"></textarea>
    </label>
  </p>
  <p>
    <label>
    <input type="checkbox" name="checkbox" value="checkbox" />
    </label>
    <label>
    <input name="radiobutton" type="radio" value="radiobutton" />
    <select name="select">
    </select>
    </label>
    <label>
    <input type="file" name="file" />
    </label>
  </p>
  <table width="200" border="0">
    <tr>
      <td>&nbsp;</td>
      <td>&nbsp;</td>
      <td>&nbsp;</td>
    </tr>
    <tr>
      <td>&nbsp;</td>
      <td>&nbsp;</td>
      <td>&nbsp;</td>
    </tr>
  </table>
  <p>&nbsp;</p>
</div>
 <div class="widget2">
  <div class="widgetlegend">XX</div>
 </div>
 <div class="widget3">
  <div class="widgetlegend">XX</div>
  <p>&nbsp;</p>
  <p>&nbsp;</p>
  <p>&nbsp;</p><p>&nbsp;</p><p>&nbsp;</p><p>&nbsp;</p><p>&nbsp;</p>
  <span class="circle1">10%</span>
  <span class="circle2">10%</span>
  <span class="circle3">10%</span>
  <span class="circle4">10%</span>
  <p>&nbsp;</p><p>&nbsp;</p><p>&nbsp;</p><p>&nbsp;</p><p>&nbsp;</p>
  <span class="box1">10%</span>
  <span class="box2">10%</span>
  <span class="box3">10%</span>
  <span class="box4">10%</span>
  <span class="box5">10%</span>
  <span class="box6">10%</span>
  <p>&nbsp;</p><p>&nbsp;</p><p>&nbsp;</p><p>&nbsp;</p><p>&nbsp;</p>
  <p>Modal</p>
  <a href="javascript:;" class="btn_2" id="btnConfirm" onClick="modal('confirm', 'btnConfirm')">Eliminar</a>
                
                <div id="confirm" class="modal">
                  <!-- Modal content -->
                  <div class="modal-content">
                     
                    <div class="modal-header">
                        <span class="close">&times;</span>
                        <h3>Boton de confirmacion</h3>
                    </div>
                    <div class="modal-body">
                         <p>confirmacion</p>
                         <p><a href="javascript:;" class="btn_borrar">Eliminar</a></p>
                    </div>
                    <div class="modal-footer">
                    </div> 
                    
                  </div>
                </div>
 </div>
</body>
</html>
