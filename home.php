<?php
 header('Content-Type: text/html; charset=iso-8859-1');
 include('core/config.php');
 include('core/ge.php');
 
 //Librerias
 include('includes/class.phpmailer.php');
 include('includes/class.pop3.php');
 include('includes/class.smtp.php');
 
 $ge = new GeCore();
 $ge->getSession();
 
 $p = $ge->getVars('p');
 
 if($ge->getVars('close')){
  $ge->closeSession();
 }

?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head><meta http-equiv="Content-Type" content="text/html; charset=windows-1252">
<meta name="viewport" content="width=device-width, user-scalable=no, initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0">

<title>GOE - Admin ...</title>
<link href="css/scsStyle.css" rel="stylesheet" type="text/css" />
<link href="css/font-awesome.css" rel="stylesheet" type="text/css" />
<link href="css/preloader.css" rel="stylesheet" type="text/css" />
<link href="css/smoothness/jquery-ui-1.9.2.custom.css" rel="stylesheet">
<link href="css/smoothness/jquery-ui-1.9.2.custom.min.css" rel="stylesheet" type="text/css" />
<link rel="stylesheet" href="css/goely.css">
<link href="https://cdn.jsdelivr.net/npm/bootstrap@5.1.3/dist/css/bootstrap.min.css" rel="stylesheet" integrity="sha384-1BmE4kWBq78iYhFldvKuhfTAU6auU8tT94WrHftjDbrCEXSU1oBoqyl2QvZ6jIW3" crossorigin="anonymous">
<link rel="stylesheet" href="css/global.css">
<link rel="stylesheet" type="text/css" href="https://cdn.jsdelivr.net/npm/toastify-js/src/toastify.min.css">
<link href="css/sb-admin-2.min.css" rel="stylesheet">
<!--<script src="js/jquery-1.8.3.js"></script>-->
<script src="js/jquery.min.js"></script>
<script src="js/jquery-ui-1.9.2.custom.js"></script>
<script src="js/graph.js"></script>
<link href="vendor/datatables/dataTables.bootstrap4.min.css" rel="stylesheet">

<!--<script id="lic" src="js/goelyliccontrol.js?lic=<?php echo $ge->license()?>"></script>-->
<script src="modules/graphreport/js/Chart.js"></script>
<link href="css/modal.css" rel="stylesheet" type="text/css" />
<script src="js/modal.js"></script>
<script type="text/javascript">

 $.datepicker.regional['es'] = {
 closeText: 'Cerrar',
 prevText: '<Ant',
 nextText: 'Sig>',
 currentText: 'Hoy',
 monthNames: ['Enero', 'Febrero', 'Marzo', 'Abril', 'Mayo', 'Junio', 'Julio', 'Agosto', 'Septiembre', 'Octubre', 'Noviembre', 'Diciembre'],
 monthNamesShort: ['Ene','Feb','Mar','Abr', 'May','Jun','Jul','Ago','Sep', 'Oct','Nov','Dic'],
 dayNames: ['Domingo', 'Lunes', 'Martes', 'Mi�rcoles', 'Jueves', 'Viernes', 'Sabado'],
 dayNamesShort: ['Dom','Lun','Mar','Mi�','Juv','Vie','Sab'],
 dayNamesMin: ['Do','Lu','Ma','Mi','Ju','Vi','Sa'],
 weekHeader: 'Sm',
 firstDay: 1,
 isRTL: false,
 showMonthAfterYear: false,
 yearSuffix: ''
 };
 $.datepicker.setDefaults($.datepicker.regional['es']);
 $(function() {
		$( ".datepicker" ).datepicker();
		$('.datepicker').datepicker('option', {dateFormat: 'yy-mm-dd'});
		$( ".datepicker2" ).datepicker();
		$('.datepicker2').datepicker('option', {dateFormat: 'yy-mm-dd'});
	});
 $( function() {
    $( "#accordion" ).accordion({
      heightStyle: "content"
    });
  } );
  
  $( function() {
    $( "#dialog" ).dialog({
      autoOpen: false,
      show: {
        effect: "blind",
        duration: 1000
      },
      hide: {
        effect: "explode",
        duration: 1000
      }
    });
 
    $( ".opener" ).on( "click", function() {
      $( ".dialog" ).dialog( "open" );
    });
  } );

  $(document).ready(function(){
    
  })
</script>
</head>

<body id="page-top" class="sidebar-toggled">
<?php include('lib/menu.php');?>
    <?php
    if(!empty($p)){ ?>
      <div class="container-fluid">
        <?php include($p); ?>
      </div>
    <?php } ?>
</div>
 <!--<div class="widget2">
  <div class="widgetlegend">&nbsp;</div>
  <?php //include('userInfo.php');?>
 </div>-->
  <script type="text/javascript" src="js/bootstrap.bundle.min.js"></script>
  <script type="text/javascript" src="js/jquery.easing.min.js"></script>
  <script type="text/javascript" src="js/sb-admin-2.js"></script>
  <script type="text/javascript" src="js/jquery.dataTables.min.js"></script>
  <script type="text/javascript" src="js/dataTables.bootstrap4.min.js"></script> 
  <script src="https://cdn.jsdelivr.net/npm/bootstrap@5.0.2/dist/js/bootstrap.bundle.min.js" integrity="sha384-MrcW6ZMFYlzcLA8Nl+NtUVF0sA7MsXsP1UyJoMp4YLEuNSfAP+JcXn/tWtIaxVXM" crossorigin="anonymous"></script> 
  <script type="text/javascript" src="js/datatables-demo.js"></script>
  <script type="text/javascript" src="https://cdn.jsdelivr.net/npm/toastify-js"></script>

</body>
</html>