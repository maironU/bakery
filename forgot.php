<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1" />
<title>BSS - Admin ...</title>
<link href="css/scsStyle.css" rel="stylesheet" type="text/css" />
<link href="css/smoothness/jquery-ui-1.9.2.custom.css" rel="stylesheet">
<script src="js/jquery-1.8.3.js"></script>
<script src="js/jquery-ui-1.9.2.custom.js"></script>

<script type="text/javascript" src="modules/sliders/js/sliders.js"></script>
<script type="text/javascript" src="usr/js/usr.js"></script>
<script type="text/javascript">
 $( "#dialog" ).dialog({
			autoOpen: true,
			width: 400,
			buttons: [
				{
					text: "Ok",
					click: function() {
						$( this ).dialog( "close" );
					}
				},
				{
					text: "Cancel",
					click: function() {
						$( this ).dialog( "close" );
					}
				}
			]
		});
</script>
<style>
 #dialog-link {
		padding: .4em 1em .4em 20px;
		text-decoration: none;
		position: relative;
	}
	#dialog-link span.ui-icon {
		margin: 0 5px 0 0;
		position: absolute;
		left: .2em;
		top: 50%;
		margin-top: -8px;
	}
.Estilo1 {color: #FFFFFF}
#bg {
  position: fixed; 
  top: 0; 
  left: 0; 

  /* Preserve aspet ratio */
  min-width: 100%;
  min-height: 100%;
  
   filter: brightness(25%);

}
</style>
</head>

<body>
    <div id="back" style="z-index: -500; min-width: 100%; min-height: 100%;">
        <div id="sliders"></div>
    </div>

<!-- ui-dialog -->

<div class="ui-overlay"><div class="ui-widget-overlay"></div><div class="ui-widget-shadow ui-corner-all" style="width: 52%; height: 64%; position: absolute; left: 20%; top: 20%;"></div></div>
	<div style="position: absolute; width: 50%; height: 58%;left: 20%; top: 20%; padding: 10px;" class="ui-widget ui-widget-content ui-corner-all">
	  <div class="ui-dialog-content ui-widget-content" style="background: none; border: 0;">
			<p>&nbsp;</p>
			<div width="100%">
	        	    <p align="center"><img src="images/logo.png" /></p>
	        </div>
	        <form action="" method="post" enctype="multipart/form-data" name="form1" id="form1" >
				<p><input type="email" name="email" id="email" placeholder="Escriba su correo electronico" required /></p>
				<p><input type="button" onclick="passRecovery1()" value="Enviar" class="btn_submit" /></p>
			</form>
			<form action="" method="post" enctype="multipart/form-data" name="form2" id="form2" style="display: none" >
				<p><input type="text" name="token" id="token" placeholder="Escriba codigo de seguridad" required /></p>
				<p><input type="button" onclick="passRecovery2()" value="Enviar" class="btn_submit" /></p>
			</form>
			<p><a href="index.php" style="color:#000">Volver</a></p>
	  </div>
	</div>
	<script>
	        
		  $("document").ready(function(){
getSlider();
			  $('#back #sliders').cycle({ 
			      pause: 1,
			      speed: 2000
			  });
		  });
		  
	</script>
</body>
</html>
