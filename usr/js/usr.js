function validate()
{
    var user = document.getElementById('user').value;
    var pass = document.getElementById('pass').value;
    
    document.getElementById('login').style.display="none";
    document.getElementById('loading').style.display="block";
    
	$.ajax({
	 type:'POST',
	 url:'usr/view/login.php',
	 async:false,
	 data: 'user='+user+"&pass="+pass,
	 success:function(data){
	     if(data == 'ok')
		 {
		     window.location.href="home.php?p=desk.php";
		 }
		 else
		 {
			document.getElementById('login').style.display="block";
    		document.getElementById('loading').style.display="none";
			document.getElementById('errmsg').style.display="block";
		 }
	 }
	});
 }
 
 function valEnter(e)
 {
     if (e.keyCode == 13) {
        validate();
        return false;
    }
 }
 
 function passRecovery1()
 {
	//document.getElementById('gauch').style.display='block';
	//document.getElementById('form1').style.display='none';
	

	 
	$.ajax({
	 type:'POST',
	 url:'usr/view/passRecoveryStep1.php',
	 async:false,
	 data:"email="+$("#email").val(),
	 success:function(data){
	     
		 if(data==0){
		  //$("form1").reset();
		  	//document.getElementById('gauch').style.display='none';
			document.getElementById('form2').style.display='block';
			alert('Se ha enviado el codigo de verificacion a tu correo electronico');
			//document.form1.reset();
		  
		 }else{
			//$( "#dialogNok" ).dialog();
			//document.getElementById('gauch').style.display='none';
			//document.getElementById('form1').style.display='block';
			alert('El correo ingresado no existe. Por favor verificar. Gracias');
		 }
		 
		 
	 }
	});
 }
 
  function passRecovery2(){
	//document.getElementById('gauch').style.display='block';
	//document.getElementById('form1').style.display='none';
	 
	$.ajax({
	 type:'POST',
	 url:'usr/view/passRecoveryStep2.php',
	 async:false,
	 data:"token="+$("#token").val(),
	 success:function(data){
		 if(data==0){
		  //$("form1").reset();
		  	//document.getElementById('gauch').style.display='none';
			document.getElementById('form2').style.display='block';
			alert('Tu nueva credencial ha sido enviada a tu correo');
			window.location.reload();
			//document.form1.reset();
			
		  
		 }else{
			//$( "#dialogNok" ).dialog();
			//document.getElementById('gauch').style.display='none';
			//document.getElementById('form1').style.display='block';
			alert('No pudo enviarse tu nueva credencial. Por favor reinicia el proceso');
			window.location.reload();
		 }
		 
		 
	 }
	});
 }
