<?php 
 class User extends GeCore
 {
   //User Info
   public $name;
   public $user;
   public $pass;
   public $active;
   public $email;
   public $sign;
   public $cel;
   public $img;
   
   //Profiles
   public $profileId;
   public $profile;
   
   public function getUsers()
   {
    $sql="select * from user, user_profile where user.up_id=user_profile.up_id and u_active=1";
	return $this->ExecuteS($sql);
   }
   
   public function getUser()
   {
    $id=$this->getVars('id');
	
	 $sql="select * from user where u_id='$id'";
 	 $row=$this->ExecuteS($sql);
 	 foreach($row as $row)
	 {
	  $this->name=$row["u_nom"];
	  $this->user=$row["u_user"];
	  $this->cel=$row["u_cel"];
	  $this->email=$row["u_mail"];
	  $this->profileId=$row["up_id"];
	  $this->img=$row["u_img"];
	  $this->employee_code=$row["employee_code"];
	 }
   }
   
   public function getUserById($id)
   {
	
	 $sql="select * from user where u_id='$id'";
 	 $row=$this->ExecuteS($sql);
 	 foreach($row as $row)
	 {
	  $this->name=$row["u_nom"];
	  $this->user=$row["u_user"];
	  $this->cel=$row["u_cel"];
	  $this->email=$row["u_mail"];
	  $this->profileId=$row["up_id"];
	  $this->img=$row["u_img"];
	 }
   }
   
   public function getUserById2($id)
   {
	
	 $sql="select * from user where u_id='$id'";
 	 return $row=$this->ExecuteS($sql);
 	 
   }
   
   public function createUsr(){
   
    $nom=$this->postVars('nom');
 	$email=$this->postVars('email');
 	$cel=$this->postVars('cel');
	
 	$user=$this->postVars('user');
 	$pass=$this->postVars('pass');
    $pro=$this->postVars('pro');
	
	$res=$this->upLoadFileProccess("img", "usr/usrImg");
	
	if($res)
	{
		$usrImg="usrImg/".$this->fname;
	}
 
 	$pass=md5($pass);
 
 	$sql="insert into `user` (`u_nom`, `u_user`, `u_pass`, `u_active`, `u_mail`, `u_sign`, `u_cel`, `up_id`, `u_img`) values ('$nom','$user','$pass','1','$email','$signa','$cel','$pro','$usrImg')";
 	
	$this->Execute($sql);
   }
   
   public function newUsr($nom, $email, $cel, $user, $pass, $pro){
	
	$res=$this->upLoadFileProccess("img", "usr/usrImg");
	
	if($res)
	{
		$usrImg="usrImg/".$this->fname;
	}
 
 	$pass=md5($pass);
 
 	$sql="insert into `user` (`u_nom`, `u_user`, `u_pass`, `u_active`, `u_mail`, `u_sign`, `u_cel`, `up_id`, `u_img`) values ('$nom','$user','$pass','1','$email','$signa','$cel','$pro','$usrImg')";
 	
	$this->Execute($sql);
   }
   
   public function editUsr(){
    
		$id=$this->postVars('id');
 		$nom=$this->postVars('nom');
 		$email=$this->postVars('email');
		$cel=$this->postVars('cel');
		$cod=$this->postVars('cod');

 
 		$user=$this->postVars('user');
 		$pass=$this->postVars('pass');
 		$pro=$this->postVars('pro');
 		
		$band=$this->postVars('band');
		
		$res=$this->upLoadFileProccess("img", "usr/usrImg");
	
		if($res)
		{
			$usrimg="usrImg/".$this->fname;
			$sql="update `user` set `u_img`='$usrimg' where u_id='$id'";
			$this->Execute($sql);
		}

		if(!empty($pass))
		{
		 $pass=md5($pass);
		 $sql="update `user` set `u_pass`='$pass' where u_id='$id'";
		 $this->Execute($sql);
		}

		if(empty($pro))
		{
		 $sql="update `user` set `u_nom`='$nom', `u_user`='$user', `u_mail`='$email', `u_cel`='$cel' , `employee_code`='$cod' where u_id='$id'";
		}
		else
		{
		 $sql="update `user` set `u_nom`='$nom', `u_user`='$user', `u_mail`='$email', `u_cel`='$cel', `employee_code`='$cod', `up_id`='$pro' where u_id='$id'";
		}

		$this->Execute($sql); 
   }
   
   public function delUsr(){
    
	$id=$this->getVars('id');
 
 	$sql="update user set u_active='0' where u_id='$id'";
 	$this->Execute($sql);
	
   }
   
   public function Auth($id_user,$pass){
	    
		$pass=md5($pass);
	    
		$sql="select * from user where u_id='$id_user'";
		$row=$this->ExecuteS($sql);
		foreach($row as $row){
		 $pass2=$row["u_pass"];		
		}
		
		if($pass==$pass2){
		 return "ok";	
		}else{
		 return "nok";	
		}   
	}
   
   /////// PROFILES
   
   public function newProfile()
   {
      $nom=$this->postVars('nom');
 
 	  $sql="insert into `user_profile` (`up_nom`) values ('$nom')";
 	  $res=$this->Execute($sql);
	
	  $this->profileId = $this->getLastID();
	  
	  $this->addPermissions();
 
   }
   
   public function editProfile()
   {
    $this->profileId=$this->postVars('id');
 	$nom=$this->postVars('nom');
 
	 $sql="update `user_profile` set `up_nom`='$nom' where up_id='".$this->profileId."'";
	 $res=$this->Execute($sql);
	 
	 $this->addPermissions();
   }
   
   public function delProfile()
   {
      $id=$this->getVars('id');
 	  $id1=$this->getVars('id1');
 
 		$sql="delete from pro_menu where up_id='$id'";
 		$this->Execute($sql);
 
 		$sql="delete from user_profile where up_id='$id'";
 		$this->Execute($sql);
   }
   
   public function addPermissions()
   {
   		 $i=1;
		 $sql="select * from menu";
		 $row=$this->ExecuteS($sql);
		 
		 foreach($row as $row)
		 {
		  $chk="chk".$i;
		  $ide="ide".$i;
		  
		  $chkx=$this->postVars($chk);
		  $idex=$this->postVars($ide);
		  
		  if($chkx==1)
		  {
		   $sql1="insert into `pro_menu` (`up_id`, `m_id`) values ('".$this->profileId."','$idex')";
		   $this->Execute($sql1);
		  }
		  $i++;
		 }
   }
   
   public function addPermissions2()
   {
   		 $id_profile=$this->postVars('id_profile');
		 $id_menu=$this->postVars('id_menu');
		 
		 $i=1;
		 $sql="select * from submenu";
		 $row=$this->ExecuteS($sql);
		 
		 foreach($row as $row)
		 {
		  $chk="chk".$i;
		  $ide="ide".$i;
		  
		  $chkx=$this->postVars($chk);
		  $idex=$this->postVars($ide);
		  
		  if($chkx==1)
		  {
		   $sql1="insert into `pro_submenu` (`up_id`, `sm_id`) values ('$id_profile','$idex')";
		   $this->Execute($sql1);
		  }
		  $i++;
		 }
   }
   
   public function delPermissions()
   {
   		 $id1=$this->getVars('id1');
 
		 $sql="delete from pro_menu where pm_id='$id1'";
		 $this->Execute($sql);
   }
   
   public function delPermissions2()
   {
   		 $id1=$this->getVars('id1');
 
		 $sql="delete from pro_submenu where psm_id='$id1'";
		 $this->Execute($sql);
   }
   
   public function getProfiles()
   {
    $sql="select * from user_profile";
	return $this->ExecuteS($sql);
   }
   
   public function getProfile(){
    
	$id = $this->getVars('id');
	
	$sql="select * from user_profile where up_id='$id'";
	$row = $this->ExecuteS($sql);
	
	foreach($row as $row)
	{
	 $this->profile=$row["up_nom"];
	}
	
   }
   
   public function getProfileById($id){
    $sql="select * from user_profile where up_id='$id'";
	$row = $this->ExecuteS($sql);
	
	foreach($row as $row)
	{
	 $this->profile=$row["up_nom"];
	}
   }
   
   public function login(){
     $user=$this->postVars('user');
	 $pass=$this->postVars('pass');
	 
	 $pass=md5($pass);
	 
	 $sql="select * from user where u_user='$user' and u_pass='$pass' and u_active='1'";
	 $row=$this->ExecuteS($sql);
	 
	 if(count($row)>0)
	 {
	  session_start();
	  
	  foreach($row as $row){
	  	$_SESSION['user_id']=$row["u_id"];
		$_SESSION['user_name']=$row["u_nom"];
		$_SESSION['user_profile']=$row["up_id"];
		$_SESSION['user_img']=$row["u_img"];
	  }
	  
	  return true;
	 }
	 else
	 {
	  return false;
	 }
   }
   
   //Forgot Password methods
   //Recovery Pass
	public function recoverPassStep1()
	{
	    $mail=$this->postVars('email');
	    
	    $sql="select * from user where u_mail='$mail'";
	    $row=$this->ExecuteS($sql);
	    
	    if(count($row)>0)
	    {
	        foreach($row as $row)
	        {
	            $this->sendMail($row["u_nom"],$mail,$row["u_id"]);
	        }
	        
	        return true;
	        
	    }
	    else
	    {
	        return false;
	    }
	
   }
   
    public function recoverPassStep2()
    {
        $token=$this->postVars('token');
        
        $sql="select * from user where u_token='$token'";
        $row=$this->ExecuteS($sql);
        if(count($row)>0)
        {
            foreach($row as $row)
            {
				$this->sendMail2($row["u_nom"],$row["u_mail"],$row["u_user"],$this->newPass($row["u_id"])); 
            }
            
            return true;
        }
        else
        {
            return false;
        }
        
    }
   
    public function newPass($id)
    {
        $char="0123456789abcdefghijklmnopqrstxyzABCDEFGHIJKLMNOPQRSTXYZ";
        for($i=0;$i<=6;$i++)
        {
            $newpass.=$char[rand(0,strlen($char))];
        }
        
        $npass=md5($newpass);
        $sql="update user set u_pass='$npass' where u_id='$id'";
        $this->Execute($sql);
        
        return $newpass;
        
	}
	
	public function changeAccount($employee_code){
		$sql="select * from user where employee_code='$employee_code'";
		$row=$this->ExecuteS($sql);
		
		if(count($row)>0){
	 		session_destroy();
			session_start();
			foreach($row as $row){
				$_SESSION['user_id']=$row["u_id"];
				$_SESSION['user_name']=$row["u_nom"];
				$_SESSION['user_profile']=$row["up_id"];
				$_SESSION['user_img']=$row["u_img"];
			}
			return true;
		}else{
			return false;
		}
	}
   
   //Send mail functions
   public function sendMail($name,$mail,$id)
   {

	 $char="0123456789";
	 for($i=0;$i<=6;$i++)
	 {
	     $token.=$char[rand(0,strlen($char))];
	 }
	 
	 $sql="update user set u_token='$token' where u_id='$id'";
	 $this->Execute($sql);
	 
	 
	 $f=fopen('../formats/passwordRecoveryStep1.html','r');
	 $html=fread($f,filesize('../formats/passwordRecoveryStep1.html'));
	 
	 
	 ini_set('SMTP',$this->getValueByName('EMAIL_HOST'));
  	 $headers="from:".$this->getValueByName('EMAIL_USER')."\r\n";
  	 $headers.= "MIME-Version: 1.0\r\n"; 
  	 $headers.= "Content-type: text/html; charset=UTF-8\r\n";
  	 
  	 
	 
	 $html=str_replace('[customername]',$name,$html);
	 $html=str_replace('[token]',$token,$html);
	 $html=str_replace('[code]',$id,$html);
	 
	 mail($mail,"Recuperacion de credenciales ".$this->customername,$html,$headers);
   }
   
    public function sendMail2($name,$mail,$user,$pass)
    {
        $f=fopen('../formats/passwordRecoveryStep2.html','r');
        $html=fread($f,filesize('../formats/passwordRecoveryStep2.html'));
        
        ini_set('SMTP',$this->getValueByName('EMAIL_HOST'));
        $headers="from:".$this->getValueByName('EMAIL_USER')."\r\n";
        $headers.= "MIME-Version: 1.0\r\n"; 
        $headers.= "Content-type: text/html; charset=UTF-8\r\n";
        
        $html=str_replace('[customername]',$name,$html);
        $html=str_replace('[user]',$user,$html);
        $html=str_replace('[pass]',$pass,$html);
        
        mail($mail,"Nuevas credenciales ".$this->customername,$html,$headers);
   }
 };
 
?>
