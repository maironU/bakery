<?php
 include('usr/model/User.php');
 
 $obj = new User();
 $obj->connect();
 
 $msg=false;
 
 if($_POST)
 {
  $obj->editUsr();
  $msg=true;
 }
 
 $obj->getUser();
?>
<div class="widget3">
 <div class="widgetlegend">Usuarios</div>
 <?php
  if($msg)
  {
  ?>
   <div class="ui-widget">
	<div class="ui-state-highlight ui-corner-all" style="margin-top: 20px; padding: 0 .7em;">
		<p><span class="ui-icon ui-icon-info" style="float: left; margin-right: .3em;"></span>
		<strong>Felicitaciones!</strong> El Usuario ha sido modificado satisfactoriamente.</p>
	</div>
</div>
  <?php
  }
 ?>
<p><a href="home.php?p=usr/view/index.php" class="btn_normal">Volver</a></p> 
<form action="#" method="post" enctype="multipart/form-data" name="forma" id="forma">
        <table width="800" border="0" align="center">
          <tr>
            <td width="128"><strong>Nombre:</strong></td>
            <td width="246"><label>
              <input name="nom" type="text" id="nom" value="<?php echo $obj->name; ?>" />
            </label></td>
          </tr>
          <tr>
            <td><strong>E-mail:</strong></td>
            <td><input name="email" type="text" id="email" value="<?php echo $obj->email; ?>" /></td>
          </tr>
          <tr>
            <td><strong>Celular:</strong></td>
            <td><input name="cel" type="text" id="cel" value="<?php echo $obj->cel; ?>" />
            </td>
          </tr>
          <tr>
            <td><strong>Codigo:</strong></td>
            <td><input name="cod" type="text" id="cod" value="<?php echo $obj->employee_code; ?>" />
            <input class="" type="button" onclick="genCode()" value="Generar Aleatorio"></input>
            
            </td>
          </tr>
          <tr>
            <td><strong><!--Firma: -->Imagen:</strong></td>
            <td><label for="img"></label>
            <input type="file" name="img" id="img" /></td>
          </tr>
          <tr>
            <td><img src="usr/<?php echo $obj->img; ?>" width="64" height="64" /></td>
            <td><input name="sign" type="hidden" id="sign" value='1'/></td>
          </tr>
          <tr>
            <td><strong>Usuario:</strong></td>
            <td><input name="user" type="text" id="user" value="<?php echo $obj->user; ?>" /></td>
          </tr>
          <tr>
            <td><strong>Contrase&ntilde;a:</strong></td>
            <td><input name="pass" type="password" id="pass" /></td>
          </tr>
          <tr>
            <td><strong>Confirmar Contrase&ntilde;a: </strong></td>
            <td><input name="cpass" type="password" id="cpass" /></td>
          </tr>
          <tr>
            <td><strong>Perfil:</strong></td>
            <td><label>
              <select name="pro" id="pro">
                <option value="">-- Seleccionar Perfil--</option>
                <?php
			 $row=$obj->getProfiles();
			 foreach($row as $row)
			 {
			  if($row["up_id"]==$obj->profileId)
			  {
			  ?>
                <option value="<?php echo $row["up_id"]; ?>" selected="selected"><?php echo $row["up_nom"]; ?></option>
                <?php
			   }
			   else
			   {
			   ?>
                <option value="<?php echo $row["up_id"]; ?>"><?php echo $row["up_nom"]; ?></option>
                <?php
			   }
			 }
			?>
              </select>
            </label></td>
          </tr>
          <tr>
            <td><input name="id" type="hidden" id="id" value="<?php echo $obj->getVars('id'); ?>" /></td>
            <td>&nbsp;</td>
          </tr>
          <tr>
            <td>&nbsp;</td>
            <td><label>
              <input type="submit" name="Submit" value="Guardar" onclick="validar();"/>
            </label></td>
          </tr>
        </table>
                              </form>
</div>

<script>
  function genCode(){
    const characters ='ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789';
    let result1= ' ';
    const charactersLength = characters.length;
    for ( let i = 0; i < 15; i++ ) {
        result1 += characters.charAt(Math.floor(Math.random() * charactersLength));
    }

    $("#cod").val(result1.trim())
  }
</script>