<?php
 include('usr/model/User.php');
 
 $obj = new User();
 $obj->connect();
 
 $msg=false;
 
 if($_POST)
 {
  $obj->editProfile();
  $msg=true;
 }
 
 if($_GET)
 {
  if($obj->getVars('ActionDelPermisos'))
  {
   $obj->delPermissions();
  }
 }
 
 $obj->getProfile();
 $id=$obj->getVars('id');
?>

<div class="widget3">
 <div class="widgetlegend">Usuarios</div>
 <?php
  if($msg)
  {
  ?>
   <div class="ui-widget">
	<div class="ui-state-highlight ui-corner-all" style="margin-top: 20px; padding: 0 .7em;">
		<p><span class="ui-icon ui-icon-info" style="float: left; margin-right: .3em;"></span>
		<strong>Felicitaciones!</strong> El perfil ha sido editado satisfactoriamente.</p>
	</div>
</div>
  <?php
  }
 ?>
<p><a href="home.php?p=usr/view/profilesUser.php" class="btn_normal">Volver</a></p> 
<form id="forma" name="forma" method="post" action="#">
      <table width="522" height="72" border="0" align="center">
        <tr>
          <td width="166">Nombre del Perfil </td>
          <td width="346"><label>
            <input name="nom" type="text" id="nom" value="<?php echo $obj->profile; ?>"/>
          </label></td>
        </tr>
        <tr>
          <td><input name="id" type="hidden" id="id" value="<?php echo $id; ?>" /></td>
          <td>&nbsp;</td>
        </tr>
        <tr>
          <td>Permisos</td>
          <td>&nbsp;</td>
        </tr>
        <tr>
          <td colspan="2"><table width="370" border="0" align="center">
            <tr>
              <td colspan="3">Menus</td>
            </tr>
            <?php
			 $i=1;
			 $row=$obj->getMenu();
			 foreach($row as $row)
			 {
			  $chk="chk".$i;
			  $ide="ide".$i;
			  
			  if($i%2==0)
			  {
			   $bg="#f5f5f5";
			  }
			  else
			  {
			   $bg="#ffffff";
			  }
			?>
            <tr bgcolor="<?php echo $bg; ?>">
              <td width="96"><label>
                <?php
				  $sql1="select * from pro_menu where up_id='$id' and m_id='".$row["m_id"]."'";
				  $res1=$obj->ExecuteS($sql1);
				  if(count($res1)>0)
				  {
				   $row1=$obj->ExecuteS($sql1);
				   foreach($row1 as $row1)
				   {
				     ?>
                		<a href="<?php $_SERVER['PHP_SELF']; ?>?p=usr/view/editProfile.php&id1=<?php echo $row1["pm_id"]; ?>&amp;id=<?php echo $id; ?>&ActionDelPermisos=true" class="btn_borrar">Quitar Permiso</a>
                	 <?php
				   }
				   
				  }
				  else
				  {
				 ?>
                <input type="checkbox" name="<?php echo $chk; ?>" value="1" />
                <input type="hidden" name="<?php echo $ide; ?>" value="<?php echo $row["m_id"]; ?>" />
                <?php
				  }
				 ?>
              </label></td>
              <td width="136"><?php echo $row["m_nom"]; ?></td>
              <td width="124">
               <?php
                $sql2="select * from submenu where m_id='".$row["m_id"]."'";
				$rowe=$obj->ExecuteS($sql2);
				if(count($rowe)>0){
				?>
                 <a href="<?php $_SERVER['PHP_SELF']; ?>?p=usr/view/editProfile2.php&id_menu=<?php echo $row["m_id"]; ?>&amp;id_profile=<?php echo $id; ?>" class="btn_normal">Permisos submenu</a>
                <?php
				}
			   ?>
              </td>
            </tr>
            <?php
		    $i++;
		   }
		  ?>
            <tr>
              <td>&nbsp;</td>
              <td colspan="2"><input name="id" type="hidden" id="id" value="<?php echo $id; ?>" /></td>
            </tr>
          </table></td>
        </tr>
        <tr>
          <td colspan="2"><label>
              <div align="center">
                <input type="submit" name="Submit" value="Editar Perfil"  />
              </div>
            </label></td>
        </tr>
      </table>
                </form>
</div>
