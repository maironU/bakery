<?php
 include('usr/model/User.php');
 
 $obj = new User();
 $obj->connect();
 
 $msg=false;
 
 if($_POST)
 {
  $obj->newProfile();
  $msg=true;
 }
?>

<div class="widget3">
 <div class="widgetlegend">Usuarios</div>
 <?php
  if($msg)
  {
  ?>
   <div class="ui-widget">
	<div class="ui-state-highlight ui-corner-all" style="margin-top: 20px; padding: 0 .7em;">
		<p><span class="ui-icon ui-icon-info" style="float: left; margin-right: .3em;"></span>
		<strong>Felicitaciones!</strong> El perfil ha sido creado satisfactoriamente.</p>
	</div>
</div>
  <?php
  }
 ?>
<p><a href="home.php?p=usr/view/profilesUser.php" class="btn_normal">Volver</a></p> 
<form id="forma" name="forma" method="post" action="#">
      <table width="401" height="72" border="0" align="center">
        <tr>
          <td width="139">Nombre del Perfil </td>
          <td width="188"><label>
            <input name="nom" type="text" id="nom" />
          </label></td>
        </tr>
        <tr>
          <td>&nbsp;</td>
          <td>&nbsp;</td>
        </tr>
        <tr>
          <td>Permisos</td>
          <td>&nbsp;</td>
        </tr>
        <tr>
          <td colspan="2"><table width="278" border="0" align="center">
              <tr>
                <td colspan="2">Menus</td>
              </tr>
              <?php
			  $i=1;
			 $permisos = $obj->getMenu();
			 foreach($permisos as $row)
			 {
			  $chk="chk".$i;
			  $ide="ide".$i;
			  
			  if($i%2==0)
			  {
			   $bg="#f5f5f5";
			  }
			  else
			  {
			   $bg="#ffffff";
			  }
			?>
              <tr bgcolor="<? echo $bg; ?>">
                <td width="97"><label>
                  <input type="checkbox" name="<? echo $chk; ?>" value="1" />
                  <input type="hidden" name="<? echo $ide; ?>" value="<? echo $row["m_id"]; ?>" />
                </label></td>
                <td width="171"><? echo $row["m_nom"]; ?></td>
              </tr>
              <?php
		    $i++;
		   }
		  ?>
              <tr>
                <td>&nbsp;</td>
                <td>&nbsp;</td>
              </tr>
          </table></td>
        </tr>
        <tr>
          <td colspan="2"><label>
              <div align="center">
                <input type="submit" name="Submit" value="Crear Perfil"  />
              </div>
            </label></td>
        </tr>
      </table>
                </form>
</div>
