<?php
 include('usr/model/User.php');
 
 $obj = new User();
 $obj->connect();
 
 $msg=false;

 //Delete action
 if($obj->getVars('delAction')=='yes'){
  $obj->delUsr();
  
  $msg=true;
 }
?>
<div class="widget3">
 <div class="widgetlegend">Usuarios</div>
<?php
  if($msg)
  {
  ?>
   <div class="ui-widget">
	<div class="ui-state-highlight ui-corner-all" style="margin-top: 20px; padding: 0 .7em;">
		<p><span class="ui-icon ui-icon-info" style="float: left; margin-right: .3em;"></span>
		<strong>Felicitaciones!</strong> El Usuario ha sido eliminado satisfactoriamente.</p>
	</div>
</div>
  <?php
  }
 ?>
<p style="width:100">
	<a href="home.php?p=usr/view/newUser.php" class="btn_normal" style="float:left; margin:5px;">Nuevo Usuario</a>
	<a href="home.php?p=usr/view/profilesUser.php" class="btn_normal" style="float:left; margin:5px;">Pefiles</a>
</p>
 
<table border="0" align="center" style="float:left">
  <tr>
    <th width="43"><div align="center"><strong>ID</strong></div></th>
    <th width="164"><div align="center"><strong>Nombre </strong></div></th>
    <th width="98"><div align="center"><strong>Email</strong></div></th>
    <th width="91"><div align="center"><strong>celular</strong></div></th>
    <th width="73"><div align="center"><strong>Usuario</strong></div></th>
    <th width="109"><div align="center"><strong>Perfil</strong></div></th>
    <th colspan="2"><div align="center"><strong>Actions</strong></div></th>
  </tr>
  <?php	
    $row = $obj->getUsers();
	foreach($row as $row)
	{       
  ?>
  <tr>
    <td><div align="center"><?php echo $row["u_id"]; ?></div></td>
    <td><div align="center"><?php echo $row["u_nom"]; ?></div></td>
    <td><div align="center"><?php echo $row["u_mail"]; ?></div></td>
    <td><div align="center"><?php echo $row["u_cel"]; ?></div></td>
    <td><div align="center"><?php echo $row["u_user"]; ?></div></td>
    <td><div align="center"><?php echo $row["up_nom"]; ?></div></td>
    <td width="45"><div align="center"><a href="home.php?id=<?php echo $row["u_id"]; ?>&p=usr/view/editUser.php" class="btn_normal">Editar</a></div></td>
    <td width="70"><div align="center"><a href="home.php?id=<?php echo $row["u_id"]; ?>&p=usr/view/index.php&delAction=yes" class="btn_borrar">Eliminar</a></div></td>
  </tr>
  <?php
    }
  ?>
  <tr>
    <td>&nbsp;</td>
    <td>&nbsp;</td>
    <td colspan="4">&nbsp;</td>
    <td>&nbsp;</td>
    <td>&nbsp;</td>
  </tr>
</table>
</div>
