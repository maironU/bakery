<?php
 include('usr/model/User.php');
 
 $obj = new User();
 $obj->connect();
 
 $msg=false;
 
 if($_POST)
 {
  $obj->addPermissions2();
  $msg=true;
 }
 
 if($_GET)
 {
  if($obj->getVars('ActionDelPermisos'))
  {
   $obj->delPermissions2();
  }
 }
 
 $id_menu=$obj->getVars('id_menu');
 $id_profile=$obj->getVars('id_profile');
?>

<div class="widget3">
 <div class="widgetlegend">Usuarios</div>
 <?php
  if($msg)
  {
  ?>
   <div class="ui-widget">
	<div class="ui-state-highlight ui-corner-all" style="margin-top: 20px; padding: 0 .7em;">
		<p><span class="ui-icon ui-icon-info" style="float: left; margin-right: .3em;"></span>
		<strong>Felicitaciones!</strong> El perfil ha sido editado satisfactoriamente.</p>
	</div>
</div>
  <?php
  }
 ?>
<p><a href="home.php?p=usr/view/profilesUser.php" class="btn_normal">Volver</a></p> 
<form id="forma" name="forma" method="post" action="#">
      <table width="522" height="72" border="0" align="center">
        <tr>
          <td><input name="id_menu" type="hidden" id="id" value="<?php echo $id_menu; ?>" />
          <input name="id_profile" type="hidden" id="id_profile" value="<?php echo $id_profile; ?>" /></td>
          <td>&nbsp;</td>
        </tr>
        <tr>
          <td>Permisos</td>
          <td>&nbsp;</td>
        </tr>
        <tr>
          <td colspan="2"><table width="370" border="0" align="center">
            <tr>
              <td colspan="3">Submenus</td>
            </tr>
            <?php
			 $i=1;
			 $row=$obj->getSubMenu($id_menu);
			 foreach($row as $row)
			 {
			  $chk="chk".$i;
			  $ide="ide".$i;
			  
			  if($i%2==0)
			  {
			   $bg="#f5f5f5";
			  }
			  else
			  {
			   $bg="#ffffff";
			  }
			?>
            <tr bgcolor="<?php echo $bg; ?>">
              <td width="96"><label>
                <?php
				  $sql1="select * from pro_submenu where up_id='$id_profile' and sm_id='".$row["sm_id"]."'";
				  $res1=$obj->ExecuteS($sql1);
				  if(count($res1)>0)
				  {
				   $row1=$obj->ExecuteS($sql1);
				   foreach($row1 as $row1)
				   {
				     ?>
                		<a href="<?php $_SERVER['PHP_SELF']; ?>?p=usr/view/editProfile2.php&id1=<?php echo $row1["psm_id"]; ?>&amp;id_profile=<?php echo $id_profile; ?>&ActionDelPermisos=true&id_menu=<?php echo $id_menu;?>" class="btn_borrar">Quitar Permiso</a>
                	 <?php
				   }
				   
				  }
				  else
				  {
				 ?>
                <input type="checkbox" name="<?php echo $chk; ?>" value="1" />
                <input type="hidden" name="<?php echo $ide; ?>" value="<?php echo $row["sm_id"]; ?>" />
                <?php
				  }
				 ?>
              </label></td>
              <td width="136"><?php echo $row["sm_nom"]; ?></td>
              <td width="124">&nbsp;</td>
            </tr>
            <?php
		    $i++;
		   }
		  ?>
            <tr>
              <td>&nbsp;</td>
              <td colspan="2">&nbsp;</td>
            </tr>
          </table></td>
        </tr>
        <tr>
          <td colspan="2"><label>
              <div align="center">
                <input type="submit" name="Submit" value="Editar Perfil"  />
              </div>
            </label></td>
        </tr>
      </table>
                </form>
</div>
