<?php
 include('usr/model/User.php');
 
 $obj = new User();
 $obj->connect();
 
 $msg=false;
 
 if($_POST)
 {
  $obj->createUsr();
  $msg=true;
 }
?>

<div class="widget3">
 <div class="widgetlegend">Usuarios</div>
 <?php
  if($msg)
  {
  ?>
   <div class="ui-widget">
	<div class="ui-state-highlight ui-corner-all" style="margin-top: 20px; padding: 0 .7em;">
		<p><span class="ui-icon ui-icon-info" style="float: left; margin-right: .3em;"></span>
		<strong>Felicitaciones!</strong> El Usuario ha sido creado satisfactoriamente.</p>
	</div>
</div>
  <?php
  }
 ?>
<p><a href="home.php?p=usr/view/index.php" class="btn_normal">Volver</a></p> 
<form action="#" method="post" enctype="multipart/form-data" name="forma" id="forma">
        <table width="800" border="0" align="center">
          <tr>
            <td width="128"><strong>Nombre:</strong></td>
            <td width="246"><label>
              <input name="nom" type="text" id="nom" />
            </label></td>
          </tr>
          <tr>
            <td><strong>E-mail:</strong></td>
            <td><input name="email" type="text" id="email" /></td>
          </tr>
          <tr>
            <td><strong>Celular:</strong></td>
            <td><input name="cel" type="text" id="cel" /></td>
          </tr>
          <tr>
            <td><strong><!--Firma: -->Imagen:</strong></td>
            <td><label for="img"></label>
            <input type="file" name="img" id="img" /></td>
          </tr>
          <tr>
            <td>&nbsp;</td>
            <td><input name="sign" type="hidden" id="sign" value='1'/></td>
          </tr>
          <tr>
            <td><strong>Usuario:</strong></td>
            <td><input name="user" type="text" id="user" /></td>
          </tr>
          <tr>
            <td><strong>Contrase&ntilde;a:</strong></td>
            <td><input name="pass" type="password" id="pass" /></td>
          </tr>
          <tr>
            <td><strong>Confirmar Contrase&ntilde;a: </strong></td>
            <td><input name="cpass" type="password" id="cpass" /></td>
          </tr>
          <tr>
            <td><strong>Perfil:</strong></td>
            <td><label>
              <select name="pro" id="pro">
                <option value="">-- Seleccionar Perfil--</option>
                <?php
			 $row=$obj->getProfiles();
			 foreach($row as $row)
			 {
			  ?>
                <option value="<?php echo $row["up_id"]; ?>"><?php echo $row["up_nom"]; ?></option>
                <?php
			 }
			?>
              </select>
            </label></td>
          </tr>
          <tr>
            <td>&nbsp;</td>
            <td>&nbsp;</td>
          </tr>
          <tr>
            <td>&nbsp;</td>
            <td><label>
              <input type="submit" name="Submit" value="Crear" onclick="validar();"/>
            </label></td>
          </tr>
        </table>
                              </form>
</div>
