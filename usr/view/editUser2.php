<?php
 include('usr/model/User.php');
 
 $obj = new User();
 $obj->connect();
 
 $msg=false;
 
 if($_POST)
 {
  $obj->editUsr();
  $msg=true;
 }
 
 $obj-> getUserById($_SESSION['user_id']);
?>
<div class="widget3">
 <div class="widgetlegend">Mi Usuario</div>
 <?php
  if($msg)
  {
  ?>
   <div class="ui-widget">
	<div class="ui-state-highlight ui-corner-all" style="margin-top: 20px; padding: 0 .7em;">
		<p><span class="ui-icon ui-icon-info" style="float: left; margin-right: .3em;"></span>
		<strong>Felicitaciones!</strong> El Usuario ha sido modificado satisfactoriamente.</p>
	</div>
</div>
  <?php
  }
 ?>

<form action="#" method="post" enctype="multipart/form-data" name="forma" id="forma">
        <table width="800" border="0" align="center">
          <tr>
            <td width="128"><strong>Nombre:</strong></td>
            <td width="246"><label>
              <input name="nom" type="text" id="nom" value="<?php echo $obj->name; ?>" />
            </label></td>
          </tr>
          <tr>
            <td><strong>E-mail:</strong></td>
            <td><input name="email" type="text" id="email" value="<?php echo $obj->email; ?>" /></td>
          </tr>
          <tr>
            <td><strong>Celular:</strong></td>
            <td><input name="cel" type="text" id="cel" value="<?php echo $obj->cel; ?>" /></td>
          </tr>
          <tr>
            <td><strong><!--Firma: -->Imagen:</strong></td>
            <td><label for="img"></label>
            <input type="file" name="img" id="img" /></td>
          </tr>
          <tr>
            <td><img src="usr/<?php echo $obj->img; ?>" width="64" height="64" /></td>
            <td><input name="sign" type="hidden" id="sign" value='1'/></td>
          </tr>
          <tr>
            <td><strong>Usuario:</strong></td>
            <td><input name="user" type="text" id="user" value="<?php echo $obj->user; ?>" /></td>
          </tr>
          <tr>
            <td><strong>Contrase&ntilde;a:</strong></td>
            <td><input name="pass" type="password" id="pass" /></td>
          </tr>
          <tr>
            <td><strong>Confirmar Contrase&ntilde;a: </strong></td>
            <td><input name="cpass" type="password" id="cpass" /></td>
          </tr>
          <tr>
            <td>
                <input name="id" type="hidden" id="id" value="<?php echo $_SESSION['user_id']; ?>" />
                <input name="pro" type="hidden" id="pro" value="<?php echo $obj->profileId; ?>" />
            </td>
            <td>&nbsp;</td>
          </tr>
          <tr>
            <td>&nbsp;</td>
            <td><label>
              <input type="submit" name="Submit" value="Guardar" onclick="validar();"/>
            </label></td>
          </tr>
        </table>
                              </form>
</div>
