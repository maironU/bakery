<?php
 include('usr/model/User.php');
 
 $obj = new User();
 $obj->connect();
 
 $msg=false;

 //Delete action
 if($obj->getVars('delAction')=='yes'){
  $obj->delProfile();
  
  $msg=true;
 }
?>
<div class="widget3">
 <div class="widgetlegend">Usuarios</div>
<?php
  if($msg)
  {
  ?>
   <div class="ui-widget">
	<div class="ui-state-highlight ui-corner-all" style="margin-top: 20px; padding: 0 .7em;">
		<p><span class="ui-icon ui-icon-info" style="float: left; margin-right: .3em;"></span>
		<strong>Felicitaciones!</strong> El Usuario ha sido eliminado satisfactoriamente.</p>
	</div>
</div>
  <?php
  }
 ?>
<p style="width:100">
	<a href="home.php?p=usr/view/newProfile.php" class="btn_normal" style="float:left; margin:5px;">Nuevo Perfil</a>
</p>
 
<table width="584" border="0" align="center">
      <tr>
        <td width="92"><div align="center"><strong>ID</strong></div></td>
        <td width="281"><div align="center"><strong>Perfil</strong></div></td>
        <td colspan="2"><div align="center"><strong>Actions</strong></div></td>
        </tr>
  <?	
    $row = $obj->getProfiles();
	foreach($row as $row)
	{       
  ?>
  <tr>
    <td><div align="center"><? echo $row["up_id"]; ?></div></td>
    <td><div align="center"><? echo $row["up_nom"]; ?></div></td>
    <td width="45"><div align="center"><a href="<?php $_SERVER['PHP_SELF']; ?>?id=<? echo $row["up_id"]; ?>&p=usr/view/editProfile.php" class="btn_normal">Editar</a></div></td>
    <td width="70"><div align="center"><a href="<?php $_SERVER['PHP_SELF']; ?>?id=<? echo $row["up_id"]; ?>&p=usr/view/profilesUser.php&delAction=yes" class="btn_borrar">Eliminar</a></div></td>
  </tr>
  <?
	   }
	  ?>
  <tr>
    <td>&nbsp;</td>
    <td>&nbsp;</td>
    <td colspan="4">&nbsp;</td>
    <td>&nbsp;</td>
    <td>&nbsp;</td>
  </tr>
</table>
</div>
