<!--<div id="resmenu"><a href="javascript:;" onClick="runResMenu()"><i class="fa fa-bars"></i></a></div>-->
<div id="wrapper">
  <ul class="navbar-nav bg-gradient-dark sidebar sidebar-dark accordion toggled bg-color-bottom" id="accordionSidebar">
    <a class="sidebar-brand d-flex align-items-center justify-content-center" href="home.php?p=desk.php">
        <div class="sidebar-brand-icon rotate-n-15">
            <i class="fas fa-laugh-wink"></i>
        </div>
        <div class="sidebar-brand-text mx-3">Columbus</div>
    </a>

<!-- PRIMERA PARTE DEL MENU -->
    <?php 
      $ge->connect();
      $sql="select * from menu,pro_menu where menu.m_id=pro_menu.m_id and pro_menu.up_id='".$_SESSION['user_profile']."' and menu.m_isModule='0' order by menu.m_id asc";      
      $menu=$ge->ExecuteS($sql);
      foreach($menu as $option){ ?>
        <li class="nav-item submenu-link-first">
            <a class="nav-link" href="<?php echo $option['m_link']; ?>">
                <i class="fa <?php echo $option['m_icon']; ?> fa-lg"></i>
                <span><?php echo $option['m_nom']; ?></span>
            </a>
        </li>
    <?php } ?>

<!-- SEGUNDA PARTE DEL MENU -->
    <?php 
      $ge->connect();
      $sql="select * from menu,pro_menu where menu.m_id=pro_menu.m_id and pro_menu.up_id='".$_SESSION['user_profile']."' and menu.m_isModule='2' order by menu.m_id asc";
      $menu=$ge->ExecuteS($sql);
      foreach($menu as $option){
        $sql2="select * from submenu as sm inner join pro_submenu as psm on sm.sm_id=psm.sm_id where psm.up_id='".$_SESSION['user_profile']."' and sm.m_id='".$option["m_id"]."'";
        $submenu = $ge->ExecuteS($sql2);
        if(count($submenu) > 0){
    ?>
        <li class="nav-item">
            <a class="nav-link submenu-link" href="<?php echo $option['m_link']; ?>" data-toggle="collapse" data-toggle="collapse" data-target="#collapse<?php echo $option["m_id"] ?>"
                aria-expanded="true" aria-controls="collapseTwo">
                <i class="fa <?php echo $option['m_icon']; ?> fa-lg"></i>
                <span><?php echo $option['m_nom']; ?></span>
            </a>
            <div id="collapse<?php echo $option["m_id"] ?>" class="collapse" aria-labelledby="headingTwo" data-parent="#accordionSidebar">
                <div class="bg-white py-2 collapse-inner rounded">
                  <?php  foreach($submenu as $suboption){ ?>
                    <a class="collapse-item text-truncate" href="<?php echo $suboption["sm_link"] ?>">
                        <?php echo $suboption['sm_nom']; ?>
                    </a>
                  <?php } ?>
                </div>
            </div>
        </li>
      <?php }else{ ?>
          <li class="nav-item">
              <a class="nav-link submenu-link-first" href="<?php echo $m['m_link']; ?>">
                  <i class="fa <?php echo $option['m_icon']; ?> fa-lg"></i>
                  <span><?php echo $option['m_nom']; ?></span>
              </a>
          </li>
      <?php 
        }
      }
    ?>

<!-- TERCERCA PARTE DEL MENU -->
    <li class="nav-item">
        <a class="nav-link submenu-link" href="#" data-target="#collapseModulo"
            aria-expanded="true" aria-controls="collapseTwo">
            <i class="fa fa-american-sign-language-interpreting fa-lg"></i>
            <span style="color: rgba(255,255,255,.8)">
              Modulos
            </span>
        </a>
        <div id="collapseModulo" class="collapse" aria-labelledby="headingTwo" data-parent="#accordionSidebar">
            <div class="bg-white py-2 collapse-inner rounded">
              <?php  
                $ge->connect();
                $sql3="select * from menu,pro_menu where menu.m_id=pro_menu.m_id and pro_menu.up_id='".$_SESSION['user_profile']."' and menu.m_isModule='1' order by menu.m_id asc";
                $submenu=$ge->ExecuteS($sql3);
                foreach($submenu as $suboption){ ?>
                  <a class="collapse-item text-truncate" href="<?php echo $suboption["m_link"] ?>">
                      <i class="fa <?php echo $suboption['m_icon']; ?> fa-lg" style="color: <?php echo $suboption['m_color']?>"></i>
                      <?php echo $suboption['m_nom']; ?>
                  </a>
                <?php } ?>
            </div>
        </div>
    </li>
  </ul>

  <div id="content-wrapper" class="d-flex flex-column">
    <div id="content" style="height: 100vh">
        <nav class="navbar navbar-expand navbar-light bg-color topbar mb-4 static-top shadow">
            <button id="sidebarToggleTop" class="btn btn-link rounded-circle mr-3">
                <i class="fa fa-bars text-white"></i>
            </button>

            <img src="images/icons/logoo.png" alt="" width="130px">
            <ul class="navbar-nav ml-auto">
                <!-- Nav Item - User Information -->
                <li class="nav-item dropdown no-arrow">
                    <a class="nav-link dropdown-toggle" href="#" id="userDropdown" role="button"
                        data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                        <div class="d-flex flex-column align-items-end">
                            <span class="mr-2 d-none d-lg-inline text-white small" id="name-store"></span>
                            <span class="mr-2 d-none d-lg-inline text-white small"><?php echo $_SESSION['user_name']; ?></span>
                        </div>             
                        <img class="rounded-circle" src="usr/<?php echo $_SESSION['user_img'] ?>" width="50px" height="50px">
                        
                    </a>
                    <!-- Dropdown - User Information -->
                    <div class="dropdown-menu dropdown-menu-right shadow animated--grow-in"
                        aria-labelledby="userDropdown">
                        <a class="dropdown-item" href="#" id="profile" data-toggle="modal" data-target="#user-profile" data-backdrop="false">
                            <i class="fa fa-user" aria-hidden="true"></i>
                            Perfil
                        </a>
                        <div class="dropdown-divider"></div>
                        <a class="dropdown-item" id="change-account" href="#" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                            <i class="fa fa-lock" aria-hidden="true"></i>
                            Cambiar Cuenta
                        </a>
                        <div class="dropdown-list dropdown-menu dropdown-menu-right shadow animated--grow-in p-3"
                                aria-labelledby="change-account" id="popup-cambiar-cuenta">
                                
                                <form action="">
                                    <div class="form-group">
                                        <label for="employee_code">Codigo Unico</label>
                                        <input type="text" id="employee_code" class="form-control">
                                    </div>
                                    <button class="btn btn-success btn-block" id="button-cambiar-cuenta">Cambiar Cuenta</button>
                                    <button class="btn btn-danger btn-block" id="button-cerrar-cambiar-cuenta">Cerrar</button>
                                </form>
                        </div>
                        <div class="dropdown-divider"></div>
                        <a class="dropdown-item" href="home.php?close=true">
                            <i class="fa fa-sign-out" aria-hidden="true"></i>
                            Cerrar Sesion
                        </a>
                    </div>
                </li>
            </ul>
        </nav>

        <div class="modal fade" id="user-profile" tabindex="-1" role="dialog" aria-labelledby="user-profile" aria-hidden="true">
            <div class="modal-dialog" role="document">
                <div class="modal-content">
                <div class="modal-header">
                    <h5 class="modal-title" id="method-pay-name"></h5>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                    </button>
                </div>
                <div class="modal-body">
                    <h5 class="text-center">Informacion del usuario</h5>
                    <div id="content-profile">
                        
                    </div>
                </div>
                </div>
            </div>
        </div>
<?php
 $ge->close();
?>

<script>
    $('#change-account').on('click', (event) => {
        event.preventDefault();
        $("#employee_code").removeClass('error-button')
        $("#employee_code").val('')

        if($("#popup-cambiar-cuenta").hasClass('show')){
            $("#popup-cambiar-cuenta").removeClass('show')
        }else{
            $("#popup-cambiar-cuenta").addClass('show')
        }
        event.stopPropagation();

    });
    $("#button-cerrar-cambiar-cuenta").on('click', function(event){
        event.preventDefault()
        $("#popup-cambiar-cuenta").removeClass('show')

        event.stopPropagation()
    })

    /*$("#popup-cambiar-cuenta").on("mouseleave", function(e) {
        $("#popup-cambiar-cuenta").removeClass('show')
    });*/
    
    $("#button-cambiar-cuenta").on('click', function(event){
        event.preventDefault()
        let employee_code = $("#employee_code").val()

        if(employee_code){
            $.post('modules/posmodule/model/api.php?method=changeAccount&employee_code=${method_id}', {employee_code}).then(response => {
                if(JSON.parse(response)){
                    location.href="/bakery/home.php?p=modules/posmodule/view/index.php"
                }else{
                    Toastify({
                        text: "Codigo Invalido",
                        duration: 2000,
                        newWindow: true,
                        close: true,
                        position: "center", // `left`, `center` or `right`
                        stopOnFocus: true, // Prevents dismissing of toast on hover
                        style: {
                        background: "#e74a3b",
                        },
                        onClick: function(){} // Callback after click
                    }).showToast();
                }
            })
        }else{
            $("#employee_code").addClass('error-button')
        }
        event.stopPropagation()
    })

    $("#profile").on('click', function(){
        let id = '<?php echo $_SESSION['user_id'] ?>'
        $.get(`modules/posmodule/model/api.php?method=getUserById&value=${id}`).then(res => {
            if(res){
                let response = JSON.parse(res)
                $("#content-profile").html("")
                $("#content-profile").append(`
                    <div class="text-center">
                        <img src="usr/${response.img}" width="140px">
                    </div>
                    <div class="mt-3 d-flex">
                        <label class="mr-1" for="">Nombre: </label>
                        <span>${response.name}</span>
                    </div>
                    <div class="mt-3 d-flex">
                        <label class="mr-1" for="">Email: </label>
                        <span> ${response.email}</span>
                    </div>
                    <div class="mt-3 d-flex">
                        <label class="mr-1" for="">Celular: </label>
                        <span>${response.cel ? response.cel : "No tiene"}</span>
                    </div>
                `)
            }else{
                $("#content-profile").text("No se encontro la informacion del usuario")
            }
        })
    })



    $(document).ready(function(){
        $(".submenu-link").hover(function() {
            removeSubmenuLink()
            $(this).removeClass('collapsed')
            $(this).attr("aria-expanded", true);
            $(this).siblings().addClass("show")
        }, function(){
            $(this).attr("aria-expanded", false);
        }
        );

        $(".submenu-link-first").hover(function() {
            removeSubmenuLink()
        }, function(){
        }
        );
        $(".submenu-link").on('click', function(){
            var href = $(this).attr('href');
            location.href = href
        })

        $("#content").hover(function(){
            removeSubmenuLink()
        })
    })

    function removeSubmenuLink(){
        $(".submenu-link").each(function(){
            $(this).addClass('collapsed')
            $(this).siblings().removeClass("show")
        })
    }
        
</script>
