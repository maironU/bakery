<?php 
 class Contact extends GeCore
 {
   
   public function sendMail()
   {
   	$name=$this->postVars('name');
	$company=$this->postVars('company');
	$email=$this->postVars('email');
	$msg=$this->postVars('msg');
	
	//Adicionales
	$phone=$this->postVars('phone');
	$mob=$this->postVars('mob');
	$city=$this->postVars('city');
	$country=$this->postVars('country');
	$service=$this->postVars('service');
	$information=$this->postVars('information');
	
	$this->addBD($name,$company,$email,$msg,$phone,$mob,$city,$country,$service,$information);
	
	ini_set('SMTP','mail.goely.co');
  	$headers="from:".$email."\r\n";
  	$headers .= "MIME-Version: 1.0\r\n"; 
  	$headers .= "Content-type: text/html; charset=UTF-8\r\n"; 
	
	$f=fopen('admin/contactus/tpls/email.html','r');
	$m = fread($f,filesize('admin/contactus/tpls/email.html'));
	
	$m=str_replace('[web]',"INSCRIPCIONES TKD",$m);
	$m=str_replace('[name]',$name,$m);
	$m=str_replace('[company]',$company,$m);
	$m=str_replace('[email]',$email,$m);
	$m=str_replace('[msg]',$msg,$m);
	
	//Adicionales
	$m=str_replace('[phone]',$phone,$m);
	$m=str_replace('[mob]',$mob,$m);
	$m=str_replace('[city]',$city,$m);
	$m=str_replace('[country]',$country,$m);
	$m=str_replace('[service]',$service,$m);
	$m=str_replace('[information]',$information,$m);
	
	
	mail($this->getEmailsToSend(),"INSCRIPCIONES TKD ",$m,$headers);
	
	session_destroy();
   }
   
   public function checkCode(){
    $numsec=$this->postVars('numsec');
	$numsec1=$this->postVars('numsec1');
	
	if($numsec==$numsec1){
	 return true;	
	}else{
	 return false;	
	}
	
   }
   
   public function Validator(){
    $name=$_SESSION["name1"]=$this->postVars('name');
	$company=$_SESSION["company1"]=$this->postVars('company');
	$email=$_SESSION["email1"]=$this->postVars('email');
	$msg=$_SESSION["msg1"]=$this->postVars('msg');
	
	//Adicionales
	$tel=$_SESSION["phone1"]=$this->postVars('phone');
	$mob=$_SESSION["mob1"]=$this->postVars('mob');
	$city=$_SESSION["city1"]=$this->postVars('city');
	$country=$_SESSION["country1"]=$this->postVars('country');
	$service=$_SESSION["service1"]=$this->postVars('service');
	$information=$_SESSION["information1"]=$this->postVars('information');
	
	
	$pass=true;
	
	if(empty($name) || empty($company) || empty($tel) || empty($msg)){
	 $err.="Verificar los campos de Nombre, Empresa, telefono, movil y comentario. Ninguno debe estar vacio <br />";
	 $pass=false;
	}
	
	if(!empty($email)){
	if(preg_match("/[a-zA-Z0-9_-.+]+@[a-zA-Z0-9-]+.[a-zA-Z]+/", $email) < 0)
  	{
		$err.="correo no valido <br />";
    	$pass=false;
  	}
	}
	else{
		$pass=false;
		$err.="Verificar los campos de correo <br />";
		}
		
	if(!$this->checkCode()){
		$pass=false;
		$err.="Codigo de seguridad incorrecto <br />";
	}	
	
	if($pass){
	return "ok";}
	else{
		return $err;
		}
   }
   
   public function generateNumSeg(){
	
    return rand(10000,99999);
   }
   
   public function addBD($name=NULL,$company=NULL,$email=NULL,$msg=NULL,$phone=NULL,$mob=NULL,$city=NULL,$country=NULL,$service=NULL,$information=NULL){
	   
	   $sql="insert into `contactus` (`name`, `company`, `email`, `msg`, `phone`, `mob`, `city`, `country`, `service`, `information`) values ('$name','$company','$email','$msg','$phone','$mob','$city','$country','$service','$information')";
	   
	   $this->Execute($sql);
	   
	   $id = $this->getLastID();
	   
	   $this->uploadFile($id);
	    
	}
	
	public function uploadFile($id){
		
		$ok=$this->upLoadFileProccess('arch', '../contactFile');
		
		if($ok){
		 $sql="update `contactus` set `information`='".$this->fname."' where id_contact='$id'";
		 $this->Execute($sql);
		}
		
		
	}
	
	public function getContactus(){
	 $sql="select * from contactus order by id_contact desc";
	 return $this->ExecuteS($sql);	
	}
	
	public function getContactById($id){
		$sql="select * from contactus where id_contact='$id'";
	 	return $this->ExecuteS($sql);	
	}
	
	public function delContact(){
		$id=$this->getVars('id');
		$query="delete from contactus where id_contact='$id'";
		$this->Execute($query);
	}
	
	//Method email config
	public function newEmail(){
		$name=$this->postVars('name');
		$email=$this->postVars('email');
		
		$sql="insert into `contactus_emails` (`ce_name`, `ce_mails`) values ('$name','$email')";
		$this->Execute($sql);
	}
	
	public function editEmail(){
		$id=$this->postVars('id');
		$name=$this->postVars('name');
		$email=$this->postVars('email');
		
		$sql="update `contactus_emails` set `ce_name`='$name', `ce_mails`='$email' where ce_id='$id'";
		$this->Execute($sql);
	}
	
	public function delEmail(){
		$id=$this->getVars('id');
		
		$sql="delete from `contactus_emails` where ce_id='$id'";
		$this->Execute($sql);
	}
	
	public function getEmails(){
	 $sql="select * from `contactus_emails`";
	 return $this->ExecuteS($sql);	
	}
	
	public function getEmailsToSend(){
	 $sql="select * from `contactus_emails`";
	 $row=$this->ExecuteS($sql);
	 foreach($row as $row){
	 	
		$emails.=$row["ce_mails"].",";
	 
	 }
	 
	 return $emails;	
	}
	
	public function getEmailById($id){
	 $sql="select * from `contactus_emails` where ce_id='$id'";
	 return $this->ExecuteS($sql);		
	}
  
  
 };
 
?>
