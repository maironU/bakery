<?php
 include('contactus/model/Contact.php');
 
 $obj = new Contact();
 $obj->connect();
 
 $id=$obj->getVars('id');
 $row=$obj->getContactById($id);
 
 if(count($row)>0){
  foreach($row as $row){
	 
	 $name=$row["name"];
	 $company=$row["company"];
	 $email=$row["email"];
	 $msg=$row["msg"]; 
	 $phone=$row["phone"]; 
	 $mob=$row["mob"];
	 $city=$row["city"];
	 $country=$row["country"];
	 $service=$row["service"];
	 $information=$row["information"];
	}	 
 }
 
?>

<div class="widget3">
 <div class="widgetlegend">Contactenos</div>
<p style="width:100">
	<a href="<?php $_SERVER['PHP_SELF'];?>?p=contactus/view/index.php" class="btn_normal" style="float:left; margin:5px;">Volver </a>
</p>
 
<table width="803" border="0">
  <tr>
    <td width="206"><strong>Nombre:</strong></td>
    <td width="587"><?php echo $name; ?></td>
  </tr>
  <tr>
    <td><strong>Empresa:</strong></td>
    <td><?php echo $company; ?></td>
  </tr>
  <tr>
    <td><strong>Email:</strong></td>
    <td><?php echo $email; ?></td>
  </tr>
  <tr>
    <td><strong>Mensaje:</strong></td>
    <td><?php echo $msg; ?></td>
  </tr>
  <tr>
    <td><strong>Telefono:</strong></td>
    <td><?php echo $phone; ?></td>
  </tr>
  <tr>
    <td><strong>Movil:</strong></td>
    <td><?php echo $mob; ?></td>
  </tr>
  <tr>
    <td><strong>Ciudad:</strong></td>
    <td><?php echo $city; ?></td>
  </tr>
  <tr>
    <td><strong>Pais:</strong></td>
    <td><?php echo $country; ?></td>
  </tr>
  <tr>
    <td><strong>Servicio:</strong></td>
    <td><?php echo $service; ?></td>
  </tr>
  <tr>
    <td><strong>Informaci&oacute;n:</strong></td>
    <td><a href="contactus/contactFile/<?php echo $information; ?>" target="_blank" class="btn_2">Descargar</a></td>
  </tr>
</table>

</div>
