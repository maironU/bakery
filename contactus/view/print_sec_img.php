<?php

/*******************************************************************************

*  Title: Helpdesk software Hesk

*  Version: 0.94.1 @ October 25, 2007

*  Author: Klemen Stirn

*  Website: http://www.phpjunkyard.com

********************************************************************************

*  COPYRIGHT NOTICE

*  Copyright 2005-2007 Klemen Stirn. All Rights Reserved.

*

*  This script may be used and modified free of charge by anyone

*  AS LONG AS COPYRIGHT NOTICES AND ALL THE COMMENTS REMAIN INTACT.

*  By using this code you agree to indemnify Klemen Stirn from any

*  liability that might arise from it's use.

*

*  Selling the code for this program, in part or full, without prior

*  written consent is expressly forbidden.

*

*  Obtain permission before redistributing this software over the Internet

*  or in any other medium. In all cases copyright and header must remain

*  intact. This Copyright is in full effect in any country that has

*  International Trade Agreements with the United States of America or

*  with the European Union.

*

*  Removing any of the copyright notices without purchasing a license

*  is illegal! To remove PHPJunkyard copyright notice you must purchase a

*  license for this script. For more information on how to obtain a license

*  please visit the site below:

*  http://www.phpjunkyard.com/copyright-removal.php

*******************************************************************************/



$numsec=$_GET['numsec'];



/* This will make sure the security image is not cahced */

        header("expires: -1");

        header("cache-control: no-cache, no-store, must-revalidate, max-age=-1");

        header("cache-control: post-check=0, pre-check=0", false);

        header("pragma: no-store,no-cache");







include('secimg.inc.php');





$sc=new PJ_SecurityImage($numsec);

$sc->printImage($numsec);



exit();

?>

