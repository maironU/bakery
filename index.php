<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head><meta http-equiv="Content-Type" content="text/html; charset=utf-8">

<title>BSS - Admin ...</title>
<link href="css/scsStyle.css" rel="stylesheet" type="text/css" />
<link href="css/smoothness/jquery-ui-1.9.2.custom.css" rel="stylesheet">
<link href="css/font-awesome.css" rel="stylesheet" type="text/css" />
<script src="js/jquery-1.8.3.js"></script>
<script src="js/jquery-ui-1.9.2.custom.js"></script>
<link href="https://cdn.jsdelivr.net/npm/bootstrap@5.1.3/dist/css/bootstrap.min.css" rel="stylesheet" integrity="sha384-1BmE4kWBq78iYhFldvKuhfTAU6auU8tT94WrHftjDbrCEXSU1oBoqyl2QvZ6jIW3" crossorigin="anonymous">
<script type="text/javascript" src="modules/sliders/js/sliders.js"></script>
<script type="text/javascript" src="usr/js/usr.js"></script>
<script type="text/javascript">
 $( "#dialog" ).dialog({
			autoOpen: true,
			width: 400,
			buttons: [
				{
					text: "Ok",
					click: function() {
						$( this ).dialog( "close" );
					}
				},
				{
					text: "Cancel",
					click: function() {
						$( this ).dialog( "close" );
					}
				}
			]
		});
</script>
<style>
 #dialog-link {
		padding: .4em 1em .4em 20px;
		text-decoration: none;
		position: relative;
	}
	#dialog-link span.ui-icon {
		margin: 0 5px 0 0;
		position: absolute;
		left: .2em;
		top: 50%;
		margin-top: -8px;
	}
.Estilo1 {color: #FFFFFF}
#bg {
  position: fixed; 
  top: 0; 
  left: 0; 

  /* Preserve aspet ratio */
  min-width: 100%;
  min-height: 100%;
  
   filter: brightness(25%);

}

.content-login {
	background: #2e3441;
	background-image: -webkit-radial-gradient(top, circle cover, #4e7a89, #2e3441 80%);
	display: flex;
	justify-content: center;
	align-items: center;
}

.signin {
    display: block;
    position: relative;
    min-width: 330px !important;
	margin: 0 auto;
    padding: 20px;
    background-color: rgba(0,0,0,0.1);
    -webkit-border-radius: 5px;
    -moz-border-radius: 5px;
    border-radius: 5px;
    -webkit-box-shadow: inset 1px 1px 0 0 rgb(255 255 255 / 20%), inset -1px -1px 0 0 rgb(0 0 0 / 20%);
    -moz-box-shadow: inset 1px 1px 0 0 rgba(255,255,255,0.2), inset -1px -1px 0 0 rgba(0,0,0,0.2);
    box-shadow: inset 1px 1px 0 0 rgb(255 255 255 / 20%), inset -1px -1px 0 0 rgb(0 0 0 / 20%);
}

.signin input[type="button"] {
    -webkit-appearance: none;
    height: 40px;
    padding: 10px 12px;
    margin-bottom: 10px;
    background-color: #538a9a;
    text-transform: uppercase;
    color: #fff;
    border: 0px;
	float: right;
	font-size: 13px;
    margin: 0;
    outline: none;
    -webkit-border-radius: 5px;
    -moz-border-radius: 5px;
    border-radius: 5px;
}

.icon-form {
	color: #538a9a;
	opacity: 0.4;
	position: absolute;
	top: 12px;
	left: 7px;
}

.icon-form-dark {
	opacity: 1;
}

.input-form {
	padding: .5rem .75rem .5rem 1.8rem;
}

@media (max-width: 992px){
    .signin{
        min-width: 40% !important;
    }
}

</style>
</head>

<body>
    <div id="back" style="z-index: -500; min-width: 100%; min-height: 100%;">
        <div id="sliders"></div>
    </div>

<!-- ui-dialog -->

<!--<div class="ui-overlay"><div class="ui-widget-overlay"></div>-->
<!--<div class="ui-widget-shadow ui-corner-all" style="width: 52%; height: 64%; position: absolute; left: 20%; top: 20%;"></div>-->
</div>
	<div class="content-login" style="width:100vw; height: 100vh;" class="d-flex justify-content-center align-items-center">
		<div class="signin">
			<div class="ui-dialog-content ui-widget-content" style="background: none; border: 0;">
					<p>&nbsp;</p>
					<form id="form1" name="form1" method="post" action="#">
						<div width="100%" class="mb-5">
							<p align="center"><img src="images/logo_new.png" width="250px"/></p>
						</div>
						<div width="100%" id="login">
								<div class="ui-widget" id="errmsg" style="display:none">
									<div class="ui-state-error ui-corner-all" style="padding: 0 .7em;">
										<p><span class="ui-icon ui-icon-alert" style="float: left; margin-right: .3em;"></span> <strong>Error:</strong> Usuario o contrase&ntilde;a incorrectos.</p>
									</div>
								</div>

								<div>
									<div class="mb-2 mt-2 position-relative">
										<input type="text" class="form-control input-form" name="user" type="text" id="user" placeholder="Usuario" required autofocus>
										<i class="fa fa-user icon-form icon-form-dark"></i>
									</div>
									<div class="mb-2 position-relative">
										<input type="password" class="form-control input-form" name="pass" id="pass" placeholder="Contrase&ntilde;a" required>
										<i class="fa fa-lock icon-form"></i>
									</div>
									<div class="d-flex justify-content-end w-100">
										<input type="button" onClick="validate();"  value="Entrar" class="btn btn-dark"/>
									</div>
								</div>

								<!--<p><input name="user" type="text" id="user" placeholder="Usuario" required /></p>
								<p><input name="pass" type="password" id="pass" placeholder="Contrase&ntilde;a" onKeyPress="return valEnter(event)" required/></p>
								<p align="center"><input type="button" onClick="validate();"  value="Entrar" class="btn_submit" /></p>-->
								<!--<p align="center"><a href="forgot.php" style="color:#000">Olvide mi contrase&ntilde;a</a></p>-->
						</div>
						<div width="100%" id="loading" style="display:none">
							<p align="center"><img src="images/loading.gif" width="80" height="80" /></p>
						</div>
					
				</form>
					
			</div>
		</div>
	</div>
<!--
	<div style="width:100vw; height: 100vh;" class="d-flex justify-content-center align-items-center">
		<div style="width: 50%;left: 20%; top: 20%; padding: 20px;box-shadow: 1px 2px #f5f5f5; max-width:420px; min-width:350px" class="ui-widget ui-widget-content ui-corner-all">
		<div class="ui-dialog-content ui-widget-content" style="background: none; border: 0;">
				<p>&nbsp;</p>
				<form id="form1" name="form1" method="post" action="#">
					<div width="100%">
						<p align="center"><img src="images/logo.png" width="250px"/></p>
					</div>
					<div width="100%" id="login">
						
							<div class="ui-widget" id="errmsg" style="display:none">
								<div class="ui-state-error ui-corner-all" style="padding: 0 .7em;">
									<p><span class="ui-icon ui-icon-alert" style="float: left; margin-right: .3em;"></span> <strong>Error:</strong> Usuario o contrase&ntilde;a incorrectos.</p>
								</div>
							</div>

							<div>
								<div class="mb-2 mt-2">
									<input type="text" class="form-control" name="user" type="text" id="user" placeholder="Usuario" required>
								</div>
								<div class="mb-2">
									<input type="password" class="form-control" name="pass" id="pass" placeholder="Contrase&ntilde;a" required>
								</div>
								<div class="d-flex justify-content-center w-100">
									<input type="button" onClick="validate();"  value="Entrar" class="btn btn-dark"/>
								</div>
							</div>

							<!--<p><input name="user" type="text" id="user" placeholder="Usuario" required /></p>
							<p><input name="pass" type="password" id="pass" placeholder="Contrase&ntilde;a" onKeyPress="return valEnter(event)" required/></p>
							<p align="center"><input type="button" onClick="validate();"  value="Entrar" class="btn_submit" /></p>-->
							<!--<p align="center"><a href="forgot.php" style="color:#000">Olvide mi contrase&ntilde;a</a></p>-->
					<!--</div>
					<div width="100%" id="loading" style="display:none">
						<p align="center"><img src="images/loading.gif" width="80" height="80" /></p>
					</div>
				
			</form>
				
		</div>
		</div>
	</div>-->
	<script>
	        
		  $("document").ready(function(){
getSlider();
			  $('#back #sliders').cycle({ 
			      pause: 1,
			      speed: 2000
			  });
		  });

		if(!$("input:text[name=user]").is(":focus")){
			$("input:text[name=user]").siblings('i').removeClass('icon-form-dark')
		}

		$("input").blur(function(){
			$(this).siblings('i').removeClass('icon-form-dark')
		});

		$("input").focus(function(){
			$(this).siblings('i').addClass('icon-form-dark')
		});

	</script>
</body>
</html>
