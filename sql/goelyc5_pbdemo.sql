-- phpMyAdmin SQL Dump
-- version 4.0.10.14
-- http://www.phpmyadmin.net
--
-- Servidor: localhost:3306
-- Tiempo de generación: 29-07-2017 a las 08:12:34
-- Versión del servidor: 5.6.34-log
-- Versión de PHP: 5.6.20

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;

--
-- Base de datos: `goelyc5_pbdemo`
--

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `configuration`
--

CREATE TABLE IF NOT EXISTS `configuration` (
  `conf_id` int(11) NOT NULL AUTO_INCREMENT,
  `conf_name` varchar(255) NOT NULL,
  `conf_value` varchar(255) NOT NULL,
  `conf_zone` varchar(255) NOT NULL,
  `conf_frontname` varchar(255) NOT NULL,
  PRIMARY KEY (`conf_id`)
) ENGINE=MyISAM  DEFAULT CHARSET=latin1 AUTO_INCREMENT=7 ;

--
-- Volcado de datos para la tabla `configuration`
--

INSERT INTO `configuration` (`conf_id`, `conf_name`, `conf_value`, `conf_zone`, `conf_frontname`) VALUES
(1, 'EMAIL_HOST', 'mail.goely.co', 'email', 'Host'),
(2, 'EMAIL_AUTH', 'true', 'email', 'Autenticacion'),
(3, 'EMAIL_SECURE', 'ssl', 'email', 'Seguridad'),
(4, 'EMAIL_PORT', '465', 'email', 'Puerto'),
(5, 'EMAIL_USER', 'info@goely.co', 'email', 'Email'),
(6, 'EMAIL_PASS', '123Info**', 'email', 'Contraseña');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `contactus`
--

CREATE TABLE IF NOT EXISTS `contactus` (
  `id_contact` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(255) NOT NULL,
  `company` varchar(255) NOT NULL,
  `email` varchar(255) NOT NULL,
  `msg` longtext NOT NULL,
  `phone` varchar(255) NOT NULL,
  `mob` varchar(255) NOT NULL,
  `city` varchar(255) NOT NULL,
  `country` varchar(255) NOT NULL,
  `service` varchar(255) NOT NULL,
  `information` longtext NOT NULL,
  PRIMARY KEY (`id_contact`)
) ENGINE=MyISAM  DEFAULT CHARSET=latin1 AUTO_INCREMENT=255 ;

--
-- Volcado de datos para la tabla `contactus`
--

INSERT INTO `contactus` (`id_contact`, `name`, `company`, `email`, `msg`, `phone`, `mob`, `city`, `country`, `service`, `information`) VALUES
(2, 'VIVIAN VILLAMIL SALAMANCA', 'cundinamarca', 'vivimar3@hotmail.com', '', '3142155600', '3142155600', '', '', 'Entrenador', ''),
(3, 'SANTIAGO ORTEGA RAMIREZ', 'ESCUELA LIGA', 'oaldemarp99@yahoo.com', '', '3116526610', '3115669053', '', '', 'Deportista', ''),
(4, 'yamid omar soto lozano', 'atlantico', 'yamtdk@hotmail.com', '', '310459310', '3104593110', '', '', 'Entrenador', ''),
(5, 'ANDRES FELIPE JARAMILLO RESTREPO', 'Antioquia', 'andres_psicologia@hotmail.com', '', '3103949655', '3103949655', '', '', 'Arbitro', ''),
(6, 'Nelson Javier Villacorte Rubiano', 'Club de taekwondo Bushido / Liga de taekwondo de Bogota', 'javiervillacorte@yahoo.com', '', '3155274460', '3155274460', '', '', 'Entrenador', ''),
(7, 'Raul Bello Mendivelso ', 'BOGOTA', 'raubello@hotmail.com', '', '3922253', '3162561131', '', '', 'Entrenador', ''),
(8, 'EDER VERGARA LAMBRAÃ‘O', 'LIGA DE TAEKWONDO DE BOLIVAR', 'maricami2@hotmail.com', '', '3145268684', '3145268684', '', '', 'Entrenador', ''),
(9, 'RAUL GOMEZ RAMOS', 'LIGA DE TAEKWONDO DE BOLIVAR', 'ligadetaekwondodebolivar@hotmail.com', '', '3004049873', '3004049873', '', '', 'Entrenador', ''),
(10, 'wilson leon loaiza', 'liga de taekwondo vaupes', 'leonintheshadow@gmail.com', '', '0', '3125608349', '', '', 'Entrenador', ''),
(11, 'AndrÃ©s Saeed Camilo Vanegas LÃ³pez ', 'BodyMind / liga de taekwondo de BogotÃ¡ ', 'Camiloedufisico@hotmail.com', '', '4037861', '3013168635', '', '', 'Entrenador', ''),
(12, 'laura isabel yepes cubillos', 'bogota', 'liyc3@hotmail.com', '', '3134207431', '', '', '', 'Entrenador', ''),
(13, 'MARIA ALEJANDRA MENDOZA MENDOZA', 'LIGA DE CORDOBA', 'mayuyis0113394@gmail.com', '', '3113624550', '3113624550', '', '', 'Entrenador', ''),
(14, 'GUSTAVO JOSE MARTINEZ OVIEDO', 'LIGA DE CORDOBA', 'GUSTMARTKD@HOTMAIL.COM', '', '3215945579', '3215945579', '', '', 'Entrenador', ''),
(15, 'JORGE LUIS PACHECO HOYOS', 'LIGA DE CORDOBA', 'JORPACH.CORDOBA@GMAIL.COM', '', '3126612965', '3126612965', '', '', 'Arbitro', ''),
(16, 'GUILLERMO PALACIOS AVILA', 'Han Kook / Liga de BogotÃ¡', 'gpalaciosavila@hotmail.com', '', ' fijo 3425559', '3135349340', '', '', 'Arbitro', ''),
(17, 'Karen Lisseth Monsalve Mercado', 'Antioquia', 'karenlissethm@gmail.com', '', '(4) 5868445', '3017328719', '', '', 'Arbitro', ''),
(18, 'JOSE RODRIGO NOCOVE DIAZ', 'Liga de Bogota', 'nocovetkd@hotmail.com', '', '2556491', '3158516958', '', '', 'Delegado', ''),
(19, 'KEVYN ALEXANDER DUARTE CARRILLO', 'SANTANDER', 'kduarte707@unab.edu.co', '', '6416330', '3134602715', '', '', 'Entrenador', ''),
(20, 'JORGE RODRIGUEZ MARTINEZ', 'Club Deportivo de Taekwondo JANSU - Soacha', 'jansutkd@hotmail.com', '', '3006013666', '3005635552', '', '', 'Entrenador', ''),
(21, 'JULIAN ALBERTO ROJAS RIVERA', 'HANKOOK-CAQUETA', 'olympicus2004@hotmail.com', '', '3112298466', '3112298466', '', '', 'Entrenador', ''),
(22, 'IGNACIO ROJAS RIVERA', 'Universidad de la Amazonia - CAQUETA', 'ignaciorojas84@outlook.com', '', '3212895633', '3212895633', '', '', 'Entrenador', ''),
(23, 'MÃ³nica Urrego Montoya', 'Antioquia', 'monicatkd_cane@yahoo.es', '', '3005998088', '3005998088', '', '', 'Entrenador', ''),
(24, 'Clara SofÃ­a BohÃ³rquez Toro', 'Antioquia', 'monicatkd_cane@yahoo.es', '', '3005998088', '3005998088', '', '', 'Deportista', ''),
(25, 'Alejandra Mosquera Grisales', 'Antioquia', 'monicatkd_cane@yahoo.es', '', '3005998088', '3005998088', '', '', 'Deportista', ''),
(26, 'Paulo Mosquea Grisales', 'Antioquia', 'monicatkd_cane@yahoo.es', '', '3005998088', '3005998088', '', '', 'Deportista', ''),
(27, 'Stephany Daza Diosa', 'Antioquia', 'monicatkd_cane@yahoo.es', '', '3005998088', '3005998088', '', '', 'Deportista', ''),
(28, 'Sebastian Pemberty', 'Antioquia', 'monicatkd_cane@yahoo.es', '', '3005998088', '3005998088', '', '', 'Deportista', ''),
(29, 'Marizoneth Carrillo TeherÃ¡n', 'Antioquia', 'monicatkd_cane@yahoo.es', '', '3005998088', '3005998088', '', '', 'Deportista', ''),
(30, 'CARLOS ALBERTO SUAREZ BERNAL', 'LIGA DE TAEKWONDO DEL QUINDIO', 'carlosalbertosuareztkd@gmail.com', '', '000000000000', '311 719 N7517', '', '', 'Entrenador', ''),
(31, 'OMAR DE JESUS RODRIGUEZ OBREDOR', 'MAGDALENA', 'clublora@hotmail.com', '', '4303185', '3006369741', '', '', 'Entrenador', ''),
(32, 'GINO ALONSO SALVEDO GOMEZ', 'BOGOTA ', 'ginosalcedo2003@yahoo.es', '', '3114700516', '3114700516', '', '', 'Entrenador', ''),
(33, 'JAVIER ALEJANDRO CAMARGO URREA', 'Fuerzas Armadas (FF.AA)', 'javineo22@hotmail.com', '', '3132525801', '3132525801', '', '', 'Arbitro', ''),
(34, 'DENNIS YERALDIN BARRERO SANCHEZ', 'Fuerzas Armadas (FF.AA)', 'chea1706@hotmail.com', '', '3118337273', '3118337273', '', '', 'Entrenador', ''),
(35, 'Humberto Martinez Zapata', 'Antioquia', 'elprofetkd@gmail.com', '', '4384418', '3108482275', '', '', 'Entrenador', ''),
(36, 'JUAN MANUEL CASAS SANCHEZ', 'Fuerzas Armadas (FF.AA)', 'juancasastkd@hotmail.com', '', '3013818810', '3013818810', '', '', 'Entrenador', ''),
(37, 'FREDY WILLIAM TOLA FLOREZ', 'Fuerzas Armadas (FF.AA)', 'fredytol43@hotmail.com', '', '3132641850', '3132641850', '', '', 'Arbitro', ''),
(38, 'JOSE JAVIER MUÃ‘OZ ALVARADO', 'Fuerzas Armadas (FF.AA)', 'suriyingbosa83335@hotmail.com', '', '3143948574', '3143948574', '', '', 'Entrenador', ''),
(39, 'NICOLAS ALEJANDRO CASTRO SALCEDO', 'Fuerzas Armadas (FF.AA)', 'casatrodt@gmail.com', '', '3114685430', '3114685430', '', '', 'Arbitro', ''),
(40, 'JULIO CESAR DUQUE CRUZ', 'Fuerzas Armadas (FF.AA)', 'koletkd@gmail.com', '', '3142964263', '3142964263', '', '', 'Entrenador', ''),
(41, 'GEYMAR PABON TREJO', 'TEMPLARIOS DEL ORIENTE / META', 'daltiulf1@gmail.com', '', '3209194840', '3209194840', '', '', 'Entrenador', ''),
(42, 'ALBERT LEONARDO AGUDELO', 'HANSU / META', 'leonardotkd1980@gmail.com', '', '3224258366', '3224258366', '', '', 'Entrenador', ''),
(43, 'marlon yesid patiÃ±o marin', 'liga de boyaca', 'marlonyesid.patino@gmail.com', '', '7721252', '3166197661', '', '', 'Arbitro', ''),
(44, 'DORIS ESMID PATIÃ‘O MARIN', 'liga de boyaca', 'xenis01@hotmail.com', '', '7721252', '3228124939', '', '', 'Entrenador', ''),
(45, 'JAZMÃN JEREZ OLACHICA', 'Fuerzas Armadas (FF.AA)', 'koletkd@gmail.com', '', '3142964263', '3142964263', '', '', 'Entrenador', ''),
(46, 'ELIANA SIZA PACHON', 'Fuerzas Armadas (FF.AA)', 'koletkd@gmail.com', '', '3142964263', '3142964263', '', '', 'Arbitro', ''),
(47, 'DUBEIMAR MARTINEZ MARTINEZ', 'BUSHIDO', 'dubeimarmartinez@gmail.com', '', '4001920', '3015297513', '', 'Entrenadores', 'Entrenador', 'DUBEIMAR MARTINEZ MARTINEZ FOTO.jpg'),
(48, 'Nelson Javier Villacorte Rubiano', 'CLUB DE TAEKWONDO BUSHIDO / LIGA DE TAEKWONDO DE BOGOTA', 'javiervillacorte@yahoo.com', '', '3155274460', '3155274460', '', 'Entrenadores', 'Entrenador', 'IMG_20170206_120346715.jpg'),
(49, 'MARTHA ESPERANZA CUBIDES AMAYA ', 'CLUB DE TAEKWONDO CISSCA', 'di_manri@hotmail.com', '', '2462861', '3107762017', '', 'Entrenadores', 'Entrenador', 'MARTICA .jpg'),
(50, 'RICARDO ADOLFO ÃLVAREZ GACHARNÃ', 'CLUB DE TAEKWONDO CISSCA', 'riaal007@hotmail.com', '', '2462861', '3106992017', '', 'Entrenadores', 'Entrenador', 'Ricardo Adolofo Ãlvarez GacharnÃ¡ C.C.79572955 BtÃ¡.jpg'),
(51, 'MARTHA PAOLA ÃLVAREZ CUBIDES', 'CLUB DE TAEKWONDO CISSCA', 'paoal96@gamil.com', '', '2462861', '3103091485', '', 'Entrenadores', 'Delegado', 'Paola Ãlvarez.jpg'),
(52, 'MARTHA PAOLA ÃLVAREZ CUBIDES', 'CLUB DE TAEKWONDO CISSCA', 'paoal96@gamil.com', '', '2462861', '3103091485', '', 'Entrenadores', 'Delegado', 'Paola Ãlvarez.jpg'),
(53, 'Frank LondoÃ±o Tamayo', 'Panama', 'franklondono@hotmail.com', '', '50766823344', '50766823344', '', 'Entrenadores', 'Entrenador', 'FotoFrank.png'),
(54, 'SANTIAGO MARIN OSORIO', 'LIGA DE CALDAS', 'marintkd@live.com', '', '3017118389', '3017118389', '', 'Jueces', 'Arbitro', '16593610_977893985675287_1423462514_o.jpg'),
(55, 'luis felipe rondon agudelo', 'caldas', 'rondi17@gmail.com', '', '8771949', '3002045915', '', 'Entrenadores', 'Entrenador', '734984_386345601455896_432871000_n.jpg'),
(56, 'Jose David Duarte Silva', 'Liga santanderiana', 'david_duarte182@hotmail.com', '', '3005004253', '3005004253', '', 'Entrenadores', 'Deportista', '20150805_155537.jpg'),
(57, 'MAURICIO ARCINIEGAS CRUZ', 'UNIVERSIDAD ECCI', 'profemaurotkd@hotmail.com', '', '3103082242', '3103082242', '', 'Entrenadores', 'Entrenador', 'FONDO AZUL.jpg'),
(58, 'alejandro pulgarin', 'caldas', 'alejandropulgarinrocha@yahoo.com', '', '3013920269', '3013920269', '', 'Entrenadores', 'Entrenador', '16558928_10158236699995204_61704863_n.jpg'),
(59, 'hedel quiridense galvis', 'caldas', 'hedel.qgq@gmail.com', '', '3107108602', '3107108602', '', 'Entrenadores', 'Delegado', '16683540_1254076924682755_890967162_n.jpg'),
(60, 'Humberto Martinez Zapata', 'Antioquia', 'elprofetkd@gmail.com|', '', '4384418', '3108482275', '', 'Entrenadores', 'Entrenador', 'FOTO.png'),
(61, 'EDISON HELI BECERRA MEJIA ', 'Bogota', 'ebecerrasura@outlook.com', '', '3138460818', '3138460818', '', 'Entrenadores', 'Entrenador', 'FOTO HOJA DE VIDA EDISON BECERRA.jpg'),
(62, 'EDISON HELI BECERRA MEJIA ', 'Bogota', 'ebecerrasura@outlook.com', '', '3138460818', '3138460818', '', 'Entrenadores', 'Entrenador', 'FOTO HOJA DE VIDA EDISON BECERRA.jpg'),
(63, 'CATALINA PAOLA TORRES BARRETO', 'UNION TONG IL - BOGOTÃ', 'paotb85@gmail.com', '', '2918068', '3004835918', '', 'Entrenadores', 'Entrenador', 'CATALINA TORRES.jpg'),
(64, 'Juan Manuel Marin', 'Antioquia', 'monicatkd_cane@yahoo.es', '', '4265723', '3005998088', '', 'Entrenadores', 'Deportista', 'IMG-20170206-WA0012-1.jpg'),
(65, 'VICTOR ADRIAN CASTAÃ‘EDA BUITRAGO ', 'UNION TONG IL - BOGOTÃ', 'castabu@gmail.com', '', '3152517527', '3152517527', '', 'Entrenadores', 'Entrenador', 'VICTOR CASTAÃ‘EDA.jpg'),
(66, 'AndrÃ©s Saeed Camilo Vanegas LÃ³pez ', 'BodyMind / liga de taekwondo de BogotÃ¡ ', 'Camiloedufisico@hotmail.com', '', '4037861', '3013168635', '', 'Entrenadores', 'Entrenador', 'IMG_20161119_101944.JPG'),
(67, 'EUDALDO AGUILAR LINARES', 'SHI JIN YU LIGA DE BOGOTA', 'SHIJINYU9@HOTMAIL.COM', '', '3202346860', '3202346860', '', 'Entrenadores', 'Entrenador', 'FOTO EUDALDO AGUILAR (1).jpg'),
(68, 'RAFAEL ESTEBAN FLOREZ BARON', 'BOGOTA', 'RAFAEL.E.FLOREZ@GMAIL.COM', '', '5261563', '3132260349', '', 'Jueces', 'Arbitro', 'RAFAEL FLOREZ.jpg'),
(69, 'Yulie andrea jimenez peÃ±a', 'Liga de Bogota', 'Yuli.jimenez_2007@hotmail.com', '', '3154826651', '3154826651', '', 'Jueces', 'Arbitro', 'FB_IMG_1479686198302.jpg'),
(70, 'CESAR ENRIQUE GONZALEZ ARDILA', 'Han Kook / Cundinamarca', 'ingcesargonzalez@gmail.com', '', '2600819', '3153572466', '', 'Jueces', 'Arbitro', 'CESARG.jpg'),
(71, 'Juan Carlos Espinosa Callejas', 'Shaya han Melgar Tolima', 'juancarlostkd476@hotmail.com', '', '3213005144', '3213005144', '', 'Jueces', 'Entrenador', 'FB_IMG_1486513881874.jpg'),
(72, 'Juan Carlos Espinosa Callejas', 'Shaya han Melgar Tolima', 'juancarlostkd476@hotmail.com', '', '3213005144', '3213005144', '', 'Jueces', 'Entrenador', 'FB_IMG_1486513881874.jpg'),
(73, 'Juan Carlos Espinosa Callejas', 'Shaya han Melgar Tolima', 'juancarlostkd476@hotmail.com', '', '3213005144', '3213005144', '', 'Jueces', 'Entrenador', 'FB_IMG_1486513881874.jpg'),
(74, 'NINI JOHANNA PALACIO ZEA', 'SAMGUK/BOGOTA', 'johannapalacio@gmail.com', '', '17582349', '3013675454', '', 'Jueces', 'Arbitro', 'SSA40525 A.jpg'),
(75, 'Karen Lisseth Monsalve Mercado ', 'Antioquia', 'karenlissethm@gmail.com', '', '5868445', '3017328719', '', 'Jueces', 'Arbitro', 'WP_20170207_19_35_09_Pro (2).jpg'),
(76, 'DAYANA STEPHANIA SALCEDO YAMA', 'NARIÃ‘O', 'dayis-ssy@hotmail.com', '', '3186030426', '3186030426', '', 'Jueces', 'Arbitro', 'Dayana.jpg'),
(77, 'BRIAN NAVID MUÃ‘OZ MONTENEGRO', 'NARIÃ‘O', 'brayan2105@hotmail.es', '', '7734159', '3182330534', '', 'Jueces', 'Arbitro', '1085939348.jpg'),
(78, 'OCTAVIO AUGUSTO FAJARDO RODRIGUEZ', 'SHI JIN YU / BOGOTA', 'octavioprofe@gmail.com', '', '4741416', '3103086251', '', 'Entrenadores', 'Entrenador', '20170207_194202[1].jpg'),
(79, 'Gabriel GonzÃ¡lez GuarÃ­n', 'Budohai  \\ liga de BogotÃ¡', 'gagogua@yahoo.es', '', '7489361', '3193375700', '', 'Entrenadores', 'Entrenador', 'IMG-20170207-WA0017.jpg'),
(80, 'EUGENIO SANTOS VILLARREAL GONZALES', 'Club Aguila - Liga tkd del Atlantico', 'villarrealeugenio@hotmail.com', '', '3728506', '3114090822', '', 'Jueces', 'Arbitro', 'Villarreal 001.jpg'),
(81, 'EUGENIO SANTOS VILLARREAL GONZALES', 'Club Aguila - Liga tkd del Atlantico', 'villarrealeugenio@hotmail.com', '', '3728506', '3114090822', '', 'Jueces', 'Arbitro', 'Villarreal 001.jpg'),
(82, 'WILSON PEÃ‘ARANDA BONILLA', 'NORTE DE SANTANDER', 'wilsonpb39@gmail.com', '', '3165750734', '3165750734', '', 'Jueces', 'Arbitro', 'FOTOGRAFIA.jpg'),
(83, 'CAROL MARCELA SALCEDO YAMA', 'NARIÃ‘O', 'carolsalcedo96@gmail.com', '', '3158035453', '3158035453', '', 'Jueces', 'Arbitro', 'Caritol.jpg'),
(84, 'MARCO FIDEL CHAVES DAVILA', 'Liga de Taekwondo de NariÃ±o', 'marcochaves8@gmail.com', '', '7360501', '3178801141', '', 'Jueces', 'Arbitro', 'fotografia.pdf'),
(85, 'Elkin Guillermo GÃ³mez PÃ©rez ', 'Ã‰lite/BogotÃ¡', 'egomezp73@gmail.com ', '', '7357515', '3165512286', '', 'Entrenadores', 'Entrenador', '2014-12-16 16.10.36.jpg'),
(86, 'NicolÃ¡s Alejandro Castro Salcedo', 'Fuerzas Armadas', 'casatrodt@gmail.com', '', '7479207', '3114685430', '', 'Jueces', 'Arbitro', 'IMG_20150510_192236.jpg'),
(87, 'DIEGO JUAN VICENTE BENAVIDES ROCHA', 'ZEN TAEKWONDO CLUB / LIGA DE TAEKWONDO DE BOGOTÃ', 'candrear@yahoo.com', '', '3166204663', '', '', 'Entrenadores', 'Deportista', 'DiegoJuanVicenteBenavidesRocha.png'),
(88, 'CLAUDIA ANDREA ROCHA LAVERDE', 'ZEN TAEKWONDO CLUB / LIGA DE TAEKWONDO DE BOGOTÃ', 'candrear@yahoo.com', '', '3166204663', '', '', 'Entrenadores', 'Deportista', 'CLAUDIAANDREAROCHALAVERDE.jpg'),
(89, 'HENRY OLRIED MORA CASTIBLANCO', 'CLUB FREEMEN / LIGA DE BOGOTA', 'tkdfreemen@hotmail.com', '', '403 67 48', '311 209 05 99', '', 'Entrenadores', 'Entrenador', 'FOTO HENRY.jpg'),
(90, 'Luis Fernando Garcia Romero', 'VALLE', 'fegartkd@hotmail.com', '', '3175126724', '3175126724', '', 'Jueces', 'Arbitro', 'img002.jpg'),
(91, 'MANUEL ARTURO ROMERO MENDOZA', 'LIGA TOLIMENSE DE TAEKWONDO', 'taekwondomelgar@gmail.com', '', '(8)2450898', '3123212956', '', 'Entrenadores', 'Entrenador', 'MANUEL ROMERO.jpg'),
(92, 'GEYMAR PABON TREJO', 'META', 'daltiulf1@gmail.com', '', '3209194840', '3209194840', '', 'Entrenadores', 'Entrenador', '6. GEYMAR PABON TREJO.jpg'),
(93, 'Fabio Hernando Bustos Alarcon', 'Club freemen Bogota', 'Pintuarte73@hotmail.com', '', '6020722', '3134680960', '', 'Entrenadores', 'Entrenador', 'IMG-20170207-WA0004.jpg'),
(94, 'Jessica Estrada Restrepo', 'Antioquia', 'jaestradr@gmail.com', '', '3113108003', '3113108003', '', 'Jueces', 'Arbitro', '1017133137.jpg'),
(95, 'Andres Alfonso Ferro', 'freemen', 'marcialtkd@hotmail.com', '', '8063905', '3178368625', '', 'Entrenadores', 'Entrenador', 'ANDRES ALFONSO.jpg'),
(96, 'Jorge Armando Chavez DÃ­az ', 'CUNDINAMARCA ', 'Conico01@hotmail.com', '', '3138677358', '3138677358', '', 'Entrenadores', 'Entrenador', 'image.jpeg'),
(97, 'LIDA YULIE VELASQUEZ SIERRA', 'CUNDINAMARCA', 'lyvelasquez22@yahoo.es', '', '(1) 8255837', '3167361080', '', 'Jueces', 'Arbitro', 'Lida VelÃ¡squez.jpg'),
(98, 'Jorge leonardo castro arana', 'Tolima', 'jlcastro00@misena.edu.co', '', '3102777594', '', '', 'Jueces', 'Arbitro', 'villeta.jpg'),
(99, 'CARLOS ALBERTO VALENCIA CAMBINDO', 'BOGOTS', 'cavaca8@hotmail.com', '', '3143198748', '3143198748', '', 'Jueces', 'Arbitro', 'CARLOS V.docx'),
(100, 'HERLEY MARTINEZ GUERRERO', 'BUSHIDO / BOGOTA', 'herley625@gmail.com', '', '4001920', '3008986929', '', 'Entrenadores', 'Entrenador', 'FOTO HERLEY MARTINEZ GUERRERO.jpg'),
(101, 'cesar Mauricio QuiÃ±onez murillo ', 'santanderana', 'cesarmauricio40@hotmail.com', '', '3105628308', '3105628308', '', 'Jueces', 'Arbitro', 'foto mao.jpg'),
(102, 'Adelmo Betancourt Lara', 'Cundinammarca', 'adelmo_tkd@yahoo.com', '', '8280294', '3167422858', '', 'Jueces', 'Arbitro', 'Ad.jpg'),
(103, 'WILSON MARTINEZ PEÃ‘A', 'BUSHIDO / BOGOTA', 'omega824@gmail.com', '', '4001920', '3178325336', '', 'Entrenadores', 'Delegado', 'FOTO WILSON MARTINEZ.jpg'),
(104, 'ANDREA KATERIN MARTINEZ MARTINEZ', 'BUSHIDO / BOGOTA', 'andrea_95kate@hotmail.com', '', '4001920', '3103037151', '', 'Entrenadores', 'Entrenador', 'FOTO ANDREA MARTINEZ.jpg'),
(105, 'santiago restrepo usma ', 'club de taekwondo ITM', 'sant9721@gmail.com', '', '2383432', '3207832323', '', 'Entrenadores', 'Deportista', 'pp.jpg'),
(106, 'JOSE ALIRIO SANCHEZ GIL', 'ARAUCA', 'aliriosanexito1@hotmail.es', '', '3132108865', '3132108865', '', 'Entrenadores', 'Arbitro', 'FOTO ALIRIO.png'),
(107, 'John Jairo Luengas CastaÃ±o', 'Antioquia', 'johnjluengas@hotmail.com', '', '4463179', '3122152175', '', 'Jueces', 'Entrenador', 'john. jpeg.jpg'),
(108, 'ALBERT LEONARDO AGUDELO', 'META', 'leonardotkd1980@gmail.com', '', '3224258366', '3224258366', '', 'Entrenadores', 'Entrenador', 'ALBERT AGUDELO.png'),
(109, 'IVAN MORENO NOVA', 'META', 'ivanmorenonova@outlook.com', '', '3125673366', '3125673366', '', 'Entrenadores', 'Entrenador', 'IVAN MORENO NOVA.jpg'),
(110, 'GABRIEL SANTA IBAÃ‘EZ', 'META', 'templariostkd@hotmail.com', '', '3202008084', '3202008084', '', 'Entrenadores', 'Entrenador', 'GABRIEL SANTA IBAÃ‘EZ.jpeg'),
(111, 'Orlando Esparza Murillo', 'Club Sang-Yong/ Liga Santandereana de Taekwondo', 'orlandoesparzam@hotmail.com', '', '3183888125', '3183888125', '', 'Entrenadores', 'Entrenador', 'Logo (1).gif'),
(112, 'Daniel Isidro Caballero Jaimes', 'Club Sang-Yong/ Liga Santandereana de Taekwondo', 'danielisidroj@hotmail.com', '', '3115284987', '3115284987', '', 'Entrenadores', 'Entrenador', 'Logo.gif'),
(113, 'Yeison Hernando Hernandez Tovar', 'Club la academia/ Liga Santandereana de taekwondo', 'yeisonh@msn.com', '', '3166946212', '3166946212', '', 'Entrenadores', 'Deportista', 'Logo (1).gif'),
(114, 'Jose Leonardo Osorio Bravo', 'Club Piedecuesta / Liga Santandereana de Taekwondo', 'leotaek12@hotmail.com', '', '3134751722', '3134751722', '', 'Entrenadores', 'Entrenador', 'Logo (1).gif'),
(115, 'Silvia Juliana PeÃ±a Sandoval', 'Club Piedecuesta / Liga Santandereana de Taekwondo', 'silvis_15@hotmail.com', '', '3178569552', '3178569552', '', 'Entrenadores', 'Deportista', 'Logo (1).gif'),
(116, 'Leonardo Enrique Garcia Rubio', 'Club Miramar / Liga Santandereana de Taekwondo', 'leg79@hotmail.com', '', '3106250552', '3106250552', '', 'Entrenadores', 'Entrenador', 'Logo (1).gif'),
(117, 'Reinaldo Alberto Orejarena Torres', 'Club la academia/ Liga Santandereana de taekwondo', 'academiakwando@gmail.com', '', '3102135195', '3102135195', '', 'Entrenadores', 'Entrenador', 'Logo (1).gif'),
(118, 'Mateo Felipe Prieto Cabrales', 'Club Academia Kwando / Liga Santandereana de Taekwondo', 'academiakwando@gmail.com', '', '3102135195', '3102135195', '', 'Entrenadores', 'Deportista', 'Logo (1).gif'),
(119, 'Maria Paula Correa Ramires ', 'Club Academia Kwando / Liga Santandereana de Taekwondo', 'academiakwando@gmail.com', '', '3102135195', '3102135195', '', 'Entrenadores', 'Deportista', 'Logo (1).gif'),
(120, 'Sebastian Camilo Gonzalez Reamirez ', 'Club Academia Kwando / Liga Santandereana de Taekwondo', 'academiakwando@gmail.com', '', '3102135195', '3102135195', '', 'Entrenadores', 'Deportista', 'Logo.gif'),
(121, 'Henry Mauricio Bohorquez Ortega ', 'Club Evolution / Liga Santandereana de Taekwondo', 'yesid_ortegatkd@hotmail.com', '', '3183126005', '3183126005', '', 'Entrenadores', 'Deportista', 'Logo.gif'),
(122, 'Yesid Ortega Parra ', 'Club Evolution / Liga Santandereana de Taekwondo', 'yesid_ortegatkd@hotmail.com', '', '3183126005', '3183126005', '', 'Entrenadores', 'Entrenador', 'Logo.gif'),
(123, 'Kevin Alexander Duarte Carrillo', 'Club Colegio Aurelio Martinez / Liga Santandereana de taekwondo', 'kduarte707@hotmail.com', '', '3134602715', '3134602715', '', 'Entrenadores', 'Entrenador', 'Logo.gif'),
(124, 'JAVIER ALEJANDRO CAMARGO URREA', 'FUERZAS ARMADAS (FF.AA)', 'javineo22@hotmail.com', '', '3132525801', '3132525801', '', 'Jueces', 'Arbitro', 'DSC_4287.JPG'),
(125, 'JUAN MANUEL CASAS SANCHEZ', 'FUERZAS ARMADAS (FF.AA)', 'juancasastkd@hotmail.com', '', '3013818810', '3013818810', '', 'Entrenadores', 'Entrenador', 'juan casas.jpg'),
(126, 'FREDY WILLIAM TOLA FLOREZ', 'FUERZAS ARMADAS (FF.AA)', 'fredytol43@hotmail.com', '', '3132641850', '3132641850', '', 'Jueces', 'Arbitro', 'FREDDY TOLA.JPG'),
(127, 'DENNIS YERALDIN BARRERO SANCHEZ', 'FUERZAS ARMADAS (FF.AA)', 'chea1706@hotmail.com', '', '3118337273', '3118337273', '', 'Entrenadores', 'Entrenador', 'DENNIS BARRERO.jpg'),
(128, 'JOSE JAVIER MUÃ‘OZ ALVARADO', 'FUERZAS ARMADAS (FF.AA)', 'suriyingbosa8335@hotmail.com', '', '3143948574', '3143948574', '', 'Entrenadores', 'Entrenador', 'DSC_4289.JPG'),
(129, 'NICOLAS ALEJANDRO CASTRO SALCEDO', 'FUERZAS ARMADAS (FF.AA)', 'casatrodt@gmail.com', '', '3114685430', '3114685430', '', 'Jueces', 'Arbitro', 'NICOLAS CASTRO.jpg'),
(130, 'JULIO CESAR DUQUE CRUZ', 'FUERZAS ARMADAS (FF.AA)', 'koletkd@hotmail.com', '', '3142964263', '3142964263', '', 'Entrenadores', 'Entrenador', 'foto cesar.JPG'),
(131, 'JULIO CESAR DUQUE CRUZ', 'FUERZAS ARMADAS (FF.AA)', 'koletkd@hotmail.com', '', '3142964263', '3142964263', '', 'Entrenadores', 'Entrenador', 'foto cesar.JPG'),
(132, 'JULIO CESAR DUQUE CRUZ', 'FUERZAS ARMADAS (FF.AA)', 'koletkd@hotmail.com', '', '3142964263', '3142964263', '', 'Entrenadores', 'Entrenador', 'foto cesar.JPG'),
(133, 'ELIANA SIZA PACHON', 'FUERZAS ARMADAS (FF.AA)', 'koletkd@hotmail.com', '', '3138892355', '3138892355', '', 'Jueces', 'Deportista', 'IMG-20170208-WA0023.jpg'),
(134, 'JAZMIN JEREZ OLACHICA', 'FUERZAS ARMADAS (FF.AA)', 'koletkd@hotmail.com', '', '3193586999', '3193586999', '', 'Entrenadores', 'Deportista', 'IMG-20170208-WA0027.jpg'),
(135, 'Leoanrdo Cortes Riveros', 'Sua D.C. Liga de Bogota', 'leonardotk@hotmail.com', '', '3212643143', '', '', 'Entrenadores', 'Entrenador', '20170208085345503.pdf'),
(136, 'JUAN ALBERTO MONTEALEGRE QUEZADA', 'BOLIVAR', 'montealegrejuan@hotmail.com', '', '3017951632', '', '', 'Jueces', 'Arbitro', 'IMG_20170208_095611.jpg'),
(137, 'Jehison Camilo Mateus Barreto', 'club SUA D.C.  liga de taekwondo de Bogota', 'camole40@gmail.com', '', '8120659', '3183992456', '', 'Entrenadores', 'Deportista', 'foto de perfil recortada.jpg'),
(138, 'YONNY NELSON ARIAS BONILLA', 'LIGA DEL CESAR', 'yonnynelson@yahoo.com', '', '6448513', '3012281728', '', 'Jueces', 'Arbitro', 'YONNY 7 X 5.jpg'),
(139, 'Michael David Rincon Guerrero', 'Club Ju Jak / Liga Santandereana de taekwondo', 'kyuby4455@gmail.com', '', '3204425032', '3204425032', '', 'Entrenadores', 'Deportista', 'Logo.gif'),
(140, 'Maria Alejandra Gomez Diaz', 'Club Ju Jak / Liga Santandereana de taekwondo', 'violdidi@hotmail.com', '', '3012630614', '3012630614', '', 'Entrenadores', 'Deportista', 'Logo (1).gif'),
(141, 'Carlos Arturo Rios Hernandez ', 'Club Ju Jak / Liga Santandereana de taekwondo', 'campeonrios@hotmail.com', '', '3214019036', '3214019036', '', 'Entrenadores', 'Deportista', 'Logo.gif'),
(142, 'VÃ­ctor Hugo paz saucedo', 'Valle', 'visaucedo@hotmail.com', '', '3006538257', '3006538257', '', 'Jueces', 'Arbitro', 'IMG_2071.PNG'),
(143, 'Raul Andres Lopez Rubio', 'Club Ju Jak / Liga Santandereana de taekwondo', 'raul9909rubio@gmail.com', '', '3176661692', '3176661692', '', 'Entrenadores', 'Deportista', 'Logo (1).gif'),
(144, 'Juan Manuel Romero Rugeles ', 'Club Ju Jak / Liga Santandereana de taekwondo', 'jromerorugeles@hotmail.com', '', '6597392', '3223788410', '', 'Entrenadores', 'Deportista', 'Logo.gif'),
(145, 'Diego Fernando Ovalle Alvarez ', 'Club Ju Jak / Liga Santandereana de taekwondo', 'ovallediego35@gmail.com', '', '3045332650', '3045332650', '', 'Entrenadores', 'Deportista', 'Logo.gif'),
(146, 'OSCAR ORLANDO MARTINEZ RICARDO ', 'LIGA DEL VALLE', 'oscarmartinezkydos@gmail.com', '', '8934312', '3164956553', '', 'Entrenadores', 'Entrenador', 'Imagen1.jpg'),
(147, 'NATALIA ANDREA MARTINEZ LOPEZ', 'FUERZAS ARMADAS (FF.AA)', 'natalia.amartinezlopez@gmail.com', '', '3507050987', '3507050987', '', 'Entrenadores', 'Deportista', 'IMG-20170208-WA0040.jpg'),
(148, 'BLANCA RUTH GUZMAN RIBEIRO', 'FUERZAS ARMADAS (FF.AA)', 'tkdfutbol3009@hotmail.com', '', '3144237339', '3144237339', '', 'Jueces', 'Arbitro', 'IMG-20170208-WA0032.jpg'),
(149, 'Edgar Garzon ', 'Dragon showa liga cundinamarca', 'Edgargarzon85@gmail.com ', '', '8478394', '3112894217', '', 'Entrenadores', 'Entrenador', '148657075774980844460.jpg'),
(150, 'JUAN CARLOS MEZA BECERRA', 'liga valle taekwondo', 'jukamebe@hotmail.com', '', '0000', '3207859939', '', 'Entrenadores', 'Entrenador', 'MEZA.jpg'),
(151, 'MARLON CUERVO FORERO', 'Liga Taekwondo BogotÃ¡', 'martkd13@hotmail.com', '', '6895471', '3003075270', '', 'Entrenadores', 'Entrenador', '11817166_10206567559682203_348095445217291461_n.jpg'),
(152, 'Marcela ArÃ©valo Vega', 'Liga de BogotÃ¡', 'mararevega@hotmail.com', '', '2058063', '3007424402', '', 'Jueces', 'Arbitro', 'Foto de Marcela ArÃ©valo Vega.jpg'),
(153, 'Mario Alberto Mora Sanabria', 'Liga de BogotÃ¡', 'moramtkd@gmail.com', '', '2058063', '3003502274', '', 'Jueces', 'Arbitro', 'Foto de Mario Alberto Mora Sanabria.jpg'),
(154, 'ARNOL LOPEZ GUTIERREZ', 'META', 'arnollopezgutierrez@outlook.es', '', '3194650860', '3194650860', '', 'Jueces', 'Entrenador', 'ARNOL LOPEZ.jpg'),
(155, 'HUGO ANDRES PEREZ LOZANO', 'HWARANGDO / BOGOTA', 'andresferrari_marlboro23@hotmail.com', '', '2703888', '3105510291', '', 'Entrenadores', 'Deportista', '004.jpg'),
(156, 'Carlos Eduardo Vanegas BriÃ±ez', 'Escuela Deportiva', 'apatinog0911@gmail.com', '', '4029662', '3057471184', '', 'Entrenadores', 'Entrenador', '20170207_125135.jpg'),
(157, 'MARTHA CAROLINA RIVAS MURILLO', 'LIGA TAEKWONDO DEL CHOCO', 'caritotkd@hotmail.com', '', '3122961637', '3122961637', '', 'Entrenadores', 'Entrenador', 'FOTO MARTHA.JPG'),
(158, 'Yeison Alfonso Castellanos Sanabria ', 'Hwarangdo/ Liga de bogota ', 'jcastellanoss.0214@outlook.es ', '', '3165306085', '3165306085', '', 'Entrenadores', 'Deportista', '20160328_200107.jpg'),
(159, 'WILSON ARTURO MILLAN RODRIGUEZ', 'Subak / BogotÃ¡', 'subakwtf@hotmail.com', '', '7218303', '3105626733', '', 'Entrenadores', 'Entrenador', 'img092.jpg'),
(160, 'JANETH SARMIENTO ESPINEL', 'TAEBEK  LIGA DE BOGOTA', 'janethtkd@hotmail.com               JANETHTKD@GMAIL.COM', '', '6594926', '3132968368', '', 'Entrenadores', 'Entrenador', 'FOTO JANETH.JPG'),
(161, 'CRISTIAN ORTEGA SINING', 'Liga Santandereana de Taekwondo ', 'cosinning@gmail.com', '', '3158279186', '3158279186', '', 'Entrenadores', 'Entrenador', 'Logo (1).gif'),
(162, 'SANDRA ROCÃO LÃ“PEZ REYES', 'LE-COQ- BOGOTÃ', 'sanlucam@hotmail.com', '', '2001604', '3172757111', '', 'Entrenadores', 'Entrenador', '20170208_151910 FOTO SANDRA ROCÃO.jpg'),
(163, 'DANIEL GOMEZ MALDONADO', 'Club Pine Black / Liga Santandereana de Taekwondo ', 'maestro_daniel_tkd@hotmail.com ', '', '3142226754', '3142226754', '', 'Entrenadores', 'Entrenador', 'Logo.gif'),
(164, 'OMAR FELIPE SANCHEZ CANCELADO ', 'Club Pine Black / Liga Santandereana de Taekwondo ', 'felipesanchezc95@gmail.com', '', '3202485967', '3202485967', '', 'Entrenadores', 'Entrenador', 'Logo.gif'),
(165, 'Luis Eduardo Torres Arias', 'Club Bushido', 'luchokwan@hotmail.com', '', '3 623762', '301 4985559', '', 'Entrenadores', 'Entrenador', '20161119_173211-1.jpg'),
(166, 'Diego Ferney Ramos RodrÃ­guez ', 'LIGA DEL TOLIMA', 'diegoferneyr@hotmail.com', '', '3144577767', '3144577767', '', 'Jueces', 'Arbitro', 'FOTOdoc1.jpg'),
(167, 'RUBEN DARIO ZULUAGA NAVAS', 'CLUB DE TAEKWONDO OLYMPIC/LIGA DE TAEKWONDO BOGOTA', 'rubenzuluaga90@gmail.com', '', '6278407', '3008871395', '', 'Jueces', 'Arbitro', 'received_10207942582992982.jpeg'),
(168, 'HUGO ALEXANDER RUBIO LOAIZA', 'CLUB DEPORTIVO FUDAM', 'fudamcolombia@yahoo.com', '', '8143063', '3158507473', '', 'Jueces', 'Entrenador', 'HUGO RUBIO LOAIZA.JPG'),
(169, 'WILSON YOPASA CAMACHO', 'SOMBAE / Liga de BogotÃ¡', 'wilsombae@yahoo.com', '', '3836044', '3042037019', '', 'Entrenadores', 'Entrenador', 'Wilson YopasÃ¡.jpg'),
(170, 'JOSÃ‰ ALFREDO BERNAL PINILLA', 'Club Zen Duk Kwan - BogotÃ¡', 'alfredobernalp@gmail.com', '', '3015758585', '3015758585', '', 'Jueces', 'Entrenador', 'Alfredo Bernal.jpg'),
(171, 'JORGE JULIAN FONSECA BOLIVAR', 'n/a', 'jorge.fonseca4590@correo.policia.gov.co', '', '3143511313', 'n/a', '', 'Entrenadores', 'Entrenador', 'IMG_20170208_173012.JPG'),
(172, 'JOSE DANILO LORZA CEBALLOS', 'liga Vallecaucana ', 'danilorza@gmail.com', '', '2 5143081', '3174488931', '', 'Jueces', 'Arbitro', 'josedanilo001.jpg'),
(173, 'Ivan Manuel Alvarez Martinez', 'Club Deportivo FUDAM', 'ivanzalvarez3@gmail.com', '', '4610499', '3212728967', '', 'Entrenadores', 'Entrenador', 'Foto documento.png'),
(174, 'Stephany Palomares RodrÃ¬guez ', 'Lourdes/Bogota', 'steffy0891@hotmail.com', '', '3053117', '3103254709', '', 'Jueces', 'Arbitro', 'IMG_3217.JPG'),
(175, 'JESSENIA BARRERA PALACIOS', 'LIGA VALLECAUCANA ', 'jesy-1xx@hotmail.com', '', '4461050', '3184654050', '', 'Jueces', 'Deportista', '16652551_10206935269500385_575577738_n.jpg'),
(176, 'Elkin Ferney Quintero Gomez', 'Freemen', 'Ekquintero@gmail.com', '', '3208935996', '3208935996', '', 'Entrenadores', 'Deportista', '20170208_211311.jpg'),
(177, 'edward fabian ipuz aldana ', 'club deportivo guagua / liga de taekwondo del huila ', 'edwardbbtkd@gmail.com', '', '3156168895', '', '', 'Entrenadores', 'Entrenador', 'edward ipuz.jpg'),
(178, 'jose david lopez  laiseca ', 'club  deportivo de taekwondo mulgyeondo / huila ', 'mulgyeongdo@hotmail.com', '', '3103304810', '', '', 'Entrenadores', 'Entrenador', 'tacho.jpg'),
(179, 'luis humberto casilima  mendez ', 'gengis khan / huila ', 'luiscasilima@hotmail.com', '', '3202566347', '', '', 'Entrenadores', 'Entrenador', 'casilima.jpg'),
(180, 'carlos andres vargas  conde ', 'tamas / huila ', 'cardres428@hotmail.com', '', '3214289464', '', '', 'Entrenadores', 'Entrenador', '942298_493052464098231_1157100590_n.jpg'),
(181, 'MarÃ­a Catalina Rojas Jaramillo', 'Club Deportivo FUDAM', 'CatalinaRojasJ1998@gmail.com', '', '4631644', '3017860530', '', 'Entrenadores', 'Deportista', 'Foto cata.JPG'),
(182, 'luis alexander tovar restrepo ', 'liga de taekwondo del huila ', 'latovar_usco@hotmail.com', '', '3112494236', '', '', 'Entrenadores', 'Entrenador', 'luis tovar.jpg'),
(183, 'fernando saenz ', 'laboyano / huila ', 'fernandosaenz4426@hotmail.com', '', '3112959032', '', '', 'Entrenadores', 'Entrenador', 'pitalito.jpg'),
(184, 'faiver herney bravo jamioy', 'cps / huila ', 'faivertkd@gmail.com', '', '31034098', '', '', 'Entrenadores', 'Entrenador', 'faiver.jpg'),
(185, 'RAUL ALFONSO PINZON PALACIOS', 'FUDAM', 'r.pinzonp@hotmail.com', '', '3118702504', '3118702504', '', 'Entrenadores', 'Entrenador', 'z. RAUL PINZON PALACIOS.jpg'),
(186, 'bryan camilo solano ramos ', 'utrahuilca / huila ', 'bryancamilo96@hotmail.com', '', '3102595579', '', '', 'Jueces', 'Arbitro', '16651152_10206390743766420_55475739_o.jpg'),
(187, 'kewin dario  arce munarez ', 'mulgyeongdo / huila ', 'mulgyeongdo@hotmail.com', '', '322422084', '', '', 'Jueces', 'Arbitro', 'kewin.jpg'),
(188, 'daniel  eduardo puentes zamora ', 'fundacion amigo como arroz / huila ', 'dapunza90@gmail.com', '', '3138550369', '', '', 'Entrenadores', 'Delegado', 'daniel puentes.jpg'),
(189, 'Paulo Cesar Martinez Reyes', 'TOLIMA ', 'licenciadopaulorreyes@hotmail.com', '', '2624217', '3232109057', '', 'Entrenadores', 'Delegado', 'IMG_20141008_135705.jpg'),
(190, 'Jeison AndrÃ©s SÃ¡nchez Agudelo', 'Universidad De Antioquia / Liga AntioqueÃ±a', 'jasandres5@gmail.com', '', '2816072', '3168242658', '', 'Jueces', 'Deportista', '20170208_225026-01.jpeg'),
(191, 'Jeison AndrÃ©s SÃ¡nchez Agudelo', 'Universidad de Antioquia', 'jasandres5@gmail.com', '', '2816072', '3168242658', '', 'Entrenadores', 'Deportista', '20170208_225026-01.jpeg'),
(192, 'ANDRES SANCHEZ PEÃ‘A', 'BOYACÃ', 'mdkboyaca@gmail.com', '', '3124514555', '3124514555', '', 'Entrenadores', 'Entrenador', 'IMG-20161102-WA0076.jpg'),
(193, 'JEISSON FABIAN SAAVEDRA TORRES', 'META', 'jeissontaekwondo@hotmail.com', '', '3142909661', '3142909661', '', 'Entrenadores', 'Entrenador', 'JEISSON SAAVEDRA.jpg'),
(194, 'GIOVANNI ORLANDO LABRADOR PARRA', 'CLUB DEPORTIVO FUDAM', 'giovannishiva@gmail.com', '', '4708575', '3202909241', '', 'Entrenadores', 'Deportista', 'FOTO HOJA DE VIDA.jpg'),
(195, 'jose luis vargas cerquera ', 'hansoo taekwondo team / huila ', 'joseluisbomberogarzon@gmail.com', '', '3202586872', '', '', 'Entrenadores', 'Entrenador', 'unnamed.jpg'),
(196, 'WILLIAM  EDGARDO PORRAS LOZANO', 'FUERZAS ARMADAS (FFAA)', 'taekwondoff.aa@hotmail.com', '', '3203182004', '3203182004', '', 'Entrenadores', 'Entrenador', 'WILLIAM.jpg'),
(197, 'Yeison Alfonso Castellanos Sanabria ', 'Hwarangdo/ Liga de bogota ', 'jcastellanoss.0214@outlook.es ', '', '3165306085', '3165306085', '', 'Entrenadores', 'Deportista', '20160328_200107.jpg'),
(198, 'jaime diaz rojas', 'fedecoam', 'clubdtaekwondotigres@hotmail.com', '', '3229424290', '3229424290', '', 'Entrenadores', 'Entrenador', 'Scan foto jaime.jpg'),
(199, 'Laura Camila Rojas Valbuena', 'UniÃ³n Tong Il', 'laurarojas935@gmail.com', '', '7534268', '3005272217', '', 'Entrenadores', 'Deportista', 'Foto Laura Rojas.jpg'),
(200, 'GINA MARCELA SANTOS PALERMO', 'Club Deportivo Fudam', 'ginaaaam@yahoo.com', '', '2799129', '3053686047', '', 'Entrenadores', 'Entrenador', 'img317.jpg'),
(201, 'Pedro Velasquez LeÃ³n ', 'Gyeongju/Cundinamarca ', 'pedrovelasquezleon@gmail.com', '', '4828659', '3202541426', '', 'Entrenadores', 'Entrenador', 'IMG_0195.JPG'),
(202, 'EDWIN RAMIREZ MURCIA', 'Liga Tolimense de Taekwondo', 'ramie1978@hotmail.com', '', '2663793', '3135146916', '', 'Entrenadores', 'Entrenador', '15894355_10209593367845434_3210483352077910512_n.jpg'),
(203, 'CATHERINE ASTRID MORENO BUITRAGO', 'Club Deportivo FUDAM ', 'cambchas@gmail.com', '', '3108899963', '6959865', '', 'Entrenadores', 'Entrenador', 'foto4.png'),
(204, 'HUGO ALEXANDER RUBIO HERRERA', 'CLUB DEPORTIVO FUDAM BOGOTA', 'harhlegend@gmail.com', '', '3108898902', '3108898902', '', 'Entrenadores', 'Entrenador', 'HUGO A RUBIO H 2013.jpg'),
(205, 'JAVIER ALEXANDER MOSCOSO MORA', 'liga de taekwondo de bogota', 'alxmos79@hotmail.com', '', '3057539138', '2057529', '', 'Entrenadores', 'Entrenador', 'mis fotos.jpg'),
(206, 'DIEGO FERNANDO GONZALEZ RODRIGUEZ', 'ELITE', 'difegonro@hotmail.com', '', '2648081', '3175763660', '', 'Entrenadores', 'Entrenador', 'IMG_20170209_105203453[1].jpg'),
(207, 'EDUIL BOBADILLA VALLEJO', 'sahayan', 'bobadillacadena@hotmail.com', '', '5555555', '3142662400', '', 'Entrenadores', 'Entrenador', 'IMG-20170209-WA0002[1].jpg'),
(208, 'GABRIEL CANO', 'FUDAM /BOGOTA', 'gabrielcano@usantotomas.edu.co', '', '2582626', '3017482816', '', 'Entrenadores', 'Deportista', 'e2d34505-12f4-42be-8848-c0be368c3ccd.jpg'),
(209, 'CESAR AUGUSTO HERNÃNDEZ SANDOVAL ', 'Juventud deportiva/bogota', 'cahernandez61@misena.edu.co ', '', '3192574930', '', '', 'Entrenadores', 'Entrenador', 'IMG_20160707_131758.jpg'),
(210, 'CESAR AUGUSTO HERNÃNDEZ SANDOVAL ', 'Juventud deportiva/bogota', 'cahernandez61@misena.edu.co ', '', '3192574930', '', '', 'Entrenadores', 'Entrenador', 'IMG_20160707_131758.jpg'),
(211, 'Juan Diego Vela Rivera', 'Bogota', 'Jdiegovelarivera@gmail.com', '', '7159573', '3195566073', '', 'Jueces', 'Deportista', 'NuevoDocumento 2017-02-09.pdf'),
(212, 'JOHANNA HERNANDEZ ROJAS', 'Hwarangdo', 'johisluna86@hotmail.com', '', '3005721177', '3005721177', '', 'Entrenadores', 'Entrenador', '395784_10150473726573821_1313402990_n.jpg'),
(213, 'JOHANNA HERNANDEZ ROJAS', 'Hwarangdo', 'johisluna86@hotmail.com', '', '3005721177', '3005721177', '', 'Entrenadores', 'Entrenador', '395784_10150473726573821_1313402990_n.jpg'),
(214, 'SANDRA VIVIANA', 'LIGA DEL VALLE', 'svnh1986@gmail.com', '', '0', '3105982372', '', 'Jueces', 'Entrenador', 'foto sam.docx'),
(215, 'IGNACIO ROJAS RIVERA', 'Universidad de la Amazonia - CAQUETA', 'olympicus2004@hotmail.com', '', '0984356738', '3153198722', '', 'Entrenadores', 'Entrenador', 'IMG-20170208-WA0094.jpg'),
(216, 'IGNACIO ROJAS RIVERA', 'Universidad de la Amazonia - CAQUETA', 'olympicus2004@hotmail.com', '', '0984356738', '3153198722', '', 'Entrenadores', 'Entrenador', 'IMG-20170208-WA0094.jpg'),
(217, 'Hector Javier Garzon Infante', 'Cheong Ryong - Cundinamarca ', 'kingtro003@gmail.com', '', '855-2721', '3123732422', '', 'Entrenadores', 'Entrenador', 'HECTOR JAVIER GARZON.jpg'),
(218, 'LUIS HERNAN VELANDIA BERNAL', 'CHEONG RYONG TENJO - CUNDINAMARCA', 'hernanv_15@hotmail.com', '', '3124632330', '3124632330', '', 'Entrenadores', 'Entrenador', 'luis velandia.jpg'),
(219, 'sergio isaac muÃ±oz sarmiento ', 'olympic taekwondo / liga de taekwondo de bogota ', 'sergiotkdpower@gmail.com', '', '3173125780', '3173125780', '', 'Entrenadores', 'Entrenador', '11760277_411700595683041_1646972702090502462_n.jpg'),
(220, 'Hernan dario guzman cumbe ', 'El dragon de girardot cundinamarca ', 'DarÃ­o-x6@hotmail.com', '', '7836554', '3168155497', '', 'Entrenadores', 'Entrenador', '1486677450853474038218.jpg'),
(221, 'Dayanna alejandra murcia cuadros ', 'El dragon de girardot  cundinamarca ', 'Dayis_tkd@hotmail.com', '', '7836554', '3172508813', '', 'Entrenadores', 'Entrenador', '1486677583024-1005680239.jpg'),
(222, 'Arturo muÃ±oz osorio ', 'El dragon de girardot  cundinamarca ', 'Academiaeldragon@hotmail.com', '', '7836554', '3107770928', '', 'Entrenadores', 'Entrenador', 'IMG-20170209-WA0000.jpg'),
(223, 'PABLO ANDRES VALENCIA CANTE', 'CHEONG RYONG TENJO - CUNDINAMARCA', 'hernanv_15@hotmail.com', '', '3135932902', '3135932902', '', 'Entrenadores', 'Entrenador', 'FOTO (2)%255b2%255d.jpg'),
(224, 'RAMIRO ANDRES HERNANDEZ NOSSA', 'KOPULSO/LIGA DE BOGOTA', 'ramiro.hernandezn@gmail.com', '', '4889960', '3124807081', '', 'Jueces', 'Arbitro', 'Foto Ramiro Hernandez.jpg'),
(225, 'Cristian Sneyder Cardona Agudelo', 'TKD Superar', 'cristian-064@hotmail.com', '', '482 97 58', '301 255 1958', '', 'Entrenadores', 'Entrenador', 'IMG_3142.JPG'),
(226, 'JOHN ALEXANDER GARZON RODRIGUEZ', 'CUNDINAMARCA', 'alexander_garzon@hotmail.com', '', '3103057489', '3103057489', '', 'Jueces', 'Arbitro', 'IMG_FOTO_ALEXANDER_GARZON.jpg'),
(227, 'Edgar geovanny lopez ibarra ', 'El dragon de girardot  cundinamarca ', 'Academiaeldragon@hotmail.com', '', '7836554', '3144012701', '', 'Entrenadores', 'Deportista', '1486688113532-947700820.jpg'),
(228, 'MILTON FORERO', 'Casanare', 'miltonforero@gmail.com', '', '3123720370', '3123720370', '', 'Entrenadores', 'Entrenador', 'miltonforero.JPG'),
(229, 'JHONNY ARLEY PARRA DAZA', 'FUERZAS ARMADAS (FF.AA)', 'parradeportes@gmail.com', '', '3005313443', '3005313443', '', 'Entrenadores', 'Entrenador', '7d5baefe-26ef-4f09-a83f-0cf6ef6c4520.jpg'),
(230, 'Jorge Luis CÃ¡ceres Franco', 'Arauca', 'franco_jc007@hotmail.com', '', '8821682', '3105639705', '', 'Entrenadores', 'Entrenador', 'IMG_20170209_224534.jpg'),
(231, 'DIEGO FERNANDO ALARCON LARA', 'FUERZAS ARMADAS (FF.AA)', 'diegowazza@hotmail.com', '', '3005313443', '3005313443', '', 'Entrenadores', 'Entrenador', '692733e4-5a47-4b4e-a38e-17cc51b2b99b.jpg'),
(232, 'CINDY LORENA GONZALEZ FORERO', 'FUERZAS ARMADAS (FF.AA)', 'Cilogofo_93@hotmail.com. ', '', '3008631855', '3008631855', '', 'Entrenadores', 'Entrenador', '6dfa23a5-94ac-4142-bb49-a6a4f7bfad5f.jpg'),
(233, 'JHON FAIBER NIETO HERRERA', 'amazonas', 'unilibretkd@gmail.com', '', '3204514617', '3168786495', '', 'Entrenadores', 'Entrenador', 'JOHN FAIBER.jpg'),
(234, 'Jesus Zuluaga Ramirez', 'valle', 'hwarangsjz@yahoo.com', '', '3103912159', '3103912159', '', 'Jueces', 'Entrenador', '20160329_150815.jpg'),
(235, 'Jesus Zuluaga Ramirez', 'valle', 'hwarangsjz@yahoo.com', '', '3103912159', '3103912159', '', 'Entrenadores', 'Entrenador', '20160329_150815.jpg'),
(236, 'Jesus Zuluaga Ramirez', 'valle', 'hwarangsjz@yahoo.com', '', '3103912159', '3103912159', '', 'Entrenadores', 'Entrenador', '20160329_150815.jpg'),
(237, 'Stevenson Montoya Marulanda', 'Valle', 'steven_montoya@outlook.com', '', '3205575644', '3205575644', '', 'Entrenadores', 'Entrenador', '14867342803882045818081.jpg'),
(238, 'PABLO ANDRES VALENCIA CANTE ', 'Cheon riong - Cundinamarca ', 'andresvalencia08@hotmail.com', '', '8474368', '3135932902', '', 'Entrenadores', 'Entrenador', 'FOTO (2)%255b2%255d.jpg'),
(239, 'Maria de los angeles  morales guzman ', 'Boyaca', 'Gymmaria@hotmail.com', '', '4226949', '3043920397', '', 'Entrenadores', 'Entrenador', 'IMG_20170210_124545.jpg'),
(240, 'Nancy Orduz Hurtado', 'Boyaca', 'lilo.tkd@hotmail.com', '', '3115704143', '3115704143', '', 'Entrenadores', 'Entrenador', 'fotolilo.jpg'),
(241, 'Allan Rene Forero Acevedo', 'Boyaca', 'allanforeroacevedo@hotmail.com', '', '3213803748', '3213803748', '', 'Entrenadores', 'Entrenador', 'foto all.jpg'),
(242, 'WILSON ANTONIO NIETO NIÃ‘O', 'Boyaca', 'nietowilson25@hotmail.com', '', '3114600685', '3114600685', '', 'Entrenadores', 'Entrenador', 'MMMMMM.jpg'),
(243, 'carlos arturo fonseca alarcon', 'bogota', 'salometkwdo14@hotmail.com', '', '3203542642', '3203542642', '', 'Entrenadores', 'Entrenador', 'carlos.jpg'),
(244, 'LORENA DIAZ MORENO', 'liga de taekwondo boyaca', 'lorenitatkd@hotmail.com', '', '7702382', '3138916071', '', 'Entrenadores', 'Entrenador', 'foto.pdf'),
(245, 'Sandra Milena Rodriguez Naranjo', 'Liga de taekwondo boyaca', 'sandrarodrigueznaranjo123@gmail.com', '', '7711617', '3138532311', '', 'Entrenadores', 'Entrenador', 'sandra foto.pdf'),
(246, 'KEVYN ALEXANDER DUARTE CARRILLO', 'SANTANDER', 'kduarte707@unab.edu.co', '', '6416330', '3134602715', '', 'Entrenadores', 'Entrenador', 'WhatsApp Image 2017-02-11 at 11.37.50 AM (7).jpeg'),
(247, 'DANIEL FELIPE ESCOBAR SALAZAR', 'guajira', 'daniel98escobar.12@gmail.com', '', '4348772', '3014971032', '', 'Entrenadores', 'Entrenador', 'WhatsApp Image 2017-02-11 at 11.41.32 AM.jpeg'),
(248, 'JESUS ANTONIO BARRERA GENES', 'MAGDALENA', 'escuelakoryo@hotmail.com', '', '4302287', '3013956211', '', 'Entrenadores', 'Entrenador', 'WhatsApp Image 2017-02-11 at 11.37.50 AM (13).jpeg'),
(249, 'Jhon Jairo Montoya Creus', 'Guajira', 'clubeko@hotmail.com', '', '3005166123', '3005166123', '', 'Entrenadores', 'Entrenador', 'Foto.jpg'),
(250, 'Miguel Ãngel Walteros Cargas ', 'Guajira', 'miguelwalteros@hotmail.com', '', '7291228', '3014642765', '', 'Entrenadores', 'Entrenador', 'IMG-20170211-WA0000.jpg'),
(251, 'MarÃ­a Alejandra Mendoza Mendoza', 'Liga de taekwondo de CÃ³rdoba', 'Mayuyis011394@gmail.com', '', '3113624550', '3113624550', '', 'Entrenadores', 'Entrenador', 'InstaBox_201721275759108.jpg'),
(252, 'Andrea Marcela  Benavides Avila', 'Boyaca', 'benavides95@gmail.com', '', '7236573', '3144836556', '', 'Entrenadores', 'Entrenador', '377712_423990717636743_645267795_n.jpg'),
(253, 'ANDREA MARCELA BENAVIDES AVILA', 'LIGA DE BOYACA', 'benavides95@gmail.com', '', '3112080825', '3112080825', '', 'Entrenadores', 'Entrenador', 'equipo_marcela_benavides.png'),
(254, 'jhon alexander feria mejia', 'colombia', 'g-anguie@hotmail.es', '', '3148648694', '', '', 'Entrenadores', 'Deportista', '14908193_337356116628822_4171671779532026642_n (1).jpg');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `contactus_emails`
--

CREATE TABLE IF NOT EXISTS `contactus_emails` (
  `ce_id` int(11) NOT NULL AUTO_INCREMENT,
  `ce_name` varchar(255) NOT NULL,
  `ce_mails` varchar(255) NOT NULL,
  PRIMARY KEY (`ce_id`)
) ENGINE=MyISAM  DEFAULT CHARSET=latin1 AUTO_INCREMENT=4 ;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `menu`
--

CREATE TABLE IF NOT EXISTS `menu` (
  `m_id` int(11) NOT NULL AUTO_INCREMENT,
  `m_nom` varchar(66) NOT NULL DEFAULT '',
  `m_icon` varchar(66) NOT NULL DEFAULT '',
  `m_link` varchar(66) NOT NULL DEFAULT '',
  `m_icon2` varchar(66) NOT NULL DEFAULT '',
  `m_isModule` int(11) NOT NULL,
  `m_modelMod` varchar(255) NOT NULL,
  `m_folder` varchar(255) NOT NULL,
  PRIMARY KEY (`m_id`)
) ENGINE=MyISAM  DEFAULT CHARSET=utf8 AUTO_INCREMENT=97 ;

--
-- Volcado de datos para la tabla `menu`
--

INSERT INTO `menu` (`m_id`, `m_nom`, `m_icon`, `m_link`, `m_icon2`, `m_isModule`, `m_modelMod`, `m_folder`) VALUES
(1, 'Inicio', 'fa-tachometer', 'home.php?p=desk.php', 'images/desktop.png', 0, '', ''),
(7, 'Usuarios', 'fa-users', 'home.php?p=usr/view/index.php', 'images/users.png', 0, '', ''),
(12, 'Admmodulos', 'fa-cogs', 'home.php?p=adminMod.php', '', 0, '', ''),
(55, 'Productos', 'fa-shopping-bag ', 'home.php?p=modules/products/view/index.php', '', 1, 'Products', 'products'),
(36, 'Contactenos', 'fa-envelope ', 'home.php?p=contactus/view/index.php', '', 0, '', ''),
(46, 'Reservas', 'fa-calendar', 'home.php?p=modules/booking/view/index.php', '', 2, '', 'booking'),
(47, 'Cuentas', 'fa-file-text', 'home.php?p=modules/accounts/view/index.php', '', 2, '', 'accounts'),
(45, 'Clientes', 'fa-male ', 'home.php?p=modules/crm/view/index.php', '', 2, '', 'crm'),
(53, 'Inventario', 'fa-archive ', 'home.php?p=modules/capsinventory/view/index.php', '', 2, 'Capsinventory', 'capsinventory'),
(54, 'Mantenimientos', 'fa-wrench', 'home.php?p=modules/maintenance/view/index.php', '', 1, 'Maintenance', 'maintenance'),
(56, 'Slider', 'fa-picture-o', 'home.php?p=modules/sliders/view/index.php', '', 1, 'Sliders', 'sliders'),
(59, 'pos', 'fa-barcode', 'home.php?p=modules/posmodule/view/index.php', '', 2, 'Posmodule', 'posmodule'),
(60, 'Cafeteria', 'fa-coffee ', 'home.php?p=modules/posmoduleadv/view/index.php', '', 2, 'Posmoduleadv', 'posmoduleadv'),
(8, 'Setup', 'fa-cog', 'home.php?p=setup/view/index.php', '', 0, '', ''),
(62, 'Mapa', 'fa-map', 'home.php?p=modules/gemap/view/index.php', '', 1, 'Gemap', 'gemap'),
(63, 'Multimedia', 'fa-film', 'home.php?p=modules/multimedia/view/index.php', '', 1, 'Multimedia', 'multimedia'),
(82, 'Contenidos', 'fa-rocket', 'home.php?p=modules/content/view/index.php', '', 1, 'Content', 'content'),
(65, 'Galeria', 'fa-camera', 'home.php?p=modules/gallery/view/index.php', '', 1, 'Gallery', 'gallery'),
(66, 'Reportes', 'fa-pie-chart ', 'home.php?p=modules/graphreport/view/index.php', '', 1, 'Graphreport', 'graphreport'),
(81, 'Cognos', 'fa-check-square', 'home.php?p=modules/getest/view/index.php', '', 1, 'getest', 'getest'),
(68, 'Zona Clientes', 'fa-user-secret', 'home.php?p=modules/customerzone/view/index.php', '', 1, 'Customerzone', 'customerzone'),
(85, 'Banners', 'fa-smile-o', 'home.php?p=modules/banner/view/index.php', '', 1, 'banner', 'banner'),
(84, 'Bloques', 'fa-object-group', 'home.php?p=modules/blocks/view/index.php', '', 1, 'blocks', 'blocks'),
(83, 'Noticias', 'fa-newspaper-o', 'home.php?p=modules/news/view/index.php', '', 1, 'News', 'news'),
(86, 'Anticipos', 'fa-money', 'home.php?p=modules/advances/view/index.php', '', 1, 'advances', 'advances'),
(87, 'Rastreo', 'fa-eye', 'home.php?p=modules/tracker/view/index.php', '', 1, 'tracker', 'tracker'),
(88, 'YouTube', 'fa-youtube-play ', 'home.php?p=modules/videos/view/index.php', '', 1, 'videos', 'videos'),
(89, 'Admon Archivos', 'fa-file', 'home.php?p=modules/filemanager/view/index.php', '', 1, 'filemanager', 'filemanager'),
(90, 'Gmap', 'fa-map-marker', 'home.php?p=modules/gmap/view/index.php', '', 1, 'gmap', 'gmap'),
(91, 'Control Dominio', 'fa-lock', 'home.php?p=modules/liccontrol/view/index.php', '', 1, 'liccontrol', 'liccontrol'),
(92, 'Gastos', 'fa-money', 'home.php?p=modules/expenses/view/index.php', '', 2, 'Expenses', 'expenses'),
(93, 'Calendario Reservas', 'fa-calendar-o', 'home.php?p=modules/bookingcalendar/view/index.php', '', 1, 'bookingcalendar', 'bookingcalendar'),
(95, 'Autoreport', 'fa-file-text-o', 'home.php?p=modules/autoreport/view/index.php', '', 1, 'autoreport', 'autoreport'),
(96, 'Doc. Acuerdo', 'fa-pencil', 'home.php?p=modules/docagree/view/index.php', '', 1, 'docagree', 'docagree');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `pro_menu`
--

CREATE TABLE IF NOT EXISTS `pro_menu` (
  `pm_id` int(11) NOT NULL AUTO_INCREMENT,
  `up_id` int(11) NOT NULL DEFAULT '0',
  `m_id` int(11) NOT NULL DEFAULT '0',
  PRIMARY KEY (`pm_id`)
) ENGINE=MyISAM  DEFAULT CHARSET=utf8 AUTO_INCREMENT=177 ;

--
-- Volcado de datos para la tabla `pro_menu`
--

INSERT INTO `pro_menu` (`pm_id`, `up_id`, `m_id`) VALUES
(1, 1, 1),
(7, 1, 7),
(11, 1, 6),
(20, 1, 10),
(21, 1, 11),
(23, 1, 12),
(24, 1, 13),
(25, 1, 14),
(26, 1, 15),
(27, 1, 16),
(28, 1, 17),
(29, 1, 18),
(30, 1, 20),
(31, 1, 20),
(32, 1, 21),
(33, 1, 21),
(34, 1, 22),
(35, 1, 23),
(36, 1, 24),
(37, 1, 25),
(38, 1, 26),
(39, 1, 27),
(40, 1, 28),
(41, 1, 29),
(42, 1, 30),
(43, 1, 32),
(44, 1, 33),
(45, 0, 1),
(46, 0, 7),
(47, 0, 12),
(48, 0, 29),
(49, 0, 33),
(50, 0, 35),
(51, 0, 27),
(52, 1, 35),
(54, 1, 37),
(55, 1, 38),
(56, 1, 39),
(57, 1, 40),
(58, 1, 41),
(76, 5, 46),
(80, 1, 54),
(74, 5, 38),
(73, 5, 1),
(63, 1, 42),
(64, 1, 43),
(65, 1, 44),
(66, 1, 45),
(67, 1, 46),
(68, 1, 47),
(69, 1, 48),
(70, 1, 49),
(71, 1, 52),
(72, 1, 53),
(77, 5, 47),
(78, 5, 45),
(79, 5, 53),
(81, 1, 55),
(82, 6, 1),
(83, 6, 46),
(84, 6, 47),
(85, 6, 45),
(86, 6, 53),
(87, 6, 54),
(88, 7, 46),
(89, 1, 56),
(90, 8, 1),
(91, 8, 46),
(92, 8, 47),
(93, 8, 45),
(113, 1, 65),
(112, 9, 64),
(111, 1, 64),
(110, 9, 63),
(109, 9, 62),
(99, 1, 57),
(100, 1, 58),
(101, 1, 59),
(102, 1, 60),
(103, 1, 61),
(104, 5, 55),
(105, 5, 54),
(106, 5, 59),
(107, 1, 62),
(108, 1, 63),
(114, 9, 65),
(115, 1, 66),
(116, 1, 36),
(117, 10, 55),
(118, 10, 46),
(119, 10, 47),
(120, 10, 45),
(121, 10, 53),
(122, 10, 54),
(123, 10, 56),
(124, 10, 59),
(125, 10, 66),
(126, 10, 1),
(127, 1, 67),
(128, 1, 68),
(129, 1, 69),
(130, 1, 70),
(131, 1, 71),
(132, 1, 80),
(133, 1, 8),
(134, 1, 81),
(135, 11, 81),
(136, 10, 60),
(137, 12, 36),
(138, 1, 82),
(139, 1, 83),
(140, 1, 84),
(141, 1, 85),
(142, 1, 86),
(143, 1, 87),
(144, 1, 88),
(145, 1, 89),
(146, 1, 90),
(147, 13, 55),
(148, 13, 45),
(149, 13, 68),
(150, 13, 86),
(151, 13, 87),
(152, 13, 89),
(153, 1, 91),
(154, 14, 7),
(155, 14, 12),
(156, 14, 55),
(158, 14, 46),
(159, 1, 92),
(160, 10, 92),
(161, 1, 93),
(162, 1, 94),
(163, 1, 95),
(164, 1, 96),
(165, 15, 7),
(166, 15, 12),
(167, 15, 55),
(168, 15, 45),
(169, 15, 53),
(170, 15, 66),
(171, 15, 86),
(172, 15, 89),
(173, 15, 90),
(174, 15, 92),
(175, 15, 96),
(176, 15, 1);

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `pro_submenu`
--

CREATE TABLE IF NOT EXISTS `pro_submenu` (
  `psm_id` int(11) NOT NULL AUTO_INCREMENT,
  `up_id` int(11) NOT NULL,
  `sm_id` int(11) NOT NULL,
  PRIMARY KEY (`psm_id`)
) ENGINE=MyISAM  DEFAULT CHARSET=latin1 AUTO_INCREMENT=130 ;

--
-- Volcado de datos para la tabla `pro_submenu`
--

INSERT INTO `pro_submenu` (`psm_id`, `up_id`, `sm_id`) VALUES
(1, 1, 15),
(2, 1, 16),
(3, 1, 17),
(10, 1, 29),
(9, 1, 30),
(8, 1, 31),
(7, 1, 21),
(11, 1, 46),
(12, 1, 45),
(13, 1, 53),
(14, 1, 54),
(15, 1, 55),
(16, 1, 58),
(17, 1, 57),
(18, 1, 56),
(19, 1, 62),
(20, 5, 31),
(21, 5, 30),
(22, 5, 29),
(23, 5, 46),
(24, 5, 45),
(25, 5, 62),
(26, 5, 15),
(27, 5, 16),
(28, 5, 18),
(29, 5, 20),
(30, 5, 21),
(31, 1, 64),
(32, 1, 63),
(34, 9, 15),
(35, 9, 16),
(36, 9, 18),
(37, 6, 46),
(38, 6, 15),
(39, 6, 16),
(40, 6, 18),
(41, 6, 58),
(42, 1, 66),
(43, 1, 67),
(44, 1, 68),
(45, 1, 69),
(46, 1, 70),
(47, 1, 71),
(48, 1, 72),
(49, 1, 73),
(50, 1, 78),
(51, 1, 79),
(52, 1, 80),
(53, 1, 81),
(54, 5, 64),
(55, 5, 63),
(56, 5, 65),
(57, 5, 58),
(58, 5, 57),
(59, 5, 56),
(60, 5, 70),
(61, 5, 71),
(62, 5, 72),
(63, 5, 73),
(64, 5, 74),
(65, 5, 75),
(66, 5, 76),
(67, 5, 77),
(68, 1, 18),
(69, 1, 19),
(70, 1, 20),
(71, 10, 31),
(72, 10, 30),
(73, 10, 29),
(74, 10, 64),
(75, 10, 46),
(76, 10, 45),
(77, 10, 62),
(78, 10, 63),
(79, 10, 65),
(80, 10, 15),
(81, 10, 16),
(82, 10, 17),
(83, 10, 18),
(84, 10, 19),
(85, 10, 20),
(86, 10, 21),
(87, 10, 58),
(88, 10, 57),
(89, 10, 56),
(90, 10, 70),
(91, 10, 71),
(92, 10, 72),
(93, 10, 73),
(94, 10, 74),
(95, 10, 75),
(96, 10, 76),
(97, 10, 77),
(98, 1, 65),
(99, 10, 78),
(100, 10, 79),
(101, 10, 80),
(102, 10, 81),
(103, 13, 15),
(104, 13, 16),
(105, 13, 21),
(106, 13, 17),
(107, 14, 31),
(108, 14, 30),
(109, 14, 29),
(110, 14, 64),
(111, 1, 82),
(112, 1, 83),
(113, 1, 84),
(114, 10, 82),
(115, 10, 83),
(116, 10, 84),
(117, 15, 15),
(118, 15, 16),
(119, 15, 17),
(120, 15, 18),
(121, 15, 19),
(122, 15, 20),
(123, 15, 21),
(124, 15, 58),
(125, 15, 57),
(126, 15, 56),
(127, 15, 82),
(128, 15, 83),
(129, 15, 84);

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `submenu`
--

CREATE TABLE IF NOT EXISTS `submenu` (
  `sm_id` int(11) NOT NULL AUTO_INCREMENT,
  `sm_nom` varchar(255) NOT NULL,
  `sm_link` varchar(255) NOT NULL,
  `m_id` int(11) NOT NULL,
  PRIMARY KEY (`sm_id`)
) ENGINE=MyISAM  DEFAULT CHARSET=latin1 AUTO_INCREMENT=85 ;

--
-- Volcado de datos para la tabla `submenu`
--

INSERT INTO `submenu` (`sm_id`, `sm_nom`, `sm_link`, `m_id`) VALUES
(31, 'Setup', 'home.php?p=modules/booking/view/showBookingConfig.php', 46),
(30, 'Adicionales', 'home.php?p=modules/booking/view/showBookingAdd.php', 46),
(29, 'Eventos', 'home.php?p=modules/booking/view/showBookingEvents.php', 46),
(53, 'Egresos', 'home.php?p=modules/capsinventory/view/newCapsInvOut.php', 52),
(46, 'Mis ventas', 'home.php?p=modules/accounts/view/showSalesUserInf.php', 47),
(45, 'Inf. Ventas', 'home.php?p=modules/accounts/view/showSalesInf.php', 47),
(54, 'Setup', 'home.php?p=modules/capsinventory/view/showSetup.php', 52),
(15, 'Clientes', 'home.php?p=modules/crm/view/showCustomers.php', 45),
(16, 'Cotizaciones', 'home.php?p=modules/crm/view/showAllQuotes.php', 45),
(17, 'Pedidos', 'home.php?p=modules/crm/view/showAllOrders.php', 45),
(18, 'Mis Cotizaciones', 'home.php?p=modules/crm/view/showUserQuotes.php', 45),
(19, 'Mis Ordenes', 'home.php?p=modules/crm/view/showUserOrders.php', 45),
(20, 'Mis Clientes', 'home.php?p=modules/crm/view/showUserCustomers.php', 45),
(21, 'Setup', 'home.php?p=modules/crm/view/showSetup.php', 45),
(62, 'Autorizaciones', 'home.php?p=modules/accounts/view/showUsers.php', 47),
(58, 'Informe', 'home.php?p=modules/capsinventory/view/showInfCapsInventory.php', 53),
(57, 'Setup', 'home.php?p=modules/capsinventory/view/showSetup.php', 53),
(56, 'Egresos', 'home.php?p=modules/capsinventory/view/newCapsInvOut.php', 53),
(55, 'Informe', 'home.php?p=modules/capsinventory/view/showInfCapsInventory.php', 52),
(63, 'Log Autorizaciones', 'home.php?p=modules/accounts/view/showInfAuthLog.php', 47),
(64, 'Autorizaciones', 'home.php?p=modules/booking/view/showUsers.php', 46),
(65, 'Productos por Defecto', 'home.php?p=modules/accounts/view/showProducts.php', 47),
(66, 'Autorizaciones', 'home.php?p=modules/posmodule/view/showSalesInf.php', 57),
(67, 'Autorizaciones', 'home.php?p=modules/posmodule/view/showUsers.php', 57),
(68, 'Log Autorizaciones', 'home.php?p=modules/posmodule/view/showInfAuthLog.php', 57),
(69, 'Setup', 'home.php?p=modules/posmodule/view/showSetup.php', 57),
(70, 'Inf. Ventas', 'home.php?p=modules/posmodule/view/showSalesInf.php', 59),
(71, 'Autorizaciones', 'home.php?p=modules/posmodule/view/showUsers.php', 59),
(72, 'Log Autorizaciones', 'home.php?p=modules/posmodule/view/showInfAuthLog.php', 59),
(73, 'Setup', 'home.php?p=modules/posmodule/view/showSetup.php', 59),
(74, 'Inf. Ventas', 'home.php?p=modules/posmodule/view/showSalesInf.php', 59),
(75, 'Autorizaciones', 'home.php?p=modules/posmodule/view/showUsers.php', 59),
(76, 'Log Autorizaciones', 'home.php?p=modules/posmodule/view/showInfAuthLog.php', 59),
(77, 'Setup', 'home.php?p=modules/posmodule/view/showSetup.php', 59),
(78, 'Inf. Ventas', 'home.php?p=modules/posmoduleadv/view/showSalesInf.php', 60),
(79, 'Autorizaciones', 'home.php?p=modules/posmoduleadv/view/showUsers.php', 60),
(80, 'Log Autorizaciones', 'home.php?p=modules/posmoduleadv/view/showInfAuthLog.php', 60),
(81, 'Setup', 'home.php?p=modules/posmoduleadv/view/showSetup.php', 60),
(82, 'Inf. gastos', 'home.php?p=modules/expenses/view/showExpensesInf.php', 92),
(83, 'Autorizaciones', 'home.php?p=modules/expenses/view/showExpensesAuth.php', 92),
(84, 'Setup', 'home.php?p=modules/expenses/view/showExpensesType.php', 92);

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `upload_img`
--

CREATE TABLE IF NOT EXISTS `upload_img` (
  `ui_id` int(11) NOT NULL AUTO_INCREMENT,
  `ui_nom` varchar(85) NOT NULL DEFAULT '',
  PRIMARY KEY (`ui_id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8 AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `user`
--

CREATE TABLE IF NOT EXISTS `user` (
  `u_id` int(11) NOT NULL AUTO_INCREMENT,
  `u_nom` varchar(66) NOT NULL DEFAULT '',
  `u_user` varchar(66) NOT NULL DEFAULT '',
  `u_pass` varchar(66) NOT NULL DEFAULT '',
  `u_active` int(11) NOT NULL DEFAULT '0',
  `u_mail` varchar(66) NOT NULL DEFAULT '',
  `u_sign` varchar(66) NOT NULL DEFAULT '',
  `u_cel` varchar(66) NOT NULL DEFAULT '',
  `up_id` int(11) NOT NULL DEFAULT '0',
  `isCommercial` varchar(100) NOT NULL,
  `letAuth` int(11) NOT NULL DEFAULT '0',
  `u_img` varchar(255) NOT NULL,
  `letAuthBooking` int(11) NOT NULL DEFAULT '0',
  `letAuthpos` int(11) NOT NULL DEFAULT '0',
  `letAuthposadv` int(11) NOT NULL DEFAULT '0',
  `crm_letAuth` int(100) NOT NULL,
  `isApprover` int(100) NOT NULL,
  PRIMARY KEY (`u_id`)
) ENGINE=MyISAM  DEFAULT CHARSET=utf8 AUTO_INCREMENT=39 ;

--
-- Volcado de datos para la tabla `user`
--

INSERT INTO `user` (`u_id`, `u_nom`, `u_user`, `u_pass`, `u_active`, `u_mail`, `u_sign`, `u_cel`, `up_id`, `isCommercial`, `letAuth`, `u_img`, `letAuthBooking`, `letAuthpos`, `letAuthposadv`, `crm_letAuth`, `isApprover`) VALUES
(1, 'admin', 'admin', '21232f297a57a5a743894a0e4a801fc3', 1, 'admin@mail.com', '', '123456', 1, '1', 1, 'usrImg/Tsonoqua Mask.gif', 1, 1, 1, 1, 1),
(16, 'jorge roa', 'jorge', 'd67326a22642a324aa1b0745f2f17abb', 0, '1@sdsa', '', '212', 4, '', 0, '', 0, 0, 0, 0, 0),
(17, 'Carlos Andres Neira', 'cana', '3017081de0a40206a60a490c5ac14496', 0, 'paintball.allimite@hotmail.com', '', '3105608644', 1, '', 1, 'usrImg/Snapshot_20141023_1.JPG', 1, 0, 0, 0, 0),
(18, 'Jorge leon', '1111', 'b59c67bf196a4758191e42f76670ceba', 0, '1@1111', '', '1111', 6, '', 0, '', 1, 0, 0, 0, 0),
(19, 'carolina', 'caro', '437612e345ed8c59db6e905a8dc0d1c6', 0, 'a@11', '', '123', 7, '', 0, '', 1, 0, 0, 0, 0),
(20, 'Camilo Betancur', 'ca', '5435c69ed3bcc5b2e4d580e393e373d3', 0, '12212', '', '2122112', 8, '', 0, '', 0, 0, 0, 0, 0),
(21, 'Felipe Huertas', 'FelipeH', 'c3070f4332e8174df01d954c09f464fd', 0, 'Felipehamaya@hotmail.com', '', '3108119661', 9, '', 0, 'usrImg/Snapshot_20141023.JPG', 0, 0, 0, 0, 0),
(22, 'Didier Garcia', 'dgarcia', 'd56ceb6a35375433ca87b7257ef04312', 0, 'didifercas666@gmail.com', '', '', 5, '', 0, '', 0, 0, 0, 0, 0),
(23, 'Jhonnatan Gamboa', '_____jhonnatan.gamboa@gmail.com', '168e99701a9625c8421490b412fb6e04', 0, 'jhonnatan.gamboa@gmail.com', '', '', 10, '', 0, '', 0, 0, 0, 0, 0),
(24, 'Marco Rios Garcia', 'marcorg_7_93@hotmail.com', '168e99701a9625c8421490b412fb6e04', 0, 'marcorg_7_93@hotmail.com', '', '', 10, '', 0, '', 0, 0, 0, 0, 0),
(25, '', 'demo', 'fe01ce2a7fbac8fafaed7c982a04e229', 0, '', '', '', 10, '', 0, '', 0, 0, 0, 0, 0),
(26, 'Guillermo Anduaga', 'ganduagas@hotmail.com', '9223f2b501f1fdace9c001c7612472ad', 0, 'ganduagas@hotmail.com', '', '', 10, '', 0, '', 0, 0, 0, 0, 0),
(27, 'demo', 'demo', 'd03e2688fcf08933b97202c7485150df', 1, '', '', '', 15, '', 0, '', 0, 0, 0, 0, 0),
(28, 'Luis Alberto Maldonado ', 'paintballsincelejo@hotmail.com', '3520a706a72f4efddd48d1a4081828bc', 0, 'paintballsincelejo@hotmail.com', '', '', 10, '', 0, '', 0, 0, 0, 0, 0),
(29, 'Mateo Fernandez', 'mateo.inglesa@hotmail.es', 'fe01ce2a7fbac8fafaed7c982a04e229', 0, 'mateo.inglesa@hotmail.es', '', '', 10, '', 1, '', 0, 1, 0, 0, 0),
(30, 'Maria Clara Rueda', 'mariac', '29578d764037bfd6968cfdd0353ed51c', 0, 'mcru2011@hotmail.com ', '', '', 11, '', 0, '', 0, 0, 0, 0, 0),
(31, 'Jorge Luis Rivera', 'jorge_rivera_ava@hotmail.com', '3520a706a72f4efddd48d1a4081828bc', 0, 'jorge_rivera_ava@hotmail.com', '', '', 10, '', 0, '', 0, 0, 0, 0, 0),
(32, 'Francisco Jimenez', 'tkdcolombia', '18a8875833adddc536589743c708f964', 0, 'fjimenezr@hotmail.com', '', '', 12, '', 0, '', 0, 0, 0, 0, 0),
(33, 'Envios Demo', 'envios', '0ee6cb775993d410537b1acd83d8fb20', 1, 'info@goely.co', '', '', 0, '', 0, '', 0, 0, 0, 0, 0),
(34, 'Envios', 'envios', '0ee6cb775993d410537b1acd83d8fb20', 0, 'info@goely.co', '', '', 13, '', 0, '', 0, 0, 0, 0, 0),
(35, 'test', 'test', 'e807f1fcf82d132f9bb018ca6738a19f', 1, 'info@goely.co', '', '654423', 10, '', 0, '', 0, 0, 0, 0, 0),
(36, 'Antonio Tupa', 'tonotg@hotmail.com', '2b15c88af47bbbac8987cf93c002f836', 0, 'tonotg@hotmail.com', '', '', 10, '', 0, '', 0, 0, 0, 0, 0),
(37, 'Jorge Illidge', 'jillidge@gmail.com', 'd03e2688fcf08933b97202c7485150df', 0, 'jillidge@gmail.com', '', '', 10, '', 0, '', 0, 0, 0, 0, 0),
(38, 'demomex', 'demomex', '96678abc2faa0eb9f0dc6469e9bd7a2e', 0, '', '', '', 10, '', 0, '', 0, 0, 0, 0, 0);

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `user_profile`
--

CREATE TABLE IF NOT EXISTS `user_profile` (
  `up_id` int(11) NOT NULL AUTO_INCREMENT,
  `up_nom` varchar(66) NOT NULL DEFAULT '',
  PRIMARY KEY (`up_id`)
) ENGINE=MyISAM  DEFAULT CHARSET=utf8 AUTO_INCREMENT=16 ;

--
-- Volcado de datos para la tabla `user_profile`
--

INSERT INTO `user_profile` (`up_id`, `up_nom`) VALUES
(1, 'Administrador'),
(5, 'admin'),
(6, 'Ventas'),
(7, 'Reservas'),
(8, 'Fotografia'),
(9, 'webmaster'),
(10, 'demo'),
(11, 'Cognos'),
(12, 'tkd'),
(13, 'Envios'),
(14, 'Prueba'),
(15, 'demo crm');

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
